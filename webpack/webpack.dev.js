const webpack = require('webpack');
const { merge } = require('webpack-merge');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const SimpleProgressWebpackPlugin = require('simple-progress-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');
const path = require('path');
const sass = require('sass');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const utils = require('./utils.js');
const commonConfig = require('./webpack.common.js');

const ENV = 'development';

module.exports = options =>
  merge(commonConfig({ env: ENV }), {
    devtool: 'cheap-module-source-map', // https://reactjs.org/docs/cross-origin-errors.html
    mode: ENV,
    entry: ['./src/main/webapp/app/index'],
    output: {
      path: utils.root('build/resources/main/static/'),
      filename: 'app/[name].bundle.js',
      chunkFilename: 'app/[id].chunk.js',
    },
    module: {
      rules: [
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            'style-loader',
            'css-loader',
            'postcss-loader',
            {
              loader: 'sass-loader',
              options: { implementation: sass },
            },
          ],
        },
      ],
    },
    devServer: {
      stats: options.stats,
      writeToDisk: true,
      hot: true,
      contentBase: './build/resources/main/static/',
      proxy: [
        {
          context: ['/api', '/services', '/management', '/swagger-resources', '/v2/api-docs', '/h2-console', '/auth'],
          target: `http${options.tls ? 's' : ''}://localhost:8000`,
          secure: false,
          changeOrigin: options.tls,
        },
        {
          context: ['/websocket'],
          target: 'ws://127.0.0.1:8000',
          ws: true,
        },
      ],
      watchOptions: {
        ignored: /node_modules/,
      },
      https: options.tls,
      historyApiFallback: true,
    },
    stats: process.env.JHI_DISABLE_WEBPACK_LOGS ? 'none' : options.stats,
    plugins: [
      new CopyWebpackPlugin({
        patterns: [
          { from: '*.{js,css,png}', to: 'swagger-ui', context: './node_modules/swagger-ui-dist/' },
          { from: './node_modules/axios/dist/axios.min.js', to: 'swagger-ui' },
          { from: './src/main/webapp//swagger-ui/', to: 'swagger-ui' },
          { from: './src/main/webapp/pdfjs/', to: 'pdfjs' },
          { from: './src/main/webapp/content/', to: 'content' },
          { from: './src/main/webapp/favicon.ico', to: 'favicon.ico' },
          { from: './src/main/webapp/manifest.webapp', to: 'manifest.webapp' },
          { from: './src/main/webapp/robots.txt', to: 'robots.txt' },
        ],
      }),
      process.env.JHI_DISABLE_WEBPACK_LOGS
        ? null
        : new SimpleProgressWebpackPlugin({
            format: options.stats === 'minimal' ? 'compact' : 'expanded',
          }),
      new FriendlyErrorsWebpackPlugin(),
      new BrowserSyncPlugin(
        {
          https: options.tls,
          host: 'localhost',
          port: 9000,
          proxy: {
            target: `http${options.tls ? 's' : ''}://localhost:9060`,
            ws: true,
            proxyOptions: {
              changeOrigin: false, //pass the Host header to the backend unchanged  https://github.com/Browsersync/browser-sync/issues/430
            },
          },
          socket: {
            clients: {
              heartbeatTimeout: 60000,
            },
          },
        },
        {
          reload: false,
        }
      ),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.WatchIgnorePlugin({
        paths: [utils.root('src/test')],
      }),
      new WebpackNotifierPlugin({
        title: 'Schoolaby',
        contentImage: path.join(__dirname, 'schoolaby_spinner.gif'),
      }),
    ].filter(Boolean),
  });
