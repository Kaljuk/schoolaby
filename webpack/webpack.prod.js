const webpack = require('webpack');
const { merge } = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const sass = require('sass');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const utils = require('./utils.js');
const commonConfig = require('./webpack.common.js');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');

const ENV = 'production';

module.exports = merge(commonConfig({ env: ENV }), {
  mode: ENV,
  entry: {
    main: './src/main/webapp/app/index',
  },
  output: {
    path: utils.root('build/resources/main/static/'),
    filename: 'app/[name].[contenthash].bundle.js',
    chunkFilename: 'app/[name].[contenthash].chunk.js',
  },
  module: {
    rules: [
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: '../',
            },
          },
          'css-loader',
          'postcss-loader',
          {
            loader: 'sass-loader',
            options: { implementation: sass },
          },
        ],
      },
    ],
  },
  optimization: {
    runtimeChunk: true,
    minimize: true,
    nodeEnv: 'production',
    minimizer: [new TerserPlugin(), new CssMinimizerPlugin()],
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        { from: './src/main/webapp/pdfjs/', to: 'pdfjs' },
        { from: './src/main/webapp/content/', to: 'content' },
        { from: './src/main/webapp/favicon.ico', to: 'favicon.ico' },
        { from: './src/main/webapp/manifest.webapp', to: 'manifest.webapp' },
        { from: './src/main/webapp/robots.txt', to: 'robots.txt' },
      ],
    }),
    new MiniCssExtractPlugin({
      ignoreOrder: true,
      filename: 'content/[name].[contenthash].css',
      chunkFilename: 'content/[name].[contenthash].css',
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
    }),
    new MomentLocalesPlugin({
      localesToKeep: ['en', 'et', 'fi', 'uk'],
    }),
  ],
});
