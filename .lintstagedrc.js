module.exports = {
  '{,src/**/}*.{json,md,tsx,ts,css,scss}': ['prettier --write', 'git add']
};
