import React, { FC, useCallback, useEffect, useState } from 'react';
import { Input } from 'app/shared/form';
import { Card } from 'reactstrap';
import { translate } from 'react-jhipster';
import debounce from 'lodash/debounce';

import './left-side-menu.scss';
import { ASSIGNMENT, MILESTONE } from 'app/shared/util/entity-utils';

export const SUGGESTED = 'suggested';
export const APPS = 'apps';

export type marketplaceTab = typeof SUGGESTED | typeof APPS;

interface LeftSideMenuProps {
  entityType: typeof ASSIGNMENT | typeof MILESTONE;
  setCurrentSearchString: (searchString: string) => void;
  activeTab: marketplaceTab;
  setActiveTab: (tab: marketplaceTab) => void;
}

const LeftSideMenu: FC<LeftSideMenuProps> = ({ entityType, setCurrentSearchString, activeTab, setActiveTab }) => {
  const [searchString, setSearchString] = useState<string>('');

  const inputProps = {
    onFocus() {},
    onBlur() {},
    onChange(e) {
      setSearchString(e.target.value);
    },
    name: 'search',
    value: searchString,
  };

  const debounceCurrentSearch = useCallback(
    debounce(value => setCurrentSearchString(value), 200),
    []
  );

  useEffect(() => {
    debounceCurrentSearch(searchString);
  }, [searchString]);

  return (
    <Card className={'left-side-menu'}>
      <Input meta={{}} input={inputProps} placeholder={translate('global.search')} iconName={'search'} />
      <div className={`${activeTab === SUGGESTED ? 'active' : ''} menu-item mb-1 cursor-pointer`} onClick={() => setActiveTab(SUGGESTED)}>
        {translate('entity.action.suggested')}
      </div>
      {entityType === ASSIGNMENT && (
        <div className={`${activeTab === APPS ? 'active' : ''} menu-item cursor-pointer`} onClick={() => setActiveTab(APPS)}>
          {translate('schoolabyApp.marketplace.apps.title')}
        </div>
      )}
    </Card>
  );
};

export default LeftSideMenu;
