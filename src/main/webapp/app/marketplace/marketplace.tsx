import React, { useEffect, useRef, useState } from 'react';
import ContentContainer from 'app/shared/layout/content-container/content-container';
import HeadingNew, { SUB_HEADING } from 'app/shared/layout/heading/heading-new';
import LeftSideMenu, { marketplaceTab, SUGGESTED } from 'app/marketplace/left-side-menu/left-side-menu';
import MaterialsSection from 'app/marketplace/materials-section/materials-section';

import './marketplace.scss';
import { useHistory } from 'react-router-dom';
import ChosenMaterialsSection from 'app/marketplace/chosen-materials-section/chosen-materials-section';
import {
  useEntityToUpdateState,
  useParentEducationalAlignmentsState,
  useSelectedEducationalAlignmentsState,
} from 'app/shared/contexts/entity-update-context';
import { useChosenMaterialState, useMaterialState } from 'app/shared/contexts/material-context';
import { IEducationalAlignment } from 'app/shared/model/educational-alignment.model';
import { translate } from 'react-jhipster';
import { ASSIGNMENT } from 'app/shared/util/entity-utils';
import AddOwnMaterialDropdown from 'app/marketplace/add-own-material/add-own-material-dropdown';
import MaterialCardModal from 'app/shared/layout/material-card/material-card-modal';
import AppsSection from 'app/marketplace/apps-section/apps-section';
import { mergeMaterials } from 'app/shared/util/lti-utils';
import { LTI_MATERIAL, Material } from 'app/shared/model/material.model';
import { IAssignment } from 'app/shared/model/assignment.model';
import { IMilestone } from 'app/shared/model/milestone.model';
import { LTI_MODAL_ACTION_UPDATE, LtiCreateModal } from 'app/shared/layout/lti/lti-create-modal';
import { createPortal } from 'react-dom';

export interface HistoryStateProps<entityType extends IAssignment | IMilestone> {
  entity: entityType;
  parentEducationalAlignments?: IEducationalAlignment[];
  back: Location;
  journeyId?: number;
  patch?: boolean;
}

const Marketplace = ({ match }) => {
  const history = useHistory();
  const [currentSearchString, setCurrentSearchString] = useState<string>('');
  const [activeTab, setActiveTab] = useState<marketplaceTab>(SUGGESTED);
  const [back, setBack] = useState<Location>();
  const entityType = match?.url?.split('/')[2]?.toUpperCase();
  const { entity, setEntity } = useEntityToUpdateState();
  const { selectedEducationalAlignments, setSelectedEducationalAlignments } = useSelectedEducationalAlignmentsState();
  const { setParentEducationalAlignments } = useParentEducationalAlignmentsState();
  const { setMaterials } = useMaterialState();
  const { chosenMaterial, setChosenMaterial } = useChosenMaterialState();
  const backHistoryState = useRef(null);
  const [journeyId, setJourneyId] = useState<number>(undefined);

  const toggleModal = () => {
    setChosenMaterial(null);
  };

  const setMergedMaterials = assignment => {
    if (assignment) {
      const resources = assignment?.ltiResources || [];
      setMaterials(mergeMaterials(assignment, resources));
    }
  };

  useEffect(() => {
    return () => {
      if (history.action === 'POP') {
        const historyState = backHistoryState.current;
        history.replace({
          ...historyState.back,
          state: { ...historyState.entity },
        });
      }
    };
  }, [history]);

  useEffect(() => {
    if (history.location.state) {
      const historyState =
        entityType === ASSIGNMENT
          ? (history.location.state as HistoryStateProps<IAssignment>)
          : (history.location.state as HistoryStateProps<IMilestone>);
      setEntity(historyState.entity);
      setJourneyId(entityType === ASSIGNMENT ? historyState.journeyId : historyState.entity.journeyId);
      setSelectedEducationalAlignments(historyState.entity.educationalAlignments);
      historyState.parentEducationalAlignments && setParentEducationalAlignments(historyState.parentEducationalAlignments);
      setBack(historyState.back);
      backHistoryState.current = historyState;

      historyState.entity.materials &&
        (entityType === ASSIGNMENT
          ? setMergedMaterials(historyState.entity)
          : setMaterials(historyState.entity.materials.map(material => Material.fromJson(material))));
    } else {
      history.push('/not_found');
    }
  }, [history.location.state]);

  const titleTranslationKey =
    entityType === ASSIGNMENT ? 'schoolabyApp.marketplace.assignment.title' : 'schoolabyApp.marketplace.milestone.title';

  return (
    <>
      <ContentContainer spinner={false} className={'marketplace-container'}>
        <HeadingNew
          isAllowedToModify
          title={translate(titleTranslationKey, { param: entity?.title })}
          headingType={SUB_HEADING}
          tags={selectedEducationalAlignments?.map(alignment => alignment.title)}
        >
          <AddOwnMaterialDropdown />
        </HeadingNew>
        <div className={'marketplace d-flex'}>
          <LeftSideMenu
            entityType={entityType}
            setCurrentSearchString={setCurrentSearchString}
            activeTab={activeTab}
            setActiveTab={setActiveTab}
          />
          {activeTab === SUGGESTED ? <MaterialsSection search={currentSearchString} /> : <AppsSection journeyId={journeyId} />}
          <ChosenMaterialsSection entityType={entityType} back={back} />
        </div>
      </ContentContainer>
      <MaterialCardModal
        isOpen={!!chosenMaterial && chosenMaterial?.type !== LTI_MATERIAL}
        material={chosenMaterial}
        addable
        removable
        toggleModal={toggleModal}
      />
      {chosenMaterial?.type === LTI_MATERIAL &&
        createPortal(
          <LtiCreateModal
            showModal={!!chosenMaterial}
            title={translate('schoolabyApp.assignment.lti.edit')}
            ltiResource={chosenMaterial?.getLtiResource()}
            onClose={toggleModal}
            journeyId={entity.journeyId}
            action={LTI_MODAL_ACTION_UPDATE}
          />,
          document.getElementById('lti-form')
        )}
    </>
  );
};

export default Marketplace;
