import React, { FC, useEffect } from 'react';
import { Switch, useHistory } from 'react-router-dom';
import Loadable from 'react-loadable';
import Logout from 'app/login/logout';
import PrivateRoute, { hasAnyAuthority } from 'app/shared/auth/private-route';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import PageNotFound from 'app/shared/error/page-not-found';
import { AUTHORITIES } from 'app/config/constants';
import HarIdTokenAuthenticationPage from 'app/harid/harid';
import OauthAuthenticationPage from 'app/harid/oauth';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import GradingScheme from 'app/administration/entities/grading-scheme';
import GradingSchemeValue from 'app/administration/entities/grading-scheme-value';
import EducationalAlignment from 'app/administration/entities/educational-alignment';
import PersonRole from 'app/administration/entities/person-role';
import { useStylesState } from 'app/shared/contexts/root-context';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { AccountSettingsHistoryStateProps } from 'app/account/settings/settings';

const Account = Loadable({
  loader: () => import(/* webpackChunkName: "account" */ 'app/account'),
  loading: () => <Spinner />,
});

const Admin = Loadable({
  loader: () => import(/* webpackChunkName: "administration" */ 'app/administration'),
  loading: () => <Spinner />,
});

const Login = Loadable({
  loader: () => import(/* webpackChunkName: "login" */ 'app/login'),
  loading: () => <Spinner />,
});

const Journey = Loadable({
  loader: () => import(/* webpackChunkName: "journey" */ 'app/journey'),
  loading: () => <Spinner />,
});

const Milestone = Loadable({
  loader: () => import(/* webpackChunkName: "milestone" */ 'app/milestone'),
  loading: () => <Spinner />,
});

const Assignment = Loadable({
  loader: () => import(/* webpackChunkName: "assignment" */ 'app/assignment'),
  loading: () => <Spinner />,
});

const Marketplace = Loadable({
  loader: () => import(/* webpackChunkName: "marketplace" */ 'app/marketplace'),
  loading: () => <Spinner />,
});

const Material = Loadable({
  loader: () => import(/* webpackChunkName: "material" */ 'app/material'),
  loading: () => <Spinner />,
});

const Dashboard = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ 'app/dashboard'),
  loading: () => <Spinner />,
});

const Conversations = Loadable({
  loader: () => import(/* webpackChunkName: "conversations" */ 'app/conversations'),
  loading: () => <Spinner />,
});

const Routes: FC<StateProps> = ({ account, isAuthenticated }) => {
  const allRoles = [AUTHORITIES.ADMIN, AUTHORITIES.USER, AUTHORITIES.TEACHER, AUTHORITIES.STUDENT, AUTHORITIES.PARENT];
  const isAdmin = hasAnyAuthority(account.authorities, [AUTHORITIES.ADMIN]);
  const { styles } = useStylesState();
  const history = useHistory();
  const SETTINGS_PATH = '/account/settings';

  const hasAccountRequiredFieldsFilled = () =>
    account.firstName?.length &&
    account.lastName?.length &&
    account.email?.length &&
    account.country?.length &&
    account.authorities?.length &&
    account.personRoles?.some(personRole => personRole?.school);

  const isPathnameSettingsUrl = () => {
    return history.location.pathname === SETTINGS_PATH;
  };

  const navigateToAccountSettings = () => {
    const historyState: AccountSettingsHistoryStateProps = {
      accountFieldsUnFilled: true,
      returnUrl: history.location.pathname + history.location.search,
    };
    history.push(SETTINGS_PATH, historyState);
  };

  useEffect(() => {
    if (!isAuthenticated || !account || isPathnameSettingsUrl()) {
      return;
    }
    if (hasAnyAuthority(account.authorities, [AUTHORITIES.ADMIN]) || hasAccountRequiredFieldsFilled()) {
      return;
    }
    navigateToAccountSettings();
  }, [history?.location, isAuthenticated, account]);

  return (
    <div className="view-routes" style={styles}>
      <Switch>
        <ErrorBoundaryRoute path="/account" component={Account} />
        <PrivateRoute path="/admin" component={Admin} hasAnyAuthorities={[AUTHORITIES.ADMIN]} />
        <ErrorBoundaryRoute path="/login" component={Login} />
        <PrivateRoute path="/logout" component={Logout} />
        <PrivateRoute path="/journey" component={Journey} />
        <PrivateRoute path="/milestone" component={Milestone} />
        <PrivateRoute path="/assignment" component={Assignment} />
        <PrivateRoute path="/marketplace" component={Marketplace} />
        <PrivateRoute path="/material" component={Material} />
        <PrivateRoute path="/" exact component={Dashboard} hasAnyAuthorities={allRoles} />
        <PrivateRoute path="/chats" exact component={Conversations} hasAnyAuthorities={allRoles} />
        <ErrorBoundaryRoute path="/authenticate/harid" component={HarIdTokenAuthenticationPage} />
        <ErrorBoundaryRoute path="/authenticate/oauth" component={OauthAuthenticationPage} />
        {isAdmin && (
          <>
            <ErrorBoundaryRoute path={'/grading-scheme'} component={GradingScheme} />
            <ErrorBoundaryRoute path={'/grading-scheme-value'} component={GradingSchemeValue} />
            <ErrorBoundaryRoute path={'/educational-alignment'} component={EducationalAlignment} />
            <ErrorBoundaryRoute path={'/person-role'} component={PersonRole} />
          </>
        )}

        <ErrorBoundaryRoute component={PageNotFound} />
      </Switch>
    </div>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  isAuthenticated: authentication.isAuthenticated,
  account: authentication.account,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(Routes);
