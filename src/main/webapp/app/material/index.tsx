import React from 'react';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import MaterialDeleteDialog from './material-delete-dialog';

const Routes = ({ match }) => <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={MaterialDeleteDialog} />;

export default Routes;
