import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { deleteEntity, reset } from './material.reducer';
import { compose } from 'redux';
import { useQueryParam } from 'app/shared/hooks/useQueryParam';
import { useGetJourney } from 'app/shared/services/journey-api';

interface IMaterialDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

const MaterialDeleteDialog = ({
  isOpen,
  history,
  toggle,
  id,
  match,
  journeyId: journeyIdProp,
  updateSuccess,
  deleteEntity: deleteAssignment,
  ...props
}: IMaterialDeleteDialogProps) => {
  const entityId = id || match?.params.id;
  const [{ journeyId }] = useQueryParam<{ journeyId: string }>();
  const derivedJourneyId = journeyIdProp || journeyId;
  useGetJourney(journeyId, updateSuccess && isOpen);

  const handleClose = () => {
    toggle(false);
    journeyId ? history.push(`/journey/${derivedJourneyId}`) : history.push('/material');
  };

  useEffect(() => {
    if (updateSuccess && isOpen) {
      props.reset();
      handleClose();
    }
  }, [updateSuccess]);

  const confirmDelete = () => {
    deleteAssignment(entityId);
  };

  return (
    <Modal isOpen={toggle && isOpen} toggle={handleClose}>
      <ModalHeader toggle={handleClose}>
        <Translate contentKey="entity.delete.title">Confirm delete operation</Translate>
      </ModalHeader>
      <ModalBody id="schoolabyApp.material.delete.question">
        <Translate contentKey="schoolabyApp.material.delete.question" interpolate={{ id: entityId }}>
          Are you sure you want to delete this Material?
        </Translate>
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={handleClose}>
          <FontAwesomeIcon icon="ban" />
          &nbsp;
          <Translate contentKey="entity.action.cancel">Cancel</Translate>
        </Button>
        <Button id="jhi-confirm-delete-material" color="danger" onClick={confirmDelete}>
          <FontAwesomeIcon icon="trash" />
          &nbsp;
          <Translate contentKey="entity.action.delete">Delete</Translate>
        </Button>
      </ModalFooter>
    </Modal>
  );
};

const mapStateToProps = ({ material: { updateSuccess } }: IRootState, { toggle, isOpen, id, journeyId }) => ({
  updateSuccess,
  id,
  toggle,
  isOpen,
  journeyId,
});

const mapDispatchToProps = { deleteEntity, reset };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default compose(connect(mapStateToProps, mapDispatchToProps), withRouter)(MaterialDeleteDialog);
