import React from 'react';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import Conversations from 'app/conversations/conversations';

const Routes = ({ match }) => <ErrorBoundaryRoute path={match.url} component={Conversations} />;

export default Routes;
