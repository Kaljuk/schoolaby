import './journey-avatar.scss';
import React, { FC } from 'react';
import { IJourney } from 'app/shared/model/journey.model';
import { getSubjectIcon } from 'app/shared/icons';

interface IJourneyAvatar {
  journey?: IJourney;
}

const JourneyAvatar: FC<IJourneyAvatar> = ({ journey }) => {
  return journey.subject ? (
    <div className="rounded-circle avatar journey-avatar">
      <span aria-hidden={'true'}>{getSubjectIcon(journey.subject.label)}</span>
    </div>
  ) : null;
};

export default React.memo(JourneyAvatar);
