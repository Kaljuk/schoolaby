import axios from 'axios';
import {
  ICrudDeleteAction,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  loadMoreDataWhenScrolled,
  parseHeaderForLinks,
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';

import { defaultValue, IEducationalAlignment } from 'app/shared/model/educational-alignment.model';

export const ACTION_TYPES = {
  FETCH_EDUCATIONALALIGNMENT_LIST: 'educationalAlignment/FETCH_EDUCATIONALALIGNMENT_LIST',
  FETCH_EDUCATIONALALIGNMENT: 'educationalAlignment/FETCH_EDUCATIONALALIGNMENT',
  CREATE_EDUCATIONALALIGNMENT: 'educationalAlignment/CREATE_EDUCATIONALALIGNMENT',
  UPDATE_EDUCATIONALALIGNMENT: 'educationalAlignment/UPDATE_EDUCATIONALALIGNMENT',
  DELETE_EDUCATIONALALIGNMENT: 'educationalAlignment/DELETE_EDUCATIONALALIGNMENT',
  RESET: 'educationalAlignment/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IEducationalAlignment>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type EducationalAlignmentState = Readonly<typeof initialState>;

// Reducer

export default (state: EducationalAlignmentState = initialState, action): EducationalAlignmentState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_EDUCATIONALALIGNMENT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_EDUCATIONALALIGNMENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_EDUCATIONALALIGNMENT):
    case REQUEST(ACTION_TYPES.UPDATE_EDUCATIONALALIGNMENT):
    case REQUEST(ACTION_TYPES.DELETE_EDUCATIONALALIGNMENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_EDUCATIONALALIGNMENT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_EDUCATIONALALIGNMENT):
    case FAILURE(ACTION_TYPES.CREATE_EDUCATIONALALIGNMENT):
    case FAILURE(ACTION_TYPES.UPDATE_EDUCATIONALALIGNMENT):
    case FAILURE(ACTION_TYPES.DELETE_EDUCATIONALALIGNMENT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_EDUCATIONALALIGNMENT_LIST): {
      const links = parseHeaderForLinks(action.payload.headers.link);

      return {
        ...state,
        loading: false,
        links,
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links),
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    }
    case SUCCESS(ACTION_TYPES.FETCH_EDUCATIONALALIGNMENT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_EDUCATIONALALIGNMENT):
    case SUCCESS(ACTION_TYPES.UPDATE_EDUCATIONALALIGNMENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_EDUCATIONALALIGNMENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/educational-alignments';

// Actions

export const getEntities: ICrudGetAllAction<IEducationalAlignment> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_EDUCATIONALALIGNMENT_LIST,
    payload: axios.get<IEducationalAlignment>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IEducationalAlignment> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_EDUCATIONALALIGNMENT,
    payload: axios.get<IEducationalAlignment>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IEducationalAlignment> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_EDUCATIONALALIGNMENT,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const updateEntity: ICrudPutAction<IEducationalAlignment> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_EDUCATIONALALIGNMENT,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IEducationalAlignment> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_EDUCATIONALALIGNMENT,
    payload: axios.delete(requestUrl),
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
