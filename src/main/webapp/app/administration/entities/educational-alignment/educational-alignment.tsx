import React, { useEffect, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { getSortState, TextFormat, Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities, reset } from './educational-alignment.reducer';
import { APP_DATE_TIME_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';

interface IEducationalAlignmentProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

const EducationalAlignment = (props: IEducationalAlignmentProps) => {
  const [paginationState, setPaginationState] = useState(
    overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE, 'id'), props.location.search)
  );
  const [sorting, setSorting] = useState(false);

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  const resetAll = () => {
    props.reset();
    setPaginationState({
      ...paginationState,
      activePage: 1,
    });
    props.getEntities();
  };

  useEffect(() => {
    resetAll();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      resetAll();
    }
  }, [props.updateSuccess]);

  useEffect(() => {
    getAllEntities();
  }, [paginationState.activePage]);

  const handleLoadMore = () => {
    if ((window as any).pageYOffset > 0) {
      setPaginationState({
        ...paginationState,
        activePage: paginationState.activePage + 1,
      });
    }
  };

  useEffect(() => {
    if (sorting) {
      getAllEntities();
      setSorting(false);
    }
  }, [sorting]);

  const sort = p => () => {
    props.reset();
    setPaginationState({
      ...paginationState,
      activePage: 1,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p,
    });
    setSorting(true);
  };

  const { educationalAlignmentList, match, loading } = props;
  return (
    <div>
      <h2 id="educational-alignment-heading">
        <Translate contentKey="schoolabyApp.educationalAlignment.home.title">Educational Alignments</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon={'plus'} />
          &nbsp;
          <Translate contentKey="schoolabyApp.educationalAlignment.home.createLabel">Create new Educational Alignment</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        <InfiniteScroll
          pageStart={paginationState.activePage}
          loadMore={handleLoadMore}
          hasMore={paginationState.activePage - 1 < props.links.next}
          loader={<div className="loader">Loading ...</div>}
          threshold={0}
          initialLoad={false}
        >
          {educationalAlignmentList && educationalAlignmentList.length > 0 ? (
            <Table responsive>
              <thead>
                <tr>
                  <th className="hand" onClick={sort('id')}>
                    <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('title')}>
                    <Translate contentKey="schoolabyApp.educationalAlignment.title">Title</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('alignmentType')}>
                    <Translate contentKey="schoolabyApp.educationalAlignment.alignmentType">Alignment Type</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('country')}>
                    <Translate contentKey="schoolabyApp.educationalAlignment.educationalFramework">Educational Framework</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('targetName')}>
                    <Translate contentKey="schoolabyApp.educationalAlignment.targetName">Target Name</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('targetUrl')}>
                    <Translate contentKey="schoolabyApp.educationalAlignment.targetUrl">Target Url</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('startDate')}>
                    <Translate contentKey="schoolabyApp.educationalAlignment.startDate">Start Date</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('endDate')}>
                    <Translate contentKey="schoolabyApp.educationalAlignment.endDate">End Date</Translate>
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('deleted')}>
                    <Translate contentKey="schoolabyApp.educationalAlignment.deleted">Deleted</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {educationalAlignmentList.map((educationalAlignment, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${educationalAlignment.id}`} color="link" size="sm">
                        {educationalAlignment.id}
                      </Button>
                    </td>
                    <td>{educationalAlignment.title}</td>
                    <td>{educationalAlignment.alignmentType}</td>
                    <td>{educationalAlignment.country}</td>
                    <td>{educationalAlignment.targetName}</td>
                    <td>{educationalAlignment.targetUrl}</td>
                    <td>
                      {educationalAlignment.startDate ? (
                        <TextFormat type="date" value={educationalAlignment.startDate} format={APP_DATE_TIME_FORMAT} />
                      ) : null}
                    </td>
                    <td>
                      {educationalAlignment.endDate ? (
                        <TextFormat type="date" value={educationalAlignment.endDate} format={APP_DATE_TIME_FORMAT} />
                      ) : null}
                    </td>
                    <td>
                      {educationalAlignment.deleted ? (
                        <TextFormat type="date" value={educationalAlignment.deleted} format={APP_DATE_TIME_FORMAT} />
                      ) : null}
                    </td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${educationalAlignment.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.view">View</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${educationalAlignment.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.edit">Edit</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${educationalAlignment.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.delete">Delete</Translate>
                          </span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            !loading && (
              <div className="alert alert-warning">
                <Translate contentKey="schoolabyApp.educationalAlignment.home.notFound">No Educational Alignments found</Translate>
              </div>
            )
          )}
        </InfiniteScroll>
      </div>
    </div>
  );
};

const mapStateToProps = ({ educationalAlignment }: IRootState) => ({
  educationalAlignmentList: educationalAlignment.entities,
  loading: educationalAlignment.loading,
  totalItems: educationalAlignment.totalItems,
  links: educationalAlignment.links,
  entity: educationalAlignment.entity,
  updateSuccess: educationalAlignment.updateSuccess,
});

const mapDispatchToProps = {
  getEntities,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(EducationalAlignment);
