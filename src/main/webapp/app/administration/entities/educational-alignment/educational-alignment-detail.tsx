import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row } from 'reactstrap';
import { TextFormat, Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './educational-alignment.reducer';
import { APP_DATE_TIME_FORMAT } from 'app/config/constants';

interface IEducationalAlignmentDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

const EducationalAlignmentDetail = (props: IEducationalAlignmentDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { educationalAlignmentEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="schoolabyApp.educationalAlignment.detail.title">EducationalAlignment</Translate> [
          <b>{educationalAlignmentEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="title">
              <Translate contentKey="schoolabyApp.educationalAlignment.title">Title</Translate>
            </span>
          </dt>
          <dd>{educationalAlignmentEntity.title}</dd>
          <dt>
            <span id="alignmentType">
              <Translate contentKey="schoolabyApp.educationalAlignment.alignmentType">Alignment Type</Translate>
            </span>
          </dt>
          <dd>{educationalAlignmentEntity.alignmentType}</dd>
          <dt>
            <span id="educationalFramework">
              <Translate contentKey="schoolabyApp.educationalAlignment.educationalFramework">Educational Framework</Translate>
            </span>
          </dt>
          <dd>{educationalAlignmentEntity.country}</dd>
          <dt>
            <span id="targetName">
              <Translate contentKey="schoolabyApp.educationalAlignment.targetName">Target Name</Translate>
            </span>
          </dt>
          <dd>{educationalAlignmentEntity.targetName}</dd>
          <dt>
            <span id="targetUrl">
              <Translate contentKey="schoolabyApp.educationalAlignment.targetUrl">Target Url</Translate>
            </span>
          </dt>
          <dd>{educationalAlignmentEntity.targetUrl}</dd>
          <dt>
            <span id="startDate">
              <Translate contentKey="schoolabyApp.educationalAlignment.startDate">Start Date</Translate>
            </span>
          </dt>
          <dd>
            {educationalAlignmentEntity.startDate ? (
              <TextFormat value={educationalAlignmentEntity.startDate} type="date" format={APP_DATE_TIME_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="endDate">
              <Translate contentKey="schoolabyApp.educationalAlignment.endDate">End Date</Translate>
            </span>
          </dt>
          <dd>
            {educationalAlignmentEntity.endDate ? (
              <TextFormat value={educationalAlignmentEntity.endDate} type="date" format={APP_DATE_TIME_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="deleted">
              <Translate contentKey="schoolabyApp.educationalAlignment.deleted">Deleted</Translate>
            </span>
          </dt>
          <dd>
            {educationalAlignmentEntity.deleted ? (
              <TextFormat value={educationalAlignmentEntity.deleted} type="date" format={APP_DATE_TIME_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <Translate contentKey="schoolabyApp.educationalAlignment.children">Children</Translate>
          </dt>
          <dd>
            {educationalAlignmentEntity.children
              ? educationalAlignmentEntity.children.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.id}</a>
                    {educationalAlignmentEntity.children && i === educationalAlignmentEntity.children.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
        </dl>
        <Button tag={Link} to="/educational-alignment" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/educational-alignment/${educationalAlignmentEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ educationalAlignment }: IRootState) => ({
  educationalAlignmentEntity: educationalAlignment.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(EducationalAlignmentDetail);
