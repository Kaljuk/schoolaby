import React, { useEffect, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { getSortState, TextFormat, Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities, reset } from './person-role.reducer';
import { APP_DATE_TIME_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';

interface IPersonRoleProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

const PersonRole = (props: IPersonRoleProps) => {
  const [paginationState, setPaginationState] = useState(
    overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE, 'id'), props.location.search)
  );
  const [sorting, setSorting] = useState(false);

  const getAllEntities = () => {
    props.getEntities(paginationState.activePage - 1, paginationState.itemsPerPage, `${paginationState.sort},${paginationState.order}`);
  };

  const resetAll = () => {
    props.reset();
    setPaginationState({
      ...paginationState,
      activePage: 1,
    });
    props.getEntities();
  };

  useEffect(() => {
    resetAll();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      resetAll();
    }
  }, [props.updateSuccess]);

  useEffect(() => {
    getAllEntities();
  }, [paginationState.activePage]);

  const handleLoadMore = () => {
    if ((window as any).pageYOffset > 0) {
      setPaginationState({
        ...paginationState,
        activePage: paginationState.activePage + 1,
      });
    }
  };

  useEffect(() => {
    if (sorting) {
      getAllEntities();
      setSorting(false);
    }
  }, [sorting]);

  const sort = p => () => {
    props.reset();
    setPaginationState({
      ...paginationState,
      activePage: 1,
      order: paginationState.order === 'asc' ? 'desc' : 'asc',
      sort: p,
    });
    setSorting(true);
  };

  const { personRoleList, match, loading } = props;
  return (
    <div>
      <h2 id="person-role-heading">
        <Translate contentKey="schoolabyApp.personRole.home.title">Person Roles</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon={'plus'} />
          &nbsp;
          <Translate contentKey="schoolabyApp.personRole.home.createLabel">Create new Person Role</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        <InfiniteScroll
          pageStart={paginationState.activePage}
          loadMore={handleLoadMore}
          hasMore={paginationState.activePage - 1 < props.links.next}
          loader={<div className="loader">Loading ...</div>}
          threshold={0}
          initialLoad={false}
        >
          {personRoleList && personRoleList.length > 0 ? (
            <Table responsive>
              <thead>
                <tr>
                  <th className="hand" onClick={sort('id')}>
                    <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('role')}>
                    <Translate contentKey="schoolabyApp.personRole.marker">Marker</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('active')}>
                    <Translate contentKey="schoolabyApp.personRole.active">Active</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('startDate')}>
                    <Translate contentKey="schoolabyApp.personRole.startDate">Start Date</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('endDate')}>
                    <Translate contentKey="schoolabyApp.personRole.endDate">End Date</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('providerEhisId')}>
                    <Translate contentKey="schoolabyApp.personRole.providerEhisId">Provider Ehis Id</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('providerRegNr')}>
                    <Translate contentKey="schoolabyApp.personRole.providerRegNr">Provider Reg Nr</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('providerName')}>
                    <Translate contentKey="schoolabyApp.personRole.providerName">Provider Name</Translate>
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('grade')}>
                    <Translate contentKey="schoolabyApp.personRole.grade">Grade</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('parallel')}>
                    <Translate contentKey="schoolabyApp.personRole.parallel">Parallel</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={sort('deleted')}>
                    <Translate contentKey="schoolabyApp.personRole.deleted">Deleted</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    <Translate contentKey="schoolabyApp.personRole.person">Person</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {personRoleList.map((personRole, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${personRole.id}`} color="link" size="sm">
                        {personRole.id}
                      </Button>
                    </td>
                    <td>{personRole.role}</td>
                    <td>{personRole?.active ? 'true' : 'false'}</td>
                    <td>
                      {personRole.startDate ? <TextFormat type="date" value={personRole.startDate} format={APP_DATE_TIME_FORMAT} /> : null}
                    </td>
                    <td>
                      {personRole.endDate ? <TextFormat type="date" value={personRole.endDate} format={APP_DATE_TIME_FORMAT} /> : null}
                    </td>
                    <td>{personRole.school.ehisId}</td>
                    <td>{personRole.school.regNr}</td>
                    <td>{personRole.school.name}</td>
                    <td>{personRole.grade}</td>
                    <td>{personRole.parallel}</td>
                    <td>
                      {personRole.deleted ? <TextFormat type="date" value={personRole.deleted} format={APP_DATE_TIME_FORMAT} /> : null}
                    </td>
                    <td>{personRole.personId ? <Link to={`person/${personRole.personId}`}>{personRole.personId}</Link> : ''}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${personRole.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.view">View</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${personRole.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.edit">Edit</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${personRole.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.delete">Delete</Translate>
                          </span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            !loading && (
              <div className="alert alert-warning">
                <Translate contentKey="schoolabyApp.personRole.home.notFound">No Person Roles found</Translate>
              </div>
            )
          )}
        </InfiniteScroll>
      </div>
    </div>
  );
};

const mapStateToProps = ({ personRole }: IRootState) => ({
  personRoleList: personRole.entities,
  loading: personRole.loading,
  totalItems: personRole.totalItems,
  links: personRole.links,
  entity: personRole.entity,
  updateSuccess: personRole.updateSuccess,
});

const mapDispatchToProps = {
  getEntities,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PersonRole);
