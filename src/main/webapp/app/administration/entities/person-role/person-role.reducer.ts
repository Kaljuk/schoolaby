import axios from 'axios';
import {
  ICrudDeleteAction,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  loadMoreDataWhenScrolled,
  parseHeaderForLinks,
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';

import { defaultValue, IPersonRole } from 'app/shared/model/person-role.model';

export const ACTION_TYPES = {
  FETCH_PERSONROLE_LIST: 'personRole/FETCH_PERSONROLE_LIST',
  FETCH_PERSONROLE: 'personRole/FETCH_PERSONROLE',
  CREATE_PERSONROLE: 'personRole/CREATE_PERSONROLE',
  UPDATE_PERSONROLE: 'personRole/UPDATE_PERSONROLE',
  DELETE_PERSONROLE: 'personRole/DELETE_PERSONROLE',
  RESET: 'personRole/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPersonRole>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type PersonRoleState = Readonly<typeof initialState>;

// Reducer

export default (state: PersonRoleState = initialState, action): PersonRoleState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PERSONROLE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PERSONROLE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_PERSONROLE):
    case REQUEST(ACTION_TYPES.UPDATE_PERSONROLE):
    case REQUEST(ACTION_TYPES.DELETE_PERSONROLE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_PERSONROLE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PERSONROLE):
    case FAILURE(ACTION_TYPES.CREATE_PERSONROLE):
    case FAILURE(ACTION_TYPES.UPDATE_PERSONROLE):
    case FAILURE(ACTION_TYPES.DELETE_PERSONROLE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PERSONROLE_LIST): {
      const links = parseHeaderForLinks(action.payload.headers.link);

      return {
        ...state,
        loading: false,
        links,
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links),
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    }
    case SUCCESS(ACTION_TYPES.FETCH_PERSONROLE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_PERSONROLE):
    case SUCCESS(ACTION_TYPES.UPDATE_PERSONROLE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_PERSONROLE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/person-roles';

// Actions

export const getEntities: ICrudGetAllAction<IPersonRole> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PERSONROLE_LIST,
    payload: axios.get<IPersonRole>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IPersonRole> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PERSONROLE,
    payload: axios.get<IPersonRole>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IPersonRole> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PERSONROLE,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const updateEntity: ICrudPutAction<IPersonRole> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PERSONROLE,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPersonRole> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PERSONROLE,
    payload: axios.delete(requestUrl),
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
