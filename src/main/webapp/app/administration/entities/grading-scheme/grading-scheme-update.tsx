import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Label, Row } from 'reactstrap';
import { AvField, AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { createEntity, getEntity, reset, updateEntity } from './grading-scheme.reducer';
import { convertDateTimeFromServer, convertDateTimeToServer, getStartOfDay } from 'app/shared/util/date-utils';

interface IGradingSchemeUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

const GradingSchemeUpdate = (props: IGradingSchemeUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { gradingSchemeEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/grading-scheme');
  };

  useEffect(() => {
    if (!isNew) {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.deleted = convertDateTimeToServer(values.deleted);

    if (errors.length === 0) {
      const entity = {
        ...gradingSchemeEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="schoolabyApp.gradingScheme.home.createOrEditLabel">
            <Translate contentKey="schoolabyApp.gradingScheme.home.createOrEditLabel">Create or edit a GradingScheme</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : gradingSchemeEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="grading-scheme-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="grading-scheme-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="nameLabel" for="grading-scheme-name">
                  <Translate contentKey="schoolabyApp.gradingScheme.name">Name</Translate>
                </Label>
                <AvField
                  id="grading-scheme-name"
                  type="text"
                  name="name"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="deletedLabel" for="grading-scheme-deleted">
                  <Translate contentKey="schoolabyApp.gradingScheme.deleted">Deleted</Translate>
                </Label>
                <AvInput
                  id="grading-scheme-deleted"
                  type="datetime-local"
                  className="form-control"
                  name="deleted"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? getStartOfDay() : convertDateTimeFromServer(props.gradingSchemeEntity.deleted)}
                />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/grading-scheme" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  gradingSchemeEntity: storeState.gradingScheme.entity,
  loading: storeState.gradingScheme.loading,
  updating: storeState.gradingScheme.updating,
  updateSuccess: storeState.gradingScheme.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(GradingSchemeUpdate);
