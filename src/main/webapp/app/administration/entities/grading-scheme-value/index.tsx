import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import GradingSchemeValue from './grading-scheme-value';
import GradingSchemeValueDetail from './grading-scheme-value-detail';
import GradingSchemeValueUpdate from './grading-scheme-value-update';
import GradingSchemeValueDeleteDialog from './grading-scheme-value-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={GradingSchemeValueUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={GradingSchemeValueUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={GradingSchemeValueDetail} />
      <ErrorBoundaryRoute path={match.url} component={GradingSchemeValue} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={GradingSchemeValueDeleteDialog} />
  </>
);

export default Routes;
