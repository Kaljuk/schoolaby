import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Label, Row } from 'reactstrap';
import { AvField, AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';
import { getEntities as getGradingSchemes } from 'app/administration/entities/grading-scheme/grading-scheme.reducer';
import { createEntity, getEntity, reset, updateEntity } from './grading-scheme-value.reducer';
import { convertDateTimeFromServer, convertDateTimeToServer, getStartOfDay } from 'app/shared/util/date-utils';

interface IGradingSchemeValueUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

const GradingSchemeValueUpdate = (props: IGradingSchemeValueUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { gradingSchemeValueEntity, gradingSchemes, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/grading-scheme-value');
  };

  useEffect(() => {
    if (!isNew) {
      props.getEntity(props.match.params.id);
    }

    props.getGradingSchemes();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.deleted = convertDateTimeToServer(values.deleted);

    if (errors.length === 0) {
      const entity = {
        ...gradingSchemeValueEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="schoolabyApp.gradingSchemeValue.home.createOrEditLabel">
            <Translate contentKey="schoolabyApp.gradingSchemeValue.home.createOrEditLabel">Create or edit a GradingSchemeValue</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : gradingSchemeValueEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="grading-scheme-value-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="grading-scheme-value-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="gradeLabel" for="grading-scheme-value-grade">
                  <Translate contentKey="schoolabyApp.gradingSchemeValue.grade">Grade</Translate>
                </Label>
                <AvField
                  id="grading-scheme-value-grade"
                  type="text"
                  name="grade"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="percentageRangeLabel" for="grading-scheme-value-percentageRange">
                  <Translate contentKey="schoolabyApp.gradingSchemeValue.percentageRange">Percentage Range</Translate>
                </Label>
                <AvField
                  id="grading-scheme-value-percentageRange"
                  type="text"
                  name="percentageRange"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    pattern: {
                      value: '[0-9]|[1-8][0-9]|9[0-9]|100',
                      errorMessage: translate('entity.validation.pattern', { pattern: '[0-9]|[1-8][0-9]|9[0-9]|100' }),
                    },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="deletedLabel" for="grading-scheme-value-deleted">
                  <Translate contentKey="schoolabyApp.gradingSchemeValue.deleted">Deleted</Translate>
                </Label>
                <AvInput
                  id="grading-scheme-value-deleted"
                  type="datetime-local"
                  className="form-control"
                  name="deleted"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? getStartOfDay() : convertDateTimeFromServer(props.gradingSchemeValueEntity.deleted)}
                />
              </AvGroup>
              <AvGroup>
                <Label for="grading-scheme-value-gradingScheme">
                  <Translate contentKey="schoolabyApp.gradingSchemeValue.gradingScheme">Grading Scheme</Translate>
                </Label>
                <AvInput id="grading-scheme-value-gradingScheme" type="select" className="form-control" name="gradingSchemeId">
                  <option value="" key="0" />
                  {gradingSchemes
                    ? gradingSchemes.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/grading-scheme-value" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  gradingSchemes: storeState.gradingScheme.entities,
  gradingSchemeValueEntity: storeState.gradingSchemeValue.entity,
  loading: storeState.gradingSchemeValue.loading,
  updating: storeState.gradingSchemeValue.updating,
  updateSuccess: storeState.gradingSchemeValue.updateSuccess,
});

const mapDispatchToProps = {
  getGradingSchemes,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(GradingSchemeValueUpdate);
