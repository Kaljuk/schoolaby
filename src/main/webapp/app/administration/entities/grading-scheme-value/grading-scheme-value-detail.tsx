import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row } from 'reactstrap';
import { TextFormat, Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './grading-scheme-value.reducer';
import { APP_DATE_TIME_FORMAT } from 'app/config/constants';

interface IGradingSchemeValueDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

const GradingSchemeValueDetail = (props: IGradingSchemeValueDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { gradingSchemeValueEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="schoolabyApp.gradingSchemeValue.detail.title">GradingSchemeValue</Translate> [
          <b>{gradingSchemeValueEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="grade">
              <Translate contentKey="schoolabyApp.gradingSchemeValue.grade">Grade</Translate>
            </span>
          </dt>
          <dd>{gradingSchemeValueEntity.grade}</dd>
          <dt>
            <span id="percentageRange">
              <Translate contentKey="schoolabyApp.gradingSchemeValue.percentageRange">Percentage Range</Translate>
            </span>
          </dt>
          <dd>{gradingSchemeValueEntity.percentageRangeStart}</dd>
          <dt>
            <span id="deleted">
              <Translate contentKey="schoolabyApp.gradingSchemeValue.deleted">Deleted</Translate>
            </span>
          </dt>
          <dd>
            {gradingSchemeValueEntity.deleted ? (
              <TextFormat value={gradingSchemeValueEntity.deleted} type="date" format={APP_DATE_TIME_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <Translate contentKey="schoolabyApp.gradingSchemeValue.gradingScheme">Grading Scheme</Translate>
          </dt>
          <dd>{gradingSchemeValueEntity.gradingSchemeId ? gradingSchemeValueEntity.gradingSchemeId : ''}</dd>
        </dl>
        <Button tag={Link} to="/grading-scheme-value" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/grading-scheme-value/${gradingSchemeValueEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ gradingSchemeValue }: IRootState) => ({
  gradingSchemeValueEntity: gradingSchemeValue.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(GradingSchemeValueDetail);
