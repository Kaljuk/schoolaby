import axios from 'axios';
import {
  ICrudDeleteAction,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  loadMoreDataWhenScrolled,
  parseHeaderForLinks,
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { FAILURE, REQUEST, SUCCESS } from 'app/shared/reducers/action-type.util';

import { defaultValue, IGradingSchemeValue } from 'app/shared/model/grading-scheme-value.model';

export const ACTION_TYPES = {
  FETCH_GRADINGSCHEMEVALUE_LIST: 'gradingSchemeValue/FETCH_GRADINGSCHEMEVALUE_LIST',
  FETCH_GRADINGSCHEMEVALUE: 'gradingSchemeValue/FETCH_GRADINGSCHEMEVALUE',
  CREATE_GRADINGSCHEMEVALUE: 'gradingSchemeValue/CREATE_GRADINGSCHEMEVALUE',
  UPDATE_GRADINGSCHEMEVALUE: 'gradingSchemeValue/UPDATE_GRADINGSCHEMEVALUE',
  DELETE_GRADINGSCHEMEVALUE: 'gradingSchemeValue/DELETE_GRADINGSCHEMEVALUE',
  RESET: 'gradingSchemeValue/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IGradingSchemeValue>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type GradingSchemeValueState = Readonly<typeof initialState>;

// Reducer

export default (state: GradingSchemeValueState = initialState, action): GradingSchemeValueState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_GRADINGSCHEMEVALUE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_GRADINGSCHEMEVALUE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_GRADINGSCHEMEVALUE):
    case REQUEST(ACTION_TYPES.UPDATE_GRADINGSCHEMEVALUE):
    case REQUEST(ACTION_TYPES.DELETE_GRADINGSCHEMEVALUE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_GRADINGSCHEMEVALUE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_GRADINGSCHEMEVALUE):
    case FAILURE(ACTION_TYPES.CREATE_GRADINGSCHEMEVALUE):
    case FAILURE(ACTION_TYPES.UPDATE_GRADINGSCHEMEVALUE):
    case FAILURE(ACTION_TYPES.DELETE_GRADINGSCHEMEVALUE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_GRADINGSCHEMEVALUE_LIST): {
      const links = parseHeaderForLinks(action.payload.headers.link);

      return {
        ...state,
        loading: false,
        links,
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links),
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    }
    case SUCCESS(ACTION_TYPES.FETCH_GRADINGSCHEMEVALUE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_GRADINGSCHEMEVALUE):
    case SUCCESS(ACTION_TYPES.UPDATE_GRADINGSCHEMEVALUE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_GRADINGSCHEMEVALUE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/grading-scheme-values';

// Actions

export const getEntities: ICrudGetAllAction<IGradingSchemeValue> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_GRADINGSCHEMEVALUE_LIST,
    payload: axios.get<IGradingSchemeValue>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IGradingSchemeValue> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_GRADINGSCHEMEVALUE,
    payload: axios.get<IGradingSchemeValue>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IGradingSchemeValue> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_GRADINGSCHEMEVALUE,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const updateEntity: ICrudPutAction<IGradingSchemeValue> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_GRADINGSCHEMEVALUE,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IGradingSchemeValue> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_GRADINGSCHEMEVALUE,
    payload: axios.delete(requestUrl),
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
