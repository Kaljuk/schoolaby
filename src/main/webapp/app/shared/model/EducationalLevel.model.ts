export interface IEducationalLevel {
  id?: number;
  name?: string;
}

export const defaultValue: Readonly<IEducationalLevel> = {};
