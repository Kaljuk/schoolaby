export interface IUploadedFile {
  id?: number;
  uniqueName?: string;
  originalName?: string;
  type?: string;
  extension?: string;
  deleted?: string;
}

export const defaultValue: Readonly<IUploadedFile> = {};
