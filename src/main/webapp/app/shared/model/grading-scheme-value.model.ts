export interface IGradingSchemeValue {
  id?: number;
  grade?: string;
  percentageRangeStart?: number;
  percentageRangeEnd?: number;
  deleted?: string;
  gradingSchemeId?: number;
}

export const defaultValue: Readonly<IGradingSchemeValue> = {};
