import { IUploadedFile } from 'app/shared/model/uploaded-file.model';

export interface ISchool {
  id?: number;
  ehisId?: string;
  regNr?: string;
  name?: string;
  country?: string;
  uploadedFile?: IUploadedFile;
}
