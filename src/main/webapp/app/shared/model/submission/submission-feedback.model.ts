import { ISubmission } from 'app/shared/model/submission/submission.model';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import { ISelectedCritreionLevel } from 'app/shared/model/rubric/selected-critreion-level.model';

export interface ISubmissionFeedback {
  id?: number;
  grade?: string;
  value?: string;
  submissionId?: string;
  studentId?: string;
  groupId?: string;
  creatorId?: number;
  feedbackDate: string;
  submission?: ISubmission;
  selectedCriterionLevels?: ISelectedCritreionLevel[];
  uploadedFiles?: IUploadedFile[];
}
