import { IMessage } from 'app/shared/model/message.model';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';
import { IUser } from 'app/shared/model/user.model';

export interface ISubmission {
  id?: number;
  value?: string;
  deleted?: string;
  messages?: IMessage[];
  uploadedFiles?: IUploadedFile[];
  authors?: IUser[];
  assignmentId?: number;
  assignmentTitle?: string;
  resubmittable?: boolean;
  submittedForGradingDate?: string;
  highestGrade?: boolean;
  groupId?: number;
  groupName?: string;
  submissionFeedbacks?: ISubmissionFeedback[];
}

export const defaultValue: Readonly<ISubmission> = {};
