import { ISubmissionPatch } from 'app/shared/model/submission/submission-patch.model';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';
import { ISelectedCritreionLevel } from 'app/shared/model/rubric/selected-critreion-level.model';

export interface ISubmissionGradingPatch extends ISubmissionPatch {
  resubmittable: boolean;
  submittedForGradingDate: string;
  feedbackFiles?: IUploadedFile[];
  submissionFeedback: ISubmissionFeedback;
  selectedCriterionLevels?: ISelectedCritreionLevel[];
  validateExistingFeedback?: boolean;
}
