import { IUser } from 'app/shared/model/user.model';

export interface IChat {
  id?: number;
  journeyId?: number;
  submissionId?: number;
  title?: string;
  people?: IUser[];
}

export const defaultValue: Readonly<IChat> = {};
