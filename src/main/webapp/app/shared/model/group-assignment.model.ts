import { IGroup } from 'app/shared/model/group.model';

export interface IGroupAssignment {
  id?: number;
  title?: string;
  createdDate?: string;
  groups?: IGroup[];
}
