export default interface ISupportEmail {
  subject?: string;
  content?: string;
}
