import { ILtiApp } from './lti-app.model';
import { ILineItem } from 'app/shared/model/line-item.model';

export interface ILtiResource {
  id?: number;
  ltiContentItemId?: number;
  assignmentId?: number;
  resourceLinkId: string;
  launchUrl?: string;
  toolResourceId?: string;
  title: string;
  description: string;
  syncGrade?: boolean;
  ltiApp: ILtiApp;
  lineItems?: ILineItem[];
  createdBy?: string;
  createdDate?: string;
  sequenceNumber?: number;
}
