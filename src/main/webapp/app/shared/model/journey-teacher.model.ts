import { IUser } from 'app/shared/model/user.model';

export interface IJourneyTeacher {
  user?: IUser;
  joinedDate?: string;
}
