import { IGradingSchemeValue } from 'app/shared/model/grading-scheme-value.model';
import { IAssignment } from 'app/shared/model/assignment.model';

export const NUMERICAL_1_5 = 'NUMERICAL_1_5';
export const ALPHABETICAL_A_F = 'ALPHABETICAL_A_F';
export const PASS_FAIL = 'PASS_FAIL';
export const PASS = 'PASS';
export const FAIL = 'FAIL';
export const NARRATIVE = 'NARRATIVE';
export const PERCENTAGE_0_100 = 'PERCENTAGE_0_100';
export const NUMERICAL_1_12 = 'NUMERICAL_1_12';
export const NONE = 'NONE';

const gradingSchemesWithoutGrade = [NARRATIVE, NONE];

export type GradeType =
  | typeof NUMERICAL_1_5
  | typeof ALPHABETICAL_A_F
  | typeof PASS_FAIL
  | typeof NARRATIVE
  | typeof PERCENTAGE_0_100
  | typeof NUMERICAL_1_12
  | typeof NONE;

export interface IGradingScheme {
  id?: number;
  name?: string;
  deleted?: string;
  values?: IGradingSchemeValue[];
  assignments?: IAssignment[];
  code?: GradeType;
  sequenceNumber?: number;
}

export class GradingScheme {
  id?: number;
  name?: string;
  deleted?: string;
  values?: IGradingSchemeValue[];
  assignments?: IAssignment[];
  code?: GradeType;
  sequenceNumber?: number;

  static fromJson(gradingSchemeJson) {
    const gradingScheme = new GradingScheme();
    gradingScheme.id = gradingSchemeJson.id;
    gradingScheme.name = gradingSchemeJson.name;
    gradingScheme.deleted = gradingSchemeJson.deleted;
    gradingScheme.values = gradingSchemeJson.values;
    gradingScheme.assignments = gradingSchemeJson.assignments;
    gradingScheme.code = gradingSchemeJson.code;
    gradingScheme.sequenceNumber = gradingSchemeJson.sequenceNumber;

    return gradingScheme;
  }

  isNonGradable(): boolean {
    return !!gradingSchemesWithoutGrade.find(s => s === this.code);
  }
}

export const defaultValue: Readonly<IGradingScheme> = {};
