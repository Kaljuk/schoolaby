import { IEducationalLevel } from 'app/shared/model/EducationalLevel.model';
import { ILtiConfig } from 'app/shared/model/lti-config.model';
import ISubject from 'app/shared/model/subject.model';
import { ISchool } from 'app/shared/model/school.model';
import { ICurriculum } from 'app/shared/model/curriculum.model';
import { IJourneyTeacher } from 'app/shared/model/journey-teacher.model';

export enum JourneyState {
  COMPLETED = 'COMPLETED',
  IN_PROGRESS = 'IN_PROGRESS',
  NOT_STARTED = 'NOT_STARTED',
}

export interface IJourney {
  id?: number;
  title?: string;
  description?: string;
  videoConferenceUrl?: string;
  signUpCode?: string;
  startDate?: string;
  endDate?: string;
  educationalLevel?: IEducationalLevel;
  studentCount?: number;
  teachers?: IJourneyTeacher[];
  creatorId?: number;
  template?: boolean;
  state?: JourneyState;
  teacherName?: string;
  imageUrl?: string;
  ltiConfigs?: ILtiConfig[];
  nationalCurriculum?: boolean;
  subject?: ISubject;
  schools?: ISchool[];
  progress?: number;
  curriculum?: ICurriculum;
  assignmentsCount?: number;
  yearsCount?: number;
  monthsCount?: number;
  daysCount?: number;
  teacherSignupCode?: string;
  studentSignupCode?: string;
  owner?: boolean;
}

export const defaultValue: Readonly<IJourney> = {};
