import { ILtiResource } from 'app/shared/model/lti-resource.model';

export interface ILtiLaunch {
  id?: number;
  result?: string;
  ltiResource?: ILtiResource;
  createdDate?: string;
}
