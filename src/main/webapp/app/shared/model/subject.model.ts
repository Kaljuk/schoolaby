export default interface ISubject {
  id: number;
  label?: string;
}
