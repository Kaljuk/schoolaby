import { IPerson } from 'app/shared/model/person.model';

export interface IGroup {
  id?: number;
  name: string;
  students: IPerson[];
  hasSubmission: boolean;
}
