export interface ILtiApp {
  id: number;
  name: string;
  description: string;
  imageUrl: string;
  version: string;
  clientId: string;
  launchUrl?: string;
  deepLinkingUrl?: string;
  loginInitiationUrl?: string;
  redirectHost?: string;
  jwksUrl?: string;
}
