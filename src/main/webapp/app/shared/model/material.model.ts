import { IEducationalAlignment } from 'app/shared/model/educational-alignment.model';
import { getImageUrlByName, getMaterial, getMaterialUrl } from 'app/shared/services/suggestion-api';
import { IEKoolikottMaterial, IEKoolikottReducedMaterial } from 'app/shared/model/e-koolikott/e-koolikott-material.model';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import { getAbsolutePath } from 'app/shared/util/navigation-utils';
import { withHttp } from 'app/shared/util/file-utils';
import { ILtiResource } from 'app/shared/model/lti-resource.model';
import clone from 'lodash/clone';

export interface IMaterial {
  id?: number;
  title?: string;
  type?: string;
  description?: string;
  restricted?: boolean;
  externalId?: string;
  url?: string;
  created?: string;
  deleted?: string;
  createdBy?: string;
  educationalAlignments?: IEducationalAlignment[];
  sequenceNumber?: number;
}

export const E_KOOLIKOTT_MATERIAL = '.EkoolikottMaterial';
export const OPIQ_MATERIAL = '.OpiqMaterial';
export const LTI_MATERIAL = 'LTI';
export const MANUAL_MATERIAL = 'MANUAL';

export class Material {
  private refreshPromise: Promise<any>;
  private kkMaterial?: IEKoolikottMaterial;
  private kkReducedMaterial?: IEKoolikottReducedMaterial;

  id?: number;
  title?: string;
  type: string;
  description?: string;
  restricted?: boolean;
  externalId?: string;
  created?: string;
  deleted?: string;
  educationalAlignments?: IEducationalAlignment[];
  url?: string;
  imageUrl?: string;
  createdBy?: string;
  uploadedFile?: IUploadedFile;
  sequenceNumber?: number;
  ltiResource?: ILtiResource;
  country?: string;

  private constructor() {}

  static fromJson(materialJson) {
    const material = new Material();
    material.id = materialJson.id;
    material.title = materialJson.title;
    material.type = materialJson.type;
    material.description = materialJson.description;
    material.restricted = materialJson.restricted;
    material.externalId = materialJson.externalId;
    material.created = materialJson.created;
    material.deleted = materialJson.deleted;
    material.educationalAlignments = materialJson.educationalAlignments;
    material.url = materialJson.url;
    material.imageUrl = materialJson.imageUrl;
    material.createdBy = materialJson.createdBy;
    material.uploadedFile = materialJson.uploadedFile;
    material.sequenceNumber = materialJson.sequenceNumber;
    material.country = materialJson.country;

    return material;
  }

  // E-Koolikott minimized search result
  static fromReducedEkoolikottMaterial(json: IEKoolikottReducedMaterial): Material {
    const material = new Material();
    material.externalId = String(json.id);
    material.type = E_KOOLIKOTT_MATERIAL;
    material.title = json.title;

    material.kkReducedMaterial = json;

    return material;
  }

  static fromEkoolikottMaterial(json: IEKoolikottMaterial): Material {
    const material = this.fromReducedEkoolikottMaterial(json);
    material.description = json.description;
    material.created = json.added;
    material.url = getMaterialUrl(json);

    material.kkMaterial = json;
    material.refreshPromise = Promise.resolve(true);

    return material;
  }

  static fromLtiResource(ltiResource: ILtiResource) {
    const material = new Material();
    material.id = ltiResource.id;
    material.title = ltiResource.title;
    material.type = 'LTI';
    material.description = ltiResource.description;
    material.created = ltiResource.createdDate;
    material.imageUrl = ltiResource.ltiApp.imageUrl;
    material.createdBy = ltiResource.createdBy;
    material.sequenceNumber = ltiResource.sequenceNumber;
    material.ltiResource = ltiResource;

    return material;
  }

  static instantiate(material): Material {
    const materialInstant = Material.fromJson(material);
    materialInstant.ltiResource = material.ltiResource;

    return materialInstant;
  }

  isLti(): boolean {
    return this.type === 'LTI' || !!this.ltiResource;
  }

  async getCreated(): Promise<string> {
    if (this.isFromEKoolikott() && !this.kkReducedMaterial) {
      await this.refresh();
    }
    return this.created;
  }

  async refresh(): Promise<void> {
    if (this.refreshPromise === undefined) {
      if (this.isFromEKoolikott()) {
        this.refreshPromise = getMaterial(+this.externalId).then(
          result => result && Object.assign(this, Material.fromEkoolikottMaterial(result))
        );
      }
    }
    await this.refreshPromise;
  }

  async getImageUrl(): Promise<string | undefined> {
    if (this.isFromEKoolikott()) {
      if (!this.kkReducedMaterial) {
        await this.refresh();
        if (this.kkMaterial?.thumbnailName) {
          return getImageUrlByName(this.kkMaterial?.thumbnailName);
        }
      }
    } else if (this.isFromOpiq()) {
      return this.imageUrl;
    }

    return this.imageUrl;
  }

  async getMaterialUrl(): Promise<string | undefined> {
    if (this.isFromEKoolikott()) {
      if (this.kkReducedMaterial) {
        return getMaterialUrl(this.kkReducedMaterial);
      }

      await this.refresh();
      return getMaterialUrl(this.kkMaterial);
    } else if (this.isFromOpiq()) {
      return this.url;
    }
  }

  async getUrl(): Promise<string> {
    await this.refresh();
    if (this.uploadedFile) {
      return getAbsolutePath(this.url);
    }
    return withHttp(this.url);
  }

  async getDescription(): Promise<string> {
    await this.refresh();
    return this.description;
  }

  async getTitle(): Promise<string> {
    if (this.isFromEKoolikott() && !this.kkReducedMaterial) {
      await this.refresh();
    }
    return this.title;
  }

  getCopy() {
    const copy = clone(this);
    copy.refreshPromise = undefined;
    return copy;
  }

  getLtiResource(): ILtiResource {
    this.ltiResource.sequenceNumber = this?.sequenceNumber;
    return this.ltiResource;
  }

  isFromEKoolikott(): boolean {
    return this.type === E_KOOLIKOTT_MATERIAL;
  }

  isFromOpiq(): boolean {
    return this.type === OPIQ_MATERIAL;
  }

  isManuallyAdded(): boolean {
    return this.type === 'LINK' || this.type === 'FILE';
  }

  isOwnedBy(login: string) {
    return login === this.createdBy;
  }

  getType() {
    return this.isManuallyAdded() ? MANUAL_MATERIAL : this.type;
  }

  getKey(): string {
    if (this.externalId) {
      return this.type + this.externalId + this.url;
    }
    return this.type + this.title + this.url;
  }
}

export const defaultValue: Readonly<IMaterial> = {};
