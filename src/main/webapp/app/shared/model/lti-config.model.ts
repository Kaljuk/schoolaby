import { ILtiApp } from 'app/shared/model/lti-app.model';

type ILtiConfigType = 'JOURNEY' | 'PLATFORM';

export interface ILtiConfig {
  id?: number;
  consumerKey: string;
  sharedSecret: string;
  ltiApp: ILtiApp;
  journeyId?: number;
  deploymentId: string;
  type: ILtiConfigType;
  launchUrl?: string;
  deepLinkingUrl?: string;
  loginInitiationUrl?: string;
  redirectHost?: string;
  jwksUrl?: string;
}
