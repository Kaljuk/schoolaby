import { ICriterionLevel } from 'app/shared/model/rubric/criterion-level.model';

export interface ISelectedCritreionLevel {
  id?: number;
  modifiedDescription: string;
  criterionLevel: ICriterionLevel;
}
