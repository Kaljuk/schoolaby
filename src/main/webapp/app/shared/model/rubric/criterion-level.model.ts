export interface ICriterionLevel {
  id?: number;
  title: string;
  points: number;
  description: string;
  sequenceNumber: number;
  criterionId?: number;
}
