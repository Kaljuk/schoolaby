import { IMilestone } from 'app/shared/model/milestone.model';
import { IJourney } from 'app/shared/model/journey.model';
import { IAssignment } from 'app/shared/model/assignment.model';
import { IPersonRole } from 'app/shared/model/person-role.model';
import { ISubmission } from 'app/shared/model/submission/submission.model';

export interface IPerson {
  id?: number;
  firstName?: string;
  lastName?: string;
  email?: string;
  phoneNumber?: string;
  personalCode?: string;
  deleted?: string;
  login?: string;
  authorities?: any[];
  milestones?: IMilestone[];
  createdJourneys?: IJourney[];
  createdAssignments?: IAssignment[];
  personRoles?: IPersonRole[];
  assignments?: IAssignment[];
  submissions?: ISubmission[];
  journeys?: IJourney[];
}

export const defaultValue: Readonly<IPerson> = {};
