import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const DraggableIcon = ({
  name,
  style = {},
  fill = 'transparent',
  width = '20px',
  height = '20px',
  viewBox = '0 0 20 20',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill={fill}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M10.833 5.00004V9.16671H14.9997V6.45837L18.5413 10L14.9997 13.5417V10.8334H10.833V15H13.5413L9.99967 18.5417L6.45801 15H9.16634V10.8334H4.99967V13.5417L1.45801 10L4.99967 6.45837V9.16671H9.16634V5.00004H6.45801L9.99967 1.45837L13.5413 5.00004H10.833Z"
      fill={fill}
    />
  </svg>
);

export default DraggableIcon;
