import React from 'react';
import { ICON_LIGHTER_GREY } from 'app/shared/util/color-utils';

const CalendarNewIcon = ({
  ariaLabel = '',
  style = {},
  fill = ICON_LIGHTER_GREY,
  width = '16',
  height = '17',
  viewBox = '0 0 16 17',
  className = '',
}) => (
  <svg
    aria-label={ariaLabel}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M13.75 3.13989H11.125V2.13989C11.125 2.07114 11.0688 2.01489 11 2.01489H10.125C10.0562 2.01489 10 2.07114 10 2.13989V3.13989H6V2.13989C6 2.07114 5.94375 2.01489 5.875 2.01489H5C4.93125 2.01489 4.875 2.07114 4.875 2.13989V3.13989H2.25C1.97344 3.13989 1.75 3.36333 1.75 3.63989V14.0149C1.75 14.2915 1.97344 14.5149 2.25 14.5149H13.75C14.0266 14.5149 14.25 14.2915 14.25 14.0149V3.63989C14.25 3.36333 14.0266 3.13989 13.75 3.13989ZM13.125 13.3899H2.875V7.45239H13.125V13.3899ZM2.875 6.38989V4.26489H4.875V5.01489C4.875 5.08364 4.93125 5.13989 5 5.13989H5.875C5.94375 5.13989 6 5.08364 6 5.01489V4.26489H10V5.01489C10 5.08364 10.0562 5.13989 10.125 5.13989H11C11.0688 5.13989 11.125 5.08364 11.125 5.01489V4.26489H13.125V6.38989H2.875Z"
      fill={fill}
    />
  </svg>
);

export default CalendarNewIcon;
