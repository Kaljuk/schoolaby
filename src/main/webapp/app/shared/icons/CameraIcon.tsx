import React from 'react';
import { ICON_WHITE } from 'app/shared/util/color-utils';

const CameraIcon = ({
  style = {},
  fill = 'none',
  stroke = ICON_WHITE,
  width = '36px',
  height = '31px',
  viewBox = '0 0 94 82',
  className = '',
  strokeWidth = '5px',
}) => (
  <svg className={className} style={style} width={width} height={height} viewBox={viewBox} fill={fill} xmlns="http://www.w3.org/2000/svg">
    <path
      d="M47 68C58.598 68 68 58.598 68 47C68 35.402 58.598 26 47 26C35.402 26 26 35.402 26 47C26 58.598 35.402 68 47 68Z"
      stroke={stroke}
      strokeWidth={strokeWidth}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M32 14L35 2H59L62 14H92V80H2V14H47"
      stroke={stroke}
      strokeWidth={strokeWidth}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default CameraIcon;
