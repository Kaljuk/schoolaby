import React from 'react';
import { ICON_WHITE } from 'app/shared/util/color-utils';

const RoundedBellIcon = ({ width = '20px', height = '19px', viewBox = '0 0 20 19', fill = ICON_WHITE }) => (
  <svg width={width} height={height} viewBox={viewBox} fill="none" xmlns="http://www.w3.org/2000/svg">
    <ellipse opacity="0.3" cx="10.3071" cy="9.16743" rx="9.4858" ry="9" fill="black" />
    <g clipPath="url(#clip0_159_1741)">
      <path
        d="M13.6013 10.6377C13.0701 10.2117 12.7655 9.58901 12.7655 8.92934V8.00001C12.7655 6.82701 11.8471 5.85601 10.6576 5.69334V5.33334C10.6576 5.14901 10.5002 5.00001 10.3062 5.00001C10.1123 5.00001 9.9549 5.14901 9.9549 5.33334V5.69334C8.76496 5.85601 7.84695 6.82701 7.84695 8.00001V8.92934C7.84695 9.58901 7.54235 10.2117 7.00798 10.6403C6.87131 10.7513 6.79297 10.9127 6.79297 11.0833C6.79297 11.405 7.06876 11.6667 7.40779 11.6667H13.2047C13.5437 11.6667 13.8195 11.405 13.8195 11.0833C13.8195 10.9127 13.7411 10.7513 13.6013 10.6377Z"
        fill={fill}
      />
      <path d="M10.3059 13C10.9422 13 11.4744 12.5697 11.5967 12H9.01514C9.1374 12.5697 9.66966 13 10.3059 13Z" fill={fill} />
    </g>
    <defs>
      <clipPath id="clip0_159_1741">
        <rect width="8.43182" height="8" fill="white" transform="translate(6.09082 5.00002)" />
      </clipPath>
    </defs>
  </svg>
);

export default RoundedBellIcon;
