import React from 'react';
import SmallFlaskIcon from './SmallFlaskIcon';
import BookWithTextIcon from './BookWithTextIcon';
import LightBulbIcon from './LightBulbIcon';
import PencilIcon from './PencilIcon';
import CalculatorIcon from './CalculatorIcon';
import HatIcon from './HatIcon';
import BasketBallIcon from './BasketBallIcon';
import HomeIcon from './HomeIcon';
import GearIcon from './GearIcon';
import PeopleIcon from './PeopleIcon';
import SquaresIcon from './SquaresIcon';
import SquaresEmptyIcon from './SquaresEmptyIcon';
import MailIcon from './MailIcon';
import SendIcon from './SendIcon';
import BellIcon from './BellIcon';
import ConversationIcon from './ConversationIcon';
import TrashIcon from './TrashIcon';
import BookIcon from './BookIcon';
import CheckmarkIcon from 'app/shared/icons/CheckmarkIcon';
import TrophyIcon from 'app/shared/icons/TrophyIcon';
import DropdownIcon from 'app/shared/icons/DropdownIcon';
import GraduateIcon from 'app/shared/icons/GraduateIcon';
import CrossIcon from 'app/shared/icons/CrossIcon';
import ExportIcon from 'app/shared/icons/ExportIcon';
import CopyLinkIcon from 'app/shared/icons/CopyLinkIcon';
import CopyIcon from 'app/shared/icons/CopyIcon';
import ZipFileExtension from 'app/shared/icons/zipFileExtension';
import DocFileExtension from 'app/shared/icons/docFileExtension';
import PdfFileExtension from 'app/shared/icons/pdfFileExtension';
import DefaultFileExtension from 'app/shared/icons/defaultFileExtension';
import ExclamationIcon from 'app/shared/icons/ExclamationIcon';
import EyeIcon from 'app/shared/icons/EyeIcon';
import PencilTiltedIcon from 'app/shared/icons/PencilTiltedIcon';
import AnalyticsIcon from './AnalyticsIcon';
import ExclamationMarkIcon from './ExclamationMarkIcon';
import { INFO, PRIMARY } from '../util/color-utils';
import HomeThinIcon from 'app/shared/icons/HomeThinIcon';
import VideoIcon from 'app/shared/icons/VideoIcon';
import CalendarIcon from 'app/shared/icons/CalendarIcon';
import ExclamationCircleFilledIcon from 'app/shared/icons/ExclamationCircleFilledIcon';
import MagnifyingGlassIcon from 'app/shared/icons/MagnifyingGlassIcon';
import ChevronLeftIcon from 'app/shared/icons/ChevronLeftIcon';
import ChevronRightIcon from 'app/shared/icons/ChevronRightIcon';
import DownloadIcon from 'app/shared/icons/DownloadIcon';
import MapIcon from 'app/shared/icons/MapIcon';
import SquaresEmptyThinIcon from 'app/shared/icons/SquaresEmptyThinIcon';
import UserIcon from 'app/shared/icons/UserIcon';
import CameraIcon from 'app/shared/icons/CameraIcon';
import CameraRotationIcon from 'app/shared/icons/CameraRotationIcon';
import WallClock from 'app/shared/icons/WallClock';
import PreferencesIcon from 'app/shared/icons/PreferencesIcon';
import EditIcon from 'app/shared/icons/EditIcon';
import GlobeIcon from 'app/shared/icons/GlobeIcon';
import MusicIcon from 'app/shared/icons/MusicIcon';
import ComputerIcon from 'app/shared/icons/ComputerIcon';
import NotebookIcon from 'app/shared/icons/NotebookIcon';
import SchoolBellIcon from 'app/shared/icons/SchoolbellIcon';
import ScissorsIcon from 'app/shared/icons/ScissorsIcon';
import AtomIcon from 'app/shared/icons/AtomIcon';
import BigFlaskIcon from 'app/shared/icons/BigFlaskIcon';
import PeopleCircledIcon from 'app/shared/icons/PeopleCircledIcon';
import DraggableIcon from 'app/shared/icons/DraggableIcon';
import GroupAssignmentIcon from 'app/shared/icons/GroupAssignmentIcon';
import EstonianFlagIcon from 'app/shared/icons/EstonianFlagIcon';
import UKFlagIcon from 'app/shared/icons/UKFlagIcon';
import FinnishFlagIcon from 'app/shared/icons/FinnishFlagIcon';
import ArrowDownIcon from 'app/shared/icons/ArrowDownIcon';
import CalendarNewIcon from 'app/shared/icons/CalendarNewIcon';
import SearchIcon from 'app/shared/icons/SearchIcon';
import CameraBlackIcon from 'app/shared/icons/CameraBlackIcon';
import PinIcon from 'app/shared/icons/PinIcon';
import SelectedPinIcon from 'app/shared/icons/SelectedPinIcon';
import CaretDownIcon from 'app/shared/icons/CaretDownIcon';
import ColoredUserIcon from 'app/shared/icons/ColoredUserIcon';
import ColoredTasksIcon from 'app/shared/icons/ColoredTasksIcon';
import ColoredTimeIcon from 'app/shared/icons/ColoredTimeIcon';
import ColoredHatIcon from 'app/shared/icons/ColoredHatIcon';
import ListViewIcon from 'app/shared/icons/ListViewIcon';
import GridViewIcon from 'app/shared/icons/GridViewIcon';
import ChatIcon from 'app/shared/icons/ChatIcon';
import StopwatchIcon from 'app/shared/icons/StopwatchIcon';
import RoundedBellIcon from 'app/shared/icons/RoundedBellIcon';
import GroupIcon from 'app/shared/icons/GroupIcon';
import InviteIcon from 'app/shared/icons/InviteIcon';
import DestinationIcon from 'app/shared/icons/DestinationIcon';
import BackpackIcon from 'app/shared/icons/BackpackIcon';
import AppsIcon from 'app/shared/icons/AppsIcon';
import AttentionIcon from 'app/shared/icons/AttentionIcon';
import EditNewIcon from 'app/shared/icons/EditNewIcon';
import HomeNewIcon from 'app/shared/icons/HomeNewIcon';
import CloseIcon from 'app/shared/icons/CloseIcon';
import PlusIcon from 'app/shared/icons/PlusIcon';
import CheckmarkNewIcon from 'app/shared/icons/CheckmarkNewIcon';
import TrashNewIcon from 'app/shared/icons/TrashNewIcon';
import FilesIcon from 'app/shared/icons/FilesIcon';
import StatisticsIcon from 'app/shared/icons/StatisticsIcon';
import AddIcon from 'app/shared/icons/AddIcon';
import TanzanianFlagIcon from 'app/shared/icons/TanzanianFlagIcon';
import PlayIcon from 'app/shared/icons/PlayIcon';
import XlsFileExtension from 'app/shared/icons/xlsFileExtension';
import UploadIcon from 'app/shared/icons/UploadIcon';
import CameraNewIcon from 'app/shared/icons/CameraNewIcon';
import PenAndRulerIcon from 'app/shared/icons/PenAndRulerIcon';
import UkrainianFlagIcon from 'app/shared/icons/UkrainianFlagIcon';
import ChatBubble from 'app/shared/icons/ChatBubble';

export type IconName =
  | 'smallFlask'
  | 'basketball'
  | 'lightbulb'
  | 'calculator'
  | 'hat'
  | 'bookWithText'
  | 'pencil'
  | 'home'
  | 'gear'
  | 'people'
  | 'squares'
  | 'squaresEmpty'
  | 'mail'
  | 'send'
  | 'bell'
  | 'roundedBell'
  | 'conversation'
  | 'trash'
  | 'book'
  | 'checkmark'
  | 'trophy'
  | 'dropdown'
  | 'graduate'
  | 'cross'
  | 'export'
  | 'copyLink'
  | 'zipFileExtension'
  | 'pdfFileExtension'
  | 'docFileExtension'
  | 'xlsFileExtension'
  | 'defaultFileExtension'
  | 'eye'
  | 'pencilTilted'
  | 'exclamation'
  | 'copy'
  | 'homeThin'
  | 'analytics'
  | 'exclamationMark'
  | 'video'
  | 'magnifyingGlass'
  | 'chevronLeft'
  | 'chevronRight'
  | 'download'
  | 'calendar'
  | 'calendarNew'
  | 'exclamationCircleFilled'
  | 'map'
  | 'squaresEmptyThin'
  | 'user'
  | 'camera'
  | 'cameraRotation'
  | 'cameraBlack'
  | 'wallClock'
  | 'preferences'
  | 'edit'
  | 'editNew'
  | 'globe'
  | 'music'
  | 'computer'
  | 'notebook'
  | 'schoolBell'
  | 'scissors'
  | 'atom'
  | 'bigFlask'
  | 'peopleCircled'
  | 'draggable'
  | 'groupAssignment'
  | 'group'
  | 'et'
  | 'en'
  | 'fi'
  | 'sw'
  | 'uk'
  | 'arrowDown'
  | 'pin'
  | 'selectedPin'
  | 'search'
  | 'caretDown'
  | 'coloredUser'
  | 'coloredTime'
  | 'coloredTasks'
  | 'coloredHat'
  | 'listView'
  | 'gridView'
  | 'chat'
  | 'stopwatch'
  | 'invite'
  | 'destination'
  | 'backpack'
  | 'apps'
  | 'attentionIcon'
  | 'homeNew'
  | 'close'
  | 'plus'
  | 'checkmarkNew'
  | 'trashNew'
  | 'files'
  | 'statisticsIcon'
  | 'add'
  | 'play'
  | 'upload'
  | 'cameraNew'
  | 'penAndRuler'
  | 'chatBubble'
  | '';

interface IconInterface {
  ariaLabel?: string;
  style?: React.CSSProperties;
  fill?: string;
  stroke?: string;
  width?: string;
  height?: string;
  viewBox?: string;
  className?: string;
  name: IconName;
  role?: string;
  strokeWidth?: string;
  onClick?: (any) => void;
}

/* eslint-disable complexity */
const Icon = (props: IconInterface) => {
  switch (props.name) {
    case 'smallFlask':
      return <SmallFlaskIcon {...props} />;
    case 'basketball':
      return <BasketBallIcon {...props} />;
    case 'lightbulb':
      return <LightBulbIcon {...props} />;
    case 'calculator':
      return <CalculatorIcon {...props} />;
    case 'hat':
      return <HatIcon {...props} />;
    case 'bookWithText':
      return <BookWithTextIcon {...props} />;
    case 'pencil':
      return <PencilIcon {...props} />;
    case 'home':
      return <HomeIcon {...props} />;
    case 'homeThin':
      return <HomeThinIcon {...props} />;
    case 'gear':
      return <GearIcon {...props} />;
    case 'people':
      return <PeopleIcon {...props} />;
    case 'squares':
      return <SquaresIcon {...props} />;
    case 'squaresEmpty':
      return <SquaresEmptyIcon {...props} />;
    case 'mail':
      return <MailIcon {...props} />;
    case 'send':
      return <SendIcon {...props} />;
    case 'bell':
      return <BellIcon {...props} />;
    case 'roundedBell':
      return <RoundedBellIcon {...props} />;
    case 'conversation':
      return <ConversationIcon {...props} />;
    case 'trash':
      return <TrashIcon {...props} />;
    case 'book':
      return <BookIcon {...props} />;
    case 'checkmark':
      return <CheckmarkIcon {...props} />;
    case 'trophy':
      return <TrophyIcon {...props} />;
    case 'dropdown':
      return <DropdownIcon {...props} />;
    case 'graduate':
      return <GraduateIcon {...props} />;
    case 'export':
      return <ExportIcon {...props} />;
    case 'copyLink':
      return <CopyLinkIcon {...props} />;
    case 'copy':
      return <CopyIcon {...props} />;
    case 'cross':
      return <CrossIcon {...props} />;
    case 'zipFileExtension':
      return <ZipFileExtension {...props} />;
    case 'docFileExtension':
      return <DocFileExtension {...props} />;
    case 'xlsFileExtension':
      return <XlsFileExtension {...props} />;
    case 'pdfFileExtension':
      return <PdfFileExtension {...props} />;
    case 'exclamation':
      return <ExclamationIcon {...props} />;
    case 'defaultFileExtension':
      return <DefaultFileExtension {...props} />;
    case 'eye':
      return <EyeIcon {...props} />;
    case 'pencilTilted':
      return <PencilTiltedIcon {...props} />;
    case 'analytics':
      return <AnalyticsIcon {...props} />;
    case 'exclamationMark':
      return <ExclamationMarkIcon {...props} />;
    case 'video':
      return <VideoIcon {...props} />;
    case 'magnifyingGlass':
      return <MagnifyingGlassIcon {...props} />;
    case 'chevronLeft':
      return <ChevronLeftIcon {...props} />;
    case 'chevronRight':
      return <ChevronRightIcon {...props} />;
    case 'download':
      return <DownloadIcon {...props} />;
    case 'calendar':
      return <CalendarIcon {...props} />;
    case 'calendarNew':
      return <CalendarNewIcon {...props} />;
    case 'exclamationCircleFilled':
      return <ExclamationCircleFilledIcon {...props} />;
    case 'map':
      return <MapIcon {...props} />;
    case 'squaresEmptyThin':
      return <SquaresEmptyThinIcon {...props} />;
    case 'user':
      return <UserIcon {...props} />;
    case 'camera':
      return <CameraIcon {...props} />;
    case 'cameraRotation':
      return <CameraRotationIcon {...props} />;
    case 'cameraBlack':
      return <CameraBlackIcon {...props} />;
    case 'wallClock':
      return <WallClock {...props} />;
    case 'preferences':
      return <PreferencesIcon {...props} />;
    case 'edit':
      return <EditIcon {...props} />;
    case 'editNew':
      return <EditNewIcon {...props} />;
    case 'globe':
      return <GlobeIcon {...props} />;
    case 'music':
      return <MusicIcon {...props} />;
    case 'computer':
      return <ComputerIcon {...props} />;
    case 'notebook':
      return <NotebookIcon {...props} />;
    case 'schoolBell':
      return <SchoolBellIcon {...props} />;
    case 'scissors':
      return <ScissorsIcon {...props} />;
    case 'atom':
      return <AtomIcon {...props} />;
    case 'bigFlask':
      return <BigFlaskIcon {...props} />;
    case 'peopleCircled':
      return <PeopleCircledIcon {...props} />;
    case 'draggable':
      return <DraggableIcon {...props} />;
    case 'groupAssignment':
      return <GroupAssignmentIcon {...props} />;
    case 'et':
      return <EstonianFlagIcon {...props} />;
    case 'en':
      return <UKFlagIcon {...props} />;
    case 'fi':
      return <FinnishFlagIcon {...props} />;
    case 'sw':
      return <TanzanianFlagIcon {...props} />;
    case 'uk':
      return <UkrainianFlagIcon {...props} />;
    case 'arrowDown':
      return <ArrowDownIcon {...props} />;
    case 'pin':
      return <PinIcon {...props} />;
    case 'selectedPin':
      return <SelectedPinIcon {...props} />;
    case 'search':
      return <SearchIcon {...props} />;
    case 'caretDown':
      return <CaretDownIcon {...props} />;
    case 'coloredUser':
      return <ColoredUserIcon {...props} />;
    case 'coloredTime':
      return <ColoredTimeIcon {...props} />;
    case 'coloredTasks':
      return <ColoredTasksIcon {...props} />;
    case 'coloredHat':
      return <ColoredHatIcon {...props} />;
    case 'listView':
      return <ListViewIcon {...props} />;
    case 'gridView':
      return <GridViewIcon {...props} />;
    case 'chat':
      return <ChatIcon {...props} />;
    case 'stopwatch':
      return <StopwatchIcon {...props} />;
    case 'invite':
      return <InviteIcon {...props} />;
    case 'destination':
      return <DestinationIcon {...props} />;
    case 'backpack':
      return <BackpackIcon {...props} />;
    case 'apps':
      return <AppsIcon {...props} />;
    case 'attentionIcon':
      return <AttentionIcon {...props} />;
    case 'group':
      return <GroupIcon {...props} />;
    case 'homeNew':
      return <HomeNewIcon {...props} />;
    case 'close':
      return <CloseIcon {...props} />;
    case 'plus':
      return <PlusIcon {...props} />;
    case 'checkmarkNew':
      return <CheckmarkNewIcon {...props} />;
    case 'trashNew':
      return <TrashNewIcon {...props} />;
    case 'files':
      return <FilesIcon {...props} />;
    case 'statisticsIcon':
      return <StatisticsIcon {...props} />;
    case 'add':
      return <AddIcon {...props} />;
    case 'play':
      return <PlayIcon {...props} />;
    case 'upload':
      return <UploadIcon {...props} />;
    case 'cameraNew':
      return <CameraNewIcon {...props} />;
    case 'penAndRuler':
      return <PenAndRulerIcon {...props} />;
    case 'chatBubble':
      return <ChatBubble {...props} />;
    default:
      return;
  }
};
/* eslint-enable complexity */

const subjectDefaultStrokeColor = 'transparent';
const fileExtensionDefaultFillColor = INFO;
const fileExtensionDefaultStrokeColor = 'transparent';

const fileExtensionIcons = {
  Zip: 'zipFileExtension',
  Doc: 'docFileExtension',
  Docx: 'docFileExtension',
  Xls: 'xlsFileExtension',
  Xlsx: 'xlsFileExtension',
  Pdf: 'pdfFileExtension',
  Default: 'defaultFileExtension',
};

const subjectIcons = {
  'schoolabyApp.subject.estonian': 'bookWithText',
  'schoolabyApp.subject.russian': 'bookWithText',
  'schoolabyApp.subject.englishForeign': 'bookWithText',
  'schoolabyApp.subject.germanForeign': 'bookWithText',
  'schoolabyApp.subject.russianForeign': 'bookWithText',
  'schoolabyApp.subject.frenchForeign': 'bookWithText',
  'schoolabyApp.subject.estonianForeign': 'bookWithText',
  'schoolabyApp.subject.mathematics': 'calculator',
  'schoolabyApp.subject.natureEducation': 'smallFlask',
  'schoolabyApp.subject.biology': 'smallFlask',
  'schoolabyApp.subject.geography': 'globe',
  'schoolabyApp.subject.physics': 'atom',
  'schoolabyApp.subject.chemistry': 'bigFlask',
  'schoolabyApp.subject.humanities': 'hat',
  'schoolabyApp.subject.history': 'hat',
  'schoolabyApp.subject.socialStudies': 'hat',
  'schoolabyApp.subject.music': 'music',
  'schoolabyApp.subject.art': 'pencil',
  'schoolabyApp.subject.industrialArts': 'pencil',
  'schoolabyApp.subject.craftsAndHousehold': 'scissors',
  'schoolabyApp.subject.technology': 'lightbulb',
  'schoolabyApp.subject.physicalEducation': 'basketball',
  'schoolabyApp.subject.literature': 'bookWithText',
  'schoolabyApp.subject.nationalDefence': 'hat',
  'schoolabyApp.subject.religiousEducation': 'hat',
  'schoolabyApp.subject.economics': 'notebook',
  'schoolabyApp.subject.finnish': 'bookWithText',
  'schoolabyApp.subject.informatics': 'computer',
  'schoolabyApp.subject.generalEducation': 'schoolBell',
  'schoolabyApp.subject.cyberDefence': 'computer',
  'schoolabyApp.subject.swedish': 'bookWithText',
  'schoolabyApp.subject.other': 'lightbulb',
  'schoolabyApp.subject.kiswahili': 'bookWithText',
  'schoolabyApp.subject.english': 'bookWithText',
  'schoolabyApp.subject.french': 'bookWithText',
  'schoolabyApp.subject.arabic': 'bookWithText',
  'schoolabyApp.subject.informationAndComputerStudies': 'computer',
  'schoolabyApp.subject.technicalEducation': 'computer',
  'schoolabyApp.subject.agricultureAndHomeEconomics': 'lightbulb',
  'schoolabyApp.subject.civics': 'hat',
  'schoolabyApp.subject.commerce': 'notebook',
  'schoolabyApp.subject.bookKeeping': 'calculator',
  'schoolabyApp.subject.fineArts': 'pencil',
  'schoolabyApp.subject.theatreArts': 'lightbulb',
};

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export const getSubjectIcon = (subjectLabel?: string, width?: string, height?: string, fill?: string) => {
  const icon = subjectLabel ? subjectIcons[subjectLabel] : undefined;
  return (
    <Icon name={icon ? icon : 'lightbulb'} fill={fill ? fill : PRIMARY} stroke={subjectDefaultStrokeColor} width={width} height={height} />
  );
};

export const getHeadingIcon = (subjectLabel = 'default') => <span className={'heading-icon'}>{getSubjectIcon(subjectLabel)}</span>;

export const getSubjectHeadingIcon = (subjectLabel?: string, width?: string, height?: string, fill?: string) => (
  <span className={'heading-icon'}>{getSubjectIcon(subjectLabel ?? '', width, height, fill)}</span>
);

export const getFileExtensionIcon = (fileExtension?: string) => {
  const icon = fileExtension ? fileExtensionIcons[capitalizeFirstLetter(fileExtension)] : undefined;
  return (
    <Icon
      name={icon ? icon : 'defaultFileExtension'}
      width={'32px'}
      height={'32px'}
      fill={fileExtensionDefaultFillColor}
      stroke={fileExtensionDefaultStrokeColor}
    />
  );
};

export default Icon;
