import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const CrossIcon = ({
  name,
  style = {},
  fill = 'transparent',
  stroke = ICON_STROKE_DEFAULT,
  width = '16px',
  height = '16px',
  viewBox = '0 0 16 16',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M15.5945 13.6364C16.1352 14.177 16.1352 15.0539 15.5945 15.5945C15.3241 15.865 14.9699 16.0001 14.6154 16.0001C14.261 16.0001 13.9068 15.865 13.6364 15.5945L8 9.95817L2.36364 15.5945C2.09317 15.865 1.73902 16.0001 1.38455 16.0001C1.03009 16.0001 0.67593 15.865 0.405465 15.5945C-0.135155 15.0539 -0.135155 14.177 0.405465 13.6364L6.04183 8L0.405465 2.36364C-0.135155 1.82302 -0.135155 0.946086 0.405465 0.405465C0.946394 -0.135155 1.82271 -0.135155 2.36364 0.405465L8 6.04183L13.6364 0.405465C14.1773 -0.135155 15.0536 -0.135155 15.5945 0.405465C16.1352 0.946086 16.1352 1.82302 15.5945 2.36364L9.95817 8L15.5945 13.6364Z"
      fill={fill}
    />
  </svg>
);

export default CrossIcon;
