import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const SquaresIcon = ({
  name,
  style = {},
  fill = 'transparent',
  stroke = ICON_STROKE_DEFAULT,
  width = '32px',
  height = '32px',
  viewBox = '0 0 32 32',
  className = '',
}) => (
  <svg width={width} height={height} viewBox={viewBox} className={className} style={style} fill={'none'} xmlns="http://www.w3.org/2000/svg">
    <path
      d="M30.9995 1H18.9992V13.0003H30.9995V1Z"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M26.9989 5.00053H22.9988V9.00064H26.9989V5.00053Z"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M30.9987 18.9989H18.9984V30.9993H30.9987V18.9989Z"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M26.9977 23.0005H22.9976V27.0006H26.9977V23.0005Z"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13.0003 1H1V13.0003H13.0003V1Z"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M9.00026 5.00053H5.00015V9.00064H9.00026V5.00053Z"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13.0003 18.9989H1V30.9993H13.0003V18.9989Z"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M9.00026 23.0005H5.00015V27.0006H9.00026V23.0005Z"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default SquaresIcon;
