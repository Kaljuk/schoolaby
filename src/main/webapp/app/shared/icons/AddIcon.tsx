import React from 'react';
import { ICON_WHITE } from 'app/shared/util/color-utils';

const AddIcon = ({ name, style = {}, fill = ICON_WHITE, width = '8px', height = '8px', viewBox = '0 0 8 8', className = '' }) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill={fill}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path fillRule="evenodd" clipRule="evenodd" d="M5 0H3V3H0V5H3V8H5V5H8V3H5V0Z" fill={fill} />
  </svg>
);

export default AddIcon;
