import React from 'react';
import { GREY } from 'app/shared/util/color-utils';

const PencilTiltedIcon = ({
  style = {},
  fill = 'none',
  stroke = GREY,
  width = '16',
  height = '16',
  viewBox = '0 0 16 16',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill={fill}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M1 11.2667V15H4.73333L15 4.73333L11.2667 1L1 11.2667Z"
      stroke={stroke}
      strokeWidth="2"
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M9.40039 2.86719L13.1337 6.60052"
      stroke={stroke}
      strokeWidth="2"
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M1 11.2666L4.73333 14.9999"
      stroke={stroke}
      strokeWidth="2"
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default PencilTiltedIcon;
