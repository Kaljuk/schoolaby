import React from 'react';

const TanzanianFlagIcon = ({ ariaLabel = '', style = {}, width = '20', height = '20', viewBox = '0 0 20 20', className = '' }) => (
  <svg
    aria-label={ariaLabel}
    style={style}
    className={className}
    width={width}
    height={height}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <mask id="mask0_1_83" style={{ maskType: 'alpha' }} maskUnits="userSpaceOnUse" x="0" y="0" width="20" height="20">
      <circle cx="10" cy="10" r="10" fill="white" />
    </mask>
    <g mask="url(#mask0_1_83)">
      <path
        d="M17.0711 2.92893C18.9464 4.8043 20 7.34784 20 10C20 12.6522 18.9464 15.1957 17.0711 17.0711C15.1957 18.9464 12.6522 20 10 20C7.34784 20 4.8043 18.9464 2.92893 17.0711L10 10L17.0711 2.92893Z"
        fill="#00A3DD"
      />
      <path
        d="M2.92893 17.0711C1.05357 15.1957 8.58834e-07 12.6522 7.58717e-07 10C6.58601e-07 7.34784 1.05357 4.8043 2.92893 2.92893C4.8043 1.05357 7.34784 1.5359e-06 10 1.23555e-06C12.6522 9.35205e-07 15.1957 1.05357 17.0711 2.92893L10 10L2.92893 17.0711Z"
        fill="#1EB53A"
      />
      <mask id="mask1_1_83" style={{ maskType: 'alpha' }} maskUnits="userSpaceOnUse" x="0" y="0" width="20" height="20">
        <path
          d="M17.0711 2.92893C20.9763 6.83418 20.9763 13.1658 17.0711 17.0711C13.1658 20.9763 6.83418 20.9763 2.92893 17.0711C-0.976311 13.1658 -0.976311 6.83418 2.92893 2.92893C6.83418 -0.976309 13.1658 -0.976309 17.0711 2.92893Z"
          fill="white"
        />
      </mask>
      <g mask="url(#mask1_1_83)">
        <path d="M31.9203 -7.67767L-9.79899 34.0416L-14.0416 29.799L27.6777 -11.9203L31.9203 -7.67767Z" fill="#FCD116" />
        <path d="M31.2132 -8.38478L-10.5061 33.3345L-13.3345 30.5061L28.3848 -11.2132L31.2132 -8.38478Z" fill="black" />
      </g>
    </g>
  </svg>
);

export default TanzanianFlagIcon;
