import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const ChevronLeftIcon = ({
  name,
  style = {},
  fill = 'transparent',
  stroke = ICON_STROKE_DEFAULT,
  width = '12px',
  height = '20px',
  viewBox = '0 0 12 20',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill={fill}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M11.3007 17.6265C11.8497 18.1545 11.8669 19.0276 11.339 19.5769C11.0683 19.8583 10.7066 20 10.3445 20C10.0004 20 9.65625 19.8724 9.38866 19.6152L0.423105 10.9944C0.152759 10.7344 0 10.3751 0 9.99996C0 9.62513 0.152759 9.26582 0.423105 9.00582L9.38866 0.385094C9.93763 -0.142839 10.8111 -0.125943 11.339 0.42337C11.8669 0.972683 11.8497 1.84579 11.3007 2.37372L3.36932 9.99996L11.3007 17.6265Z"
      fill={fill}
    />
  </svg>
);

export default ChevronLeftIcon;
