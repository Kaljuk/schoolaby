import React from 'react';
import { ICON_WHITE } from 'app/shared/util/color-utils';

const CameraRotationIcon = ({
  style = {},
  fill = 'none',
  stroke = ICON_WHITE,
  width = '36px',
  height = '31px',
  viewBox = '0 0 94 82',
  className = '',
  strokeWidth = '5px',
}) => (
  <svg className={className} style={style} width={width} height={height} viewBox={viewBox} fill={fill} xmlns="http://www.w3.org/2000/svg">
    <path d="M47 14H2V80H38" stroke={stroke} strokeWidth={strokeWidth} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path
      d="M32 14L35 2H59L62 14H92V46"
      stroke={stroke}
      strokeWidth={strokeWidth}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M63.9206 34.4613C60.3314 30.212 56.1646 26.2514 46.9989 25.9998C41.9105 25.86 30.6613 28.4857 26.6267 41.8845C25.9194 44.2335 24.8513 53.2382 30.6623 60.1795"
      stroke={stroke}
      strokeWidth={strokeWidth}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M47 74C47 77.3 49.7 80 53 80H77C80.3 80 83 77.3 83 74V59L92 68"
      stroke={stroke}
      strokeWidth={strokeWidth}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path d="M83 59L74 68" stroke={stroke} strokeWidth={strokeWidth} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path
      d="M83 50C83 46.7 80.3 44 77 44H53C49.7 44 47 46.7 47 50V65L38 56"
      stroke={stroke}
      strokeWidth={strokeWidth}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path d="M47 65L56 56" stroke={stroke} strokeWidth={strokeWidth} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
  </svg>
);

export default CameraRotationIcon;
