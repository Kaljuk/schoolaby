import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const PeopleIcon = ({
  name,
  style = {},
  fill = 'transparent',
  stroke = ICON_STROKE_DEFAULT,
  width = '32px',
  height = '26px',
  viewBox = '0 0 32 26',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M16 15C18.7614 15 21 12.7614 21 10C21 7.23858 18.7614 5 16 5C13.2386 5 11 7.23858 11 10C11 12.7614 13.2386 15 16 15Z"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M23 25C23 21.1 19.9 18 16 18C12.1 18 9 21.1 9 25H23Z"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M24 11C26.8 11 29 8.8 29 6C29 3.2 26.8 1 24 1C22.4 1 20.9 1.8 20 3"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M25 21H31C31 17.1 27.9 14 24 14"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M12 3C11.1 1.8 9.6 1 8 1C5.2 1 3 3.2 3 6C3 8.8 5.2 11 8 11"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path d="M8 14C4.1 14 1 17.1 1 21H7" fill={fill} stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
  </svg>
);

export default PeopleIcon;
