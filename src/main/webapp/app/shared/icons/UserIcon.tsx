import React from 'react';

const UserIcon = ({
  name,
  style = {},
  fill = 'transparent',
  stroke = '#ffffff',
  width = '14px',
  height = '17px',
  viewBox = '0 0 14 17',
  className = '',
  strokeWidth = '2px',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill={fill}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M7 8C8.933 8 10.5 6.433 10.5 4.5C10.5 2.567 8.933 1 7 1C5.067 1 3.5 2.567 3.5 4.5C3.5 6.433 5.067 8 7 8Z"
      stroke={stroke}
      strokeWidth={strokeWidth}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13 16C13 12.7 10.3 10 7 10C3.7 10 1 12.7 1 16H13Z"
      stroke={stroke}
      strokeWidth={strokeWidth}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default UserIcon;
