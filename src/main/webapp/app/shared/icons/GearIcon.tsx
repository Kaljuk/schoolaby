import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const GearIcon = ({
  name,
  style = {},
  fill = 'transparent',
  stroke = ICON_STROKE_DEFAULT,
  width = '32px',
  height = '32px',
  viewBox = '0 0 32 32',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M28 13H27.6C27.3 11.9 26.9 10.9 26.3 9.89999L26.6 9.60001C27.8 8.40001 27.8 6.49999 26.6 5.39999C25.4 4.19999 23.5 4.19999 22.4 5.39999L22.1 5.70001C21.1 5.10001 20.1 4.69999 19 4.39999V4C19 2.3 17.7 1 16 1C14.3 1 13 2.3 13 4V4.39999C11.9 4.69999 10.9 5.10001 9.89999 5.70001L9.60001 5.39999C8.40001 4.19999 6.49999 4.19999 5.39999 5.39999C4.19999 6.59999 4.19999 8.50001 5.39999 9.60001L5.70001 9.89999C5.10001 10.9 4.69999 11.9 4.39999 13H4C2.3 13 1 14.3 1 16C1 17.7 2.3 19 4 19H4.39999C4.69999 20.1 5.10001 21.1 5.70001 22.1L5.39999 22.4C4.19999 23.6 4.19999 25.5 5.39999 26.6C6.59999 27.8 8.50001 27.8 9.60001 26.6L9.89999 26.3C10.9 26.9 11.9 27.3 13 27.6V28C13 29.7 14.3 31 16 31C17.7 31 19 29.7 19 28V27.6C20.1 27.3 21.1 26.9 22.1 26.3L22.4 26.6C23.6 27.8 25.5 27.8 26.6 26.6C27.8 25.4 27.8 23.5 26.6 22.4L26.3 22.1C26.9 21.1 27.3 20.1 27.6 19H28C29.7 19 31 17.7 31 16C31 14.3 29.7 13 28 13Z"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M16 24C20.4183 24 24 20.4183 24 16C24 11.5817 20.4183 8 16 8C11.5817 8 8 11.5817 8 16C8 20.4183 11.5817 24 16 24Z"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default GearIcon;
