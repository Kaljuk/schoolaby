import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const HomeNewIcon = ({
  name,
  style = {},
  fill = 'transparent',
  stroke = ICON_STROKE_DEFAULT,
  width = '12px',
  height = '13px',
  viewBox = '0 0 12 13',
  className = '',
  ariaLabel = '',
}) => (
  <svg
    aria-label={ariaLabel}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g clipPath="url(#clip0_631_5599)">
      <path
        d="M11.7985 5.98459L6.41446 1.07871C6.17814 0.863343 5.82177 0.863366 5.58555 1.07869L0.201516 5.98461C0.0122119 6.15711 -0.0503192 6.42289 0.042165 6.6617C0.134673 6.9005 0.359907 7.05479 0.616008 7.05479H1.47593V11.9701C1.47593 12.165 1.63394 12.323 1.82883 12.323H4.77993C4.97481 12.323 5.13283 12.165 5.13283 11.9701V8.98569H6.86725V11.9701C6.86725 12.165 7.02526 12.323 7.22014 12.323H10.1711C10.366 12.323 10.524 12.165 10.524 11.9701V7.05479H11.3841C11.6402 7.05479 11.8654 6.90048 11.9579 6.6617C12.0503 6.42287 11.9878 6.15711 11.7985 5.98459Z"
        fill={fill}
      />
      <path d="M10.4325 1.6216H8.0625L10.7854 4.09744V1.97447C10.7854 1.77959 10.6274 1.6216 10.4325 1.6216Z" fill={fill} />
    </g>
    <defs>
      <clipPath id="clip0_631_5599">
        <rect width="12" height="12" fill="white" transform="translate(0 0.620117)" />
      </clipPath>
    </defs>
  </svg>
);

export default HomeNewIcon;
