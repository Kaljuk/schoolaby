import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const CheckmarkIcon = ({
  name,
  style = {},
  fill = 'transparent',
  stroke = ICON_STROKE_DEFAULT,
  width = '18px',
  height = '14px',
  viewBox = '0 0 18 14',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M17.7777 2.77772L6.96723 13.7764C6.67252 14.0762 6.1966 14.0743 5.90428 13.772L0.218007 7.89276C-0.0743208 7.59049 -0.0724416 7.10237 0.222329 6.80248L1.64538 5.3547C1.94009 5.05488 2.41601 5.05681 2.70834 5.35913L6.45354 9.23152L15.3074 0.223604C15.6021 -0.0762187 16.0781 -0.0742906 16.3704 0.227973L17.782 1.68745C18.0743 1.98978 18.0724 2.4779 17.7777 2.77772Z"
      fill={fill}
      stroke={stroke}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default CheckmarkIcon;
