import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const ExportIcon = ({
  name,
  style = {},
  fill = 'none',
  stroke = ICON_STROKE_DEFAULT,
  width = '32px',
  height = '20px',
  viewBox = '0 0 32 20',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill={fill}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M21 19H3C1.9 19 1 18.1 1 17V3C1 1.9 1.9 1 3 1H21C22.1 1 23 1.9 23 3V17C23 18.1 22.1 19 21 19Z"
      stroke={stroke}
      strokeWidth="3"
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path d="M31 18L23 14V6L31 2V18Z" stroke={stroke} strokeWidth="3" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
  </svg>
);

export default ExportIcon;
