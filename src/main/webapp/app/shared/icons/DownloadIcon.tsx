import React from 'react';
import { ICON_LIGHT_GREY, ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const DownloadIcon = ({
  name,
  style = {},
  fill = ICON_LIGHT_GREY,
  stroke = ICON_STROKE_DEFAULT,
  width = '21px',
  height = '22px',
  viewBox = '0 0 21 22',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill={fill}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M9.62836 15.6382C9.85789 15.8726 10.1724 16.0047 10.5006 16.0047C10.829 16.0047 11.1433 15.8726 11.3731 15.6382L16.2438 10.6651C16.7156 10.1835 16.7076 9.41044 16.2258 8.93843C15.744 8.46691 14.9709 8.47472 14.4991 8.9565L11.7215 11.7925V1.22093C11.7215 0.546733 11.1748 0 10.5006 0C9.82639 0 9.27966 0.546733 9.27966 1.22093V11.7922L6.50253 8.9565C6.03076 8.47472 5.25816 8.46691 4.77589 8.93843C4.29436 9.41044 4.2863 10.1833 4.75807 10.6651L9.62836 15.6382ZM21 14.1069V20.7558C21 21.43 20.4533 21.9767 19.7791 21.9767H1.22093C0.546733 21.9767 0 21.43 0 20.7558V14.1069C0 13.4327 0.546733 12.8859 1.22093 12.8859C1.89513 12.8859 2.44186 13.4327 2.44186 14.1069V19.5349H18.5581V14.1069C18.5581 13.4327 19.1049 12.8859 19.7791 12.8859C20.4533 12.8859 21 13.4327 21 14.1069Z"
      fill={fill}
    />
  </svg>
);

export default DownloadIcon;
