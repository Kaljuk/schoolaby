import React from 'react';
import { ICON_WHITE } from 'app/shared/util/color-utils';

const StopwatchIcon = ({ width = '20px', height = '19px', viewBox = '0 0 20 19', fill = ICON_WHITE, className = '' }) => (
  <svg className={className} width={width} height={height} viewBox={viewBox} fill="none" xmlns="http://www.w3.org/2000/svg">
    <ellipse opacity="0.3" cx="10.0429" cy="9.16743" rx="9.4858" ry="9" fill="black" />
    <g clipPath="url(#clip0_159_1735)">
      <path
        d="M11.7827 6.06537L11.4995 6.5303L12.4808 7.06767L12.764 6.60273C12.842 6.47367 12.7955 6.30992 12.66 6.23536L12.17 5.96666C12.0344 5.89218 11.8613 5.93624 11.7827 6.06537Z"
        fill={fill}
      />
      <path
        d="M10.0428 6.37737C10.2219 6.37737 10.3969 6.394 10.5698 6.41743V5.98256L10.9321 5.97993V5.41743C10.9321 5.27925 10.8142 5.16743 10.6686 5.16743H9.41975C9.2741 5.16743 9.15625 5.27925 9.15625 5.41743V5.97993L9.51579 5.98256V6.41743C9.68871 6.394 9.86367 6.37737 10.0428 6.37737Z"
        fill={fill}
      />
      <path
        d="M10.0431 6.66743C8.15129 6.66743 6.61768 8.12249 6.61768 9.91743C6.61768 11.7124 8.15129 13.1674 10.0431 13.1674C11.9349 13.1674 13.4685 11.7124 13.4685 9.91743C13.4685 8.12249 11.9349 6.66743 10.0431 6.66743ZM11.6438 11.4449L9.64785 10.1339V8.28299H10.2239V9.84755L11.9717 10.9956L11.6438 11.4449Z"
        fill={fill}
      />
    </g>
    <defs>
      <clipPath id="clip0_159_1735">
        <rect width="8.43182" height="8" fill="white" transform="translate(5.82715 5.16743)" />
      </clipPath>
    </defs>
  </svg>
);

export default StopwatchIcon;
