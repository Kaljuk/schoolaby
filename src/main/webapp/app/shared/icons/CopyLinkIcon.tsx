import React from 'react';
import { PRIMARY } from '../util/color-utils';

const CopyLinkIcon = ({ name, style = {}, stroke = PRIMARY, width = '27', height = '21', viewBox = '0 0 27 21', className = '' }) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M11.5993 15.6598C10.9569 16.2412 9.99331 16.2412 9.35093 15.6598L1.48178 7.85152C0.839405 7.27006 0.839405 6.19019 1.48178 5.60873L4.85428 2.28607C5.49666 1.70461 6.46023 1.70461 7.10261 2.28607L14.9718 10.0943C15.6141 10.6758 15.6141 11.7556 14.9718 12.3371"
      stroke={stroke}
      strokeWidth="2"
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M14.49 6.19019C15.1324 5.60872 16.096 5.60872 16.7384 6.19019L24.6075 13.9984C25.2499 14.5799 25.2499 15.6597 24.6075 16.2412L21.235 19.5639C20.5926 20.1453 19.6291 20.1453 18.9867 19.5639L11.1175 11.7556C10.4751 11.1742 10.4751 10.0943 11.1175 9.51284"
      stroke={stroke}
      strokeWidth="2"
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

export default CopyLinkIcon;
