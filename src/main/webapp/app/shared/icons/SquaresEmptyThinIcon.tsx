import React from 'react';
import { ICON_STROKE_DEFAULT } from 'app/shared/util/color-utils';

const SquaresEmptyThinIcon = ({
  name,
  style = {},
  fill = 'transparent',
  stroke = ICON_STROKE_DEFAULT,
  width = '32px',
  height = '32px',
  viewBox = '0 0 32 32',
  className = '',
}) => (
  <svg
    aria-label={name}
    className={className}
    width={width}
    height={height}
    style={style}
    viewBox={viewBox}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M13 1H1V13H13V1Z" fill={fill} stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M13 19H1V31H13V19Z" fill={fill} stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M31 19H19V31H31V19Z" fill={fill} stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M31 1H19V13H31V1Z" fill={fill} stroke={stroke} strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
  </svg>
);

export default SquaresEmptyThinIcon;
