import './file-dropzone.scss';
import React, { FC, useEffect, useMemo, useRef } from 'react';
import { CameraProvider, useCameraMountTargetState, useUppyState } from 'app/shared/contexts/camera-context';
import { Button, Col, Row } from 'reactstrap';
import Icon from 'app/shared/icons';
import { ICON_WHITE } from 'app/shared/util/color-utils';
import DropTarget from '@uppy/drop-target';
import Uppy from '@uppy/core';
import StatusBar from '@uppy/status-bar';
import Informer from '@uppy/informer';
import CameraWrapper from 'app/shared/layout/webcams/camera-wrapper';
import { translate } from 'react-jhipster';
import { CAMERA, FILE } from 'app/shared/layout/save-material-modal-new/save-material-modal-new';

type IProps = {
  uppy: Uppy.Uppy<Uppy.StrictTypes>;
  fileUploadType?: typeof FILE | typeof CAMERA;
  disableCamera: boolean;
};

const FileDropzone: FC<IProps> = ({ uppy, fileUploadType, disableCamera }) => {
  const dropzoneRef = useRef<HTMLSpanElement>();
  const statusBarRef = useRef<HTMLSpanElement>();
  const fileInputRef = useRef<HTMLInputElement>();

  useEffect(() => {
    uppy.use(DropTarget, {
      target: dropzoneRef?.current,
    });
    uppy.use(StatusBar, {
      hideUploadButton: true,
      // Hide after finish as this does not refresh between switching students
      hideAfterFinish: true,
      target: statusBarRef?.current,
    });
    uppy.use(Informer, {
      target: dropzoneRef?.current,
    });
    uppy.on('file-removed', () => {
      fileInputRef?.current?.value && (fileInputRef.current.value = null);
    });
  }, []);

  const UploadButton = () => {
    const uploadFiles = event => {
      const files: File[] = Array.from(event.target.files);

      files.forEach((file: File) => {
        try {
          uppy.addFile({
            source: 'file input',
            name: file.name,
            type: file.type,
            data: file,
          });
        } catch (err) {
          if (err.isRestriction) {
            // handle restrictions
            console.error('Restriction error:', err);
          } else {
            // handle other errors
            console.error(err);
          }
        }
      });
    };

    const buttonTitle = !fileUploadType ? translate('global.upload.browse') : translate('global.upload.upload').toLowerCase();

    return (
      <>
        <input
          className={'d-none'}
          type={'file'}
          id={'file'}
          ref={fileInputRef}
          onChange={uploadFiles}
          aria-label={translate('global.upload.fileUploader')}
        />
        <Button id={'upload-file'} className={'py-2'} color={'primary'} onClick={() => fileInputRef?.current?.click()}>
          {buttonTitle}
          <Icon className={'ml-2'} name={'download'} fill={ICON_WHITE} stroke={'transparent'} height={'20px'} />
        </Button>
      </>
    );
  };

  const CameraButton = () => {
    const { setCameraMountTarget } = useCameraMountTargetState();
    const { setUppy } = useUppyState();

    useEffect(() => setUppy(uppy), [uppy]);

    return (
      <>
        <Button
          className={'ml-4 py-2'}
          color={'primary'}
          onClick={() => {
            setCameraMountTarget('MODAL');
          }}
          disabled={disableCamera}
        >
          {!fileUploadType ? translate('global.upload.camera') : translate('global.upload.useCamera')}
          <Icon className={'ml-2'} name={!fileUploadType ? 'camera' : 'cameraBlack'} strokeWidth={'8px'} height={'20px'} />
        </Button>
      </>
    );
  };

  return useMemo(
    () => (
      <span ref={dropzoneRef}>
        <Row noGutters className={`d-flex file-dropzone align-items-center rounded${' ' + fileUploadType}`}>
          <Col xs={12} className={'file-dropzone-content justify-content-center'}>
            <CameraProvider>
              <CameraWrapper
                rowClassName={'justify-content-center'}
                className={'mt-2'}
                showDescription={!fileUploadType || fileUploadType === FILE}
                icon={fileUploadType && <Icon name={'files'} className={'mb-3 w-100'} />}
                description={!fileUploadType ? translate('global.upload.dropPasteImport') : translate('global.upload.dropPasteMaterials')}
              >
                {(!fileUploadType || fileUploadType === FILE) && <UploadButton />}
                {(!fileUploadType || fileUploadType === CAMERA) && <CameraButton />}
              </CameraWrapper>
            </CameraProvider>
          </Col>
          <span className={'col-12 align-self-end'} ref={statusBarRef} />
        </Row>
      </span>
    ),
    [dropzoneRef, statusBarRef, disableCamera]
  );
};

export default FileDropzone;
