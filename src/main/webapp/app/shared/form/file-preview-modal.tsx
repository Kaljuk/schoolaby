import './file-preview-modal.scss';
import React, { useEffect, useRef, useState } from 'react';
import Slider from 'react-slick';
import { Storage, translate } from 'react-jhipster';
import Icon, { getFileExtensionIcon } from 'app/shared/icons';
import { usePreviewFileState } from 'app/shared/contexts/uploaded-file-context';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import { createDownloadLink, isPreviewableFile, isPreviewableImg } from 'app/shared/util/file-utils';
import { downloadUploadedFile, getFileUrl } from 'app/shared/services/uploaded-file-api';
import CloseButton from 'app/shared/layout/close-button/close-button';
import { Button, Col, ModalBody, ModalFooter, Row } from 'reactstrap';
import { ICON_GREY } from 'app/shared/util/color-utils';
import { EmbeddedFile } from 'app/shared/form/embedded-file';
import Painterro from 'painterro';
import { useFeedbackFilesState } from 'app/shared/contexts/submission-grading-context';
import Uppy, { UppyFile } from '@uppy/core';
import { getAuthToken } from 'app/shared/util/auth-utils';
import XHRUpload from '@uppy/xhr-upload';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import Truncate from 'react-truncate';
import { toast } from 'react-toastify';
import { useTeacherCreatingFeedbackState } from 'app/shared/contexts/root-context';
import InfoModal from 'app/shared/layout/info-modal/info-modal';
import { isSafari } from 'react-device-detect';
import { WEBM_INFO_MODAL_SEEN } from 'app/config/constants';
import CustomModal from 'app/shared/layout/custom-modal/custom-modal';

interface IProps extends StateProps {
  files: IUploadedFile[];
  canEdit: boolean;
}

const FilePreviewModal = ({ files, canEdit, locale }: IProps) => {
  const { previewFile, setPreviewFile } = usePreviewFileState();
  const { teacherCreatingFeedback } = useTeacherCreatingFeedbackState();
  const { setFeedbackFiles } = teacherCreatingFeedback ? useFeedbackFilesState() : { setFeedbackFiles: undefined };
  const [mainSliderMounted, setMainSliderMounted] = useState<boolean>(false);
  const [editorOpen, setEditorOpen] = useState<boolean>(false);
  const [showInfoModal, setShowInfoModal] = useState<boolean>(false);
  const mainSlider = useRef<Slider>(null);
  const [previewableFiles, setPreviewableFiles] = useState<IUploadedFile[]>([]);
  const [slideIndex, setSlideIndex] = useState<number>(null);
  let painterro;
  let uppy;

  useEffect(() => {
    const containsWebm = previewableFiles?.some(file => file.type === 'webm');
    const infoSeen = Storage.local.get(WEBM_INFO_MODAL_SEEN);
    setShowInfoModal(isSafari && containsWebm && !infoSeen);
  }, [previewableFiles, !!previewFile?.id]);

  useEffect(() => {
    if (editorOpen) {
      uppy = Uppy()
        .use(XHRUpload, {
          fieldName: 'file',
          endpoint: `/api/files`,
          headers: { Authorization: `Bearer ${getAuthToken()}` },
          limit: 1,
        })
        .on('upload-success', (file: UppyFile, response: any) => {
          toast.success(translate('schoolabyApp.assignment.detail.feedbackUploaded'));
          return teacherCreatingFeedback && setFeedbackFiles(previousState => [...previousState, response.body]);
        });

      const configuration = {
        id: 'image-editor',
        defaultTool: 'brush',
        language: locale,
        hiddenTools: [
          'crop',
          'settings',
          'pixelize',
          'resize',
          'eraser',
          'select',
          'ellipse',
          'open',
          'zoomin',
          'zoomout',
          'save',
          'close',
        ],
        saveHandler(image, doneCallback) {
          uppy.addFile({
            name: previewFile?.originalName,
            type: image.getOriginalMimeType(),
            data: image.asBlob(),
          });
          uppy.upload();

          doneCallback(true);
          setEditorOpen(false);
        },
        onClose() {
          setEditorOpen(false);
        },
      };

      if (locale === 'et') {
        // @ts-ignore
        configuration.translation = {
          name: 'et',
          strings: {
            close: 'Sulge',
            fillColor: 'Värv',
            fillColorFull: 'Värv',
            lineColor: 'Värv',
            lineColorFull: 'Värv',
            lineWidth: 'Laius',
            lineWidthFull: 'Laius',
            arrowLength: 'Pikkus',
            arrowLengthFull: 'Pikkus',
            shadowOn: 'Vari',
            shadowOnFull: 'Vari',
            textColor: 'Värv',
            textColorFull: 'Teksti värv',
            fontSize: 'Fondi suurus',
            fontSizeFull: 'Fondi suurus',
            fontStrokeSize: 'Laius',
            fontStrokeSizeFull: 'Laius',
            fontName: 'Fondi nimi',
            fontNameFull: 'Fondi nimi',
            fontStrokeAndShadow: 'Vari',
            fontStrokeAndShadowFull: 'Vari',
            tools: {
              rect: 'Ristkülik',
              line: 'Joon',
              arrow: 'Nool',
              rotate: 'Pööra',
              text: 'Tekst',
              brush: 'Pliiats',
              undo: 'Võta tagasi',
              redo: 'Mine edasi',
            },
          },
        };
      }

      painterro = Painterro(configuration).show(getFileUrl(previewFile?.uniqueName));
    }
  }, [editorOpen]);

  useEffect(() => {
    setPreviewableFiles(files?.filter(file => isPreviewableFile(file)));
  }, [files]);

  useEffect(() => {
    if (previewFile && previewableFiles.length > 0) {
      const index = previewableFiles?.findIndex(f => f.id === previewFile.id);
      setSlideIndex(index);
    }
  }, [previewFile]);

  const closeModal = () => {
    setEditorOpen(false);
    setPreviewFile(null);
  };

  const downloadFile = () =>
    downloadUploadedFile(previewFile?.uniqueName).then(response => {
      const link = createDownloadLink(response);
      link.download = previewFile?.originalName;
      link.click();
    });

  const PrevArrow = props => {
    const { className, style, onClick } = props;
    return (
      <span className={className} style={style} onClick={onClick}>
        <Icon name={'chevronLeft'} fill={ICON_GREY} />
      </span>
    );
  };
  const NextArrow = props => {
    const { className, style, onClick } = props;
    return (
      <span className={className} style={style} onClick={onClick}>
        <Icon name={'chevronRight'} fill={ICON_GREY} />
      </span>
    );
  };

  const sliderSettings = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    swipe: false,
  };

  const sliderNavSettings = {
    slidesToShow: previewableFiles?.length > 6 ? 6 : previewableFiles?.length,
    slidesToScroll: 1,
    infinite: true,
    focusOnSelect: true,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    responsive: [
      {
        breakpoint: 700,
        settings: {
          slidesToShow: previewableFiles?.length > 4 ? 4 : previewableFiles?.length,
          initialSlide: slideIndex,
          infinite: true,
          arrows: false,
        },
      },
      {
        breakpoint: 450,
        settings: {
          slidesToShow: previewableFiles?.length > 3 ? 3 : previewableFiles?.length,
          initialSlide: slideIndex,
          infinite: true,
          arrows: false,
        },
      },
    ],
  };

  const setMainSlider = slider => {
    if (slider) {
      mainSlider.current = slider;
      slider.slickGoTo(slideIndex);
      setMainSliderMounted(true);
    }
  };

  const renderSlide = (file: IUploadedFile, showFileName: boolean, embed: boolean) => (
    <div key={file.id} className={'slide-container'}>
      <div className={'slide-preview'}>
        <EmbeddedFile file={file} embed={embed} />
      </div>
      {showFileName && (
        <div className={'slide-name'}>
          <Truncate lines={2} className={'file-name'}>
            {file.originalName}
          </Truncate>
        </div>
      )}
    </div>
  );

  const Slides = () => (
    <div className="p-2">
      <Slider className={'main-slider'} {...sliderSettings} ref={setMainSlider}>
        {previewableFiles?.map(file => renderSlide(file, false, true))}
      </Slider>
      {mainSliderMounted && (
        <Slider
          className={'nav-slider'}
          {...sliderNavSettings}
          initialSlide={slideIndex}
          beforeChange={(oldIndex, newIndex) => {
            setPreviewFile(previewableFiles[newIndex]);
            mainSlider?.current?.slickGoTo(newIndex);
          }}
        >
          {previewableFiles?.map(file => renderSlide(file, true, false))}
        </Slider>
      )}
    </div>
  );

  return (
    <CustomModal
      isOpen={!!previewFile?.id}
      className="modal-lg base-modal file-preview-modal"
      modalClassName={'h-80'}
      contentClassName={'w-100 px-3 px-sm-4'}
    >
      <ModalBody className="p-0">
        <Row>
          <Col xs={'9'} sm={'11'}>
            <h2 className={'align-items-center d-flex'}>
              {getFileExtensionIcon(previewFile?.extension)}
              <Truncate className={'ml-2 d-inline-block w-100'} lines={1}>
                {previewFile?.originalName}
              </Truncate>
            </h2>
          </Col>
          <Col className={'align-items-start d-flex'} xs={'3'} sm={'1'}>
            <CloseButton onClick={closeModal} />
          </Col>
        </Row>
        <div className={'d-flex mt-2'}>
          <Col className="slides p-0">
            {editorOpen && <div id="image-editor" />}
            {!editorOpen && <Slides />}
          </Col>
        </div>
      </ModalBody>
      <ModalFooter className="p-0 pt-2">
        <Row noGutters className={'button-container justify-content-end w-100 text-center'}>
          {editorOpen ? (
            <>
              <Col sm={6} md={3} lg={2}>
                <Button
                  variant="outline-secondary"
                  className="mr-sm-1 w-100"
                  color={'secondary'}
                  onClick={() => setEditorOpen(false)}
                  size="lg"
                >
                  {translate('global.close')}
                </Button>
              </Col>
              <Col sm={6} md={3} lg={2}>
                <Button color="primary" onClick={() => painterro?.save()} className="ml-sm-1 mt-2 mt-sm-0 w-100" size="lg">
                  {translate('entity.action.save')}
                </Button>
              </Col>
            </>
          ) : (
            <>
              {canEdit && previewFile && isPreviewableImg(previewFile) && (
                <Col sm={6} md={3} lg={2}>
                  <Button
                    variant="outline-secondary"
                    className="mr-sm-1 w-100"
                    color={'secondary'}
                    onClick={() => setEditorOpen(true)}
                    size="lg"
                  >
                    {translate('entity.action.edit')}
                  </Button>
                </Col>
              )}
              <Col sm={6} md={3} lg={2}>
                <Button color="primary" onClick={downloadFile} className="ml-sm-1 mt-2 mt-sm-0 w-100" size="lg">
                  {translate('global.download')}
                </Button>
              </Col>
            </>
          )}
        </Row>
      </ModalFooter>
      <InfoModal
        seenKey={WEBM_INFO_MODAL_SEEN}
        showModal={showInfoModal}
        setShowModal={setShowInfoModal}
        bodyText={translate('schoolabyApp.uploadedFile.webmInfo')}
      />
    </CustomModal>
  );
};

const mapStateToProps = ({ locale }: IRootState) => ({
  locale: locale?.currentLocale,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(FilePreviewModal);
