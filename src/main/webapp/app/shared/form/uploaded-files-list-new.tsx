import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import React, { FC } from 'react';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import FilePreviewModal from 'app/shared/form/file-preview-modal';
import { MANUAL_MATERIAL } from 'app/shared/model/material.model';
import WideCardNew from 'app/shared/layout/material-card-new/material-wide-card-new/wide-card-new';
import { usePreviewFileState } from 'app/shared/contexts/uploaded-file-context';

interface IStudentUploadedFiles {
  files: IUploadedFile[];
  canEdit?: boolean;
  onFileDelete?: (file: IUploadedFile) => void;
}

const UploadedFilesListNew: FC<IStudentUploadedFiles> = ({ files, canEdit = true, onFileDelete }) => {
  const { setPreviewFile } = usePreviewFileState();

  return (
    <div>
      {files?.length &&
        files.map((file, i) => (
          <WideCardNew
            onClick={() => setPreviewFile(file)}
            materialAction={() => onFileDelete(file)}
            deletable
            key={i}
            title={file.originalName}
            type={MANUAL_MATERIAL}
            showButton
          />
        ))}
      <FilePreviewModal files={files} canEdit={canEdit} />
    </div>
  );
};

export default UploadedFilesListNew;
