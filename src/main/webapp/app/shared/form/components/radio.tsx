import React, { FC } from 'react';
import { Field } from 'react-final-form';

interface IProps {
  name: string;
  label: string;
  onChange?: (input) => void;
  onClick?: (input) => void;
  className?: string;
  value?: string | number;
  checked?: boolean;
}

const Radio: FC<IProps> = ({ label, name, className, value, onChange, checked, onClick }) => {
  const handleChange = (input, event) => {
    onChange ? onChange(input) : input.onChange(event);
  };

  const handleClick = input => {
    if (onClick) {
      onClick(input);
    }
  };

  return (
    <Field name={name} className={className} type="radio" value={value}>
      {({ input }) => {
        return (
          <div className={`radio-button ${className ? className : ''}`}>
            <label>
              <input
                name={input.name}
                type="radio"
                value={input.value}
                checked={checked || input.checked}
                onChange={event => handleChange(input, event)}
                onClick={() => handleClick(input)}
              />
              <span className="radio-control" />
              <span className="pl-2">{label}</span>
            </label>
          </div>
        );
      }}
    </Field>
  );
};

export default Radio;
