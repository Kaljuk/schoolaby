import React, { FC } from 'react';
import { FieldRenderProps } from 'react-final-form';
import { MarkdownEditor } from 'app/assignment/assignment-detail/submission/shared/markdown-editor';
import { FormFeedback, FormGroup } from 'reactstrap';

const MarkdownEditorInput: FC<FieldRenderProps<any, any>> = ({ input, meta, containerClassName, autoFocus, label }) => {
  const errorMessage = meta?.touched && meta?.error;

  return (
    <FormGroup className={containerClassName}>
      <label>{label}</label>
      <MarkdownEditor
        options={{ autofocus: autoFocus }}
        value={input?.value}
        onEditorStateChange={(value: string) => input?.onChange(value)}
      />
      {errorMessage && <FormFeedback>{errorMessage}</FormFeedback>}
    </FormGroup>
  );
};

export default MarkdownEditorInput;
