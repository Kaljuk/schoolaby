import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { FC } from 'react';
import { FieldRenderProps } from 'react-final-form';
import { CustomInput, FormFeedback, FormGroup, UncontrolledTooltip } from 'reactstrap';

interface IProps extends FieldRenderProps<any, any> {
  label?: string;
  tooltip?: string;
}

const Switch: FC<IProps> = ({ input, meta, tooltip, ...props }) => {
  const id = `field-${input.name}`;
  const errorMessage = meta.touched && meta.error;

  return (
    <FormGroup className="text-left">
      <CustomInput id={`field-${input.name}`} {...input} {...props} type="switch" inline={!!tooltip} invalid={!!errorMessage} />
      {tooltip && (
        <span id={`${id}-tooltip`} className="ml-2">
          <FontAwesomeIcon icon={'eye'} />
          <UncontrolledTooltip target={`${id}-tooltip`}>{tooltip}</UncontrolledTooltip>
        </span>
      )}
      {errorMessage && <FormFeedback>{errorMessage}</FormFeedback>}
    </FormGroup>
  );
};

export default Switch;
