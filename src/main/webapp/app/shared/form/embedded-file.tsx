import React, { FC } from 'react';
import Iframe from 'app/shared/layout/iframe/iframe';
import { getPdfEmbedUrl } from 'app/shared/services/embed-api';
import Icon from 'app/shared/icons';
import { ICON_GREY } from 'app/shared/util/color-utils';
import { getFileUrl } from 'app/shared/services/uploaded-file-api';
import { getAbsolutePath } from 'app/shared/util/navigation-utils';
import { isPreviewableAudio, isPreviewableImg, isPreviewableVideo } from 'app/shared/util/file-utils';

interface IEmbeddedFile {
  file: any;
  embed?: boolean;
  embedPdf?: boolean;
}

export const EmbeddedFile: FC<IEmbeddedFile> = ({ file, embed = false, embedPdf = true }) => {
  const fileUrl = getFileUrl(file?.uniqueName);
  const absoluteUrl = getAbsolutePath(fileUrl);
  const isPdf = file.extension?.toLowerCase() === 'pdf';

  const getPdf = () =>
    embed ? (
      <Iframe src={getPdfEmbedUrl(absoluteUrl, false)} className="embed-responsive-item" role={'pdf-content'} allowFullScreen />
    ) : (
      <Icon name={'pdfFileExtension'} fill={ICON_GREY} height={'50px'} width={'50px'} />
    );

  const getVideo = () =>
    embed ? (
      // @ts-ignore
      <video
        controls
        playsInline
        disablePictureInPicture
        controlsList="disablepictureinpicture noremoteplayback nodownload"
        role={'video-content'}
      >
        <source src={fileUrl} />
      </video>
    ) : (
      <Icon name={'video'} fill={ICON_GREY} height={'50px'} width={'50px'} />
    );

  function getAudio() {
    return embed ? (
      <audio controls src={fileUrl} role={'audio-content'} />
    ) : (
      <Icon name={'defaultFileExtension'} fill={ICON_GREY} height={'50px'} width={'50px'} />
    );
  }

  return (
    <>
      {isPdf && embedPdf && getPdf()}
      {isPreviewableImg(file) && <img src={fileUrl} alt={file.originalName} role={'img-content'} />}
      {isPreviewableVideo(file) && getVideo()}
      {isPreviewableAudio(file) && getAudio()}
    </>
  );
};
