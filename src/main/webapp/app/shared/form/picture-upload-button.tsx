import './picture-upload-button.scss';
import Uppy, { UppyFile } from '@uppy/core';
import '@uppy/core/dist/style.css';
import '@uppy/file-input/dist/style.css';
import { FileInput } from '@uppy/react';
import XHRUpload from '@uppy/xhr-upload';
import React, { FC, memo, useEffect, useRef } from 'react';
import { translate } from 'react-jhipster';
import { getAuthToken } from '../util/auth-utils';
import { connect } from 'react-redux';
import { IRootState } from 'app/shared/reducers';
import Compressor from 'app/shared/form/compressor';
import Informer from '@uppy/informer';
import { getFileUrl } from 'app/shared/services/uploaded-file-api';

interface IProps extends StateProps {
  onUpload: (file: string) => void;
  existingImage: string;
  uploadDisabled: boolean;
  resetOn?: any;
}

const PictureUploadButton: FC<IProps> = ({ locale, uploadDisabled, existingImage, onUpload, resetOn }) => {
  const BUTTON_ID = 'FileInput';

  const uppyRef = useRef<Uppy.Uppy<Uppy.StrictTypes>>(
    Uppy({
      autoProceed: true,
      debug: process.env.NODE_ENV === 'development',
      restrictions: {
        maxFileSize: null,
        maxNumberOfFiles: 1,
        minNumberOfFiles: 0,
        allowedFileTypes: null,
      },
    })
      .use(XHRUpload, {
        fieldName: 'file',
        endpoint: `/api/files`,
        headers: { Authorization: `Bearer ${getAuthToken()}` },
        limit: 1,
      })
      // @ts-ignore
      .use(Compressor, {
        quality: 0.9,
        maxWidth: 2500,
        maxHeight: 2500,
        convertSize: 3000000,
      })
      .use(Informer, {})
      .on('upload-success', (file: UppyFile, response: any) => {
        onUpload(getFileUrl(response.body.uniqueName));
      })
  );

  useEffect(() => {
    uppyRef.current.reset();
  }, [resetOn]);

  useEffect(() => {
    return () => {
      uppyRef.current.close();
    };
  }, []);

  useEffect(() => {
    uppyRef.current?.getPlugin(BUTTON_ID)?.setOptions({
      locale: {
        strings: {
          chooseFiles: translate('global.upload.chooseImage'),
        },
      },
    });

    uppyRef.current?.setOptions({
      locale: {
        strings: {
          youCanOnlyUploadX: {
            '0': translate('global.upload.youCanOnlyUpload1'),
            '1': translate('global.upload.youCanOnlyUploadX'),
          },
        },
      },
    });
  }, [locale]);

  return (
    <div className="p-1 border border-primary rounded material-picture">
      <div className="d-flex justify-content-center align-items-center mt-1">
        {!uploadDisabled && <FileInput id={BUTTON_ID} uppy={uppyRef.current} pretty inputName="files" />}
      </div>
      {!!existingImage && <img src={existingImage} alt="uploaded-image" className="w-100" />}
    </div>
  );
};

const mapStateToProps = ({ locale }: IRootState) => ({ locale });
type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(memo(PictureUploadButton));
