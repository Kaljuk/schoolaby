import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import React, { FC } from 'react';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import BlockListFileItem from 'app/shared/form/block-list-file-item';
import BlockList from 'app/shared/form/block-list';
import { UploadedFileProvider } from 'app/shared/contexts/uploaded-file-context';
import FilePreviewModal from 'app/shared/form/file-preview-modal';

interface IStudentUploadedFiles {
  files: IUploadedFile[];
  alternativeText?: string;
  className?: string;
  deletable?: boolean;
  onDelete?: (file) => void;
  canEdit?: boolean;
}

const UploadedFilesList: FC<IStudentUploadedFiles> = ({
  files,
  alternativeText,
  className,
  onDelete,
  deletable = false,
  canEdit = false,
}) => {
  return (
    <UploadedFileProvider>
      <BlockList className={className + ' d-flex flex-column flex-grow-1'}>
        {files?.length ? (
          files.map((file, i) => <BlockListFileItem key={i} uploadedFile={file} deletable={deletable} onDelete={() => onDelete(file)} />)
        ) : alternativeText ? (
          <p>{alternativeText}</p>
        ) : (
          <></>
        )}
        <FilePreviewModal files={files} canEdit={canEdit} />
      </BlockList>
    </UploadedFileProvider>
  );
};

export default UploadedFilesList;
