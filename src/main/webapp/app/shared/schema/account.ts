import { useMemo } from 'react';
import { translate as t } from 'react-jhipster';
import * as yup from 'yup';

const createYupStringMinMaxValidation = (min: number, max: number, translationPredicate: string) =>
  yup
    .string()
    .required(t(`${translationPredicate}.required`))
    .min(min, t(`${translationPredicate}.minlength`))
    .max(max, t(`${translationPredicate}.maxlength`));

export const useSettingsSchema = (locale: string) => {
  return useMemo(
    () =>
      yup.object().shape({
        firstName: createYupStringMinMaxValidation(1, 50, 'settings.messages.validate.firstname'),
        lastName: createYupStringMinMaxValidation(1, 50, 'settings.messages.validate.lastname'),
        email: createYupStringMinMaxValidation(5, 254, 'settings.messages.validate.email'),
        personRoles: yup.array().of(
          yup.object().shape({
            school: yup.object().shape({
              name: yup.string().required(t('settings.messages.validate.school.required')),
            }),
          })
        ),
      }),
    [locale]
  );
};

export const useRegisterSchema = (locale: string) => {
  return useMemo(
    () =>
      yup.object().shape({
        authority: yup.string().required(t('entity.validation.required')),
        country: yup.string().required(t('entity.validation.required')),
        username: createYupStringMinMaxValidation(1, 50, 'register.messages.validate.login').matches(
          /^[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$|^[_.@A-Za-z0-9-]+$/,
          t('register.messages.validate.login.pattern')
        ),
        firstName: createYupStringMinMaxValidation(1, 50, 'global.messages.validate.firstname'),
        lastName: createYupStringMinMaxValidation(1, 50, 'global.messages.validate.lastname'),
        email: createYupStringMinMaxValidation(5, 254, 'global.messages.validate.email'),
        personRoles: yup.array().of(
          yup.object().shape({
            school: yup.object().shape({
              name: yup.string().required(t('settings.messages.validate.school.required')),
            }),
          })
        ),
        termsAgreed: yup.boolean().oneOf([true], t('userManagement.termsAndConditions')),
        firstPassword: createYupStringMinMaxValidation(4, 50, 'global.messages.validate.newpassword'),
        secondPassword: createYupStringMinMaxValidation(4, 50, 'global.messages.validate.confirmpassword').test(
          'passwords-match',
          t('global.messages.error.dontmatch'),
          function (value) {
            return this.parent.firstPassword === value;
          }
        ),
      }),
    [locale]
  );
};
