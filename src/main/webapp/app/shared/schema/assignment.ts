import { useMemo } from 'react';
import * as yup from 'yup';
import { translate } from 'react-jhipster';

export const useAssignmentTitlePatchSchema = (locale: string) => {
  return useMemo(
    () =>
      yup.object().shape({
        title: yup.string().required(translate('entity.validation.required')),
      }),
    [locale]
  );
};

export const useAssignmentDeadlinePatchSchema = (locale: string) => {
  return useMemo(
    () =>
      yup.object().shape({
        deadline: yup.date().required(translate('entity.validation.required')),
      }),
    [locale]
  );
};
