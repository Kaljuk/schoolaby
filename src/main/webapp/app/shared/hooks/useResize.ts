import { useEffect, useState } from 'react';

export const useResize = ref => {
  const [size, setSize] = useState({
    width: 0,
    height: 0,
  });

  const handleResize = () => {
    setSize({
      width: ref.current.clientWidth,
      height: ref.current.clientHeight,
    });
  };

  useEffect(() => {
    const instance = ref?.current;
    if (instance) {
      handleResize();
      instance.addEventListener('resize', handleResize);
    }
    return () => {
      instance.removeEventListener('resize', handleResize);
    };
  }, [ref?.current?.clientHeight, ref?.current?.clientWidth]);

  return { width: size.width, height: size.height };
};
