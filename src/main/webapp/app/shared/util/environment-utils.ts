export const isLiveEnvironment = () => window?.location?.hostname === 'app.schoolaby.com';
