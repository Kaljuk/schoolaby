import { IPerson } from 'app/shared/model/person.model';

export const getFullName = (person: IPerson) => {
  const name = `${person?.firstName} ${person?.lastName}`;
  return name.replace(/(^\w{1})|(\s{1}\w{1})/g, match => match.toUpperCase());
};

export const getFullNames = (persons: IPerson[]) => {
  return persons.map(person => getFullName(person)).join(', ');
};

export const capitalize = (s: string) => s.replace(/(^\w{1})|(\s{1}\w{1})/g, match => match.toUpperCase());
