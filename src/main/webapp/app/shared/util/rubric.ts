import { ISelectedCritreionLevel } from 'app/shared/model/rubric/selected-critreion-level.model';
import { IRubric } from 'app/shared/model/rubric/rubric.model';

export const createRubric = (): IRubric => ({
  title: 'Test rubric',
  maxPoints: 8,
  minPoints: 3,
  isTemplate: false,
  assignmentId: 1234,
  criterions: [
    {
      id: 1,
      title: 'Criterion 1',
      description: 'Criterion description',
      sequenceNumber: 0,
      maxPoints: 5,
      minPoints: 2,
      levels: [
        {
          id: 1,
          title: 'Level 1',
          description: 'Level description',
          sequenceNumber: 0,
          points: 5,
          criterionId: 1,
        },
        {
          id: 2,
          title: 'Level 2',
          description: 'Level description',
          sequenceNumber: 1,
          points: 2,
          criterionId: 1,
        },
      ],
    },
    {
      id: 2,
      title: 'Criterion 2',
      description: 'Criterion description',
      sequenceNumber: 1,
      maxPoints: 3,
      minPoints: 1,
      levels: [
        {
          id: 3,
          title: 'Level 1',
          description: 'Level description',
          sequenceNumber: 0,
          points: 3,
          criterionId: 2,
        },
        {
          id: 4,
          title: 'Level 2',
          description: 'Level description',
          sequenceNumber: 1,
          points: 1,
          criterionId: 2,
        },
      ],
    },
  ],
});

export const createSelectedCriterionLevels = (): ISelectedCritreionLevel[] => [
  {
    id: 1,
    modifiedDescription: 'Modified description',
    criterionLevel: {
      id: 1,
      title: 'Level 1',
      description: 'Level description',
      points: 5,
      sequenceNumber: 0,
      criterionId: 1,
    },
  },
  {
    id: 2,
    modifiedDescription: 'Modified description',
    criterionLevel: {
      id: 4,
      title: 'Level 2',
      description: 'Level description',
      points: 1,
      sequenceNumber: 1,
      criterionId: 2,
    },
  },
];
