import { EntityState } from 'app/shared/model/enumerations/entity-state';
import inRange from 'lodash/inRange';
import { IGradingSchemeValue } from 'app/shared/model/grading-scheme-value.model';

type FieldType = 'text' | 'bg' | 'border' | 'icon' | 'scrollbar';
type ColorWeight = 'light' | 'medium' | 'dark' | 'transparent';
export type ColorType = 'success' | 'warning' | 'danger';

export const SBY_PRIMARY = '#009B50';
export const SBY_ORANGE = '#F78E1E';
export const SBY_PURPLE = '#5557CE';
export const SBY_ICON_BEIGE = '#D9D1C0';

export const SBY_DANGER = '#FF0022';

export const GREEN = '#009B50';
export const YELLOW = '#F78E1E';
export const RED = '#FF0022';
export const GREY = '#7B7B7B';

export const PRIMARY = '#009B50';
export const SECONDARY = '#a0a0a0';
export const WARNING = '#f9c700';
export const DANGER = '#e97556';
export const INFO = '#5187D8';
export const BLACK = '#000000';

export const P_ELEMENT = GREY;
export const LIGHT_ICON = '#DADADA';
export const ICON_DARK_GREY = '#8C8C8C';
export const ICON_GREY = '#666666';
export const ICON_GREY_2 = '#575757';
export const ICON_LIGHT_GREY = '#999999';
export const ICON_LIGHTER_GREY = '#BFBFBF';
export const EXTRA_LIGHT_GREY = '#E8E8E8';
export const ICON_WHITE = '#ffffff';
export const SBY_GRAY = '#595959';
export const SBY_GRAY_11 = '#8C8C8C';
export const SBY_BEIGE_2 = '#F6F2EA';
export const SBY_BEIGE_3 = '#DDD5C5';

export const DASHBOARD_ICON_GREY = '#8c8c8c';

export const ICON_STROKE_DEFAULT = RED;

const COLORS_BY_GRADE_PERCENTAGE_RANGE = [
  { color: GREEN, percentageRangeStart: 75, percentageRangeEnd: 101, className: 'green' },
  { color: YELLOW, percentageRangeStart: 50, percentageRangeEnd: 75, className: 'yellow' },
  { color: RED, percentageRangeStart: 0, percentageRangeEnd: 50, className: 'red' },
];

export const findColorByGradingSchemeValue = (gradingSchemeValue: IGradingSchemeValue) => {
  if (gradingSchemeValue?.percentageRangeStart != null && gradingSchemeValue?.percentageRangeEnd != null) {
    return COLORS_BY_GRADE_PERCENTAGE_RANGE.find(entry => {
      const middleValue = (gradingSchemeValue.percentageRangeEnd + gradingSchemeValue.percentageRangeStart) / 2;
      return inRange(middleValue, entry.percentageRangeStart, entry.percentageRangeEnd);
    }).color;
  }
  return GREY;
};

export const findClassNameByGradePercentage = (gradePercentage: number) => {
  return COLORS_BY_GRADE_PERCENTAGE_RANGE.find(entry => inRange(gradePercentage, entry.percentageRangeStart, entry.percentageRangeEnd))
    ?.className;
};

const getClassNameByState = (state: EntityState, field: FieldType, colorWeight?: ColorWeight) => {
  switch (state) {
    case EntityState.NOT_STARTED:
      return `${colorWeight ? colorWeight + '-' : ''}${field}-blue`;
    case EntityState.COMPLETED:
      return `${colorWeight ? colorWeight + '-' : ''}${field}-green`;
    case EntityState.IN_PROGRESS:
    case EntityState.URGENT:
      return `${colorWeight ? colorWeight + '-' : ''}${field}-orange`;
    case EntityState.REJECTED:
    case EntityState.OVERDUE:
      return `${colorWeight ? colorWeight + '-' : ''}${field}-red`;
    case EntityState.FAILED:
      return `${colorWeight ? colorWeight + '-' : ''}${field}-black`;
    default:
      return '';
  }
};

export const getClassNameByAssignmentState = (state: EntityState, field: FieldType, colorWeight?: ColorWeight): string => {
  return getClassNameByState(state, field, colorWeight);
};

export const getClassNameByMilestoneState = (state: EntityState, field: FieldType, colorWeight?: ColorWeight, newDesign?: boolean) => {
  if (field === 'text') {
    if (state === EntityState.OVERDUE || state === EntityState.REJECTED) {
      return `${colorWeight ? colorWeight + '-' : ''}${field}-red`;
    }
    if (newDesign) {
      if (state === EntityState.COMPLETED) {
        return '';
      }
    } else {
      return '';
    }
  }
  return getClassNameByState(state, field, colorWeight);
};
export const getColor = (colorType: ColorType, field: FieldType) => {
  if (field === 'border' && colorType === 'success') {
    return;
  } else {
    return `${field}-${colorType}`;
  }
};

export const getAssignmentCheckmarkColor = assignment => {
  switch (assignment.state) {
    case EntityState.COMPLETED:
      return PRIMARY;
    case EntityState.IN_PROGRESS:
    case EntityState.NOT_STARTED:
    case EntityState.URGENT:
      return SECONDARY;
    case EntityState.REJECTED:
    case EntityState.FAILED:
      return DANGER;
    case EntityState.OVERDUE:
    default:
      return '';
  }
};
