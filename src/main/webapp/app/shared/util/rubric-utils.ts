import { IRubric } from 'app/shared/model/rubric/rubric.model';
import { ICriterionLevel } from 'app/shared/model/rubric/criterion-level.model';
import { ISelectedCritreionLevel } from 'app/shared/model/rubric/selected-critreion-level.model';

interface ICriterionLevelCoordinates {
  criterionIndex: number;
  criterionLevelIndex: number;
}

export const calculateSelectedCriterionTotal = (selectedCriterionLevels: ISelectedCritreionLevel[]) =>
  selectedCriterionLevels?.reduce((acc, level) => acc + level.criterionLevel.points, 0) || 0;

export const getCriterionLevelCoordinatesByIdFromRubric = (criterionLevelId: number, rubric: IRubric): ICriterionLevelCoordinates => {
  let coordinates = null;
  rubric.criterions.forEach((criterion, criterionIndex) => {
    criterion.levels.forEach((level, levelIndex) => {
      if (level.id === criterionLevelId) {
        coordinates = {
          criterionIndex,
          criterionLevelIndex: levelIndex,
        };
      }
    });
  });
  return coordinates;
};

export const getCriterionLevelColorClass = (criterionLevel: ICriterionLevel, criterionLevelsCount: number) => {
  if (criterionLevel.sequenceNumber === 0) {
    return 'green';
  }
  if (criterionLevel.sequenceNumber + 1 === criterionLevelsCount) {
    return 'red';
  }
  return 'yellow';
};

export const getPointsColorClass = (points: number, minPoints: number, maxPoints: number) => {
  const decreasedPoints = points - minPoints;
  const minMaxDifference = maxPoints - minPoints;
  const percentageOfPoints = (decreasedPoints * 100) / minMaxDifference;
  const roundedPercentage = Math.round(percentageOfPoints * 10) / 10;

  if (roundedPercentage > 66.7) {
    return 'green';
  } else if (roundedPercentage <= 66.7 && roundedPercentage > 33.3) {
    return 'yellow';
  }
  return 'red';
};

export const clearRubric = rubric => {
  if (rubric) {
    rubric.id = undefined;
    rubric.isTemplate = false;
    rubric.criterions?.forEach(criterion => {
      criterion.id = undefined;
      criterion.levels?.forEach(level => (level.id = undefined));
    });
  }
};
