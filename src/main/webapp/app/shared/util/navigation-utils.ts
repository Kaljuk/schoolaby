export class NavigationPath {
  static ADD_ASSIGNMENT = '/assignment/new';
  static ADD_MILESTONE = '/milestone/new';
  static DELETE_USER = '/admin/user-management/student/delete';
  static JOIN_JOURNEY = '/journey/join';
  static CREATE_JOURNEY = '/journey/new';
}

export const getAbsolutePath = (url: string) => {
  url = url.startsWith('/') ? url.substr(1) : url;
  return `${window.location.origin}/${url}`;
};
