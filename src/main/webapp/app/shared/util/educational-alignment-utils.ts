import { IEducationalAlignment } from 'app/shared/model/educational-alignment.model';
import differenceBy from 'lodash/differenceBy';
import groupBy from 'lodash/groupBy';
import uniqBy from 'lodash/uniqBy';

export interface IDropdownItem {
  label?: string;
  value?: number;
  data?: IEducationalAlignment;
}

export const educationalAlignmentsDifferenceByTitle = (
  alignmentsToFilter: IEducationalAlignment[],
  alignmentsToFilterBy: IEducationalAlignment[]
) => differenceBy(alignmentsToFilter, alignmentsToFilterBy, 'title');

export const handleDuplicateEducationalAlignments = (educationalAlignments: IEducationalAlignment[]) => {
  const alignmentsGroupedByTitle = groupBy(educationalAlignments, 'title');
  const uniqueAlignments: IEducationalAlignment[] = uniqBy(educationalAlignments, 'title');
  return uniqueAlignments.map(uniqueAlignment => ({
    ...uniqueAlignment,
    taxonIds: alignmentsGroupedByTitle[uniqueAlignment.title].map(duplicateAlignment => duplicateAlignment.taxonId),
  }));
};

export const mapToDropdownItem = (educationalAlignment: IEducationalAlignment): IDropdownItem => ({
  label: educationalAlignment?.title,
  value: educationalAlignment?.id,
  data: educationalAlignment,
});
