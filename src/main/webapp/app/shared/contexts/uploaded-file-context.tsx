import React, { useState } from 'react';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';

export const UploadedFileContext = React.createContext(undefined);

function UploadedFileProvider({ children }) {
  const [previewFile, setPreviewFile] = useState<IUploadedFile>(null);

  return (
    <UploadedFileContext.Provider
      value={{
        previewFile,
        setPreviewFile,
      }}
    >
      {children}
    </UploadedFileContext.Provider>
  );
}

const usePreviewFileState = (): {
  previewFile: IUploadedFile;
  setPreviewFile: React.Dispatch<React.SetStateAction<IUploadedFile>>;
} => {
  const context = React.useContext(UploadedFileContext);
  if (context === undefined) {
    throw new Error('usePreviewFileState must be used within a UploadedFileProvider');
  }
  return {
    previewFile: context.previewFile,
    setPreviewFile(file: IUploadedFile) {
      return context.setPreviewFile(file);
    },
  };
};

export { UploadedFileProvider, usePreviewFileState };
