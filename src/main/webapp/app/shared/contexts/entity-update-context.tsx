import React, { useState } from 'react';
import { IEducationalAlignment } from 'app/shared/model/educational-alignment.model';
import { Material } from 'app/shared/model/material.model';
import ISubject from 'app/shared/model/subject.model';
import { IRubric } from 'app/shared/model/rubric/rubric.model';
import { IPerson } from 'app/shared/model/person.model';
import { IGroup } from 'app/shared/model/group.model';
import { IAssignment } from 'app/shared/model/assignment.model';
import { IMilestone } from 'app/shared/model/milestone.model';

export const EntityUpdateContext = React.createContext(undefined);

export const UPDATE_MILESTONE = 'UPDATE_MILESTONE';
export const UPDATE_ASSIGNMENT = 'UPDATE_ASSIGNMENT';
type EntityUpdateType = typeof UPDATE_MILESTONE | typeof UPDATE_ASSIGNMENT;

function EntityUpdateProvider({ children }) {
  const [selectedMaterials, setSelectedMaterials] = useState<Material[]>([]);
  const [selectedEducationalAlignments, setSelectedEducationalAlignments] = useState<IEducationalAlignment[]>([]);
  const [parentEducationalAlignments, setParentEducationalAlignments] = useState<IEducationalAlignment[]>([]);
  const [selectedSubject, setSelectedSubject] = useState<ISubject>(undefined);
  const [selectedRubric, setSelectedRubric] = useState<IRubric>(undefined);
  const [savingRubricAsTemplate, setSavingRubricAsTemplate] = useState<boolean>(false);
  const [unSavingRubricAsTemplate, setUnSavingRubricAsTemplate] = useState<boolean>(false);
  const [selectingRubricTemplate, setSelectingRubricTemplate] = useState<boolean>(false);
  const [selectedStudents, setSelectedStudents] = useState<IPerson[]>(undefined);
  const [selectedGroups, setSelectedGroups] = useState<IGroup[]>([]);
  const [gradeGroupMembersIndividually, setGradeGroupMembersIndividually] = useState<boolean>(false);
  const [selectedGroupAssignment, setSelectedGroupAssignment] = useState<IAssignment>(undefined);
  const [displayEntityUpdate, setDisplayEntityUpdate] = useState<EntityUpdateType>(undefined);
  const [milestoneUpdateId, setMilestoneUpdateId] = useState<number>(undefined);
  const [assignmentUpdateId, setAssignmentUpdateId] = useState<number>(undefined);
  const [entity, setEntity] = useState<IMilestone | IAssignment>(null);

  const clearMilestoneUpdateId = () => {
    setMilestoneUpdateId(undefined);
  };

  const clearAssignmentUpdateId = () => {
    setAssignmentUpdateId(undefined);
  };

  const clearEntityUpdateIds = () => {
    clearMilestoneUpdateId();
    clearAssignmentUpdateId();
  };

  const closeEntityUpdate = () => {
    clearEntityUpdateIds();
    setDisplayEntityUpdate(undefined);
  };

  const openMilestoneUpdate = (milestoneId?: number) => {
    !!milestoneId && setMilestoneUpdateId(milestoneId);
    setDisplayEntityUpdate(UPDATE_MILESTONE);
  };

  const openAssignmentUpdate = (assignmentId?: number) => {
    !!assignmentId && setAssignmentUpdateId(assignmentId);
    setDisplayEntityUpdate(UPDATE_ASSIGNMENT);
  };

  return (
    <EntityUpdateContext.Provider
      value={{
        selectedMaterials,
        setSelectedMaterials,
        selectedEducationalAlignments,
        setSelectedEducationalAlignments,
        parentEducationalAlignments,
        setParentEducationalAlignments,
        selectedSubject,
        setSelectedSubject,
        selectedRubric,
        setSelectedRubric,
        selectedStudents,
        setSelectedStudents,
        selectedGroups,
        setSelectedGroups,
        gradeGroupMembersIndividually,
        setGradeGroupMembersIndividually,
        selectedGroupAssignment,
        setSelectedGroupAssignment,
        displayEntityUpdate,
        setDisplayEntityUpdate,
        closeEntityUpdate,
        milestoneUpdateId,
        setMilestoneUpdateId,
        assignmentUpdateId,
        setAssignmentUpdateId,
        openMilestoneUpdate,
        openAssignmentUpdate,
        clearMilestoneUpdateId,
        clearAssignmentUpdateId,
        clearEntityUpdateIds,
        entity,
        setEntity,
        savingRubricAsTemplate,
        setSavingRubricAsTemplate,
        unSavingRubricAsTemplate,
        setUnSavingRubricAsTemplate,
        selectingRubricTemplate,
        setSelectingRubricTemplate,
      }}
    >
      {children}
    </EntityUpdateContext.Provider>
  );
}

function useSelectedEducationalAlignmentsState() {
  const context = React.useContext(EntityUpdateContext);
  if (context === undefined) {
    throw new Error('useSelectedEducationalAlignmentState must be used within a EntityUpdateProvider');
  }
  return {
    selectedEducationalAlignments: context.selectedEducationalAlignments,
    setSelectedEducationalAlignments: context.setSelectedEducationalAlignments,
    addSelectedEducationAlignment(educationalAlignment: IEducationalAlignment) {
      context.setSelectedEducationalAlignments(prevState => [...prevState, educationalAlignment]);
    },
    removeSelectedEducationalAlignment(educationalAlignment) {
      context.setSelectedEducationalAlignments(prevState => [
        ...prevState.filter(selectedAlignment => selectedAlignment !== educationalAlignment),
      ]);
    },
  };
}

function useSelectedSubjectState() {
  const context = React.useContext(EntityUpdateContext);
  if (context === undefined) {
    throw new Error('useSelectedSubjectState must be used within a EntityUpdateProvider');
  }
  return {
    selectedSubject: context.selectedSubject,
    setSelectedSubject: context.setSelectedSubject,
  };
}

const useSelectedMaterialsState = (): {
  selectedMaterials: Material[];
  setSelectedMaterials: React.Dispatch<React.SetStateAction<Material[]>>;
} => {
  const context = React.useContext(EntityUpdateContext);
  if (context === undefined) {
    throw new Error('useSelectedMaterialsState must be used within a EntityUpdateProvider');
  }
  return { selectedMaterials: context.selectedMaterials, setSelectedMaterials: context.setSelectedMaterials };
};

const useParentEducationalAlignmentsState = () => {
  const context = React.useContext(EntityUpdateContext);
  if (context === undefined) {
    throw new Error('useParentEducationalAlignmentsState must be used within a EntityUpdateProvider');
  }
  return {
    parentEducationalAlignments: context.parentEducationalAlignments,
    setParentEducationalAlignments: context.setParentEducationalAlignments,
  };
};

const useRubricTemplateState = (): {
  savingRubricAsTemplate: boolean;
  setSavingRubricAsTemplate: React.Dispatch<React.SetStateAction<boolean>>;
  unSavingRubricAsTemplate: boolean;
  setUnSavingRubricAsTemplate: React.Dispatch<React.SetStateAction<boolean>>;
  selectingRubricTemplate: boolean;
  setSelectingRubricTemplate: React.Dispatch<React.SetStateAction<boolean>>;
} => {
  const context = React.useContext(EntityUpdateContext);
  if (context === undefined) {
    throw new Error('useRubricTemplateState must be used within a EntityUpdateProvider');
  }
  return {
    savingRubricAsTemplate: context.savingRubricAsTemplate,
    setSavingRubricAsTemplate: context.setSavingRubricAsTemplate,
    unSavingRubricAsTemplate: context.unSavingRubricAsTemplate,
    setUnSavingRubricAsTemplate: context.setUnSavingRubricAsTemplate,
    selectingRubricTemplate: context.selectingRubricTemplate,
    setSelectingRubricTemplate: context.setSelectingRubricTemplate,
  };
};

const useSelectedRubricState = () => {
  const context = React.useContext(EntityUpdateContext);
  if (context === undefined) {
    throw new Error('useSelectedRubricState must be used within a EntityUpdateProvider');
  }
  return {
    selectedRubric: context.selectedRubric,
    setSelectedRubric: context.setSelectedRubric,
  };
};

const useSelectedStudentsState = (): {
  selectedStudents: IPerson[];
  setSelectedStudents: React.Dispatch<React.SetStateAction<IPerson[]>>;
} => {
  const context = React.useContext(EntityUpdateContext);
  if (context === undefined) {
    throw new Error('useSelectedStudentsState must be used within a EntityUpdateContext');
  }
  return {
    selectedStudents: context.selectedStudents,
    setSelectedStudents: context.setSelectedStudents,
  };
};

const useSelectedGroupsState = (): {
  selectedGroups: IGroup[];
  setSelectedGroups: React.Dispatch<React.SetStateAction<IGroup[]>>;
} => {
  const context = React.useContext(EntityUpdateContext);
  if (context === undefined) {
    throw new Error('useSelectedGroupsState must be used within a EntityUpdateProvider');
  }
  return {
    selectedGroups: context.selectedGroups,
    setSelectedGroups: context.setSelectedGroups,
  };
};

const useGroupMembersGradedIndividuallyState = (): {
  gradeGroupMembersIndividually: boolean;
  setGradeGroupMembersIndividually: React.Dispatch<React.SetStateAction<boolean>>;
} => {
  const context = React.useContext(EntityUpdateContext);
  if (context === undefined) {
    throw new Error('useGroupMembersGradedIndividuallyState must be used within a EntityUpdateProvider');
  }
  return {
    gradeGroupMembersIndividually: context.gradeGroupMembersIndividually,
    setGradeGroupMembersIndividually: context.setGradeGroupMembersIndividually,
  };
};

const useSelectedGroupAssignmentState = (): {
  selectedGroupAssignment: IAssignment;
  setSelectedGroupAssignment: React.Dispatch<React.SetStateAction<IAssignment>>;
} => {
  const context = React.useContext(EntityUpdateContext);
  if (context === undefined) {
    throw new Error('useSelectedGroupAssignmentState must be used within a EntityUpdateProvider');
  }
  return {
    selectedGroupAssignment: context.selectedGroupAssignment,
    setSelectedGroupAssignment: context.setSelectedGroupAssignment,
  };
};

const useDisplayEntityUpdateState = (): {
  displayEntityUpdate: EntityUpdateType;
  setDisplayEntityUpdate: React.Dispatch<React.SetStateAction<EntityUpdateType>>;
  openMilestoneUpdate: (milestoneId?: number) => void;
  openAssignmentUpdate: (assignmentId?: number) => void;
  closeEntityUpdate: () => void;
} => {
  const context = React.useContext(EntityUpdateContext);
  if (context === undefined) {
    throw new Error('useDisplayEntityUpdateState must be used within a EntityUpdateProvider');
  }
  return {
    displayEntityUpdate: context.displayEntityUpdate,
    setDisplayEntityUpdate: context.setDisplayEntityUpdate,
    openMilestoneUpdate: context.openMilestoneUpdate,
    openAssignmentUpdate: context.openAssignmentUpdate,
    closeEntityUpdate: context.closeEntityUpdate,
  };
};
const useEntityUpdateIdState = (): {
  milestoneUpdateId: number;
  setMilestoneUpdateId: React.Dispatch<React.SetStateAction<number>>;
  assignmentUpdateId: number;
  setAssignmentUpdateId: React.Dispatch<React.SetStateAction<number>>;
  clearMilestoneUpdateId: () => void;
  clearAssignmentUpdateId: () => void;
  clearEntityUpdateIds: () => void;
} => {
  const context = React.useContext(EntityUpdateContext);
  if (context === undefined) {
    throw new Error('useEntityUpdateIdState must be used within a EntityUpdateProvider');
  }
  return {
    milestoneUpdateId: context.milestoneUpdateId,
    setMilestoneUpdateId: context.setMilestoneUpdateId,
    assignmentUpdateId: context.assignmentUpdateId,
    setAssignmentUpdateId: context.setAssignmentUpdateId,
    clearMilestoneUpdateId: context.clearMilestoneUpdateId,
    clearAssignmentUpdateId: context.clearAssignmentUpdateId,
    clearEntityUpdateIds: context.clearEntityUpdateIds,
  };
};

const useEntityToUpdateState = <entityType extends IMilestone | IAssignment>(): {
  entity: entityType;
  setEntity: React.Dispatch<React.SetStateAction<entityType>>;
} => {
  const context = React.useContext(EntityUpdateContext);
  if (context === undefined) {
    throw new Error('useEntityToUpdateState must be used within a EntityUpdateProvider');
  }
  return {
    entity: context.entity,
    setEntity: context.setEntity,
  };
};

export {
  EntityUpdateProvider,
  useSelectedEducationalAlignmentsState,
  useSelectedMaterialsState,
  useParentEducationalAlignmentsState,
  useSelectedSubjectState,
  useSelectedRubricState,
  useRubricTemplateState,
  useSelectedStudentsState,
  useSelectedGroupsState,
  useGroupMembersGradedIndividuallyState,
  useSelectedGroupAssignmentState,
  useDisplayEntityUpdateState,
  useEntityUpdateIdState,
  useEntityToUpdateState,
};
