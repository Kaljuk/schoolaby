import React, { useState } from 'react';
import { IUser } from 'app/shared/model/user.model';
import { GradingScheme } from 'app/shared/model/grading-scheme.model';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { IMilestone } from 'app/shared/model/milestone.model';
import { IAssignment } from 'app/shared/model/assignment.model';
import { ILtiLaunch } from 'app/shared/model/lti-launch.model';
import filter from 'lodash/filter';
import { IGroup } from 'app/shared/model/group.model';

const EntityDetailContext = React.createContext(undefined);

function EntityDetailProvider({ children }) {
  const [milestone, setMilestone] = useState<IMilestone>();
  const [entity, setEntity] = useState<IAssignment>();
  const [submissions, setSubmissions] = useState<ISubmission[]>([]);
  const [gradingScheme, setGradingScheme] = useState<GradingScheme>();
  const [selectedUser, setSelectedUser] = useState<IUser>();
  const [selectedGroup, setSelectedGroup] = useState<IGroup>();
  const [ltiLaunches, setLtiLaunches] = useState<ILtiLaunch[]>([]);
  const [deadlineTarget, setDeadlineTarget] = useState<IAssignment>(null);
  const [newDeadline, setNewDeadline] = useState<string>(null);
  const [selectedSubmission, setSelectedSubmission] = useState<ISubmission>(null);

  return (
    <EntityDetailContext.Provider
      value={{
        selectedUser,
        setSelectedUser,
        selectedGroup,
        setSelectedGroup,
        gradingScheme,
        setGradingScheme,
        submissions,
        setSubmissions,
        entity,
        setEntity,
        milestone,
        setMilestone,
        ltiLaunches,
        setLtiLaunches,
        deadlineTarget,
        setDeadlineTarget,
        newDeadline,
        setNewDeadline,
        selectedSubmission,
        setSelectedSubmission,
      }}
    >
      {children}
    </EntityDetailContext.Provider>
  );
}

function useSelectedUserState(): { selectedUser: IUser; setSelectedUser: React.Dispatch<React.SetStateAction<IUser>> } {
  const context = React.useContext(EntityDetailContext);
  if (context === undefined) {
    throw new Error('useSelectedUserState must be used within a EntityDetailContext');
  }
  return {
    selectedUser: context.selectedUser,
    setSelectedUser: context.setSelectedUser,
  };
}

function useSelectedGroupState(): { selectedGroup: IGroup; setSelectedGroup: React.Dispatch<React.SetStateAction<IGroup>> } {
  const context = React.useContext(EntityDetailContext);
  if (context === undefined) {
    throw new Error('useSelectedGroupState must be used within a EntityDetailContext');
  }
  return {
    selectedGroup: context.selectedGroup,
    setSelectedGroup: context.setSelectedGroup,
  };
}

function useSelectedSubmissionState(): {
  selectedSubmission: ISubmission;
  setSelectedSubmission: React.Dispatch<React.SetStateAction<ISubmission>>;
} {
  const context = React.useContext(EntityDetailContext);
  if (context === undefined) {
    throw new Error('useSelectedSubmissionState must be used within a EntityDetailContext');
  }
  return {
    selectedSubmission: context.selectedSubmission,
    setSelectedSubmission: context.setSelectedSubmission,
  };
}

function useGradingSchemeState(): { gradingScheme: GradingScheme; setGradingScheme: React.Dispatch<React.SetStateAction<GradingScheme>> } {
  const context = React.useContext(EntityDetailContext);
  if (context === undefined) {
    throw new Error('useGradingSchemeState must be used within a EntityDetailContext');
  }
  return { gradingScheme: context.gradingScheme, setGradingScheme: context.setGradingScheme };
}

function useSubmissionsState(): { submissions: ISubmission[]; setSubmissions: React.Dispatch<React.SetStateAction<ISubmission[]>> } {
  const context = React.useContext(EntityDetailContext);
  if (context === undefined) {
    throw new Error('useSubmissionsState must be used within a EntityDetailContext');
  }
  return { submissions: context.submissions, setSubmissions: context.setSubmissions };
}

function useAssignmentState(): { entity: IAssignment; setEntity: React.Dispatch<React.SetStateAction<IAssignment>> } {
  const context = React.useContext(EntityDetailContext);
  if (context === undefined) {
    throw new Error('useAssignmentState must be used within a EntityDetailContext');
  }
  return {
    entity: context.entity,
    setEntity: context.setEntity,
  };
}

function useLtiLaunchesState(): { ltiLaunches: ILtiLaunch[]; setLtiLaunches: React.Dispatch<React.SetStateAction<ILtiLaunch[]>> } {
  const context = React.useContext(EntityDetailContext);
  if (context === undefined) {
    throw new Error('useLtiLaunchesState must be used within a EntityDetailContext');
  }
  return {
    ltiLaunches: context.ltiLaunches,
    setLtiLaunches(launches: ILtiLaunch[]) {
      const gradableLaunches = filter(launches, launch => launch?.ltiResource?.syncGrade);
      return context.setLtiLaunches(gradableLaunches);
    },
  };
}

function useDeadlineTargetState(): { deadlineTarget: IAssignment; setDeadlineTarget: React.Dispatch<React.SetStateAction<IAssignment>> } {
  const context = React.useContext(EntityDetailContext);
  if (context === undefined) {
    throw new Error('useDeadlineTargetState must be used within a EntityDetailContext');
  }
  return {
    deadlineTarget: context.deadlineTarget,
    setDeadlineTarget(deadlineTarget: IAssignment) {
      return context.setDeadlineTarget(deadlineTarget);
    },
  };
}

function useNewDeadlineState(): { newDeadline: string; setNewDeadline: React.Dispatch<React.SetStateAction<string>> } {
  const context = React.useContext(EntityDetailContext);
  if (context === undefined) {
    throw new Error('useNewDeadlineState must be used within a EntityDetailContext');
  }
  return {
    newDeadline: context.newDeadline,
    setNewDeadline(newDeadline: string) {
      return context.setNewDeadline(newDeadline);
    },
  };
}

export {
  EntityDetailProvider,
  useSelectedUserState,
  useSelectedGroupState,
  useSelectedSubmissionState,
  useGradingSchemeState,
  useSubmissionsState,
  useAssignmentState,
  useLtiLaunchesState,
  useDeadlineTargetState,
  useNewDeadlineState,
};
