import React, { useState } from 'react';
import { IChat } from 'app/shared/model/chat.model';

const ChatContext = React.createContext(undefined);

function ChatProvider({ children }) {
  const [generatedChat, setGeneratedChat] = useState<IChat>(undefined);

  const clearGeneratedChat = (): void => {
    setGeneratedChat(undefined);
  };

  return (
    <ChatContext.Provider
      value={{
        setGeneratedChat,
        generatedChat,
        clearGeneratedChat,
      }}
    >
      {children}
    </ChatContext.Provider>
  );
}

const useGeneratedChatState = (): {
  generatedChat: IChat;
  setGeneratedChat: React.Dispatch<React.SetStateAction<IChat>>;
  clearGeneratedChat(): void;
} => {
  const context = React.useContext(ChatContext);
  if (context === undefined) {
    throw new Error('useGeneratedChatState must be used within a ChatProvider');
  }
  return {
    generatedChat: context.generatedChat,
    setGeneratedChat: context.setGeneratedChat,
    clearGeneratedChat: context.clearGeneratedChat,
  };
};

export { ChatProvider, useGeneratedChatState };
