import React from 'react';
import { Route, RouteProps } from 'react-router-dom';
import ErrorBoundary from 'app/shared/error/error-boundary';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { MaterialProvider } from 'app/shared/contexts/material-context';

const ErrorBoundaryUpdateEntityRoute = ({ component: Component, ...rest }: RouteProps) => {
  const encloseInErrorBoundary = props => (
    <ErrorBoundary>
      <EntityUpdateProvider>
        <MaterialProvider>
          <Component {...props} />
        </MaterialProvider>
      </EntityUpdateProvider>
    </ErrorBoundary>
  );

  if (!Component) throw new Error(`A component needs to be specified for path ${(rest as any).path}`);

  return <Route {...rest} render={encloseInErrorBoundary} />;
};

export default ErrorBoundaryUpdateEntityRoute;
