import React, { useEffect, useState } from 'react';
import { translate } from 'react-jhipster';
import { Button, Col, Row } from 'reactstrap';

const PageNotFound = () => {
  const [imageSrc, setImageSrc] = useState<string>('');
  const images = ['content/images/not_found_1.svg', 'content/images/not_found_2.svg', 'content/images/not_found_3.svg'];

  useEffect(() => {
    setImageSrc(images[Math.floor(Math.random() * images.length)]);
  }, []);

  return (
    <div>
      <Row>
        <Col className="text-center pt-5 mt-5 px-5 mx-5">
          <img alt={'success'} src={imageSrc} className="img-fluid" />
          <h2 className={'mt-4'}>{translate('error.http.404')}</h2>
          <h4 className={'text-secondary font-weight-normal mt-4'}>{translate('error.404Description')}</h4>
          <Button color="primary" className="mt-3" href="/">
            <span className="mx-3">{translate('error.backToDashboard')}</span>
          </Button>
        </Col>
      </Row>
    </div>
  );
};

export default PageNotFound;
