import axios from 'axios';
import { IMessage } from 'app/shared/model/message.model';
import { cleanEntity } from 'app/shared/util/entity-utils';
import { IMessageView } from 'app/shared/model/messageView.model';
import { toast } from 'react-toastify';
import { translate } from 'react-jhipster';

interface MessageFilter {
  submissionId?: number;
  journeyId?: number;
  feedback?: boolean;
  sort?: string;
  page?: number;
  size?: number;
}

export const createMessage = entity => {
  return axios.post<IMessage>(`api/messages`, cleanEntity(entity)).catch(error => {
    if (error?.response?.data?.message) {
      toast.error(translate(error.response.data.message));
    }
    return Promise.reject(error);
  });
};

export const updateMessage = entity => {
  return axios.put<IMessage>(`api/messages`, cleanEntity(entity)).catch(error => {
    if (error?.response?.data?.message) {
      toast.error(translate(error.response.data.message));
    }
    return Promise.reject(error);
  });
};

export const findAllMessages = (params: MessageFilter) => {
  return axios
    .get<IMessage[]>(`api/messages`, { params })
    .catch(error => {
      if (error?.response?.data?.message) {
        toast.error(translate(error.response.data.message));
      }
      return Promise.reject(error);
    });
};

export const getMessageViews = (sort?: string, page?: number, size?: number) => {
  const params = { page, size, sort };
  return axios
    .get<IMessageView[]>(`api/messages/views`, { params })
    .catch(error => {
      if (error?.response?.data?.message) {
        toast.error(translate(error.response.data.message));
      }
      return Promise.reject(error);
    });
};
