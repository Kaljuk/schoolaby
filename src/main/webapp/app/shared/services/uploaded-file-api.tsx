import axios from 'axios';
import { JOURNEY_KEY, UPLOADED_FILE_KEY } from 'app/config/reactQueryKeyConstants';
import { useQueryClient, useMutation } from 'react-query';

export const getFileUrl = (uniqueFileName: string) => {
  return `api/files/${uniqueFileName}`;
};

export const getFullFileUrl = (uniqueFileName: string) => {
  return `${window.location.origin}/api/files/${uniqueFileName}`;
};

export const downloadUploadedFile = (uniqueFileName: string) => {
  return axios.get(getFileUrl(uniqueFileName), {
    responseType: 'arraybuffer',
  });
};

export const useDeleteUploadedFile = () => {
  const queryClient = useQueryClient();
  return useMutation(async (id: number) => axios.delete(`api/uploaded-files/${id}`), {
    onSuccess() {
      queryClient.invalidateQueries(UPLOADED_FILE_KEY);
    },
  });
};
