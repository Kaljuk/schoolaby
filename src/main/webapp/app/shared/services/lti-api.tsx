import axios from 'axios';
import { ILtiResourceParameters } from 'app/shared/layout/lti/lti-modal';
import { IJourney } from 'app/shared/model/journey.model';
import { ILtiResource } from 'app/shared/model/lti-resource.model';
import { ILtiApp } from 'app/shared/model/lti-app.model';
import { ILtiConfig } from 'app/shared/model/lti-config.model';
import { ILtiScore } from 'app/shared/model/lti-score.model';
import { ASSIGNMENT_LTI_RESOURCES_KEY, LTI_CONFIGS_KEY, LTI_SCORES_KEY } from 'app/config/reactQueryKeyConstants';
import { useQuery } from 'react-query';

// TODO: use react-query

export const ENTITY_TYPE_ASSIGNMENT = 'ASSIGNMENT';

interface LtiResourceLaunchRequestParameters {
  ltiResourceId: number;
}

interface LtiAppLaunchRequestParameters {
  ltiAppId: number;
  journeyId: number;
  ltiResourceLinkId: string;
}

interface LtiConfigurationsRequestParameters {
  journeyId: string | number;
}

interface LtiConfigurationCreationParameters {
  journey: IJourney;
  sharedSecret: string;
  consumerKey: string;
}

interface LtiConfigurationUpdateParameters {
  id?: number;
  journey: IJourney;
  sharedSecret: string;
  consumerKey: string;
}

interface LtiConfigurationDeletionParameters {
  id: number;
}

interface LtiResourcesParameters {
  milestoneId?: number;
  assignmentId?: number;
}

interface LtiScoresParams {
  assignmentId: number;
  userId?: number;
}

export const getLtiConfigurations = (params: LtiConfigurationsRequestParameters) =>
  axios.get<ILtiConfig[]>(`api/lti/config?journeyId=${params.journeyId}`);

export const useGetLtiConfigurations = (
  params: LtiConfigurationsRequestParameters,
  enabled = true,
  onSuccess?: (ltiConfigs: ILtiConfig[]) => void
) =>
  useQuery<ILtiConfig[], Error>(
    [LTI_CONFIGS_KEY, params.journeyId],
    async () => axios.get<ILtiConfig[]>(`api/lti/config?journeyId=${params.journeyId}`).then(r => r.data),
    {
      enabled,
      onSuccess,
    }
  );

export const getLtiResources = (params: LtiResourcesParameters) =>
  axios
    .get<ILtiResource[]>(`/api/lti/resource`, { params: { ...params } })
    .then(response => response.data);

export const useGetLtiResources = (params: LtiResourcesParameters, enabled = true) =>
  useQuery<ILtiResource[], Error>(
    [ASSIGNMENT_LTI_RESOURCES_KEY, params.assignmentId, params.milestoneId],
    async () => getLtiResources(params),
    {
      enabled,
    }
  );

export const getLtiResourceLaunchParameters = (params: LtiResourceLaunchRequestParameters) =>
  axios.get<ILtiResourceParameters>(`/api/lti/resource/launch/parameters`, {
    params: {
      ltiResourceId: params.ltiResourceId,
      returnUrl: `${window.location.origin}/content/files/lti_launch_presentation.html`,
    },
  });

export const getLtiAppLaunchParameters = (params: LtiAppLaunchRequestParameters) =>
  axios.get<ILtiResourceParameters>(`/api/lti/app/launch/parameters`, {
    params: {
      ltiAppId: params.ltiAppId,
      journeyId: params.journeyId,
      ltiResourceLinkId: params.ltiResourceLinkId,
      returnUrl: `${window.location.origin}/content/files/lti_launch_presentation.html`,
    },
  });

export const createLtiConfiguration = (params: LtiConfigurationCreationParameters) => axios.post('api/lti/config', params);

export const updateLtiConfiguration = (params: LtiConfigurationUpdateParameters) => axios.put('api/lti/config', params);

export const deleteLtiConfiguration = (params: LtiConfigurationDeletionParameters) => axios.delete(`api/lti/config/${params.id}`);

export const getLtiApps = () => axios.get<ILtiApp[]>('api/lti/app');

export const getLtiScores = (params: { assignmentId: number; userId?: number }) =>
  axios.get<ILtiScore[]>('api/lti-scores', {
    params,
  });

export const useGetLtiScores = (params: LtiScoresParams, enabled = true) =>
  useQuery<ILtiScore[], Error>(
    [LTI_SCORES_KEY, params.assignmentId, params.userId],
    async () =>
      axios
        .get<ILtiScore[]>('api/lti-scores', {
          params,
        })
        .then(response => response.data),
    {
      enabled,
    }
  );
