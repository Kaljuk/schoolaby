import { useMutation } from 'react-query';
import axios from 'axios';
import ISupportEmail from 'app/shared/model/support-email.model';

export const useCreateSupportEmail = (onSuccess?: () => void) =>
  useMutation((supportEmail: ISupportEmail) => axios.post('api/support', supportEmail), {
    onSuccess,
  });
