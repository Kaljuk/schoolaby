import axios from 'axios';
import { toast } from 'react-toastify';
import { translate } from 'react-jhipster';
import { INotification } from 'app/shared/model/notification.model';
import { NOTIFICATIONS_KEY } from 'app/config/reactQueryKeyConstants';
import { useQuery, useQueryClient, useMutation } from 'react-query';

type NotificationType = 'GENERAL' | 'MESSAGE';

interface GetNotificationsParameters {
  type?: NotificationType;
  sort?: string;
  page?: number;
  size?: number;
}

export interface PatchNotificationsParam {
  id: number;
  read: string;
}

export const getNotifications = (params: GetNotificationsParameters) => {
  return axios
    .get<INotification[]>(`api/notifications`, {
      params,
    })
    .then(response => response.data)
    .catch(error => {
      if (error?.response?.data?.message) {
        toast.error(translate(error.response.data.message));
        return Promise.reject(error);
      }
    });
};

export const useGetNotifications = (params: GetNotificationsParameters, enabled = true) =>
  useQuery<INotification[]>([NOTIFICATIONS_KEY, params.type], async () => getNotifications(params), {
    enabled,
  });

export const patchNotifications = (params?: PatchNotificationsParam[]) => axios.patch<INotification[]>(`api/notifications`, [...params]);

export const usePatchNotifications = () => {
  const queryClient = useQueryClient();
  return useMutation(async (params?: PatchNotificationsParam[]) => patchNotifications(params), {
    onSuccess() {
      queryClient.invalidateQueries([NOTIFICATIONS_KEY]);
    },
  });
};
