import axios from 'axios';
import { cleanEntity, EntityType } from 'app/shared/util/entity-utils';
import { IChat } from 'app/shared/model/chat.model';

export const getChatsByJourney = (entityType: EntityType, journeyId: number, page = 0) => {
  return axios.get<IChat[]>(`api/chats?journeyId=${journeyId}&entity=${entityType}&page=${page}&sort=messages.createdDate,desc`);
};

export const getChatsBySubmission = (submissionId: number) => {
  return axios.get<IChat[]>(`api/chats?submissionId=${submissionId}&sort=messages.createdDate,desc`);
};

export const createChat = (entity: IChat) => {
  return axios.post<IChat>(`api/chats`, cleanEntity(entity), { showToast: false });
};
