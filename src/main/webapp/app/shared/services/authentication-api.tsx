import axios from 'axios';

export const refreshAuthentication = () => {
  return axios.get(`api/authenticate/refresh`);
};
