import axios from 'axios';
import { SCHOOL_KEY } from 'app/config/reactQueryKeyConstants';
import { useQuery } from 'react-query';
import { ISchool } from 'app/shared/model/school.model';

export const useGetSchoolsBySearch = (search: string, country: string) => {
  return useQuery<ISchool[], Error>([SCHOOL_KEY, search, country], async () =>
    axios
      .get<ISchool[]>('api/schools', { params: { search, country } })
      .then(res => res.data)
  );
};
