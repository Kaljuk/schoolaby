import axios from 'axios';
import { Material } from 'app/shared/model/material.model';
import { MATERIALS_KEY } from 'app/config/reactQueryKeyConstants';
import { QueryObserverResult, useQuery } from 'react-query';
import { IEducationalAlignment } from 'app/shared/model/educational-alignment.model';
import { COUNTRY } from 'app/config/constants';

export const useGetPublicSchoolabyMaterials = (
  educationalAlignments: IEducationalAlignment[],
  page = 0
): QueryObserverResult<Material[], Error> =>
  useQuery<Material[], Error>(
    [MATERIALS_KEY, educationalAlignments],
    async () =>
      axios
        .get('/api/materials/suggested', {
          params: {
            educationalAlignmentIds: educationalAlignments.map(e => e.id).join(','),
            page,
          },
        })
        .then(res => res.data.map(Material.fromJson)),
    {
      enabled: !!educationalAlignments?.length,
    }
  );

export const useGetPublicSchoolabyMaterialsBySearchString = (
  searchString: string,
  isUserUkrainian = false
): QueryObserverResult<Material[], Error> =>
  useQuery<Material[], Error>(
    [MATERIALS_KEY, searchString, { type: 'public' }],
    async () =>
      axios
        .get('/api/materials/search', {
          params: {
            searchString,
            country: isUserUkrainian ? COUNTRY.UKRAINE : undefined,
          },
        })
        .then(res => res.data.map(Material.fromJson)),
    { enabled: isUserUkrainian || !!searchString }
  );
