import { useQuery } from 'react-query';
import { ICurriculum } from 'app/shared/model/curriculum.model';
import { CURRICULA_KEY } from 'app/config/reactQueryKeyConstants';
import axios from 'axios';
import { orderBy } from 'lodash';

export const useGetCurricula = (country: string) =>
  useQuery<ICurriculum[]>([CURRICULA_KEY, country], async () =>
    axios
      .get<ICurriculum[]>('api/curricula/', { params: { country } })
      .then(response => orderBy(response.data, ['sequenceNumber'], ['asc']))
  );
