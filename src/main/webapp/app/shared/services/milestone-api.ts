import axios from 'axios';
import { Material } from 'app/shared/model/material.model';
import { IMilestone } from 'app/shared/model/milestone.model';
import { JOURNEY_KEY, MILESTONE_KEY, MILESTONES_KEY } from 'app/config/reactQueryKeyConstants';
import { useMutation, useQuery, useQueryClient } from 'react-query';

export const useGetMilestone = (id: number | string, enabled = true, template?: boolean) =>
  useQuery<IMilestone, Error>(
    [MILESTONE_KEY, +id],
    async () =>
      axios
        .get<IMilestone>(`api/milestones/${id}`, {
          params: {
            template,
          },
        })
        .then(response => {
          response.data.materials = response.data.materials?.map(Material.fromJson);
          return response.data;
        }),
    { enabled }
  );

export const useGetMilestones = (journeyId: number | string, enabled = true, template?: boolean, sort = 'endDate') =>
  useQuery<IMilestone[], Error>(
    [MILESTONES_KEY, journeyId],
    async () =>
      axios
        .get<IMilestone[]>('api/milestones', {
          params: {
            journeyId,
            sort,
            template,
          },
        })
        .then(response => {
          response.data?.map(milestone => (milestone.materials = milestone.materials?.map(Material.fromJson)));
          return response.data;
        }),
    { enabled }
  );

export const useCreateMilestone = () => {
  const queryClient = useQueryClient();
  return useMutation(async (milestone: IMilestone) => axios.post('api/milestones/', milestone), {
    onSuccess() {
      queryClient.invalidateQueries(JOURNEY_KEY);
    },
  });
};

export const useUpdateMilestone = (milestoneId: number) => {
  const queryClient = useQueryClient();
  return useMutation(async (milestone: IMilestone) => axios.put('api/milestones/', milestone), {
    onSuccess() {
      queryClient.invalidateQueries(JOURNEY_KEY);
      queryClient.invalidateQueries([MILESTONE_KEY, milestoneId]);
    },
  });
};

export const useDeleteMilestone = (id: number) => {
  const queryClient = useQueryClient();
  return useMutation(async () => axios.delete(`api/milestones/${id}`), {
    onSuccess() {
      queryClient.invalidateQueries(JOURNEY_KEY);
    },
  });
};
