import axios from 'axios';
import { IAuditEvent } from 'app/shared/model/auditEvent.model';

export const getAuditEventsByJourneyId = (journeyId: number) => {
  return axios.get<IAuditEvent[]>(`management/audits?journeyId=${journeyId}`);
};
