import { IRubric } from 'app/shared/model/rubric/rubric.model';
import axios from 'axios';
import { RUBRIC_TEMPLATES_KEY, RUBRICS_KEY } from 'app/config/reactQueryKeyConstants';
import { useQuery, useMutation } from 'react-query';

export const useGetTemplates = (search: string) => {
  return useQuery<IRubric[], Error>([RUBRIC_TEMPLATES_KEY, search], async () =>
    axios
      .get<IRubric[]>('api/rubrics/templates', { params: { search } })
      .then(res => res.data)
  );
};

export const useGetRubric = (assignmentId: number) => {
  return useQuery<IRubric, Error>(
    [RUBRICS_KEY, assignmentId],
    async () =>
      axios
        .get<IRubric>('api/rubrics/', { params: { assignmentId } })
        .then(res => (typeof res.data !== 'string' ? res.data : undefined)),
    { enabled: !!assignmentId }
  );
};

export const usePostRubric = () => {
  return useMutation((rubric: IRubric) => axios.post('api/rubrics', rubric), {});
};

export const useUpdateRubric = (onSuccess?: () => void) => {
  return useMutation((rubric: IRubric) => axios.put('api/rubrics', rubric), {
    onSuccess,
  });
};

export const useDeleteRubric = (onSuccess?: () => void) => {
  return useMutation((id: number) => axios.delete(`api/rubrics/${id}`), {
    onSuccess,
  });
};
