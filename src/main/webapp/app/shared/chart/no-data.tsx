import Icon from 'app/shared/icons';
import { ICON_GREY } from 'app/shared/util/color-utils';
import React from 'react';
import { translate } from 'react-jhipster';

type IProps = {
  show: boolean;
};

export const NoData = ({ show }: IProps) =>
  show ? (
    <span className={'pb-5 pt-3 d-flex align-items-center justify-content-center'}>
      <h2 className={'text-muted'}>
        <Icon name={'exclamationMark'} width={'24px'} height={'24px'} className={'mr-3 mb-2'} stroke={ICON_GREY} />
        {translate('schoolabyApp.assignment.analytics.barChart.noData')}
      </h2>
    </span>
  ) : (
    <></>
  );
