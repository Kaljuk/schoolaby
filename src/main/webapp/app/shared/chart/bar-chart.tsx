import React, { useEffect, useRef } from 'react';
import * as d3 from 'd3';
import { Col, Row } from 'reactstrap';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { DataPoint } from 'app/assignment/assignment-detail/analytics/assignment-analytics';
import { D3Chart } from 'app/shared/chart/d3-chart';

interface IBarChart extends StateProps {
  data: DataPoint[];
  colors: string[];
  barWidth: number;
}

const BarChart = ({ data, language, colors, barWidth }: IBarChart) => {
  const svgRef = useRef<SVGSVGElement>();
  const containerRef = useRef<HTMLDivElement>();

  const renderChart = () => {
    const svg = d3.select(svgRef.current);
    const margin = { left: 40, right: 0, top: 20, bottom: 10 };
    const pointCount = data.length;
    const bounds = containerRef.current.getBoundingClientRect();
    const height = bounds.height - margin.top - margin.bottom;
    const xAxisLegendHeight = 15;
    const xValues = data.map(entryPoint => entryPoint.key);
    const maxY = data.reduce((max, item) => (item.value > max ? item.value : max), 0);
    const stateColorScale = d3.scaleOrdinal(xValues).range(colors);

    let calculatedWidth;
    if (bounds.width < 800) {
      calculatedWidth = barWidth / 2;
    } else {
      calculatedWidth = barWidth;
    }

    const barPadding = 50;
    const xAxisPadding = 15;
    const paddingLeft = 0;
    const width = (calculatedWidth + barPadding) * pointCount;

    D3Chart.scrollable(svg, width, height);

    D3Chart.clearCanvas(svg);

    const canvas = D3Chart.createCanvas(svg, paddingLeft, margin, data);
    const xScale = D3Chart.createXScale(xValues, width);
    const yScale = D3Chart.createYScale(maxY, height, xAxisLegendHeight);

    D3Chart.createBars(canvas, calculatedWidth, barPadding, yScale, height, xAxisLegendHeight, d => stateColorScale(d.key));
    D3Chart.createBarNumbers(canvas, calculatedWidth, barPadding, height, xAxisLegendHeight, yScale);
    D3Chart.createXAxis(svg, xScale, height, xAxisPadding, paddingLeft, xAxisLegendHeight).call(g => g.select('.domain').remove());
  };

  useEffect(() => {
    renderChart();
  }, [language]);

  useEffect(() => {
    if (svgRef.current) {
      renderChart();
      window.addEventListener('resize', renderChart);
    }
    return () => window.removeEventListener('resize', renderChart);
  }, [svgRef.current]);

  return (
    <Row style={{ minHeight: '400px' }}>
      <Col className={'svg-container mb-0 mx-3 mt-3'}>
        <div ref={containerRef}>
          <svg ref={svgRef} height={'400px'} />
        </div>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ locale }: IRootState) => ({
  language: locale.currentLocale,
});
type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(BarChart);
