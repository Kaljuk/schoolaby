import React, { useEffect, useRef } from 'react';
import * as d3 from 'd3';
import { Col, Row } from 'reactstrap';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { GREEN, RED } from 'app/shared/util/color-utils';
import { DataPoint } from 'app/assignment/assignment-detail/analytics/assignment-analytics';
import { DateTime } from 'luxon';
import { D3Chart } from 'app/shared/chart/d3-chart';

interface IDateBarChart extends StateProps {
  data: DataPoint[];
  barWidth: number;
  barPadding: number;
  deadline: any;
}

const DateBarChart = ({ data, language, barWidth, deadline, barPadding }: IDateBarChart) => {
  const svgRef = useRef<SVGSVGElement>();

  const renderChart = () => {
    const svg = d3.select(svgRef.current);
    const margin = { left: 40, right: 0, top: 20, bottom: 60 };
    const pointCount = data.length;
    const bounds = D3Chart.getBounds(svg);
    const width = (barWidth + barPadding) * pointCount;
    const height = bounds.height - margin.top - margin.bottom;
    const xAxisPadding = 15;
    const xAxisLegendHeight = 20;
    const xValues = data.map(entryPoint => entryPoint.key);
    const maxY = data.reduce((max, item) => (item.value > max ? item.value : max), 0);

    let paddingLeft = 0;

    if (width > bounds.width) {
      D3Chart.scrollable(svg, width, height);
    } else {
      paddingLeft = D3Chart.getCenterAlignedPadding(bounds, barWidth, barPadding, pointCount);
    }

    D3Chart.clearCanvas(svg);

    const canvas = D3Chart.createCanvas(svg, paddingLeft, margin, data);
    const xScale = D3Chart.createXScale(xValues, width);
    const yScale = D3Chart.createYScale(maxY, height, xAxisLegendHeight);

    D3Chart.createBars(canvas, barWidth, barPadding, yScale, height, xAxisLegendHeight, d => {
      return DateTime.fromFormat(d.key, 'dd.MM.yyyy') > DateTime.fromISO(deadline) ? RED : GREEN;
    });
    D3Chart.createBarNumbers(canvas, barWidth, barPadding, height, xAxisLegendHeight, yScale);
    D3Chart.createXAxis(svg, xScale, height, xAxisPadding, paddingLeft, xAxisLegendHeight);
    D3Chart.dottedXAxisStyling(svg, data);
  };

  useEffect(() => {
    renderChart();
  }, [language]);

  useEffect(() => {
    if (svgRef.current) {
      renderChart();
      window.addEventListener('resize', renderChart);
    }
    return () => window.removeEventListener('resize', renderChart);
  }, [svgRef.current]);

  return (
    <Row style={{ minHeight: '400px' }}>
      <Col className={'svg-container mb-0 mx-3 mt-3'}>
        <svg ref={svgRef} height={'400px'} />
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ locale }: IRootState) => ({
  language: locale.currentLocale,
});
type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(DateBarChart);
