import './horizontal-radio-button.scss';
import React, { FC } from 'react';
import { ButtonProps } from 'reactstrap';

interface Props extends ButtonProps {
  children: React.ReactNode;
  className?: string;
}

const HorizontalRadio: FC<Props> = ({ children, className }) => {
  return <div className={`horizontal-radio ${className || ''}`}>{children}</div>;
};
export default HorizontalRadio;
