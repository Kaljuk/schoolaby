import React from 'react';
import Icon from 'app/shared/icons';
import { UncontrolledTooltip } from 'reactstrap';
import { translate } from 'react-jhipster';

const LtiWarningIcon = () => (
  <span className={'tabs-lti-warning'}>
    <UncontrolledTooltip placement="left" target="tabs-lti-warning">
      {translate('global.overlayWarning.ltiNotConfiguredWarning')}
    </UncontrolledTooltip>

    <span id="tabs-lti-warning">
      <Icon className={'position-absolute ml-auto icon'} name={'exclamationMark'} width={'20px'} height={'20px'} />
    </span>
  </span>
);

export default LtiWarningIcon;
