import React from 'react';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';
import { translate } from 'react-jhipster';

const MyJourneysBreadcrumbItem = () => <BreadcrumbsItem to="/journey">{translate('global.menu.journeys')}</BreadcrumbsItem>;

export default MyJourneysBreadcrumbItem;
