import React from 'react';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';
import { translate } from 'react-jhipster';

const AppsBreadcrumbItem = ({ journeyId }) => (
  <BreadcrumbsItem to={`/journey/${journeyId}/apps`}>{translate('schoolabyApp.journey.detail.tabs.apps')}</BreadcrumbsItem>
);

export default AppsBreadcrumbItem;
