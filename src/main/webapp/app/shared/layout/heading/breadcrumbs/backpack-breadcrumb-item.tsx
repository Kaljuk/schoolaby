import React from 'react';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';
import { translate } from 'react-jhipster';

const BackpackBreadcrumbItem = ({ assignmentId, journeyId }) => (
  <BreadcrumbsItem to={`/assignment/${assignmentId}/backpack?journeyId=${journeyId}`}>
    {translate('schoolabyApp.assignment.detail.tabs.backpack')}
  </BreadcrumbsItem>
);

export default BackpackBreadcrumbItem;
