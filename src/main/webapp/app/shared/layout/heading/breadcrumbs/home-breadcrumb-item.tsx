import React from 'react';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';
import Icon from 'app/shared/icons';
import { ICON_GREY_2 } from 'app/shared/util/color-utils';
import { translate } from 'react-jhipster';

export const HomeBreadcrumbItem = () => (
  <BreadcrumbsItem to={`/`}>
    <Icon ariaLabel={translate('global.home')} name={'homeNew'} fill={ICON_GREY_2} />
  </BreadcrumbsItem>
);
