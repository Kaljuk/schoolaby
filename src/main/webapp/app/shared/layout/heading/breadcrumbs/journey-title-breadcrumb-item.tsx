import React from 'react';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';

const JourneyTitleBreadcrumbItem = ({ journeyId, journeyTitle }) => (
  <BreadcrumbsItem to={`/journey/${journeyId}`}>{journeyTitle}</BreadcrumbsItem>
);

export default JourneyTitleBreadcrumbItem;
