import React from 'react';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';
import { translate } from 'react-jhipster';

const StudentsBreadcrumbItem = ({ journeyId }) => (
  <BreadcrumbsItem to={`/journey/${journeyId}/students`}>{translate('global.heading.tabs.students')}</BreadcrumbsItem>
);

export default StudentsBreadcrumbItem;
