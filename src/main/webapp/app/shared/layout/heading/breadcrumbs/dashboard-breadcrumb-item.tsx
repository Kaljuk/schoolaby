import React from 'react';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';
import { translate } from 'react-jhipster';

const DashboardBreadcrumbItem = () => <BreadcrumbsItem to={'/'}>{translate('global.menu.dashboard')}</BreadcrumbsItem>;

export default DashboardBreadcrumbItem;
