import React from 'react';
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic';

const AssignmentTitleBreadcrumbItem = ({ journeyId, assignmentId, assignmentTitle }) => {
  return <BreadcrumbsItem to={`/assignment/${assignmentId}?journeyId=${journeyId}`}>{assignmentTitle}</BreadcrumbsItem>;
};

export default AssignmentTitleBreadcrumbItem;
