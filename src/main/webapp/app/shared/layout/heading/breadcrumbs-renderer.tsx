import './breadcrumbs-renderer.scss';
import React from 'react';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import Icon from 'app/shared/icons';
import { Link } from 'react-router-dom';
import { LIGHT_ICON } from 'app/shared/util/color-utils';
import { Breadcrumbs } from 'react-breadcrumbs-dynamic';

interface IProps {
  children: string;
  href: string;
  active: boolean;
}

const LocationBasedBreadcrumbItem = ({ href, children, active }: IProps) => (
  <BreadcrumbItem active={active} className={'d-flex align-items-center border rounded mr-3 pl-0'}>
    <Link to={href || ''} className={'text-muted px-3'}>
      {children}
    </Link>
  </BreadcrumbItem>
);

const BreadcrumbsRenderer = () => (
  <Breadcrumb>
    <span>
      <Icon name={'homeThin'} fill={LIGHT_ICON} className={'mr-3'} />
    </span>
    <Breadcrumbs renameProps={{ to: 'href' }} finalProps={{ active: true }} item={LocationBasedBreadcrumbItem} />
  </Breadcrumb>
);

export default BreadcrumbsRenderer;
