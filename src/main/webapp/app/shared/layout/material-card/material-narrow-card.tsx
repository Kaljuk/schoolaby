import './material-narrow-card.scss';
import React, { FC, useEffect, useState } from 'react';
import { Card, CardBody, CardImg, Col, Row } from 'reactstrap';
import { APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { TextFormat, translate } from 'react-jhipster';
import { E_KOOLIKOTT_MATERIAL, Material, OPIQ_MATERIAL } from 'app/shared/model/material.model';
import MaterialCardModal from 'app/shared/layout/material-card/material-card-modal';
import sanitizeHtml from 'sanitize-html';
import Truncate from 'react-truncate';
import seedrandom from 'seedrandom';
import OwnedByUserIcon from 'app/shared/layout/owned-by-user-icon/owned-by-user-icon';
import { connect } from 'react-redux';
import { IRootState } from 'app/shared/reducers';

interface IProps extends StateProps {
  material: Material;
  provided?: any;
  add?: boolean;
  remove?: boolean;
  tooltipId?: string;
  onAdd?: () => void;
  sequenced?: boolean;
}

const MaterialNarrowCard: FC<IProps> = ({ material, provided, tooltipId, add, remove, onAdd, sequenced, login }: IProps) => {
  const [imageUrl, setImageUrl] = useState('');
  const [created, setCreated] = useState(material.created);
  const [title, setTitle] = useState(material.title);
  const [modal, setModal] = useState(false);
  const toggleModal = () => setModal(!modal);
  const random = seedrandom(title);

  const getRandomImage = () => {
    random() < 0.5 ? setImageUrl('/content/images/material_green.png') : setImageUrl('/content/images/material_blue.png');
  };

  useEffect(() => {
    (async () => {
      if (material) {
        const _imageUrl = await material.getImageUrl();
        _imageUrl ? setImageUrl(_imageUrl) : getRandomImage();
        const _created = await material.getCreated();
        setCreated(_created);
        const _title = await material.getTitle();
        setTitle(_title);
      }
    })();
  }, [material]);

  const renderEkoolikottMaterialIcon = () => (
    <span className={'position-absolute rounded-circle d-flex justify-content-center align-items-center ekoolikott-material-card-icon'}>
      <img src={'content/images/ekoolikoti_logo.svg'} width={'30'} height={'30'} alt={'ekoolikott-icon'} />
    </span>
  );

  const renderOpiqMaterialIcon = () => (
    <img
      src={'content/images/opiqlogo.svg'}
      className={'position-absolute rounded-circle opiq-material-card-icon'}
      width={'60'}
      height={'60'}
      alt={'opiq-icon'}
    />
  );

  const renderIconByMaterial = () => {
    if (material.type === OPIQ_MATERIAL) {
      return renderOpiqMaterialIcon();
    } else if (material.type === E_KOOLIKOTT_MATERIAL) {
      return renderEkoolikottMaterialIcon();
    } else if (material.isManuallyAdded() && material.isOwnedBy(login)) {
      return <OwnedByUserIcon id={+tooltipId} />;
    } else {
      return null;
    }
  };

  return (
    <span ref={provided?.innerRef} {...provided?.draggableProps} {...provided?.dragHandleProps}>
      <Card data-testid={'material-card'} className="material-narrow-card h-100" onClick={onAdd ? onAdd : toggleModal}>
        <Row className="d-flex" noGutters>
          <Col xs="12" className={'position-relative'}>
            {renderIconByMaterial()}
            <CardImg top width="100%" src={imageUrl} />
          </Col>
        </Row>
        <Row className={'d-flex flex-grow-1'}>
          <Col xs="12" className="d-flex">
            <CardBody className="d-flex flex-column pb-0 pt-1">
              <Row noGutters>
                <Col xs="11">
                  <h5 className={'break-all'}>
                    <Truncate lines={1}>{title}</Truncate>
                  </h5>
                </Col>
                {material?.description && (
                  <Col xs="11">
                    <h6 className={'text-secondary font-weight-light break-all'}>
                      <Truncate lines={2}>{sanitizeHtml(material.description, { allowedTags: [] })}</Truncate>
                    </h6>
                  </Col>
                )}
              </Row>
              <Row className={'mb-3 material-footer d-flex align-items-end'} noGutters>
                <Col xs={3} className={'material-sequence'}>
                  {sequenced && material?.sequenceNumber >= 0 && material?.sequenceNumber != null && (
                    <span
                      className={'d-flex justify-content-center rounded'}
                      aria-label={translate('schoolabyApp.material.sequenceNumber')}
                    >
                      {material.sequenceNumber + 1}
                    </span>
                  )}
                </Col>
                {created && (
                  <Col xs={9} className="mt-auto text-light text-right">
                    {translate('schoolabyApp.material.createdAt')}
                    <b className="text-info font-weight-normal">
                      {' '}
                      <TextFormat type="date" value={created} format={APP_LOCAL_DATE_FORMAT} />
                    </b>
                  </Col>
                )}
              </Row>
            </CardBody>
          </Col>
        </Row>
      </Card>
      {modal && <MaterialCardModal material={material} addable={add} removable={remove} toggleModal={toggleModal} />}
    </span>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  login: authentication?.account?.login,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(MaterialNarrowCard);
