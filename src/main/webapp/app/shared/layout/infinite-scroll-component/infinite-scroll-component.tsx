import React, { memo } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { Spinner } from 'app/shared/layout/spinner/spinner';

interface IProps {
  dataLength: number;
  children: JSX.Element[];
  hasMore: boolean;
  next: () => void;
  scrollableTarget: string;
  endMessage?: JSX.Element;
  loader?: JSX.Element;
}

const InfiniteScrollComponent = ({ dataLength, children, next, hasMore, scrollableTarget, endMessage }: IProps) => {
  return (
    <InfiniteScroll
      dataLength={dataLength}
      next={next}
      hasMore={hasMore}
      scrollableTarget={scrollableTarget}
      loader={<Spinner />}
      endMessage={endMessage}
    >
      {children}
    </InfiniteScroll>
  );
};

export default memo(InfiniteScrollComponent);
