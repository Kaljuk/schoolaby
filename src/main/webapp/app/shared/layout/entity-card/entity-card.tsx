import './entity-card.scss';
import Icon, { getSubjectIcon } from 'app/shared/icons';
import React, { FC, useState } from 'react';
import { Card, Col, Dropdown, DropdownToggle, Row } from 'reactstrap';
import { APP_LOCAL_DATE_FORMAT, APP_LOCAL_DATE_TIME_FORMAT } from 'app/config/constants';
import { TextFormat, translate } from 'react-jhipster';
import LinesEllipsis from 'react-lines-ellipsis';
import { Link, useHistory } from 'react-router-dom';
import { ColorType, getColor, ICON_GREY, SECONDARY } from 'app/shared/util/color-utils';
import Avatar from '../header/avatar';
import ISubject from 'app/shared/model/subject.model';

export interface IEntityCardProps {
  entityId: number;
  title: string;
  deadline: string;
  color: ColorType;
  teacherName?: string;
  href?: any;
  studentCount?: number;
  dropdown?: JSX.Element;
  greyedOut?: boolean;
  style?: React.CSSProperties;
  className?: string;
  subject?: ISubject;
  isAssignment?: boolean;
}

const EntityCard: FC<IEntityCardProps> = (props: IEntityCardProps) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const toggleDropDown = () => setDropdownOpen(prevState => !prevState);
  const history = useHistory();

  const shouldShowStudentCount = () => {
    return props.studentCount || props.studentCount === 0;
  };

  const CardLeftContent = ({ subject }) => {
    return (
      <Col className={`card-left d-flex p-3 flex-column`}>
        <Row noGutters>
          <Col
            xs="12"
            className={`subject-icon d-flex mb-1 justify-content-center align-items-center pt-2 ${getColor(props.color, 'icon')}`}
          >
            {getSubjectIcon(subject ? subject?.label : '')}
          </Col>
          {/* {shouldShowStudentCount() && (*/}
          {/*  <Col xs={'12'} className={'d-flex justify-content-center align-items-end'}>*/}
          {/*    <h4 className={'mb-0 mt-2'}>11b</h4>*/}
          {/*  </Col>*/}
          {/* )}*/}
        </Row>
        <Row>
          <Col xs="12" className="d-flex justify-content-center align-items-start pt-3">
            <span className="text-center text-secondary subject">{subject ? translate(subject?.label) : ''}</span>
          </Col>
        </Row>
      </Col>
    );
  };

  const CardRightContent = () => {
    let cardFooter; // = <h6 className="text-left mb-0">11b</h6>;
    if (shouldShowStudentCount()) {
      cardFooter = (
        <div className={'d-flex align-items-center'}>
          <Icon name={'graduate'} fill={SECONDARY} stroke={'transparent'} />
          <span className={'text-muted ml-1'}>{props.studentCount}</span>
        </div>
      );
    }

    return (
      <Col className={'d-flex card-right flex-column px-3 pt-3 pb-3'}>
        <Row noGutters>
          <Col className={'entity-card-title'} xs={props.dropdown ? '10' : '12'}>
            <h4>
              <LinesEllipsis text={props.title} basedOn={'letters'} maxLine={2} />
            </h4>
          </Col>
          {props.dropdown && (
            <Col xs={'2'} className={'d-flex justify-content-end'}>
              <Dropdown
                onClick={event => {
                  event.stopPropagation();
                  event.preventDefault();
                }}
                isOpen={dropdownOpen}
                toggle={toggleDropDown}
                aria-label={translate('global.heading.dropdown')}
              >
                <DropdownToggle className={'p-0'}>
                  <Icon name={'dropdown'} width={'16px'} height={'16px'} stroke={ICON_GREY} fill={ICON_GREY} />
                </DropdownToggle>
                {props.dropdown}
              </Dropdown>
            </Col>
          )}
        </Row>
        {props.teacherName && (
          <Row noGutters>
            <Avatar fullName={props.teacherName} showName className={'mb-3'} />
          </Row>
        )}
        <Row noGutters className={'d-flex flex-grow-1 align-items-end'}>
          <Col xs={'2'} className={'justify-content-start'}>
            {/* TODO: generate class number in backend*/}
            {cardFooter}
          </Col>
          <Col className={'d-flex justify-content-end'}>
            <p className="text-right text-secondary mb-0">
              {translate('schoolabyApp.assignment.deadline')}
              <span className={`deadline ${getColor(props.color, 'text')}`}>
                &nbsp;
                <TextFormat
                  type="date"
                  value={props.deadline}
                  format={props.isAssignment ? APP_LOCAL_DATE_TIME_FORMAT : APP_LOCAL_DATE_FORMAT}
                />
              </span>
            </p>
          </Col>
        </Row>
      </Col>
    );
  };

  return (
    <Link to={props.href || ''} className={'link'}>
      <Card
        key={props.entityId}
        style={props.style}
        className={`entity-card h-100 ${props.greyedOut ? 'greyed-out' : ''} ${props.className} ${getColor(props.color, 'border')} rounded`}
        onClick={e => {
          e.preventDefault();
          history.push(props.href);
        }}
      >
        <Row className={'h-100 d-flex justify-content-center'} noGutters>
          <CardLeftContent subject={props.subject} />
          <CardRightContent />
        </Row>
      </Card>
    </Link>
  );
};

export default React.memo(EntityCard);
