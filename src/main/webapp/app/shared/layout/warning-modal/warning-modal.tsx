import React, { FC } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { translate } from 'react-jhipster';

type IWarningModal = {
  showModal: boolean;
  setShowModal: (boolean) => void;
  headerText: string;
  bodyText: string;
  handleConfirmation: () => void;
};

const WarningModal: FC<IWarningModal> = ({ headerText, bodyText, showModal, setShowModal, handleConfirmation }) => {
  const toggleModal = () => setShowModal(!showModal);

  return (
    <Modal centered isOpen={showModal} toggle={toggleModal}>
      <ModalHeader className={'border-0'}>{headerText}</ModalHeader>
      <ModalBody>
        <p>{bodyText}</p>
      </ModalBody>
      <ModalFooter className={'border-0'}>
        <Button color="danger" onClick={toggleModal}>
          {translate('entity.action.cancel')}
        </Button>
        <Button onClick={handleConfirmation} color="primary">
          {translate('global.yes')}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default WarningModal;
