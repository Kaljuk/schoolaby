import React from 'react';
import JourneyTemplateSelection from 'app/shared/layout/journey-template/journey-template-selection/journey-template-selection';
import JourneyTemplatePreview from 'app/shared/layout/journey-template/journey-template-preview/journey-template-preview';
import './journey-template.scss';

const JourneyTemplate = () => {
  return (
    <div className="journey-template d-flex">
      <JourneyTemplateSelection />
      <JourneyTemplatePreview />
    </div>
  );
};

export default JourneyTemplate;
