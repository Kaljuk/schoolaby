import React, { FC } from 'react';
import { IJourney } from 'app/shared/model/journey.model';
import Icon, { getSubjectIcon } from 'app/shared/icons';

import './journey-template-card.scss';
import { translate } from 'react-jhipster';
import { useSelectedJourneyTemplateState } from 'app/shared/contexts/journey-template-context';

interface JourneyTemplateCardProps {
  journey: IJourney;
}

const JourneyTemplateCard: FC<JourneyTemplateCardProps> = ({ journey }) => {
  const { selectedTemplateId, setSelectedTemplateId } = useSelectedJourneyTemplateState();

  const getTranslationKey = postfix => {
    const translationPathPrefix = 'schoolabyApp.journey.templates.';
    return translate(translationPathPrefix + postfix);
  };

  const Duration = () => {
    return (
      <>
        {journey.yearsCount > 0 && (
          <>
            <span className="ml-1 font-weight-bold">{journey.yearsCount} </span>
            <span>{getTranslationKey(journey.yearsCount === 1 ? 'year' : 'years')}</span>
          </>
        )}
        {journey.monthsCount > 0 && (
          <>
            <span className="ml-1 font-weight-bold">{journey.monthsCount} </span>
            <span>{getTranslationKey(journey.monthsCount === 1 ? 'month' : 'months')}</span>
          </>
        )}
        <span className="ml-1 font-weight-bold">{journey.daysCount} </span>
        <span>{getTranslationKey(journey.daysCount === 1 ? 'day' : 'days')}</span>
      </>
    );
  };

  return (
    <div
      className={`journey-template-card d-flex p-2 my-3 ${selectedTemplateId === journey.id ? 'selected-card' : ''}`}
      onClick={() => setSelectedTemplateId(journey.id)}
    >
      <div className="pr-2">
        <div className="template-icon d-flex justify-content-center align-items-center">
          {getSubjectIcon(journey?.subject?.label, '12', '12', 'white')}
        </div>
      </div>
      <div className="pl-1">
        <h5 className="mb-0">{journey.title}</h5>
        <div>
          <Icon width="14" height="14" name="coloredUser" />
          <span className="ml-1 font-weight-bold">{journey.teacherName}</span>
        </div>
        <div>
          <Icon width="14" height="14" name="coloredTime" />
          <Duration />
        </div>
        <div>
          <Icon width="14" height="14" name="coloredTasks" />
          <span className="ml-1 font-weight-bold">{journey.assignmentsCount} </span>
          <span>{getTranslationKey(journey.assignmentsCount === 1 ? 'assignment' : 'assignments')}</span>
        </div>
        <div>
          <Icon width="14" height="14" name="coloredHat" />
          <span className="ml-1 font-weight-bold">{journey.educationalLevel.name}</span>
        </div>
      </div>
    </div>
  );
};

export default JourneyTemplateCard;
