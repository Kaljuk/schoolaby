import React, { useMemo } from 'react';
import { translate } from 'react-jhipster';
import { useSelectedJourneyTemplateState } from 'app/shared/contexts/journey-template-context';
import JourneyDetailOld from 'app/journey/journey-detail/journey-detail-old';
import './journey-template-preview.scss';

const JourneyTemplatePreview = () => {
  const { selectedTemplateId } = useSelectedJourneyTemplateState();

  const PreviewInstructions = () => (
    <div className="preview-instructions">
      <h4 className="text-center mb-5">{translate('schoolabyApp.journey.templates.previewInstructions')}</h4>
      <img alt="Journey template cover" src="/content/images/journey/journey-template-cover.svg" />
    </div>
  );

  return useMemo(
    () =>
      selectedTemplateId ? (
        <div className="journey-template-preview position-relative">
          <JourneyDetailOld previewEnabled previewJourneyId={selectedTemplateId} match={null} history={null} location={null} />
        </div>
      ) : (
        <div className="journey-template-preview d-flex justify-content-center">
          <PreviewInstructions />
        </div>
      ),
    [selectedTemplateId]
  );
};

export default JourneyTemplatePreview;
