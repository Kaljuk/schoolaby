import React, { FC } from 'react';
import Heading, { ITabParams, PreviewTabType } from 'app/shared/layout/heading/heading';
import AssignmentInfo from 'app/assignment/assignment-detail/assignment-info';
import { translate } from 'react-jhipster';
import { useGetRubric } from 'app/shared/services/rubric-api';
import Icon from 'app/shared/icons';
import { Button, Col, Row } from 'reactstrap';
import { hasMaterials } from 'app/assignment/assignment-detail/assignment-util';
import AssignmentMaterials from 'app/assignment/assignment-detail/assignment-materials';
import { useGetAssignment } from 'app/shared/services/assignment-api';
import { useGetLtiResources } from 'app/shared/services/lti-api';
import Backpack from 'app/assignment/assignment-detail/backpack/backpack';
import RubricOverview from 'app/assignment/assessment-rubric-new/rubric-overview/rubric-overview';
import { useSelectedRubricState } from 'app/shared/contexts/entity-update-context';
import RubricViewModes from 'app/assignment/assessment-rubric-new/rubric-overview/rubric-view-modes';

export interface IAssignmentPreviewProps {
  assignmentId: number;
  onBackClicked: () => void;
  openedPreviewTab: PreviewTabType;
  setOpenedPreviewTab: (tab: PreviewTabType) => void;
}

const AssignmentPreview: FC<IAssignmentPreviewProps> = ({ assignmentId, onBackClicked, openedPreviewTab, setOpenedPreviewTab }) => {
  const { data: assignment } = useGetAssignment(assignmentId, true, { template: true });
  const { data: rubric } = useGetRubric(assignmentId);
  const { selectedRubric, setSelectedRubric } = useSelectedRubricState();
  const { data: ltiResources } = useGetLtiResources({ assignmentId });
  const assignmentTabParams: ITabParams = {
    entityType: 'ASSIGNMENT',
    tabEntityId: assignmentId,
  };

  if (openedPreviewTab === 'ASSIGNMENT_BACKPACK') {
    const routeComponentProps = {
      match: null,
      history: null,
      location: null,
    };

    return (
      <Backpack
        previewEnabled
        assignmentId={assignmentId}
        openedPreviewTab={openedPreviewTab}
        setOpenedPreviewTab={setOpenedPreviewTab}
        onBackClicked={onBackClicked}
        {...routeComponentProps}
      />
    );
  }

  return (
    <div className="pb-3">
      <Heading
        title={translate('schoolabyApp.assignment.detail.title')}
        tabParams={assignmentTabParams}
        previewEnabled
        onBackClicked={onBackClicked}
        openedPreviewTab={openedPreviewTab}
        setOpenedPreviewTab={setOpenedPreviewTab}
      />
      <div className="mx-5">
        <AssignmentInfo assignmentId={assignmentId} showHiddenFields={false} previewEnabled />
        {!!rubric && (
          <Button type="button" color="primary" className="d-inline-block mb-4" onClick={() => setSelectedRubric(rubric)}>
            <Icon name="preferences" stroke="white" className="btn-icon" width="20px" height="20px" viewBox="0 0 25 20" />
            <span className="text-center pl-2">{translate('schoolabyApp.assignment.rubric.viewTitle')}</span>
          </Button>
        )}
        <Row noGutters>
          <h3>{translate('schoolabyApp.assignment.detail.assets')}</h3>
          <Col md={'12'} className={'p-0 materials-col'}>
            <Row noGutters className={'d-flex'}>
              {hasMaterials(assignment) || ltiResources?.length ? (
                <AssignmentMaterials assignment={assignment} />
              ) : (
                <p>{translate('schoolabyApp.assignment.detail.noAssets')}</p>
              )}
            </Row>
          </Col>
        </Row>
      </div>
      {!!selectedRubric && <RubricOverview viewMode={RubricViewModes.VIEW} />}
    </div>
  );
};

export default AssignmentPreview;
