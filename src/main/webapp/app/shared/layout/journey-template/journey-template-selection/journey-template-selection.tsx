import React, { useState } from 'react';
import { translate } from 'react-jhipster';
import Icon from 'app/shared/icons';
import { SBY_GRAY } from 'app/shared/util/color-utils';

import './journey-template-selection.scss';
import { useSelectedJourneyTemplateState, useShowJourneyTemplatesViewState } from 'app/shared/contexts/journey-template-context';
import { Button } from 'reactstrap';
import ConfirmationModalNew from 'app/shared/layout/confirmation-modal-new/confirmation-modal-new';
import { useHistory } from 'react-router-dom';
import TemplateList from 'app/shared/layout/journey-template/template-list/template-list';

const JourneyTemplateSelection = () => {
  const [search, setSearch] = useState<string>('');
  const [showingWarningModal, setShowingWarningModal] = useState<boolean>(false);
  const { setShowTemplatesView } = useShowJourneyTemplatesViewState();
  const { selectedTemplateId } = useSelectedJourneyTemplateState();
  const history = useHistory();

  const onSelectClicked = () => {
    !!selectedTemplateId && history.push(`/journey/new?template=${selectedTemplateId}`);
  };

  return (
    <div className="journey-template-selection d-flex flex-column bg-white py-3 px-4">
      <h1>{translate('schoolabyApp.journey.templates.title')}</h1>
      <p className={'my-3'}>{translate('schoolabyApp.journey.templates.description')}</p>
      <div className="journey-template-search w-100 my-4 position-relative">
        <input className="w-100 py-2 pl-3" placeholder={translate('global.search')} onChange={event => setSearch(event.target.value)} />
        <Icon name={'search'} className="search-icon position-absolute" width="20" height="21" fill={SBY_GRAY} />
      </div>
      <TemplateList search={search} />
      <div className="controls justify-self-end d-flex justify-content-between pb-3 pt-4">
        <Button type="button" className="cancel-btn px-4 py-2" onClick={() => setShowingWarningModal(true)}>
          {translate('entity.action.back')}
        </Button>
        <Button type="button" className="btn-primary border-0 px-4 py-2" onClick={onSelectClicked} disabled={!selectedTemplateId}>
          {translate('entity.action.select')}
        </Button>
      </div>
      {showingWarningModal ? (
        <ConfirmationModalNew
          isModalOpen={showingWarningModal}
          closeModal={() => setShowingWarningModal(false)}
          handleConfirmation={() => setShowTemplatesView(false)}
          modalTitle={translate('schoolabyApp.journey.warningModal.title')}
        >
          <p>{translate('schoolabyApp.journey.warningModal.description')}</p>
        </ConfirmationModalNew>
      ) : null}
    </div>
  );
};

export default JourneyTemplateSelection;
