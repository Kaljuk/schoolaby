import React, { FC, useEffect, useMemo, useState } from 'react';
import './school-search-dropdown.scss';
import { useGetSchoolsBySearch } from 'app/shared/services/school-api';
import { ISchool } from 'app/shared/model/school.model';
import useConstant from 'use-constant';
import debounce from 'lodash/debounce';
import { FieldRenderProps, useForm } from 'react-final-form';
import { translate } from 'react-jhipster';
import { INFO } from 'app/shared/util/color-utils';
import AsyncSelect from 'react-select/async';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button, FormFeedback } from 'reactstrap';
import '../multiselect-search-dropdown/multiselect-search-dropdown.scss';

interface SchoolSearchDropdownProps extends FieldRenderProps<any, any> {
  country: string;
  selectedRole?: string;
  removePersonRole: () => void;
  selectSchool?: (school: ISchool) => void;
}

interface IDropdownItem {
  label?: string;
  value?: number;
  data?: ISchool;
}

const SchoolSearchDropdown: FC<SchoolSearchDropdownProps> = ({ input, meta, country, selectSchool, removePersonRole }) => {
  const [searchString, setSearchString] = useState<string>('');
  const [dropdownValue, setDropdownValue] = useState<IDropdownItem>({ label: undefined, value: undefined, data: {} });
  const { refetch: refetchSchools } = useGetSchoolsBySearch(searchString, country);
  const isInvalid = meta?.touched && meta?.error;
  const form = useForm();

  const mapToDropdownItem = (school: ISchool): IDropdownItem => ({
    label: school?.name,
    value: school?.id,
    data: school,
  });

  useEffect(() => {
    setDropdownValue(mapToDropdownItem(input?.value));
  }, [input]);

  const loadOptions = (inputValue, callback) => {
    refetchSchools().then(res => {
      callback(res.data?.map(school => mapToDropdownItem(school)));
    });
  };

  const handleInputChange = (inputValue: string) => {
    setSearchString(inputValue);
  };

  const memoizedLoadOptions = useConstant(() => debounce(loadOptions, 100));

  return useMemo(
    () => (
      <div className={`multiselect-search-dropdown`}>
        <div className={'input-search position-relative'}>
          <AsyncSelect
            key={JSON.stringify(country)}
            classNamePrefix={`${isInvalid ? 'async-select-invalid' : 'async-select'}`}
            className={'async-select'}
            value={dropdownValue?.label ? dropdownValue : null}
            noOptionsMessage={() => translate('entity.action.noResults')}
            placeholder={translate('settings.form.searchSchool')}
            loadOptions={memoizedLoadOptions}
            onInputChange={handleInputChange}
            onChange={(dropdownItem: IDropdownItem) => {
              selectSchool(dropdownItem.data);
              setDropdownValue(dropdownItem);
            }}
            defaultOptions
          />
          <FontAwesomeIcon className={'position-absolute search-icon'} icon={['fas', 'search']} color={INFO} size={'lg'} />
          {form.getState().values.personRoles?.length > 1 && (
            <Button
              color="danger"
              aria-label={translate('entity.remove.school')}
              className={'rounded-circle btn-subtract text-center ml-3'}
              onClick={removePersonRole}
              outline
            >
              -
            </Button>
          )}
        </div>
        {isInvalid && <FormFeedback className={'form-feedback'}>{translate('settings.messages.validate.school.required')}</FormFeedback>}
      </div>
    ),
    [input, meta, country, dropdownValue]
  );
};

export default SchoolSearchDropdown;
