import React, { FC, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useGetEducationalAlignmentsByTitle } from 'app/shared/services/educational-alignment-api';
import debounce from 'lodash/debounce';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import Input from 'app/shared/form/components/input';

import './educational-alignment-multiselect.scss';
import { useEntityToUpdateState, useSelectedEducationalAlignmentsState } from 'app/shared/contexts/entity-update-context';
import { translate } from 'react-jhipster';
import Tag from 'app/shared/layout/tag/tag';
import { FieldRenderProps } from 'react-final-form';
import { EVENT_CODE_ARROW_DOWN, EVENT_CODE_ARROW_UP, EVENT_CODE_ENTER } from 'app/config/constants';
import {
  educationalAlignmentsDifferenceByTitle,
  handleDuplicateEducationalAlignments,
  IDropdownItem,
  mapToDropdownItem,
} from 'app/shared/util/educational-alignment-utils';

interface IProps extends FieldRenderProps<any, any>, StateProps {}

const EducationalAlignmentMultiselect: FC<IProps> = ({ input, meta, disabled, country }) => {
  const selectRef = useRef(null);
  const [searchString, setSearchString] = useState<string>('');
  const [currentSearchString, setCurrentSearchString] = useState<string>('');
  const [shouldRefetch, setShouldRefetch] = useState<boolean>(false);
  const [dropdownItems, setDropdownItems] = useState<IDropdownItem[]>([]);
  const [dropdownFocused, setDropdownFocused] = useState<boolean>(false);
  const { entity } = useEntityToUpdateState();
  const {
    setSelectedEducationalAlignments,
    selectedEducationalAlignments,
    addSelectedEducationAlignment,
    removeSelectedEducationalAlignment,
  } = useSelectedEducationalAlignmentsState();
  const { refetch: refetchEducationalAlignments, isRefetching } = useGetEducationalAlignmentsByTitle(currentSearchString, country);
  const [currentAlignmentPosition, setCurrentAlignmentPosition] = useState<number>(0);

  useEffect(() => {
    input?.value?.length && !entity && setSelectedEducationalAlignments(handleDuplicateEducationalAlignments(input.value));
  }, [input?.value, entity]);

  const loadOptions = () => {
    refetchEducationalAlignments().then(res => {
      const results = handleDuplicateEducationalAlignments(res.data);
      const filteredResults = educationalAlignmentsDifferenceByTitle(results, selectedEducationalAlignments);
      setDropdownItems(filteredResults.map(result => mapToDropdownItem(result)));
    });
  };

  const debounceCurrentSearch = useCallback(
    debounce(value => setCurrentSearchString(value), 200),
    []
  );

  useEffect(() => {
    debounceCurrentSearch(searchString);
  }, [searchString]);

  useEffect(() => {
    !!currentSearchString.trim() && loadOptions();
    setShouldRefetch(false);
  }, [currentSearchString, shouldRefetch]);

  const handleSelect = data => {
    addSelectedEducationAlignment(data);
    setDropdownItems(prevState => [...prevState.filter(item => item.data !== data)]);
  };

  const handleRemove = alignment => {
    removeSelectedEducationalAlignment(alignment);
    !!currentSearchString.trim() && setShouldRefetch(true);
  };

  const handleKeyboardNavigation = (e: KeyboardEvent) => {
    if (e.key === EVENT_CODE_ARROW_DOWN) {
      e.preventDefault();
      if (dropdownItems?.length - 1 !== currentAlignmentPosition) {
        setCurrentAlignmentPosition(currentAlignmentPosition + 1);
      }
    }
    if (e.key === EVENT_CODE_ARROW_UP) {
      e.preventDefault();
      if (currentAlignmentPosition !== 0) {
        setCurrentAlignmentPosition(currentAlignmentPosition - 1);
      }
    }
    if (e.key === EVENT_CODE_ENTER) {
      e.preventDefault();
      if (dropdownItems?.[currentAlignmentPosition] !== null) {
        handleSelect(dropdownItems[currentAlignmentPosition].data);
        setCurrentAlignmentPosition(0);
      }
    }
  };

  useEffect(() => {
    selectRef?.current?.scrollTo(0, 29 * currentAlignmentPosition);
  }, [currentAlignmentPosition]);

  useEffect(() => {
    setCurrentAlignmentPosition(0);
  }, [dropdownFocused, currentSearchString]);

  return (
    <div className={'educational-alignment-multiselect'}>
      <Input
        input={input}
        meta={meta}
        onKeyDown={handleKeyboardNavigation}
        iconName={'search'}
        type={'text'}
        label={translate('schoolabyApp.educationalAlignment.detail.title')}
        placeholder={translate('schoolabyApp.educationalAlignment.detail.searchFromNationalCurriculum')}
        autoComplete={'off'}
        value={searchString}
        disabled={disabled}
        onChange={e => setSearchString(e.target.value)}
        onFocus={() => setDropdownFocused(true)}
        onBlur={() => {
          setDropdownFocused(false);
          input.onBlur();
        }}
      />
      <div className={`select-container position-relative ${currentSearchString.trim() && dropdownFocused ? 'select-visible' : ''}`}>
        <div className={'select'} ref={selectRef} role={'select'}>
          {currentSearchString.trim() &&
            dropdownItems?.map((item, index) => (
              <div
                key={`alignment-option-${item.value}`}
                className={`option cursor-pointer${currentAlignmentPosition === index ? ' selected' : ''}`}
                onMouseDown={e => e.preventDefault()}
                onClick={() => handleSelect(item.data)}
                role={'option'}
              >
                {item.label}
              </div>
            ))}
        </div>
        {useMemo(
          () =>
            !dropdownItems.length &&
            !isRefetching &&
            dropdownFocused &&
            currentSearchString.trim() && (
              <span className={'no-content position-absolute center-vertically'}>{translate('entity.action.noResults')}</span>
            ),
          [dropdownItems, dropdownFocused]
        )}
      </div>
      {!!selectedEducationalAlignments?.length && (
        <div className={'selected-alignments d-flex'}>
          {selectedEducationalAlignments.map(alignment => (
            <Tag key={`selected-alignment-${alignment.id}`} title={alignment.title} handleRemove={() => handleRemove(alignment)} />
          ))}
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  country: storeState.authentication.account.country,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(EducationalAlignmentMultiselect);
