import React, { FC, ReactNode, useEffect } from 'react';
import { translate } from 'react-jhipster';
import { useGetSubjects } from 'app/shared/services/subject-api';
import { FieldRenderProps } from 'react-final-form';
import AsyncSelect from 'react-select/async';
import ISubject from 'app/shared/model/subject.model';
import { FormFeedback } from 'reactstrap';
import './subject-search-dropdown-new.scss';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import Icon from 'app/shared/icons';
import { StylesConfig } from 'react-select';

interface IProps extends FieldRenderProps<any, any>, StateProps {
  selectedSubject: ISubject;
  setSelectedSubject: (subject: ISubject) => void;
  disabled?: boolean;
  label?: string;
  containerClassName?: string;
  customStyles?: StylesConfig<unknown, false>;
  placeholder?: string;
  customIcon?: ReactNode;
}

interface IDropdownItem {
  label?: string;
  value?: number;
  data?: ISubject;
}

const SubjectSearchDropdownNew: FC<IProps> = ({
  disabled,
  meta,
  input,
  country,
  label,
  containerClassName,
  customStyles,
  placeholder,
  customIcon,
  selectedSubject,
  setSelectedSubject,
}) => {
  const { data: subjects } = useGetSubjects({ country });
  const isInvalid = meta?.touched && meta?.error;

  const mapToDropdownItem = (subject: ISubject): IDropdownItem => ({
    label: translate(subject?.label),
    value: subject?.id,
    data: subject,
  });

  const loadOptions = (search: string, callback) => {
    callback(subjects?.filter(subject => translate(subject?.label).toLowerCase().includes(search.toLowerCase())).map(mapToDropdownItem));
  };

  useEffect(() => {
    if (input) {
      setSelectedSubject(input?.value);
    }
  }, [input?.value]);

  const onChange = (value: IDropdownItem) => {
    setSelectedSubject(value.data);
  };

  return (
    <div className={`subject-search-dropdown-new ${containerClassName || ''}`}>
      {label ? <label className="d-block">{label}</label> : null}
      <div className={'input-search position-relative pb-0'}>
        <AsyncSelect
          value={selectedSubject ? mapToDropdownItem(selectedSubject) : null}
          classNamePrefix={`${isInvalid ? 'async-select-invalid' : 'async-select'}`}
          className={'async-select'}
          noOptionsMessage={() => translate('entity.action.noResults')}
          placeholder={placeholder || translate('entity.action.searchByKeyword')}
          dropdownIndicator={null}
          loadOptions={loadOptions}
          isDisabled={disabled}
          onChange={inputValue => onChange(inputValue.valueOf())}
          styles={customStyles}
        />
        {customIcon ? customIcon : <Icon name={'search'} width={'20'} height={'21'} className={'position-absolute search-icon-new'} />}
      </div>
      {isInvalid && <FormFeedback className={'form-feedback mt-0'}>{meta.error}</FormFeedback>}
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  country: storeState.authentication.account.country,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(SubjectSearchDropdownNew);
