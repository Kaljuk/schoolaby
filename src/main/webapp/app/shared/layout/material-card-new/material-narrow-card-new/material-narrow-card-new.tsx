import React, { FC, useEffect, useState, useMemo } from 'react';

import '../material-card-new.scss';
import './material-narrow-card-new.scss';
import Icon from 'app/shared/icons';
import { Button } from 'reactstrap';
import { Material } from 'app/shared/model/material.model';
import { useChosenMaterialState, useMaterialState } from 'app/shared/contexts/material-context';
import { getRandomImage } from 'app/shared/util/image-utils';

export interface MaterialNarrowCardProps {
  material: Material;
  infoClassName?: string;
  materialAction?: () => void;
  showButton?: boolean;
}

const MaterialNarrowCardNew: FC<MaterialNarrowCardProps> = ({ material, infoClassName = '', materialAction }) => {
  const { materials } = useMaterialState();
  const { setChosenMaterial } = useChosenMaterialState();
  const [chosen, setChosen] = useState<boolean>(false);

  useEffect(() => {
    setChosen(materials.some(chosenMaterial => chosenMaterial.getKey() === material.getKey()));
  }, [materials]);

  return (
    <div
      data-testid={'material-card'}
      className={'material-card-new narrow d-flex flex-column'}
      onClick={() => setChosenMaterial(material)}
    >
      <div className={'material-cover mb-3'}>
        {useMemo(
          () => (
            <img
              alt={material.title}
              src={material.imageUrl ? material.imageUrl : getRandomImage()}
              onError={e => {
                // @ts-ignore
                e.target.src = getRandomImage();
              }}
            />
          ),
          [material]
        )}
      </div>
      <div className={'d-flex'}>
        <div className={`material-info d-flex ${infoClassName}`}>
          <div className={'d-flex flex-column'}>
            <h5 className={'title ellipsis mx-0 mt-0'}>{material.title}</h5>
            <p className={'description mx-0'}>{material.description}</p>
          </div>
        </div>
        {materialAction && (
          <Button
            color={'transparent'}
            disabled={chosen}
            className={'d-flex align-items-center justify-content-center align-self-start p-0'}
            onClick={e => {
              e.stopPropagation();
              materialAction();
            }}
          >
            {chosen ? <Icon name={'checkmarkNew'} /> : <Icon name={'plus'} />}
          </Button>
        )}
      </div>
    </div>
  );
};

export default MaterialNarrowCardNew;
