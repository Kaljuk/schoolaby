import React, { FC } from 'react';
import { E_KOOLIKOTT_MATERIAL, LTI_MATERIAL, OPIQ_MATERIAL } from 'app/shared/model/material.model';

import '../material-card-new.scss';
import './material-wide-card-new.scss';
import Icon from 'app/shared/icons';
import { Button } from 'reactstrap';
import { translate } from 'react-jhipster';

export interface WideCardNewProps {
  title?: string;
  description?: string;
  type?: string;
  image?: JSX.Element;
  deletable?: boolean;
  materialAction?: () => void;
  onClick?: () => void;
  chosen?: boolean;
  showButton?: boolean;
}

const WideCardNew: FC<WideCardNewProps> = ({
  title,
  description,
  type,
  image,
  deletable,
  materialAction,
  chosen = false,
  onClick,
  showButton,
}) => {
  const renderMaterialSourceIcon = imageSource => (
    <span className={'position-absolute d-flex justify-content-center align-items-center p-2 source-icon w-100 h-100'}>
      <img src={imageSource} width={'15'} height={'15'} alt={'ekoolikott-icon'} />
    </span>
  );

  const renderIconByMaterial = () => {
    if (type === OPIQ_MATERIAL) {
      return renderMaterialSourceIcon('content/images/opiqlogo.svg');
    } else if (type === E_KOOLIKOTT_MATERIAL) {
      return renderMaterialSourceIcon('content/images/ekoolikoti_logo.svg');
    } else if (type !== LTI_MATERIAL) {
      return renderMaterialSourceIcon('content/images/logo_short.svg');
    }
    return null;
  };

  const ActionIcon = () => (deletable ? <Icon name={'trashNew'} /> : <Icon name={'plus'} />);

  return (
    <div data-testid={'material-card'} className={'material-card-new wide d-flex align-items-center'} onClick={onClick}>
      <div
        className={`material-cover d-flex align-items-center justify-content-center position-relative${
          type === LTI_MATERIAL ? ' lti-material' : ''
        }`}
      >
        {image}
        {renderIconByMaterial()}
      </div>
      <div className={'material-info'}>
        <h5 className={'title ellipsis mt-0'}>{title}</h5>
        <p className={'description mt-0 mb-0'}>{description}</p>
      </div>
      {showButton && (
        <Button
          aria-label={translate('entity.action.delete')}
          data-testid={'material-delete-button'}
          color={'transparent'}
          disabled={chosen && !deletable}
          className={'d-flex align-items-center justify-content-center align-self-start p-0'}
          onClick={e => {
            e.stopPropagation();
            materialAction();
          }}
        >
          {chosen && !deletable ? <Icon name={'checkmarkNew'} /> : <ActionIcon />}
        </Button>
      )}
    </div>
  );
};

export default WideCardNew;
