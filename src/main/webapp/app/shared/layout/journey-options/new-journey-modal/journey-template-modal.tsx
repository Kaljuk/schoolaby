import React, { FC } from 'react';
import '../journey-option-modal/journey-option-modal.scss';
import JourneyOptionModal from 'app/shared/layout/journey-options/journey-option-modal/journey-option-modal';
import { translate } from 'react-jhipster';
import { Field } from 'react-final-form';
import SubjectSearchDropdownNew from 'app/shared/layout/subject-search-dropdown-new/subject-search-dropdown-new';
import Icon from 'app/shared/icons';
import { NewJourneyModalProps } from 'app/shared/layout/journey-options/new-journey-modal-props';
import { useJourneyTemplateSubjectState, useShowJourneyTemplatesViewState } from 'app/shared/contexts/journey-template-context';
import { useNewJourneyFromTemplateOptionSchema } from 'app/shared/schema/update-entity';

const JourneyTemplateModal: FC<NewJourneyModalProps> = ({ isModalOpen, closeModal }) => {
  const { setShowTemplatesView } = useShowJourneyTemplatesViewState();
  const { templateSubject, setTemplateSubject } = useJourneyTemplateSubjectState();

  return (
    <JourneyOptionModal
      isModalOpen={isModalOpen}
      closeModal={closeModal}
      imageSrc="content/images/template_journey_image.svg"
      title={translate('schoolabyApp.journey.selectSubjectToNewJourney')}
      onSubmit={() => setShowTemplatesView(true)}
      getValidationSchema={useNewJourneyFromTemplateOptionSchema}
    >
      <Field
        name="subject"
        placeholder={translate('schoolabyApp.journey.selectSubject')}
        containerClassName="mb-2"
        customIcon={<Icon name={'caretDown'} width="20" height="20" className={'position-absolute search-icon-new'} />}
        selectedSubject={templateSubject}
        setSelectedSubject={setTemplateSubject}
        component={SubjectSearchDropdownNew}
      />
    </JourneyOptionModal>
  );
};

export default JourneyTemplateModal;
