import React, { FC } from 'react';
import { SBY_GRAY_11 } from 'app/shared/util/color-utils';

import './tag.scss';
import CloseButtonNew from 'app/shared/layout/close-button/close-button-new';

interface SelectedEducationalAlignmentProps {
  title: string;
  handleRemove?: () => void;
  disabled?: boolean;
}

const Tag: FC<SelectedEducationalAlignmentProps> = ({ title, handleRemove, disabled }) => (
  <div data-testid={'tag'} className={'selected-alignment d-flex align-items-center'}>
    <span>{title}</span>
    {!disabled && (
      <CloseButtonNew crossColor={SBY_GRAY_11} className={'p-0'} onMouseDown={e => e.preventDefault()} onClick={handleRemove} />
    )}
  </div>
);

export default Tag;
