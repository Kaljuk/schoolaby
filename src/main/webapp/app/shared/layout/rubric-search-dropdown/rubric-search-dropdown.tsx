import React, { useState } from 'react';
import { IRubric } from 'app/shared/model/rubric/rubric.model';
import { translate } from 'react-jhipster';
import AsyncSelect from 'react-select/async';
import { useGetTemplates } from 'app/shared/services/rubric-api';
import './rubric-search-dropdown.scss';
import debounce from 'lodash/debounce';
import { useSelectedRubricState } from 'app/shared/contexts/entity-update-context';
import Icon from 'app/shared/icons';

interface IDropdownItem {
  label?: string;
  value?: number;
  data?: IRubric;
}

const RubricSearchDropdown = () => {
  const { selectedRubric, setSelectedRubric } = useSelectedRubricState();
  const [search, setSearch] = useState(null);
  const { refetch } = useGetTemplates(search);

  const mapToDropdownItem = (rubric: IRubric): IDropdownItem => ({
    label: rubric?.title,
    value: rubric?.id,
    data: rubric,
  });

  const onChange = (value: IDropdownItem) => {
    setSelectedRubric(value.data);
  };

  const loadOptions = (searchInput: string, callback) => {
    setSearch(searchInput);
    refetch().then(({ data }) => callback(data?.map(rubric => mapToDropdownItem(rubric))));
  };

  return (
    <div className={'rubric-search-dropdown position-relative mb-3 mb-md-0'}>
      <AsyncSelect
        value={selectedRubric ? mapToDropdownItem(selectedRubric) : null}
        classNamePrefix={'async-select'}
        className={'async-select'}
        noOptionsMessage={() => translate('entity.action.noResults')}
        placeholder={translate('schoolabyApp.assignment.rubric.search')}
        loadOptions={debounce(loadOptions, 100)}
        onChange={inputValue => onChange(inputValue.valueOf())}
      />
      <div className="position-absolute search-icon">
        <Icon name="search" />
      </div>
    </div>
  );
};

export default RubricSearchDropdown;
