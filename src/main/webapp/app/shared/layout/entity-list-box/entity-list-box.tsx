import './entity-list-box.scss';
import React, { FC } from 'react';
import { ButtonProps } from 'reactstrap';

interface IEntityListBox extends ButtonProps {
  title?: string;
  entity?: any;
  avatar: JSX.Element;
  onClick?: React.MouseEventHandler<any>;
  additionalComponent?: JSX.Element;
  subTitle?: string;
  isActive?: boolean;
  className?: string;
  role?: string;
}

const EntityListBox: FC<IEntityListBox> = ({ title, subTitle, avatar, additionalComponent, onClick, entity, isActive, className }) => {
  const handleOnClick = () => {
    onClick(entity);
  };

  return (
    <div className={`${className || ''}${isActive ? 'selected ' : ''}list-box-wrapper d-flex`} onClick={() => handleOnClick()}>
      <>
        <div className="d-flex align-items-center px-2">
          {avatar}
          <div className={'ml-3'}>
            <span>{title ? title : entity ? entity.title : ''}</span>
            {subTitle && <p className={'mb-0'}>{subTitle}</p>}
          </div>
        </div>
        {additionalComponent}
      </>
    </div>
  );
};
export default EntityListBox;
