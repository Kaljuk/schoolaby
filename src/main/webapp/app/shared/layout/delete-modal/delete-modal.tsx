import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Button, ModalBody, ModalFooter } from 'reactstrap';
import { translate } from 'react-jhipster';
import { compose } from 'redux';
import CustomModal from 'app/shared/layout/custom-modal/custom-modal';
import CustomModalHeader from 'app/shared/layout/custom-modal/custom-modal-header';

export interface IDeleteModal {
  entityType: string;
  isOpen: boolean;
  entityId: number;
  deleteAction: () => void;
  journeyId?: number;
  entityTitle?: string;
  toggle?: () => void;
}

interface DeleteModalProps extends RouteComponentProps {
  isOpen: boolean;
  toggle: () => void;
  deleteAction: () => void;
  entityTitle?: string;
  customDescription?: string;
}

const DeleteModal: FC<DeleteModalProps> = ({ isOpen, toggle, deleteAction, entityTitle, customDescription }) => {
  const handleDelete = () => {
    deleteAction();
    toggle();
  };

  return (
    <CustomModal isOpen={isOpen} toggle={toggle}>
      <CustomModalHeader toggle={toggle}>
        <h5 className="modal-title">{translate('entity.delete.title')}</h5>
      </CustomModalHeader>
      <ModalBody id="schoolabyApp.journey.delete.question">
        {customDescription ? customDescription : translate('entity.delete.description', { entityTitle })}
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={toggle}>
          {translate('entity.action.cancel')}
        </Button>
        <Button color="danger" onClick={handleDelete}>
          {translate('entity.action.delete')}
        </Button>
      </ModalFooter>
    </CustomModal>
  );
};

export default compose(withRouter)(DeleteModal);
