import React, { FC, useMemo, useState } from 'react';
import { useMaterialState } from 'app/shared/contexts/material-context';

import './choose-materials.scss';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import { translate } from 'react-jhipster';
import MaterialWideCardNew from 'app/shared/layout/material-card-new/material-wide-card-new/material-wide-card-new';
import { isMobile } from 'react-device-detect';
import { Material } from 'app/shared/model/material.model';
import { UseBaseMutationResult } from 'react-query';
import DeleteModal from 'app/shared/layout/delete-modal/delete-modal';
import { PatchAssignmentParams } from 'app/shared/services/assignment-api';
import { DropdownItem } from 'reactstrap';
import AddOwnMaterialDropdown from 'app/marketplace/add-own-material/add-own-material-dropdown';

interface ChooseMaterialsProps {
  setGoToMarketplace?: (goToMarketPlace: boolean) => void;
  patchRequest?: UseBaseMutationResult;
  isDropdown?: boolean;
  isAllowedToModify?: boolean;
}

const ChooseMaterials: FC<ChooseMaterialsProps> = ({ setGoToMarketplace, patchRequest, isAllowedToModify, isDropdown = false }) => {
  const { materials, removeMaterial } = useMaterialState();
  const [materialToRemove, setMaterialToRemove] = useState<any>(null);

  const deleteAction = () => {
    const patchBody: PatchAssignmentParams = {};
    if (materialToRemove.isLti()) {
      patchBody['ltiResources'] = materials
        .filter(mat => mat.isLti() && mat.id !== materialToRemove.id)
        .map(material => material.getLtiResource());
    } else {
      patchBody['materials'] = materials.filter(mat => !mat.isLti() && mat.id !== materialToRemove.id).map(material => material.getCopy());
    }
    patchRequest.mutateAsync(patchBody);
  };

  const trashIconOnClick = (material: Material) => {
    if (patchRequest) {
      setMaterialToRemove(material);
    } else {
      removeMaterial(material);
    }
  };

  const Placeholder = () => (
    <div className={'choose-materials-placeholder'}>
      <div className={'left-section'}>
        <h4>{translate('schoolabyApp.marketplace.chooseMaterials')}</h4>
        <p>{translate('schoolabyApp.marketplace.marketplaceIntro')}</p>
        <CustomButton
          type={'submit'}
          title={translate('schoolabyApp.marketplace.findMaterial')}
          buttonType={'white'}
          outline
          className={'mb-0'}
          onClick={() => setGoToMarketplace(true)}
          disabled={!!isMobile}
        />
      </div>
      <div className={'right-section'}>
        <img
          className={'w-100 h-100'}
          src={'content/images/marketplace_filler.png'}
          alt={translate('schoolabyApp.marketplace.chooseMaterials')}
        />
      </div>
    </div>
  );

  return useMemo(
    () => (
      <>
        <div className={'choose-materials-container'}>
          <div className={'d-flex align-items-center justify-content-between'}>
            <h4 className={'mb-0'}>{translate('schoolabyApp.marketplace.materials')}</h4>
            {isAllowedToModify &&
              (isDropdown ? (
                <AddOwnMaterialDropdown
                  patchRequest={patchRequest}
                  toggleButton={
                    <CustomButton
                      className={'add-material-button'}
                      type={'submit'}
                      iconName={'plus'}
                      buttonType={'white'}
                      size={'sm'}
                      title={translate('global.form.addNewMaterialBtn')}
                      disabled={!!isMobile}
                    />
                  }
                >
                  <DropdownItem onClick={() => setGoToMarketplace(true)}>
                    {translate('schoolabyApp.marketplace.add.marketplace')}
                  </DropdownItem>
                </AddOwnMaterialDropdown>
              ) : (
                <CustomButton
                  type={'submit'}
                  iconName={'plus'}
                  buttonType={'white'}
                  size={'sm'}
                  onClick={() => setGoToMarketplace(true)}
                  title={translate('global.form.addNewMaterialBtn')}
                  disabled={!!isMobile}
                />
              ))}
          </div>
          <hr className={`${!materials.length && !isMobile ? 'd-none' : ''}`} />
          {isMobile && <p>{translate('schoolabyApp.marketplace.blocked')}</p>}
          {materials.length
            ? materials.map(material => (
                <MaterialWideCardNew
                  key={`material-${material.getKey()}`}
                  material={material}
                  deletable={isAllowedToModify}
                  showButton={isAllowedToModify}
                  materialAction={() => trashIconOnClick(material)}
                />
              ))
            : isAllowedToModify && <Placeholder />}
        </div>
        <DeleteModal
          deleteAction={deleteAction}
          isOpen={!!materialToRemove}
          toggle={() => setMaterialToRemove(null)}
          entityTitle={materialToRemove?.title}
        />
      </>
    ),
    [materials, materialToRemove]
  );
};

export default ChooseMaterials;
