import React, { FC } from 'react';
import { useCameraMountTargetState } from 'app/shared/contexts/camera-context';
import { VideoWebcam } from 'app/shared/layout/webcams/video-webcam';
import { Camera } from 'app/shared/layout/webcams/camera/camera';
import CameraModal from 'app/shared/layout/webcams/camera-modal/camera-modal';
import { Col, Row } from 'reactstrap';
import { isMobile, isTablet } from 'react-device-detect';
import CameraModalMobile from 'app/shared/layout/webcams/camera-modal-mobile/camera-modal-mobile';

interface CameraWrapperProps {
  icon?: JSX.Element;
  description?: string;
  showDescription?: boolean;
  children: JSX.Element | JSX.Element[];
  className?: string;
  rowClassName?: string;
}

const CameraWrapper: FC<CameraWrapperProps> = ({
  icon,
  description,
  showDescription = true,
  children: dropZoneButtons,
  className = '',
  rowClassName = '',
}) => {
  const { cameraMountTarget } = useCameraMountTargetState();

  return (
    <Row className={rowClassName}>
      {showDescription && icon}
      <div>
        {cameraMountTarget !== 'BODY' && showDescription && (
          <Col xs={12} className={'d-flex text-secondary justify-content-center placeholder'}>
            {description}
          </Col>
        )}
        <Col xs={12} className={'d-flex file-upload-buttons ' + className}>
          {cameraMountTarget !== 'BODY' && <span>{dropZoneButtons}</span>}
          {cameraMountTarget !== undefined && <VideoWebcam />}
          {cameraMountTarget === 'BODY' && <Camera />}
          {cameraMountTarget === 'MODAL' && (isMobile || isTablet) ? <CameraModalMobile /> : <CameraModal />}
        </Col>
      </div>
    </Row>
  );
};

export default CameraWrapper;
