import './camera-modal/camera-modal.scss';
import React, { FC } from 'react';
import {
  useCameraFacingModeState,
  useCameraMountTargetState,
  useCapturingState,
  useTakePictureState,
} from 'app/shared/contexts/camera-context';
import Icon from 'app/shared/icons';
import { Row } from 'reactstrap';
import { translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import debounce from 'lodash/debounce';
import { isMobile, isTablet } from 'react-device-detect';

export const CameraButtons: FC = () => {
  const { capturing, setCapturing } = useCapturingState();
  const { takePicture, setTakePicture } = useTakePictureState();
  const { cameraMountTarget, setCameraMountTarget } = useCameraMountTargetState();
  const { toggleCameraFacingMode } = useCameraFacingModeState();

  return (
    <Row className={'justify-content-around'}>
      <button
        id={'take-picture-button'}
        className={'take-picture-button d-flex'}
        aria-label={translate('schoolabyApp.submission.camera.takePicture')}
        disabled={takePicture}
        onClick={debounce(() => setTakePicture(true), 500)}
        title={translate('schoolabyApp.submission.camera.takePicture')}
      >
        <Icon name={'camera'} />
      </button>
      {capturing ? (
        <button
          id={'stop-recording-button'}
          className={'stop-recording-button ml-2'}
          aria-label={translate('schoolabyApp.submission.camera.stopRecording')}
          onClick={() => setCapturing(false)}
          title={translate('schoolabyApp.submission.camera.stopRecording')}
        >
          <div className={'bg-grey w-100 h-100'} />
        </button>
      ) : (
        <button
          id={'start-recording-button'}
          className={'start-recording-button bg-white ml-2'}
          aria-label={translate('schoolabyApp.submission.camera.startRecording')}
          title={translate('schoolabyApp.submission.camera.startRecording')}
          onClick={() => setCapturing(true)}
        >
          <div className={'bg-red w-100 h-100 rounded-circle'} />
        </button>
      )}
      {(isMobile || isTablet) && (
        <button
          disabled={capturing}
          id={'toggle-facing-mode-button'}
          className={'toggle-facing-mode-button d-flex ml-2'}
          aria-label={translate('schoolabyApp.submission.camera.toggleFacingMode')}
          title={translate('schoolabyApp.submission.camera.toggleFacingMode')}
          onClick={toggleCameraFacingMode}
        >
          <Icon name={'cameraRotation'} />
        </button>
      )}
      {cameraMountTarget === 'BODY' && (
        <>
          <button
            id={'maximize-button'}
            className={'maximize-button ml-2'}
            aria-label={translate('schoolabyApp.submission.camera.maximize')}
            title={translate('schoolabyApp.submission.camera.maximize')}
            onClick={() => setCameraMountTarget('MODAL')}
          >
            <FontAwesomeIcon icon={'external-link-alt'} width={'32px'} />
          </button>
          <button
            id={'close-button'}
            className={'close-button ml-2'}
            aria-label={translate('schoolabyApp.submission.camera.close')}
            title={translate('schoolabyApp.submission.camera.close')}
            onClick={() => {
              setCameraMountTarget(undefined);
              setCapturing(false);
            }}
          >
            <Icon name={'cross'} width={'20px'} stroke={'white'} fill={'white'} />
          </button>
        </>
      )}
      {cameraMountTarget === 'MODAL' && (
        <button
          id={'minimize-button'}
          className={'minimize-button ml-2'}
          aria-label={translate('schoolabyApp.submission.camera.minimize')}
          title={translate('schoolabyApp.submission.camera.minimize')}
          onClick={() => setCameraMountTarget('BODY')}
        >
          <FontAwesomeIcon icon={'window-restore'} width={'32px'} />
        </button>
      )}
    </Row>
  );
};
