import React, { FC, useContext, useState } from 'react';
import { translate } from 'react-jhipster';
import AssignmentTabs from 'app/shared/layout/tabs/assignment-tabs';
import MobileMenuOptionsModal from 'app/shared/layout/mobile-menus/mobile-menu/mobile-menu-options-modal/mobile-menu-options-modal';
import MobileMenuOption from 'app/shared/layout/mobile-menus/mobile-menu/mobile-menu-options-modal/mobile-menu-option';
import MobileMenu from 'app/shared/layout/mobile-menus/mobile-menu/mobile-menu';
import { useDisplayEntityUpdateState } from 'app/shared/contexts/entity-update-context';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { AppContext } from 'app/app';
import { ASSIGNMENT } from 'app/shared/util/entity-utils';
import { useHistory } from 'react-router-dom';
import { useDeleteAssignment } from 'app/shared/services/assignment-api';

interface AssignmentMobileMenuProps {
  newAssignmentViewDesign?: boolean;
}

const AssignmentMobileMenu: FC<AssignmentMobileMenuProps> = ({ newAssignmentViewDesign = false }) => {
  const history = useHistory();
  const [displayMoreOptions, setDisplayMoreOptions] = useState<boolean>(false);
  const { openAssignmentUpdate } = useDisplayEntityUpdateState();
  const { assignment, isAllowedToModify, journey } = useContext(AssignmentContext);
  const { setModal: setDeleteModal } = useContext(AppContext);
  const deleteAssignment = useDeleteAssignment(assignment?.id);

  const toggleOptionsModal = () => {
    setDisplayMoreOptions(prev => !prev);
  };

  const handleAssignmentEdit = () => {
    openAssignmentUpdate(assignment.id);
  };

  const deleteAction = () => deleteAssignment.mutateAsync().then(() => history.push(`/journey/${journey.id}`));

  const handleAssignmentDelete = () => {
    setDeleteModal({
      entityType: ASSIGNMENT,
      isOpen: true,
      entityId: assignment.id,
      entityTitle: assignment.title,
      journeyId: journey.id,
      deleteAction,
    });
  };

  const MoreOptions = () => (
    <MobileMenuOptionsModal isOpen={displayMoreOptions} toggleModal={toggleOptionsModal}>
      <MobileMenuOption iconName={'editNew'} text={translate('entity.action.edit')} onClick={handleAssignmentEdit} />
      <MobileMenuOption iconName={'trashNew'} text={translate('entity.action.delete')} onClick={handleAssignmentDelete} />
    </MobileMenuOptionsModal>
  );

  return (
    <MobileMenu options={isAllowedToModify && <MoreOptions />} toggleOptionsModal={toggleOptionsModal}>
      <AssignmentTabs mobileTabs newAssignmentViewDesign={newAssignmentViewDesign} />
    </MobileMenu>
  );
};

export default AssignmentMobileMenu;
