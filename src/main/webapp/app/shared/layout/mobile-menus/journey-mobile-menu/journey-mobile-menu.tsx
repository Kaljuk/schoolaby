import React, { FC, useContext, useState } from 'react';
import { IJourney } from 'app/shared/model/journey.model';
import { useHistory } from 'react-router-dom';
import { translate } from 'react-jhipster';
import { withHttp } from 'app/shared/util/file-utils';
import InviteModal from 'app/journey/journey-detail/invite-modal/invite-modal';
import JourneyTabs from 'app/shared/layout/tabs/journey-tabs';
import MobileMenuOptionsModal from 'app/shared/layout/mobile-menus/mobile-menu/mobile-menu-options-modal/mobile-menu-options-modal';
import MobileMenuOption from 'app/shared/layout/mobile-menus/mobile-menu/mobile-menu-options-modal/mobile-menu-option';
import MobileMenu from 'app/shared/layout/mobile-menus/mobile-menu/mobile-menu';
import { JOURNEY } from 'app/shared/util/entity-utils';
import { AppContext } from 'app/app';
import { useDeleteJourney } from 'app/shared/services/journey-api';

interface JourneyMobileMenuProps {
  journey: IJourney;
}

const JourneyMobileMenu: FC<JourneyMobileMenuProps> = ({ journey }) => {
  const [displayMoreOptions, setDisplayMoreOptions] = useState<boolean>(false);
  const [displayStudentsInviteModal, setDisplayStudentsInviteModal] = useState<boolean>(false);
  const { setModal: setDeleteModal } = useContext(AppContext);
  const history = useHistory();
  const deleteJourney = useDeleteJourney(journey.id);

  const toggleOptionsModal = () => {
    setDisplayMoreOptions(prev => !prev);
  };

  const toggleInviteModal = () => {
    setDisplayStudentsInviteModal(prev => !prev);
  };

  const handleJourneyEdit = () => {
    history.push(`/journey/${journey.id}/edit`);
  };

  const deleteAction = () => deleteJourney.mutateAsync().then(() => history.push('/journey/'));

  const handleJourneyDelete = () => {
    setDeleteModal({
      entityType: JOURNEY,
      isOpen: true,
      entityId: journey.id,
      entityTitle: journey.title,
      deleteAction,
    });
  };

  const MoreOptions = () => (
    <MobileMenuOptionsModal isOpen={displayMoreOptions} toggleModal={toggleOptionsModal}>
      <MobileMenuOption iconName={'editNew'} text={translate('entity.action.edit')} onClick={handleJourneyEdit} />
      <MobileMenuOption iconName={'trashNew'} text={translate('entity.action.delete')} onClick={handleJourneyDelete} />
      <MobileMenuOption iconName={'invite'} text={translate('schoolabyApp.journey.detail.inviteStudents')} onClick={toggleInviteModal} />
      <a href={withHttp(journey.videoConferenceUrl)} target="_blank" rel="noopener noreferrer">
        <MobileMenuOption iconName={'video'} text={translate('schoolabyApp.journey.detail.viewLive')} />
      </a>
    </MobileMenuOptionsModal>
  );

  return (
    <>
      <MobileMenu options={<MoreOptions />} toggleOptionsModal={toggleOptionsModal}>
        <JourneyTabs mobileTabs />
      </MobileMenu>
      <InviteModal type={'STUDENT'} modal={displayStudentsInviteModal} toggle={toggleInviteModal} journey={journey} />
    </>
  );
};

export default JourneyMobileMenu;
