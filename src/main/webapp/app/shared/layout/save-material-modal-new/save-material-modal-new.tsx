import React, { FC, useRef, useState } from 'react';
import { Modal } from 'reactstrap';

import './save-material-modal-new.scss';
import { Field, FieldRenderProps, Form } from 'react-final-form';
import { Input } from 'app/shared/form';
import { translate } from 'react-jhipster';
import { COUNTRY, EVENT_CODE_ENTER } from 'app/config/constants';
import { Material } from 'app/shared/model/material.model';
import { mapEntityIdList } from 'app/shared/util/entity-utils';
import { useSelectedEducationalAlignmentsState } from 'app/shared/contexts/entity-update-context';
import { useMaterialState } from 'app/shared/contexts/material-context';
import { DateTime } from 'luxon';
import { useAddMaterialSchema } from 'app/shared/schema/add-material';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { createYupValidator } from 'app/shared/util/form-utils';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import { getAuthToken } from 'app/shared/util/auth-utils';
import Compressor from 'app/shared/form/compressor';
import { getFileUrl } from 'app/shared/services/uploaded-file-api';
import Uppy, { UppyFile } from '@uppy/core';
import XHRUpload from '@uppy/xhr-upload';
import Informer from '@uppy/informer';
import FileUpload from 'app/shared/form/file-upload';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import { FileUploadProvider } from 'app/shared/form/file-upload-context';
import { OnChange } from 'react-final-form-listeners';
import { UseBaseMutationResult } from 'react-query';

export const LINK = 'link';
export const FILE = 'file';
export const CAMERA = 'camera';

export type SaveMaterialModalType = typeof LINK | typeof FILE | typeof CAMERA;

interface SaveMaterialModalNewProps extends StateProps {
  isOpen: boolean;
  toggleModal: () => void;
  type: SaveMaterialModalType;
  patchRequest?: UseBaseMutationResult;
}

const SaveMaterialModalNew: FC<SaveMaterialModalNewProps> = ({ isOpen, toggleModal, locale, type, patchRequest, isUserUkrainian }) => {
  const defaultImageUrl = '/content/images/default_cover.svg';
  const schema = useAddMaterialSchema(locale, type);
  const imageUrlRef = useRef(null);
  const title =
    type === LINK
      ? translate('schoolabyApp.marketplace.modal.add.link')
      : type === FILE
      ? translate('schoolabyApp.marketplace.modal.add.file')
      : translate('schoolabyApp.marketplace.modal.add.camera');
  const [imageUrl, setImageUrl] = useState<string>(defaultImageUrl);
  const { selectedEducationalAlignments } = useSelectedEducationalAlignmentsState();
  const { addMaterial, materials } = useMaterialState();

  const toggle = () => {
    setImageUrl(defaultImageUrl);
    toggleModal();
  };

  const uppy = Uppy({
    autoProceed: true,
    debug: process.env.NODE_ENV === 'development',
    restrictions: {
      maxFileSize: null,
      maxNumberOfFiles: 1,
      minNumberOfFiles: 0,
      allowedFileTypes: null,
    },
  })
    .use(XHRUpload, {
      fieldName: 'file',
      endpoint: `/api/files`,
      headers: { Authorization: `Bearer ${getAuthToken()}` },
      limit: 1,
    })
    // @ts-ignore
    .use(Compressor, {
      quality: 0.9,
      maxWidth: 2500,
      maxHeight: 2500,
      convertSize: 3000000,
    })
    .use(Informer, {})
    .on('upload-success', (file: UppyFile, response: any) => {
      setImageUrl(getFileUrl(response.body.uniqueName));
    });

  const onImageInputChange = event => {
    const files = Array.from(event.target.files);

    files.forEach((file: File) => {
      uppy.addFile({
        source: 'file input',
        name: file.name,
        type: file.type,
        data: file,
      });
    });
  };

  const saveMaterial = values => {
    const materialFromJson = Material.fromJson({
      ...values,
      imageUrl,
      type: values.uploadedFile ? 'FILE' : 'LINK',
      educationalAlignments: mapEntityIdList(selectedEducationalAlignments),
      created: DateTime.local().toISO(),
      country: isUserUkrainian ? COUNTRY.UKRAINE : undefined,
    });

    if (patchRequest) {
      const patchedMaterials = materials.filter(material => !material.isLti());
      patchedMaterials.push(materialFromJson);
      patchRequest.mutateAsync({
        materials: patchedMaterials,
      });
    } else {
      addMaterial(materialFromJson);
    }

    toggle();
  };

  return (
    <Modal
      size={'lg'}
      className={'save-material-modal-new'}
      backdropClassName={'dark-backdrop'}
      centered
      isOpen={isOpen}
      toggle={() => {
        setImageUrl('/content/images/default_cover.svg');
        toggle();
      }}
    >
      <div className={'img-container w-100 position-relative'}>
        <img alt={translate('schoolabyApp.marketplace.modal.coverImage')} src={imageUrl} />
        <CustomButton
          className={'position-absolute'}
          iconName={'cameraBlack'}
          buttonType={'white'}
          title={translate('schoolabyApp.marketplace.changeCover')}
          onClick={() => imageUrlRef.current.click()}
        />
      </div>
      <div className={'form'}>
        <h4>{title}</h4>
        <Form
          initialValues={{}}
          onSubmit={saveMaterial}
          validate={createYupValidator(schema)}
          mutators={{
            setMaterialUrl(args, state, utils) {
              utils.changeValue(state, 'url', () => args[0]);
            },
          }}
        >
          {({ form, handleSubmit }) => (
            <form onSubmit={handleSubmit}>
              <Field
                hidden
                id="cover-image"
                name="imageUrl"
                type={'file'}
                innerRef={imageUrlRef}
                onChange={onImageInputChange}
                component={Input}
              />
              <Field
                id="material-title"
                type="text"
                name="title"
                label={translate('schoolabyApp.material.title')}
                placeholder={translate('schoolabyApp.marketplace.modal.titlePlaceholder')}
                component={Input}
              />
              <Field
                id="material-description"
                type="textarea"
                name="description"
                rows="3"
                onKeyPress={e => {
                  if (e.code === EVENT_CODE_ENTER) {
                    e.stopPropagation();
                  }
                }}
                label={translate('schoolabyApp.material.description')}
                placeholder={translate('schoolabyApp.marketplace.modal.descriptionPlaceholder')}
                component={Input}
              />
              {type !== LINK && (
                <>
                  <Field
                    id="uploaded-file"
                    name="uploadedFile"
                    render={({ input: { value, onChange } }: FieldRenderProps<IUploadedFile>) => (
                      <FileUploadProvider>
                        <FileUpload
                          existingFiles={value ? [value] : []}
                          onUpload={onChange}
                          onDelete={() => onChange(null)}
                          uploadDisabled={false}
                          maxFiles={1}
                          fileUploadType={type}
                        />
                      </FileUploadProvider>
                    )}
                  />
                  <OnChange name="uploadedFile">
                    {(value: IUploadedFile) => {
                      form.mutators.setMaterialUrl(value ? getFileUrl(value.uniqueName) : '');
                    }}
                  </OnChange>
                </>
              )}
              <Field
                id="material-url"
                type="text"
                name="url"
                label={translate('schoolabyApp.material.url')}
                placeholder={translate('schoolabyApp.marketplace.modal.linkPlaceholder')}
                hidden={type !== LINK}
                component={Input}
              />
              <Field
                id={'material-restricted'}
                label={translate('schoolabyApp.material.privateMaterial')}
                type="checkbox"
                name="restricted"
                initialValue={false}
                component={Input}
                labelAfterInput
                labelClassName={'m-0'}
                containerClassName={'d-flex align-items-center'}
              />
              <div className={'d-flex justify-content-between'}>
                <CustomButton buttonType={'cancel'} title={translate('entity.action.cancel')} onClick={toggle} />
                <CustomButton type={'submit'} buttonType={'primary'} title={translate('entity.action.add')} />
              </div>
            </form>
          )}
        </Form>
      </div>
    </Modal>
  );
};

const mapStateToProps = ({ locale, authentication }: IRootState) => ({
  locale: locale.currentLocale,
  isUserUkrainian: authentication.account.country === COUNTRY.UKRAINE,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(SaveMaterialModalNew);
