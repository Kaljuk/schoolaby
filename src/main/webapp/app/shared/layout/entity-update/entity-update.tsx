import React, { FC, useEffect, useState } from 'react';
import {
  UPDATE_ASSIGNMENT,
  UPDATE_MILESTONE,
  useDisplayEntityUpdateState,
  useEntityToUpdateState,
  useSelectedEducationalAlignmentsState,
} from 'app/shared/contexts/entity-update-context';
import AssignmentUpdate from 'app/assignment/assignment-update';
import './entity-update.scss';
import { Material } from 'app/shared/model/material.model';
import { useHistory } from 'react-router-dom';
import { useMaterialState } from 'app/shared/contexts/material-context';
import MilestoneUpdate from 'app/milestone/milestone-update';

const EntityUpdate: FC = () => {
  const history = useHistory();
  const [shouldShowEdit, setShouldShowEdit] = useState<boolean>(false);
  const { entity, setEntity } = useEntityToUpdateState();
  const { setMaterials } = useMaterialState();
  const { displayEntityUpdate } = useDisplayEntityUpdateState();
  const { setSelectedEducationalAlignments } = useSelectedEducationalAlignmentsState();
  const { openAssignmentUpdate, openMilestoneUpdate } = useDisplayEntityUpdateState();

  useEffect(() => {
    history.location.state && setEntity(history.location.state);
    history.location.state && setShouldShowEdit(true);
    history.replace({ ...history.location, state: null });
  }, [history.location.state]);

  useEffect(() => {
    if (entity && shouldShowEdit) {
      'milestoneId' in entity ? openAssignmentUpdate(entity.id) : openMilestoneUpdate(entity.id);
      entity.materials ? setMaterials(entity.materials.map(material => Material.fromJson(material))) : setMaterials([]);
      entity.educationalAlignments ? setSelectedEducationalAlignments(entity.educationalAlignments) : setSelectedEducationalAlignments([]);
      setShouldShowEdit(false);
    }
  }, [entity, shouldShowEdit]);

  switch (displayEntityUpdate) {
    case UPDATE_MILESTONE:
      return <MilestoneUpdate />;
    case UPDATE_ASSIGNMENT:
      return <AssignmentUpdate />;
    default:
      return null;
  }
};

export default EntityUpdate;
