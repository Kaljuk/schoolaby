import React, { FC, ReactNode } from 'react';
import { FieldRenderProps } from 'react-final-form';
import { ICON_LIGHT_GREY, INFO } from 'app/shared/util/color-utils';
import AddButton from 'app/shared/layout/add-button/add-button';
import { translate } from 'react-jhipster';
import AsyncSelect from 'react-select/async';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button, FormFeedback } from 'reactstrap';
import './multiselect-search-dropdown.scss';

interface IProps extends FieldRenderProps<any, any> {
  handleInputChange: (inputValue: string) => void;
  loadOptions: (inputValue, callback) => void;
  label: ReactNode;
  inputValues: IDropdownItem[];
  setInputValues: (values: IDropdownItem[]) => void;
  errorMessage?: string;
  disabled?: boolean;
  addButtonAriaLabel?: string;
  removeButtonAriaLabel?: string;
  className?: string;
}

interface IDropdownItem {
  label?: string;
  value?: number;
  data?: any;
}

const MultiselectSearchDropdown: FC<IProps> = ({
  meta,
  handleInputChange,
  disabled,
  inputValues,
  setInputValues,
  loadOptions,
  label,
  errorMessage,
  addButtonAriaLabel,
  removeButtonAriaLabel,
  className,
}) => {
  const isInvalid = meta?.touched && meta?.error;

  const removeItem = (index: number) => {
    const updatedInputValues = [...inputValues];
    updatedInputValues.splice(index, 1);
    setInputValues(updatedInputValues);
  };

  const onChange = (value: IDropdownItem, key: number) => {
    const updatedInputValues = [...inputValues];
    updatedInputValues[key] = value;
    setInputValues(updatedInputValues);
  };

  return (
    <div className={`multiselect-search-dropdown ${className || ''}`}>
      {label}
      {inputValues?.map((value, index) => (
        <div className={'input-search position-relative'} key={index}>
          <AsyncSelect
            classNamePrefix={`${isInvalid ? 'async-select-invalid' : 'async-select'}`}
            className={'async-select'}
            value={value.label ? value : null}
            noOptionsMessage={() => translate('entity.action.noResults')}
            placeholder={translate('entity.action.searchByKeyword')}
            loadOptions={loadOptions}
            isDisabled={disabled}
            onInputChange={handleInputChange}
            onChange={inputValue => onChange(inputValue.valueOf(), index)}
          />
          <FontAwesomeIcon
            className={'position-absolute search-icon'}
            icon={['fas', 'search']}
            color={disabled ? ICON_LIGHT_GREY : INFO}
            size={'lg'}
          />
          {inputValues.length > 1 && !disabled && (
            <Button
              aria-label={removeButtonAriaLabel || ''}
              className={'btn-danger rounded-circle btn-subtract text-center ml-3'}
              onClick={() => removeItem(index)}
            >
              -
            </Button>
          )}
        </div>
      ))}
      {isInvalid && <FormFeedback className={'form-feedback'}>{errorMessage}</FormFeedback>}
      {!disabled && (
        <AddButton
          aria-label={addButtonAriaLabel || ''}
          className="justify-content-end pt-1 mb-2"
          onClick={() =>
            setInputValues([
              ...inputValues,
              {
                label: undefined,
                value: undefined,
                data: {},
              },
            ])
          }
        />
      )}
    </div>
  );
};

export default MultiselectSearchDropdown;
