import './header.scss';
import React, { useState } from 'react';
import { Storage, Translate } from 'react-jhipster';
import { Collapse, Navbar, NavbarToggler } from 'reactstrap';
import { LoadingBar } from 'react-redux-loading-bar';
import { Brand } from './header-components';
import RightMenu from './right-menu';
import LeftMenu from 'app/shared/layout/header/left-menu';
import { useHistory, useLocation } from 'react-router-dom';
import { LocaleMenuWithFlags } from 'app/shared/layout/menus';
import { IRootState } from 'app/shared/reducers';
import { setLocale } from 'app/shared/reducers/locale';
import { connect } from 'react-redux';

interface IHeaderProps extends StateProps, DispatchProps {
  hasSelectedRole: boolean;
  isAdmin: boolean;
  ribbonEnv: string;
  isInProduction: boolean;
  isSwaggerEnabled: boolean;
}

const Header = (props: IHeaderProps) => {
  const [menuOpen, setMenuOpen] = useState(false);
  const location = useLocation();
  const history = useHistory();
  const locationsExcluded = ['/lti/content-item-selection', '/error/not_found'];

  const isExcludedFromPage = () => {
    return locationsExcluded.includes(location.pathname);
  };

  const renderDevRibbon = () =>
    props.isInProduction === false ? (
      <div className="ribbon dev">
        <a href="">
          <Translate contentKey={`global.ribbon.${props.ribbonEnv}`} />
        </a>
      </div>
    ) : null;

  const handleLocaleChange = locale => {
    Storage.session.set('locale', locale);
    props.setLocale(locale);
    const params = new URLSearchParams();
    params.append('lang', locale);
    history.push({ search: params.toString() });
  };

  return !isExcludedFromPage() ? (
    <div id="app-header">
      {renderDevRibbon()}
      <LoadingBar className="loading-bar" />
      {props.hasSelectedRole && (
        <Navbar light expand="lg" fixed="top" className="jh-navbar">
          <NavbarToggler aria-label="Menu" onClick={() => setMenuOpen(!menuOpen)} />
          <Brand />
          <Collapse isOpen={menuOpen} navbar>
            <LeftMenu isAdmin={props.isAdmin} isAuthenticated={props.hasSelectedRole} isSwaggerEnabled={props.isSwaggerEnabled} />
            <RightMenu />
          </Collapse>
        </Navbar>
      )}
      {!props.hasSelectedRole && (
        <Navbar expand="lg" fixed="top" className="position-static px-4 login-header">
          <Brand />
          <div className="locale-dropdown">
            <LocaleMenuWithFlags currentLocale={props.currentLocale} onClick={handleLocaleChange} />
          </div>
        </Navbar>
      )}
    </div>
  ) : null;
};

const mapStateToProps = ({ locale }: IRootState) => ({
  currentLocale: locale?.currentLocale as 'et' | 'en' | 'fi',
});

const mapDispatchToProps = { setLocale };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Header);
