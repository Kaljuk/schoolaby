import React from 'react';
import './header.scss';

interface IAvatarProps {
  fullName?: string;
  showName?: boolean;
  className?: string;
  avatarContainerClassName?: string;
}

const Avatar = ({ fullName, showName, className, avatarContainerClassName }: IAvatarProps) => {
  const getInitials = () => {
    const names = fullName.split(/\s/g);
    return ((names[0][0] || '') + (names[names.length - 1][0] || '')).toUpperCase();
  };

  return (
    <div className={`d-flex avatar-container align-items-center ${className || ''}`}>
      <div className={`rounded-circle avatar ${avatarContainerClassName || ''}`}>
        <span aria-hidden={'true'}>{getInitials()}</span>
      </div>
      {showName && (
        <span data-testid={'user-fullname'} className={'ml-2 d-flex text-secondary align-items-center full-name'}>
          {fullName}
        </span>
      )}
    </div>
  );
};

export default Avatar;
