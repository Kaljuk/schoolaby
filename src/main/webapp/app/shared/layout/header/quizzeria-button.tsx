import React, { useEffect, useState } from 'react';
import { Button } from 'reactstrap';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { AUTHORITIES } from 'app/config/constants';
import { v4 as uuidv4 } from 'uuid';
import { refreshAuthentication } from 'app/shared/services/authentication-api';

const QuizzeriaButton = ({ isAllowedToModify, authorities }: StateProps) => {
  const [accessToken, setAccessToken] = useState<string>();

  useEffect(() => {
    if (accessToken == null && isAllowedToModify) {
      refreshAuthentication().then(jwtResponse => {
        setAccessToken(jwtResponse?.data?.id_token);
      });
    }
  }, [authorities]);

  return isAllowedToModify ? (
    <form action={'/api/lti-advantage/launch'} method={'POST'}>
      <input name={'access_token'} type={'hidden'} value={accessToken} />
      <input name={'appClientId'} type={'hidden'} value={'schoolaby-or-school-or-teacher'} />
      <input name={'ltiConfigType'} type={'hidden'} value={'PLATFORM'} />
      <input name={'resourceLinkId'} type={'hidden'} value={uuidv4()} />
      <Button type={'submit'} className={'d-flex align-items-center'} formTarget={'_blank'}>
        <img alt="Quizzeria logo" src="/content/images/lti/quizzeria.svg" />
      </Button>
    </form>
  ) : null;
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  isAllowedToModify: hasAnyAuthority(authentication?.account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
  authorities: authentication?.account?.authorities,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(QuizzeriaButton);
