import React from 'react';
import { NavbarBrand } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';

export const Brand = props => (
  <NavbarBrand tag={Link} to="/" className={'mr-lg-2'} data-tour="logo">
    <div {...props}>
      <img src="content/images/logo.svg" alt="Schoolaby" className="brand-icon" />
    </div>
  </NavbarBrand>
);
