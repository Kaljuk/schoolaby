import { DateTime } from 'luxon';
import { ITimeSent } from 'app/shared/layout/header/notification/notification';
import { INotification } from 'app/shared/model/notification.model';
import uniqBy from 'lodash/uniqBy';

export const getDiffFromToday = (date: string): ITimeSent => {
  const formattedDate = DateTime.fromISO(date);
  const today = DateTime.local();
  const diff = today.diff(formattedDate, ['months', 'days', 'hours', 'minutes']);

  if (diff.values?.months) {
    return {
      time: diff.values.months,
      translationKey: 'schoolabyApp.notification.time.monthsAgo',
    };
  } else if (diff.values?.days) {
    return {
      time: diff.values.days,
      translationKey: 'schoolabyApp.notification.time.daysAgo',
    };
  } else if (diff.values?.hours) {
    return {
      time: diff.values.hours,
      translationKey: 'schoolabyApp.notification.time.hoursAgo',
    };
  } else if (Math.round(diff.values?.minutes)) {
    return {
      time: Math.round(diff.values.minutes),
      translationKey: 'schoolabyApp.notification.time.minutesAgo',
    };
  } else {
    return { translationKey: 'schoolabyApp.notification.time.now' };
  }
};

export const filterNotifications = (notifications: INotification[], subString: string): INotification[] =>
  notifications.filter(
    n =>
      n.creatorName?.toLowerCase().includes(subString) ||
      n.journeyName?.toLowerCase().includes(subString) ||
      n.assignmentName?.toLowerCase().includes(subString)
  );

export const getUniqueNotificationsByLink = (notifications: INotification[]): INotification[] =>
  uniqBy(notifications, notification => notification.link);

export const filterUnreadTeacherNotificationsByJourneyId = (notifications: INotification[], id: number) =>
  notifications.filter(
    notification =>
      ['{SUBMITTED_SOLUTION}', '{RECEIVED_MESSAGE}', '{JOINED_JOURNEY}'].includes(notification.message) &&
      notification.journeyId === id &&
      !notification.read
  );

export const getUnreadTeacherNotificationsCount = (notifications: INotification[], journeyId: number): number => {
  const uniqueNotifications = getUniqueNotificationsByLink(notifications);
  const unreadTeacherNotifications = filterUnreadTeacherNotificationsByJourneyId(uniqueNotifications, journeyId);
  return unreadTeacherNotifications.length;
};
