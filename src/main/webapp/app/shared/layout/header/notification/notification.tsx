import React, { memo } from 'react';
import { Col, Row } from 'reactstrap';
import Avatar from 'app/shared/layout/header/avatar';
import { useHistory } from 'react-router-dom';
import { translate } from 'react-jhipster';
import { INotification } from 'app/shared/model/notification.model';
import { getDiffFromToday } from 'app/shared/layout/header/notification/notification-util';

export interface ITimeSent {
  time?: number;
  translationKey: string;
}

interface IProps {
  notification: INotification;
  onClick?: React.MouseEventHandler<any>;
}

const Notification = ({ notification, onClick }: IProps) => {
  const history = useHistory();

  const handleOnClick = e => {
    onClick(e);
    e.preventDefault();
    history.push(notification.link);
  };

  const getDisplayTime = (): string => {
    const timeSent = getDiffFromToday(notification.createdDate);
    return timeSent.time ? timeSent.time + ` ${translate(timeSent.translationKey)}` : `${translate(timeSent.translationKey)}`;
  };

  const displayTitle = (): string => {
    switch (notification.message) {
      case '{RECEIVED_MESSAGE}':
        return notification.assignmentName ? notification.assignmentName : `„${translate('schoolabyApp.chat.generalChat')}“`;
      case '{SUBMITTED_SOLUTION}':
        return notification.assignmentName;
      case '{HAS_REJECTED}':
        return notification.assignmentName;
      case '{GIVEN_FEEDBACK}':
        return notification.assignmentName;
      case '{ASSIGNMENT_DEADLINE_TODAY}':
        return notification.assignmentName;
      case '{JOINED_JOURNEY}':
        return null;
      default:
        return notification.assignmentName;
    }
  };

  return (
    <Row className={'w-100 pt-2 pb-2 notification-row px-3' + (notification.read ? ' read' : '')} onClick={e => handleOnClick(e)}>
      {notification.creatorName ? (
        <Col xs="2" className={'pr-0 avatar-row'}>
          <Avatar fullName={notification.creatorName} />
        </Col>
      ) : null}
      <Col xs="10" className={'notification-information-row'}>
        {notification.creatorName || notification.groupName ? (
          <h6 className={'m-0 d-inline' + (notification.read ? ' text-secondary' : '')}>
            {notification.groupName ? notification.groupName : notification.creatorName}
          </h6>
        ) : null}
        <span className={notification.read ? ' text-secondary' : ''}>
          {' '}
          {translate(`schoolabyApp.notification.${notification.message}`)}{' '}
        </span>
        <div className={'h6 journey-title mb-0' + (notification.read ? ' text-secondary' : '')}>{displayTitle()}</div>
        <div className={'text-secondary'}>{getDisplayTime()}</div>
      </Col>
    </Row>
  );
};

export default memo(Notification);
