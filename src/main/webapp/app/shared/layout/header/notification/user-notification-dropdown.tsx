import React, { memo, useEffect, useState } from 'react';
import { translate } from 'react-jhipster';
import Icon from 'app/shared/icons';
import { ICON_GREY } from 'app/shared/util/color-utils';
import { useGetNotifications } from 'app/shared/services/notification-api';
import NotificationDropdown from 'app/shared/layout/header/notification/notification-dropdown';
import { useUserNotificationDropdownState } from 'app/shared/contexts/user-notification-context';

const UserNotificationDropdown = () => {
  const [unreadNotificationCount, setUnreadNotificationCount] = useState<number>(0);
  const { dropdownOpen, setDropdownOpen, journeyId } = useUserNotificationDropdownState();
  const { data: notifications } = useGetNotifications({ type: 'GENERAL', sort: 'id,desc' });

  useEffect(() => {
    setUnreadNotificationCount(notifications?.filter(d => !d.read).length);
  }, [notifications]);

  const NotificationIcon = (): JSX.Element => {
    return <Icon name={'bell'} width={'18px'} height={'22px'} stroke={ICON_GREY} />;
  };
  const NotificationsTitle = (): JSX.Element => {
    return <h3>{translate('global.header.notifications')}</h3>;
  };
  return (
    <span data-tour="notifications">
      <NotificationDropdown
        notifications={notifications ? notifications : []}
        icon={<NotificationIcon />}
        title={<NotificationsTitle />}
        unreadCount={unreadNotificationCount}
        dropdownOpen={dropdownOpen}
        setDropdownOpen={setDropdownOpen}
        journeyId={journeyId}
      />
    </span>
  );
};

export default memo(UserNotificationDropdown);
