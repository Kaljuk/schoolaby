import React, { FC, memo, useState } from 'react';
import { Nav, NavItem, UncontrolledTooltip } from 'reactstrap';
import MessageNotificationDropdown from 'app/shared/layout/header/notification/message-notification-dropdown';
import AvatarMenuItem from 'app/shared/layout/header/avatar-menu-item';
import UserNotificationDropdown from 'app/shared/layout/header/notification/user-notification-dropdown';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ICON_GREY } from 'app/shared/util/color-utils';
import SupportModal from 'app/shared/layout/support-modal/support-modal';
import { translate } from 'react-jhipster';

const RightMenu: FC = () => {
  const [displaySupportModal, setDisplaySupportModal] = useState<boolean>(false);

  const toggleSupportModal = () => {
    setDisplaySupportModal(prev => !prev);
  };

  return (
    <>
      <Nav className="ml-auto right-menu">
        <MessageNotificationDropdown />
        <UserNotificationDropdown />
        <div id="schoolaby-support" className={'p-2 hand'} onClick={toggleSupportModal}>
          <FontAwesomeIcon
            aria-label={translate('schoolabyApp.support.sendSupportMessage')}
            icon={['fas', 'question']}
            width={'18px'}
            height={'22px'}
            color={ICON_GREY}
          />
          <UncontrolledTooltip target="schoolaby-support">{translate('schoolabyApp.support.sendSupportMessage')}</UncontrolledTooltip>
        </div>
        <NavItem className="d-flex">
          <AvatarMenuItem />
        </NavItem>
      </Nav>
      <SupportModal isOpen={displaySupportModal} toggle={toggleSupportModal} />
    </>
  );
};

export default memo(RightMenu);
