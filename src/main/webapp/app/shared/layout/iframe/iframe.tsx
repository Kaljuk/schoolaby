import React, { DetailedHTMLProps, IframeHTMLAttributes, memo } from 'react';

const Iframe = (props: DetailedHTMLProps<IframeHTMLAttributes<HTMLIFrameElement>, HTMLIFrameElement>) => {
  const { src, id } = props;
  return (
    <>
      <iframe id={`iframe-${id}`} {...props} />
      <form action={src} method="get" target={`iframe-${id}`} />
    </>
  );
};

export default memo(Iframe);
