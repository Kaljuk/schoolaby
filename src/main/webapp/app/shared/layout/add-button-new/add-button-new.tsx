import React, { FC } from 'react';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import './add-button-new.scss';

interface AddButtonNewProps {
  title: string;
  onClick: () => void;
  className?: string;
  showIcon?: boolean;
}
const AddButtonNew: FC<AddButtonNewProps> = ({ title, onClick, className, showIcon, ...other }) => (
  <CustomButton
    title={title}
    onClick={onClick}
    className={`add-button border-0 ${className ? className : ''}`}
    buttonType="primary"
    iconName={showIcon && 'add'}
    {...other}
  />
);

export default AddButtonNew;
