import React, { FC, ReactNode } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, FormFeedback } from 'reactstrap';
import { translate } from 'react-jhipster';

interface ConfirmationModalProps {
  isModalOpen: boolean;
  closeModal: () => void;
  modalTitle: string;
  modalContent: string;
  confirmationButtonDisabled?: boolean;
  handleConfirmation?: () => void;
  customCancelButton?: ReactNode;
  customConfirmationButton?: ReactNode;
  errorMessage?: string;
}

const ConfirmationModal: FC<ConfirmationModalProps> = ({
  isModalOpen,
  closeModal,
  modalTitle,
  modalContent,
  confirmationButtonDisabled,
  handleConfirmation,
  customCancelButton,
  customConfirmationButton,
  errorMessage,
}) => {
  return (
    <Modal isOpen={isModalOpen} toggle={closeModal}>
      <ModalHeader>{modalTitle}</ModalHeader>
      <ModalBody>
        <p>{modalContent}</p>
        {errorMessage && <FormFeedback className={'form-feedback'}>{errorMessage}</FormFeedback>}
      </ModalBody>
      <ModalFooter>
        {customCancelButton ? (
          customCancelButton
        ) : (
          <Button color="secondary" onClick={closeModal}>
            {translate('entity.action.cancel')}
          </Button>
        )}
        {customConfirmationButton ? (
          customConfirmationButton
        ) : (
          <Button onClick={handleConfirmation} color="primary" disabled={confirmationButtonDisabled}>
            {translate('global.yes')}
          </Button>
        )}
      </ModalFooter>
    </Modal>
  );
};

export default ConfirmationModal;
