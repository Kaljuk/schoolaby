import React, { FC } from 'react';
import './tabs.scss';
import { ASSIGNMENT, JOURNEY } from 'app/shared/util/entity-utils';
import JourneyTabs from 'app/shared/layout/tabs/journey-tabs';
import AssignmentTabs from './assignment-tabs';

export const JOURNEY_TAB = 'journey';
export const GRADES_TAB = 'grades';
export const APPS_TAB = 'apps';
export const SUBMISSIONS_TAB = 'submissions';
export const ASSIGNMENT_TAB = 'assignment';
export const BACKPACK_TAB = 'backpack';
export const ANALYTICS_TAB = 'analytics';

export type TabEntityType = typeof JOURNEY | typeof ASSIGNMENT;

export interface TabsProps {
  entityType: TabEntityType;
  newAssignmentViewDesign?: boolean; // TODO: SCHOOL-970 Remove
}

export type TabType =
  | typeof JOURNEY_TAB
  | typeof GRADES_TAB
  | typeof APPS_TAB
  | typeof SUBMISSIONS_TAB
  | typeof ASSIGNMENT_TAB
  | typeof BACKPACK_TAB
  | typeof ANALYTICS_TAB;

const Tabs: FC<TabsProps> = ({ entityType, newAssignmentViewDesign = false }) => {
  if (entityType === JOURNEY) {
    return <JourneyTabs />;
  } else if (entityType === ASSIGNMENT) {
    return <AssignmentTabs newAssignmentViewDesign={newAssignmentViewDesign} />;
  }

  return null;
};

export default Tabs;
