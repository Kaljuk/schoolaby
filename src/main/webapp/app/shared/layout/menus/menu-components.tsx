import React, { CSSProperties, FC } from 'react';

import { DropdownMenu, DropdownToggle, UncontrolledDropdown } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ICON_GREY } from 'app/shared/util/color-utils';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

interface NavDropdownProps {
  id?: string;
  caret?: boolean;
  icon?: IconProp;
  name: string;
  style?: CSSProperties;
  children: JSX.Element | JSX.Element[];
}
export const NavDropdown: FC<NavDropdownProps> = ({ id, caret, icon, name, style, children }) => (
  <UncontrolledDropdown nav inNavbar id={id} tag="div">
    <DropdownToggle nav caret={caret} className="d-flex align-items-center pl-1 pr-2">
      {icon && <FontAwesomeIcon icon={icon} />}
      <span style={{ fontWeight: 500, color: ICON_GREY }}>{name}</span>
    </DropdownToggle>
    <DropdownMenu right style={style}>
      {children}
    </DropdownMenu>
  </UncontrolledDropdown>
);
