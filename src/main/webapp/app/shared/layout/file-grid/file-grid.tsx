import React, { FC, useEffect, useMemo, useRef, useState } from 'react';

import './file-grid.scss';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import FileGridItem from 'app/shared/layout/file-grid/file-grid-item/file-grid-item';
import FilePreviewModal from 'app/shared/form/file-preview-modal';
import { usePreviewFileState } from 'app/shared/contexts/uploaded-file-context';
import { createDownloadLink, isPreviewableFile } from 'app/shared/util/file-utils';
import { downloadUploadedFile } from 'app/shared/services/uploaded-file-api';

interface FileGridProps {
  files: IUploadedFile[];
  className?: string;
}

const FileGrid: FC<FileGridProps> = ({ files, className = '' }) => {
  const gridRef = useRef(null);
  const [gridWidth, _setGridWidth] = useState<number>(0);
  const [maxGridItems, _setMaxGridItems] = useState<number>(1);

  const [showAll, setShowAll] = useState(false);
  const [filesToShow, setFilesToShow] = useState<IUploadedFile[]>([]);

  const { setPreviewFile } = usePreviewFileState();

  const setGridWidth = () => gridRef?.current && _setGridWidth(gridRef.current.getBoundingClientRect().width);

  const setMaxGridItems = () => {
    if (gridWidth > 1065) {
      _setMaxGridItems(11);
    } else if (gridWidth > 870) {
      _setMaxGridItems(9);
    } else if (gridWidth > 691) {
      _setMaxGridItems(7);
    } else if (gridWidth > 565) {
      _setMaxGridItems(6);
    } else if (gridWidth > 487) {
      _setMaxGridItems(5);
    } else if (gridWidth > 401) {
      _setMaxGridItems(4);
    } else if (gridWidth > 362) {
      _setMaxGridItems(3);
    } else {
      _setMaxGridItems(2);
    }
  };

  useEffect(() => {
    setGridWidth();
    window.addEventListener('resize', setGridWidth);

    return () => window.removeEventListener('resize', setGridWidth);
  }, []);

  useEffect(() => {
    !showAll && setMaxGridItems();
  }, [gridWidth, showAll]);

  useEffect(() => {
    !showAll && setFilesToShow(files?.slice(0, maxGridItems - 1));
  }, [files, showAll, maxGridItems]);

  const downloadFile = (file: IUploadedFile) =>
    downloadUploadedFile(file?.uniqueName).then(response => {
      const link = createDownloadLink(response);
      link.download = file?.originalName;
      link.click();
    });

  const handleOnGridItemClick = (file: IUploadedFile) => {
    isPreviewableFile(file) ? setPreviewFile(file) : downloadFile(file);
  };

  const memoizedInitialFiles = () =>
    useMemo(
      () =>
        !!filesToShow?.length &&
        filesToShow.map(file => (
          <FileGridItem key={`student-upload-${file.uniqueName}`} file={file} onClick={() => handleOnGridItemClick(file)} />
        )),
      [filesToShow]
    );

  const gridItemWithOverlay = () =>
    files?.length >= maxGridItems &&
    !showAll && (
      <FileGridItem file={files[maxGridItems - 1]} onClick={() => setShowAll(true)} overlay={!showAll && files.length - maxGridItems} />
    );

  const remainingFiles = () =>
    files?.length >= maxGridItems &&
    showAll &&
    files
      .slice(maxGridItems - 1, files.length + 1)
      .map(file => <FileGridItem key={`student-upload-${file.uniqueName}`} file={file} onClick={() => handleOnGridItemClick(file)} />);

  return (
    <div ref={gridRef} className={`file-grid ${gridWidth <= 362 ? 'full-width-items' : ''} ${className}`}>
      {memoizedInitialFiles()}
      {gridItemWithOverlay()}
      {remainingFiles()}
      <FilePreviewModal files={files} canEdit />
    </div>
  );
};

export default FileGrid;
