import './lti-modal.scss';
import React, { useEffect, useMemo, useRef, useState } from 'react';
import { Button, Col, ModalBody, ModalFooter, Row } from 'reactstrap';
import * as yup from 'yup';
import { getLtiAppLaunchParameters } from 'app/shared/services/lti-api';
import { Storage, translate } from 'react-jhipster';
import { createYupValidator } from 'app/shared/util/form-utils';
import { Field, Form } from 'react-final-form';
import { Input, Switch } from 'app/shared/form';
import { ILtiResourceParameters } from 'app/shared/layout/lti/lti-modal';
import { ILtiResource } from 'app/shared/model/lti-resource.model';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import { isLtiAdvantage } from 'app/shared/util/lti-utils';
import { v4 as uuidv4 } from 'uuid';
import { useMaterialState } from 'app/shared/contexts/material-context';
import { Material } from 'app/shared/model/material.model';
import { ILineItem } from 'app/shared/model/line-item.model';
import { ContentItemSelection } from 'app/lti/content-item-selection';
import CustomModal from 'app/shared/layout/custom-modal/custom-modal';
import CloseButton from 'app/shared/layout/close-button/close-button';

export const LTI_MODAL_ACTION_CREATE = 'CREATE';
export const LTI_MODAL_ACTION_UPDATE = 'UPDATE';
export type LtiCreateModalAction = 'CREATE' | 'UPDATE';

export type ILtiCreateModal = {
  title?: string;
  showModal: boolean;
  ltiResource: ILtiResource;
  onClose: () => void;
  journeyId: number;
  action?: LtiCreateModalAction;
  className?: string;
};

/* eslint-disable @typescript-eslint/camelcase */
interface ILtiAdvantageDeepLinkingParameters {
  launch_url: string;
  access_token: string;
  appId: number;
  journeyId: number;
  message_type?: string;
}

export const LtiCreateModal = ({ title, showModal, onClose, ltiResource, journeyId, action }: ILtiCreateModal) => {
  const [ltiParameters, setLtiParameters] = useState<ILtiResourceParameters | ILtiAdvantageDeepLinkingParameters>();
  const [ltiResourceTitle, setLtiResourceTitle] = useState<string>();
  const [ltiError, setLtiError] = useState<string>();
  const [ltiResourceDescription, setLtiResourceDescription] = useState<string>();
  const [ltiResourceLinkId, setLtiResourceLinkId] = useState<string>();
  const [ltiResourceMaxScore, setLtiResourceMaxScore] = useState<number>();
  const [deepLinkingResponse, setDeepLinkingResponse] = useState(null);
  const [syncGrade, setSyncGrade] = useState<boolean>(false);
  const { materials, setMaterials } = useMaterialState();
  const [spinner, setSpinner] = useState(true);
  const formRef = useRef<HTMLFormElement>();

  const getExistingLtiResourceIndex = () => {
    return materials.findIndex(m => m?.ltiResource?.resourceLinkId === ltiResource?.resourceLinkId);
  };

  const removeLtiResource = () => {
    const existingLtiResourceIndex = getExistingLtiResourceIndex();
    if (existingLtiResourceIndex !== -1) {
      materials.splice(existingLtiResourceIndex, 1);
    }
  };

  const is1vp0LineItem = (extLineItem): boolean => extLineItem && !('scoreMaximum' in extLineItem);

  const convertExternalLineItem = (extLineItem): ILineItem[] => {
    const convertedLineItems: ILineItem[] = [];
    if (extLineItem) {
      if (is1vp0LineItem(extLineItem)) {
        convertedLineItems.push({
          scoreMaximum: extLineItem?.scoreConstraints?.totalMaximum,
          label: deepLinkingResponse?.detail[0].title,
        });
      } else {
        convertedLineItems.push(extLineItem);
      }
    }
    return convertedLineItems;
  };

  const addLtiResource = values => {
    ltiResource.title = values.ltiResourceTitle;
    ltiResource.description = values.ltiResourceDescription;
    ltiResource.syncGrade = values.syncGrade;
    ltiResource.resourceLinkId = values.ltiResourceLinkId;
    ltiResource.lineItems = convertExternalLineItem(deepLinkingResponse?.detail[0].lineItem);
    ltiResource.toolResourceId = deepLinkingResponse?.detail[0]?.lineItem?.resourceId || ltiResource.toolResourceId;
    ltiResource.launchUrl = deepLinkingResponse?.detail[0]?.url || ltiResource.launchUrl;
    setMaterials([...materials, Material.fromLtiResource(ltiResource)]);
  };

  const resetFormFields = () => {
    setLtiResourceTitle(undefined);
    setLtiResourceDescription(undefined);
    setLtiResourceMaxScore(undefined);
    setLtiResourceLinkId(undefined);
  };

  const onSubmit = values => {
    removeLtiResource();
    addLtiResource(values);
    onClose();
  };

  const onDelete = () => {
    removeLtiResource();
    onClose();
  };

  const initLtiResourceParameters = (appVersion: string): void => {
    if (isLtiAdvantage(appVersion)) {
      setLtiParameters({
        launch_url: '/api/lti-advantage/deep-linking',
        access_token: Storage.local.get('jhi-authenticationToken'),
        appId: ltiResource.ltiApp.id,
        journeyId: +journeyId,
      });
    } else {
      getLtiAppLaunchParameters({
        ltiAppId: ltiResource?.ltiApp?.id,
        journeyId: +journeyId,
        ltiResourceLinkId: ltiResource?.resourceLinkId,
      })
        .then(response => setLtiParameters(response.data))
        .catch(err => {
          setLtiError(err?.response?.data?.detail);
        });
    }
  };

  const handleDeepLinkingResponse = e => {
    setDeepLinkingResponse(e);
    setLtiResourceLinkId(uuidv4());
    setLtiResourceTitle(e.detail[0]?.title);
    setLtiResourceDescription(e.detail[0]?.text);
    setLtiResourceMaxScore(convertExternalLineItem(e.detail[0]?.lineItem)[0].scoreMaximum);
  };

  useEffect(() => {
    window.document.addEventListener('deepLinkingResponse', handleDeepLinkingResponse);
    return () => {
      window.document.removeEventListener('deepLinkingResponse', handleDeepLinkingResponse);
    };
  });

  useEffect(() => {
    if (ltiParameters) {
      formRef.current.submit();
      setSpinner(false);
    } else if (ltiError) {
      setSpinner(false);
    }
  }, [ltiParameters, ltiError]);

  useEffect(() => {
    !ltiResource?.resourceLinkId && resetFormFields();
    if (showModal) {
      setSpinner(true);
      initLtiResourceParameters(ltiResource.ltiApp.version);
    } else {
      setDeepLinkingResponse(null);
      setSpinner(false);
    }
  }, [showModal]);

  useEffect(() => {
    ltiResource?.title && setLtiResourceTitle(ltiResource.title);
  }, [ltiResource?.title]);

  useEffect(() => {
    ltiResource?.description && setLtiResourceDescription(ltiResource.description);
  }, [ltiResource?.description]);

  useEffect(() => {
    setSyncGrade(ltiResource?.syncGrade);
  }, [ltiResource?.syncGrade]);

  useEffect(() => {
    ltiResource?.resourceLinkId && setLtiResourceLinkId(ltiResource.resourceLinkId);
  }, [ltiResource?.resourceLinkId]);

  useEffect(() => {
    setLtiResourceMaxScore(ltiResource?.lineItems?.length && ltiResource?.lineItems[0]?.scoreMaximum);
  }, [ltiResource?.lineItems?.length && ltiResource?.lineItems[0]?.scoreMaximum]);

  const createLtiResourceSchema = () => {
    return useMemo(
      () =>
        yup.object().shape({
          ltiResourceTitle: yup.string().required(translate('entity.validation.required')),
          ltiResourceLinkId: yup.string().required(translate('entity.validation.required')),
        }),
      [ltiResourceTitle, ltiResourceLinkId]
    );
  };

  const getFooterButtons = () => {
    if (action === LTI_MODAL_ACTION_CREATE) {
      return (
        <Button color="primary" type={'submit'} className={'ml-3'}>
          {translate('global.form.chooseMaterial')}
        </Button>
      );
    }
    if (action === LTI_MODAL_ACTION_UPDATE) {
      return (
        <>
          <Button color="danger" onClick={onDelete}>
            {translate('global.form.removeChosen')}
          </Button>
          <Button color="primary" type={'submit'} className={'ml-3'}>
            {translate('global.form.updateMaterial')}
          </Button>
        </>
      );
    }
  };

  const ltiResourceSchema = createLtiResourceSchema();

  const initialValues = {
    ltiResourceTitle,
    ltiResourceLinkId,
    ltiResourceDescription,
    ltiResourceMaxScore,
    syncGrade,
  };

  const modal = (
    <CustomModal backdrop="static" isOpen={showModal} className="modal-lg lti-content-modal border border-0 p-2">
      <Form onSubmit={onSubmit} validate={createYupValidator(ltiResourceSchema)} initialValues={initialValues}>
        {({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <ModalBody className="pl-4 pr-4 pb-4">
              <Row>
                <Col className={'d-flex'} xs={'9'} s={'10'}>
                  <h2>{title}</h2>
                </Col>
                <Col className={'align-items-center d-flex'} xs={'3'} s={'9'}>
                  <CloseButton onClick={onClose} />
                </Col>
              </Row>
              <Row className="mt-4 mb-3">
                {useMemo(
                  () => (
                    <Col>
                      {!deepLinkingResponse && !ltiError && (
                        <div>
                          <iframe name={'lti-content'} width={'100%'} style={{ height: '60vh' }} allowFullScreen />
                        </div>
                      )}
                      {(deepLinkingResponse || ltiError) && <ContentItemSelection successful={!ltiError} />}
                    </Col>
                  ),
                  [ltiError, deepLinkingResponse]
                )}
              </Row>
              <Row>
                <Col xs={'12'}>
                  <Row>
                    <Col xs={12} md={6}>
                      <h6 id="titleLabel">{translate('schoolabyApp.assignment.lti.title')}</h6>
                      <Field id="lti-resource-title" type="text" name="ltiResourceTitle" component={Input} />
                      <h6 id="resourceLinkIdLabel">{translate('schoolabyApp.assignment.lti.resourceLinkId')}</h6>
                      <Field id="lti-resource-id" type="text" name="ltiResourceLinkId" disabled component={Input} />
                      {ltiResourceMaxScore !== undefined && (
                        <>
                          <h6 id="resourceMaxScoreLabel">{translate('schoolabyApp.assignment.lti.maximumScore')}</h6>
                          <Field id="lti-resource-max-score" type="text" name="ltiResourceMaxScore" disabled component={Input} />
                        </>
                      )}
                    </Col>
                    <Col xs={12} md={6}>
                      <h6 id="descriptionLabel">{translate('schoolabyApp.assignment.lti.description')}</h6>
                      <Field id="lti-resource-description" type="textarea" name="ltiResourceDescription" component={Input} />
                      <h6 id="syncGradeLabel">{translate('schoolabyApp.assignment.lti.syncGrade')}</h6>
                      <Field id="lti-resource-sync-grade" type="checkbox" name="syncGrade" component={Switch} />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Row className={'my-3'}>{getFooterButtons()}</Row>
            </ModalFooter>
          </form>
        )}
      </Form>
    </CustomModal>
  );

  const renderLtiResourceForm = ltiParameters ? (
    <form action={ltiParameters.launch_url} method={'post'} target={'lti-content'} ref={formRef}>
      {Object.entries(ltiParameters).map(([name, value]) => (
        <input key={`input-${name}`} name={name} type={'hidden'} value={value} />
      ))}
    </form>
  ) : null;

  return ltiResource?.ltiApp ? (
    <>
      <Spinner loading={spinner} />
      {modal}
      {renderLtiResourceForm}
    </>
  ) : null;
};
