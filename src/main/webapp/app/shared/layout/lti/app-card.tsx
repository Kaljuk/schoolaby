import './app-card.scss';
import React, { FC, useEffect, useState } from 'react';
import Icon from 'app/shared/icons';
import { Col, Dropdown, DropdownToggle, Row, UncontrolledTooltip } from 'reactstrap';
import { translate } from 'react-jhipster';
import LinesEllipsis from 'react-lines-ellipsis';
import { ICON_GREY, P_ELEMENT, RED, WARNING } from 'app/shared/util/color-utils';
import { isLtiConfigured } from 'app/shared/util/lti-utils';
import AddButton from 'app/shared/layout/add-button/add-button';

export interface IAppCardProps {
  xs?: string;
  md?: string;
  lg?: string;
  xl?: string;
  key?: number;
  dropDown?: JSX.Element;
  onClick?: (ILtiApp) => void;
  imageSrc: string;
  title: string;
  description: string;
  consumerKey?: string;
  sharedSecret?: string;
  ltiVersion?: string;
  provided?: any;
  gradable?: boolean;
  previewEnabled?: boolean;
}

const AppCard: FC<IAppCardProps> = ({
  imageSrc,
  title,
  dropDown,
  description,
  consumerKey,
  sharedSecret,
  ltiVersion,
  xs,
  md,
  lg,
  xl,
  onClick,
  provided,
  gradable,
  previewEnabled,
}) => {
  useEffect(() => {}, [description]);

  const appDropdown = () => {
    const [dropdownOpen, setDropdownOpen] = useState(false);
    const toggleDropDown = e => {
      setDropdownOpen(prevState => !prevState);
      e.stopPropagation();
    };

    return dropDown && !previewEnabled ? (
      <Dropdown toggle={toggleDropDown} className={'float-right'} isOpen={dropdownOpen} aria-label={translate('global.heading.dropdown')}>
        <DropdownToggle className={'p-0 mb-2'}>
          <Icon name={'dropdown'} width={'16px'} height={'16px'} stroke={ICON_GREY} fill={ICON_GREY} />
        </DropdownToggle>
        {dropDown}
      </Dropdown>
    ) : null;
  };

  const cardLeftContent = () => {
    return <img className={'p-4 mt-4 mt-sm-0'} src={imageSrc} alt={title} />;
  };

  const cardRightContent = () => {
    return (
      <div>
        <Row>
          <Col xs={dropDown ? '9' : '12'}>
            <h3>
              <LinesEllipsis text={title} maxLine={1} />
            </h3>
          </Col>
          <Col xs={'3'} className={dropDown ? 'd-flex align-items-center justify-content-end' : 'd-none'}>
            {appDropdown()}
          </Col>
        </Row>
        {gradable && (
          <Row noGutters>
            <Col xs={12}>
              <h6 className={'d-flex align-items-center text-warning'}>
                <Icon name={'exclamation'} stroke={WARNING} height="22px" width="22px" />
                <span className={'ml-1'}>{translate('schoolabyApp.assignment.lti.gradable')}</span>
              </h6>
            </Col>
          </Row>
        )}
        {description && (
          <div>
            <LinesEllipsis text={description} maxLine={4} style={{ color: P_ELEMENT }} />
          </div>
        )}
      </div>
    );
  };

  return (
    <span
      onClick={onClick}
      className={`app-card-container mb-4 col-xs-${xs || 12} col-md-${md || 12} col-lg-${lg || 12} col-xl-${xl || 12}`}
      ref={provided?.innerRef}
      {...provided?.draggableProps}
      {...provided?.dragHandleProps}
      data-testid={'app-card'}
    >
      <Row noGutters className={'base-card app-card rounded border w-100'}>
        {!previewEnabled && (
          <AddButton
            orientation={'down'}
            label={translate('global.form.addNewMaterialBtn')}
            onClick={() => {}}
            className={`add-hover-btn`}
          />
        )}
        {!isLtiConfigured(ltiVersion, consumerKey, sharedSecret) && (
          <span className={'card-lti-warning'}>
            <UncontrolledTooltip placement="right" target="card-lti-warning">
              {translate('global.overlayWarning.ltiNotConfiguredWarning')}
            </UncontrolledTooltip>

            <span id="card-lti-warning">
              <Icon className={'position-absolute icon'} name={'exclamationMark'} width={'30px'} height={'30px'} stroke={RED} />
            </span>
          </span>
        )}
        <Col
          xs={12}
          sm={5}
          lg={3}
          className={'d-flex align-items-center justify-content-center d-none d-sm-flex left-content border-right'}
        >
          {cardLeftContent()}
        </Col>
        <Col xs={12} sm={7} lg={9} className={'p-3 right-content px-4'}>
          {cardRightContent()}
        </Col>
      </Row>
    </span>
  );
};

export default AppCard;
