import React, { useEffect, useMemo, useState } from 'react';
import { translate, TextFormat } from 'react-jhipster';
import AsyncSelect from 'react-select/async';
import { INFO } from 'app/shared/util/color-utils';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './group-search-dropdown.scss';
import useConstant from 'use-constant';
import { debounce } from 'lodash';
import { useSelectedGroupAssignmentState } from 'app/shared/contexts/entity-update-context';
import { IGroupAssignment } from 'app/shared/model/group-assignment.model';
import { useGetGroupAssignmentsBySearch } from 'app/shared/services/assignment-api';
import { APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { Col, Row } from 'reactstrap';
import { useQueryParam } from 'app/shared/hooks/useQueryParam';

interface IDropdownItem {
  label?: string;
  value?: number;
  data?: IGroupAssignment;
}

const GroupSearchDropdown = ({ disabled }) => {
  const [search, setSearch] = useState<string>('');
  const [{ journeyId }] = useQueryParam<{ journeyId: string }>();
  const { refetch: refetchGroupAssignments } = useGetGroupAssignmentsBySearch(journeyId, search);
  const { setSelectedGroupAssignment } = useSelectedGroupAssignmentState();

  useEffect(() => () => setSelectedGroupAssignment(undefined), []);

  const mapToDropdownItem = (assignment: IGroupAssignment): IDropdownItem => ({
    label: assignment?.title,
    value: assignment?.id,
    data: assignment,
  });

  const onChange = (value: IDropdownItem) => {
    setSelectedGroupAssignment(value.data);
  };

  const loadOptions = (inputValue, callback) => {
    refetchGroupAssignments().then(res => {
      callback(res.data?.map(assignment => mapToDropdownItem(assignment)));
    });
  };

  const memoizedLoadOptions = useConstant(() => debounce(loadOptions, 100));

  const CustomOption = ({ innerProps, data: { data: assignment } }) => (
    <Row {...innerProps} noGutters className={'p-2 dropdown-option'}>
      <Col md={6}>{assignment.title}</Col>
      <Col md={6} className={'text-muted text-right'}>
        {`(${assignment.groups.length} ${
          assignment.groups.length === 1
            ? translate('schoolabyApp.assignment.groupSelected')
            : translate('schoolabyApp.assignment.groupsSelected')
        }, ${translate('schoolabyApp.assignment.createdDate')} `}
        <TextFormat type="date" value={assignment.createdDate} format={APP_LOCAL_DATE_FORMAT} />
        {')'}
      </Col>
    </Row>
  );

  return useMemo(
    () => (
      <div className={'group-search-dropdown position-relative'}>
        <AsyncSelect
          isDisabled={disabled}
          classNamePrefix={'async-select'}
          className={'async-select'}
          noOptionsMessage={() => translate('entity.action.noResults')}
          placeholder={translate('schoolabyApp.assignment.member.searchGroups')}
          onInputChange={setSearch}
          loadOptions={memoizedLoadOptions}
          onChange={onChange}
          components={{ Option: CustomOption }}
        />
        <FontAwesomeIcon className={'position-absolute search-icon'} icon={['fas', 'search']} color={INFO} size={'lg'} />
      </div>
    ),
    [search, disabled]
  );
};

export default GroupSearchDropdown;
