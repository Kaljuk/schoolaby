import React, { FC } from 'react';
import { Spinner } from '../spinner/spinner';

interface SpinnerContainerProps {
  displaySpinner: boolean;
  spinnerClassName?: string;
}
const SpinnerContainer: FC<SpinnerContainerProps> = ({ displaySpinner, spinnerClassName, children }) =>
  displaySpinner ? <Spinner className={spinnerClassName} /> : <>{children}</>;

export default SpinnerContainer;
