import './add-button.scss';
import React, { FC } from 'react';
import { Button, ButtonProps } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

type Orientation = 'left' | 'right' | 'down';

interface AddButtonProps extends ButtonProps {
  onClick: React.MouseEventHandler<any>;
  label?: string;
  className?: string;
  orientation?: Orientation;
}

const AddButton: FC<AddButtonProps> = ({ orientation, label, onClick, className, ...other }) => {
  const isOrientationDown = orientation === 'down';

  return (
    <div className={'align-items-center ' + (className || '')}>
      <div className={`add-btn-container ${isOrientationDown ? '' : 'row no-gutters align-items-center'}`} onClick={onClick}>
        {label && orientation === 'left' && <span className="add-btn-label pr-2 pt-1">{label}</span>}
        <Button
          className={
            'btn-primary rounded-circle add-btn d-flex justify-content-center align-items-center' +
            (isOrientationDown ? ' d-block ml-auto mr-auto mb-2' : '')
          }
          type="button"
          {...other}
        >
          <FontAwesomeIcon icon={'plus'} />
        </Button>
        {label && orientation === 'right' && <span className="add-btn-label pl-2">{label}</span>}
        {label && isOrientationDown && <span className="add-btn-label">{label}</span>}
      </div>
    </div>
  );
};

AddButton.defaultProps = {
  orientation: 'left',
};

export default AddButton;
