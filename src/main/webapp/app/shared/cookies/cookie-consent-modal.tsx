import { Switch } from 'app/shared/form';
import React, { FC, useState } from 'react';
import CookieConsent from 'react-cookie-consent';
import { translate } from 'react-jhipster';
import { Col, Label, Row } from 'reactstrap';
import { Field, Form } from 'react-final-form';
import { OnChange } from 'react-final-form-listeners';

import './cookie-consent-modal.scss';

interface IProps {
  onAccept: () => void;
}

const CookieConsentModal: FC<IProps> = ({ onAccept }) => {
  const [cookieValue, setCookieValue] = useState<string>('necessary;');

  return (
    <CookieConsent
      disableStyles
      buttonText={translate('global.accept')}
      cookieName="schoolabyCCv2"
      cookieValue={cookieValue}
      containerClasses={'cookie-consent'}
      buttonClasses="btn btn-primary"
      expires={150}
      onAccept={onAccept}
    >
      <span className={'text-left'}>
        <span className={'d-block'}>{translate('global.cookieConsent')}</span>
        <span className={'terms mt-2 d-block'}>{translate('global.termsAndPrivacy')}</span>
        <Form onSubmit={() => {}}>
          {() => (
            <form className={'my-1'}>
              <Row>
                <Col xs={12} className={'d-flex align-items-start mt-2'}>
                  <Field
                    id={'necessaryCookies'}
                    type="checkbox"
                    name="necessaryCookies"
                    defaultValue={true}
                    component={Switch}
                    disabled
                    className={'d-inline-flex justify-content-start mt-1'}
                  />
                  <Label className={'d-inline ml-2'} for={'necessaryCookies'}>
                    {translate('global.necessaryCookies')}
                  </Label>
                </Col>
                <Col xs={12} className={'d-flex align-items-start mt-2'}>
                  <Field
                    id={'analyticalCookies'}
                    type="checkbox"
                    name="analyticalCookies"
                    component={Switch}
                    className={'d-inline-flex justify-content-start mt-1'}
                  />
                  <OnChange name="analyticalCookies">
                    {value => (value ? setCookieValue('necessary;analytics;') : setCookieValue('necessary;'))}
                  </OnChange>
                  <Label className={'d-inline ml-2'} for={'analyticalCookies'}>
                    {translate('global.analyticalCookies')}
                  </Label>
                </Col>
              </Row>
            </form>
          )}
        </Form>
      </span>
    </CookieConsent>
  );
};

export default CookieConsentModal;
