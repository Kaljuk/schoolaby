import './settings.scss';
import React, { useEffect, useState } from 'react';
import { Button, FormFeedback, Row, Alert } from 'reactstrap';
import { connect } from 'react-redux';
import { Storage, translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import { AUTH_TOKEN_KEY, getSession } from 'app/shared/reducers/authentication';
import { reset, saveAccountSettings } from './settings.reducer';
import { AUTHORITIES, COUNTRY } from 'app/config/constants';
import { refreshAuthentication } from 'app/shared/services/authentication-api';
import { useHistory } from 'react-router-dom';
import { useQueryParam } from 'app/shared/hooks/useQueryParam';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { countries } from 'app/config/translation';
import { Field, Form } from 'react-final-form';
import { Input } from 'app/shared/form';
import { createYupValidator } from 'app/shared/util/form-utils';
import { useSettingsSchema } from 'app/shared/schema/account';
import CountryChoiceField from 'app/account/common/country-choice-field';
import RoleChoiceField from 'app/account/common/role-choice-field';
import PersonRoleFieldArray from 'app/shared/layout/person-role-field-array/person-role-field-array';
import arrayMutators from 'final-form-arrays';
import { IUser } from 'app/shared/model/user.model';
import { getRoleBasedOnAuthority } from 'app/shared/util/auth-utils';
import { useFirstLogin } from 'app/shared/services/account-api';
import { useQueryClient } from 'react-query';
import { JOURNEYS_KEY } from 'app/config/reactQueryKeyConstants';
import { getFullName } from 'app/shared/util/string-utils';

interface FormUser extends IUser {
  authority: string;
}

export interface IUserSettingsProps extends StateProps, DispatchProps {}

export interface AccountSettingsHistoryStateProps {
  accountFieldsUnFilled: boolean;
  returnUrl: string;
}

export const SettingsPage = (props: IUserSettingsProps) => {
  const queryClient = useQueryClient();
  const history = useHistory();
  const [{ success }] = useQueryParam<{ success: string }>();
  const [oauthConnections, setOauthConnections] = useState([]);
  const googleConnected = oauthConnections.includes('google');
  const azureConnected = oauthConnections.includes('azure');
  const ekoolConnected = oauthConnections.includes('ekool');
  const [selectedCountry, setSelectedCountry] = useState<string>(COUNTRY.ESTONIA);
  const [currentAccount, setCurrentAccount] = useState<FormUser>(props.account);
  const { mutate: firstLogin } = useFirstLogin();
  const historyState: AccountSettingsHistoryStateProps = history.location.state as AccountSettingsHistoryStateProps;

  useEffect(() => {
    props.getSession();
    return () => {
      if (props?.account?.firstLogin) {
        firstLogin();
      }
      props.reset();
    };
  }, []);

  const updateJwt = async () => {
    const jwtResponse = await refreshAuthentication();
    if (jwtResponse?.data?.id_token) {
      const jwt = jwtResponse.data.id_token;
      Storage.local.set(AUTH_TOKEN_KEY, jwt);
    }
  };

  const handleValidSubmit = async values => {
    setCurrentAccount(values);
    const account = {
      ...props.account,
      ...values,
      langKey: countries.get(values.country)?.langKey,
      authorities: [...props.account.authorities.filter(authority => authority === AUTHORITIES.ADMIN), values.authority],
      personRoles: [...values.personRoles],
      termsAgreed: props?.account?.termsAgreed,
    };

    await Promise.resolve(props.saveAccountSettings(account)).then(async () => {
      queryClient.invalidateQueries(JOURNEYS_KEY);
      await updateJwt().then(() => history.push(historyState?.returnUrl || '/'));
    });
  };

  const getActiveRole = () => {
    let activeRole = AUTHORITIES.TEACHER;
    if (hasAnyAuthority(currentAccount.authority ? [currentAccount.authority] : props.account.authorities, [AUTHORITIES.TEACHER])) {
      activeRole = AUTHORITIES.TEACHER;
    }
    if (hasAnyAuthority(currentAccount.authority ? [currentAccount.authority] : props.account.authorities, [AUTHORITIES.STUDENT])) {
      activeRole = AUTHORITIES.STUDENT;
    }

    return activeRole;
  };

  const getPersonRoles = () => {
    return currentAccount.personRoles?.length
      ? currentAccount.personRoles.filter(personRole => personRole.role !== 'ADMIN')
      : [{ role: getRoleBasedOnAuthority(currentAccount.authority), school: {}, active: true }];
  };

  const initialValues = () => {
    return {
      ...currentAccount,
      country: currentAccount.country || COUNTRY.ESTONIA,
      authority: getActiveRole(),
      terms: props.hasRole,
      personRoles: getPersonRoles(),
    };
  };

  useEffect(() => {
    setOauthConnections([success].concat(props.account.externalAuthentications));
    props.account.country && setSelectedCountry(props.account.country);
    props.account && setCurrentAccount(initialValues());
  }, [success, props.account]);

  const renderRoleChoice = () => {
    if (props.account.authorities.find(a => a === AUTHORITIES.ADMIN)) {
      return;
    }

    return (
      <>
        <h3 className="my-4">{translate('global.form.role.selectYourMainRole')}</h3>
        <RoleChoiceField />
      </>
    );
  };

  return (
    <div className="d-flex flex-column align-items-center pt-3 settings">
      <h1>{translate('settings.title')}</h1>
      <img className="user-image rounded-circle bg-white mt-2" src="content/images/user-circle.png" alt="user-icon" />
      <Form
        onSubmit={handleValidSubmit}
        validate={createYupValidator(useSettingsSchema(props.locale))}
        initialValues={currentAccount}
        mutators={{ ...arrayMutators }}
      >
        {({ handleSubmit }) => (
          <form className="settings-form bg-white" onSubmit={handleSubmit}>
            <h2 className="mt-1 mb-4 text-center user-name">{getFullName(props.account)}</h2>
            {historyState?.accountFieldsUnFilled && <Alert color="danger">{translate('settings.messages.requiredFieldsUnFilled')}</Alert>}
            <Field
              name="firstName"
              className="form-control disabled-input"
              placeholder={translate('settings.form.firstname.placeholder')}
              label={`${translate('settings.form.firstname')}*`}
              component={Input}
            />
            <Field
              name="lastName"
              className="form-control disabled-input"
              placeholder={translate('settings.form.lastname.placeholder')}
              label={`${translate('settings.form.lastname')}*`}
              component={Input}
            />
            <Field
              name="email"
              className="disabled-input"
              placeholder={translate('global.form.email.placeholder')}
              label={`${translate('global.form.email.label')}*`}
              type="email"
              component={Input}
            />

            <h3 className="my-4">{translate('global.form.role.selectYourLocale')}</h3>
            <CountryChoiceField setSelectedCountry={setSelectedCountry} />

            {renderRoleChoice()}

            <h3 className="my-4">{translate('global.form.personRoles')}</h3>
            <PersonRoleFieldArray selectedCountry={selectedCountry} setCurrentAccount={setCurrentAccount} />

            {props.hasRole && (
              <>
                <h3 className="my-4">{translate('settings.connectAccounts')}</h3>
                <Row className="mx-1">
                  <a
                    href={'/oauth2/authorization/google?id=' + props.account?.id}
                    className={`btn btn-lg d-flex justify-content-center align-items-center flex-column btn-oauth mr-4
                    ${googleConnected && 'disabled'}`}
                  >
                    <div>
                      <img src="content/images/google.png" alt={'Google'} />
                      {translate('login.google')}
                    </div>
                    <div>
                      <small>
                        <b>{googleConnected && translate('settings.connected')}</b>
                      </small>
                    </div>
                  </a>
                  <a
                    href={'oauth2/authorization/azure?id=' + props.account?.id}
                    className={`btn btn-lg d-flex justify-content-center align-items-center flex-column btn-oauth mr-4
                    ${azureConnected && 'disabled'}`}
                  >
                    <div>
                      <img src="content/images/microsoft.png" alt={'Microsoft'} />
                      {translate('login.microsoft')}
                    </div>
                    <div>
                      <small>
                        <b>{azureConnected && translate('settings.connected')}</b>
                      </small>
                    </div>
                  </a>
                  <a
                    href={'/oauth2/authorization/ekool?id=' + props.account?.id}
                    className={`py-2 btn btn-lg d-flex justify-content-center align-items-center flex-column btn-oauth
                    ${ekoolConnected && 'disabled'}`}
                  >
                    <div className={ekoolConnected ? 'h-50' : 'h-100'}>
                      <img className={'h-100'} src="content/images/ekool.png" alt={'Ekool'} />
                    </div>
                    {ekoolConnected && (
                      <div className={'h-50'}>
                        <small>
                          <b>{translate('settings.connected')}</b>
                        </small>
                      </div>
                    )}
                  </a>
                </Row>
              </>
            )}
            {!props.hasRole && (
              <Field name="termsAgreed" type="checkbox">
                {({ input, meta }) => {
                  return (
                    <div className={'form-group mt-4'}>
                      <input id="termsAgreed" name={input.name} type="checkbox" checked={input.checked} onChange={input.onChange} />
                      <label htmlFor="termsAgreed" className="terms ml-1 mb-0">
                        {translate('userManagement.agreeToTermsAndConditions')}
                      </label>
                      {meta?.touched && meta?.error && (
                        <FormFeedback className={'d-block'}>{translate('entity.validation.required')}</FormFeedback>
                      )}
                    </div>
                  );
                }}
              </Field>
            )}
            <Button color="primary" type="submit" className="mt-4 float-right submit" size="lg">
              {translate('settings.form.button')}
            </Button>
          </form>
        )}
      </Form>
    </div>
  );
};

const mapStateToProps = ({ locale, authentication }: IRootState) => ({
  account: authentication.account,
  hasRole: hasAnyAuthority(authentication.account.authorities, [
    AUTHORITIES.TEACHER,
    AUTHORITIES.ADMIN,
    AUTHORITIES.STUDENT,
    AUTHORITIES.PARENT,
  ]),
  locale: locale.currentLocale,
});

const mapDispatchToProps = { getSession, saveAccountSettings, reset };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage);
