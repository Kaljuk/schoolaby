import { AUTHORITIES } from 'app/config/constants';

const authorityOptions = [
  {
    role: AUTHORITIES.TEACHER,
    translationKey: 'global.teacher',
  },
  {
    role: AUTHORITIES.STUDENT,
    translationKey: 'global.student',
  },
];

export default authorityOptions;
