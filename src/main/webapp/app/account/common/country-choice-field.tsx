import React, { FC } from 'react';
import { countries } from 'app/config/translation';
import HorizontalRadio from 'app/shared/layout/horizontal-radio-button/horizontal-radio-button';
import { Field, useForm } from 'react-final-form';
import { FormFeedback, Label } from 'reactstrap';

interface CountryChoiceFieldProps {
  setSelectedCountry: (country: string) => void;
}
const CountryChoiceField: FC<CountryChoiceFieldProps> = ({ setSelectedCountry }) => {
  const { submitFailed, errors } = useForm().getState();

  return (
    <HorizontalRadio className={`form-control border-0 p-0 h-auto ${submitFailed && errors?.country ? 'is-invalid text-danger' : ''}`}>
      {[...countries.keys()].map(key => (
        <Field key={key} type={'radio'} value={key} name={'country'}>
          {({ input }) => {
            return (
              <div className={'form-group'}>
                <input
                  id={`country-${key}`}
                  name={input.name}
                  type="radio"
                  value={key}
                  checked={input.checked}
                  onChange={event => {
                    input.onChange(event);
                    setSelectedCountry(key);
                  }}
                />
                <Label for={`country-${key}`}>{countries.get(key).nativeName}</Label>
              </div>
            );
          }}
        </Field>
      ))}
      {submitFailed && errors?.country && <FormFeedback className="d-block">{errors.country}</FormFeedback>}
    </HorizontalRadio>
  );
};

export default CountryChoiceField;
