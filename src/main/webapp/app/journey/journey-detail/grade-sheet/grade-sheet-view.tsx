import './grade-sheet.scss';
import React from 'react';
import MyJourneysBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/my-journeys-breadcrumb-item';
import JourneyTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/journey-title-breadcrumb-item';
import Heading, { ITabParams } from 'app/shared/layout/heading/heading';
import { getHeadingIcon } from 'app/shared/util/journey-utils';
import { useGetJourney } from 'app/shared/services/journey-api';
import { RouteComponentProps } from 'react-router-dom';
import { JOURNEY } from 'app/shared/util/entity-utils';
import GradesBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/grades-breadcrumb-item';
import GradeSheet from 'app/journey/journey-detail/grade-sheet/grade-sheet';

const GradeSheetView = ({ match }: RouteComponentProps<{ id: string }>) => {
  const { data: journey } = useGetJourney(match.params.id);

  const tabParams: ITabParams = {
    entityType: JOURNEY,
    tabEntityId: Number(match.params.id),
  };

  return (
    <>
      <MyJourneysBreadcrumbItem />
      <JourneyTitleBreadcrumbItem journeyId={journey?.id} journeyTitle={journey?.title} />
      <GradesBreadcrumbItem journeyId={journey?.id} />
      <Heading journey={journey} tabParams={tabParams} icon={getHeadingIcon(journey)} title={journey?.title} />
      <GradeSheet journey={journey} />
    </>
  );
};

export default React.memo(GradeSheetView);
