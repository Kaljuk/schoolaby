import React, { SyntheticEvent, useContext, useState } from 'react';
import { Dropdown, DropdownToggle, UncontrolledTooltip } from 'reactstrap';
import { TextFormat, translate } from 'react-jhipster';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-old';
import Icon from 'app/shared/icons';
import {
  getAssignmentCheckmarkColor,
  getClassNameByAssignmentState,
  ICON_GREY,
  INFO,
  SECONDARY,
  YELLOW,
} from 'app/shared/util/color-utils';
import MenuDropdown from 'app/shared/layout/menu-dropdown/menu-dropdown';
import LinesEllipsis from 'react-lines-ellipsis';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import { ASSIGNMENT } from 'app/shared/util/entity-utils';
import { APP_LOCAL_DATE_TIME_FORMAT } from 'app/config/constants';
import './assignment-container.scss';
import { getFullName } from 'app/shared/util/string-utils';
import { useDeadlineTargetState } from 'app/shared/contexts/entity-detail-context';
import clone from 'lodash/clone';
import { Link } from 'react-router-dom';

export const AssignmentContainer = () => {
  const { assignment, isAllowedToModify, journey, previewEnabled, toggleAssignmentView } = useContext(AssignmentContext);
  const checkmarkColor = getAssignmentCheckmarkColor(assignment);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const currentState = previewEnabled ? EntityState.NOT_STARTED : assignment.state;
  const { setDeadlineTarget } = useDeadlineTargetState();

  const toggleDropDown = (event: SyntheticEvent) => {
    event.preventDefault();
    setDropdownOpen(prevState => !prevState);
  };

  const getStatusIcon = () => {
    switch (assignment?.state) {
      case EntityState.IN_PROGRESS:
      case EntityState.COMPLETED:
        return 'checkmark';
      case EntityState.REJECTED:
      case EntityState.FAILED:
        return 'cross';
      case EntityState.NOT_STARTED:
      case EntityState.OVERDUE:
      case EntityState.URGENT:
      default:
        return '';
    }
  };

  const TrophyIcon = () => {
    if (!(isAllowedToModify || previewEnabled || !assignment?.requiresSubmission) && assignment?.highestGrade) {
      return <Icon name={'trophy'} width={'18px'} height={'16px'} fill={YELLOW} stroke={YELLOW} />;
    }
    return null;
  };

  const SubmissionsCountIcon = () => {
    if (isAllowedToModify && !assignment.students?.length) {
      return (
        <>
          <UncontrolledTooltip
            placement={'bottom'}
            target={`submissionCountContainer-${assignment.id}`}
          >{`${assignment.submissionsCount} / ${journey.studentCount}`}</UncontrolledTooltip>
          <span data-testid="custom-element" id={`submissionCountContainer-${assignment.id}`}>
            <Icon className={'mr-1'} name={'people'} width={'18px'} height={'16px'} fill={SECONDARY} stroke={SECONDARY} />
            {assignment.submissionsCount}
          </span>
        </>
      );
    }
    return null;
  };

  const AssignmentStudentsIcon = () => {
    if (isAllowedToModify && assignment.students?.length > 0) {
      return (
        <>
          <UncontrolledTooltip
            innerClassName={'px-3 pt-2'}
            placement={'bottom'}
            target={`assignmentStudentsIconContainer-${assignment.id}`}
          >
            {assignment.students.map(student => {
              return (
                <div key={student.id} className={'mb-1'}>
                  {getFullName(student)}
                </div>
              );
            })}
          </UncontrolledTooltip>
          <span id={`assignmentStudentsIconContainer-${assignment.id}`} data-testid={'personalized-icon'}>
            <Icon className={'mr-1'} name={'user'} width={'18px'} height={'16px'} fill={'none'} stroke={INFO} />
          </span>
        </>
      );
    }
    return null;
  };

  const ContainerWrapper = ({ children }) => {
    const className = `assignment d-flex flex-column ${getClassNameByAssignmentState(currentState, 'bg', 'light')}`;
    return previewEnabled ? (
      <div className={className} onClick={() => toggleAssignmentView(assignment?.id)}>
        {children}
      </div>
    ) : (
      <Link to={`/assignment/${assignment.id}?journeyId=${journey.id}`}>
        <div className={className}>{children}</div>
      </Link>
    );
  };

  return (
    <ContainerWrapper>
      <div className="assignment-header d-flex">
        <LinesEllipsis text={assignment.title} maxLine={3} className={`assignment-title`} />
        {isAllowedToModify && (
          <Dropdown isOpen={dropdownOpen} toggle={toggleDropDown} aria-label={translate('global.heading.dropdown')}>
            <DropdownToggle>
              <Icon name={'dropdown'} width={'16px'} height={'16px'} stroke={ICON_GREY} fill={ICON_GREY} />
            </DropdownToggle>
            <MenuDropdown entityId={assignment.id} entityType={ASSIGNMENT} journeyId={journey?.id} entityTitle={assignment?.title} />
          </Dropdown>
        )}
      </div>
      <div className={'assignment-footer'}>
        {!previewEnabled && (
          <>
            {getStatusIcon() && (
              <Icon name={getStatusIcon()} width={'18px'} height={'14px'} fill={checkmarkColor} stroke={checkmarkColor} />
            )}
            <TrophyIcon />
            <AssignmentStudentsIcon />
            <SubmissionsCountIcon />
          </>
        )}
        <span className={'d-inline-block text-nowrap float-right'}>
          <span className={'mr-2 text-secondary'}>
            {currentState === EntityState.COMPLETED
              ? translate('schoolabyApp.assignment.completed')
              : translate('schoolabyApp.assignment.deadline')}
          </span>
          <span className={`mr-1 ${getClassNameByAssignmentState(currentState, 'text')}`}>
            <TextFormat type="date" value={assignment.deadline} format={APP_LOCAL_DATE_TIME_FORMAT} />
            <span
              onClick={e => {
                if (isAllowedToModify) {
                  e.preventDefault();
                  setDeadlineTarget(clone(assignment));
                }
              }}
            >
              <Icon name={'calendarNew'} width={'20'} height={'21'} className={'calendar-icon mb-1 ml-1'} />
            </span>
          </span>
          {!assignment.flexibleDeadline && <Icon name={'exclamationCircleFilled'} className={'mb-1 exclamation-circle'} />}
        </span>
      </div>
    </ContainerWrapper>
  );
};
