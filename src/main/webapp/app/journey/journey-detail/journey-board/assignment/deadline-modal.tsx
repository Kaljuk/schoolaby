import React, { FC, useEffect, useState } from 'react';
import { Button, Col, ModalBody, ModalFooter, ModalHeader, Row } from 'reactstrap';
import { translate } from 'react-jhipster';
import './assignment-container.scss';
import DatePicker from 'react-datepicker';
import { Modal } from 'reactstrap/lib';
import CloseButton from 'app/shared/layout/close-button/close-button';
import { DateTime } from 'luxon';
import { useDeadlineTargetState, useNewDeadlineState } from 'app/shared/contexts/entity-detail-context';

export const DeadlineModal: FC = () => {
  const { setDeadlineTarget, deadlineTarget } = useDeadlineTargetState();
  const { setNewDeadline } = useNewDeadlineState();
  const [currentDeadline, setCurrentDeadline] = useState(null);

  useEffect(() => {
    if (deadlineTarget?.deadline) {
      // @ts-ignore
      if (deadlineTarget?.deadline instanceof Date) {
        setCurrentDeadline(deadlineTarget?.deadline);
      } else {
        setCurrentDeadline(DateTime.fromISO(deadlineTarget?.deadline).setZone('local', { keepLocalTime: false }).toJSDate());
      }
    }
  }, [deadlineTarget?.deadline]);

  const closeModal = () => setDeadlineTarget(null);

  return (
    <Modal className={'deadline-modal'} centered isOpen={!!deadlineTarget}>
      <ModalHeader className={'border-0 d-inline'}>
        <Row noGutters>
          <Col xs={9}>{translate('schoolabyApp.assignment.deadlineModal.choose')}</Col>
          <Col xs={3} className={'d-flex float-right'}>
            <CloseButton onClick={closeModal} className={'close-modal-button'} />
          </Col>
        </Row>
      </ModalHeader>
      <ModalBody>
        <DatePicker
          selected={currentDeadline}
          onChange={date => setCurrentDeadline(DateTime.fromJSDate(date).setZone('local', { keepLocalTime: true }).toJSDate())}
          open
          customInput={<input type={'hidden'} />}
          showTimeSelect
        />
      </ModalBody>
      <ModalFooter className={'border-0'}>
        <Row className={'w-100 justify-content-end'}>
          <Col md={6}>
            <Button className="text-uppercase w-100" color="white" onClick={closeModal}>
              {translate('entity.action.cancel')}
            </Button>
          </Col>
          <Col md={6}>
            <Button
              type="button"
              onClick={() => {
                setNewDeadline(currentDeadline);
              }}
              className="text-uppercase w-100"
              color="primary"
            >
              {translate('entity.action.save')}
            </Button>
          </Col>
        </Row>
      </ModalFooter>
    </Modal>
  );
};
