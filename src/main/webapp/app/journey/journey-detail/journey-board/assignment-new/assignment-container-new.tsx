import React, { SyntheticEvent, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { Dropdown, DropdownToggle, UncontrolledTooltip } from 'reactstrap';
import { TextFormat, translate } from 'react-jhipster';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import Icon from 'app/shared/icons';
import { SBY_ICON_BEIGE } from 'app/shared/util/color-utils';
import { MenuDropdown } from 'app/shared/layout/menu-dropdown/menu-dropdown';
import LinesEllipsis from 'react-lines-ellipsis';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import { ASSIGNMENT } from 'app/shared/util/entity-utils';
import { APP_LOCAL_DATE_TIME_FORMAT } from 'app/config/constants';
import { capitalize, getFullName } from 'app/shared/util/string-utils';
import clone from 'lodash/clone';

import './assignment-container-new.scss';
import { useDeadlineTargetState } from 'app/shared/contexts/entity-detail-context';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import { useDisplayEntityUpdateState, useEntityToUpdateState } from 'app/shared/contexts/entity-update-context';

const AssignmentContainerNew = () => {
  const { assignment, isAllowedToModify, journey } = useContext(AssignmentContext);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const { setEntity } = useEntityToUpdateState();
  const { setDeadlineTarget } = useDeadlineTargetState();
  const { openAssignmentUpdate } = useDisplayEntityUpdateState();

  const toggleDropDown = (event: SyntheticEvent) => {
    event.preventDefault();
    setDropdownOpen(prevState => !prevState);
  };

  const submittedCount = () =>
    assignment.state === EntityState.OVERDUE || assignment.state === EntityState.URGENT ? (
      <strong>{assignment.submissionsCount}</strong>
    ) : (
      assignment.submissionsCount
    );

  const getSubmissionsCount = () => {
    if (isAllowedToModify) {
      return (
        <div className={'submissions'} data-testid={'submissions-count'}>
          {submittedCount()}
          {`/${assignment.students?.length || assignment.groups?.length || journey.studentCount}`}
        </div>
      );
    }
  };

  const getAssignmentStudentsIcon = () => {
    if (assignment.groups?.length) {
      return <Icon name={'group'} className={'pupil-icon'} width={'13'} height={'12'} viewBox={'0 0 13 12'} />;
    } else if (assignment.students?.length) {
      return <Icon name={'coloredUser'} width={'9'} height={'9'} fill={SBY_ICON_BEIGE} className={'pupil-icon'} />;
    }
  };

  const getAssignmentStudentsIconWithTooltip = () => {
    if (isAllowedToModify) {
      return (
        <>
          <UncontrolledTooltip
            innerClassName={'px-3 pt-2'}
            placement={'bottom'}
            target={`assignmentStudentsIconContainer-${assignment.id}`}
          >
            {assignment.students?.map(student => {
              return (
                <div key={student.id} className={'mb-1'}>
                  {getFullName(student)}
                </div>
              );
            }) ||
              assignment.groups?.map(group => {
                return (
                  <div key={group.id} className={'mb-1'}>
                    {capitalize(group.name)}
                  </div>
                );
              })}
          </UncontrolledTooltip>
          <div
            id={`assignmentStudentsIconContainer-${assignment.id}`}
            className={'d-flex align-items-center h-100'}
            data-testid={'personalized-icon'}
          >
            {getAssignmentStudentsIcon()}
          </div>
        </>
      );
    }
  };

  const deadlineText =
    assignment.state === EntityState.COMPLETED ? translate('schoolabyApp.assignment.done') : translate('schoolabyApp.assignment.deadline');

  return assignment?.id && journey?.id ? (
    <Link to={`/assignment/${assignment.id}?journeyId=${journey.id}`}>
      <div className={`assignment-new d-flex flex-column ${assignment?.state?.toLowerCase() || ''}`}>
        <div className="assignment-header d-flex justify-content-between">
          <LinesEllipsis text={assignment.title} maxLine={3} className={`assignment-title`} />
          {isAllowedToModify && (
            <Dropdown isOpen={dropdownOpen} toggle={toggleDropDown} aria-label={translate('global.heading.dropdown')}>
              <DropdownToggle tag={'div'} className={'p-0'}>
                <CustomButton
                  title={translate('entity.action.edit')}
                  buttonType={'secondary'}
                  outline
                  size={'sm'}
                  sharper
                  onClick={e => e.preventDefault()}
                />
              </DropdownToggle>
              <MenuDropdown
                entityId={assignment.id}
                entityType={ASSIGNMENT}
                journeyId={journey.id}
                isAllowedToModify={isAllowedToModify}
                onEditClicked={() => {
                  setEntity(null);
                  openAssignmentUpdate(assignment.id);
                }}
                entityTitle={assignment.title}
              />
            </Dropdown>
          )}
        </div>
        <div className={'assignment-footer d-flex justify-content-between align-items-center'}>
          <div className={'d-flex'}>
            <span
              onClick={e => {
                if (isAllowedToModify) {
                  e.preventDefault();
                  setDeadlineTarget(clone(assignment));
                }
              }}
            >
              <Icon name={'stopwatch'} className={'stopwatch'} />
            </span>
            <div className={`ml-2 d-flex align-items-center deadline ${assignment.state === EntityState.OVERDUE ? 'overdue' : ''}`}>
              {deadlineText}
              <strong className={'ml-1'}>
                <TextFormat type="date" value={assignment.deadline} format={APP_LOCAL_DATE_TIME_FORMAT} />
              </strong>
            </div>
          </div>
          <div className={'d-flex align-items-center'}>
            {getSubmissionsCount()}
            {getAssignmentStudentsIconWithTooltip()}
          </div>
        </div>
      </div>
    </Link>
  ) : (
    <></>
  );
};

export default AssignmentContainerNew;
