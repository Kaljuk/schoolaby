import React, { FC, useContext, useEffect, useState } from 'react';
import { AssignmentContext, JourneyDetailContext, MilestoneContext } from 'app/journey/journey-detail/journey-detail-new';
import { MilestoneContainerNew } from 'app/journey/journey-detail/journey-board/milestone/milestone-container-new';
import { translate } from 'react-jhipster';
import { Col, Row } from 'reactstrap';
import { IAssignment } from 'app/shared/model/assignment.model';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import AssignmentContainerNew from 'app/journey/journey-detail/journey-board/assignment-new/assignment-container-new';
import { Draggable, Droppable } from 'react-beautiful-dnd';
import { sortBy } from 'lodash';
import ReactDOM from 'react-dom';

import './milestone-with-assignments-new.scss';
import { useDisplayEntityUpdateState, useEntityUpdateIdState } from 'app/shared/contexts/entity-update-context';

interface MilestoneWithAssignmentsProps {
  milestoneIndex: number;
}

const MilestoneWithAssignmentsNew: FC<MilestoneWithAssignmentsProps> = ({ milestoneIndex }) => {
  const { milestone } = useContext(MilestoneContext);
  const { isAllowedToModify } = useContext(JourneyDetailContext);
  const { openAssignmentUpdate } = useDisplayEntityUpdateState();
  const { clearAssignmentUpdateId, setMilestoneUpdateId } = useEntityUpdateIdState();

  const [assignments, setAssignments] = useState<IAssignment[]>([]);

  const handleAddTaskClicked = () => {
    clearAssignmentUpdateId();
    setMilestoneUpdateId(milestone.id);
    openAssignmentUpdate();
  };

  const AddTaskButton = () => (
    <div className={'d-flex align-items-center justify-content-center add-task cursor-pointer'} onClick={handleAddTaskClicked}>
      <Row>
        <Col data-testid={'add-task'} className={'d-flex justify-content-center align-items-center'} xs={12}>
          + {translate('entity.add.task')}
        </Col>
      </Row>
    </div>
  );

  useEffect(() => {
    const sortedAssignments = sortBy(milestone.assignments, assignment => new Date(assignment.deadline));
    setAssignments(sortedAssignments);
  }, [milestone]);

  return (
    <span className={'milestone-with-assignments-new content-wrapper'}>
      <MilestoneContainerNew />
      <Droppable droppableId={milestone.id?.toString()}>
        {provided => (
          <ul ref={provided.innerRef} {...provided.droppableProps} key={milestoneIndex} className={`assignments-list p-0`}>
            {assignments && <AssignmentsList assignments={Object.values(assignments)} />}
            {provided.placeholder}
            {isAllowedToModify && <AddTaskButton />}
          </ul>
        )}
      </Droppable>
    </span>
  );
};

const AssignmentListItem = ({ provided, index, className = '' }) => (
  <li
    key={index}
    ref={provided.innerRef}
    {...provided.draggableProps}
    {...provided.dragHandleProps}
    className={`assignment-list-item ${className}`}
  >
    <AssignmentContainerNew />
  </li>
);

const AssignmentsList = ({ assignments }: { assignments: IAssignment[] }) => {
  const { isAllowedToModify, journey } = useContext(MilestoneContext);

  return (
    <>
      {assignments.length > 0 &&
        assignments.map((assignment, j) => (
          <Draggable
            key={assignment.id?.toString()}
            draggableId={assignment.id?.toString()}
            index={j}
            isDragDisabled={!isAllowedToModify || assignment.state === EntityState.COMPLETED}
          >
            {(provided, snapshot) => (
              <AssignmentContext.Provider value={{ assignment, isAllowedToModify, journey }}>
                {snapshot.isDragging ? (
                  ReactDOM.createPortal(<AssignmentListItem index={j} provided={provided} className={'dragging'} />, document.body)
                ) : (
                  <AssignmentListItem index={j} provided={provided} />
                )}
              </AssignmentContext.Provider>
            )}
          </Draggable>
        ))}
    </>
  );
};

export default MilestoneWithAssignmentsNew;
