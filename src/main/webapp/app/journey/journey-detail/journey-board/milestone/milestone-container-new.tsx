import React, { FC, useContext, useEffect, useRef, useState } from 'react';
import { MilestoneContext } from 'app/journey/journey-detail/journey-detail-new';
import './milestone-container-new.scss';
import { Dropdown, DropdownToggle, Row } from 'reactstrap';
import { DateTime } from 'luxon';
import { translate } from 'react-jhipster';
import Icon from 'app/shared/icons';
import { getClassNameByMilestoneState } from 'app/shared/util/color-utils';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import { MenuDropdown } from 'app/shared/layout/menu-dropdown/menu-dropdown';
import { MILESTONE } from 'app/shared/util/entity-utils';
import { useDisplayEntityUpdateState, useEntityToUpdateState } from 'app/shared/contexts/entity-update-context';
import { Link } from 'react-router-dom';

export const MilestoneContainerNew: FC = () => {
  const { milestone, journey, isAllowedToModify, scrollToMilestoneId } = useContext(MilestoneContext);
  const { setEntity } = useEntityToUpdateState();
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const { openMilestoneUpdate } = useDisplayEntityUpdateState();

  const toggleDropdown = () => setDropdownOpen(prevState => !prevState);

  const scrollRef = useRef(null);

  useEffect(() => {
    milestone.id === scrollToMilestoneId && scrollRef?.current?.scrollIntoView({ behavior: 'smooth', inline: 'center' });
  }, [scrollToMilestoneId, scrollRef?.current]);

  return (
    <Link to={`/milestone/${milestone?.id}?journeyId=${journey?.id}`} className={`milestone-new`} ref={scrollRef}>
      <Row noGutters className={`milestone-title text-left justify-content-between align-items-center`}>
        <div className={'ellipsis'}>{milestone.title}</div>
        {isAllowedToModify && (
          <Dropdown isOpen={dropdownOpen} toggle={toggleDropdown}>
            <DropdownToggle tag={'div'} className={'h-100'}>
              <CustomButton
                title={translate('entity.action.edit')}
                buttonType={'secondary'}
                outline
                size={'sm'}
                sharper
                onClick={e => e.preventDefault()}
              />
            </DropdownToggle>
            <MenuDropdown
              entityId={milestone.id}
              journeyId={journey.id}
              entityType={MILESTONE}
              entityTitle={milestone?.title}
              isAllowedToModify={isAllowedToModify}
              onEditClicked={() => {
                setEntity(null);
                openMilestoneUpdate(milestone.id);
              }}
            />
          </Dropdown>
        )}
      </Row>
      <Row noGutters>
        <span className={'d-flex milestone-end-date'}>
          <span className={`rounded-circle deadline-icon ${getClassNameByMilestoneState(milestone.state, 'bg')} new`}>
            <Icon name={'stopwatch'} />
          </span>
          <span className={'ml-2 mr-1'}>{translate('global.deadline')}</span>
          <span className={`font-weight-bold ${getClassNameByMilestoneState(milestone.state, 'text', null, true)} new`}>
            {DateTime.fromISO(milestone.endDate).toFormat('dd.MM.yyyy')}
          </span>
        </span>
      </Row>
    </Link>
  );
};
