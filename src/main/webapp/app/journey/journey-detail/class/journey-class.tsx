import React from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import Heading from 'app/shared/layout/heading/heading';
import { translate } from 'react-jhipster';
import { Col, Row } from 'reactstrap';
import { IClass } from 'app/shared/model/class.model';
import { classes } from './journey-class-mock-data';
import { ClassCard } from './class-card';

export type MockClassState = {
  class: IClass;
};

type MockClassLocationState = {
  from: {
    pathname: string;
  };
};

const JourneyClass = () => {
  const history = useHistory<MockClassState>();
  const location = useLocation<MockClassLocationState>();

  const onCardClick = (clazz: IClass) => {
    history.push({
      pathname: location.state.from.pathname,
      state: { class: clazz },
    });
  };

  return (
    <div>
      <Heading dropdown={null} title={translate('schoolabyApp.journey.students.add.classes')} />
      <Row className={'justify-content-center'}>
        <Col xs={'10'}>
          <Row className={'d-flex'}>
            {classes.map(clazz => (
              <ClassCard
                key={clazz.name}
                name={clazz.name}
                studentCount={clazz.students.length}
                timeRange={clazz.timeRange}
                onClick={() => onCardClick(clazz)}
              />
            ))}
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default JourneyClass;
