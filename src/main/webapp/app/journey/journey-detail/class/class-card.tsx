import React from 'react';
import { Row, Col } from 'reactstrap';
import Icon from 'app/shared/icons';
import { SECONDARY } from 'app/shared/util/color-utils';

type IClassCard = {
  name: string;
  studentCount: number;
  timeRange: string;
  onClick: () => void;
};

export const ClassCard = ({ name, studentCount, timeRange, onClick }: IClassCard) => {
  return (
    <Col key={name} xs={'12'} md={'4'} lg={'2'} className={'mt-3'} onClick={onClick}>
      <div className={'h-100 base-card class-card rounded border'}>
        <Row>
          <Col className={'d-flex justify-content-center align-items-center'} style={{ minHeight: '160px' }}>
            <h1>{name}</h1>
          </Col>
        </Row>
        <hr className={'my-0'} />
        <Row className={'p-3'} noGutters>
          <Col xs={'4'} className={'d-flex justify-content-start'}>
            <div className={'d-flex align-items-center'}>
              <Icon name={'graduate'} fill={SECONDARY} stroke={'transparent'} />
              <span className={'text-muted ml-1'}>{studentCount}</span>
            </div>
          </Col>
          <Col xs={'8'} className={'d-flex justify-content-end'}>
            {timeRange}
          </Col>
        </Row>
      </div>
    </Col>
  );
};
