import './journey-overview.scss';
import React, { FC } from 'react';
import { Button, Modal, ModalBody } from 'reactstrap';
import Icon, { getSubjectIcon } from 'app/shared/icons';
import { IJourney } from 'app/shared/model/journey.model';
import CloseButton from 'app/shared/layout/close-button/close-button';
import Avatar from 'app/shared/layout/header/avatar';
import { RED } from 'app/shared/util/color-utils';
import { getFormattedDate } from 'app/shared/util/date-utils';
import { DateTime } from 'luxon';
import { translate } from 'react-jhipster';
import ReactMarkdown from 'react-markdown';

interface IProps {
  modal: boolean;
  toggle: () => void;
  journey: IJourney;
}

const JourneyOverview: FC<IProps> = ({ toggle, modal, journey }) => {
  const getHeadingIcon = () =>
    journey.subject ? getSubjectIcon(journey.subject.label, '10rem', '10rem', RED) : getSubjectIcon('default', '10rem', '10rem', RED);

  return (
    <Modal isOpen={modal} toggle={toggle} className={'modal-lg welcome-modal'} centered contentClassName={'p-3 border-0 shadow'}>
      <CloseButton onClick={toggle} className={'close-modal-button'} />
      <ModalBody className={'pb-4'}>
        <div className="d-flex justify-content-center mt-4 journey-image">
          {journey?.imageUrl ? <img src={journey?.imageUrl} alt={'journey image'} /> : getHeadingIcon()}
        </div>
        <h1 className="d-flex justify-content-center mt-4">{journey.title}</h1>
        <div className={`d-flex justify-content-center avatar-container `}>
          <Avatar fullName={journey.teacherName} />
          <h3 className={'ml-2 d-flex text-secondary'}>{journey.teacherName}</h3>
        </div>
        <div className="d-flex mt-3 justify-content-between">
          <div className="d-flex flex-column align-items-center ml-5 pl-5">
            <Icon name={'wallClock'} fill={RED} stroke={RED} width="2rem" height="2rem" />
            <b className="mt-2">
              {getFormattedDate(journey.startDate, DateTime.DATE_SHORT)} - {getFormattedDate(journey.endDate, DateTime.DATE_SHORT)}
            </b>
          </div>
          <div className="d-flex flex-column align-items-center educational-level">
            <Icon name={'hat'} fill={RED} stroke={RED} width="2rem" height="2rem" />
            <b className="mt-2">{journey?.educationalLevel?.name}</b>
          </div>
          <div className="d-flex flex-column align-items-center mr-5 pr-5 alignment">
            <Icon name={'lightbulb'} fill={RED} stroke={RED} width="2rem" height="2rem" />
            <b className="mt-2">{journey?.subject ? translate(journey.subject.label) : ''}</b>
          </div>
        </div>
        <div className="my-5 mx-5 preserve-space text-break description">
          <ReactMarkdown source={journey.description} />
        </div>
        <div className="d-flex justify-content-center">
          <Button size="lg" outline color={'secondary'} onClick={toggle}>
            {translate('global.close')}
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};

export default JourneyOverview;
