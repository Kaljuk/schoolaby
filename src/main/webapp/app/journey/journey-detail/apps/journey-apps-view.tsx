import './journey-apps.scss';
import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import Heading, { ITabParams, PreviewTabType } from 'app/shared/layout/heading/heading';
import { getHeadingIcon } from 'app/shared/util/journey-utils';
import MyJourneysBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/my-journeys-breadcrumb-item';
import JourneyTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/journey-title-breadcrumb-item';
import AppsBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/apps-breadcrumb-item';
import { useGetJourney } from 'app/shared/services/journey-api';
import { JOURNEY } from 'app/shared/util/entity-utils';
import { IJourney } from 'app/shared/model/journey.model';
import JourneyApps from 'app/journey/journey-detail/apps/journey-apps';

export interface IJourneyAppsProps extends RouteComponentProps<{ id: string }> {
  previewEnabled?: boolean;
  templateJourney?: IJourney;
  openedPreviewTab?: PreviewTabType;
  setOpenedPreviewTab?: (tab: PreviewTabType) => void;
}

export const JourneyAppsView = ({ match, previewEnabled, templateJourney, openedPreviewTab, setOpenedPreviewTab }: IJourneyAppsProps) => {
  const { data: receivedJourney } = useGetJourney(match?.params?.id, !previewEnabled);
  const journey = previewEnabled ? templateJourney : receivedJourney;

  const tabParams: ITabParams = {
    entityType: JOURNEY,
    tabEntityId: journey?.id,
  };

  return journey ? (
    <>
      <Heading
        previewEnabled={previewEnabled}
        journey={journey}
        openedPreviewTab={openedPreviewTab}
        setOpenedPreviewTab={setOpenedPreviewTab}
        tabParams={tabParams}
        icon={getHeadingIcon(journey)}
        title={journey?.title}
      />
      {!previewEnabled && (
        <>
          <MyJourneysBreadcrumbItem />
          <JourneyTitleBreadcrumbItem journeyId={journey?.id} journeyTitle={journey?.title} />
          <AppsBreadcrumbItem journeyId={journey?.id} />
        </>
      )}
      <JourneyApps previewEnabled={previewEnabled} journey={journey} />
    </>
  ) : null;
};

export default JourneyAppsView;
