import './app-config-modal.scss';
import React from 'react';
import { Button, Col, ModalBody, ModalFooter, Row } from 'reactstrap';
import { translate } from 'react-jhipster';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import { ILtiConfig } from 'app/shared/model/lti-config.model';
import { ILtiApp } from 'app/shared/model/lti-app.model';
import { v4 as uuidv4 } from 'uuid';
import { isLtiAdvantage } from 'app/shared/util/lti-utils';
import CustomModal from 'app/shared/layout/custom-modal/custom-modal';
import CloseButton from 'app/shared/layout/close-button/close-button';

export type IAppConfigModal = {
  ltiConfig?: ILtiConfig;
  ltiApp?: ILtiApp;
  toggle: () => void;
  showModal: boolean;
  onSubmit: (event: any, values: any) => void;
};

export const AppConfigModal = ({ ltiApp, ltiConfig, showModal, toggle, onSubmit }: IAppConfigModal) => {
  let app = ltiApp;
  let isUpdate;
  if (ltiConfig) {
    isUpdate = true;
    app = ltiConfig.ltiApp;
  }

  const LtiAdvantageParams = () => (
    <>
      <AvField value={app.clientId} label={'Client ID'} name={'clientId'} disabled={true} className="disabled-input" />
      <AvField
        value={ltiConfig ? ltiConfig.deploymentId : uuidv4()}
        label={'Deployment ID'}
        name={'deploymentId'}
        disabled={true}
        className="disabled-input"
      />
      <AvField value={window.location.origin} label={'Platform ID'} name={'platformId'} disabled={true} className="disabled-input" />
      <AvField
        value={`${window.location.origin}/api/lti-advantage/authorize`}
        label={'Platform OIDC Auth URL'}
        name={'platformId'}
        disabled={true}
        className="disabled-input"
      />
      <AvField
        value={`${window.location.origin}/api/lti-advantage/token`}
        label={'Platform OAuth Access Token URL'}
        name={'platformId'}
        disabled={true}
        className="disabled-input"
      />
      <AvField
        value={`${window.location.origin}/api/lti-advantage/keys`}
        label={'Platform JSON Web Key Set URL'}
        name={'platformId'}
        disabled={true}
        className="disabled-input"
      />
      <AvField
        value={app.deepLinkingUrl || ltiConfig?.deepLinkingUrl}
        disabled={!!app?.deepLinkingUrl}
        placeholder={'Deep linking url'}
        label={'Deep linking url'}
        name={'deepLinkingUrl'}
        className={!!app?.deepLinkingUrl && 'disabled-input'}
        validate={{
          required: {
            value: true,
            errorMessage: translate('global.messages.validate.deepLinkingUrl.required'),
          },
        }}
      />
      <AvField
        value={app.loginInitiationUrl || ltiConfig?.loginInitiationUrl}
        disabled={!!app?.loginInitiationUrl}
        placeholder={'Login initiation url'}
        label={'Login initiation url'}
        name={'loginInitiationUrl'}
        className={!!app?.loginInitiationUrl && 'disabled-input'}
        validate={{
          required: {
            value: true,
            errorMessage: translate('global.messages.validate.loginInitiationUrl.required'),
          },
        }}
      />
      <AvField
        value={app.redirectHost || ltiConfig?.redirectHost}
        disabled={!!app?.redirectHost}
        placeholder={'Redirect host'}
        label={'Redirect host'}
        name={'redirectHost'}
        className={!!app?.redirectHost && 'disabled-input'}
        validate={{
          required: {
            value: true,
            errorMessage: translate('global.messages.validate.redirectHost.required'),
          },
        }}
      />
      <AvField
        value={app.jwksUrl || ltiConfig?.jwksUrl}
        disabled={!!app?.jwksUrl}
        placeholder={'JWKS url'}
        label={'JWKS url'}
        name={'jwksUrl'}
        className={!!app?.jwksUrl && 'disabled-input'}
        validate={{
          required: {
            value: true,
            errorMessage: translate('global.messages.validate.jwksUrl.required'),
          },
        }}
      />
    </>
  );

  const Lti1Params = () => (
    <>
      <AvField
        disabled={ltiConfig?.type === 'PLATFORM'}
        value={ltiConfig ? ltiConfig.consumerKey : ''}
        placeholder={translate('schoolabyApp.journey.app.register.consumerKey')}
        label={translate('schoolabyApp.journey.app.register.consumerKey')}
        name={'consumerKey'}
        className={ltiConfig?.type === 'PLATFORM' && 'disabled-input'}
        validate={{
          required: {
            value: true,
            errorMessage: translate('global.messages.validate.consumerkey.required'),
          },
        }}
      />
      <AvField
        disabled={ltiConfig?.type === 'PLATFORM'}
        value={ltiConfig ? ltiConfig.sharedSecret : ''}
        placeholder={translate('schoolabyApp.journey.app.register.sharedSecret')}
        label={translate('schoolabyApp.journey.app.register.sharedSecret')}
        name={'sharedSecret'}
        className={ltiConfig?.type === 'PLATFORM' && 'disabled-input'}
        validate={{
          required: {
            value: true,
            errorMessage: translate('global.messages.validate.sharedsecret.required'),
          },
        }}
      />
    </>
  );

  return app ? (
    <CustomModal backdrop={'static'} isOpen={showModal} toggle={toggle} className="modal-lg app-registration-modal p-4" centered>
      <div className={'p-3'}>
        <ModalBody className={'pl-4 pr-4 pb-4'}>
          <Row>
            <Col xs={10} s={11}>
              <h2>{app.name}</h2>
            </Col>
            <Col xs={2} s={1} className={'d-flex justify-content-end align-items-start mt-2'}>
              <CloseButton onClick={toggle} />
            </Col>
          </Row>
          <Row className={'mt-4'}>
            <Col xs={12} sm={12} lg={4} className={'d-flex justify-content-center'}>
              <img src={app.imageUrl} alt={app.name} />
            </Col>
            <Col xs={12} sm={12} lg={8} className={'pl-4 mt-1'}>
              <p>{app.description}</p>
            </Col>
          </Row>
          <Row className={'mt-5'}>
            <Col xs={12}>
              <AvForm id={'app-form'} action={'PUT'} onValidSubmit={onSubmit}>
                {isLtiAdvantage(app.version) ? <LtiAdvantageParams /> : <Lti1Params />}
                <AvField
                  value={app.launchUrl || ltiConfig?.launchUrl}
                  disabled={!!app?.launchUrl}
                  placeholder={'Launch url'}
                  label={'Launch url'}
                  name={'launchUrl'}
                  className={!!app?.launchUrl && 'disabled-input'}
                  validate={{
                    required: {
                      value: true,
                      errorMessage: translate('global.messages.validate.launchUrl.required'),
                    },
                  }}
                />
                <AvField value={app} name={'ltiApp'} type={'hidden'} />
              </AvForm>
            </Col>
          </Row>
        </ModalBody>
        {ltiConfig?.type !== 'PLATFORM' && (
          <ModalFooter className={'pl-4 pr-4 pb-4'}>
            <Button form="app-form" color="primary" type={'submit'}>
              {translate(isUpdate ? 'schoolabyApp.journey.app.register.update' : 'schoolabyApp.journey.app.register.add')}
            </Button>
          </ModalFooter>
        )}
      </div>
    </CustomModal>
  ) : null;
};
