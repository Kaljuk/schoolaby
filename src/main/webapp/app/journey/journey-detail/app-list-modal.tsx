import React from 'react';
import { Col, ModalBody, Row } from 'reactstrap';
import { translate } from 'react-jhipster';
import LinesEllipsis from 'react-lines-ellipsis';
import { ILtiApp } from 'app/shared/model/lti-app.model';
import CustomModal from 'app/shared/layout/custom-modal/custom-modal';
import AppCardNew from 'app/shared/layout/lti/app-card-new/app-card-new';
import CloseButton from 'app/shared/layout/close-button/close-button';

export type IAppListModal = {
  apps: ILtiApp[];
  toggle: () => void;
  showModal: boolean;
  onCardClick: (ILtiApp) => void;
};

export const AppListModal = ({ apps, showModal, toggle, onCardClick }: IAppListModal) => (
  <CustomModal backdrop={'static'} isOpen={showModal} toggle={toggle} className="modal-xl p-4 app-list-modal" centered>
    <ModalBody>
      <Row>
        <Col xs={10} s={11}>
          <h2>
            <LinesEllipsis text={translate('schoolabyApp.journey.app.modal.title')} maxLine={1} />
          </h2>
        </Col>
        <Col xs={2} s={1} className={'d-flex justify-content-end align-items-start mt-2'}>
          <CloseButton onClick={toggle} />
        </Col>
      </Row>
      <Row className={'mt-4'}>
        {apps.map((app, index) => (
          <AppCardNew
            onClick={() => onCardClick(app)}
            key={index}
            title={app.name}
            description={app.description}
            imageSrc={app.imageUrl}
            md={'12'}
            lg={'6'}
            xl={'6'}
          />
        ))}
      </Row>
    </ModalBody>
  </CustomModal>
);
