import React, { FC, useLayoutEffect, useState } from 'react';
import { Button, FormFeedback, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import Icon from 'app/shared/icons';
import { translate } from 'react-jhipster';
import { useHistory } from 'react-router-dom';
import { PRIMARY } from 'app/shared/util/color-utils';
import { useJoinJourney } from 'app/shared/services/journey-api';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import CustomModal from 'app/shared/layout/custom-modal/custom-modal';
import { Field, Form } from 'react-final-form';
import { Input } from 'app/shared/form';
import { useJoinJourneySchema } from 'app/shared/schema/update-entity';
import { createYupValidator } from 'app/shared/util/form-utils';
import classNames from 'classnames';
import './join-journey.scss';
import { useQueryParam } from 'app/shared/hooks/useQueryParam';
import { Spinner } from 'app/shared/layout/spinner/spinner';

const JoinJourney: FC<StateProps> = ({ locale }) => {
  const [{ signUpCode }] = useQueryParam<{ signUpCode: string }>();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [isCodeInvalid, setIsCodeInvalid] = useState(false);
  const history = useHistory();

  const handleSuccess = () => journeyId => history.push(`/journey/${journeyId}`);

  const handleError = () => (signUpCode ? history.push('/') : setIsCodeInvalid(true));

  const { mutateAsync: joinJourney, isLoading: isJoiningJourney } = useJoinJourney(handleSuccess, handleError);
  const schema = useJoinJourneySchema(locale);

  const handleJoinJourney = ({ signUpCodeValue }) => joinJourney(signUpCodeValue);

  useLayoutEffect(() => {
    signUpCode ? handleJoinJourney({ signUpCodeValue: signUpCode }) : setIsLoading(false);
  }, [signUpCode]);

  return isLoading ? (
    <Spinner />
  ) : (
    <CustomModal isOpen contentClassName="join-journey p-2 new">
      <ModalHeader tag={'h2'} className="border-bottom-0">
        <div className="d-flex align-items-center">
          <Icon name="export" className="mr-2" fill={PRIMARY} width="33" height="28" />
          {translate('schoolabyApp.journey.join.title')}
        </div>
      </ModalHeader>
      <ModalBody>
        <Form onSubmit={handleJoinJourney} validate={createYupValidator(schema)}>
          {({ handleSubmit }) => (
            <form onSubmit={handleSubmit} id="joinJourneyForm">
              <Field
                id="signUpCodeInput"
                name="signUpCodeValue"
                className={classNames({ 'is-invalid': isCodeInvalid })}
                label={translate('schoolabyApp.journey.signUpCode')}
                component={Input}
              />
              {isCodeInvalid && <FormFeedback>{translate('schoolabyApp.journey.join.invalidSignUpCode')}</FormFeedback>}
            </form>
          )}
        </Form>
      </ModalBody>
      <ModalFooter className="border-top-0 container">
        <div className="container">
          <div className="row justify-content-end">
            <Button className="btn-white col-md-3 mr-md-2 mb-md-0 mb-1" onClick={() => history.push('/journey')}>
              {translate('entity.action.cancel')}
            </Button>
            <Button type="submit" color="primary" className="col-md-4" form="joinJourneyForm" disabled={isJoiningJourney}>
              {isJoiningJourney ? translate('schoolabyApp.journey.join.joining') : translate('schoolabyApp.journey.join.submit')}
            </Button>
          </div>
        </div>
      </ModalFooter>
    </CustomModal>
  );
};

const mapStateToProps = ({ locale }: IRootState) => ({
  locale: locale.currentLocale,
});

type StateProps = ReturnType<typeof mapStateToProps>;
export default connect(mapStateToProps)(JoinJourney);
