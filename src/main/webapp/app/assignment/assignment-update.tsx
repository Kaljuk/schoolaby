import React, { FC, useCallback, useEffect, useState } from 'react';
import RightSidePanel from 'app/shared/layout/right-side-panel/right-side-panel';
import { translate } from 'react-jhipster';
import {
  useDisplayEntityUpdateState,
  useEntityToUpdateState,
  useEntityUpdateIdState,
  useParentEducationalAlignmentsState,
  useSelectedEducationalAlignmentsState,
} from 'app/shared/contexts/entity-update-context';
import { createYupValidator } from 'app/shared/util/form-utils';
import { convertDateTimeToServer, getEndOfDay } from 'app/shared/util/date-utils';
import DatePickerNew from 'app/shared/layout/date-picker-new/date-picker-new';
import { DatePicker, Input, MarkdownEditorInput, Radio } from 'app/shared/form';
import { Field, Form } from 'react-final-form';
import { Button } from 'reactstrap';
import { useUpdateAssignmentEntitySchema } from 'app/shared/schema/update-entity';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { useCreateAssignment, useGetAssignment, useUpdateAssignment } from 'app/shared/services/assignment-api';
import { useGetGradingSchemes } from 'app/shared/services/grading-scheme-api';
import { useGetJourney, useGetJourneyStudents } from 'app/shared/services/journey-api';
import SpinnerContainer from 'app/shared/layout/spinner-container/spinner-container';
import { IAssignment } from 'app/shared/model/assignment.model';
import { mapEntityIdList } from 'app/shared/util/entity-utils';
import { useChosenMaterialState, useMaterialState } from 'app/shared/contexts/material-context';
import { useGetMilestone } from 'app/shared/services/milestone-api';
import classNames from 'classnames';
import EducationalAlignmentMultiselect from 'app/shared/layout/educational-alignment-multiselect/educational-alignment-multiselect';
import { DATEPICKER_DATE_TIME_FORMAT } from 'app/shared/form/components/date-picker';
import Icon from 'app/shared/icons';
import { DateTime } from 'luxon';
import ChooseMaterials from 'app/shared/layout/choose-materials/choose-materials';
import { useHistory } from 'react-router-dom';
import sortBy from 'lodash/sortBy';
import { educationalAlignmentsDifferenceByTitle, handleDuplicateEducationalAlignments } from 'app/shared/util/educational-alignment-utils';
import { mergeMaterials } from 'app/shared/util/lti-utils';
import { useGetLtiResources } from 'app/shared/services/lti-api';
import MaterialCardModal from 'app/shared/layout/material-card/material-card-modal';
import { LTI_MATERIAL } from 'app/shared/model/material.model';
import { LTI_MODAL_ACTION_UPDATE, LtiCreateModal } from 'app/shared/layout/lti/lti-create-modal';
import { createPortal } from 'react-dom';
import SelectStudents from 'app/assignment/select-students/select-students';
import { mapToUserIdList } from 'app/shared/model/journey-student.model';
import { OnChange } from 'react-final-form-listeners';
import RubricLink from 'app/assignment/assessment-rubric-new/rubric-link/rubric-link';
import { useGetRubric } from 'app/shared/services/rubric-api';
import { COUNTRY } from 'app/config/constants';

const AssignmentUpdate: FC<StateProps> = ({ locale, isInProduction, isUserUkrainian }) => {
  const history = useHistory();
  const { closeEntityUpdate } = useDisplayEntityUpdateState();
  const { assignmentUpdateId, milestoneUpdateId } = useEntityUpdateIdState();
  const { selectedEducationalAlignments, setSelectedEducationalAlignments } = useSelectedEducationalAlignmentsState();
  const { parentEducationalAlignments, setParentEducationalAlignments } = useParentEducationalAlignmentsState();
  const { entity, setEntity } = useEntityToUpdateState<IAssignment>();
  const { data: existingAssignment } = useGetAssignment(assignmentUpdateId, !!assignmentUpdateId);
  const { data: ltiResources } = useGetLtiResources({ assignmentId: existingAssignment?.id }, !!existingAssignment?.id);
  const { data: milestone } = useGetMilestone(
    milestoneUpdateId || existingAssignment?.milestoneId || entity?.milestoneId,
    !!milestoneUpdateId || !!existingAssignment?.milestoneId || !!entity?.milestoneId
  );
  const { materials, setMaterials } = useMaterialState();
  const { chosenMaterial, setChosenMaterial } = useChosenMaterialState();
  const journeyId = existingAssignment?.journeyId || milestone?.journeyId;
  const { data: journey } = useGetJourney(journeyId, !!existingAssignment || !!milestone);
  const { data: gradingSchemes } = useGetGradingSchemes();
  const { mutate: createAssignment } = useCreateAssignment(closeEntityUpdate);
  const { mutate: updateAssignment } = useUpdateAssignment(assignmentUpdateId, () => {
    closeEntityUpdate();
  });
  const { data: journeyStudents } = useGetJourneyStudents(journeyId, !!journeyId);
  const { data: rubric } = useGetRubric(existingAssignment?.id);
  const [radio, setRadio] = useState<string>();
  const [goToMarketplace, setGoToMarketplace] = useState<boolean>(false);
  const [goToRubricView, setGoToRubricView] = useState<boolean>(false);
  const [gradingSchemeChosen, setGradingSchemeChosen] = useState(false);
  const isNew = !assignmentUpdateId;

  const toggleModal = () => setChosenMaterial(null);

  const setMergedMaterials = assignment => {
    if (assignment && (assignment.ltiResources || ltiResources)) {
      const resources = assignment?.ltiResources || ltiResources || [];
      setMaterials(sortBy(mergeMaterials(assignment, resources), 'sequenceNumber'));
    }
  };

  useEffect(() => {
    return () => {
      setSelectedEducationalAlignments([]);
      setMaterials([]);
      setEntity(null);
    };
  }, []);

  useEffect(() => {
    setMergedMaterials(entity);
  }, [entity, ltiResources]);

  useEffect(() => {
    const published = entity ? entity.published : existingAssignment?.published;
    if (!assignmentUpdateId && !entity) {
      setRadio('published');
    } else if (!published) {
      setRadio('unpublished');
    } else if (DateTime.fromISO(published).toUTC() <= DateTime.local().toUTC()) {
      setRadio('published');
    } else {
      setRadio('publishedAt');
    }
  }, [existingAssignment?.published]);

  useEffect(() => {
    !entity && existingAssignment?.educationalAlignments && setSelectedEducationalAlignments(existingAssignment.educationalAlignments);
  }, [assignmentUpdateId]);

  useEffect(() => {
    if (existingAssignment?.id && !entity) {
      setMergedMaterials(existingAssignment);
    }
  }, [existingAssignment, ltiResources]);

  const existingAssignmentIfExists = existingAssignment
    ? {
        ...existingAssignment,
        students: existingAssignment.students?.length
          ? existingAssignment.students?.map(student => student.id)
          : mapToUserIdList(journeyStudents),
        rubric,
      }
    : {
        flexibleDeadline: true,
        requiresSubmission: true,
        gradingSchemeId: 0,
        deadline: getEndOfDay(),
        students: mapToUserIdList(journeyStudents),
      };

  const initialValues = entity
    ? {
        ...entity,
        students: entity.students?.length ? entity.students?.map(student => student.id) : mapToUserIdList(journeyStudents),
      }
    : existingAssignmentIfExists;

  const memoizedInitialValues = useCallback(() => initialValues, [existingAssignment, journeyStudents, rubric]);

  useEffect(() => {
    const milestoneEducationalAlignments = handleDuplicateEducationalAlignments(milestone?.educationalAlignments);
    const filteredMilestoneEducationalAlignments = educationalAlignmentsDifferenceByTitle(
      milestoneEducationalAlignments,
      selectedEducationalAlignments
    );
    setParentEducationalAlignments(filteredMilestoneEducationalAlignments);
  }, [milestone]);

  const saveAssignment = values => {
    values.deadline = convertDateTimeToServer(values.deadline);
    values.deleted = convertDateTimeToServer(values.deleted);

    if (!entity && !assignmentUpdateId && radio === 'published') {
      values.published = DateTime.local();
    }

    if (values.published !== null) {
      values.published = convertDateTimeToServer(values.published);
    }

    const assignment: IAssignment = {
      ...existingAssignment,
      ...values,
      materials: materials.filter(material => material.type !== 'LTI'),
      educationalAlignments: mapEntityIdList(selectedEducationalAlignments),
      ltiResources: materials.filter(material => material.type === 'LTI').map(material => material.getLtiResource()),
      milestoneId: existingAssignment?.milestoneId || milestoneUpdateId || entity.milestoneId,
      students:
        values.students.length < journey?.studentCount
          ? journeyStudents.map(journeyStudent => journeyStudent.user).filter(user => values.students.includes(user.id))
          : [],
    };

    const historyState = {
      entity: {
        ...assignment,
        educationalAlignments: handleDuplicateEducationalAlignments(selectedEducationalAlignments),
        materials: materials.map(material => material.getCopy()),
      },
      parentEducationalAlignments,
      back: history.location,
      journeyId: journey?.id,
    };

    if (goToMarketplace) {
      const url = values.id ? `/marketplace/assignment/${values.id}` : '/marketplace/assignment/new';
      history.push({
        pathname: url,
        state: historyState,
      });
    } else if (goToRubricView) {
      const url = values.id ? `/assignment/${values.id}/assessment-rubric` : '/assignment/new/assessment-rubric';
      history.push({
        pathname: url,
        state: historyState,
      });
    } else if (isNew) {
      createAssignment(assignment);
    } else {
      updateAssignment(assignment);
    }
  };

  const schema = useUpdateAssignmentEntitySchema(locale, journey?.curriculum?.requiresSubject, !!journeyStudents?.length, !isUserUkrainian);

  return (
    <>
      <RightSidePanel title={isNew ? translate('entity.add.task') : translate('entity.edit.task')} closePanel={closeEntityUpdate}>
        <div className="entity-update">
          <SpinnerContainer displaySpinner={assignmentUpdateId && !existingAssignment} spinnerClassName="position-absolute">
            <Form
              onSubmit={saveAssignment}
              initialValues={memoizedInitialValues()}
              validate={createYupValidator(schema)}
              mutators={{
                selectAllStudents(args, state, utils) {
                  utils.changeValue(state, 'students', () => (args[0] ? journeyStudents?.map<number>(student => student.user.id) : []));
                },
                setGradingSchemeToNone(args, state, utils) {
                  utils.changeValue(state, 'gradingSchemeId', () => gradingSchemes.find(gs => gs.code === 'NONE').id);
                },
                removeRubric(args, state, utils) {
                  utils.changeValue(state, 'rubric', () => undefined);
                },
              }}
            >
              {({ handleSubmit, values, form, errors, touched, hasValidationErrors }) => {
                useEffect(() => {
                  hasValidationErrors && goToMarketplace && setGoToMarketplace(false);
                }, [hasValidationErrors, goToMarketplace]);

                useEffect(() => {
                  hasValidationErrors && goToRubricView && setGoToRubricView(false);
                }, [hasValidationErrors, goToRubricView]);

                return (
                  <form onSubmit={handleSubmit}>
                    <Field
                      id="assignment-title"
                      name="title"
                      type="text"
                      placeholder={translate('schoolabyApp.assignment.titlePlaceholder')}
                      label={translate('schoolabyApp.assignment.title')}
                      component={Input}
                    />
                    <Field
                      id="assignment-description"
                      name="description"
                      type="textarea"
                      label={translate('schoolabyApp.assignment.description')}
                      component={MarkdownEditorInput}
                    />
                    <Field
                      id="assignment-educational-alignments"
                      name="educationalAlignments"
                      component={EducationalAlignmentMultiselect}
                      disabled={!journey?.curriculum?.requiresSubject}
                    />
                    <Field
                      id="assignment-grading-scheme"
                      label={translate('schoolabyApp.assignment.gradingScheme')}
                      type="select"
                      className={classNames({
                        'disabled-input': !!existingAssignment?.hasGrades,
                        placeholder: values.gradingSchemeId === 0,
                        'bg-none': true,
                      })}
                      containerClassName="mt-3"
                      name="gradingSchemeId"
                      disabled={existingAssignment?.hasGrades}
                      iconName="caretDown"
                      component={Input}
                    >
                      <option value={0} disabled>
                        {translate('entity.action.select')}
                      </option>
                      {gradingSchemes && gradingSchemes.length
                        ? gradingSchemes.map(gradingScheme => (
                            <option value={gradingScheme.id} key={gradingScheme.id}>
                              {translate(`schoolabyApp.gradingScheme.code.${gradingScheme.code}`)}
                            </option>
                          ))
                        : null}
                    </Field>
                    <OnChange name={'gradingSchemeId'}>{() => setGradingSchemeChosen(true)}</OnChange>
                    {/* TODO: SCHOOL-970 Remove production check */}
                    {!isInProduction && <RubricLink className="mb-3" setGoToRubric={setGoToRubricView} rubric={values.rubric} />}
                    <Field
                      id="assignment-requires-submission"
                      name="requiresSubmission"
                      type="checkbox"
                      label={translate('schoolabyApp.assignment.requiresSubmission')}
                      labelClassName="ml-2 mb-0"
                      labelAfterInput
                      component={Input}
                    />
                    <OnChange name={'requiresSubmission'}>
                      {(value: boolean) => !value && isNew && !gradingSchemeChosen && form.mutators.setGradingSchemeToNone(value)}
                    </OnChange>
                    <label htmlFor="assignment-deadline">{translate('schoolabyApp.assignment.deadline')}</label>
                    <div className="d-flex align-items-center">
                      <Field
                        id="assignment-deadline"
                        name="deadline"
                        type="date"
                        className="entity-datepicker"
                        dateTimeFormat={DATEPICKER_DATE_TIME_FORMAT}
                        component={DatePickerNew}
                      />
                      <Field
                        id="assignment-flexible-deadline"
                        name="flexibleDeadline"
                        type="checkbox"
                        label={translate('schoolabyApp.assignment.flexibleDeadline')}
                        labelClassName="ml-2 mb-0"
                        labelAfterInput
                        className={classNames({
                          'ml-3': true,
                          'mb-4': !!errors.deadline && touched.deadline,
                        })}
                        component={Input}
                      />
                    </div>
                    <Radio
                      name="published"
                      label={translate('entity.publishedState.published')}
                      onClick={() => setRadio('published')}
                      onChange={input => form.change(input.name, convertDateTimeToServer(DateTime.local()))}
                      checked={radio === 'published'}
                    />
                    <Radio
                      name="published"
                      label={translate('entity.publishedState.unpublished')}
                      onClick={() => setRadio('unpublished')}
                      onChange={input => form.change(input.name, null)}
                      checked={radio === 'unpublished'}
                    />
                    <span className={'published-date-container'}>
                      <Radio
                        name="published"
                        className="d-inline"
                        label={translate('entity.publishedState.publishAt')}
                        onClick={() => setRadio('publishedAt')}
                        onChange={input => form.change(input.name, DateTime.local().startOf('day').plus({ days: 1 }))}
                        checked={radio === 'publishedAt'}
                      />
                      {radio === 'publishedAt' && (
                        <Field
                          label={'Publish at'}
                          type="datetime-local"
                          name="published"
                          placeholder={'YYYY-MM-DD HH:mm:ss'}
                          render={dateProps => (
                            <span className="published-date mt-1">
                              <>
                                <DatePicker meta={dateProps.meta} input={dateProps.input} dateTimeFormat={DATEPICKER_DATE_TIME_FORMAT} />
                                <Icon onClick={() => dateProps.input.click()} className={'published-date-icon'} name={'calendarNew'} />
                              </>
                            </span>
                          )}
                        />
                      )}
                    </span>
                    <hr />
                    <ChooseMaterials setGoToMarketplace={setGoToMarketplace} isAllowedToModify />
                    <SelectStudents
                      journeyStudents={journeyStudents}
                      selectedStudentsIds={values.students}
                      selectAll={form.mutators.selectAllStudents}
                      journey={journey}
                    />
                    <div className="entity-update-footer d-flex justify-content-end mt-3">
                      <Button className="cancel-btn bg-white px-4" type="button" onClick={closeEntityUpdate}>
                        {translate('entity.action.cancel')}
                      </Button>
                      <Button data-testid="save-entity" className="ml-2 px-4" color="primary">
                        {translate('entity.action.save')}
                      </Button>
                    </div>
                  </form>
                );
              }}
            </Form>
          </SpinnerContainer>
        </div>
      </RightSidePanel>
      <MaterialCardModal
        isOpen={!!chosenMaterial && chosenMaterial?.type !== LTI_MATERIAL}
        material={chosenMaterial}
        addable
        removable
        toggleModal={toggleModal}
      />
      {chosenMaterial?.type === LTI_MATERIAL &&
        createPortal(
          <LtiCreateModal
            showModal={!!chosenMaterial}
            title={translate('schoolabyApp.assignment.lti.edit')}
            ltiResource={chosenMaterial?.getLtiResource()}
            onClose={toggleModal}
            journeyId={journeyId}
            action={LTI_MODAL_ACTION_UPDATE}
          />,
          document.getElementById('lti-form')
        )}
    </>
  );
};

const mapStateToProps = ({ locale, applicationProfile, authentication }: IRootState) => ({
  locale: locale.currentLocale,
  isInProduction: applicationProfile.inProduction,
  isUserUkrainian: authentication.account.country === COUNTRY.UKRAINE,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(AssignmentUpdate);
