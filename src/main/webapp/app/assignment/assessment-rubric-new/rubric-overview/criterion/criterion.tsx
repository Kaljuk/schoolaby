import React, { FC } from 'react';
import './criterion.scss';
import { ICriterion } from 'app/shared/model/rubric/criterion.model';
import { FormFeedback } from 'reactstrap';

interface CriterionProps {
  criterion: ICriterion;
  errorMessage?: string;
}

const Criterion: FC<CriterionProps> = ({ criterion, errorMessage }) => {
  return (
    <div className="criterion mx-1 py-2">
      <h6 className="mb-3 criterion-title">{criterion.title}</h6>
      {!!errorMessage && (
        <div className="mb-2 mt-n2">
          <FormFeedback className={'form-feedback'}>{errorMessage}</FormFeedback>
        </div>
      )}
      <p>{criterion.description}</p>
    </div>
  );
};

export default Criterion;
