import React, { FC, useContext, useEffect, useMemo, useState } from 'react';
import './rubric-overview.scss';
import LargeModal from 'app/shared/layout/large-modal/large-modal';
import { useRubricTemplateState, useSelectedRubricState } from 'app/shared/contexts/entity-update-context';
import CustomModalHeader from 'app/shared/layout/custom-modal/custom-modal-header';
import Criterion from './criterion/criterion';
import CriterionLevel from 'app/assignment/assessment-rubric-new/rubric-overview/criterion-level/criterion-level';
import { ModalFooter } from 'reactstrap';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import { translate } from 'react-jhipster';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { useDeleteRubric, useUpdateRubric } from 'app/shared/services/rubric-api';
import ConfirmationModalNew from 'app/shared/layout/confirmation-modal-new/confirmation-modal-new';
import { toast } from 'react-toastify';
import RubricViewModes from 'app/assignment/assessment-rubric-new/rubric-overview/rubric-view-modes';
import { Form } from 'react-final-form';
import { createYupValidator } from 'app/shared/util/form-utils';
import { useFeedbackRubricSchema } from 'app/shared/schema/update-entity';
import arrayMutators from 'final-form-arrays';
import { getCriterionLevelColorClass, getCriterionLevelCoordinatesByIdFromRubric } from 'app/shared/util/rubric-utils';
import cloneDeep from 'lodash/cloneDeep';
import { ISelectedCritreionLevel } from 'app/shared/model/rubric/selected-critreion-level.model';
import { FieldArray } from 'react-final-form-arrays';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { IRubric } from 'app/shared/model/rubric/rubric.model';

interface RubricOverviewProps extends StateProps {
  viewMode: RubricViewModes;
  selectedLevels?: ISelectedCritreionLevel[];
  saveSelectedLevels?: (selectedLevels: ISelectedCritreionLevel[]) => void;
  studentFullName?: string;
}

const RubricOverview: FC<RubricOverviewProps> = ({ viewMode, selectedLevels, saveSelectedLevels, account, locale, studentFullName }) => {
  const { selectedRubric, setSelectedRubric } = useSelectedRubricState();
  const { setSelectingRubricTemplate } = useRubricTemplateState();
  const [showDeleteRubricTemplateModal, setShowDeleteRubricTemplateModal] = useState<boolean>(false);
  const [visuallyModifiedRubric, setVisuallyModifiedRubric] = useState<IRubric>(cloneDeep(selectedRubric));
  const assignmentContext = useContext(AssignmentContext);

  const showTemplateRemovedToast = () => {
    toast.success(translate('schoolabyApp.assignment.rubric.template.unSavedToast'));
  };
  const { mutate: updateRubric } = useUpdateRubric(showTemplateRemovedToast);
  const { mutate: deleteRubric } = useDeleteRubric(showTemplateRemovedToast);

  const closeModal = () => {
    setSelectedRubric(undefined);
  };

  const handleDeleteRubricTemplate = () => {
    if (selectedRubric.assignmentId) {
      const rubric = {
        ...selectedRubric,
        isTemplate: false,
      };
      updateRubric(rubric);
    } else {
      deleteRubric(selectedRubric.id);
    }
    closeModal();
  };

  const getInitialValues = () => {
    const initialValues = cloneDeep(selectedRubric);
    selectedLevels?.forEach(selectedCriterionLevel => {
      const coordinates =
        selectedRubric && getCriterionLevelCoordinatesByIdFromRubric(selectedCriterionLevel.criterionLevel.id, selectedRubric);
      if (coordinates) {
        initialValues.criterions[coordinates.criterionIndex].selected = String(selectedCriterionLevel.criterionLevel.id);
        initialValues.criterions[coordinates.criterionIndex].levels[coordinates.criterionLevelIndex].description =
          selectedCriterionLevel.modifiedDescription;
        initialValues.criterions[coordinates.criterionIndex].levels[coordinates.criterionLevelIndex].selectedLevelId =
          selectedCriterionLevel.id;
      }
    });

    return initialValues;
  };

  const getLevelClassFromFormValues = (formValues, criterionLevel, criterionIndex) => {
    if (formValues.criterions.map(criterion => criterion.selected).includes(String(criterionLevel.id))) {
      return getCriterionLevelColorClass(criterionLevel, formValues.criterions[criterionIndex].levels.length);
    }
    return '';
  };

  const getLevelClassFromSelectedLevels = (criterionLevel, criterionIndex) => {
    if (selectedLevels?.map(level => level.criterionLevel.id).includes(criterionLevel.id)) {
      return getCriterionLevelColorClass(criterionLevel, selectedRubric.criterions[criterionIndex].levels.length);
    }
    return '';
  };

  const findSelectedCriterionLevelFromCriterion = criterion => criterion.levels.find(level => level.id === Number(criterion.selected));

  const mapCriterionLevelIntoSelectedLevel = (criterionLevel): ISelectedCritreionLevel => ({
    id: criterionLevel.selectedLevelId,
    modifiedDescription: criterionLevel.description,
    criterionLevel,
  });

  const mapValuesIntoSelectedLevels = (values): ISelectedCritreionLevel[] => {
    const selectedCriterionLevels = [];

    values.criterions.forEach(criterion => {
      const selectedLevelFromCriterion = findSelectedCriterionLevelFromCriterion(criterion);
      selectedCriterionLevels.push(mapCriterionLevelIntoSelectedLevel(selectedLevelFromCriterion));
    });

    return selectedCriterionLevels;
  };

  const handleSaveAndCloseModal = values => {
    saveSelectedLevels(mapValuesIntoSelectedLevels(values));
    closeModal();
  };

  const setModifiedRubricValues = () => {
    const modifiedRubric = cloneDeep(selectedRubric);
    selectedLevels?.forEach(selectedLevel => {
      const coordinates = modifiedRubric && getCriterionLevelCoordinatesByIdFromRubric(selectedLevel.criterionLevel.id, modifiedRubric);
      if (coordinates) {
        modifiedRubric.criterions[coordinates.criterionIndex].levels[coordinates.criterionLevelIndex].description =
          selectedLevel.modifiedDescription;
      }
    });
    setVisuallyModifiedRubric(modifiedRubric);
  };

  useEffect(() => {
    selectedRubric && selectedLevels && setModifiedRubricValues();
  }, [selectedRubric, selectedLevels]);

  const RubricOverviewFooter = () => {
    if (viewMode === RubricViewModes.GRADE) {
      return (
        <>
          <CustomButton className="px-3" onClick={closeModal} buttonType="cancel" title={translate('entity.action.cancel')} />
          <CustomButton
            className="px-3"
            buttonType="primary"
            title={translate('entity.action.save')}
            type="submit"
            form="rubric-grading-form"
          />
        </>
      );
    }

    if (viewMode === RubricViewModes.TEMPLATE_PREVIEW) {
      return (
        <>
          <CustomButton className="px-3" onClick={closeModal} buttonType="cancel" title={translate('entity.action.cancel')} />
          {selectedRubric.createdBy === account.login && (
            <CustomButton
              className="px-3"
              onClick={() => {
                setShowDeleteRubricTemplateModal(true);
              }}
              buttonType="danger"
              title={translate('schoolabyApp.assignment.rubric.template.removeTemplate')}
            />
          )}
          <CustomButton
            className="px-3"
            onClick={() => setSelectingRubricTemplate(true)}
            buttonType="primary"
            title={translate('entity.action.select')}
          />
        </>
      );
    }

    return (
      <>
        <CustomButton className="px-3" onClick={closeModal} buttonType="cancel" title={translate('global.close')} />
      </>
    );
  };

  const GradableModalBodyAndFooter = () => (
    <>
      <div className="criterions px-3 mx-n1">
        <Form
          onSubmit={handleSaveAndCloseModal}
          initialValues={getInitialValues()}
          mutators={{
            ...arrayMutators,
          }}
          validate={createYupValidator(useFeedbackRubricSchema(locale))}
        >
          {({ handleSubmit, values, errors, submitFailed }) => {
            return (
              <form id="rubric-grading-form" onSubmit={handleSubmit}>
                {visuallyModifiedRubric?.criterions.map((criterion, criterionIndex) => (
                  <div className="criterion-row d-flex mb-2" key={`criteiron-${criterion.id}`}>
                    <Criterion criterion={criterion} errorMessage={!!submitFailed && errors?.criterions?.[criterionIndex]?.selected} />
                    <FieldArray name={`criterions[${criterionIndex}].levels`}>
                      {({ fields: criterionLevelFields }) =>
                        useMemo(
                          () =>
                            criterionLevelFields.map((criterionLevelField, levelIndex) => {
                              const criterionLevel = criterion.levels[levelIndex];
                              return (
                                <CriterionLevel
                                  key={`criterionLevel-${criterionLevel.id}`}
                                  criterionLevel={criterionLevel}
                                  gradable={assignmentContext?.isAllowedToModify && viewMode === RubricViewModes.GRADE}
                                  fieldName={criterionLevelField}
                                  criterionIndex={criterionIndex}
                                  selectedClass={getLevelClassFromFormValues(values, criterionLevel, criterionIndex)}
                                  onLevelSelectionChange={(event, input) => {
                                    input.onChange(event);
                                  }}
                                />
                              );
                            }),
                          [criterionLevelFields]
                        )
                      }
                    </FieldArray>
                  </div>
                ))}
              </form>
            );
          }}
        </Form>
      </div>
      <ModalFooter className="d-flex align-items-end">
        <RubricOverviewFooter />
      </ModalFooter>
    </>
  );

  const ModalBodyAndFooter: FC = () => {
    return (
      <>
        <div className="criterions px-3 mx-n1">
          {visuallyModifiedRubric?.criterions.map((criterion, criterionIndex) => (
            <div className="criterion-row d-flex mb-2" key={`criteiron-${criterion.id}`}>
              <Criterion criterion={criterion} />
              {criterion.levels.map(criterionLevel => (
                <CriterionLevel
                  key={`criterionLevel-${criterionLevel.id}`}
                  criterionLevel={criterionLevel}
                  gradable={assignmentContext?.isAllowedToModify && viewMode === RubricViewModes.GRADE}
                  criterionIndex={criterionIndex}
                  selectedClass={getLevelClassFromSelectedLevels(criterionLevel, criterionIndex)}
                />
              ))}
            </div>
          ))}
        </div>
        <ModalFooter className="d-flex align-items-end">
          <RubricOverviewFooter />
        </ModalFooter>
      </>
    );
  };

  return useMemo(
    () => (
      <>
        <LargeModal isOpen toggle={closeModal} className="rubric-overview-new" contentClassName="px-4" data-testid="rubric-overview">
          <CustomModalHeader toggle={closeModal}>
            <h5 className="view-title m-0 font-weight-bold">
              {viewMode === RubricViewModes.GRADE && studentFullName?.length
                ? translate('schoolabyApp.assignment.rubric.gradingTitle', { fullName: studentFullName })
                : visuallyModifiedRubric?.title}
            </h5>
          </CustomModalHeader>
          {assignmentContext?.isAllowedToModify ? <GradableModalBodyAndFooter /> : <ModalBodyAndFooter />}
        </LargeModal>
        {showDeleteRubricTemplateModal && (
          <ConfirmationModalNew
            isModalOpen={showDeleteRubricTemplateModal}
            closeModal={() => {
              setShowDeleteRubricTemplateModal(false);
            }}
            modalTitle={translate('schoolabyApp.assignment.rubric.template.unSaveModalTitle')}
            handleConfirmation={handleDeleteRubricTemplate}
          >
            <p className="mt-2">{translate('schoolabyApp.assignment.rubric.template.unSaveModalContent')}</p>
          </ConfirmationModalNew>
        )}
      </>
    ),
    [visuallyModifiedRubric]
  );
};

const mapStateToProps = ({ locale, authentication }: IRootState) => ({
  locale: locale.currentLocale,
  account: authentication.account,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(RubricOverview);
