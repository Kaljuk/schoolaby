import React, { FC } from 'react';
import './criterion-level.scss';
import { ICriterionLevel } from 'app/shared/model/rubric/criterion-level.model';
import { Field } from 'react-final-form';
import { Input } from 'app/shared/form';

interface CriterionLevelProps {
  criterionLevel: ICriterionLevel;
  criterionIndex: number;
  gradable?: boolean;
  fieldName?: string;
  onLevelSelectionChange?: (event, input) => void;
  selectedClass?: string;
}

const CriterionLevel: FC<CriterionLevelProps> = ({
  criterionLevel,
  criterionIndex,
  gradable = false,
  selectedClass,
  fieldName,
  onLevelSelectionChange,
}) => {
  return (
    <div className={`criterion-level ${selectedClass ? selectedClass : ''} p-3 mx-1 ${gradable ? 'd-flex flex-column' : ''}`}>
      <div className="d-flex justify-content-between align-items-center">
        <h6 className="criterion-level-points text-center">{criterionLevel.points}</h6>
        {!!gradable && (
          <Field name={`criterions[${criterionIndex}].selected`} className="d-inline-block" type="radio" value={String(criterionLevel.id)}>
            {({ input }) => {
              return (
                <div className="radio-button">
                  <input
                    name={input.name}
                    type="radio"
                    value={input.value}
                    checked={input.checked}
                    onChange={event => onLevelSelectionChange(event, input)}
                  />
                  <span className="radio-control" />
                </div>
              );
            }}
          </Field>
        )}
      </div>
      <h6 className="criterion-level-title">{criterionLevel.title}</h6>
      {gradable ? (
        <Field
          name={`${fieldName}.description`}
          containerClassName="flex-grow-1 mb-0"
          className="h-100"
          type="textarea"
          component={Input}
        />
      ) : (
        <p className="m-0">{criterionLevel.description}</p>
      )}
    </div>
  );
};

export default CriterionLevel;
