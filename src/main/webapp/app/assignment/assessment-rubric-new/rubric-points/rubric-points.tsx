import React, { FC } from 'react';
import './rubric-points.scss';
import { Col, Row } from 'reactstrap';
import { IRubric } from 'app/shared/model/rubric/rubric.model';
import { ISelectedCritreionLevel } from 'app/shared/model/rubric/selected-critreion-level.model';
import { ICriterion } from 'app/shared/model/rubric/criterion.model';
import { calculateSelectedCriterionTotal, getPointsColorClass } from 'app/shared/util/rubric-utils';
import { translate } from 'react-jhipster';

interface RubricPointsProps {
  rubric: IRubric;
  selectedCriterionLevels: ISelectedCritreionLevel[];
  className?: string;
}

const RubricPoints: FC<RubricPointsProps> = ({ rubric, selectedCriterionLevels, className }) => {
  const getSelectedLevelForCriterion = (criterion: ICriterion) => {
    return selectedCriterionLevels?.length
      ? selectedCriterionLevels.filter(selectedLevel => selectedLevel.criterionLevel.criterionId === criterion.id)[0]
      : undefined;
  };

  const RubricPointsRow: FC<{ criterion: ICriterion }> = ({ criterion }) => {
    const selectedLevel = getSelectedLevelForCriterion(criterion);
    const criterionPointsColorClass = selectedLevel
      ? getPointsColorClass(selectedLevel?.criterionLevel?.points, criterion.minPoints, criterion.maxPoints)
      : '';

    return (
      <Row className={`my-3 ${criterionPointsColorClass}`}>
        <Col>
          <p className="mb-0">{criterion.title}</p>
        </Col>
        <Col>
          <p className="mb-0 text-right">{`${selectedLevel?.criterionLevel?.points || 0}/${criterion.maxPoints}`}</p>
        </Col>
      </Row>
    );
  };

  return (
    <div className={`rubric-points ${className ? className : ''}`}>
      <Row className="rubric-points-header mb-3">
        <Col>
          <p className="mb-0">{translate('schoolabyApp.assignment.detail.criterion')}</p>
        </Col>
        <Col>
          <p className="mb-0 text-right">{translate('schoolabyApp.assignment.detail.points')}</p>
        </Col>
      </Row>
      <div className="rubric-points-body">
        {rubric?.criterions.map(criterion => (
          <RubricPointsRow key={criterion.id} criterion={criterion} />
        ))}
        <Row className="my-3 total-points">
          <Col>
            <p className="mb-0">{translate('schoolabyApp.assignment.rubric.assessmentRubricTotalPoints')}</p>
          </Col>
          <Col>
            <p className="mb-0 text-right">{`${calculateSelectedCriterionTotal(selectedCriterionLevels)}/${rubric?.maxPoints}`}</p>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default RubricPoints;
