import React, { FC, useMemo, useState } from 'react';
import './criterion-update.scss';
import { Input } from 'app/shared/form';
import Icon from 'app/shared/icons';
import ContentCard from 'app/shared/layout/content-card/content-card';
import { Field, useForm } from 'react-final-form';
import { Button } from 'reactstrap';
import { translate } from 'react-jhipster';
import classNames from 'classnames';
import { FieldArray } from 'react-final-form-arrays';
import CriterionLevelUpdate from 'app/assignment/assessment-rubric-new/rubric-form-content/criterion-level-update/criterion-level-update';
import DeleteModal from 'app/shared/layout/delete-modal/delete-modal';
import { SBY_PRIMARY } from 'app/shared/util/color-utils';
import { createDefaultCriterionLevel } from 'app/assignment/assessment-rubric-new/assessment-rubric';
import { increaseSequenceNumbers } from '../rubric-form-content';
import sortBy from 'lodash/sortBy';

interface CriterionProps {
  formFieldName: string;
  onDeleteClick: () => void;
  criterionIndex: number;
  setRubricAndRestartForm: (rubric, form) => void;
  deleteDisabled?: boolean;
  indicateDelete?: boolean;
}

const CriterionUpdate: FC<CriterionProps> = ({
  formFieldName,
  onDeleteClick,
  criterionIndex,
  setRubricAndRestartForm,
  deleteDisabled = false,
  indicateDelete = false,
}) => {
  const [deleteCriterionLevelIndex, setDeleteCriterionLevelIndex] = useState(undefined);
  const form = useForm();

  const closeDeleteCriterionLevelDialog = () => setDeleteCriterionLevelIndex(undefined);

  const getRubricCriterionLevels = rubric => rubric.criterions[criterionIndex].levels;

  const createNewCriterionLevel = (index: number) => {
    const rubric = form.getState().values;
    const criterionLevels = getRubricCriterionLevels(rubric);

    const newLevel = {
      ...createDefaultCriterionLevel(),
      sequenceNumber: index,
    };
    increaseSequenceNumbers(getRubricCriterionLevels(rubric), index);
    criterionLevels.push(newLevel);
    rubric.criterions[criterionIndex].levels = sortBy(criterionLevels, 'sequenceNumber');
    setRubricAndRestartForm(rubric, form);
  };

  const removeCriterionLevel = () => {
    const rubric = form.getState().values;

    getRubricCriterionLevels(rubric).splice(deleteCriterionLevelIndex, 1);
    getRubricCriterionLevels(rubric).forEach((level, index) => (level.sequenceNumber = index));
    closeDeleteCriterionLevelDialog();
    setRubricAndRestartForm(rubric, form);
  };

  const AddCriterionLevelButton: FC<{ newCriterionLevelIndex: number }> = ({ newCriterionLevelIndex }) => (
    <div className="add-criterion-level d-flex align-items-center">
      <Button
        color=""
        type="button"
        className="p-0 border-0"
        onClick={() => createNewCriterionLevel(newCriterionLevelIndex)}
        aria-label={translate('schoolabyApp.assignment.rubric.criterion.level.addNew')}
      >
        <Icon name={'plus'} fill={SBY_PRIMARY} />
      </Button>
    </div>
  );

  return (
    <ContentCard
      className={classNames({
        'criterion-update': true,
        delete: indicateDelete,
      })}
    >
      <div className="criterion-header d-flex">
        <Field
          id={`${formFieldName}.title`}
          name={`${formFieldName}.title`}
          type="text"
          label={translate('schoolabyApp.assignment.rubric.criterion.title')}
          placeholder={translate('schoolabyApp.assignment.rubric.criterion.titlePlaceholder')}
          containerClassName="criterion-title"
          component={Input}
        />
        <Field
          id={`${formFieldName}.description`}
          name={`${formFieldName}.description`}
          type="text"
          label={translate('schoolabyApp.assignment.rubric.criterion.description')}
          placeholder={translate('schoolabyApp.assignment.rubric.criterion.descriptionPlaceholder')}
          containerClassName="w-100 criterion-description"
          component={Input}
        />
        <div className="criterion-remove text-right">
          {!deleteDisabled && (
            <Button color={''} className="p-0" type="button" onClick={onDeleteClick}>
              <Icon name="trashNew" /> {translate('schoolabyApp.assignment.rubric.criterion.delete')}
            </Button>
          )}
        </div>
      </div>
      <div className="criterion-levels py-1 d-flex">
        <div className="d-flex">
          <AddCriterionLevelButton newCriterionLevelIndex={0} />
        </div>
        <FieldArray name={`${formFieldName}.levels`} subscription={{ pristine: true }}>
          {({ fields: criterionLevelFields }) =>
            useMemo(
              () =>
                criterionLevelFields.map((criterionLevelName, index) => (
                  <div key={criterionLevelName} className="d-flex">
                    <CriterionLevelUpdate
                      formFieldName={criterionLevelName}
                      onDeleteClick={() => setDeleteCriterionLevelIndex(index)}
                      indicateDelete={deleteCriterionLevelIndex === index}
                      deleteDisabled={criterionLevelFields.length === 1}
                    />
                    <AddCriterionLevelButton newCriterionLevelIndex={index + 1} />
                  </div>
                )),
              [criterionLevelFields]
            )
          }
        </FieldArray>
      </div>
      {deleteCriterionLevelIndex !== undefined && (
        <DeleteModal
          isOpen
          toggle={closeDeleteCriterionLevelDialog}
          deleteAction={removeCriterionLevel}
          customDescription={translate('schoolabyApp.assignment.rubric.criterion.level.deleteQuestion')}
        />
      )}
    </ContentCard>
  );
};

export default CriterionUpdate;
