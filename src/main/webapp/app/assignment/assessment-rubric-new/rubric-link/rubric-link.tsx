import React, { FC } from 'react';
import './rubric-link.scss';
import { translate } from 'react-jhipster';
import Icon from 'app/shared/icons';
import { IRubric } from 'app/shared/model/rubric/rubric.model';
import { Button } from 'reactstrap';
import { useForm } from 'react-final-form';

interface RubricLinkProps {
  setGoToRubric: (goToRubric: boolean) => void;
  className?: string;
  rubric?: IRubric;
}

const RubricLink: FC<RubricLinkProps> = ({ setGoToRubric, className, rubric }) => {
  const form = useForm();

  const LinkText = () => {
    return (
      <p className={`m-0${rubric ? ' with-rubric text-underline' : ''}`}>
        {rubric
          ? `${translate('schoolabyApp.assignment.rubric.titleWithName', { rubricTitle: rubric.title })} (${rubric.maxPoints} p)`
          : `+ ${translate('schoolabyApp.assignment.addRubric')}`}
      </p>
    );
  };

  return (
    <div className={`d-flex align-items-center rubric-link ${className ? className : ''}`}>
      <Button color="" onClick={() => setGoToRubric(true)} className="d-flex align-items-center rubric-button p-0 text-left" type="submit">
        <div className="mr-1">
          <Icon name="penAndRuler" />
        </div>
        <LinkText />
      </Button>
      {!!rubric && (
        <Button color="" onClick={form.mutators.removeRubric} className="p-0 delete-button ml-2" type="button">
          <Icon name="trashNew" />
        </Button>
      )}
    </div>
  );
};

export default RubricLink;
