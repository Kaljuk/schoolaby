import React, { FC } from 'react';
import { IGroup } from 'app/shared/model/group.model';
import { Field } from 'react-final-form';
import { Input } from 'app/shared/form';
import { Droppable } from 'react-beautiful-dnd';
import RemoveButton from 'app/shared/layout/remove-button/remove-button';
import StudentDraggable from './student-draggable';
import { translate } from 'react-jhipster';

interface IProps {
  index: number;
  setGroups: (groups: IGroup[]) => void;
  groups: IGroup[];
  disabled: boolean;
  newStudentIds: number[];
}

const GroupBox: FC<IProps> = ({ index, setGroups, groups, disabled, newStudentIds }) => {
  const { name, students } = groups[index];

  const changeName = value => {
    const shallowGroupsCopy = [...groups];
    shallowGroupsCopy[index].name = value;
    setGroups(shallowGroupsCopy);
  };

  const isGroupNameRepeated = () => {
    return name?.length && groups.filter(g => g.name === name).length > 1;
  };

  function removeGroup() {
    const updatedGroups = [...groups];
    updatedGroups.splice(index, 1);
    setGroups(updatedGroups);
  }

  return (
    <Droppable droppableId={index.toString()} isDropDisabled={disabled}>
      {provided => (
        <div className={'col-9 col-sm-3 px-0 group-box mb-4'} data-testid={'group-box'}>
          <RemoveButton
            onClick={() => removeGroup()}
            aria-label={`remove-group-${index}`}
            iconWidth={'10px'}
            iconHeight={'10px'}
            className={'position-absolute remove-btn'}
            disabled={disabled}
          />
          <div className={`group-box-inner py-2 pl-2 ${disabled && 'disabled'}`}>
            <Field
              name={'name' + index}
              aria-label={'group-' + index}
              className={`mt-2 ${isGroupNameRepeated() ? 'warning' : ''}`}
              errMessage={!name?.trim().length && 'Group name can not be empty'}
              render={props => <Input {...props} value={name} onChange={event => changeName(event.target.value)} />}
            />
            <div ref={provided.innerRef} className={'students-container mt-3'}>
              {students.map((student, i) => {
                return (
                  <StudentDraggable
                    key={student.id}
                    student={student}
                    index={i}
                    isDragDisabled={disabled && !newStudentIds.includes(student.id)}
                  />
                );
              })}
              {provided.placeholder}
            </div>
            {disabled && (
              <div className={'h-100 warning-container'}>
                <div className={'warning h-100 d-flex align-items-center p-2'}>
                  {translate('schoolabyApp.assignment.member.groupHasSubmitted')}
                </div>
              </div>
            )}
          </div>
        </div>
      )}
    </Droppable>
  );
};

export default GroupBox;
