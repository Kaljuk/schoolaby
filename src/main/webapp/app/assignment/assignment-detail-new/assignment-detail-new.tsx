import React, { useCallback, useEffect, useState } from 'react';
import HeadingNew, { SUB_HEADING } from 'app/shared/layout/heading/heading-new';
import { useGetAssignment, UseGetAssignmentParams } from 'app/shared/services/assignment-api';
import { useGetJourney } from 'app/shared/services/journey-api';
import Icon, { getHeadingIcon } from 'app/shared/icons';
import { IAssignment } from 'app/shared/model/assignment.model';
import { IRootState } from 'app/shared/reducers';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { APP_LOCAL_DATE_TIME_FORMAT, AUTHORITIES } from 'app/config/constants';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { TextFormat, translate } from 'react-jhipster';
import { ASSIGNMENT } from 'app/shared/util/entity-utils';
import ContentContainer from 'app/shared/layout/content-container/content-container';
import { ANALYTICS_TAB, ASSIGNMENT_TAB, BACKPACK_TAB, SUBMISSIONS_TAB, TabType } from 'app/shared/layout/tabs/tabs';
import { HomeBreadcrumbItem } from 'app/shared/layout/heading/breadcrumbs/home-breadcrumb-item';
import MyJourneysBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/my-journeys-breadcrumb-item';
import JourneyTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/journey-title-breadcrumb-item';
import { useGetMilestone } from 'app/shared/services/milestone-api';
import MilestoneTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/milestone-title-breadcrumb-item';
import AssignmentTitleBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/assignment-title-breadcrumb-item';
import { IJourney } from 'app/shared/model/journey.model';
import { IMilestone } from 'app/shared/model/milestone.model';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import BackpackBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/backpack-breadcrumb-item';
import { AnalyticsAssignmentBreadcrumbItem } from 'app/shared/layout/heading/breadcrumbs/analytics-breadcrumb-item';
import AssignmentStudentsBreadcrumbItem from 'app/shared/layout/heading/breadcrumbs/assignment-students-breadcrumb-item';
import AnalyticsContent from 'app/assignment/assignment-detail/analytics/analytics-content';
import { SubmissionGradingProvider } from 'app/shared/contexts/submission-grading-context';
import { useSelectedGroupState, useSelectedUserState } from 'app/shared/contexts/entity-detail-context';
import AssignmentInfo from 'app/assignment/assignment-detail-new/assignment-info';

import './assignment-detail-new.scss';
import SubmissionsContent from 'app/assignment/assignment-detail/students/submissions-content';
import BackpackContentNew from 'app/assignment/assignment-detail-new/backpack-content-new';
import { useGetRubric } from 'app/shared/services/rubric-api';
import { Button } from 'reactstrap';
import { useSelectedRubricState } from 'app/shared/contexts/entity-update-context';
import RubricOverview from 'app/assignment/assessment-rubric-new/rubric-overview/rubric-overview';
import RubricViewModes from 'app/assignment/assessment-rubric-new/rubric-overview/rubric-view-modes';
import StudentAssignmentView from 'app/assignment/assignment-detail/students/student-assignment-view/student-assignment-view';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { createSubmission, useGetSubmissions } from 'app/shared/services/submission-api';
import { IChat } from 'app/shared/model/chat.model';
import { useGeneratedChatState } from 'app/shared/contexts/chat-context';

export const AssignmentDetailContext = React.createContext<{
  assignment: IAssignment;
  journey: IJourney;
  milestone: IMilestone;
  isAllowedToModify: boolean;
  activeTab: TabType;
}>(null);

const AssignmentDetailNew = ({ match, account, isTeacherOrAdmin }) => {
  const history = useHistory();
  const { setSelectedUser: setSelectedStudent } = useSelectedUserState();
  const { setSelectedGroup } = useSelectedGroupState();
  const [assignmentId, setAssignmentId] = useState<number>(null);
  const useGetAssignmentParams: UseGetAssignmentParams = {
    withPreviousAndNextIds: true,
  };
  const { data: assignment } = useGetAssignment(
    assignmentId || +match?.params?.id,
    !!assignmentId || !!match?.params?.id,
    useGetAssignmentParams
  );
  const { data: milestone } = useGetMilestone(assignment?.milestoneId, !!assignment?.id);
  const { data: journey, isFetching: isJourneyFetching } = useGetJourney(assignment?.journeyId, !!assignment?.id);
  const currentUrl = window.location.protocol + '//' + window.location.hostname + '/assignment/' + assignment?.id;
  const isAllowedToModify = isTeacherOrAdmin && journey?.teachers?.some(teacher => teacher.user.id === account.id);
  const [activeTab, setActiveTab] = useState<TabType>(ASSIGNMENT_TAB);
  const [openRubricOverview, setOpenRubricOverview] = useState<boolean>(false);
  const { data: rubric } = useGetRubric(assignment?.id);
  const { selectedRubric, setSelectedRubric } = useSelectedRubricState();

  useEffect(() => {
    return () => {
      setSelectedStudent(null);
      setSelectedGroup(null);
    };
  }, []);

  useEffect(() => {
    const path = history.location.pathname.split('/');

    // TODO: SCHOOL-970 Change path indexes 4 -> 3
    if (path[4] === 'submissions') {
      setActiveTab(SUBMISSIONS_TAB);
    } else if (path[4] === 'backpack') {
      setActiveTab(BACKPACK_TAB);
    } else if (path[4] === 'analytics') {
      setActiveTab(ANALYTICS_TAB);
    } else {
      setActiveTab(ASSIGNMENT_TAB);
    }
  }, [history.location.pathname]);

  const getTabContent = useCallback(() => {
    if (activeTab === ASSIGNMENT_TAB) {
      return isAllowedToModify ? <AssignmentInfo setAssignmentId={setAssignmentId} /> : <StudentAssignmentView />;
    } else if (activeTab === SUBMISSIONS_TAB && isAllowedToModify) {
      return (
        <SubmissionGradingProvider>
          <SubmissionsContent />
        </SubmissionGradingProvider>
      );
    } else if (activeTab === BACKPACK_TAB) {
      return <BackpackContentNew />;
    } else if (activeTab === ANALYTICS_TAB) {
      return (
        <div className={'assignment-detail-view new'}>
          <AnalyticsContent />
        </div>
      );
    }
  }, [activeTab, isAllowedToModify, milestone]);

  const getBreadcrumbItem = useCallback(() => {
    if (activeTab === SUBMISSIONS_TAB) {
      return <AssignmentStudentsBreadcrumbItem assignmentId={assignment?.id} />;
    } else if (activeTab === BACKPACK_TAB) {
      return <BackpackBreadcrumbItem assignmentId={assignment?.id} journeyId={journey?.id} />;
    } else if (activeTab === ANALYTICS_TAB) {
      return <AnalyticsAssignmentBreadcrumbItem assignmentId={assignment?.id} journeyId={journey?.id} />;
    }
  }, [activeTab, journey, assignment]);

  const NavigateToChatButton = () => {
    const { data: submissions } = useGetSubmissions({ assignmentId: assignment?.id }, !!assignment?.id);
    const { setGeneratedChat } = useGeneratedChatState();
    let submission = submissions?.[0];

    const generateCustomChat = (sub: ISubmission) => {
      const chat: IChat = {
        journeyId: journey.id,
        submissionId: sub.id,
        title: assignment.title,
        people: [...sub.authors, ...journey.teachers.map(teacher => teacher.user)],
      };
      setGeneratedChat(chat);
    };

    const saveEmptySubmission = async () => {
      const values = {
        value: '',
        submittedForGradingDate: null,
        uploadedFiles: [],
      };
      const entity = {
        ...values,
        assignmentId: assignment.id,
        authors: [account],
        groupId: assignment.groups?.find(group => group.students.some(student => student.id === account.id)).id,
      };
      return await createSubmission(entity);
    };

    const navigateToChat = async () => {
      if (!submission) {
        submission = await saveEmptySubmission();
      }
      // TODO: Check if chat exists
      generateCustomChat(submission);
      history.push(`/chats?journeyId=${journey.id}&submissionId=${submission.id}`);
    };

    return (
      <span className={'d-flex align-items-center cursor-pointer'} onClick={navigateToChat}>
        <Icon name={'chatBubble'} />
        <span className={'text-secondary mr-1'}>{translate('schoolabyApp.submission.askMore')}</span>
      </span>
    );
  };

  useEffect(() => {
    !selectedRubric && setOpenRubricOverview(false);
  }, [selectedRubric]);

  const AdditionalBreadcrumbRowItems = () => (
    <div className={'additional-breadcrumbs-row-items d-flex'}>
      {/* TODO: Show only if user is student in current journey */}
      {!isAllowedToModify && <NavigateToChatButton />}
      {isAllowedToModify && (
        <span className={'d-flex align-items-center'}>
          <a
            className={'d-flex align-items-center'}
            href={`https://stuudium.link/jaga?url=${currentUrl}&title=${assignment?.title}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <img src="content/images/stuudium_logo.png" alt="Stuudium_icon" width="16" height="16" />
            <span>{translate('schoolabyApp.assignment.shareToStuudium')}</span>
          </a>
        </span>
      )}
      {!!rubric && (
        <>
          <Button
            color=""
            className="no-outline-on-focus p-0"
            type="button"
            onClick={() => {
              setSelectedRubric(rubric);
              setOpenRubricOverview(true);
            }}
          >
            <Icon name={'penAndRuler'} />
            <span className={'ml-2'}>{translate('schoolabyApp.assignment.rubric.viewTitle')}</span>
          </Button>
          {!!selectedRubric && openRubricOverview && <RubricOverview viewMode={RubricViewModes.VIEW} />}
        </>
      )}
      <span className={'d-flex align-items-center'}>
        <Icon name={'stopwatch'} className={assignment?.state.toLowerCase() + ' stopwatch'} />
        <span className={'text-secondary mr-1'}>
          {translate('global.deadline')}{' '}
          <strong>
            <TextFormat type={'date'} value={assignment?.deadline} format={APP_LOCAL_DATE_TIME_FORMAT} />
          </strong>
        </span>
      </span>
      {isAllowedToModify && (
        <span className={'d-flex align-items-center'}>
          <input type={'checkbox'} checked={!assignment?.published} disabled />
          <span className={'text-secondary mr-1'}>{translate('entity.publishedState.hidden')}</span>
        </span>
      )}
    </div>
  );

  return (
    <AssignmentContext.Provider value={{ assignment, journey, milestone, isAllowedToModify, rubric }}>
      <ContentContainer
        spinner={isJourneyFetching}
        className={`assignment-detail-new-container d-flex${
          activeTab === ASSIGNMENT_TAB || activeTab === SUBMISSIONS_TAB ? ' overflow-hidden' : ''
        }`}
      >
        <HeadingNew
          title={journey?.title}
          icon={getHeadingIcon(journey?.subject?.label)}
          headingType={SUB_HEADING}
          tabEntityType={ASSIGNMENT}
          newAssignmentViewDesign
          isAllowedToModify={isAllowedToModify}
          additionalBreadcrumbRowItems={<AdditionalBreadcrumbRowItems />}
        />
        <>
          <HomeBreadcrumbItem />
          <MyJourneysBreadcrumbItem />
          {!!journey && <JourneyTitleBreadcrumbItem journeyId={journey.id} journeyTitle={journey.title} />}
          {!!milestone && (
            <MilestoneTitleBreadcrumbItem journeyId={journey?.id} milestoneId={milestone.id} milestoneTitle={milestone.title} />
          )}
          {!!assignment && (
            <AssignmentTitleBreadcrumbItem journeyId={journey?.id} assignmentId={assignment.id} assignmentTitle={assignment.title} />
          )}
          {getBreadcrumbItem()}
        </>
        {getTabContent()}
      </ContentContainer>
    </AssignmentContext.Provider>
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  account: authentication.account,
  isTeacherOrAdmin: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.TEACHER, AUTHORITIES.ADMIN]),
});

export default connect(mapStateToProps)(AssignmentDetailNew);
