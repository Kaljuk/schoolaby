import React, { FC, useContext, useEffect, useState } from 'react';
import { APP_LOCAL_DATE_TIME_FORMAT } from 'app/config/constants';
import { mergeMaterials } from 'app/shared/util/lti-utils';
import { useGetAssignment, UseGetAssignmentParams, usePatchAssignment } from 'app/shared/services/assignment-api';
import { useGetGradingScheme } from 'app/shared/services/grading-scheme-api';
import { useGetLtiResources } from 'app/shared/services/lti-api';
import { useChosenMaterialState, useMaterialState } from 'app/shared/contexts/material-context';
import { TextFormat, translate } from 'react-jhipster';
import Icon from 'app/shared/icons';
import { ICON_GREY_2 } from 'app/shared/util/color-utils';
import { Col, Row, Container } from 'reactstrap';
import MaterialCardModal from 'app/shared/layout/material-card/material-card-modal';
import { useHistory } from 'react-router-dom';
import ChooseMaterials from 'app/shared/layout/choose-materials/choose-materials';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import EditableText from 'app/shared/form/editable-text';
import { createYupValidator } from 'app/shared/util/form-utils';
import { useAssignmentDeadlinePatchSchema, useAssignmentTitlePatchSchema } from 'app/shared/schema/assignment';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import ReactMarkdown from 'react-markdown';
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { DATEPICKER_DATE_TIME_FORMAT } from 'app/shared/form/components/date-picker';
import { IAssignment } from 'app/shared/model/assignment.model';
import { HistoryStateProps } from 'app/marketplace/marketplace';

import './assignment-info.scss';
import { LTI_MATERIAL } from 'app/shared/model/material.model';
import { LTI_MODAL_ACTION_UPDATE, LtiCreateModal } from 'app/shared/layout/lti/lti-create-modal';
import { createPortal } from 'react-dom';
import LtiModal from 'app/shared/layout/lti/lti-modal';

interface AssignmentInfoProps extends StateProps {
  setAssignmentId?: (id: number) => void;
}

const AssignmentInfo: FC<AssignmentInfoProps> = ({ setAssignmentId, locale }) => {
  const history = useHistory();
  const [deadlineEdit, setDeadlineEdit] = useState<boolean>(false);
  const { assignment: contextAssignment, journey, isAllowedToModify } = useContext(AssignmentContext);
  const useGetAssignmentParams: UseGetAssignmentParams = {
    withPreviousAndNextIds: true,
  };
  const { data: assignment } = useGetAssignment(contextAssignment?.id, !!contextAssignment?.id, useGetAssignmentParams);
  const { data: previousAssignment } = useGetAssignment(
    assignment?.previousAssignmentId,
    !!assignment?.previousAssignmentId,
    useGetAssignmentParams
  );
  const { data: nextAssignment } = useGetAssignment(assignment?.nextAssignmentId, !!assignment?.nextAssignmentId, useGetAssignmentParams);
  const { data: gradingScheme } = useGetGradingScheme(assignment?.gradingSchemeId, !!assignment?.gradingSchemeId);
  const { data: ltiResources } = useGetLtiResources({ assignmentId: assignment?.id }, !!assignment?.id);
  const { setMaterials, materials } = useMaterialState();
  const { chosenMaterial, setChosenMaterial } = useChosenMaterialState();
  const patchAssignment = usePatchAssignment(assignment?.id);
  const [goToMarketplace, setGoToMarketplace] = useState<boolean>(false);

  useEffect(() => {
    if (goToMarketplace) {
      const url = `/marketplace/assignment/${assignment.id}`;
      history.push({
        pathname: url,
        state: {
          entity: {
            ...assignment,
            materials: materials.map(material => material.getCopy()),
          },
          back: history.location,
          journeyId: journey?.id,
          patch: true,
        },
      });
    }
  }, [goToMarketplace]);

  const toggleModal = () => {
    setChosenMaterial(null);
  };

  const NavigationButton = ({ assignmentTitle, onClick, actionForward = false, hidden = false }) => (
    <Col
      xs={4}
      className={`navigation-button-container d-flex align-items-center${actionForward ? ' flex-row-reverse' : ''}${
        !hidden ? ' cursor-pointer' : ''
      }`}
      onClick={() => !hidden && onClick()}
    >
      <span className={`navigation-button d-flex align-items-center justify-content-center ${hidden ? ' d-none' : ''}`}>
        <Icon name={actionForward ? 'chevronRight' : 'chevronLeft'} fill={ICON_GREY_2} width={'5'} />
      </span>
      <span className={'ellipsis'}>{assignmentTitle}</span>
    </Col>
  );

  const NavigationButtons = ({ className = '' }) => (
    <Row noGutters className={`justify-content-between ${className}`}>
      <NavigationButton
        assignmentTitle={previousAssignment?.title}
        hidden={!assignment?.previousAssignmentId}
        onClick={() => {
          setAssignmentId(assignment?.previousAssignmentId);
          // TODO: SCHOOL-970 Remove newDesign
          history.replace(`/assignment/newDesign/${assignment?.previousAssignmentId}?journeyId=${journey?.id}`);
        }}
      />
      <NavigationButton
        assignmentTitle={nextAssignment?.title}
        hidden={!assignment?.nextAssignmentId}
        onClick={() => {
          setAssignmentId(assignment?.nextAssignmentId);
          // TODO: SCHOOL-970 Remove newDesign
          history.replace(`/assignment/newDesign/${assignment?.nextAssignmentId}?journeyId=${journey?.id}`);
        }}
        actionForward
      />
    </Row>
  );

  useEffect(() => {
    const historyState = history.location.state as HistoryStateProps<IAssignment>;
    if (historyState) {
      setMaterials(mergeMaterials(historyState, null));
      history.location.state = null;
    } else {
      setMaterials(mergeMaterials(assignment, ltiResources));
    }
  }, [assignment, ltiResources]);

  return (
    <Container className={'assignment-detail-new d-flex flex-column'}>
      {isAllowedToModify && <NavigationButtons className={'mt-2'} />}
      <img
        className={'cover-image w-100'}
        src={journey?.imageUrl ?? '/content/images/default_cover.svg'}
        alt={translate('schoolabyApp.journey.image')}
        onError={e => {
          // @ts-ignore
          e.target.src = '/content/images/default_cover.svg';
        }}
      />
      <div className={'d-flex flex-row assignment-info card position-relative'}>
        <div className={'assignment-title'}>
          <EditableText
            name={'title'}
            initialValue={assignment?.title}
            patchRequest={patchAssignment}
            validate={createYupValidator(useAssignmentTitlePatchSchema(locale))}
            isAllowedToModify={isAllowedToModify}
          >
            <h2 className={'mb-0'}>{assignment?.title}</h2>
          </EditableText>
        </div>
        <div className={`info d-flex flex-column${deadlineEdit ? ' deadline-edit' : ''}`}>
          <span>
            <EditableText
              name={'deadline'}
              initialValue={assignment?.deadline && convertDateTimeFromServer(assignment.deadline)}
              patchRequest={patchAssignment}
              editor={'datepicker'}
              footerClassName={'deadline-edit-footer'}
              editableFieldClassName={isAllowedToModify && 'deadline-editable-field'}
              setEditing={setDeadlineEdit}
              datePickerProps={{
                id: 'deadline-datepicker',
                onChange() {},
                label: translate('global.deadline'),
                dateTimeFormat: DATEPICKER_DATE_TIME_FORMAT,
                popperPlacement: 'top-end',
              }}
              validate={createYupValidator(useAssignmentDeadlinePatchSchema(locale))}
              isAllowedToModify={isAllowedToModify}
            >
              {translate('global.deadline')} <TextFormat type={'date'} value={assignment?.deadline} format={APP_LOCAL_DATE_TIME_FORMAT} />
            </EditableText>
          </span>
          <span>
            {translate('schoolabyApp.assignment.gradingScheme')}:{' '}
            {gradingScheme?.code && translate(`schoolabyApp.gradingScheme.code.${gradingScheme?.code}`)}
          </span>
        </div>
      </div>
      <div className={'assignment-description card'}>
        <EditableText
          name={'description'}
          initialValue={assignment?.description}
          patchRequest={patchAssignment}
          editor={'markdown'}
          isAllowedToModify={isAllowedToModify}
        >
          {assignment?.description ? (
            <ReactMarkdown
              source={assignment?.description}
              renderers={{
                code: ({ value: codeValue }) => <div>{codeValue}</div>,
              }}
            />
          ) : (
            <div className={'w-100 h-100 d-flex flex-column align-items-center justify-content-center mt-2 mb-2'}>
              <img alt={''} src={'content/images/happy_folder.svg'} />
              <p className={'empty d-flex'}>{translate('schoolabyApp.assignment.home.noDescription')}</p>
            </div>
          )}
        </EditableText>
      </div>
      <div className={'assignment-materials card'}>
        <ChooseMaterials
          isDropdown
          patchRequest={patchAssignment}
          setGoToMarketplace={setGoToMarketplace}
          isAllowedToModify={isAllowedToModify}
        />
      </div>
      {isAllowedToModify && <NavigationButtons />}
      <MaterialCardModal
        patchRequest={patchAssignment}
        removable
        isOpen={!!chosenMaterial}
        material={chosenMaterial}
        toggleModal={toggleModal}
      />
      {chosenMaterial?.type === LTI_MATERIAL &&
        (isAllowedToModify ? (
          createPortal(
            <LtiCreateModal
              showModal={!!chosenMaterial}
              title={translate('schoolabyApp.assignment.lti.edit')}
              ltiResource={chosenMaterial?.getLtiResource()}
              onClose={toggleModal}
              journeyId={assignment?.journeyId}
              action={LTI_MODAL_ACTION_UPDATE}
            />,
            document.getElementById('lti-form')
          )
        ) : (
          <LtiModal ltiResource={chosenMaterial?.ltiResource} key={chosenMaterial?.ltiResource.id} onClose={toggleModal} />
        ))}
    </Container>
  );
};

const mapStateToProps = ({ locale }: IRootState) => ({
  locale: locale.currentLocale,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(AssignmentInfo);
