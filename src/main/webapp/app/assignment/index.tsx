import React from 'react';
import { Switch } from 'react-router-dom';
import AssignmentDetail from './assignment-detail';
import AssignmentAnalytics from 'app/assignment/assignment-detail/analytics/assignment-analytics';
import Backpack from 'app/assignment/assignment-detail/backpack/backpack';
import ErrorBoundaryDetailUpdateEntityRoute from 'app/shared/error/error-boundary-detail-update-entity-route';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import AssignmentDetailNew from 'app/assignment/assignment-detail-new/assignment-detail-new';
import PageNotFound from 'app/shared/error/page-not-found';
import AssessmentRubric from 'app/assignment/assessment-rubric-new/assessment-rubric';

const Routes = ({ match, shouldShowOldDesign }) => (
  <>
    <Switch>
      <ErrorBoundaryDetailUpdateEntityRoute exact path={`${match.url}/:id`} component={AssignmentDetail} />
      <ErrorBoundaryDetailUpdateEntityRoute exact path={`${match.url}/:id/analytics`} component={AssignmentAnalytics} />
      <ErrorBoundaryDetailUpdateEntityRoute exact path={`${match.url}/:id/backpack`} component={Backpack} />
      <ErrorBoundaryDetailUpdateEntityRoute exact path={`${match.url}/:id/assessment-rubric`} component={AssessmentRubric} />
      <ErrorBoundaryDetailUpdateEntityRoute exact path={`${match.url}/new/assessment-rubric`} component={AssessmentRubric} />
      {/* TODO: SCHOOL-970 Remove newDesign part from routes */}
      <ErrorBoundaryDetailUpdateEntityRoute
        exact
        path={`${match.url}/newDesign/:id/`}
        component={shouldShowOldDesign ? PageNotFound : AssignmentDetailNew}
      />
      <ErrorBoundaryDetailUpdateEntityRoute
        exact
        path={`${match.url}/newDesign/:id/submissions`}
        component={shouldShowOldDesign ? PageNotFound : AssignmentDetailNew}
      />
      <ErrorBoundaryDetailUpdateEntityRoute
        exact
        path={`${match.url}/newDesign/:id/backpack`}
        component={shouldShowOldDesign ? PageNotFound : AssignmentDetailNew}
      />
      <ErrorBoundaryDetailUpdateEntityRoute
        exact
        path={`${match.url}/newDesign/:id/analytics`}
        component={shouldShowOldDesign ? PageNotFound : AssignmentDetailNew}
      />
    </Switch>
  </>
);

const mapStateToProps = ({ applicationProfile }: IRootState) => ({
  shouldShowOldDesign: applicationProfile.inProduction || applicationProfile.inTestcafe,
});

export default connect(mapStateToProps)(Routes);
