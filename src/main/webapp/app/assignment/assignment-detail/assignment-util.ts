import { IAssignment } from 'app/shared/model/assignment.model';
import { DateTime } from 'luxon';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { IMilestone } from 'app/shared/model/milestone.model';

export const isPastDeadline = (assignment: IAssignment): boolean => {
  return new Date(DateTime.fromISO(assignment.deadline)) < new Date(DateTime.local());
};

export const isPastDate = (date1: Date, date2: Date) => {
  return date1 < date2;
};

export const hasMaterials = (entity: IAssignment | IMilestone): boolean => {
  return !!entity?.materials?.length || (!!entity && 'ltiResources' in entity && !!entity.ltiResources?.length);
};

export const hasSubmissions = (submissions: ISubmission[]) => submissions?.length !== null && submissions?.length > 0;
