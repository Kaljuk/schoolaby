import './submission-times-bar-chart.scss';
import React, { useEffect, useState } from 'react';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import Icon from 'app/shared/icons';
import { SECONDARY } from 'app/shared/util/color-utils';
import { Col, Row, Tooltip } from 'reactstrap';
import { translate } from 'react-jhipster';
import { IJourney } from 'app/shared/model/journey.model';
import { IAssignment } from 'app/shared/model/assignment.model';
import { convertDateTimeFromServer, days } from 'app/shared/util/date-utils';
import { DateTime, Interval } from 'luxon';
import { DataPoint } from 'app/assignment/assignment-detail/analytics/assignment-analytics';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import maxBy from 'lodash/maxBy';
import minBy from 'lodash/minBy';
import { NoData } from 'app/shared/chart/no-data';
import DateBarChart from 'app/shared/chart/date-bar-chart';
import { hasSubmissions } from 'app/assignment/assignment-detail/assignment-util';
import { IJourneyStudent } from 'app/shared/model/journey-student.model';

type IProps = {
  journey: IJourney;
  submissions: ISubmission[];
  assignment?: IAssignment;
  students: IJourneyStudent[];
};

const SubmissionTimesBarChart = ({ journey, submissions, assignment, students }: IProps) => {
  const [loading, setLoading] = useState<boolean>(true);
  const [submissionTimes, setSubmissionTimes] = useState<DataPoint[]>();
  const [deadline, setDeadline] = useState<DateTime>();

  const getLatestSubmissionDate = (): DateTime => {
    const latestSubmission = maxBy(submissions, (submission: ISubmission) =>
      DateTime.fromISO(submission.submittedForGradingDate).valueOf()
    );
    return DateTime.fromISO(latestSubmission.submittedForGradingDate);
  };

  const getOldestSubmissionDate = (): DateTime => {
    const oldestSubmission = minBy(submissions, (submission: ISubmission) =>
      DateTime.fromISO(submission.submittedForGradingDate).valueOf()
    );
    return DateTime.fromISO(oldestSubmission.submittedForGradingDate);
  };

  const getSubmissionsOverDeadline = () => {
    const result = submissions.filter(value => value.submittedForGradingDate > deadline);
    return result?.length ? result.length : 0;
  };

  const getSubmissionsBeforeDeadline = () => {
    const result = submissions.filter(value => value.submittedForGradingDate <= deadline);
    return result?.length ? result.length : 0;
  };

  const getMissingSubmissions = () => {
    const result = students?.length - submissions?.length;
    return result >= 0 ? result : 0;
  };

  const getSubmissionCountForDay = (day: DateTime) => {
    const count = submissions.filter(
      value => DateTime.fromISO(value.submittedForGradingDate).toFormat('dd.MM.yyyy') === day.toFormat('dd.MM.yyyy')
    )?.length;
    return count ? count : 0;
  };

  const getSubmissionTimes = () => {
    const submissionInterval = Interval.fromDateTimes(
      DateTime.fromISO(getOldestSubmissionDate()),
      DateTime.fromISO(getLatestSubmissionDate())
    );
    return days(submissionInterval).map(dayDateTime => ({
      key: dayDateTime.toFormat('dd.MM.yyyy'),
      value: getSubmissionCountForDay(dayDateTime),
    }));
  };

  useEffect(() => {
    if (journey && submissions && assignment) {
      if (hasSubmissions(submissions)) {
        setDeadline(convertDateTimeFromServer(assignment?.deadline));
        setSubmissionTimes(getSubmissionTimes());
      }
      setLoading(false);
    }
  }, [journey, submissions, assignment]);

  const BarChartFooter = () => (
    <>
      <hr className={'mt-0'} />
      <Row className={'px-3 px-sm-5 pb-3 pb-sm-4 pt-0 pt-sm-2'}>
        <Col xs={3} className={'d-flex align-items-center'}>
          <Icon width={'23px'} height={'23px'} name={'graduate'} fill={SECONDARY} stroke={'transparent'} />
          <span className={'text-muted ml-1'} aria-label={translate('schoolabyApp.assignment.analytics.barChart.studentCount')}>
            {students?.length}
          </span>
        </Col>
        <Col xs={9} className={'d-flex justify-content-end'}>
          <Legend value={getSubmissionsBeforeDeadline()} color={'green'} tooltip={translate('schoolabyApp.submission.onTime')} />
          <Legend value={getSubmissionsOverDeadline()} color={'red'} tooltip={translate('schoolabyApp.submission.overdue')} />
          <Legend value={getMissingSubmissions()} color={'grey'} tooltip={translate('schoolabyApp.submission.notSubmitted')} />
        </Col>
      </Row>
    </>
  );

  const BarChartHeader = () => (
    <Row className={'p-3'}>
      <Col>
        <h2 className={'d-flex justify-content-center text-muted'}>
          {translate('schoolabyApp.assignment.analytics.submissionTimes.title')}
        </h2>
      </Col>
    </Row>
  );

  const Legend = ({ value, color, tooltip }) => {
    const [tooltipOpen, setTooltipOpen] = useState(false);
    const toggle = () => setTooltipOpen(!tooltipOpen);

    return (
      <span aria-label={tooltip} id={`legend-container-${color}`} className={`legend bg-${color} rounded py-2 px-3 ml-3`}>
        <span>{value}</span>
        <Tooltip placement="top" isOpen={tooltipOpen} target={`legend-container-${color}`} toggle={toggle}>
          {tooltip}
        </Tooltip>
      </span>
    );
  };

  return loading ? (
    <Spinner />
  ) : (
    <div className={'submission-times d3-chart base-card my-5 rounded'}>
      <BarChartHeader />
      {hasSubmissions(submissions) && <DateBarChart deadline={deadline} data={submissionTimes} barWidth={50} barPadding={50} />}
      <NoData show={!hasSubmissions(submissions)} />
      <BarChartFooter />
    </div>
  );
};

export default SubmissionTimesBarChart;
