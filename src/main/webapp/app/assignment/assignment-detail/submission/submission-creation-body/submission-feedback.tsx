import React from 'react';
import { Col, Row } from 'reactstrap';
import { TextFormat, translate } from 'react-jhipster';
import { APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import ReactMarkdown from 'react-markdown';
import Icon from 'app/shared/icons';
import { SubmissionGradeContainer } from 'app/assignment/assignment-detail/submission/shared/submission-grade-container';
import UploadedFilesList from 'app/shared/form/uploaded-files-list';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';

interface IProps {
  submission: ISubmission;
  feedback: ISubmissionFeedback;
  submissionFeedback: string;
}

export const SubmissionFeedback = ({ submission, feedback, submissionFeedback }: IProps) => {
  return (
    <>
      <Row>
        <Col md="9">
          <h3 className={'mb-3'}>{translate('schoolabyApp.submission.teachersFeedback')}</h3>
          <div className={'mb-3'}>
            <span className={'d-inline-block'}>
              <span className={'mr-2 text-secondary'}>{translate('schoolabyApp.submission.feedbackDate')}</span>
              <Icon name={'calendar'} className={'mb-1 mr-2'} />
              <span className={'text-dark'}>
                <TextFormat type="date" value={feedback.feedbackDate} format={APP_LOCAL_DATE_FORMAT} />
              </span>
            </span>
          </div>
        </Col>
        {submission.resubmittable && (
          <Col md="3" className={'align-self-center'}>
            <SubmissionGradeContainer feedback={feedback} />
          </Col>
        )}
      </Row>
      <Row>
        <Col className={'pt-3 pb-2'} xs={12}>
          <span className={'assignment-description text-break preserve-space'}>
            {submissionFeedback || feedback?.value ? (
              <ReactMarkdown
                source={feedback?.value || submissionFeedback}
                renderers={{
                  code: ({ value }) => <span>{value}</span>,
                }}
              />
            ) : null}
          </span>
        </Col>
        <Col>
          <UploadedFilesList files={feedback?.uploadedFiles} />
        </Col>
      </Row>
    </>
  );
};
