import React, { useContext, useEffect, useState } from 'react';
import { Button, Col, Row } from 'reactstrap';
import { connect } from 'react-redux';
import { TextFormat, translate } from 'react-jhipster';
import { MarkdownEditor } from 'app/assignment/assignment-detail/submission/shared/markdown-editor';
import { ILtiResource } from 'app/shared/model/lti-resource.model';
import { useAssignmentState, useSubmissionsState } from 'app/shared/contexts/entity-detail-context';
import { IRootState } from 'app/shared/reducers';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { APP_LOCAL_DATE_FORMAT, AUTHORITIES } from 'app/config/constants';
import { createEntity } from 'app/submission/submission.reducer';
import { getLtiResources, getLtiScores } from 'app/shared/services/lti-api';
import { hasMaterials, isPastDate, isPastDeadline } from 'app/assignment/assignment-detail/assignment-util';
import { useHistory } from 'react-router-dom';
import { convertDateTimeToServer } from 'app/shared/util/date-utils';
import { DateTime } from 'luxon';
import LtiResults from 'app/shared/layout/lti/lti-results';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import { createSubmission, patchSubmission } from 'app/shared/services/submission-api';
import AssignmentInfo from 'app/assignment/assignment-detail/assignment-info';
import ReactMarkdown from 'react-markdown';
import Icon from 'app/shared/icons';
import { SubmissionGradeContainer } from 'app/assignment/assignment-detail/submission/shared/submission-grade-container';
import { IChat } from 'app/shared/model/chat.model';
import { IJourney } from 'app/shared/model/journey.model';
import { useGeneratedChatState } from 'app/shared/contexts/chat-context';
import { IMessage } from 'app/shared/model/message.model';
import { findAllMessages } from 'app/shared/services/message-api';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import UploadedFilesList from 'app/shared/form/uploaded-files-list';
import FileUpload from 'app/shared/form/file-upload';
import { useQuery } from 'react-query';
import { Spinner } from 'app/shared/layout/spinner/spinner';
import { ASSIGNMENT_LTI_RESOURCES_KEY } from 'app/config/reactQueryKeyConstants';
import SuccessModal from 'app/shared/layout/success-modal/success-modal';
import { SubmissionFeedback } from './submission-feedback';
import AssignmentMaterials from 'app/assignment/assignment-detail/assignment-materials';
import { ILtiScore } from 'app/shared/model/lti-score.model';
import { IGroup } from 'app/shared/model/group.model';
import { getGroupByStudentId } from 'app/shared/util/group-utils';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';
import { useGetRubric } from 'app/shared/services/rubric-api';
import { FileUploadProvider } from 'app/shared/form/file-upload-context';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import RubricOverview from 'app/assignment/assessment-rubric-new/rubric-overview/rubric-overview';
import RubricViewModes from 'app/assignment/assessment-rubric-new/rubric-overview/rubric-view-modes';
import { useSelectedRubricState } from 'app/shared/contexts/entity-update-context';

interface ICreateSubmissionBody extends StateProps, DispatchProps {
  journey?: IJourney;
}

const SubmissionCreationBody = ({ isAdmin, account, journey: passedJourney }: ICreateSubmissionBody) => {
  const [ltiScores, setLtiScores] = useState<ILtiScore[]>();
  const [valueFromEditor, setValueFromEditor] = useState<string>();
  const [submitted, setSubmitted] = useState<boolean>();
  const [files, setFiles] = useState<IUploadedFile[]>([]);
  const journey = passedJourney ? passedJourney : useContext(AssignmentContext)?.journey;
  const assignment = useAssignmentState().entity ? useAssignmentState().entity : useContext(AssignmentContext)?.assignment;
  const history = useHistory();
  const { submissions } = useSubmissionsState();
  const userGroup: IGroup = getGroupByStudentId(account.id, assignment?.groups);
  const [latestSubmission, setLatestSubmission] = useState<ISubmission>();
  const { setGeneratedChat } = useGeneratedChatState();
  const [submissionFeedback, setSubmissionFeedback] = useState<IMessage>();
  const [modal, setModal] = useState(false);
  const toggleModal = () => setModal(!modal);
  const [isTeacher, setIsTeacher] = useState<boolean>(false);
  const { data: rubric } = useGetRubric(assignment?.id);
  const { selectedRubric, setSelectedRubric } = useSelectedRubricState();
  const toggleShowingRubric = () => setSelectedRubric(rubric);
  const [latestFeedback, setLatestFeedback] = useState<ISubmissionFeedback>(undefined);

  const { data: ltiResources } = useQuery<ILtiResource[]>(
    [ASSIGNMENT_LTI_RESOURCES_KEY, +assignment?.id],
    () => getLtiResources({ assignmentId: assignment.id }),
    { enabled: !!assignment?.id }
  );

  const closeSuccessModal = () => {
    toggleModal();
    journey?.id && history.push(`/journey/${journey.id}`);
  };

  useEffect(() => {
    submissions?.length && setLatestSubmission(submissions[0]);
  }, [submissions]);

  useEffect(() => {
    setIsTeacher(isAdmin || journey?.teachers.some(teacher => teacher.user.id === account.id));
  }, [journey, account]);

  useEffect(() => {
    latestSubmission?.id &&
      findAllMessages({
        submissionId: latestSubmission.id,
        feedback: true,
      }).then(res => {
        setSubmissionFeedback(res?.data[0]);
      });

    latestSubmission && setFiles(latestSubmission.uploadedFiles || []);
  }, [latestSubmission]);

  const updateLtiScores = () => {
    if (account && ltiResources) {
      getLtiScores({
        assignmentId: assignment.id,
        userId: account.id,
      }).then(result => {
        setLtiScores(result.data);
      });
    }
  };

  useEffect(() => {
    updateLtiScores();
  }, [account, ltiResources]);

  const saveSubmission = () => {
    setSubmitted(true);

    const values: ISubmission = {
      value: valueFromEditor ? valueFromEditor : '',
      submittedForGradingDate: convertDateTimeToServer(DateTime.local().toString()),
      uploadedFiles: files,
      groupId: userGroup?.id,
    };

    if (latestSubmission) {
      const entity = {
        ...values,
      };
      patchSubmission(latestSubmission.id, {
        ...entity,
        className: '.SubmissionResubmitPatchDTO',
      }).then(() => toggleModal());
    } else {
      createSubmission({
        ...values,
        assignmentId: assignment.id,
        authors: [account],
      }).then(() => toggleModal());
    }
  };

  const hasGrade = (): boolean => {
    return (
      !!latestSubmission?.submissionFeedbacks?.find(feedback => feedback.studentId === account.id)?.grade ||
      !!latestSubmission?.submissionFeedbacks?.find(feedback => +feedback.groupId === latestSubmission.groupId)?.grade
    );
  };

  const getFeedbackDate = (): string => {
    const submissionFeedbacks = latestSubmission.submissionFeedbacks;
    const studentFeedback = submissionFeedbacks?.find(feedback => feedback.studentId === account.id);
    const groupFeedback = submissionFeedbacks?.find(feedback => !!feedback.groupId);

    return studentFeedback?.feedbackDate || groupFeedback?.feedbackDate;
  };

  const isSubmitDisabled = () => {
    if (latestSubmission?.resubmittable || (assignment?.flexibleDeadline && !!latestSubmission && !getFeedbackDate())) {
      return false;
    }

    const isOverdue: boolean = isPastDeadline(assignment) && !assignment.flexibleDeadline;

    return hasGrade() || isOverdue;
  };

  const onFileUpload = (file: IUploadedFile) => {
    setFiles(previousState => [...previousState, file]);
  };

  const onFileDelete = (file: IUploadedFile) => {
    setFiles(previousState => previousState.filter(prevFile => prevFile.id !== file.id));
  };

  const getFeedbackByCurrentUser = (): ISubmissionFeedback => {
    return latestSubmission?.submissionFeedbacks?.find(feedback => +feedback.studentId === account?.id);
  };

  const getFeedbackByCurrentUsersGroup = (): ISubmissionFeedback => {
    return latestSubmission?.submissionFeedbacks?.find(feedback => +feedback.groupId === latestSubmission.groupId);
  };

  const getFeedback = (): ISubmissionFeedback => {
    if (assignment?.groups?.length) {
      return getFeedbackByCurrentUser() || getFeedbackByCurrentUsersGroup();
    } else {
      return getFeedbackByCurrentUser();
    }
  };

  useEffect(() => {
    setLatestFeedback(getFeedback());
  }, [assignment]);

  const getSubmissionFeedBack = () => (
    <>
      {!latestSubmission?.resubmittable && (
        <Col xs="12" className={'px-0'}>
          <hr />
        </Col>
      )}
      <Col md="12" className={'feedback-container pl-xl-5'}>
        <SubmissionFeedback submission={latestSubmission} feedback={getFeedback()} submissionFeedback={submissionFeedback?.value} />
      </Col>
    </>
  );

  const navigateToChatButton = (translationKey: string) => {
    const generateCustomChat = (submission: ISubmission) => {
      const chat: IChat = {
        journeyId: journey.id,
        submissionId: submission.id,
        title: assignment.title,
        people: [...submission.authors, ...journey.teachers.map(teacher => teacher.user)],
      };
      setGeneratedChat(chat);
    };

    const saveEmptySubmission = async () => {
      const values = {
        value: '',
        submittedForGradingDate: null,
        uploadedFiles: [],
      };
      const entity = {
        ...values,
        assignmentId: assignment.id,
        authors: [account],
        groupId: assignment.groups?.find(group => group.students.some(student => student.id === account.id)).id,
      };
      return await createSubmission(entity);
    };

    const handleOnClick = async () => {
      let submission: ISubmission = latestSubmission;
      if (!latestSubmission) {
        submission = await saveEmptySubmission();
      }
      !submissionFeedback && generateCustomChat(submission);
      history.push(`/chats?journeyId=${journey.id}&submissionId=${submission.id}`);
    };

    return (
      <Button onClick={async () => await handleOnClick()} color="primary" type="submit" className="mr-4">
        <Icon name={'conversation'} className={'mr-2'} stroke="white" width="19px" height="17px" />
        {translate(translationKey)}
      </Button>
    );
  };

  const submissionCreation = () => (
    <>
      {(hasGrade() || (latestSubmission && getFeedbackDate())) && getSubmissionFeedBack()}
      {latestSubmission?.resubmittable && (
        <Col xs="12" className={'px-0'}>
          <hr />
        </Col>
      )}
      <Col md="12" className={'pl-xl-5 editor-container'}>
        <h3 className="mb-2">{translate('schoolabyApp.assignment.detail.submitTask')}</h3>
        <LtiResults scores={ltiScores} ltiResources={ltiResources} />
        <MarkdownEditor value={latestSubmission?.value} onEditorStateChange={data => setValueFromEditor(data)} />
      </Col>
      <Col md="12" className={'upload-container mb-5 pl-xl-5 mt-4'}>
        <h3 className="mt-2">{translate('schoolabyApp.assignment.detail.addFiles')}</h3>
        <FileUploadProvider>
          <FileUpload
            videoFileNamePrefix={account.firstName + ' ' + account.lastName + '_' + assignment.title}
            existingFiles={files}
            onUpload={onFileUpload}
            onDelete={onFileDelete}
            uploadDisabled={isSubmitDisabled()}
          />
        </FileUploadProvider>
      </Col>
    </>
  );

  const submissionDetail = () => (
    <>
      <Col md="12" className={'submission-detail-col pl-xl-5'}>
        <Row>
          <Col md="9">
            <Row className={'mb-2'}>
              <h3 className={'ml-3 mb-0'}>{translate('schoolabyApp.submission.submissionInfo.yourSubmission')}</h3>
            </Row>
            <Row className={'mb-2'}>
              <Col>
                <span className={'d-inline-block'}>
                  <span className={'mr-2 text-secondary'}>{translate('schoolabyApp.submission.submitted')}</span>
                  <Icon name={'calendar'} className={'mb-1 mr-2'} />
                  <span
                    className={`${
                      isPastDate(new Date(latestSubmission.submittedForGradingDate), new Date(assignment.deadline))
                        ? 'text-success'
                        : 'text-danger'
                    }`}
                  >
                    <TextFormat type="date" value={latestSubmission.submittedForGradingDate} format={APP_LOCAL_DATE_FORMAT} />
                  </span>
                </span>
              </Col>
            </Row>
          </Col>
          <Col md="3" className={'align-self-center'}>
            <SubmissionGradeContainer feedback={getFeedback()} />
          </Col>
        </Row>
        <Row className={'mb-2'}>
          <Col>
            <LtiResults scores={ltiScores} ltiResources={ltiResources} />
          </Col>
        </Row>
        <Row className={'mb-2'}>
          <Col>
            <span className={'assignment-description text-break preserve-space'}>
              <ReactMarkdown source={latestSubmission.value} />
            </span>
          </Col>
        </Row>
      </Col>
      <Col md="12" className={'uploaded-files pl-xl-5'}>
        <h3 className={'mb-3'}>{translate('schoolabyApp.submission.submissionInfo.uploadedFiles')}</h3>
        <div className={'material-list'}>
          <UploadedFilesList
            files={latestSubmission?.uploadedFiles}
            alternativeText={translate('schoolabyApp.submission.submissionInfo.noAssets')}
            canEdit={isTeacher}
          />
        </div>
      </Col>
      {hasGrade() && !latestSubmission?.resubmittable && getSubmissionFeedBack()}
      <Col md={12} className={'pt-2 pl-xl-5'}>
        {navigateToChatButton('schoolabyApp.submission.sendMessage')}
      </Col>
    </>
  );

  const getSubmission = () => {
    if ((hasGrade() && !latestSubmission?.resubmittable) || (isSubmitDisabled() && !!latestSubmission)) {
      return submissionDetail();
    }
    if (!isSubmitDisabled()) {
      return submissionCreation();
    }
    return <p className="pl-4">{translate('schoolabyApp.assignment.detail.deadlinePassed')}</p>;
  };

  return assignment ? (
    <>
      <Row className={'detail-body'}>
        <Col md="5" className={'border-right py-4'}>
          <div className={'creation-assignment-info-container h-100 position-relative'}>
            <div className={'creation-assignment-info-content pr-md-3 pr-xl-4'}>
              <AssignmentInfo showHiddenFields={isTeacher} assignmentId={assignment.id} userGroup={userGroup} />
              <div className={'pt-2 pb-4 pl-0 d-inline-block'}>
                {!isSubmitDisabled() && navigateToChatButton('schoolabyApp.submission.askMore')}
              </div>
              {!!rubric && (
                <Button type="button" color="primary" className="d-inline-block" onClick={toggleShowingRubric}>
                  <Icon name="preferences" stroke="white" className="btn-icon" width="20px" height="20px" viewBox="0 0 25 20" />
                  <span className="text-center pl-2">{translate('schoolabyApp.assignment.rubric.viewTitle')}</span>
                </Button>
              )}
              <Row className={'m-0 p-0'}>
                <h3>{translate('schoolabyApp.assignment.detail.assets')}</h3>
                <Col md={'12'} className={'p-0 materials-col'}>
                  <Row noGutters className={'d-flex'}>
                    {hasMaterials(assignment) || ltiResources?.length ? (
                      <AssignmentMaterials assignment={assignment} />
                    ) : (
                      <p>{translate('schoolabyApp.assignment.detail.noAssets')}</p>
                    )}
                  </Row>
                </Col>
              </Row>
            </div>
          </div>
        </Col>
        <Col md="7" className={'pt-4 submission-creation-col bg-white border-top'}>
          <Row className={'mb-0 h-100'}>
            {assignment.requiresSubmission ? (
              <>
                <Col className={'pl-0 pr-0'}>{getSubmission()}</Col>
                <Col md="12" className={'submit-button-col mb-5 mt-auto'}>
                  <Button
                    disabled={isSubmitDisabled() || submitted}
                    onClick={saveSubmission}
                    className={'capitalized - btn submit-button d-block ml-auto'}
                    color={'primary'}
                    id={'submission-submit'}
                    role={'submit'}
                  >
                    {assignment.groups?.length
                      ? translate('schoolabyApp.assignment.detail.sendGroupAnswer')
                      : translate('schoolabyApp.assignment.detail.sendAnswer')}
                  </Button>
                </Col>
              </>
            ) : (
              <Col className="w-100 justify-content-center align-self-center">
                <Col className="text-center pb-5">
                  <img alt={'success'} src="content/images/submission_not_required.svg" className="img-fluid" />
                  <h2 className={'mt-4'}>{translate('schoolabyApp.assignment.detail.requiresSubmissionTitle')}</h2>
                  <h4 className={'text-secondary font-weight-normal mt-4'}>
                    {translate('schoolabyApp.assignment.detail.requiresSubmissionDescription')}
                  </h4>
                </Col>
                <Col className="d-flex align-items-end flex-column pr-5 pt-5">
                  <Button size="lg" outline color={'secondary'} onClick={() => history.push(`/journey/${journey.id}`)}>
                    {translate('entity.action.back')}
                  </Button>
                </Col>
              </Col>
            )}
          </Row>
        </Col>
      </Row>
      <SuccessModal
        modal={modal}
        toggle={closeSuccessModal}
        title={translate('schoolabyApp.submission.goodWork')}
        description={translate('schoolabyApp.submission.submitSuccess')}
        isJourneyModal
      />
      {!!selectedRubric && <RubricOverview viewMode={RubricViewModes.VIEW} selectedLevels={latestFeedback?.selectedCriterionLevels} />}
    </>
  ) : (
    <Spinner />
  );
};

const mapStateToProps = ({ authentication }: IRootState) => ({
  isAdmin: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ADMIN]),
  account: authentication.account,
});

const mapDispatchToProps = {
  createSubmission: createEntity,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SubmissionCreationBody);
