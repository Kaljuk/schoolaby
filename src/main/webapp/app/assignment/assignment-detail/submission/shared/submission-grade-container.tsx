import React, { FC } from 'react';
import { useGradingSchemeState } from 'app/shared/contexts/entity-detail-context';
import { PASS_FAIL, PERCENTAGE_0_100 } from 'app/shared/model/grading-scheme.model';
import {
  getGradeText,
  getGradingContainerClassNameByGradingType,
} from 'app/assignment/assignment-detail/submission/shared/grade-select-util';
import { getGrade } from 'app/shared/util/grade-util';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';

interface IProps {
  feedback: ISubmissionFeedback;
}

export const SubmissionGradeContainer: FC<IProps> = ({ feedback }) => {
  const { gradingScheme } = useGradingSchemeState();

  const getGradingScore = () => {
    const gradeText = getGradeText(gradingScheme?.code);
    if (gradeText === '') {
      if (gradingScheme?.code === PASS_FAIL) {
        return feedback?.grade ? getGrade(feedback) : `-`;
      }
    }
    if (gradingScheme?.code === PERCENTAGE_0_100) {
      return gradeText && (feedback?.grade ? `${feedback.grade}%${gradeText}` : `-${gradeText}`);
    } else {
      return gradeText && (feedback?.grade ? `${feedback.grade}${gradeText}` : `-${gradeText}`);
    }
  };

  return (
    <>
      {!gradingScheme?.isNonGradable() ? (
        <div className={`student-grade ${getGradingContainerClassNameByGradingType(gradingScheme?.code, feedback?.grade)}`}>
          {getGradingScore()}
        </div>
      ) : (
        <> </>
      )}
    </>
  );
};
