import React, { useEffect, useMemo, useState } from 'react';
import SimpleMDE from 'react-simplemde-editor';
import 'easymde/dist/easymde.min.css';
import ReactMarkdown from 'react-markdown';
import ReactDOMServer from 'react-dom/server';
import './markdown-editor.scss';

interface IProps {
  value?: any;
  onEditorStateChange?: any;
  options?: any;
}

export const MarkdownEditor = ({ value, onEditorStateChange, options }: IProps) => {
  const [editorState, setEditorState] = useState();

  const handleEditorStateChange = data => {
    onEditorStateChange(data);
    setEditorState(data);
  };

  useEffect(() => {
    handleEditorStateChange(value);
  }, [value]);

  const customRendererOptions = useMemo(() => {
    return {
      spellChecker: false,
      hideIcons: ['side-by-side', 'fullscreen', 'guide'],
      status: false,
      previewRender(text) {
        return ReactDOMServer.renderToString(
          <ReactMarkdown
            source={text}
            renderers={{
              code: ({ value: codeValue }) => <div>{codeValue}</div>,
            }}
          />
        );
      },
      ...options,
    };
  }, []);

  return <SimpleMDE onChange={handleEditorStateChange} value={editorState} options={customRendererOptions} />;
};
