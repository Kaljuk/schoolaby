import React, { FC, useContext, useEffect, useState } from 'react';
import AssignmentInfo from 'app/assignment/assignment-detail-new/assignment-info';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { Col, Container, Row } from 'reactstrap';
import { TextFormat, translate } from 'react-jhipster';
import ContentCard from 'app/shared/layout/content-card/content-card';

import './students-assignment-view.scss';
import { createSubmission, patchSubmission, useGetSubmissions } from 'app/shared/services/submission-api';
import ReactMarkdown from 'react-markdown';
import FileGrid from 'app/shared/layout/file-grid/file-grid';
import { UploadedFileProvider } from 'app/shared/contexts/uploaded-file-context';
import LtiResults from 'app/assignment/assignment-detail/students/lti-results/lti-results';
import { useGetLtiResources, useGetLtiScores } from 'app/shared/services/lti-api';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import { DateTime } from 'luxon';
import FeedbackHistory from 'app/assignment/assignment-detail/students/feedback-history/feedback-history';
import Icon from 'app/shared/icons';
import { APP_LOCAL_DATE_TIME_FORMAT } from 'app/config/constants';
import CustomButton from 'app/shared/layout/custom-button/custom-button';
import { isPastDeadline } from 'app/assignment/assignment-detail/assignment-util';
import { orderBy } from 'lodash';
import { MarkdownEditor } from 'app/assignment/assignment-detail/submission/shared/markdown-editor';
import { FileUploadProvider } from 'app/shared/form/file-upload-context';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { convertDateTimeToServer } from 'app/shared/util/date-utils';
import FileUploadNew from 'app/shared/form/file-upload-new';

const StudentAssignmentView: FC<StateProps> = ({ account }) => {
  const [editMode, setEditMode] = useState<boolean>(false);
  const [submitted, setSubmitted] = useState<boolean>(false);
  const { assignment } = useContext(AssignmentContext);
  const { data: submissions, refetch } = useGetSubmissions({ assignmentId: assignment?.id }, !!assignment?.id);
  const { data: ltiResources } = useGetLtiResources({ assignmentId: assignment?.id }, !!assignment?.id);
  const { data: ltiScores } = useGetLtiScores(
    {
      assignmentId: assignment?.id,
      userId: account?.id,
    },
    !!assignment?.id && !!ltiResources?.length && !!account?.id
  );
  const submission = submissions?.[0];

  const getSubmissionState = () =>
    new Date(DateTime.fromISO(submission?.submittedForGradingDate)) <= new Date(DateTime.fromISO(assignment?.deadline))
      ? 'completed'
      : 'overdue';

  const latestSubmissionGraded = () => {
    const latestFeedback = orderBy(submission?.submissionFeedbacks, 'feedbackDate', 'desc')[0];
    return new Date(DateTime.fromISO(latestFeedback?.feedbackDate)) >= new Date(DateTime.fromISO(submission?.submittedForGradingDate));
  };

  const isSubmitDisabled = () => {
    if (submission?.resubmittable || (assignment?.flexibleDeadline && !!submission && !latestSubmissionGraded())) {
      return false;
    }

    const isOverdue: boolean = isPastDeadline(assignment) && !assignment.flexibleDeadline;

    return latestSubmissionGraded() || isOverdue;
  };

  const saveSubmission = (valueFromEditor: string, files: IUploadedFile[]) => {
    setSubmitted(true);

    const values: ISubmission = {
      value: valueFromEditor ? valueFromEditor : '',
      submittedForGradingDate: convertDateTimeToServer(DateTime.local().toString()),
      uploadedFiles: files,
    };

    if (submission) {
      const entity = {
        ...values,
      };
      patchSubmission(submission.id, {
        ...entity,
        className: '.SubmissionResubmitPatchDTO',
      }).then(() => {
        setEditMode(false);
        refetch();
      });
    } else {
      createSubmission({
        ...values,
        assignmentId: assignment.id,
        authors: [account],
      }).then(() => {
        setEditMode(false);
        refetch();
      });
    }

    setSubmitted(false);
  };

  const deadlinePassedAndNotFlexible = (): boolean => {
    return DateTime.fromISO(assignment?.deadline) < Date.now() && !assignment?.flexibleDeadline;
  };

  const showPlaceHolderImage = (): boolean => !submission && (!assignment?.requiresSubmission || deadlinePassedAndNotFlexible());

  const SubmissionPlaceholderImage = () => {
    if (!assignment?.requiresSubmission) {
      return (
        <ContentCard className="text-center pb-5">
          <img alt={'success'} src={'content/images/submission_not_required.svg'} className="img-fluid" />
          <h2 className={'mt-4'}>{translate('schoolabyApp.assignment.detail.requiresSubmissionTitle')}</h2>
          <h4 className={'text-secondary font-weight-normal mt-4'}>
            {translate('schoolabyApp.assignment.detail.requiresSubmissionDescription')}
          </h4>
        </ContentCard>
      );
    } else if (deadlinePassedAndNotFlexible()) {
      return (
        <ContentCard className="text-center pb-5">
          <img alt={'fail'} src={'content/images/submission_overdue.svg'} className="img-fluid" />
          <h2 className={'mt-4'}>{translate('schoolabyApp.assignment.detail.oops')}</h2>
          <h4 className={'text-secondary font-weight-normal mt-4'}>
            {translate('schoolabyApp.assignment.detail.submissionNotFlexibleAndOverdue')}
          </h4>
        </ContentCard>
      );
    } else {
      return null;
    }
  };

  const EditableSubmission = () => {
    const [valueFromEditor, setValueFromEditor] = useState<string>('');
    const [files, setFiles] = useState<IUploadedFile[]>([]);

    useEffect(() => {
      submission?.value ? setValueFromEditor(submission.value) : setValueFromEditor('');
      submission?.uploadedFiles?.length && setFiles(submission.uploadedFiles);
    }, [submission]);

    const onFileUpload = (file: IUploadedFile) => {
      setFiles(previousState => [...previousState, file]);
    };

    const onFileDelete = (file: IUploadedFile) => {
      setFiles(previousState => previousState.filter(prevFile => prevFile.id !== file.id));
    };

    return !submission || editMode ? (
      <>
        <h6 className={'card-title mb-4'}>{translate('schoolabyApp.assignment.detail.submitTask')}</h6>
        <LtiResults scores={ltiScores} />
        <MarkdownEditor value={valueFromEditor} onEditorStateChange={data => setValueFromEditor(data)} />
        <FileUploadProvider>
          <FileUploadNew
            videoFileNamePrefix={account.firstName + ' ' + account.lastName + '_' + assignment.title}
            existingFiles={files}
            onUpload={onFileUpload}
            onDelete={onFileDelete}
            uploadDisabled={isSubmitDisabled()}
          />
        </FileUploadProvider>
        <div className="d-flex gap-2 justify-content-end mt-3">
          {!!submission && (
            <CustomButton title={translate('entity.action.cancel')} buttonType={'cancel'} onClick={() => setEditMode(false)} />
          )}
          <CustomButton
            title={translate('schoolabyApp.assignment.detail.sendAnswer')}
            buttonType={'primary'}
            disabled={submitted}
            onClick={() => saveSubmission(valueFromEditor, files)}
          />
        </div>
      </>
    ) : (
      <>
        <div className="d-flex align-items-center justify-content-between mb-1">
          <h6 className={'card-title mb-0'}>{translate('schoolabyApp.submission.submissionInfo.yourSubmission')}</h6>
          {!isSubmitDisabled() && (
            <CustomButton
              title={translate('entity.action.edit')}
              className={'edit-button'}
              buttonType={'secondary'}
              outline
              size={'sm'}
              sharper
              onClick={() => {
                setEditMode(true);
                setFiles(submission.uploadedFiles || []);
              }}
            />
          )}
        </div>
        <span className={'d-flex align-items-center mb-3 submitted-date'}>
          <Icon name={'stopwatch'} className={getSubmissionState() + ' stopwatch mr-2'} />
          {translate('schoolabyApp.submission.submitted')}
          <strong className={'ml-1'}>
            <TextFormat type="date" value={submission?.submittedForGradingDate} format={APP_LOCAL_DATE_TIME_FORMAT} />
          </strong>
        </span>
        <ReactMarkdown
          source={submission?.value}
          renderers={{
            code: ({ value: codeValue }) => <div>{codeValue}</div>,
          }}
        />
        <UploadedFileProvider>
          <FileGrid files={submission?.uploadedFiles} />
        </UploadedFileProvider>
      </>
    );
  };

  const SubmissionSection = () => (
    <div className={'d-flex flex-column gap-2'}>
      <ContentCard className={'student-solution'}>
        <EditableSubmission />
      </ContentCard>
      {!editMode && <LtiResults scores={ltiScores} />}
    </div>
  );

  return (
    <Container className="students-assignment-view-container">
      <Row className="h-100 students-assignment-view justify-content-center">
        {assignment ? (
          <>
            <Col lg={6} className="w-100 justify-content-center pl-lg-0 pr-lg-1 pb-2">
              <AssignmentInfo />
            </Col>
            <Col className="w-100 justify-content-center pr-lg-0 pl-lg-1 student-solution-container pb-2" lg={6}>
              {showPlaceHolderImage() ? <SubmissionPlaceholderImage /> : <SubmissionSection />}
              <div className={'mt-2'}>
                <FeedbackHistory submission={submission} studentId={account.id} />
              </div>
            </Col>
          </>
        ) : null}
      </Row>
    </Container>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  account: storeState.authentication.account,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(StudentAssignmentView);
