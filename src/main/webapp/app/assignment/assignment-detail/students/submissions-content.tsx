import React, { FC, useContext, useEffect } from 'react';
import './submissions-content.scss';
import { useGradingSchemeState, useSelectedSubmissionState, useSelectedUserState } from 'app/shared/contexts/entity-detail-context';
import StudentsList from './students-list/students-list';
import { useGetSubmissions } from 'app/shared/services/submission-api';
import { useGetGradingScheme } from 'app/shared/services/grading-scheme-api';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import StudentSolution from 'app/assignment/assignment-detail/students/student-solution/student-solution';
import { Col, Row } from 'reactstrap';
import { useGetJourneyStudents } from 'app/shared/services/journey-api';
import FeedbackHistory from 'app/assignment/assignment-detail/students/feedback-history/feedback-history';
import Feedback from 'app/assignment/assignment-detail/students/feedback/feedback';
import { ISubmission } from 'app/shared/model/submission/submission.model';

const SubmissionsContent: FC = () => {
  const { assignment } = useContext(AssignmentContext);
  const { setGradingScheme } = useGradingSchemeState();
  const { selectedUser } = useSelectedUserState();
  const { data: submittedSubmissions } = useGetSubmissions({ assignmentId: assignment?.id }, !!assignment?.id);
  const { selectedSubmission, setSelectedSubmission } = useSelectedSubmissionState();
  useGetGradingScheme(+assignment?.gradingSchemeId, !!assignment?.gradingSchemeId, setGradingScheme);
  const { data: journeyStudents } = useGetJourneyStudents(assignment?.journeyId, !!assignment?.id);

  const getSubmissionBySelectedStudent = (): ISubmission => {
    return submittedSubmissions?.find(s => s?.authors?.find(a => a.id === selectedUser?.id));
  };

  useEffect(() => {
    if (selectedUser && submittedSubmissions?.length) {
      const submissionSentForGrading = getSubmissionBySelectedStudent();
      setSelectedSubmission(submissionSentForGrading);
    } else {
      setSelectedSubmission(null);
    }
  }, [submittedSubmissions]);

  return (
    <div className="submissions-content">
      <h5>{assignment?.title}</h5>
      <Row noGutters>
        <Col xs={12} md={3}>
          <StudentsList />
        </Col>
        {(journeyStudents?.length || assignment?.students?.length) && (
          <>
            <Col xs={12} md={6} className="px-2">
              <StudentSolution />
            </Col>
            {selectedUser && (
              <Col className={'submissions-content-right'} xs={12} md={3}>
                <Row noGutters>
                  <Col xs={12}>
                    <Feedback assignment={assignment} />
                  </Col>
                  <Col className={'my-2'} xs={12}>
                    <FeedbackHistory studentId={selectedUser?.id} submission={selectedSubmission} />
                  </Col>
                </Row>
              </Col>
            )}
          </>
        )}
      </Row>
    </div>
  );
};

export default SubmissionsContent;
