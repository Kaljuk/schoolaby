import React from 'react';
import FileGrid from 'app/shared/layout/file-grid/file-grid';
import { getFullName } from 'app/shared/util/string-utils';

import { UploadedFileProvider } from 'app/shared/contexts/uploaded-file-context';
import { translate } from 'react-jhipster';
import ContentCard from 'app/shared/layout/content-card/content-card';
import { useSelectedSubmissionState, useSelectedUserState } from 'app/shared/contexts/entity-detail-context';

const StudentUploads = () => {
  const { selectedUser } = useSelectedUserState();
  const { selectedSubmission } = useSelectedSubmissionState();

  const getStudentUploadsTranslation = () => {
    const fullName = getFullName(selectedUser);
    return fullName.toLowerCase().endsWith('s')
      ? translate('schoolabyApp.submission.submissionInfo.studentNameUploadsEndingWithS', { fullName })
      : translate('schoolabyApp.submission.submissionInfo.studentNameUploads', { fullName });
  };

  return selectedSubmission?.uploadedFiles?.length ? (
    <ContentCard className={'student-uploads'}>
      <h6 className={'mb-3 ml-2 card-title'}>{getStudentUploadsTranslation()}</h6>
      <UploadedFileProvider>
        <FileGrid className={'ml-2'} files={selectedSubmission?.uploadedFiles} />
      </UploadedFileProvider>
    </ContentCard>
  ) : null;
};

export default StudentUploads;
