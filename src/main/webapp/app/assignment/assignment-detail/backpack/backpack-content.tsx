import React from 'react';
import MaterialNarrowCard from 'app/shared/layout/material-card/material-narrow-card';
import sortBy from 'lodash/sortBy';
import { Row, Col } from 'reactstrap';

import './backpack-content.scss';

const BackpackContent = ({ milestone }) => (
  <Row className={'mt-3 mx-2 backpack-content'}>
    {sortBy(milestone?.materials ? milestone.materials : [], 'sequenceNumber')?.map(material => (
      <Col key={`material-card-col-${material.id}`} xs={12} sm={6} md={4} xl={2} className="mt-2 px-1" style={{ maxWidth: '30rem' }}>
        <MaterialNarrowCard material={material} />
      </Col>
    ))}
  </Row>
);

export default BackpackContent;
