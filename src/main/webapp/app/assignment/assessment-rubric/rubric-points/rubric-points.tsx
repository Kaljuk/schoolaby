import React, { FC } from 'react';
import './rubric-points.scss';
import { IRubric } from 'app/shared/model/rubric/rubric.model';
import { ISelectedCritreionLevel } from 'app/shared/model/rubric/selected-critreion-level.model';
import { ICriterion } from 'app/shared/model/rubric/criterion.model';
import { Col, Row } from 'reactstrap';
import { calculateSelectedCriterionTotal, getPointsColorClass } from 'app/shared/util/rubric-utils';
import { translate } from 'react-jhipster';

interface RubricPointsProps {
  rubric: IRubric;
  selectedCriterionLevels: ISelectedCritreionLevel[];
}

const RubricPoints: FC<RubricPointsProps> = ({ rubric, selectedCriterionLevels }) => {
  const getSelectedLevelForCriterion = (criterion: ICriterion) => {
    return selectedCriterionLevels?.length
      ? selectedCriterionLevels.filter(selectedLevel => selectedLevel.criterionLevel.criterionId === criterion.id)[0]
      : undefined;
  };

  const getPointsColorClassFromSelectedLevels = () => {
    return getPointsColorClass(calculateSelectedCriterionTotal(selectedCriterionLevels), rubric.minPoints, rubric.maxPoints);
  };

  const CriterionPoints = ({ points, maxPoints, pointsColor, selectedCriterionLevel, customClasses }) => (
    <div className={`criterion-points d-inline-block ${pointsColor} ${customClasses ? customClasses : ''}`}>
      <div className={`d-flex justify-content-center`}>
        <div>
          <span className={`${pointsColor} points`}>{points}</span> / {maxPoints}
        </div>
        {selectedCriterionLevel ? (
          <div className={'level-description-wrapper'}>
            <span className={'level-description pl-1'}>{selectedCriterionLevel.modifiedDescription}</span>
          </div>
        ) : null}
      </div>
    </div>
  );

  const CriterionPointsRow = ({ criterion, selectedCriterionLevel }) => {
    const getCriterionPointsColorClass = () => {
      return selectedCriterionLevel
        ? getPointsColorClass(selectedCriterionLevel?.criterionLevel?.points, criterion.minPoints, criterion.maxPoints)
        : '';
    };

    return (
      <Row className={'py-1 align-items-center'}>
        <Col lg={selectedCriterionLevels?.length ? 3 : 6}>
          <p className={'row-title mb-0'}>{criterion.title}</p>
        </Col>
        <Col lg={selectedCriterionLevels?.length ? 9 : 6}>
          <CriterionPoints
            points={selectedCriterionLevel?.criterionLevel?.points || 0}
            maxPoints={criterion.maxPoints}
            pointsColor={getCriterionPointsColorClass()}
            selectedCriterionLevel={selectedCriterionLevel}
            customClasses={undefined}
          />
        </Col>
      </Row>
    );
  };

  return (
    <div className="w-100 rubric-points mb-3">
      {rubric?.criterions.map(criterion => (
        <CriterionPointsRow key={criterion.id} criterion={criterion} selectedCriterionLevel={getSelectedLevelForCriterion(criterion)} />
      ))}
      <Row className={'py-2 align-items-center'}>
        <Col lg={7}>
          <p className={'row-title mb-0'}>{translate('schoolabyApp.assignment.rubric.assessmentRubricTotalPoints')}</p>
        </Col>
        <Col lg={5}>
          <CriterionPoints
            points={selectedCriterionLevels ? calculateSelectedCriterionTotal(selectedCriterionLevels) : 0}
            maxPoints={rubric.maxPoints}
            pointsColor={selectedCriterionLevels ? getPointsColorClassFromSelectedLevels() : 'light-green'}
            selectedCriterionLevel={undefined}
            customClasses={'total-points'}
          />
        </Col>
      </Row>
    </div>
  );
};

export default RubricPoints;
