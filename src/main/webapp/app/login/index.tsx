import React from 'react';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import Login from './login';

const Routes = ({ match }) => <ErrorBoundaryRoute path={match.url} component={Login} />;

export default Routes;
