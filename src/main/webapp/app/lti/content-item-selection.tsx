import React from 'react';
import { translate } from 'react-jhipster';
import { Alert } from 'reactstrap';
import Icon from 'app/shared/icons';

interface IContentItemSelection {
  successful?: boolean;
}

export const ContentItemSelection = ({ successful }: IContentItemSelection) => {
  const ContentItemSuccess = () => (
    <span className={'success'}>
      <img alt={'success'} src={'content/images/success.svg'} />
      <Alert className={'mt-3 d-flex'} color="success">
        {translate('schoolabyApp.ltiContentItem.success')}
      </Alert>
    </span>
  );

  const ContentItemFail = () => (
    <span className={'fail'}>
      <Alert color="danger">
        <Icon className={'mr-2'} name={'exclamationMark'} />
        {translate('schoolabyApp.ltiContentItem.failure')}
      </Alert>
    </span>
  );

  return (
    <div className="d-flex align-items-center justify-content-center content-item-selection">
      {successful ? <ContentItemSuccess /> : <ContentItemFail />}
    </div>
  );
};
