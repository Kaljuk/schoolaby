import React, { FC, useMemo } from 'react';
import { Col } from 'reactstrap';
import { IMilestone } from 'app/shared/model/milestone.model';
import sortBy from 'lodash/sortBy';
import MaterialWideCard from 'app/shared/layout/material-card/material-wide-card';
import { Material } from 'app/shared/model/material.model';

interface IProps {
  milestone: IMilestone;
  previewEnabled?: boolean;
}

export const MilestoneMaterials: FC<IProps> = ({ milestone, previewEnabled }) => {
  return useMemo(
    () => (
      <>
        {sortBy(
          milestone.materials.map(material => Material.fromJson(material)),
          'sequenceNumber'
        )?.map(material => (
          <Col sm={12} xl={6} key={material.id} className={'mt-2'}>
            <MaterialWideCard material={material} previewEnabled={previewEnabled} />
          </Col>
        ))}
      </>
    ),
    [milestone?.materials]
  );
};
