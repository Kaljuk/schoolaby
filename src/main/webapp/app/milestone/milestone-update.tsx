import React, { FC, useContext, useEffect, useState } from 'react';
import RightSidePanel from 'app/shared/layout/right-side-panel/right-side-panel';
import { Field, Form } from 'react-final-form';
import Input from 'app/shared/form/components/input';
import DatePickerNew from 'app/shared/layout/date-picker-new/date-picker-new';
import { DatePicker, MarkdownEditorInput, Radio } from 'app/shared/form';
import { Button } from 'reactstrap';
import { translate } from 'react-jhipster';
import {
  useDisplayEntityUpdateState,
  useEntityUpdateIdState,
  useEntityToUpdateState,
  useSelectedEducationalAlignmentsState,
} from 'app/shared/contexts/entity-update-context';
import { useCreateMilestone, useGetMilestone, useUpdateMilestone } from 'app/shared/services/milestone-api';
import { useQueryParam } from 'app/shared/hooks/useQueryParam';
import { useChosenMaterialState, useMaterialState } from 'app/shared/contexts/material-context';
import sortBy from 'lodash/sortBy';
import { mapEntityIdList } from 'app/shared/util/entity-utils';
import { JourneyDetailContext } from 'app/journey/journey-detail/journey-detail-new';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { useUpdateMilestoneEntitySchema } from 'app/shared/schema/update-entity';
import { connect } from 'react-redux';
import { IRootState } from 'app/shared/reducers';
import { useGetJourney } from 'app/shared/services/journey-api';
import { createYupValidator } from 'app/shared/util/form-utils';
import SpinnerContainer from 'app/shared/layout/spinner-container/spinner-container';
import { DATEPICKER_DATE_TIME_FORMAT } from 'app/shared/form/components/date-picker';
import Icon from 'app/shared/icons';
import { DateTime } from 'luxon';
import EducationalAlignmentMultiselect from 'app/shared/layout/educational-alignment-multiselect/educational-alignment-multiselect';
import { useHistory } from 'react-router-dom';

import ChooseMaterials from 'app/shared/layout/choose-materials/choose-materials';
import MaterialCardModal from 'app/shared/layout/material-card/material-card-modal';
import { LTI_MATERIAL } from 'app/shared/model/material.model';
import { COUNTRY } from 'app/config/constants';

const MilestoneUpdate: FC<StateProps> = ({ locale, isUserUkrainian }) => {
  const history = useHistory();
  const journeyContext = useContext(JourneyDetailContext);
  const { closeEntityUpdate } = useDisplayEntityUpdateState();
  const { milestoneUpdateId } = useEntityUpdateIdState();
  const [goToMarketplace, setGoToMarketplace] = useState(false);
  const { entity, setEntity } = useEntityToUpdateState();
  const { data: existingMilestone } = useGetMilestone(milestoneUpdateId, !!milestoneUpdateId);
  const { mutate: createMilestone } = useCreateMilestone();
  const { mutate: updateMilestone } = useUpdateMilestone(milestoneUpdateId);
  const [{ journeyId: journeyIdParam }] = useQueryParam<{ journeyId: string }>();
  const journeyId = existingMilestone ? existingMilestone.journeyId : journeyContext ? journeyContext.journey.id : journeyIdParam;
  const { data: journey } = useGetJourney(journeyId, !!journeyId);
  const { materials, setMaterials } = useMaterialState();
  const { chosenMaterial, setChosenMaterial } = useChosenMaterialState();
  const { selectedEducationalAlignments, setSelectedEducationalAlignments } = useSelectedEducationalAlignmentsState();
  const [radio, setRadio] = useState<string>();

  const toggleModal = () => setChosenMaterial(null);

  useEffect(() => {
    const published = entity ? entity.published : existingMilestone?.published;
    if (!milestoneUpdateId && !entity) {
      setRadio('published');
    } else if (!published) {
      setRadio('unpublished');
    } else if (DateTime.fromISO(published).toUTC() <= DateTime.local().toUTC()) {
      setRadio('published');
    } else {
      setRadio('publishedAt');
    }
  }, [existingMilestone?.published]);

  useEffect(() => {
    return () => {
      setSelectedEducationalAlignments([]);
      setMaterials([]);
      setEntity(null);
    };
  }, []);

  useEffect(() => {
    if (existingMilestone?.id && !entity) {
      const milestoneMaterials = existingMilestone?.materials ? existingMilestone.materials : [];
      setMaterials(sortBy(milestoneMaterials, 'sequenceNumber'));
    }
  }, [existingMilestone]);

  const saveMilestone = values => {
    values.endDate = convertDateTimeToServer(values.endDate);
    values.deleted = convertDateTimeToServer(values.deleted);

    if (!entity && !milestoneUpdateId && radio === 'published') {
      values.published = DateTime.local();
    }

    if (values?.published !== null) {
      values.published = convertDateTimeToServer(values.published);
    }

    const milestone = {
      ...values,
      journeyId,
      materials: materials.filter(material => material.type !== 'LTI'),
      educationalAlignments: mapEntityIdList(selectedEducationalAlignments),
    };

    if (goToMarketplace) {
      const url = values.id ? `/marketplace/milestone/${values.id}` : '/marketplace/milestone/new';
      history.push({
        pathname: url,
        state: {
          entity: {
            ...milestone,
            educationalAlignments: selectedEducationalAlignments,
            materials: materials.map(material => material.getCopy()),
          },
          back: history.location,
        },
      });
    } else {
      values.id ? updateMilestone(milestone) : createMilestone(milestone);
      closeEntityUpdate();
    }
  };

  const schema = useUpdateMilestoneEntitySchema(locale, journey?.curriculum?.requiresSubject, !isUserUkrainian);

  return (
    <>
      <RightSidePanel
        title={milestoneUpdateId ? translate('entity.edit.milestone') : translate('entity.add.milestone')}
        closePanel={closeEntityUpdate}
        className="position-relative"
      >
        <div className="entity-update">
          <SpinnerContainer displaySpinner={milestoneUpdateId && !existingMilestone} spinnerClassName="position-absolute">
            <Form onSubmit={saveMilestone} initialValues={entity ? entity : existingMilestone} validate={createYupValidator(schema)}>
              {({ handleSubmit, form, hasValidationErrors }) => {
                useEffect(() => {
                  hasValidationErrors && goToMarketplace && setGoToMarketplace(false);
                }, [hasValidationErrors, goToMarketplace]);

                return (
                  <form onSubmit={handleSubmit}>
                    <Field
                      id="milestone-title"
                      name="title"
                      type="text"
                      placeholder={translate('schoolabyApp.milestone.titlePlaceholder')}
                      label={translate('schoolabyApp.milestone.title')}
                      component={Input}
                    />
                    <Field
                      id="milestone-description"
                      name="description"
                      type="textarea"
                      label={translate('schoolabyApp.milestone.description')}
                      component={MarkdownEditorInput}
                    />
                    <Field
                      id="milestone-educational-alignments"
                      name="educationalAlignments"
                      component={EducationalAlignmentMultiselect}
                      disabled={!journey?.curriculum?.requiresSubject}
                    />
                    <Field
                      id="milestone-end-date"
                      name="endDate"
                      type="date"
                      label={translate('schoolabyApp.milestone.deadline')}
                      className="entity-datepicker"
                      initialValue={!!existingMilestone && convertDateTimeFromServer(existingMilestone.endDate)}
                      component={DatePickerNew}
                    />
                    <Radio
                      name="published"
                      label={translate('entity.publishedState.published')}
                      onClick={() => setRadio('published')}
                      onChange={input => form.change(input.name, convertDateTimeToServer(DateTime.local()))}
                      checked={radio === 'published'}
                    />
                    <Radio
                      name="published"
                      label={translate('entity.publishedState.unpublished')}
                      onClick={() => setRadio('unpublished')}
                      onChange={input => form.change(input.name, null)}
                      checked={radio === 'unpublished'}
                    />
                    <span className={'published-date-container'}>
                      <Radio
                        name="published"
                        className="d-inline"
                        label={translate('entity.publishedState.publishAt')}
                        onClick={() => setRadio('publishedAt')}
                        onChange={input => form.change(input.name, DateTime.local().startOf('day').plus({ days: 1 }))}
                        checked={radio === 'publishedAt'}
                      />
                      {radio === 'publishedAt' && (
                        <Field
                          label={'Publish at'}
                          type="datetime-local"
                          name="published"
                          placeholder={'YYYY-MM-DD HH:mm:ss'}
                          render={dateProps => (
                            <span className="published-date mt-1">
                              <>
                                <DatePicker meta={dateProps.meta} input={dateProps.input} dateTimeFormat={DATEPICKER_DATE_TIME_FORMAT} />
                                <Icon onClick={() => dateProps.input.click()} className={'published-date-icon'} name={'calendarNew'} />
                              </>
                            </span>
                          )}
                        />
                      )}
                    </span>
                    <hr />
                    <ChooseMaterials setGoToMarketplace={setGoToMarketplace} isAllowedToModify />
                    <div className="entity-update-footer d-flex justify-content-end mt-3">
                      <Button className="cancel-btn bg-white px-4" type="button" onClick={closeEntityUpdate}>
                        {translate('entity.action.cancel')}
                      </Button>
                      <Button data-testid="save-entity" className="ml-2 px-4" color="primary">
                        {translate('entity.action.save')}
                      </Button>
                    </div>
                  </form>
                );
              }}
            </Form>
          </SpinnerContainer>
        </div>
      </RightSidePanel>
      <MaterialCardModal
        isOpen={!!chosenMaterial && chosenMaterial?.type !== LTI_MATERIAL}
        material={chosenMaterial}
        addable
        removable
        toggleModal={toggleModal}
      />
    </>
  );
};

const mapStateToProps = ({ locale, authentication }: IRootState) => ({
  locale: locale.currentLocale,
  isUserUkrainian: authentication.account.country === COUNTRY.UKRAINE,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(MilestoneUpdate);
