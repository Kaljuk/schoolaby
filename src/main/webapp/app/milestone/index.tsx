import React from 'react';
import { Switch } from 'react-router-dom';
import MilestoneDetail from './milestone-detail';
import ErrorBoundaryDetailUpdateEntityRoute from 'app/shared/error/error-boundary-detail-update-entity-route';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryDetailUpdateEntityRoute exact path={`${match.url}/:id`} component={MilestoneDetail} />
    </Switch>
  </>
);

export default Routes;
