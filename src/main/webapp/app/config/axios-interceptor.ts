import axios from 'axios';
import { Storage, translate } from 'react-jhipster';

import { SERVER_API_URL } from 'app/config/constants';
import { toast } from 'react-toastify';
import Cookies from 'js-cookie';

const TIMEOUT = 1 * 60 * 1000;
axios.defaults.timeout = TIMEOUT;
axios.defaults.baseURL = SERVER_API_URL;
axios.defaults.showToast = true;

const setupAxiosInterceptors = onUnauthenticated => {
  const onRequestSuccess = config => {
    const token =
      Storage.local.get('jhi-authenticationToken') ||
      Storage.session.get('jhi-authenticationToken') ||
      Cookies.get('jhi-authenticationToken');
    if (token && config.headers?.Authorization !== 'none') {
      config.headers.Authorization = `Bearer ${token}`;
    } else if (config.headers?.Authorization === 'none') {
      delete config.headers.Authorization;
    }
    return config;
  };
  const onResponseSuccess = response => {
    const showToast = response?.config?.showToast;
    let alert: string = null;
    let params: string = null;
    if (response?.headers) {
      Object.entries<string>(response.headers).forEach(([key, value]) => {
        if (key.toLowerCase().endsWith('app-alert')) {
          alert = value;
        } else if (key.toLowerCase().endsWith('app-params')) {
          params = decodeURIComponent(value.replace(/\+/g, ' '));
        }
      });
    }
    if (alert && showToast) {
      toast.success(translate(alert, { param: params }));
    }

    return response;
  };
  const onResponseError = err => {
    const showToast = err?.response?.config?.showToast;
    const errorMessage = err?.response?.data?.message;
    if (showToast) {
      if (errorMessage) {
        if (errorMessage !== 'error.externalRequest') {
          toast.error(translate(errorMessage));
        }
      } else {
        toast.error(translate('error.unknown'));
      }
    }

    const status = err.status || (err.response ? err.response.status : 0);
    if (status === 403 || status === 401) {
      onUnauthenticated();
    } else if (status === 404) {
      return (window.location.href = '/error/not_found');
    }
    return Promise.reject(err);
  };
  axios.interceptors.request.use(onRequestSuccess);
  axios.interceptors.response.use(onResponseSuccess, onResponseError);
};

export default setupAxiosInterceptors;
