import * as axios from 'axios';

declare module 'axios' {
  // @ts-ignore
  interface AxiosRequestConfig extends axios.AxiosRequestConfig {
    showToast?: boolean;
  }
}
