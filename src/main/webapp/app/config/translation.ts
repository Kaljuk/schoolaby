import { Storage, TranslatorContext } from 'react-jhipster';

import { setLocale } from 'app/shared/reducers/locale';
import { COUNTRY } from 'app/config/constants';

TranslatorContext.setDefaultLocale('en');
TranslatorContext.setRenderInnerTextForMissingKeys(false);
TranslatorContext.setMissingTranslationMsg('');

export type Locale = 'et' | 'en' | 'fi' | 'sw' | 'uk';

export const languages: any = {
  en: { name: 'English' },
  et: { name: 'Eesti' },
  fi: { name: 'Suomi' },
  sw: { name: 'Swahili' },
  uk: { name: 'Yкраїнський' },
};

export const countries = new Map<string, { langKey: string; nativeName: string }>([
  [COUNTRY.ESTONIA, { langKey: 'et', nativeName: 'Eesti' }],
  [COUNTRY.TANZANIA, { langKey: 'sw', nativeName: 'Tanzania' }],
  [COUNTRY.UNITED_STATES, { langKey: 'en', nativeName: 'United States' }],
  [COUNTRY.FINLAND, { langKey: 'fi', nativeName: 'Suomi' }],
  [COUNTRY.UKRAINE, { langKey: 'uk', nativeName: 'Україна' }],
]);

export const locales: Locale[] = Object.keys(languages).sort() as Locale[];

export const registerLocale = store => {
  store.dispatch(setLocale(Storage.session.get('locale', 'et')));
};
