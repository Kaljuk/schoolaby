import './dashboard-messages.scss';
import React, { FC, useEffect, useState } from 'react';
import { Card, Col, Row } from 'reactstrap';
import { getSubjectIcon } from 'app/shared/icons';
import Avatar from 'app/shared/layout/header/avatar';
import LinesEllipsis from 'react-lines-ellipsis';
import { getMessageViews } from 'app/shared/services/message-api';
import { APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { TextFormat, translate } from 'react-jhipster';
import { DateTime } from 'luxon';
import { IMessageView } from 'app/shared/model/messageView.model';
import { formatToStartOfDay } from 'app/shared/util/date-utils';
import { useHistory } from 'react-router-dom';

const calculateDate = date => {
  const formattedTime = formatToStartOfDay(DateTime.fromISO(date)).toISODate();
  const today = formatToStartOfDay(DateTime.local());

  const yesterday = today.minus({ days: 1 }).toISODate();
  const secondDay = today.minus({ days: 2 }).toISODate();
  const thirdDay = today.minus({ days: 3 }).toISODate();

  if (formattedTime === today.toISODate()) {
    return translate('schoolabyApp.dashboard.recentMessages.today');
  } else if (formattedTime === yesterday) {
    return translate('schoolabyApp.dashboard.recentMessages.oneDayAgo');
  } else if (formattedTime === secondDay) {
    return translate('schoolabyApp.dashboard.recentMessages.twoDaysAgo');
  } else if (formattedTime === thirdDay) {
    return translate('schoolabyApp.dashboard.recentMessages.threeDaysAgo');
  } else {
    return <TextFormat type="date" value={date} format={APP_LOCAL_DATE_FORMAT} />;
  }
};

const DashboardMessages: FC = () => {
  const [messagesViews, setMessageViews] = useState<IMessageView[]>([]);
  const history = useHistory();

  useEffect(() => {
    getMessageViews('id,desc', 0, 5).then(resp => setMessageViews(resp.data));
  }, []);

  const handleOnClick = (e, url) => {
    e.preventDefault();
    history.push(url);
  };

  return (
    <>
      {messagesViews.map((messageView, i) => (
        <Card key={i} className="px-3 py-2 dashboard-message-card border-0 mb-2" onClick={e => handleOnClick(e, messageView.urlToChat)}>
          <Row>
            <Col className="avatar-wrapper pr-0">
              <Avatar fullName={messageView.authorName} />
            </Col>
            <Col className="mb-1 pt-1 pl-2">
              <div className="d-flex text-align-center">
                <h5 className="my-1 mr-2">{messageView.authorName}</h5>
                <p className="my-auto ml-auto mr-0 float-right text-light">{calculateDate(messageView.messageCreatedDate)}</p>
              </div>
              <div className="d-block">
                {getSubjectIcon(messageView.subject?.label)}
                <span className="mb-0 mt-2 ml-2 text-info">{messageView.journeyName}</span>
              </div>
              <div className="d-block">
                <LinesEllipsis text={messageView.messageValue} maxLine={3} className={'text-secondary mb-0 mt-2'} />
              </div>
            </Col>
          </Row>
        </Card>
      ))}
    </>
  );
};

export default DashboardMessages;
