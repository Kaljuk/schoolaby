import React from 'react';
import { Switch } from 'react-router-dom';
import Dashboard from 'app/dashboard/dashboard';
import ErrorBoundaryUpdateEntityRoute from '../shared/error/error-boundary-update-entity-route';

const Routes = ({ match }) => (
  <Switch>
    <ErrorBoundaryUpdateEntityRoute path={match.url} component={Dashboard} />
  </Switch>
);

export default Routes;
