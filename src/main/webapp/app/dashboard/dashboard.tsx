import React from 'react';
import { AUTHORITIES } from 'app/config/constants';
import { hasAnyAuthority } from 'app/shared/auth/private-route';
import { IRootState } from 'app/shared/reducers';
import { connect } from 'react-redux';
import DashboardOld from './student-dashboard';
import DashboardNew from './teacher-dashboard';
import { JourneyTemplateProvider } from '../shared/contexts/journey-template-context';
import { EntityDetailProvider } from 'app/shared/contexts/entity-detail-context';

export const Dashboard = ({ isStudent }: StateProps) =>
  isStudent ? (
    <DashboardOld />
  ) : (
    <EntityDetailProvider>
      <JourneyTemplateProvider>
        <DashboardNew />
      </JourneyTemplateProvider>
    </EntityDetailProvider>
  );

const mapStateToProps = ({ authentication }: IRootState) => ({
  isStudent: hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.STUDENT]),
});

type StateProps = ReturnType<typeof mapStateToProps>;
export default connect(mapStateToProps)(Dashboard);
