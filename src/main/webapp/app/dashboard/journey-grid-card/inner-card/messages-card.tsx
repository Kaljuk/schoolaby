import React from 'react';
import InnerCard, { MESSAGES } from 'app/dashboard/journey-grid-card/inner-card/inner-card';
import { useGetNotifications } from 'app/shared/services/notification-api';
import { getUnreadTeacherNotificationsCount } from 'app/shared/layout/header/notification/notification-util';
import { useHistory } from 'react-router-dom';
import { translate } from 'react-jhipster';

const MessagesCard = ({ journeyId }) => {
  const history = useHistory();
  const { data: messageNotifications } = useGetNotifications({ type: 'MESSAGE' }, false);
  const unreadCount = getUnreadTeacherNotificationsCount(messageNotifications ?? [], journeyId);

  const navigateToJourneyGeneralChat = () => history.push(`/chats?journeyId=${journeyId}`);

  return (
    <InnerCard
      type={MESSAGES}
      newCount={unreadCount}
      title={translate(unreadCount === 1 ? 'schoolabyApp.dashboard.grid.newMessage' : 'schoolabyApp.dashboard.grid.newMessages')}
      iconName={'chat'}
      onClick={navigateToJourneyGeneralChat}
    />
  );
};

export default MessagesCard;
