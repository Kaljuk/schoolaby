import React, { FC, useState } from 'react';
import InnerCard from 'app/dashboard/journey-list-card/inner-card/inner-card';
import { useGetAssignments } from 'app/shared/services/assignment-api';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import { translate } from 'react-jhipster';
import { DateTime } from 'luxon';
import InnerCardRow from 'app/dashboard/journey-list-card/inner-card/inner-card-row/inner-card-row';

interface ActiveTasksCardProps {
  journeyId: number;
  journeyStudentCount: number;
}

const ActiveTasksCard: FC<ActiveTasksCardProps> = ({ journeyId, journeyStudentCount }) => {
  const today = DateTime.local().toISODate();
  const [totalCount, setTotalCount] = useState<number>(0);
  const { data: activeAssignments } = useGetAssignments(
    [EntityState.IN_PROGRESS, EntityState.URGENT, EntityState.NOT_STARTED],
    journeyId,
    today,
    null,
    setTotalCount,
    2
  );

  return (
    <InnerCard
      title={translate('schoolabyApp.dashboard.tasks')}
      totalCount={totalCount}
      emptyText={translate('schoolabyApp.dashboard.noUpcomingTasks')}
      notificationCountLabel={translate('schoolabyApp.dashboard.upcomingTasksCount')}
    >
      {!!activeAssignments?.length &&
        activeAssignments.map((assignment, i) => (
          <InnerCardRow
            key={`active-${assignment.id}`}
            link={`/assignment/${assignment.id}?journeyId=${journeyId}`}
            title={assignment.title}
            date={assignment.deadline}
            pupilSection={
              <>
                <strong>{`${assignment.submissionsCount}`}</strong>/
                {assignment.students?.length || assignment.groups?.length || journeyStudentCount}
              </>
            }
            shouldHaveHr={i < activeAssignments.length - 1}
          />
        ))}
    </InnerCard>
  );
};

export default ActiveTasksCard;
