import React, { FC, useEffect, useState } from 'react';
import InnerCard from 'app/dashboard/journey-list-card/inner-card/inner-card';
import { translate } from 'react-jhipster';
import { useGetNotifications } from 'app/shared/services/notification-api';
import {
  filterUnreadTeacherNotificationsByJourneyId,
  getUniqueNotificationsByLink,
} from 'app/shared/layout/header/notification/notification-util';
import InnerCardRow from 'app/dashboard/journey-list-card/inner-card/inner-card-row/inner-card-row';
import { INotification } from 'app/shared/model/notification.model';

interface RecentMessagesCardProps {
  journeyId: number;
}

const QuestionsCard: FC<RecentMessagesCardProps> = ({ journeyId }) => {
  const { data: messageNotifications } = useGetNotifications({ type: 'MESSAGE' }, false);

  const [journeyMessages, setJourneyMessages] = useState<INotification[]>([]);
  const [unreadNotificationsCount, setUnreadNotificationsCount] = useState<number>(0);

  useEffect(() => {
    const fetchedMessages = messageNotifications ? getUniqueNotificationsByLink(messageNotifications) : [];
    setJourneyMessages(filterUnreadTeacherNotificationsByJourneyId(fetchedMessages, journeyId));
  }, [messageNotifications]);

  useEffect(() => {
    setUnreadNotificationsCount(journeyMessages.length);
  }, [journeyMessages]);

  return (
    <InnerCard
      title={translate('schoolabyApp.dashboard.questions')}
      emptyText={translate('schoolabyApp.dashboard.noQuestions')}
      notificationCountLabel={translate('schoolabyApp.dashboard.recentQuestionsCount')}
      totalCount={unreadNotificationsCount}
    >
      {!!journeyMessages.length &&
        journeyMessages.map((message, i) => (
          <InnerCardRow
            key={message.id}
            link={message.link}
            title={message.assignmentName || translate('schoolabyApp.chat.generalChat')}
            date={message.createdDate}
            pupilSection={message.creatorName || message.groupName}
            shouldHaveHr={journeyMessages.length > 1 && i === 0}
          />
        ))}
    </InnerCard>
  );
};

export default QuestionsCard;
