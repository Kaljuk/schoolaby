import React, { FC } from 'react';
import StudentRow from 'app/dashboard/journey-list-card/inner-card/students-card/student-row/student-row';
import InnerCard from 'app/dashboard/journey-list-card/inner-card/inner-card';
import { translate } from 'react-jhipster';
import { useGetJourneyStudents } from 'app/shared/services/journey-api';
import { orderBy } from 'lodash';

interface StudentsCardProps {
  journeyId: number;
}

const StudentsCard: FC<StudentsCardProps> = ({ journeyId }) => {
  const { data: journeyStudents } = useGetJourneyStudents(journeyId, true);

  return (
    <InnerCard
      title={translate('schoolabyApp.assignment.member.students')}
      emptyText={translate('schoolabyApp.dashboard.noStudents')}
      showNotificationCount={false}
      light
      scroll
    >
      {!!journeyStudents?.length &&
        orderBy(journeyStudents, ['pinnedDate', 'user.lastName', 'user.firstName'], ['asc']).map(student => (
          <StudentRow key={`${journeyId}-${student?.user?.id}`} journeyId={journeyId} journeyStudent={student} />
        ))}
    </InnerCard>
  );
};

export default StudentsCard;
