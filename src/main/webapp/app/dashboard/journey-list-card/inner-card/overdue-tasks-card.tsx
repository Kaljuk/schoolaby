import React, { FC, useState } from 'react';
import InnerCard from 'app/dashboard/journey-list-card/inner-card/inner-card';
import { useGetAssignments } from 'app/shared/services/assignment-api';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import InnerCardRow from 'app/dashboard/journey-list-card/inner-card/inner-card-row/inner-card-row';
import { translate } from 'react-jhipster';

interface OverdueTasksCardProps {
  journeyId: number;
  journeyStudentCount: number;
}

const OverdueTasksCard: FC<OverdueTasksCardProps> = ({ journeyId, journeyStudentCount }) => {
  const [totalCount, setTotalCount] = useState<number>(0);
  const { data: overdueAssignments } = useGetAssignments([EntityState.OVERDUE], journeyId, null, null, setTotalCount, 2);

  const getNotSubmittedText = (submissionCount, expectedSubmissionCount): string | JSX.Element => {
    const notSubmittedCount = expectedSubmissionCount - submissionCount;

    return (
      <>
        <strong>{`${notSubmittedCount}`}</strong>/{expectedSubmissionCount}
      </>
    );
  };

  return (
    <InnerCard
      title={translate('schoolabyApp.dashboard.teacher.overdueTasks')}
      totalCount={totalCount}
      emptyText={translate('schoolabyApp.dashboard.teacher.noOverdueTasks')}
      notificationCountLabel={translate('schoolabyApp.dashboard.overdueTasksCount')}
    >
      {!!overdueAssignments?.length &&
        overdueAssignments.map((assignment, i) => (
          <InnerCardRow
            key={`active-${assignment.id}`}
            link={`/assignment/${assignment.id}?journeyId=${journeyId}`}
            title={assignment.title}
            date={assignment.deadline}
            pupilSection={getNotSubmittedText(
              assignment.submissionsCount,
              assignment.students?.length || assignment.groups?.length || journeyStudentCount
            )}
            shouldHaveHr={i < overdueAssignments.length - 1}
          />
        ))}
    </InnerCard>
  );
};

export default OverdueTasksCard;
