--Delete data from tables--
TRUNCATE public.lti_app CASCADE;
TRUNCATE public.lti_config CASCADE;
TRUNCATE public.lti_resource CASCADE;
TRUNCATE public.submission_uploaded_files CASCADE;
TRUNCATE public.person_role CASCADE;
TRUNCATE public.message CASCADE;
TRUNCATE public.material_educational_alignments CASCADE;
TRUNCATE public.milestone_materials CASCADE;
TRUNCATE public.material CASCADE;
TRUNCATE public.chat_people CASCADE;
TRUNCATE public.chat CASCADE;
TRUNCATE public.submission_authors CASCADE;
TRUNCATE public.submission CASCADE;
TRUNCATE public.journey_teachers CASCADE;
TRUNCATE public.journey_students CASCADE;
TRUNCATE public.assignment_students CASCADE;
TRUNCATE public.assignment_educational_alignments CASCADE;
TRUNCATE public.assignment CASCADE;
TRUNCATE public.milestone CASCADE;
TRUNCATE public.journey CASCADE;
TRUNCATE public.jhi_user_authority CASCADE;
TRUNCATE public.jhi_user CASCADE;
TRUNCATE public.jhi_persistent_audit_event CASCADE;
TRUNCATE public."group" CASCADE;
TRUNCATE public.journey_state CASCADE;
TRUNCATE public.journey_signup_code CASCADE;

--
-- Data for Name: jhi_persistent_audit_event; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.jhi_persistent_audit_event (event_id, principal, event_date, event_type)
VALUES (1001, 'admin', '2020-07-01 09:42:25.974', 'AUTHENTICATION_SUCCESS');
INSERT INTO public.jhi_persistent_audit_event (event_id, principal, event_date, event_type)
VALUES (1002, 'admin', '2020-07-01 11:45:15.895', 'AUTHENTICATION_SUCCESS');

--
-- Data for Name: jhi_user; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
-- $2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e = passwordpassword
INSERT INTO public.jhi_user (id, login, password_hash, first_name, last_name, email, image_url, activated, lang_key,
                             activation_key, reset_key, created_by, created_date, reset_date, last_modified_by,
                             last_modified_date, deleted)
VALUES (1, 'system', '$2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e', 'System', 'System',
        'system@localhost', '', TRUE, 'en', NULL, NULL, 'system', NULL, NULL, 'system', NULL, NULL);
INSERT INTO public.jhi_user (id, login, password_hash, first_name, last_name, email, image_url, activated, lang_key,
                             activation_key, reset_key, created_by, created_date, reset_date, last_modified_by,
                             last_modified_date, deleted)
VALUES (2, 'anonymoususer', '$2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e', 'Anonymous', 'User',
        'anonymous@localhost', '', TRUE, 'en', NULL, NULL, 'system', NULL, NULL, 'system', NULL, NULL);
INSERT INTO public.jhi_user (id, login, password_hash, first_name, last_name, email, image_url, activated, lang_key,
                             activation_key, reset_key, created_by, created_date, reset_date, last_modified_by,
                             last_modified_date, deleted)
VALUES (3, 'admin', '$2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e', 'Administrator', 'Administrator',
        'admin@localhost', '', TRUE, 'en', NULL, NULL, 'system', NULL, NULL, 'system', NULL, NULL);
INSERT INTO public.jhi_user (id, login, password_hash, first_name, last_name, email, image_url, activated, lang_key,
                             activation_key, reset_key, created_by, created_date, reset_date, last_modified_by,
                             last_modified_date, deleted)
VALUES (4, 'user', '$2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e', 'User', 'User', 'user@localhost', '',
        TRUE, 'en', NULL, NULL, 'system', NULL, NULL, 'system', NULL, NULL);
INSERT INTO public.jhi_user (id, login, password_hash, first_name, last_name, email, phone_number, personal_code,
                             image_url, activated, lang_key,
                             activation_key, reset_key, created_by, created_date, reset_date, last_modified_by,
                             last_modified_date, deleted, country)
VALUES (5, 'student', '$2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e', 'student', 'student',
        'student@localhost.com', NULL, NULL, NULL,
        TRUE,
        'en', 'MXRqeG6S5juFHKAxLoLJ', NULL, 'anonymousUser', '2020-07-14 10:59:43.511113', NULL, 'anonymousUser',
        '2020-07-14 10:59:43.511113', NULL, 'Estonia');
INSERT INTO public.external_authentication(external_id, type, user_id, created_by, data)
VALUES ('7c925919-c91b-4c7f-9851-f8eac17b179e', 'HARID', 5, 'system', NULL);
INSERT INTO public.jhi_user (id, login, password_hash, first_name, last_name, email, phone_number, personal_code,
                             image_url, activated, lang_key,
                             activation_key, reset_key, created_by, created_date, reset_date, last_modified_by,
                             last_modified_date, deleted, country)
VALUES (6, 'teacher', '$2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e', 'teacher', 'teacher',
        'teacher@localhost.com', NULL, NULL, NULL,
        TRUE,
        'en', 'PbDVvGJbxpG9XVpTSQWL', NULL, 'anonymousUser', '2020-07-14 10:31:24.918622', NULL, 'anonymousUser',
        '2020-07-14 10:31:24.918622', NULL, 'Estonia');
INSERT INTO public.external_authentication(external_id, type, user_id, created_by, data)
VALUES ('1abe50dc-1067-40e2-86e6-86ab2d395a78', 'HARID', 6, 'system', NULL);
INSERT INTO public.jhi_user (id, login, password_hash, first_name, last_name, email, phone_number, personal_code,
                             image_url, activated, lang_key,
                             activation_key, reset_key, created_by, created_date, reset_date, last_modified_by,
                             last_modified_date, deleted)
VALUES (7, 'student2', '$2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e', 'student2', 'student2',
        'student2@localhost.com', NULL, NULL,
        NULL, TRUE,
        'en', 'PbDVvGJbxpG9XVpTSQWL', NULL, 'anonymousUser', '2020-07-14 10:31:24.918622', NULL, 'anonymousUser',
        '2020-07-14 10:31:24.918622', NULL);
INSERT INTO public.jhi_user (id, login, password_hash, first_name, last_name, email, phone_number, personal_code,
                             image_url, activated, lang_key,
                             activation_key, reset_key, created_by, created_date, reset_date, last_modified_by,
                             last_modified_date, deleted)
VALUES (8, 'student3', '$2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e', 'student3', 'student3',
        'student3@localhost.com', NULL, NULL,
        NULL, TRUE,
        'en', 'PbDVvGJbxpG9XVpTSQWL', NULL, 'anonymousUser', '2020-07-14 10:31:24.918622', NULL, 'anonymousUser',
        '2020-07-14 10:31:24.918622', NULL);
INSERT INTO public.jhi_user (id, login, password_hash, first_name, last_name, email, phone_number, personal_code,
                             image_url, activated, lang_key,
                             activation_key, reset_key, created_by, created_date, reset_date, last_modified_by,
                             last_modified_date, deleted)
VALUES (9, 'student4', '$2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e', 'student4', 'student4',
        'student4@localhost.com', NULL, NULL,
        NULL, TRUE,
        'en', 'PbDVvGJbxpG9XVpTSQWL', NULL, 'anonymousUser', '2020-07-14 10:31:24.918622', NULL, 'anonymousUser',
        '2020-07-14 10:31:24.918622', NULL);
INSERT INTO public.jhi_user (id, login, password_hash, first_name, last_name, email, phone_number, personal_code,
                             image_url, activated, lang_key,
                             activation_key, reset_key, created_by, created_date, reset_date, last_modified_by,
                             last_modified_date, deleted)
VALUES (10, 'parent', '$2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e', 'parent', 'parent',
        'parent@localhost.com', NULL, NULL, NULL,
        TRUE,
        'en', 'PbDVvGJbxpG9XVpTSQWL', NULL, 'anonymousUser', '2020-07-14 10:31:24.918622', NULL, 'anonymousUser',
        '2020-07-14 10:31:24.918622', NULL);
INSERT INTO public.jhi_user (id, login, password_hash, first_name, last_name, email, phone_number, personal_code,
                             image_url, activated, lang_key,
                             activation_key, reset_key, created_by, created_date, reset_date, last_modified_by,
                             last_modified_date, deleted, country)
VALUES (11, 'testcafe.student', '$2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e', 'Testcafe', 'Testcafe',
        'testcafe.student@netgroup.com', NULL, NULL, NULL,
        TRUE,
        'et', 'PbDVvGJbxpG9XVpTSQWL', NULL, 'anonymousUser', '2020-07-14 10:31:24.918622', NULL, 'anonymousUser',
        '2020-07-14 10:31:24.918622', NULL, 'Estonia');
INSERT INTO public.jhi_user (id, login, password_hash, first_name, last_name, email, phone_number, personal_code,
                             image_url, activated, lang_key,
                             activation_key, reset_key, created_by, created_date, reset_date, last_modified_by,
                             last_modified_date, deleted, country)
VALUES (12, 'testcafe.teacher', '$2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e', 'Testcafe', 'Testcafe',
        'testcafe.teacher@netgroup.com', NULL, NULL, NULL,
        TRUE,
        'et', 'PbDVvGJbxpG9XVpTSQWL', NULL, 'anonymousUser', '2020-07-14 10:31:24.918622', NULL, 'anonymousUser',
        '2020-07-14 10:31:24.918622', NULL, 'Estonia');

--
-- Data for Name: jhi_user_authority; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.jhi_user_authority (user_id, authority_name)
VALUES (1, 'ROLE_ADMIN');
INSERT INTO public.jhi_user_authority (user_id, authority_name)
VALUES (3, 'ROLE_ADMIN');
INSERT INTO public.jhi_user_authority (user_id, authority_name)
VALUES (4, 'ROLE_STUDENT');
INSERT INTO public.jhi_user_authority (user_id, authority_name)
VALUES (5, 'ROLE_STUDENT');
INSERT INTO public.jhi_user_authority (user_id, authority_name)
VALUES (6, 'ROLE_TEACHER');
INSERT INTO public.jhi_user_authority (user_id, authority_name)
VALUES (7, 'ROLE_STUDENT');
INSERT INTO public.jhi_user_authority (user_id, authority_name)
VALUES (8, 'ROLE_STUDENT');
INSERT INTO public.jhi_user_authority (user_id, authority_name)
VALUES (9, 'ROLE_STUDENT');
INSERT INTO public.jhi_user_authority (user_id, authority_name)
VALUES (10, 'ROLE_PARENT');
INSERT INTO public.jhi_user_authority (user_id, authority_name)
VALUES (11, 'ROLE_STUDENT');
INSERT INTO public.jhi_user_authority (user_id, authority_name)
VALUES (12, 'ROLE_TEACHER');

--
-- Data for Name: journey; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.journey (id, title, description, video_conference_url, sign_up_code, start_date, end_date, deleted,
                            creator_id, created_by, created_date, last_modified_by, last_modified_date, template,
                            educational_level_id, national_curriculum, image_url, subject_id, curriculum_id)
VALUES (7, 'Mükoloogia', 'Lähme seenele!', null, 'wldtkdEtdg1', '2020-12-03 22:00:00.000000', '2020-12-05 21:59:59.000000',
        null, 6, 'teacher', '2020-12-08 11:50:14.394107', 'teacher', '2020-12-08 11:51:38.294929', false, 1, true, null,
        10, 2);
INSERT INTO public.journey (id, title, description, video_conference_url, sign_up_code, start_date, end_date, deleted,
                            creator_id, created_by, created_date, last_modified_by, last_modified_date, template,
                            educational_level_id, national_curriculum, image_url, subject_id, curriculum_id)
VALUES (1, 'Matemaatika', 'Matemaatika 8. klassile', 'conference_url.ee/matemaatika', 'wldtkdEtdg2',
        '2021-09-15 15:30:52.385448', '2022-02-27 15:30:52.385448', null, 6, 'Tõnu Sepp', '2021-09-08 15:30:52.385448',
        'system', '2021-10-27 15:30:52.385448', false, 1, true, null, 8, 2);
INSERT INTO public.journey (id, title, description, video_conference_url, sign_up_code, start_date, end_date, deleted,
                            creator_id, created_by, created_date, last_modified_by, last_modified_date, template,
                            educational_level_id, national_curriculum, image_url, subject_id, curriculum_id)
VALUES (3, 'Keemia', 'Keemia 8. klassile', 'conference_url.ee/keemia', 'wldtkdEtdg3',
        '2021-12-08 15:30:52.385448', '2022-02-27 15:30:52.385448', null, 6, 'Peeter Paan',
        '2021-08-18 15:30:52.385448', 'system', '2021-10-27 15:30:52.385448', false, 1, true, null, 13,
        2);
INSERT INTO public.journey (id, title, description, video_conference_url, sign_up_code, start_date, end_date, deleted,
                            creator_id, created_by, created_date, last_modified_by, last_modified_date, template,
                            educational_level_id, national_curriculum, image_url, subject_id, curriculum_id)
VALUES (5, 'Muusika', 'Muusika 8. klassile', 'conference_url.ee/muusika', 'wldtkdEtdg4', '2021-12-08 15:30:52.385448',
        '2022-02-27 15:30:52.385448', null, 6, 'Jaan Karu', '2021-08-18 15:30:52.385448', 'system',
        '2021-10-27 15:30:52.385448', false, 1, true, null, 17, 2);
INSERT INTO public.journey (id, title, description, video_conference_url, sign_up_code, start_date, end_date, deleted,
                            creator_id, created_by, created_date, last_modified_by, last_modified_date, template,
                            educational_level_id, national_curriculum, image_url, subject_id, curriculum_id)
VALUES (4, 'Füüsika', 'Füüsika 8. klassile. Tutvume füüsika põhitõdedega ja omandame fundamentaalsed teadmised',
        'conference_url.ee/fuusika', 'XYKDHSC35KS92277D', '2021-12-08 15:30:52.385448', '2022-02-27 15:30:52.385448',
        null, 6, 'Imbi Noor', '2021-09-01 15:30:52.385448', 'system', '2021-10-27 15:30:52.385448', false, 1, true,
        null, 12, 2);
INSERT INTO public.journey (id, title, description, video_conference_url, sign_up_code, start_date, end_date, deleted,
                            creator_id, created_by, created_date, last_modified_by, last_modified_date, template,
                            educational_level_id, national_curriculum, image_url, subject_id, curriculum_id)
VALUES (2, 'Bioloogia', 'Bioloogia 8. klassile', 'conference_url.ee/bioloogia', 'wldtkdEtdg5',
        '2021-12-08 15:30:52.385448', '2022-02-27 15:30:52.385448', null, 6, 'Shrek', '2021-08-25 15:30:52.385448',
        'system', '2021-10-27 15:30:52.385448', false, 1, true, null, 10, 2);

--
-- Data for Name: milestone; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.milestone (id, title, description, end_date, deleted, journey_id, creator_id, created_by,
                              created_date, last_modified_by, last_modified_date)
VALUES (1, 'Koordinaatteljestik', 'Õpime tundma koordinaatteljestiku. Verstapost lõppeb kontrolltööga.',
        now() + INTERVAL '3 DAY', NULL, 1, 6,
        'teacher', now() - INTERVAL '1 DAY', 'teacher', now());
INSERT INTO public.milestone (id, title, description, end_date, deleted, journey_id, creator_id, created_by,
                              created_date, last_modified_by, last_modified_date)
VALUES (2, 'Lineaarvõrrandid', 'Õpime tundma lineaarvõrrandeid', now() - INTERVAL '13 DAY', NULL, 1, 6, 'teacher',
        now() - INTERVAL '1 DAY',
        'teacher', now());
INSERT INTO public.milestone (id, title, description, end_date, deleted, journey_id, creator_id, created_by,
                              created_date, last_modified_by, last_modified_date)
VALUES (3, 'Ruutvõrrandid',
        'Reaalarvuliste kordajatega ruutvõrrandil on reaalarvude hulgas alati kas kaks erinevat, kaks kokkulangevat või mitte ühtegi lahendit. Geomeetrilises tõlgenduses asuvad ruutvõrrandi lahendid kohtadel, kus ruutfunktsiooni y=ax^{2}+bx+c} graafik lõikab x-telge.',
        now() + INTERVAL '9 DAY', NULL, 1, 6, 'teacher', now() - INTERVAL '1 DAY', 'teacher', now());
INSERT INTO public.milestone (id, title, description, end_date, deleted, journey_id, creator_id, created_by,
                              created_date, last_modified_by, last_modified_date)
VALUES (4, 'Sagedustabel', 'Sagedustabeli kirjeldus', now() + INTERVAL '15 DAY', NULL, 1, 6, 'teacher',
        now() - INTERVAL '1 DAY', 'teacher', now());
INSERT INTO public.milestone (id, title, description, end_date, deleted, journey_id, creator_id, created_by,
                              created_date, last_modified_by, last_modified_date)
VALUES (5, 'Võrrandi süsteemid', 'Õpime mõisteid, valemeid ja kõike muud paremat!', now() - INTERVAL '6 DAY', NULL, 1,
        6, 'teacher',
        now() - INTERVAL '1 DAY', 'teacher', now());
INSERT INTO public.milestone (id, title, description, end_date, deleted, journey_id, creator_id, created_by,
                              created_date, last_modified_by, last_modified_date)
VALUES (6, 'Summa ja vahe kuup',
        'Kahe üksliikme summa ja samade üksliikmete vahe mittetäieliku ruudu korrutis võrdub nende üksliikmete kuupide summaga.',
        now() - INTERVAL '13 DAY', NULL, 1, 6, 'teacher', now() - INTERVAL '1 DAY', 'teacher', now());
INSERT INTO public.milestone (id, title, description, end_date, deleted, journey_id, creator_id, created_by,
                              created_date, last_modified_by, last_modified_date)
VALUES (7, 'Seente korjamine', 'Korjame seeni', '2020-12-06 21:59:59.000000', NULL, 7, 6, 'teacher',
        '2020-12-08 11:52:38.807486', 'teacher', '2020-12-08 11:53:15.693481');
UPDATE public.milestone SET published = NOW() - INTERVAL '1 DAY';


--
-- Data for Name: assignment; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.assignment(id, title, description, deadline, flexible_deadline, deleted,
                              grading_scheme_id, milestone_id, creator_id, created_by, created_date, last_modified_by,
                              last_modified_date)
VALUES (1, 'Lahendada koordinaatteljestiku ülesanne',
        'Ülesanne 123, lk 34. Tuleb ära lahendada antud ülesanne õpiku juhendite näitel.',
        now() - INTERVAL '3 DAY', FALSE, NULL, 1, 1, 6, 'teacher', now() - INTERVAL '10 DAY', 'admin', now());
INSERT INTO public.assignment(id, title, description, deadline, flexible_deadline, deleted,
                              grading_scheme_id, milestone_id, creator_id, created_by, created_date, last_modified_by,
                              last_modified_date)
VALUES (2, 'Lahendada koordinaatteljestiku ülesanne', 'Ülesanne 124, lk 35.', now() + INTERVAL '1 DAY', FALSE, NULL, 1,
        1, 6, 'teacher',
        now() - INTERVAL '10 DAY', 'admin', now());
INSERT INTO public.assignment(id, title, description, deadline, flexible_deadline, deleted,
                              grading_scheme_id, milestone_id, creator_id, created_by, created_date, last_modified_by,
                              last_modified_date)
VALUES (3, 'Lahendada koordinaatteljestiku ülesanne', 'Ülesanne 125-126, lk 35.', now() + INTERVAL '3 DAY', FALSE, NULL,
        1, 1, 6, 'teacher',
        now() - INTERVAL '10 DAY', 'admin', now());
INSERT INTO public.assignment(id, title, description, deadline, flexible_deadline, deleted,
                              grading_scheme_id, milestone_id, creator_id, created_by, created_date, last_modified_by,
                              last_modified_date)
VALUES (4, 'Lahendada koordinaatteljestiku ülesanne', 'Ülesanne 127-128, lk 35.', now() + INTERVAL '3 HOUR', FALSE,
        NULL, 1, 1, 6, 'teacher',
        now() - INTERVAL '10 DAY', 'admin', now());
INSERT INTO public.assignment(id, title, description, deadline, flexible_deadline, deleted,
                              grading_scheme_id, milestone_id, creator_id, created_by, created_date, last_modified_by,
                              last_modified_date)
VALUES (5, 'Lahendada koordinaatteljestiku ülesanne', 'Ülesanne 129, lk 36.', now() + INTERVAL '6 DAY', FALSE, NULL, 1,
        1, 6, 'teacher',
        now() - INTERVAL '10 DAY', 'admin', now());
INSERT INTO public.assignment(id, title, description, deadline, flexible_deadline, deleted,
                              grading_scheme_id, milestone_id, creator_id, created_by, created_date, last_modified_by,
                              last_modified_date)
VALUES (6, 'Lahendada ülesanne', 'Ülesanne 145, lk 50.', now() + INTERVAL '30 DAY', FALSE, NULL, 1, 2, 6, 'teacher',
        now() - INTERVAL '10 DAY',
        'admin', now());
INSERT INTO public.assignment(id, title, description, deadline, flexible_deadline, deleted,
                              grading_scheme_id, milestone_id, creator_id, created_by, created_date, last_modified_by,
                              last_modified_date)
VALUES (7, 'Lahendada ülesanne', 'Ülesanne 146, lk 50.', now() + INTERVAL '31 DAY', FALSE, NULL, 1, 2, 6, 'teacher',
        now() - INTERVAL '10 DAY',
        'admin', now());
INSERT INTO public.assignment(id, title, description, deadline, flexible_deadline, deleted,
                              grading_scheme_id, milestone_id, creator_id, created_by, created_date, last_modified_by,
                              last_modified_date)
VALUES (8, 'Lahendada ülesanne', 'Ülesanne 147-149, lk 50-51.', now() + INTERVAL '33 DAY', FALSE, NULL, 2, 5, 6,
        'teacher', now() - INTERVAL '10 DAY',
        'admin', now());
INSERT INTO public.assignment(id, title, description, deadline, flexible_deadline, deleted,
                              grading_scheme_id, milestone_id, creator_id, created_by, created_date, last_modified_by,
                              last_modified_date)
VALUES (9, 'Lahendada ülesanne', 'Ülesanne 145, lk 50.', now() + INTERVAL '35 DAY', FALSE, NULL, 5, 4, 6, 'teacher',
        now() - INTERVAL '10 DAY',
        'admin',
        now());
INSERT INTO public.assignment(id, title, description, deadline, flexible_deadline, deleted,
                              grading_scheme_id, milestone_id, creator_id, created_by, created_date, last_modified_by,
                              last_modified_date)
VALUES (10, 'Mõtle välja ise ülesanne', 'Peab mõtlema välja ise ülesande, mida hiljem kaasõpilasele lahendamiseks anda',
        now() + INTERVAL '39 DAY', FALSE, NULL, 2, 4, 6, 'teacher', now() - INTERVAL '10 DAY', 'admin', now());
INSERT INTO public.assignment(id, title, description, deadline, flexible_deadline, deleted,
                              grading_scheme_id, milestone_id, creator_id, created_by, created_date, last_modified_by,
                              last_modified_date)
VALUES (11, 'Lahendada ülesanne', 'Ülesanne 201, lk 83.', now() - INTERVAL '39 DAY', FALSE, NULL, 4, 6, 6, 'teacher',
        now() - INTERVAL '10 DAY',
        'admin', now());
INSERT INTO public.assignment(id, title, description, deadline, flexible_deadline, deleted,
                              grading_scheme_id, milestone_id, creator_id, created_by, created_date, last_modified_by,
                              last_modified_date)
VALUES (12, 'Lahendada ülesanne', 'Ülesanne 241, lk 92.', now() - INTERVAL '2 HOUR', FALSE, NULL, 3, 6, 6, 'teacher',
        now() - INTERVAL '10 DAY',
        'admin', now());
UPDATE public.assignment SET published = NOW() - INTERVAL '1 DAY';


--
-- Data for Name: assignment_educational_alignments; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.assignment_educational_alignments (educational_alignments_id, assignment_id)
VALUES (1063, 1),
       (3, 2),
       (3, 3),
       (3, 4),
       (3, 5),
       (3, 6),
       (3, 7),
       (3, 8),
       (3, 9),
       (3, 10),
       (3, 11),
       (3, 12);

--
-- Data for Name: milestone_educational_alignments; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.milestone_educational_alignments (educational_alignments_id, milestone_id)
VALUES (1063, 1),
       (1068, 2),
       (1072, 3),
       (982, 4),
       (1087, 5),
       (1074, 6),
       (1164, 7);

--
-- Data for Name: assignment_students; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.assignment_students (students_id, assignment_id)
VALUES (5, 1),
       (5, 2),
       (5, 3),
       (5, 4),
       (5, 5),
       (5, 6),
       (5, 7),
       (5, 8),
       (5, 9),
       (5, 10),
       (5, 11),
       (7, 1),
       (7, 2),
       (7, 3),
       (7, 4),
       (7, 5),
       (7, 6),
       (7, 7),
       (7, 8),
       (7, 9),
       (7, 11),
       (8, 1),
       (8, 2),
       (8, 3),
       (8, 4),
       (8, 5),
       (8, 6),
       (8, 7),
       (8, 8),
       (8, 9),
       (8, 11),
       (9, 1),
       (9, 2),
       (9, 3),
       (9, 4),
       (9, 5),
       (9, 6),
       (9, 7),
       (9, 8),
       (9, 9),
       (9, 11);

--
-- Data for Name: rubric; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.rubric (id, title, assignment_id, created_by, created_date, last_modified_by, last_modified_date, deleted, is_template) VALUES (1, 'Hindamismudel', 9, 'teacher', '2022-03-04 07:14:35.288065', 'teacher', '2022-03-04 07:14:53.766288', null, true);

--
-- Data for Name: criterion; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.criterion (id, title, description, sequence_number, rubric_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES (1, 'Vormistus', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 2, 1, 'teacher', '2022-03-04 07:14:35.296212', 'teacher', '2022-03-04 07:14:53.766510', null);
INSERT INTO public.criterion (id, title, description, sequence_number, rubric_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES (2, 'Turvalisus', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 0, 1, 'teacher', '2022-03-04 07:14:35.303107', 'teacher', '2022-03-04 07:14:53.766749', null);
INSERT INTO public.criterion (id, title, description, sequence_number, rubric_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES (3, 'Tulemuste analüüs', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 1, 1, 'teacher', '2022-03-04 07:14:35.303608', 'teacher', '2022-03-04 07:14:53.766986', null);

--
-- Data for Name: criterion_level; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.criterion_level (id, title, points, description, sequence_number, criterion_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES (1, 'Super', 5, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 0, 1, 'teacher', '2022-03-04 07:14:35.300825', 'teacher', '2022-03-04 07:14:53.768743', null);
INSERT INTO public.criterion_level (id, title, points, description, sequence_number, criterion_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES (2, 'Rahuldav', 3, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 2, 1, 'teacher', '2022-03-04 07:14:35.302158', 'teacher', '2022-03-04 07:14:53.768932', null);
INSERT INTO public.criterion_level (id, title, points, description, sequence_number, criterion_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES (3, 'Hea', 4, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 1, 1, 'teacher', '2022-03-04 07:14:35.302566', 'teacher', '2022-03-04 07:14:53.769309', null);
INSERT INTO public.criterion_level (id, title, points, description, sequence_number, criterion_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES (4, 'Halb', 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 4, 1, 'teacher', '2022-03-04 07:14:35.302702', 'teacher', '2022-03-04 07:14:53.769520', null);
INSERT INTO public.criterion_level (id, title, points, description, sequence_number, criterion_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES (5, 'Mitterahuldav', 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 3, 1, 'teacher', '2022-03-04 07:14:35.302954', 'teacher', '2022-03-04 07:14:53.769708', null);
INSERT INTO public.criterion_level (id, title, points, description, sequence_number, criterion_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES (6, 'Super', 5, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 0, 2, 'teacher', '2022-03-04 07:14:35.303253', 'teacher', '2022-03-04 07:14:53.768157', null);
INSERT INTO public.criterion_level (id, title, points, description, sequence_number, criterion_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES (7, 'Averaagne', 4, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 1, 2, 'teacher', '2022-03-04 07:14:35.303376', 'teacher', '2022-03-04 07:14:53.768350', null);
INSERT INTO public.criterion_level (id, title, points, description, sequence_number, criterion_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES (8, 'Horrible', 3, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 2, 2, 'teacher', '2022-03-04 07:14:35.303490', 'teacher', '2022-03-04 07:14:53.768550', null);
INSERT INTO public.criterion_level (id, title, points, description, sequence_number, criterion_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES (9, 'Averaagne', 3, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 2, 3, 'teacher', '2022-03-04 07:14:35.303731', 'teacher', '2022-03-04 07:14:53.767210', null);
INSERT INTO public.criterion_level (id, title, points, description, sequence_number, criterion_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES (10, 'Mitterahuldav', 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 3, 3, 'teacher', '2022-03-04 07:14:35.303847', 'teacher', '2022-03-04 07:14:53.767415', null);
INSERT INTO public.criterion_level (id, title, points, description, sequence_number, criterion_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES (11, 'OK', 4, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 1, 3, 'teacher', '2022-03-04 07:14:35.303970', 'teacher', '2022-03-04 07:14:53.767628', null);
INSERT INTO public.criterion_level (id, title, points, description, sequence_number, criterion_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES (12, 'Super', 5, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 0, 3, 'teacher', '2022-03-04 07:14:35.304252', 'teacher', '2022-03-04 07:14:53.767927', null);


--
-- Data for Name: journey_teachers; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.journey_teachers (user_id, journey_id, joined_date)
VALUES (6, 7, NOW()::timestamp);
INSERT INTO public.journey_teachers (user_id, journey_id, joined_date)
VALUES (6, 5, NOW()::timestamp);
INSERT INTO public.journey_teachers (user_id, journey_id, joined_date)
VALUES (6, 4, NOW()::timestamp);
INSERT INTO public.journey_teachers (user_id, journey_id, joined_date)
VALUES (6, 3, NOW()::timestamp);
INSERT INTO public.journey_teachers (user_id, journey_id, joined_date)
VALUES (6, 2, NOW()::timestamp);
INSERT INTO public.journey_teachers (user_id, journey_id, joined_date)
VALUES (6, 1, NOW()::timestamp);

--
-- Data for Name: journey_students; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (4, 3);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (4, 5);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (4, 4);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (4, 2);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (4, 1);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (5, 2);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (5, 3);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (5, 1);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (5, 5);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (5, 4);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (7, 3);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (7, 2);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (7, 1);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (7, 4);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (7, 5);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (8, 4);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (8, 2);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (8, 1);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (8, 3);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (9, 2);
INSERT INTO public.journey_students (user_id, journey_id)
VALUES (9, 1);

--
-- Data for Name: journey_state; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 1, 7, 'IN_PROGRESS', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 1, 4, 'IN_PROGRESS', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 1, 8, 'IN_PROGRESS', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 1, 6, 'IN_PROGRESS', 'ROLE_TEACHER');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 1, 9, 'IN_PROGRESS', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 1, 5, 'IN_PROGRESS', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 2, 7, 'NOT_STARTED', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 2, 8, 'NOT_STARTED', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 2, 6, 'NOT_STARTED', 'ROLE_TEACHER');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 2, 5, 'NOT_STARTED', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 2, 4, 'NOT_STARTED', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 2, 9, 'NOT_STARTED', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 3, 4, 'NOT_STARTED', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 3, 6, 'NOT_STARTED', 'ROLE_TEACHER');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 3, 5, 'NOT_STARTED', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 3, 7, 'NOT_STARTED', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 3, 8, 'NOT_STARTED', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 4, 4, 'NOT_STARTED', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 4, 8, 'NOT_STARTED', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 4, 6, 'NOT_STARTED', 'ROLE_TEACHER');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 4, 5, 'NOT_STARTED', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 4, 7, 'NOT_STARTED', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 5, 5, 'NOT_STARTED', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 5, 7, 'NOT_STARTED', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 5, 4, 'NOT_STARTED', 'ROLE_STUDENT');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 5, 6, 'NOT_STARTED', 'ROLE_TEACHER');
INSERT INTO public.journey_state (id, journey_id, user_id, state, authority_name)
VALUES (nextval('journey_state_seq'), 7, 6, 'COMPLETED', 'ROLE_TEACHER');

--
-- Data for Name: submission; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.submission (id, value, submitted_for_grading_date, deleted, assignment_id, created_by, created_date,
                               last_modified_by, last_modified_date)
VALUES (1, 'Hey this **editor** rocks 😀  123', localtimestamp, NULL, 1,
        NULL, NULL, NULL, NULL);
INSERT INTO public.submission (id, value, submitted_for_grading_date, deleted, assignment_id, created_by, created_date,
                               last_modified_by, last_modified_date)
VALUES (2, 'Hey this **editor** rocks 😀  12345', localtimestamp, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO public.submission (id, value, submitted_for_grading_date, deleted, assignment_id, created_by, created_date,
                               last_modified_by, last_modified_date)
VALUES (3, 'Hey this **editor** rocks 😀    1234567', localtimestamp, NULL, 3,
        NULL, NULL, NULL, NULL);
INSERT INTO public.submission (id, value, submitted_for_grading_date, deleted, assignment_id, created_by, created_date,
                               last_modified_by, last_modified_date)
VALUES (4, 'Hey this **editor** rocks 😀   1345789', localtimestamp, NULL, 6, NULL, NULL, NULL, NULL);
INSERT INTO public.submission (id, value, submitted_for_grading_date, deleted, assignment_id, created_by, created_date,
                               last_modified_by, last_modified_date)
VALUES (5, 'Hey this **editor** rocks 😀  123456789123', localtimestamp, NULL, 7, NULL, NULL, NULL, NULL);
INSERT INTO public.submission (id, value, submitted_for_grading_date, deleted, assignment_id, created_by, created_date,
                               last_modified_by, last_modified_date)
VALUES (6, 'Hey this **editor** rocks 😀  12345678913456789', localtimestamp, NULL, 8, NULL, NULL, NULL, NULL);
INSERT INTO public.submission (id, value, submitted_for_grading_date, deleted, assignment_id, created_by, created_date,
                               last_modified_by, last_modified_date)
VALUES (7, 'Hey this **editor** rocks 😀  13492234670987659856789', localtimestamp, NULL, NULL, NULL, NULL, NULL,
        NULL);
INSERT INTO public.submission (id, value, submitted_for_grading_date, deleted, assignment_id, created_by, created_date,
                               last_modified_by, last_modified_date)
VALUES (8, 'Hey this **editor** rocks 😀  098765323456780234567899876543', localtimestamp, NULL, 2, NULL, NULL,
        NULL,
        NULL);
INSERT INTO public.submission (id, value, submitted_for_grading_date, deleted, assignment_id, created_by, created_date,
                               last_modified_by, last_modified_date)
VALUES (9, 'Hey this **editor** rocks 😀', localtimestamp, NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO public.submission (id, value, submitted_for_grading_date, deleted, assignment_id, created_by, created_date,
                               last_modified_by, last_modified_date)
VALUES (10, 'Hey this **editor** rocks 😀', localtimestamp, NULL, 2, NULL, NULL,
        NULL, NULL);

-- 1-5
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (11, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (12, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (13, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (14, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (15, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (16, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (17, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (18, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (19, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (20, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (21, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (22, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (23, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (24, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (25, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (26, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (27, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (28, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (29, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (30, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (31, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (32, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (33, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (34, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (35, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (36, 'Hey this **editor** rocks 😀', null, 6, null, null, null, null, false, '2021-03-15 10:01:47.890798');

-- A-F
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (37, 'Hey this **editor** rocks 😀', null, 8, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (38, 'Hey this **editor** rocks 😀', null, 8, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (39, 'Hey this **editor** rocks 😀', null, 8, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (40, 'Hey this **editor** rocks 😀', null, 8, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (41, 'Hey this **editor** rocks 😀', null, 8, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (42, 'Hey this **editor** rocks 😀', null, 8, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (43, 'Hey this **editor** rocks 😀', null, 8, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (44, 'Hey this **editor** rocks 😀', null, 8, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (45, 'Hey this **editor** rocks 😀', null, 8, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (46, 'Hey this **editor** rocks 😀', null, 8, null, null, null, null, false, '2021-03-15 10:01:47.890798');

-- PASS-FAIL
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (47, 'Hey this **editor** rocks 😀', null, 12, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (48, 'Hey this **editor** rocks 😀', null, 12, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (49, 'Hey this **editor** rocks 😀', null, 12, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (50, 'Hey this **editor** rocks 😀', null, 12, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (51, 'Hey this **editor** rocks 😀', null, 12, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (52, 'Hey this **editor** rocks 😀', null, 12, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (53, 'Hey this **editor** rocks 😀', null, 12, null, null, null, null, false, '2021-03-15 10:01:47.890798');

-- NARRATIVE
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (54, 'Hey this **editor** rocks 😀', null, 11, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (55, 'Hey this **editor** rocks 😀', null, 11, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (56, 'Hey this **editor** rocks 😀', null, 11, null, null, null, null, false, '2021-03-15 10:01:47.890798');

-- 1-100%
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (57, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (58, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (59, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (60, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (61, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (62, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (63, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (64, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (65, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (66, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (67, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (68, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (69, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (70, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (71, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (72, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (73, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (74, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (75, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (76, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (77, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');
INSERT INTO public.submission (id, value, deleted, assignment_id, created_by, created_date, last_modified_by,
                               last_modified_date, resubmittable, submitted_for_grading_date)
VALUES (78, 'Hey this **editor** rocks 😀', null, 9, null, null, null, null, false, '2021-03-15 10:01:47.890798');

--
-- Data for Name: submission_authors; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.submission_authors (authors_id, submission_id)
VALUES (5, 1);
INSERT INTO public.submission_authors (authors_id, submission_id)
VALUES (5, 2);
INSERT INTO public.submission_authors (authors_id, submission_id)
VALUES (5, 3);
INSERT INTO public.submission_authors (authors_id, submission_id)
VALUES (5, 4);
INSERT INTO public.submission_authors (authors_id, submission_id)
VALUES (5, 5);
INSERT INTO public.submission_authors (authors_id, submission_id)
VALUES (5, 6);
INSERT INTO public.submission_authors (authors_id, submission_id)
VALUES (5, 7);
INSERT INTO public.submission_authors (authors_id, submission_id)
VALUES (7, 8);
INSERT INTO public.submission_authors (authors_id, submission_id)
VALUES (8, 9);
INSERT INTO public.submission_authors (authors_id, submission_id)
VALUES (9, 10);

--
-- Data for Name: material; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.material (id, title, type, description, external_id, url, deleted, created_by, created_date,
                             last_modified_by, last_modified_date)
VALUES (1, 'Ergonomic Frozen', 'Unbranded Cotton Pizza Buckinghamshire', 'Markets salmon', 'aggregate',
        'http://wilber.org', NULL, NULL, NOW(), NULL, NULL);
INSERT INTO public.material (id, title, type, description, external_id, url, deleted, created_by, created_date,
                             last_modified_by, last_modified_date)
VALUES (2, 'Focused multi-byte', 'Rustic Savings Account pixel', 'SAS', 'purple SCSI', 'http://estrella.net', NULL,
        NULL, NULL, NULL, NULL);
INSERT INTO public.material (id, title, type, description, external_id, url, deleted, created_by, created_date,
                             last_modified_by, last_modified_date)
VALUES (3, 'disintermediate transitional', 'Nevada', 'service-desk array', 'copy Licensed Granite Fish',
        'https://marcos.net', NULL, NULL, NOW(), NULL, NULL);
INSERT INTO public.material (id, title, type, description, external_id, url, deleted, created_by, created_date,
                             last_modified_by, last_modified_date)
VALUES (4, 'withdrawal Home Loan Account', 'panel Rhode Island', 'Estonia Naira', 'Ohio Checking Account',
        'https://candido.net', NULL, NULL, NOW(), NULL, NULL);
INSERT INTO public.material (id, title, type, description, external_id, url, deleted, created_by, created_date,
                             last_modified_by, last_modified_date)
VALUES (5, 'Brunei Dollar', 'Automotive', 'Refined engage', 'transmit Quality Orchestrator', 'https://cassandra.org',
        NULL, NULL, NOW(), NULL, NULL);
INSERT INTO public.material (id, title, type, description, external_id, url, deleted, created_by, created_date,
                             last_modified_by, last_modified_date)
VALUES (6, 'hardware Car IB', 'purple Music strategize', 'sky blue', 'white', 'http://kristofer.org', NULL, NULL, NOW(),
        NULL, NULL);
INSERT INTO public.material (id, title, type, description, external_id, url, deleted, created_by, created_date,
                             last_modified_by, last_modified_date)
VALUES (7, 'Savings Account transmitting Branding', 'Program invoice', 'redefine transition Chair', 'Accounts sky blue',
        'https://leonardo.org', NULL, NULL, NOW(), NULL, NULL);
INSERT INTO public.material (id, title, type, description, external_id, url, deleted, created_by, created_date,
                             last_modified_by, last_modified_date)
VALUES (8, 'Pennsylvania', 'cutting-edge', 'Future', 'Light', 'http://sabryna.info', NULL, NULL, NOW(), NULL, NULL);
INSERT INTO public.material (id, title, type, description, external_id, url, deleted, created_by, created_date,
                             last_modified_by, last_modified_date)
VALUES (9, 'heuristic Table Australian Dollar', 'Steel Sleek Fresh Chair', 'Stand-alone', 'Plaza capacitor',
        'https://adaline.net', NULL, NULL, NOW(), NULL, NULL);
INSERT INTO public.material (id, title, type, description, external_id, url, deleted, created_by, created_date,
                             last_modified_by, last_modified_date)
VALUES (10, 'deposit', 'quantifying quantify Intelligent Wooden Gloves', 'quantifying',
        'Jamaican Dollar Sausages back-end', 'https://blair.com', NULL, NULL, NOW(), NULL, NULL);

--
-- Data for Name: assignment_materials; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.assignment_materials(materials_id, assignment_id)
VALUES (1, 6);
INSERT INTO public.assignment_materials(materials_id, assignment_id)
VALUES (2, 6);
INSERT INTO public.assignment_materials(materials_id, assignment_id)
VALUES (3, 6);
INSERT INTO public.assignment_materials(materials_id, assignment_id)
VALUES (4, 6);
INSERT INTO public.assignment_materials(materials_id, assignment_id)
VALUES (5, 6);
INSERT INTO public.assignment_materials(materials_id, assignment_id)
VALUES (6, 6);
INSERT INTO public.assignment_materials(materials_id, assignment_id)
VALUES (7, 6);
INSERT INTO public.assignment_materials(materials_id, assignment_id)
VALUES (8, 6);
INSERT INTO public.assignment_materials(materials_id, assignment_id)
VALUES (9, 6);
INSERT INTO public.assignment_materials(materials_id, assignment_id)
VALUES (10, 6);

--
-- Data for Name: Chat; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.chat (id, title, deleted, submission_id, journey_id,
                         created_by, created_date, last_modified_by, last_modified_date)
VALUES (1, 'Solutions Cove', NULL, 5, 1, 'teacher', now() - INTERVAL '10 DAY', 'teacher', now());

INSERT INTO public.chat (id, title, deleted, submission_id, journey_id,
                         created_by, created_date, last_modified_by, last_modified_date)
VALUES (2, 'Solutions Cove 2', NULL, NULL, 1, 'teacher', now() - INTERVAL '10 DAY', 'teacher', now());

INSERT INTO public.chat_people (chat_id, person_id)
VALUES (1, 5),
       (1, 6),
       (2, 5),
       (2, 6);

--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.message (id, value, deleted, creator_id, chat_id,
                            created_by, created_date, last_modified_by, last_modified_date)
VALUES (1, 'Solutions Cove', NULL, 5, 1, NULL, now() - INTERVAL '10 DAY', NULL, NULL);
INSERT INTO public.message (id, value, deleted, creator_id, chat_id,
                            created_by, created_date, last_modified_by, last_modified_date)
VALUES (2, 'Global payment violet', NULL, 6, 1, NULL, now() - INTERVAL '31 DAY', NULL, NULL);
INSERT INTO public.message (id, value, deleted, creator_id, chat_id,
                            created_by, created_date, last_modified_by, last_modified_date)
VALUES (3, 'UAE Dirham International Quality-focused', NULL, 6, 1, NULL, now() - INTERVAL '1 DAY', NULL, NULL);
INSERT INTO public.message (id, value, deleted, creator_id, chat_id,
                            created_by, created_date, last_modified_by, last_modified_date)
VALUES (4, 'metrics FTP invoice', NULL, 5, 1, NULL, now() - INTERVAL '2 MONTH', NULL, NULL);
INSERT INTO public.message (id, value, deleted, creator_id, chat_id,
                            created_by, created_date, last_modified_by, last_modified_date)
VALUES (5, 'Fish Dam', NULL, 5, 1, NULL, NULL, NULL, NULL);
INSERT INTO public.message (id, value, deleted, creator_id, chat_id,
                            created_by, created_date, last_modified_by, last_modified_date)
VALUES (6, 'leverage executive card', NULL, 6, 2, NULL, NULL, NULL, NULL);
INSERT INTO public.message (id, value, deleted, creator_id, chat_id,
                            created_by, created_date, last_modified_by, last_modified_date)
VALUES (7, 'non-volatile Station', NULL, 5, 2, NULL, NULL, NULL, NULL);
INSERT INTO public.message (id, value, deleted, creator_id, chat_id,
                            created_by, created_date, last_modified_by, last_modified_date)
VALUES (8, 'plug-and-play', NULL, 6, 2, NULL, NULL, NULL, NULL);
INSERT INTO public.message (id, value, deleted, creator_id, chat_id,
                            created_by, created_date, last_modified_by, last_modified_date)
VALUES (9, 'Ethiopian Birr', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.message (id, value, deleted, creator_id, chat_id,
                            created_by, created_date, last_modified_by, last_modified_date)
VALUES (10, 'Outdoors cultivate Small Frozen Bacon', NULL, 6, 2, NULL, NULL, NULL, NULL);

--
-- Data for Name: uploaded_file; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.uploaded_file (id, original_name, type, extension, unique_name, deleted, created_by, created_date, last_modified_by, last_modified_date) VALUES (-1, 'pshg.svg', 'svg', 'svg', '123-223-444-555-66666.pshg.svg', null, 'teacher', '2021-12-20 09:59:04.914201', 'teacher', '2021-12-20 09:59:04.914201')
ON CONFLICT DO NOTHING;

--
-- Data for Name: school; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--

INSERT INTO public.school (id, ehis_id, reg_nr, name, created_by, created_date, last_modified_by, last_modified_date,
                           deleted, country, uploaded_file_id)
VALUES (-1, -1, 0, 'Test school', 'system', now()::timestamp, 'system', now()::timestamp, NULL, 'Estonia', -1)
ON CONFLICT DO NOTHING;

--
-- Data for Name: journey_schools; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO journey_schools
VALUES (1, -1);

--
-- Data for Name: person_role; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--

INSERT INTO public.person_role (id, role, active, start_date, end_date, grade, parallel, deleted, person_id, created_by,
                                created_date,
                                last_modified_by, last_modified_date, school_id)
VALUES (1, 'TEACHER', TRUE, '2020-07-01 05:22:08', NULL, NULL,
        NULL, NULL, 6, NULL, NULL, NULL, NULL, -1);
INSERT INTO public.person_role (id, role, active, start_date, end_date, grade, parallel, deleted, person_id, created_by,
                                created_date,
                                last_modified_by, last_modified_date, school_id)
VALUES (2, 'STUDENT', TRUE, '2020-06-30 07:52:03', NULL, '11', 'B', NULL, 5, NULL, NULL, NULL, NULL, -1);
INSERT INTO public.person_role (id, role, active, start_date, end_date, grade, parallel, deleted, person_id, created_by,
                                created_date,
                                last_modified_by, last_modified_date)
VALUES (3, 'STUDENT', TRUE, '2020-06-30 12:01:46', NULL, '11',
        'B', NULL, 7, NULL, NULL, NULL, NULL);
INSERT INTO public.person_role (id, role, active, start_date, end_date, grade, parallel, deleted, person_id, created_by,
                                created_date,
                                last_modified_by, last_modified_date)
VALUES (4, 'STUDENT', TRUE, '2020-06-30 20:01:12', NULL, '11', 'A', NULL, 8,
        NULL, NULL, NULL, NULL);
INSERT INTO public.person_role (id, role, active, start_date, end_date, grade, parallel, deleted, person_id, created_by,
                                created_date,
                                last_modified_by, last_modified_date)
VALUES (5, 'STUDENT', TRUE, '2020-06-30 09:59:41', NULL, '11', 'A', NULL, 9, NULL, NULL, NULL, NULL);
INSERT INTO public.person_role (id, role, active, start_date, end_date, grade, parallel, deleted, person_id, created_by,
                                created_date,
                                last_modified_by, last_modified_date)
VALUES (6, 'STUDENT', FALSE, '2020-06-30 23:13:35', '2020-06-30 20:27:30', 'Soft', 'Assimilated', NULL, NULL, NULL,
        NULL, NULL, NULL);
INSERT INTO public.person_role (id, role, active, start_date, end_date, grade, parallel, deleted, person_id, created_by,
                                created_date,
                                last_modified_by, last_modified_date)
VALUES (7, 'STUDENT', FALSE, '2020-06-30 19:21:26', '2020-06-30 19:40:22', 'parse', 'bluetooth', NULL, NULL, NULL,
        NULL, NULL, NULL);
INSERT INTO public.person_role (id, role, active, start_date, end_date, grade, parallel, deleted, person_id, created_by,
                                created_date,
                                last_modified_by, last_modified_date)
VALUES (8, 'STUDENT', TRUE, '2020-07-01 06:43:07', '2020-06-30 11:04:38', 'Accountability Home', 'firewall Shoes', NULL,
        NULL, NULL, NULL,
        NULL,
        NULL);
INSERT INTO public.person_role (id, role, active, start_date, end_date, grade, parallel, deleted, person_id, created_by,
                                created_date,
                                last_modified_by, last_modified_date)
VALUES (9, 'STUDENT', FALSE, '2020-06-30 15:42:53', '2020-07-01 01:50:25', 'e-services Automotive redundant',
        'Credit Card Account',
        NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.person_role (id, role, active, start_date, end_date, grade, parallel, deleted, person_id, created_by,
                                created_date,
                                last_modified_by, last_modified_date)
VALUES (10, 'STUDENT', TRUE, '2020-07-01 03:23:34', '2020-06-30 12:34:07', 'facilitate Riel Keyboard',
        'well-modulated auxiliary',
        NULL, NULL,
        NULL, NULL, NULL, NULL);
INSERT INTO public.person_role (id, role, active, start_date, end_date, grade, parallel, deleted, person_id, created_by,
                                created_date,
                                last_modified_by, last_modified_date, school_id)
VALUES (11, 'STUDENT', TRUE, '2020-07-01 05:22:08', NULL, NULL,
        NULL, NULL, 11, NULL, NULL, NULL, NULL, 500);
INSERT INTO public.person_role (id, role, active, start_date, end_date, grade, parallel, deleted, person_id, created_by,
                                created_date,
                                last_modified_by, last_modified_date, school_id)
VALUES (12, 'TEACHER', TRUE, '2020-07-01 05:22:08', NULL, NULL,
        NULL, NULL, 12, NULL, NULL, NULL, NULL, 500);

--
-- Data for Name: lti_app; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.lti_app(id, name, description, image_url, version, launch_url)
VALUES (1, 'Chemvantage',
        'ChemVantage is an Open Education Resource for teaching and learning college-level General Chemistry. Free quizzes, homework assignments and practice exams can be customized by the instructor. Scores are automatically returned to the LMS.',
        '/content/images/lti/chemvantage.png',
        'LTI-1p0',
        'https://dev-vantage-hrd.appspot.com/lti');

INSERT INTO public.lti_app(id, name, description, image_url, version, launch_url)
VALUES (2, 'Edpuzzle',
        'Easily create beautiful interactive video lessons with Edpuzzle and get data on who watched the video, how many questions they answered correctly and more!',
        '/content/images/lti/edpuzzle.svg',
        'LTI-1p0',
        'https://edpuzzle.com/lti/courses/launch');

INSERT INTO public.lti_app(id, name, description, image_url, version, launch_url, deep_linking_url,
                           login_initiation_url, client_id, jwks_url, redirect_host)
VALUES (3, 'Chemvantage (1.3)',
        'ChemVantage is an Open Education Resource for teaching and learning college-level General Chemistry. Free quizzes, homework assignments and practice exams can be customized by the instructor. Scores are automatically returned to the LMS.',
        '/content/images/lti/chemvantage.png',
        '1.3.0',
        'https://dev-vantage-hrd.appspot.com/lti/launch',
        'https://dev-vantage-hrd.appspot.com/lti/deeplinks',
        'https://dev-vantage-hrd.appspot.com/auth/token',
        '123123123',
        'https://dev-vantage-hrd.appspot.com/jwks',
        'dev-vantage-hrd.appspot.com');
INSERT INTO public.lti_app (id, name, description, version, launch_url, image_url, created_by, created_date,
                            last_modified_by, last_modified_date, deleted, deep_linking_url, login_initiation_url,
                            client_id, jwks_url, redirect_host, resubmittable)
VALUES (9, 'Quizzeria (1.3)',
        'Quizzeria',
        '1.3.0', 'https://localhost:8080/api/launch', '/content/images/lti/quizzeria.svg', NULL, NULL,
        NULL, NULL, NULL, 'https://localhost:8080/api/deeplink',
        'https://localhost:8080/api/oidclaunch', '123123123', 'https://localhost:8080/api/keys',
        'localhost', false);
INSERT INTO public.lti_app (id, name, description, version, launch_url, image_url, created_by, created_date,
                            last_modified_by, last_modified_date, deleted, deep_linking_url, login_initiation_url,
                            client_id, jwks_url, redirect_host, content_item_selection_enabled)
VALUES (10, 'Google Assignments', 'Google Assignments', 'LTI-1p0', 'https://assignments.google.com/lti/a',
        'https://blogs.swarthmore.edu/its/wp-content/uploads/2020/01/google_assignments_logo.png', null, null, null,
        null, null, null, null, null, null, null, true);

--
-- Data for Name: lti_config; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.lti_config(id, consumer_key, shared_secret, lti_app_id, journey_id, type)
VALUES (3, '123123123', '123123123', 2, NULL, 'PLATFORM');
INSERT INTO public.lti_config(id, consumer_key, shared_secret, lti_app_id, journey_id, type)
VALUES (2, '123123123', '123123123', 2, 2, 'JOURNEY');
INSERT INTO public.lti_config(id, consumer_key, shared_secret, lti_app_id, journey_id, type)
VALUES (1, '123123123', '123123123', 1, 1, 'JOURNEY');
INSERT INTO public.lti_config(id, consumer_key, shared_secret, lti_app_id, journey_id, deployment_id, type)
VALUES (0, NULL, NULL, 3, 1, '1', 'JOURNEY');
INSERT INTO public.lti_config(id, consumer_key, shared_secret, lti_app_id, journey_id, deployment_id, type)
VALUES (4, NULL, NULL, 9, NULL, '123123123', 'PLATFORM');
INSERT INTO public.lti_config (id, consumer_key, shared_secret, created_by, created_date, last_modified_by,
                               last_modified_date, deleted, lti_app_id, journey_id, deployment_id, type)
VALUES (1151, '123123123', '123123123', 'teacher', '2021-03-30 12:52:48.447216',
        'teacher', '2021-03-30 12:52:48.447216', null, 10, 1, null, 'JOURNEY');

--
-- Data for Name: lti_resource; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.lti_resource (id, lti_app_id, assignment_id, resource_link_id, title, sync_grade)
VALUES (1, 1, 6, 'a-b-c-d', 'Test LTI resource', TRUE);
INSERT INTO public.lti_resource (id, lti_app_id, assignment_id, resource_link_id, title)
VALUES (2, 1, 7, '1-2-3-4', 'Test LTI resource 2');
INSERT INTO public.lti_resource (id, lti_app_id, assignment_id, resource_link_id, title, sync_grade)
VALUES (3, 3, 6, '1-2-3-4', 'Test LTI 1.3 resource 1', true);
INSERT INTO public.lti_resource (id, lti_app_id, assignment_id, resource_link_id, title, sync_grade)
VALUES (4, 9, 6, '1-2-3-4', 'Test LTI 1.3 resource 2', true);

--
-- Data for Name: lti_launch; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.lti_launch (id, user_id, lti_resource_id, result, created_by, created_date, last_modified_by,
                               last_modified_date, deleted)
VALUES (-1, 5, 1, '0.1', '', '2020-11-09 10:30:36.000000', '', NULL, NULL);
INSERT INTO public.lti_launch (id, user_id, lti_resource_id, result, created_by, created_date, last_modified_by,
                               last_modified_date, deleted)
VALUES (-2, 5, 1, '0.5', '', '2020-11-09 10:30:37.000000', '', NULL, NULL);
INSERT INTO public.lti_launch (id, user_id, lti_resource_id, result, created_by, created_date, last_modified_by,
                               last_modified_date, deleted)
VALUES (-3, 5, 1, '', '', '2020-11-09 10:30:38.000000', '', NULL, NULL);
INSERT INTO public.lti_launch (id, user_id, lti_resource_id, result, created_by, created_date, last_modified_by,
                               last_modified_date, deleted)
VALUES (-4, 5, 3, '', '', '2021-02-7 10:32:36.000000', '', NULL, NULL);
INSERT INTO public.lti_launch (id, user_id, lti_resource_id, result, created_by, created_date, last_modified_by,
                               last_modified_date, deleted)
VALUES (-5, 5, 3, '', '', '2021-02-8 11:50:36.000000', '', NULL, NULL);
INSERT INTO public.lti_launch (id, user_id, lti_resource_id, result, created_by, created_date, last_modified_by,
                               last_modified_date, deleted)
VALUES (-6, 5, 4, '', '', '2021-02-8 11:50:36.000000', '', NULL, NULL);
INSERT INTO public.lti_launch (id, user_id, lti_resource_id, result, created_by, created_date, last_modified_by,
                               last_modified_date, deleted)
VALUES (-7, 5, 4, '', '', '2021-02-8 11:52:36.000000', '', NULL, NULL);

--
-- Data for Name: lti_line_item; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.lti_line_item(id, score_maximum, label, lti_resource_id, created_by, created_date, last_modified_by,
                                 last_modified_date)
VALUES (-1, 10, 'CV 1.3 result', 3, 'teacher', '2021-02-8 11:52:36.000000', 'teacher', '2021-02-8 11:52:36.000000'),
       (-2, 6, 'Quizzeria 1.3 result', 4, 'teacher', '2021-02-8 11:52:36.000000', 'teacher',
        '2021-02-8 11:52:36.000000');

--
-- Data for Name: lti_score; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.lti_score (id, line_item_id, timestamp, score_given, score_maximum, comment, activity_progress,
                              grading_progress, user_id,
                              created_by, created_date, last_modified_by, last_modified_date)
VALUES (-1, -1, '2021-02-8 11:49:36.000000', 8, 10, 'First attempt', 'Completed', 'FullyGraded', 5, 'student',
        '2021-02-8 11:50:36.000000', 'student', '2021-02-8 11:50:36.000000'),
       (-2, -2, '2021-02-8 11:51:00.000000', 8, 10, 'First attempt', 'Completed', 'FullyGraded', 5, 'student',
        '2021-02-8 11:51:00.000000', 'student', '2021-02-8 11:51:00.000000'),
       (-3, -2, '2021-02-8 11:56:00.000000', 7, 10, 'Second attempt', 'Completed', 'FullyGraded', 5, 'student',
        '2021-02-8 11:56:00.000000', 'student', '2021-02-8 11:56:00.000000');

--
-- Data for Name: notification; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.notification (id, message, type, link, recipient_id, read, journey_id, assignment_id, creator_id,
                                 created_by, created_date, last_modified_by, last_modified_date)
VALUES (1, '{GIVEN_FEEDBACK}', 'GENERAL', '/assignment/1?journeyId=1', 5, null, 1, 1, 6, 'teacher',
        '2021-01-26 10:30:36.000000', 'teacher', '2021-01-26 11:34:36.000000');
INSERT INTO public.notification (id, message, type, link, recipient_id, read, journey_id, assignment_id, creator_id,
                                 created_by, created_date, last_modified_by, last_modified_date)
VALUES (2, '{HAS_REJECTED}', 'GENERAL', '/assignment/2?journeyId=1', 5, null, 1, 2, 6, 'teacher',
        '2020-11-28 14:01:00.000000', 'teacher', '2020-11-28 16:59:00.000000');
INSERT INTO public.notification (id, message, type, link, recipient_id, read, journey_id, assignment_id, creator_id,
                                 created_by, created_date, last_modified_by, last_modified_date)
VALUES (3, '{JOINED_JOURNEY}', 'GENERAL', '/journey/1', 6, null, 1, 1, 5, 'student', '2021-11-12 10:30:36.000000',
        'student', '2021-11-12 11:34:36.000000');
INSERT INTO public.notification (id, message, type, link, recipient_id, read, journey_id, assignment_id, creator_id,
                                 created_by, created_date, last_modified_by, last_modified_date)
VALUES (4, '{SUBMITTED_SOLUTION}', 'GENERAL', '/assignment/1?journeyId=1&studentId=5', 6, null, 1, 1, 5, 'student',
        '2021-01-12 10:30:36.000000', 'student', '2021-01-26 11:34:36.000000');
INSERT INTO public.notification (id, message, type, link, recipient_id, read, journey_id, assignment_id, creator_id,
                                 created_by, created_date, last_modified_by, last_modified_date)
VALUES (5, '{RECEIVED_MESSAGE}', 'MESSAGE', '/chats?journeyId=1', 5, null, 1, 2, 6, 'teacher',
        '2021-01-13 12:15:48.656688', 'teacher', '2021-01-28 12:25:20.948953');
INSERT INTO public.notification (id, message, type, link, recipient_id, read, journey_id, assignment_id, creator_id,
                                 created_by, created_date, last_modified_by, last_modified_date)
VALUES (6, '{RECEIVED_MESSAGE}', 'MESSAGE', '/chats?journeyId=2', 5, null, 1, 2, 6, 'teacher',
        '2021-01-28 12:15:48.656688', 'teacher', '2021-01-28 12:25:20.948953');
INSERT INTO public.notification (id, message, type, link, recipient_id, read, journey_id, assignment_id, creator_id,
                                 created_by, created_date, last_modified_by, last_modified_date)
VALUES (7, '{RECEIVED_MESSAGE}', 'MESSAGE', '/chats?journeyId=3', 5, null, 1, 2, 6, 'teacher',
        '2020-11-12 10:30:36.000000', 'teacher', '2021-01-28 12:25:20.948953');
INSERT INTO public.notification (id, message, type, link, recipient_id, read, journey_id, assignment_id, creator_id,
                                 created_by, created_date, last_modified_by, last_modified_date)
VALUES (8, '{RECEIVED_MESSAGE}', 'MESSAGE', '/chats?journeyId=4', 5, null, 1, 2, 6, 'teacher',
        NOW() - interval '2 hour', 'teacher', '2021-01-28 12:25:20.948953');
INSERT INTO public.notification (id, message, type, link, recipient_id, read, journey_id, assignment_id, creator_id,
                                 created_by, created_date, last_modified_by, last_modified_date)
VALUES (9, '{RECEIVED_MESSAGE}', 'MESSAGE', '/chats?journeyId=5', 5, null, 1, 2, 6, 'teacher',
        NOW() - interval '2 year', 'teacher', '2021-01-28 12:25:20.948953');
INSERT INTO public.notification (id, message, type, link, recipient_id, read, journey_id, assignment_id, creator_id,
                                 created_by, created_date, last_modified_by, last_modified_date)
VALUES (10, '{RECEIVED_MESSAGE}', 'MESSAGE', '/chats?journeyId=5', 5, null, 1, 2, 6, 'teacher',
        NOW() - interval '2 year', 'teacher', '2021-01-28 12:25:20.948953');

--
-- Data for Name: journey_signup_code; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.journey_signup_code (sign_up_code, authority_name, journey_id) VALUES ('wldtkdEtdg1', 'ROLE_TEACHER', 7);
INSERT INTO public.journey_signup_code (sign_up_code, authority_name, journey_id) VALUES ('wldtkdEtdg2', 'ROLE_TEACHER', 1);
INSERT INTO public.journey_signup_code (sign_up_code, authority_name, journey_id) VALUES ('wldtkdEtdg3', 'ROLE_TEACHER', 3);
INSERT INTO public.journey_signup_code (sign_up_code, authority_name, journey_id) VALUES ('wldtkdEtdg4', 'ROLE_TEACHER', 5);
INSERT INTO public.journey_signup_code (sign_up_code, authority_name, journey_id) VALUES ('wldtkdEtdg5', 'ROLE_TEACHER', 4);
INSERT INTO public.journey_signup_code (sign_up_code, authority_name, journey_id) VALUES ('wldtkdEtdg6', 'ROLE_TEACHER', 2);
INSERT INTO public.journey_signup_code (sign_up_code, authority_name, journey_id) VALUES ('wldtkdEtdg7', 'ROLE_STUDENT', 7);
INSERT INTO public.journey_signup_code (sign_up_code, authority_name, journey_id) VALUES ('wldtkdEtdg8', 'ROLE_STUDENT', 1);
INSERT INTO public.journey_signup_code (sign_up_code, authority_name, journey_id) VALUES ('wldtkdEtdg9', 'ROLE_STUDENT', 3);
INSERT INTO public.journey_signup_code (sign_up_code, authority_name, journey_id) VALUES ('wldtkdEtdg10', 'ROLE_STUDENT', 5);
INSERT INTO public.journey_signup_code (sign_up_code, authority_name, journey_id) VALUES ('wldtkdEtdg11', 'ROLE_STUDENT', 4);
INSERT INTO public.journey_signup_code (sign_up_code, authority_name, journey_id) VALUES ('wldtkdEtdg12', 'ROLE_STUDENT', 2);
