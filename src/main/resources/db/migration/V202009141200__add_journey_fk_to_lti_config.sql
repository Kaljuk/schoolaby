ALTER TABLE public.lti_config
    ADD CONSTRAINT fk_lti_config_journey_id FOREIGN KEY (journey_id) REFERENCES public.journey (id);
