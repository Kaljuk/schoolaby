UPDATE public.assignment
SET requires_submission = false
WHERE requires_submission IS NULL;
