CREATE TABLE school
(
    id                 bigserial PRIMARY KEY,
    ehis_id            text UNIQUE,
    reg_nr             text UNIQUE,
    name               text UNIQUE,
    created_by         TEXT NOT NULL,
    created_date       TIMESTAMP,
    last_modified_by   TEXT,
    last_modified_date TIMESTAMP,
    deleted            TIMESTAMP
);

TRUNCATE TABLE person_role;

ALTER TABLE person_role
    DROP COLUMN provider_ehis_id,
    DROP COLUMN provider_reg_nr,
    DROP COLUMN provider_name,
    ADD COLUMN school_id BIGINT REFERENCES school;
