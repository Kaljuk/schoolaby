CREATE TABLE journey_teachers
(
    user_id  bigint NOT NULL
        CONSTRAINT fk_journey_teachers_user_id
            REFERENCES jhi_user,
    journey_id bigint NOT NULL
        CONSTRAINT fk_journey_teachers_journey_id
            REFERENCES journey,
    PRIMARY KEY (journey_id, user_id)
);

CREATE TABLE journey_students
(
    user_id  bigint NOT NULL
        CONSTRAINT fk_journey_students_user_id
            REFERENCES jhi_user,
    journey_id bigint NOT NULL
        CONSTRAINT fk_journey_students_journey_id
            REFERENCES journey,
    PRIMARY KEY (journey_id, user_id)
);

INSERT INTO journey_teachers SELECT journey_people.people_id AS user_id, journey_people.journey_id FROM journey_people INNER JOIN jhi_user_authority ON jhi_user_authority.user_id = journey_people.people_id WHERE jhi_user_authority.authority_name = 'ROLE_TEACHER';

INSERT INTO journey_students SELECT journey_people.people_id AS user_id, journey_people.journey_id FROM journey_people INNER JOIN jhi_user_authority ON jhi_user_authority.user_id = journey_people.people_id WHERE jhi_user_authority.authority_name = 'ROLE_STUDENT';


DROP TABLE journey_people;
