INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '45578', '80582909', '1+1 Tantsustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '16651', '80387314', '4/8 Dance Studio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2197', '80288989', 'A&E Keeltekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2247', '12575556', 'A&J Lutsari Judokool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43539', '80163165', 'Aavo Otsa Puhkpillikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1253', '75015835', 'Abja Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2465', '80177480', 'Agneta Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1212', '75002790', 'Ahtme Kunstide Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3391', '11975970', 'Ainula balleti ja iluvõimlemise stuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '32751', '80410634', 'Aitado Judokool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2353', '80334614', 'Aivi Auga spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6531', '80391084', 'Akrobaatikakool PartnerAkro', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2290', '80329814', 'Akrobaatikakool Twister', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2373', '80332785', 'Akvarellid Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1098', '75014936', 'Alatskivi Kunstide Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6411', '10693582', 'Alguskeskuse Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1219', '80205733', 'Alta Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '9591', '11177279', 'Alta Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '8411', '80376943', 'Alta võimlemise ja tantsu spordikool ', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2364', '80322441', 'Altia Judokool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2399', '80326781', 'Altus Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '23091', '12246166', 'Andrese Talu Ratsakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5611', '80238862', 'Anna Melody tantsu- ja iluvõimlemise spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1373', '10088007', 'Annatädi Keelekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1114', '75010424', 'Antsla Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38631', '80034094', 'Aplaus on paus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4451', '80359175', 'ARGO Ujumisklubi Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2388', '80378592', 'Arhitektuurikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3731', '80321540', 'Arma Ratsakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1365', '80267065', 'Armeenia Pühapäevakool MAŠTOTS', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43111', '80555864', 'Arne Lepla Galeriikool Abjas ', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1164', '10065101', 'Arsise Kellade Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2416', '80079820', 'Artemi Tepi Poksiklubi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1167', '75010789', 'Aruküla Huvialakeskus Pääsulind', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '17611', '14064096', 'Arvutikool ITKool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6611', '80310818', 'Aserbaidžaani huvikool Orhan', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1078', '80111995', 'Aserbaidžaani Pühapäevakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1170', '80021097', 'Athena Maja Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2145', '10589471', 'Atlasnet', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2251', '80314178', 'Atlasnet Huvikeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2093', '90014106', 'Audentese Huvikeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1241', '90014106', 'Audentese Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '34631', '80408732', 'Audru Judokool Aitado', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1376', '80157454', 'Avantage Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2382', '80142926', 'BC Kalev/Cramo Korvpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4171', '80243290', 'BC STAR korvpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1367', '10953764', 'Belgardi Õppekeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '18031', '80568401', 'Black and Brownie Tänavatantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4771', '80005046', 'Bramanise Käsipallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4711', '80243403', 'Bruno Ujumiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2080', '80272095', 'BUDOLINN JUDOKOOL', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5011', '12481298', 'Bumble Erahuvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2218', '80140821', 'Carolina tantsustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '33751', '80360706', 'Ciara Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6971', '80384842', 'Crause Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1368', '80065350', 'Cultor Huvialakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2076', '80223665', 'D.r.e.a.m. Studio tantsustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2201', '80309332', 'DanceAct Seminar', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '11951', '80202152', 'DanceAct Tantsustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '45499', '80387510', 'DanceAct Tartu Tantsustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '42471', '80421224', 'Danza Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '37371', '14051290', 'Digi´sCool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31511', '12771702', 'Digirobo huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31531', '80410090', 'Digirobolab huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44025', '14714565', 'Digiruum Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1331', '10259271', 'Disaini ja Kunsti Erakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1278', '80051098', 'DO Judokool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '10111', '12748637', 'Drive It Up kitarrikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '36951', '14497493', 'Dronootika Erahuvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1364', '80016669', 'Duo Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '45481', '80373146', 'E STuudio koori- ja tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5731', '80148142', 'Edu Valem Pühapäevakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1112', '80256920', 'Edu-line Haabersti Huvikeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1110', '11210737', 'Edulux', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2831', '12329745', 'Eelkooliealiste laste arenduskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2871', '80336642', 'Eesti Golfikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4751', '80360439', 'Eesti Laste Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2110', '80037537', 'Eesti Maaülikooli Spordiklubi Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44187', '80575111', 'Eesti Motospordi Akadeemia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2183', '80075242', 'Eesti Spordiselts Kalev Jahtklubi Purjespordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1313', '80080734', 'Eesti Spordiselts Põhjakotkas Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1161', '90006822', 'Eesti Tantsuagentuuri Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2478', '80032400', 'Eesti Vaimsete Puuetega Inimeste Spordiliidu ujumiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6491', '80377612', 'Eesti Veespordialade Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7893', '80308048', 'Eestürk pühapäevakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '32471', '80393203', 'Elamusspordialade Spordikool Airpark', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1358', '75008120', 'Elva Huviala- ja Koolituskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '22851', '80149466', 'Elva Kergejõustikukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1126', '75008189', 'Elva Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3311', '80041421', 'Elva Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44030', '80561043', 'Endla Koostöökogu', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '24091', '14185322', 'Endla Murd Laulustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4951', '12456403', 'English School Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2269', '80328039', 'Ensinar Keeltekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '19791', '80371221', 'Erahuvikool "Kratila"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '46877', '14799294', 'Erahuvikool „Happy Classes“', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6911', '80167047', 'Erahuvikool „Uisukool Talveunistus“', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1329', '80060223', 'Erahuvikool Akrobaatika Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '41653', '80555717', 'Erahuvikool Edusamm', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2351', '11648744', 'Erahuvikool Eterna', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1084', '90014359', 'Erahuvikool HuviTERA', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '19631', '10505735', 'ERAHUVIKOOL INTER LANGUAGE', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7091', '12717754', 'Erahuvikool IRKAS ', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31212', '12564392', 'ERAHUVIKOOL KontaktK OÜ', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '40512', '80552452', 'Erahuvikool Loomeviis', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2121', '80234692', 'Erahuvikool Loovustuba', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5051', '80202226', 'Erahuvikool Maadluskool Aberg', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '14135', '80239137', 'Erahuvikool Marina Tširkova Balletiakadeemia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7771', '80385296', 'Erahuvikool Minu Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '41272', '14703969', 'Erahuvikool Nutik', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2262', '12012267', 'Erahuvikool Optimum semper', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1061', '80277537', 'Erahuvikool Ring', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '39594', '80123350', 'Erahuvikool Tallinna Capoeira Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2421', '80330757', 'Erahuvikool Tantsumeka', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1124', '80121522', 'Erahuvikool Tantsustuudio TODES', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '12551', '80396549', 'Erahuvikool Teadmiskeskus Collegium Eruditionis ', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '30731', '12838122', 'ERAHUVIKOOL TÄHEKIIR', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1335', '80167079', 'Erakool Keeltemaja', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2186', '80058806', 'Erakunstikool Anne', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1245', '10491045', 'Erakunstikool Lasteakadeemia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1220', '80240356', 'Eramuusikakool Ardente', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1188', '80231707', 'Eramuusikakool GAMME', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5491', '80368286', 'Eramuusikakool Helivõlu', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '35151', '12995407', 'Eraspordikool SEPPS', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2275', '80005046', 'Erki Noole Kergejõustikukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2374', '80224854', 'Erkmaa Korvpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3151', '80187076', 'Error Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1075', '80222476', 'Esperanza Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1064', '10245984', 'Esteetika- ja tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '30451', '80278123', 'EV Arengu huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2185', '80043199', 'Evald Kree nimeline Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1370', '80179881', 'FACE Moe- ja Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '33151', '80340253', 'Fantaasia Erahuvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '46218', '80587195  ', 'FC Haabersti Jalgpallikool ', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2213', '80307505', 'FC HELIOSE jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2344', '80063629', 'FC Kotkas Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '10891', '80125359', 'FC Kuressaare Jalgpallikool ', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2410', '80155047', 'FC Tiigrid Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2259', '80059220', 'FCF Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3771', '80350777', 'FDS Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1154', '90006845', 'Fine 5 Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2072', '80233557', 'Fotokool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2420', '80341962', 'Fredi Vöörmani Tenniseakadeemia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '14028', '80291951', 'Free Flow Studio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '16591', '11130349', 'FreshStart Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2392', '80253092', 'FRIENDS Tantsu-ja Teatrikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '14391', '80389437', 'Fukuri Judokool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '46003', '80584699', 'Goal Diggers tantsustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31711', '90009973', 'Golden Dance Club', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2273', '80318348', 'Gruusia pühapäevakool Bagrati', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '45579', '11710839', 'Guitarium', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1235', '90009200', 'Gustav Adolfi Koolituskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7216', '12492617', 'Gustavi Maja Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2207', '80205561', 'Haaberst Sport Spordiklubi HAABERSTI SPORDIKOOL', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1171', '75012848', 'Haapsalu Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1101', '75012831', 'Haapsalu Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1165', '75012877', 'Haapsalu Noorte Huvikeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1191', '80241189', 'Haapsalu Purjespordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2245', '80048630', 'Haapsalu Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1334', '10265923', 'Hakerus Erakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '14256', '14028054', 'Hanna-Liina Võsa Muusikalikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '24291', '14201143', 'Happy Teen Club', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5131', '12565859', 'Haritlase keelekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2462', '80296316', 'Harju Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3711', '90011042', 'Harku valla huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43211', '80114226', 'HC Oskar Käsipallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3191', '80149041', 'HC Panter Hokikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43606', '80567318', 'HC Tabasalu', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4071', '80014074', 'HC Viimsi Käsipallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '19891', '80372738', 'HC Viking Hokikool ', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1094', '11456861', 'Helen Doron Early English', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2349', '11706217', 'Helen Doron Early English Ropka Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2214', '12621859', 'Helen Doron English Lasnamäe Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '42551', '11565596', 'Helen Doron English Narva Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '41217', '14705721', 'Helen Doron English Peetri Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2284', '80314929', 'Helios Võru Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5771', '12109251', 'Hiina Keele Huvikool "AiZhong"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2464', '80107284', 'Hiina Võitluskunstide keskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '33251', '80344437', 'Hiiumaa Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44676', '77001273', 'Hiiumaa Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2173', '80057178', 'Hillar Hanssoo Malekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '46465', '16021955', 'Hope Art Stuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1947', '80195130', 'Humanitaararenduse Keskus Prosha', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1073', '10418025', 'Huvialakool Tip Top', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6111', '90010213', 'Huvikeskus "Faehna"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6771', '80406785', 'Huvikeskus Iidam ja Aadam', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1217', '10516532', 'Huvikeskus Intellekt', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1226', '80246778', 'Huvikeskus Loomekoda', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31371', '80402505', 'Huvikool "JUST SPEAK IT"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '34431', '80263340', 'Huvikool "Mari"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '14472', '12850956', 'Huvikool "Värv Pintslil"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '17851', '10656977', 'Huvikool Akubens', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1146', '80177528', 'Huvikool Almari', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '10511', '80392592', 'Huvikool Avang', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2271', '80124012', 'Huvikool Berimba', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1263', '80019887', 'Huvikool Budokeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '16891', '80397744', 'Huvikool Capoeira Vadeia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1336', '80195590', 'Huvikool EAST+WEST', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '37331', '80551151', 'Huvikool Eesti Padeliakadeemia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '34651', '80074834', 'Huvikool Eesti Purjetamisakadeemia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '20731', '80406868', 'Huvikool Kevin Renno Võitlusspordi Akadeemia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38131', '14545588', 'Huvikool Kirjutuslaud ', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '14171', '80391569', 'Huvikool Kunstiring', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2291', '80202315', 'Huvikool Lapsepõlve Akadeemia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38031', '80405745', 'Huvikool Laulustuudio Laulupesa', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '45984', '80580419', 'Huvikool Mereklubi TK   ', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1321', '10178101', 'Huvikool Midrimaake', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5671', '12618254', 'Huvikool miniLABOR', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2208', '80276420', 'Huvikool ModeRato', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1337', '80231920', 'Huvikool Naba', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38531', '80070575', 'Huvikool Nissi Trollid', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6651', '80269348', 'Huvikool NOODIPUU', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7217', '80385540', 'Huvikool OGOGO', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43656', '80576719', 'Huvikool OLEN EDUKAS', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44480', '14656827', 'Huvikool OpenIQ', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '40193', '14565059', 'Huvikool Pingu’s English Center', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3811', '80264546', 'Huvikool Pääsupoeg', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6431', '90010829', 'Huvikool SATVeL', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '41731', '80564194', 'Huvikool Spordiklubi Aquamarine', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2101', '80066757', 'Huvikool Spordiklubi Garant', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2394', '80172264', 'Huvikool Spordiklubi Rim', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2337', '80331544', 'Huvikool Stuudio DAK', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4111', '80182771', 'Huvikool Tabasalu Ujumisklubi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5331', '80315611', 'Huvikool Tammeka Jalgpalliakadeemia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '30151', '12548163', 'Huvikool Tee', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5691', '12180579', 'Huvikool Tee Tulevikku', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '45776', '80318041', 'Huvikool Tulevikutähed', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44517', '80404013', 'Huvikool Vigor Sport Club', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6311', '80374200', 'Huvikool Väike Täht', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '37771', '80292057', 'Huvikool Värvimäng', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '34691', '80343033', 'Huvikool YAWARA', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5411', '80365678', 'Huvistuudio ÕHIN', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '47036', '90015063', 'Härmake', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1252', '75000822', 'Häädemeeste Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1330', '80109604', 'Ida Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38557', '14559641', 'Idekas Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '37675', '80416393', 'Ihaste Ratsakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1227', '75013693', 'Iisaku Kunstide Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1168', '75015427', 'Ilmatsalu Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2771', '80266410', 'Iluuisutamisklubi Staar', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '42771', '10863512', 'Impuls Ujumisklubi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4671', '80355898', 'Invaspordiklubi Parasport Invaspordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38814', '80381542', 'Irina Embrichi Vehklemiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2371', '80291371', 'Irina Kikkase VK Võimlemiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '32571', '80421017', 'iSmart Club Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2476', '80079470', 'IVL Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2209', '90010077', 'Jakob Westholmi Koolituskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1103', '80229646', 'Jakobi Mäe Kultuurikoda', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2115', '80125566', 'Jalgpalli Huvikool Vaprus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2912', '80100885', 'Jalgpalliklubi Ajax TLMK', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5251', '80350798', 'Jalgpalliklubi Tabasalu Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2390', '80340796', 'Jalgpallikool Augur', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5451', '80302287', 'Jalgpallikool FC Ararat', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '33313', '80427675', 'Jalgpallikool FC Tallinn', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2372', '80348214', 'Jalgpallikool Infonet', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38411', '80355065', 'Jalgpallikool Poseidon', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '32692', '80070598', 'Jalgpallikool Rakvere Tarvas', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '20571', '80247973', 'Jalgpallikool Saku Sporting', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2168', '80293625', 'Jalgpallikool Vändra Vaprus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '9655', '80327494', 'Jalgrattakool Peloton', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1128', '90004740', 'Jana Trink Erakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6851', '80548338', 'Janne Ristimetsa Tantsustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2345', '80231699', 'Jeti tali- ja jääspordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2387', '80079636', 'JEVGENI SPORDIKOOL', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1395', '80227255', 'JJ-Street Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '41152', '80553227', 'JK Vaprus Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '39971', '80559075', 'JK Vapruse Pisikesed Pallurid', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38271', '80552311', 'Judokool Dokyo', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2731', '80089801', 'Judokool NAISS', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '40591', '14678823', 'Juku ja Juta erahuvikool ', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1369', '80083141', 'Jumbo Huvialakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2261', '80037253', 'Just Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1286', '75025973', 'Jõelähtme Muusika- ja Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1291', '75003306', 'Jõgeva Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1228', '75010269', 'Jõgeva Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '41231', '77000973', 'Jõgeva Valla Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6511', '80325331', 'Jõhvi Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1095', '75002590', 'Jõhvi Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1097', '75002666', 'Jõhvi Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1091', '75002637', 'Jõhvi Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38071', '80221086', 'Järvamaa Ratsaspordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1201', '80249481', 'Järvamaa Saalihokiklubi Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1344', '80558420', 'Kaari Sillamaa Kaunite Kunstide Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '45516', '80210970', 'Kaarli Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1186', '80233623', 'Kabardiini Pühapäevakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1198', '75007847', 'Kadrina Kunstidekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2288', '80329398', 'Kadrioru Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2206', '80310186', 'Kaie Kõrbi Balletistuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3291', '80079872', 'Kaitsejõudude Spordiklubi huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1343', '75025396', 'Kaiu Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '14059', '90012768', 'Kalamaja Avatud Kooli huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2252', '80311688', 'Kalamaja Koolituskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2169', '80297681', 'Kalevi Jalgrattakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '20131', '80399140', 'Kalevi Piljardikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2188', '80302784', 'Kalevi Ujumiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2493', '80060826', 'Karate-Do klubi Nüke Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2212', '80133287', 'Karateklubi Jyoshinmon Shorinryu', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3371', '80318549', 'Karateklubi Osanago', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1394', '11352386', 'Karatekool NINTAI', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1312', '75006434', 'Karksi-Nuia Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1204', '75023253', 'Karksi-Nuia Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1254', '75004961', 'Karl Jüri Rammi nim Sindi Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2474', '80040433', 'Karl Lemani Poksiklubi Poksikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6051', '12703296', 'Keeleilu Erahuvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1266', '80209412', 'Keeltekool IN DOWN-TOWN', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1141', '75013115', 'Kehra Kunstidekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1318', '75021959', 'Kehtna Kunstide Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6231', '80313345', 'Keila Korvpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1277', '75014511', 'Keila Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1068', '80130805', 'Keila Swimclubi Indrek Sei Ujumiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7111', '10896977', 'Kersti Võlu koolituskeskuse Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4471', '90004639', 'Kihnu Pärimuskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1160', '75022214', 'Kiili Kunstide Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1232', '75003795', 'Kilingi-Nõmme Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1115', '75005204', 'Kiviõli Kunstide Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2365', '12202654', 'Klaveristuudio Peppi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1378', '80167576', 'Kloostrikunstide Kool Labora', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43417', '80116018', 'Klubi Top Tennis Team', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1207', '75029323', 'Koeru Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '22871', '80409542', 'Kohila Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1178', '75027867', 'Kohila Koolituskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3351', '80192278', 'Kohila Spordiklubi Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2400', '80106793', 'Kohila Võrkpalliklubi Võrkpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1296', '75006227', 'Kohtla-Järve Koolinoorte Loomemaja', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1131', '75002784', 'Kohtla-Järve Kunstide Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5511', '80330622', 'Koidu Keelte Keskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31051', '80395923', 'Kooli Tänava Koolituskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2254', '80319075', 'Koolituskeskus "Killuke"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1185', '80063204', 'Korea Pühapäevakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1393', '80174429', 'Korvpalliklubi Viimsi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43562', '80142004', 'Korvpallikool Newox', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2417', '80264440', 'Korvpallikool Tallinn', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '45482', '80582594', 'Kose Erahuvikool Kotzebue', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44477', '77001300', 'Kose Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2851', '80347096', 'Kose Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '25972', '14225553', 'Kriipsujuku Erahuvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '17311', '80399329', 'Kristina Koroljak Spordiklubi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '42091', '14471789', 'Kru Academy Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6871', '80290377', 'KSG Koolituskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6991', '80384670', 'K-Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2305', '80221005', 'Kukrumäe Ratsakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1234', '80151078', 'Kunstistuudio Galerii-Welt', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1306', '75024567', 'Kuressaare Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1305', '75023349', 'Kuressaare Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1237', '75025515', 'Kuusalu Kunstide Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1246', '75011234', 'Käina Huvi- ja Kultuurikeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1093', '75020612', 'Kärla Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4991', '80335128', 'Käsipalliklubi HC Tallinn Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '13994', '12717430', 'LA Klaverikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1082', '10784209', 'Laagri Huvialakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '17931', '80404355', 'Laagri Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3271', '10048261', 'Lagedi Ratsaspordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '27271', '14228126', 'Lahe Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1320', '80203349', 'Laine Mägi Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1120', '80188785', 'Lasnamäe Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1175', '75007563', 'Lasnamäe Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7307', '12905982', 'Laste Arengukeskus „Pääsuke“', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '36851', '80428462', 'Laste ja noorte õppekeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2211', '10050341', 'Laste Keeltekeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2470', '80342915', 'Laste Muusika ja Kunsti Arenduse Keskus TereMok', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '24071', '80311334', 'Laste Tervisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '17871', '80328683', 'Laste Õpi- ja Huviklubi Rada SN', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2113', '80288216', 'Laste õpi- ja huviklubi Väike Päike', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '14536', '12679066', 'Laste õue Loovuskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1149', '80017598', 'Lasteekraani Muusikastuudio Koorikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '8851', '80184304', 'Lastesport', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '39011', '80558354', 'Lastestuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '11831', '80388099', 'Lauatennise Huvikool Spinmaster', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3111', '80319690', 'Lauatennisekeskuse Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2891', '80100827', 'Lauatenniseklubi Kalev', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1218', '80226161', 'Laulu- ja tantsukool WAF', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2127', '80107982', 'Laulustuudio "Minimuusikal"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2407', '80063983', 'Leksi Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1353', '80082845', 'LEMMINKÄINEN', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1215', '80212638', 'Lezgini pühapäevakool "Sharvili"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '11851', '12522910', 'Let`s speak English Erahuvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1121', '80209091', 'Lets Dance Tantsustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43416', '14312080', 'LifeDance Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1169', '75022404', 'Lihula Muusika- ja Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '23971', '12279094', 'Liidia Kobzeva Keeltekool "Likool"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1408', '11739091', 'Liina Lille Klaveristuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '32832', '11026557', 'Lingua Bella', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '37971', '80553026', 'Lingua Bella Kids', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '17491', '80246436', 'Lingua Rustica Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31211', '12764056', 'LingvaKool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3751', '11041309', 'Logoservi huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '20331', '14090708', 'LOHE looduskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1275', '75023444', 'Loksa Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2263', '80295937', 'Loomingu Stuudio LADJA', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2205', '11664625', 'Loovarengu kool Bjarte', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '18051', '80339474', 'LOOVUSLABOR', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31971', '80416648', 'Lugemislusti Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44081', '80568329', 'LULU Tantsu- ja Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5111', '80350275', 'Lumeakadeemia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1130', '75012854', 'Läänemaa Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44360', '77001257', 'Lääneranna Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2281', '80163503', 'Lääne-Virumaa T. Ševtšenko nim. Ukraina Kultuuri Pühapäevakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1240', '75001655', 'M. Lüdigi nim. Vändra Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3031', '80115680', 'Maadlusklubi NELSON', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '39031', '80559000', 'Maardu Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1107', '75011984', 'Maardu Kunstide Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43713', '80555574', 'Maardu Linnameeskonna Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2199', '80133519', 'Maardu Ukraina Pühapäevakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7052', '80214689', 'Maavalla Pärandkäsitöökool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6731', '80564981', 'Macte Keeltekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2396', '75037966', 'Maidla Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1177', '80182021', 'Maire Eliste Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1371', '80114551', 'Maleakadeemia "Vabaettur"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2270', '80320492', 'Malekool Chess Continent', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '42231', '80545280', 'Malekool Kaksikodad', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2404', '80184379', 'Mambo tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4231', '80343091', 'Mamma Mia Laulustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5391', '80333543', 'Maret Ani Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2267', '80156182', 'Mart Poomi Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2107', '80256682', 'Martin Reimi Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '24991', '80342175', 'Massu Ratsaspordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38273', '80364070', 'M-DOJO Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '32191', '14090142', 'Meediakool Cinemer', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1272', '80199027', 'Meero Muusik', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '45176', '16125583', 'Mentaalse aritmeetika kool Soroboom', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1092', '80211679', 'Midrimaa Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2083', '80017581', 'Miikaeli Ühenduse Arengukeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2081', '80017581', 'Miikaeli Ühenduse Keeltekeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2155', '80296210', 'Mikata Judokool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '21391', '10356005', 'Miksikese Koduõpetaja', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2405', '80331774', 'MK Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '40936', '12875324', 'Moosimaja huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2381', '80146396', 'Mooste Rahvamuusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '30991', '80418475', 'MZIURI Rahvuskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4351', '80331538', 'Murzi Mängukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1067', '80227605', 'Musamari Koorikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1411', '10139549', 'Musamari Mudilaste Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1140', '75018414', 'Mustamäe Laste Loomingu Maja', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1287', '75027070', 'Mustvee Muusika- ja Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1289', '80095629', 'Muusika- ja Kunstikool Kaur', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2397', '80185060', 'Muusika- ja mängustuudio JO-LE-MI', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2350', '80294553', 'Muusikakoja Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2089', '80269354', 'Muusikakool ELEEGIA', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1233', '11129553', 'Muusikakool Rajaots', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3671', '80338434', 'Muusikakool Virumus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2469', '80344624', 'Muusika-mängutuba RütmiPall', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5151', '80361025', 'Muusikastuudio Laululinnuhaldjas', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '18851', '80373838', 'Mängides Targaks', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2340', '12659827', 'Mängukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '32831', '14339305', 'MÄNGUTEGU', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1172', '75022806', 'Märjamaa Muusika- ja Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2294', '80330029', 'Märt Agu Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2406', '80334353', 'MyDance Tantsuklubi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '29651', '11273563', 'MyFitness Sport', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2202', '80033947', 'Narva Iluuisutamiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1229', '75026684', 'Narva Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1174', '75024283', 'Narva Laste Loomemaja', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2200', '80128458', 'Narva linna Ukraina Kaaslaskonna Pühapäevakool „Zlagoda“', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1194', '75024337', 'Narva Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1108', '75009160', 'Narva Noorte Meremeeste klubi/NNMK/', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1192', '75024449', 'Narva Paemurru Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2176', '80189276', 'Narva Rahvuskultuuride keskuse Pühapäevakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1159', '75009177', 'Narva Spordikool "Energia"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '11351', '80371733', 'Narva Trans Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1338', '80180660', 'Narva Usbeki Pühapäeva kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1339', '80033315', 'Narva Vene pühapäeva kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1111', '75005506', 'Narva-Jõesuu Laste Muusika- ja Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '23891', '14149540', 'NeestiN Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2348', '80231995', 'Noblessneri Jahtklubi Purjetamiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1123', '80164402', 'Nukufilm Lastestuudio huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2389', '12244960', 'Nupula Erahuvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '22391', '14124645', 'Nutigeen Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44516', '80418110', 'Nutigeen Teaduskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '8731', '12922101', 'Nutikas Erahuvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '36310', '80370509', 'Nutivõsu Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2278', '80048819', 'Nõmme Kalju Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1182', '75016154', 'Nõmme Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '35951', '80347887', 'Nõmme Pärimuskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2282', '80296368', 'Nõmme Rattakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1292', '80140599', 'Nõmme Spordiklubi Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1299', '75008048', 'Nõo Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2276', '11504519', 'Nõo Ratsakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44386', '77001174', 'Nõo Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1129', '80144523', 'Näpustuudio Käsitöökool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '10491', '80395426', 'Olympic Iluuisutamiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '34571', '14343956', 'Oma Kool Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '29951', '80569576', 'Omamoodi Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2377', '12147796', 'Omanäoline Erahuvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '33331', '14257470', 'Open Mind Club', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2332', '80332383', 'Orca Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1096', '75020769', 'Orissaare Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2432', '80332638', 'Orissaare Sport Spordiklubi Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1184', '75027672', 'Otepää Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3871', '80065678', 'Ott Ahoneni Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '9391', '80390334', 'PAE HUVIKOOL', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2062', '80052940', 'Paide Karatekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2050', '80056546', 'Paide Korvpallikool Seitse', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1328', '75002502', 'Paide Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2454', '80339528', 'Paide Linnameeskonna Viktor Metsa Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43588', '80114568', 'Paide Maadluskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1346', '75002525', 'Paide Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2243', '80313701', 'Paide TaiPoksikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1354', '80173157', 'Paide Ujumisklubi Ujumiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1390', '80167429', 'Paide Võimlemiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1946', '80115177', 'Paide Võrkpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '23111', '80373086', 'Palamuse Laulustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1135', '75021178', 'Paldiski Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2082', '80171690', 'Palusalu Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43191', '10371453', 'Pariisi Tall', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '12271', '80086091', 'Par-Par pühapäevane huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2260', '80323022', 'Peeter Sirge Fotokool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1270', '75000393', 'Pernova Hariduskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '45596', '80569234', 'Pianoruum', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2375', '80134565', 'Pikakannu Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44203', '14830509', 'Pingu´s English Huvikool Pärnu', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43864', '14818201', 'Pingu´s English Huvikool Viimsi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4651', '80075118', 'Pirita Aerutamisklubi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2911', '80297899', 'Pisipesa Mängukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '46276', '80024919', 'PJK Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '39111', '14155284', 'Pleasant English Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6831', '80182742', 'PLMF Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1133', '80301885', 'PMG Koolituskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2360', '80032052', 'Poksikool Dünamo Tallinn', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38673', '80421951', 'Poksikool Kalev', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2104', '80268716', 'Polygoni Teatrikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '45376', '80394183', 'Porkuni huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '16411', '80021826', 'Practical Wing Tchun School', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1342', '75031834', 'Puka Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2249', '80260399', 'Puškini Instituudi vene keele kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '19151', '80160093', 'Põhja-Tallinna Jalgpalliklubi Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1099', '80019551', 'Põltsamaa Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1281', '75003097', 'Põltsamaa Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1284', '75009250', 'Põlva Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1255', '75009208', 'Põlva Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '24871', '80275260', 'Põlva Spordi- ja Tantsukool Meie Stuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1242', '75009196', 'Põlva Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1388', '75005630', 'Päri Spordihoone', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7191', '80098303', 'Pärnu Ingerisoomlaste Kultuuriseltsi erahuvikool Orvokki', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1392', '80058516', 'Pärnu Jahtklubi Purjespordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1166', '80108527', 'Pärnu Kesklinna Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5711', '80254708', 'Pärnu Korvpallikool Paulus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1260', '75000377', 'Pärnu Kunstide Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3791', '80314623', 'Pärnu Käsikellakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2198', '80002384', 'Pärnu Loovuskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1258', '75000495', 'Pärnu Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2193', '80116946', 'Pärnu Ooperistuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1256', '75000383', 'Pärnu Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1210', '80083879', 'Pärnu Spordiselts Kalev Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31191', '80241261', 'Pärnu Sulgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2412', '80090721', 'Pärnu Sõudeklubi Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2366', '80338807', 'Pärnu Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1268', '80217943', 'Pärnu Vanalinna Koolituskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4291', '12493640', 'Pärnu väikelastekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1285', '75001796', 'Pärnu-Jaagupi Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44339', '90010947', 'Püha Johannese Huvihariduskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '35131', '80432417', 'Pühapäevakool Alif', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1181', '80201468', 'Pühapäevakool Bilim', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6931', '80381884', 'Pühapäevakool NUR', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2266', '80099662', 'Pühapäevakool Zlata', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2315', '80268538', 'Raadio Laste Laulustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '29191', '14225271', 'Rae Eelkool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1122', '75026129', 'Rae Huvialakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38324', '14544652', 'Raekooli Erahuvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1383', '80189276', 'Rahvuskultuuride Keskuse(RKK) Huvialakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '40531', '12666000', 'Rakvere Joogakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2402', '80329843', 'Rakvere Loovuskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1282', '75025124', 'Rakvere Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1267', '75025087', 'Rakvere Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2105', '75035772', 'Rapla Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1288', '80213520', 'Rapla Korvpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1211', '75024225', 'Rapla Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '30711', '80372833', 'Rapla Tantsustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38751', '14468155', 'Rapla Treeningstuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3231', '80344038', 'Raplamaa Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2411', '80192386', 'Reaali Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1333', '80221666', 'Reaalkooli Täienduskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1216', '10683603', 'Rein Ottosoni Purjespordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44445', '80411993', 'Reinar Halliku Korvpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2085', '80154255', 'Rene Buschi Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '22213', '80204633', 'Rider Ratsaspordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '13829', '80089273', 'Riveta Klubi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '32011', '80421187', 'ROBOGO Robootikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '17671', '80338351', 'Robootikakool RoboKaru', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2098', '10651000', 'Rocca al Mare Kooli Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2751', '80160278', 'Rocca al Mare Kooli Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2075', '80212087', 'Rocca al Mare Uisukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44487', '80564343', 'Rock Korvpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7214', '12836703', 'Ropka Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '32631', '80404881', 'Round12 poksikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44191', '80557892', 'RubyAir Studio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1214', '75009438', 'Rudolf Tobiase nimeline Kärdla Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5351', '80264581', 'Rull-iluuisuklubi Fox', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1407', '80159370', 'RVK KOOLITUSKESKUS', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1104', '75009881', 'Räpina Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1279', '75035418', 'Räpina Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2078', '80249860', 'Rüütli Erahuvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1066', '90007786', 'SA Põltsamaa Sport Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5991', '80372922', 'Saalijalgpallikool FC COSMOS', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1307', '75024544', 'Saaremaa Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2343', '80331900', 'Saaremaa Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43665', '80570220', 'Saareobu Ratsakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38832', '80556071', 'SaareSära Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '46464', '80326589', 'Saku Korvpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1290', '75019796', 'Saku Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '14471', '80375694', 'Saku Sofi huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1074', '80036012', 'Sally Stuudio Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4091', '80313807', 'Samurai Judokool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1090', '80237354', 'Santose jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7071', '12281027', 'SATTARI ERAHUVIKOOL', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '19431', '80059728', 'Saue Jalgpalliklubi Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1325', '75027979', 'Saue Kultuuri- ja Huvikeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1070', '75014592', 'Saue Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '32051', '77000039', 'Saue Spordikeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6191', '11808679', 'SAVISALONG KERAAMIKAKOOL ', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31611', '12107000', 'Savisõber', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2138', '80273092', 'Sensei Judokool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2203', '80053134', 'SERGEI BOBROVI SPORDIKOOL', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4391', '80145051', 'Shaté Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1251', '75015373', 'Sillamäe Huvi- ja Noortekeskus Ulei', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1250', '75015367', 'Sillamäe Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '41352', '80343725', 'SJK DINA', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2114', '80041295', 'SK FC Levadia Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '24851', '80229656', 'SK Finess Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6131', '12711098', 'SK Indigo', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1230', '80149874', 'SK ProFit Võru Huvikeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5571', '12652251', 'Smart Kids Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38358', '12832510', 'Soroboom Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2495', '80215358', 'Spa Viimsi Tervis Ujumiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '36378', '80404094', 'SPARK Makerlabi Inseneeriakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2571', '80272741', 'Sparta Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1069', '80235964', 'Spordi ja Kultuurikool Humanitas', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2711', '80333678', 'Spordiklubi Aquaway Ujumiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '20351', '80389940', 'Spordiklubi Barra', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43517', '80570237', 'Spordiklubi BG2', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2463', '80007694', 'Spordiklubi Budo Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '40934', '80555315', 'Spordiklubi Byakko', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2046', '80172726', 'Spordiklubi CFC Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2191', '80163584', 'Spordiklubi Daigo', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4591', '80273686', 'Spordiklubi Everest.SK', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3131', '80022688', 'Spordiklubi HC Tallas Käsipallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2043', '80193059', 'Spordiklubi Hotsport Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4971', '80247192', 'Spordiklubi IMAN', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '20651', '80408057', 'Spordiklubi Kati Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31111', '80419888', 'Spordiklubi Kiirus Kergejõustikukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38671', '80423565', 'Spordiklubi Kontakt', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6371', '80357957', 'Spordiklubi Korrus3', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43687', '80256601', 'Spordiklubi Neemeco', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2124', '80287895', 'Spordiklubi Nord', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2092', '80269561', 'Spordiklubi OOKAMI Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7051', '80374246', 'Spordiklubi Pole Style Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3071', '80160887', 'Spordiklubi Raesport Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1125', '80092565', 'Spordiklubi Reval Sport Käsipallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7971', '80028277', 'Spordiklubi Rullest', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1379', '80199180', 'Spordiklubi SharK Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '18811', '80402497', 'Spordiklubi Tanuki', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2379', '80273396', 'Spordiklubi Tats Võrkpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2395', '80234120', 'Spordikool "Estonian Academy of Kickboxing"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3451', '80255642', 'Spordikool Akvalang', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2461', '80148923', 'Spordikool Bushinkan', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2451', '80182104', 'Spordikool EDU-DO', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3011', '80242898', 'Spordikool Fortis', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1269', '80132187', 'Spordikool Fortuna', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2363', '80339050', 'Spordikool Hüpe', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3051', '80107858', 'Spordikool Impact', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '30971', '80268389', 'Spordikool Juna', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2471', '80151612', 'Spordikool KIK-DŽEB', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2091', '80081061', 'Spordikool Kõmgan', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2338', '80242533', 'Spordikool Lindon', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5791', '80361456', 'Spordikool Lääne-Virumaa Kalev', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2409', '80174122', 'Spordikool Meduus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2408', '80131839', 'Spordikool Meritäht', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2356', '80085766', 'Spordikool Sintai-S', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2398', '80143802', 'Spordikool Trefoil', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4631', '80165666', 'Spordiühing Ekstreempark', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3251', '80011130', 'SRD Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1079', '80246169', 'Stassi tantsustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '26971', '80388308', 'STEP UP DANCE STUDIO Tantsustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6171', '80222341', 'STORMI JÄÄPALLIKOOL', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '10371', '80384730', 'Studio Happy Dancers', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2097', '11559868', 'Studio Melos', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3851', '80331768', 'Stuudio Maru Karu', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1197', '80081596', 'Sulgpallikool Triiton', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '14931', '80205609', 'SunStar Tantsustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5311', '11179261', 'Suuresti Ratsakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44385', '14308003', 'Suusi Stuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '13131', '80006152', 'Sõudmise ja Aerutamise klubi „Tartu“ Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7031', '80331194', 'Tabasalu Koolituskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1942', '75014178', 'Tabasalu Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7011', '80385066', 'Tabasalu Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2336', '75037535', 'Tabivere Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2422', '80040717', 'TAIFU Spordiklubi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '37573', '80316295', 'Taisi Laululapsed', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2931', '80044916', 'Talkur', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '19451', '80405811', 'TALLINK TENNISE JA SULGPALLIKOOL', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1363', '10680734', 'Tallinn Language Centre YL', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1295', '80014743', 'Tallinna Armeenia Rahvussseltsi Pühapäevakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '34551', '80396940', 'Tallinna Filmikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1148', '75016486', 'Tallinna Huvikeskus "Kullo"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2071', '80141387', 'Tallinna Iluuisutamiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2475', '80064793', 'Tallinna Invaspordiühing invaspordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1225', '80097657', 'Tallinna Jahtklubi Purjetamiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '33211', '80428999', 'Tallinna Jalgpallikool LEGION', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2459', '80060677', 'Tallinna Judokool Kibuvits', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2453', '80314468', 'Tallinna Kalevi Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1156', '75017828', 'Tallinna Kanutiaia Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1362', '10991233', 'Tallinna Kaunite Kunstide Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '19911', '80396265', 'Tallinna Kiiruisuklubi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1143', '75017403', 'Tallinna Kopli Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '29971', '80391919', 'TALLINNA KSK FC ŠTROMMI', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2951', '80044419', 'Tallinna Kunstigümnaasiumi Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1176', '75016177', 'Tallinna Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '41833', '80206589', 'Tallinna Käsipalliakadeemia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4131', '80354947', 'Tallinna läti rahvuskool Taurenis', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1179', '75016160', 'Tallinna Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2385', '80052904', 'Tallinna Mõõk Vehklemiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6291', '80192404', 'Tallinna Noorte Malekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1183', '75016668', 'Tallinna Nõmme Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2324', '80226333', 'Tallinna Poksikool Sofron', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1231', '80261507', 'Tallinna Purjelauakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1404', '80070368', 'Tallinna Raskejõustiku Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2418', '90010887', 'Tallinna Ratsaspordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44171', '80345167', 'Tallinna Roma Kultuurikeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3431', '80091531', 'Tallinna Spordiklubi Jõud Kergejõustikukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2255', '80173430', 'Tallinna Spordikool Altius/Vars', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2457', '80067254', 'Tallinna Spordikool Dvigatel', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1405', '80031960', 'Tallinna Spordiselts Kalev K.Palusalu Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1351', '80031960', 'Tallinna Spordiselts Kalev Kergejõustikukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1406', '80031960', 'Tallinna Spordiselts Kalev Sportmängude Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1355', '80031960', 'Tallinna Spordiselts Kalev Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1063', '80246318', 'Tallinna Sulgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2691', '80004615', 'Tallinna Sõudeklubi Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1080', '80246169', 'Tallinna Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '24191', '77000016', 'Tallinna Tondiraba Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2116', '11126796', 'Tallinna Vanalinna kunstikeskuse kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '36911', '80333402', 'Tallinna vene keele kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1193', '80221270', 'Tallinna Vene Kultuuri Pühapäevakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2182', '80297853', 'Tallinna Venus Uisukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '19931', '80341873', 'Tallinna Vibukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6811', '80378988', 'Tallinna Võrkpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2651', '80321764', 'Tallinna Ülikooli Võimlemiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4051', '80213359', 'Tantsu- ja Moekool Max Moda', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '37551', '80551866', 'Tantsu- ja Spordiklubi MM13', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '40391', '80586770', 'Tantsu- ja vabaajakeskus Stuudio dotE', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2292', '80329211', 'Tantsu- ja võimlemiskool Arte Movimento', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '8071', '11967633', 'TantsuGeen Tantsustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6891', '80356627', 'Tantsuklubi Koit', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1142', '80226574', 'Tantsukool "The Way To Dance Agency"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4151', '80230911', 'Tantsukool Danceland', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2347', '80127938', 'Tantsukool Laguun', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2339', '80097628', 'Tantsukool Leevi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '37491', '12415350', 'Tantsukool Maerobic', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2265', '80314965', 'Tantsukool NEWSTART', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1147', '80201244', 'Tantsukool Noor Ballett', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '19751', '80358371', 'Tantsukool Prestige', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31811', '80418794', 'Tantsukool Respect', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1139', '80217664', 'Tantsukool SEMIIR', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1119', '80112196', 'Tantsukool Tango', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2334', '80196081', 'Tantsukool Twist', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1261', '80159482', 'Tantsukool WAF DANCE', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '13371', '80398347', 'Tantsustuudio Cestants', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2151', '80151302', 'Tantsustuudio KREEDO DANCE', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5973', '80272379', 'Tantsuteater Polly', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1244', '80103381', 'Tantsuteater Rada', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1221', '74019520', 'Tapa Muusika- ja Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '36321', '75033477', 'Tapa valla Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '32511', '14329590', 'Tarkuseterake', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1317', '75006575', 'Tartu I Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1319', '75006581', 'Tartu II Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1187', '80253407', 'Tartu Iluuisutamiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '42491', '80304607', 'Tartu Kalev Jalgpalliakadeemia Jalgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6551', '80379048', 'Tartu Katoliku Spordiklubi spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1117', '75006598', 'Tartu Lastekunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1304', '90007247', 'Tartu Loodusmaja', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1127', '80207614', 'Tartu Palliklubi Korvpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '35993', '80335482', 'Tartu Peetri Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1314', '80077488', 'Tartu Puhkpillistuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31451', '14197779', 'Tartu Robootika Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4691', '80081395', 'Tartu Spordiklubi Kajakas Hokikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1190', '80068087', 'Tartu Spordiseltsi Kalev Kergejõustikukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1189', '80068087', 'Tartu Spordiseltsi Kalev Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2111', '80093530', 'Tartu Suusaklubi Suusakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2164', '80155277', 'Tartu Ujumiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5531', '80078364', 'Tartu Waldorfgümnaasiumi Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1106', '75012305', 'Tartu Valla Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1116', '75033537', 'Tartu Valla Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1238', '80046097', 'Tartu võimlemisklubi „ Rütmika" Võimlemiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1202', '80072321', 'Tartu Ülikooli Akadeemilise Spordiklubi Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1206', '75012200', 'Tarvastu Muusika- ja Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4331', '80570349', 'Tasapisi Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2230', '80296115', 'Tatari pühapäevakool Dulkõn', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6631', '80363121', 'Tatiana Ilvese VK Võimlemisklubi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5371', '12609858', 'Teele ja Taavi Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '33471', '90012604', 'Tehnoloogiakool INNOKAS', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2358', '80150713', 'Tennisekool Altius-KT', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44456', '12930388', 'Tervisesõbra Erahuvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '32693', '80312896', 'Tiina Huvikeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1195', '80007329', 'Tiit Soku Korvpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2376', '80329783', 'Tiiu Valgemäe Uisukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2189', '80288067', 'TIP-TOP KLUB', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38111', '80325727', 'Tirel Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6011', '80319365', 'TIRTS&PÕNN HUVIKOOL', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1271', '75001537', 'Toila Muusika- ja Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2791', '80000907', 'Tondi Ratsakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2090', '80272801', 'Tondiraba Sulgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1366', '80122680', 'Tondiraba Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2049', '80261045', 'TONI JUDOKOOL', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '18131', '80404622', 'Toni Judokool EST', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2415', '80202195', 'Toomas Suursoo Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44876', '80581181', 'Top One Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1345', '80080869', 'TOPi Ujumiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4031', '12268541', 'Tosca Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '28091', '12404004', 'Triibuvurru Tantsustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7272', '12541008', 'Triin-Maret Laulu Klaveristuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5031', '80349099', 'TSA KALEV huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2328', '80222080', 'Tsirkusestuudio Folie', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '40933', '80329719  ', 'TSKA ERAHUVIKOOL', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '32111', '80407425', 'TTÜ Korvpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3911', '80000675', 'Tuulemaa Vabade Kunstide Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2611', '80132454', 'Tõnu Truusi Malekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31791', '14322434', 'Tõrukese Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1173', '75005788', 'Tõrva Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5431', '80144380', 'Tähe Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2455', '12254941', 'Tähetarga ettevalmistav huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2194', '80299318', 'Tähtvere Tantsukeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1203', '80262725', 'Tähtvere Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1280', '75006316', 'Türi Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2190', '80049061', 'Türi Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3891', '80103837', 'Uisukool Reval', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '44478', '80580218', 'Ujume koos Ujumiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1349', '80200598', 'Ujumise Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2256', '80000379', 'Ujumiskool Briis', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1297', '80096043', 'Ukraina Kaasmaalaste Kogukonna Pühapäevakool Vodograi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1381', '80127690', 'Ukraina kaasmaalaste pühapäevakool Nadija', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2195', '90009967', 'Ukraina kasakate kultuuri pühapäevakool „KOZAK-MAMAI“', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2289', '80002094', 'Ukraina pühapäevakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '47176', '16141754', 'Uku Kuusk Purjespordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2088', '90009973', 'US Tennisekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31331', '80386692', 'Vabatantsuklubi', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1350', '75005110', 'Valga Kultuuri- ja Huvialakeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1310', '75005127', 'Valga Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2149', '80294530', 'Valga Ukraina laupäevakool "Kalõna"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3411', '80279128', 'Valgamaa Noorte Tehnikakeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '16871', '80365129', 'Valgevene Kultuuri Pühapäevakool "Busliki"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3591', '12086395', 'VaNadi Art Kunsti Stuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1137', '75017805', 'Vanalinna Hariduskolleegiumi Kunstimaja', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1136', '75017805', 'Vanalinna Hariduskolleegiumi Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1138', '75017805', 'Vanalinna Hariduskolleegiumi Muusikamaja', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1065', '80155509', 'Vanemuise Tantsu- ja Balletikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1132', '75001997', 'Vasalemma Kunstide Kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1113', '75021043', 'Vastseliina Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31671', '80420549', 'Veeriku Badminton sulgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2286', '80326715', 'Vene Pühapäevakool "DREVO"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2222', '80309875', 'Vene Pühapäevakool KITOVRAS', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1409', '80153077', 'Vene pühapäevakool Põlvkond', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43092', '80568683', 'Vertical Fitness Dance Studio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1180', '75021285', 'Viimsi Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2184', '80131727', 'Viimsi LTK Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1199', '75021296', 'Viimsi Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2403', '80195288', 'Viimsi Sulgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2118', '80290515', 'Viimsi Tantsustuudio', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6331', '80355415', 'Viimsi Tiigrid Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1089', '75005446', 'Viljandi Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3991', '80020135', 'Viljandi Jalgpallikool Tulevik', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1213', '75033520', 'Viljandi Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1118', '75005452', 'Viljandi Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1265', '75005268', 'Viljandi Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1086', '80253598', 'Viljandi Vene Kultuuri Sõprade Ühingu Pühapäevakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '45498', '16004098', 'Villi Nõmmiku Erahuvikool ', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '18911', '80377339', 'Vipers Hockey School', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1247', '75020552', 'Viru-Nigula valla muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2391', '80339764', 'Vivendi Koolituskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2311', '80056078', 'VK Budokan Budokool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1396', '80192545', 'VK Janika Tallinn Võimlemiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1301', '80072404', 'VK Janika Võimlemiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '19071', '14026821', 'Voilaa Erahuvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3971', '80213537', 'Vortex Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1315', '75034554', 'Võhma Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3831', '80205851', 'Võimlemis-ja Tantsuklubi Keeris Võimlemiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1264', '80097031', 'Võimlemisklubi "Piruett" Võimlemiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '7256', '80374039', 'Võimlemisklubi Graatsia', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6391', '80374140', 'Võimlemisklubi MyGym', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2383', '80222005', 'Võimlemisklubi Noorus Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '33011', '80292287', 'Võimlemiskool Akros', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2370', '80256771', 'Võimlemiskool Elegance', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2277', '80239413', 'Võimlemiskool Excite Dance', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4191', '80073935', 'Võimlemiskool Rüht', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '4531', '80359301', 'Võitluskunstide kool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2304', '80039795', 'Võrkpallikool Täht', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1293', '80101152', 'Võru Judokeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2355', '80077554', 'Võru Korvpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1085', '75029332', 'Võru Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1134', '80149590', 'Võru Loovuskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1076', '75020032', 'Võru Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1083', '75020017', 'Võru Spordikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '33371', '80426807', 'Võru Sulgpalliklubi Sulgpallikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '42611', '80554209', 'Võru Tantsukeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1200', '80186390', 'Võru Tantsukool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2279', '80072500', 'Võrumaa Malekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '32151', '80419859', 'Väike Tõru eelkool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1163', '75027212', 'Väike-Maarja Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '2631', '80333891', 'Väikese Päikese Kesklinna Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '20431', '14118136', 'Väikeste Meistrite Erahuvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1249', '75025845', 'Värska Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43991', '14859803', 'Õpetaja Lauri eelkool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '38034', '14195266', 'Ökula Huvikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1105', '75007451', 'Ülenurme Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '20671', '80384150', 'Ülenurme Võimlemiskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '20931', '80408525', 'Xteadus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');

