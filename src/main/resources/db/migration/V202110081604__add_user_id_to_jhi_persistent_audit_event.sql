ALTER TABLE public.jhi_persistent_audit_event
    ADD COLUMN user_id BIGINT,
    ADD CONSTRAINT fk_user_id
        FOREIGN KEY (user_id) REFERENCES jhi_user (id);

UPDATE public.jhi_persistent_audit_event audit_event
SET user_id =
        (SELECT id
         FROM public.jhi_user u
         WHERE u.login = audit_event.principal
        );

UPDATE public.jhi_persistent_audit_event audit_event
SET user_id =
        (SELECT user_id
         FROM public.external_authentication ea
         WHERE ea.external_id = audit_event.principal
        )
WHERE user_id IS NULL;
