UPDATE notification as n
SET link = concat(n.link, '&studentId=', n.creator_id)
WHERE message = '{SUBMITTED_SOLUTION}';
