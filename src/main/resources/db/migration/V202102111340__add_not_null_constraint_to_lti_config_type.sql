UPDATE public.lti_config
SET type = 'JOURNEY'
WHERE type IS NULL;

ALTER TABLE ONLY public.lti_config
    ALTER COLUMN type SET NOT NULL;
