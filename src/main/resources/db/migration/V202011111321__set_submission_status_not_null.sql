UPDATE submission
SET status = 'IN_PROGRESS'
WHERE status IS NULL;

ALTER TABLE submission
    ALTER COLUMN status SET NOT NULL;
