CREATE TABLE public.journey_state (
    journey_id BIGINT NOT NULL REFERENCES public.journey,
    user_id BIGINT NOT NULL REFERENCES public.jhi_user,
    state TEXT NOT NULL
)
