ALTER TABLE public.assignment
    DROP COLUMN lti_config_id;

CREATE TABLE public.lti_resource
(
    id               BIGINT,
    lti_app_id            BIGINT,
    entity_type       TEXT,
    entity_id         BIGINT,
    resource_link_id TEXT,
    created_by         text,
    created_date       timestamp without time zone,
    last_modified_by   text,
    last_modified_date timestamp without time zone,
    deleted            timestamp without time zone
);

ALTER TABLE ONLY public.lti_resource
    ADD CONSTRAINT fk_lti_resource_lti_app_id FOREIGN KEY (lti_app_id) REFERENCES public.lti_app (id);
