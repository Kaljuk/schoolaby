TRUNCATE public.notification CASCADE;

ALTER TABLE public.notification
    RENAME COLUMN user_id TO recipient_id;

ALTER TABLE public.notification
    RENAME CONSTRAINT fk_notification_user_id TO fk_notification_recipient_id;

ALTER TABLE public.notification
    ADD COLUMN read          timestamp without time zone DEFAULT NULL,
    ADD COLUMN journey_id    BIGINT,
    ADD COLUMN assignment_id BIGINT,
    ADD COLUMN creator_id    BIGINT;

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fk_notification_journey_id FOREIGN KEY (journey_id) REFERENCES public.journey (id),
    ADD CONSTRAINT fk_notification_assignment_id FOREIGN KEY (assignment_id) REFERENCES public.assignment (id),
    ADD CONSTRAINT fk_notification_creator_id FOREIGN KEY (creator_id) REFERENCES public.jhi_user (id);
