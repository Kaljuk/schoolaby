ALTER TABLE lti_app
    ADD COLUMN deep_linking_url     TEXT,
    ADD COLUMN login_initiation_url TEXT,
    ADD COLUMN client_id            TEXT,
    ADD COLUMN jwks_url             TEXT;

ALTER TABLE lti_config
    ADD COLUMN deployment_id TEXT;
