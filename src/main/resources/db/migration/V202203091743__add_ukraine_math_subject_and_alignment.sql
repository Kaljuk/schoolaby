INSERT INTO subject(id, label, country)
VALUES (53, 'schoolabyApp.subject.mathematics', 'Ukraine');

INSERT INTO educational_alignment(id, title, alignment_type, target_name, subject_id, country)
VALUES (nextval('educational_alignment_sequence'), 'Математика', 'subject', 'Математика', 53, 'Ukraine');
