UPDATE public."group" g
SET deleted = now()::timestamp
WHERE id NOT IN (SELECT group_id
                 FROM public.group_students gs
                 WHERE gs.group_id = g.id)
  AND g.deleted IS NULL;
