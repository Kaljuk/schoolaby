ALTER TABLE public.journey_state ADD COLUMN authority_name TEXT REFERENCES public.jhi_authority;

UPDATE public.journey_state SET authority_name = 'ROLE_STUDENT'
WHERE user_id IN (SELECT user_id FROM public.journey_students js WHERE js.journey_id = journey_state.journey_id);

UPDATE public.journey_state SET authority_name = 'ROLE_TEACHER'
WHERE user_id IN (SELECT user_id FROM public.journey_teachers jt WHERE jt.journey_id = journey_state.journey_id);

ALTER TABLE public.journey_state ADD CONSTRAINT journey_state_pkey PRIMARY KEY (journey_id, user_id, authority_name);
