CREATE TABLE submission_feedback_files
(
    submission_id    BIGINT REFERENCES submission (id),
    feedback_file_id BIGINT REFERENCES uploaded_file (id),
    PRIMARY KEY (submission_id, feedback_file_id)
);
