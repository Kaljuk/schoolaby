ALTER TABLE public.journey_students
    ADD COLUMN average_grade FLOAT;

UPDATE public.journey_students as "to_update"
SET average_grade = (SELECT round(avg, 2)
                     FROM (SELECT js.journey_id             as "journey_id",
                                  js.user_id                as "student_id",
                                  avg(gsv.percentage_range) as "avg"
                           FROM public.journey_students js
                                    RIGHT JOIN public.journey j ON j.id = js.journey_id
                                    RIGHT JOIN public.milestone m on j.id = m.journey_id
                                    RIGHT JOIN public.assignment a on m.id = a.milestone_id
                                    RIGHT JOIN public.grading_scheme gs on a.grading_scheme_id = gs.id
                                    RIGHT JOIN public.submission s on a.id = s.assignment_id
                                    RIGHT JOIN public.submission_feedback sf
                                               on s.id = sf.submission_id AND sf.student_id = js.user_id
                                    RIGHT JOIN public.grading_scheme_value gsv
                                              on gs.id = gsv.grading_scheme_id AND sf.grade = gsv.grade
                           WHERE gs.id NOT IN (4, 6)
                           GROUP BY js.user_id, js.journey_id) as jag
                     WHERE jag.journey_id = to_update.journey_id
                       AND jag.student_id = to_update.user_id)
WHERE average_grade IS NULL;
