ALTER TABLE grading_scheme_value
    ALTER COLUMN percentage_range TYPE integer USING percentage_range::integer;
