ALTER TABLE public.jhi_user
    ADD COLUMN first_login BOOLEAN DEFAULT TRUE;
UPDATE public.jhi_user
    SET first_login = FALSE;
