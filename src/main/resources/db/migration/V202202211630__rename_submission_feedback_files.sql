ALTER TABLE submission_feedback_files
    RENAME TO submission_feedback_uploaded_files;
ALTER TABLE submission_feedback_uploaded_files
    RENAME COLUMN feedback_id TO submission_feedback_id;
ALTER TABLE submission_feedback_uploaded_files
    RENAME COLUMN feedback_file_id TO uploaded_file_id;
