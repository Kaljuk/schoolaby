-- Grading scheme
INSERT INTO public.grading_scheme (id, name, deleted, created_by, created_date, last_modified_by, last_modified_date,
                                   code)
VALUES (7, 'Numerical (1-12)', NULL, NULL, NULL, NULL, NULL, 'NUMERICAL_1_12');

-- Grading scheme values
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (119, '12', 96, NULL, 7, NULL, NULL, NULL, NULL);

INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (120, '11', 90, NULL, 7, NULL, NULL, NULL, NULL);

INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (121, '10', 84, NULL, 7, NULL, NULL, NULL, NULL);

INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (122, '9', 78, NULL, 7, NULL, NULL, NULL, NULL);

INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (123, '8', 72, NULL, 7, NULL, NULL, NULL, NULL);

INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (124, '7', 66, NULL, 7, NULL, NULL, NULL, NULL);

INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (125, '6', 61, NULL, 7, NULL, NULL, NULL, NULL);

INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (126, '5', 56, NULL, 7, NULL, NULL, NULL, NULL);

INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (127, '4', 51, NULL, 7, NULL, NULL, NULL, NULL);

INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (128, '3', 31, NULL, 7, NULL, NULL, NULL, NULL);

INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (129, '2', 11, NULL, 7, NULL, NULL, NULL, NULL);

INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (130, '1', 0, NULL, 7, NULL, NULL, NULL, NULL);

-- Column for ordering in UI
ALTER TABLE public.grading_scheme ADD COLUMN sequence_number INTEGER;

-- Update existing grading schemes with sequence numbers
UPDATE public.grading_scheme SET sequence_number = 1 WHERE id = 1;
UPDATE public.grading_scheme SET sequence_number = 2 WHERE id = 7;
UPDATE public.grading_scheme SET sequence_number = 3 WHERE id = 2;
UPDATE public.grading_scheme SET sequence_number = 4 WHERE id = 3;
UPDATE public.grading_scheme SET sequence_number = 5 WHERE id = 5;
UPDATE public.grading_scheme SET sequence_number = 6 WHERE id = 4;
UPDATE public.grading_scheme SET sequence_number = 7 WHERE id = 6;

-- Add not null constraint
ALTER TABLE public.grading_scheme ALTER COLUMN sequence_number SET NOT NULL;
