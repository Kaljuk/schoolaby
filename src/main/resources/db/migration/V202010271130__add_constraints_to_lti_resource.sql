ALTER TABLE public.lti_resource
ALTER COLUMN title SET NOT NULL;

ALTER TABLE public.lti_resource
ALTER COLUMN entity_id SET NOT NULL;

ALTER TABLE public.lti_resource
ALTER COLUMN entity_type SET NOT NULL;
