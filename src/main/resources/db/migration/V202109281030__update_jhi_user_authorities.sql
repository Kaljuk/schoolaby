UPDATE public.jhi_user_authority
SET authority_name = 'ROLE_STUDENT'
WHERE authority_name = 'ROLE_USER'
  AND user_id NOT IN (
    SELECT user_id
    FROM public.jhi_user_authority
    WHERE authority_name IN ('ROLE_STUDENT',
                             'ROLE_TEACHER',
                             'ROLE_ADMIN')
);

DELETE FROM public.jhi_user_authority WHERE authority_name = 'ROLE_USER';

DELETE FROM public.jhi_authority WHERE name = 'ROLE_USER';
