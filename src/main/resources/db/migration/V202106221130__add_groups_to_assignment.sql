CREATE TABLE public.group
(
    id      bigint PRIMARY KEY,
    name    text NOT NULL,
    deleted timestamp without time zone,
    created_by         text,
    created_date       timestamp without time zone,
    last_modified_by   text,
    last_modified_date timestamp without time zone
);

CREATE TABLE public.group_students
(
    group_id    bigint NOT NULL REFERENCES public.group,
    students_id bigint NOT NULL REFERENCES public.jhi_user
);

CREATE TABLE public.assignment_groups
(
    group_id      bigint NOT NULL REFERENCES public.group,
    assignment_id bigint NOT NULL REFERENCES public.assignment
);
