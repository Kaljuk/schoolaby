ALTER TABLE "group" ADD COLUMN assignment_id BIGINT;

UPDATE "group" SET assignment_id = (SELECT assignment_id FROM assignment_groups WHERE group_id = "group".id);

DROP TABLE assignment_groups;
