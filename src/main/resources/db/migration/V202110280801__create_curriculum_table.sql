CREATE TABLE public.curriculum
(
    id BIGINT PRIMARY KEY,
    title   TEXT,
    country TEXT,
    sequence_number INTEGER,
    requires_subject BOOLEAN DEFAULT TRUE,
    UNIQUE (title, country)
);

INSERT INTO public.curriculum (id, title, country, requires_subject, sequence_number)
VALUES (1, 'NATIONAL_SECONDARY_EDUCATION', 'Estonia', TRUE, 1),
       (2, 'NATIONAL_PRIMARY_EDUCATION', 'Estonia', TRUE, 2),
       (3, 'NATIONAL_CURRICULUM', 'Tanzania', TRUE, 3),
       (4, 'EXTRACURRICULAR', NULL, FALSE, 4);
