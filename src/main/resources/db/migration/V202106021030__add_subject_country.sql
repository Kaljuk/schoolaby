ALTER TABLE subject
    ADD COLUMN country text;

UPDATE subject
SET country = 'Estonia';

INSERT INTO public.subject (id, label, country) VALUES (33, 'schoolabyApp.subject.kiswahili', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (34, 'schoolabyApp.subject.english', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (35, 'schoolabyApp.subject.french', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (36, 'schoolabyApp.subject.arabic', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (37, 'schoolabyApp.subject.biology', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (38, 'schoolabyApp.subject.chemistry', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (39, 'schoolabyApp.subject.physics', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (40, 'schoolabyApp.subject.mathematics', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (41, 'schoolabyApp.subject.informationAndComputerStudies', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (42, 'schoolabyApp.subject.technicalEducation', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (43, 'schoolabyApp.subject.agricultureAndHomeEconomics', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (44, 'schoolabyApp.subject.history', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (45, 'schoolabyApp.subject.geography', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (46, 'schoolabyApp.subject.civics', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (47, 'schoolabyApp.subject.commerce', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (48, 'schoolabyApp.subject.bookKeeping', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (49, 'schoolabyApp.subject.fineArts', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (50, 'schoolabyApp.subject.theatreArts', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (51, 'schoolabyApp.subject.physicalEducation', 'Tanzania');
INSERT INTO public.subject (id, label, country) VALUES (52, 'schoolabyApp.subject.music', 'Tanzania');
