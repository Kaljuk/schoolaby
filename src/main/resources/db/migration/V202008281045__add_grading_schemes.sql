--
-- Data for Name: grading_scheme; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.grading_scheme (id, name, deleted, created_by, created_date, last_modified_by, last_modified_date, code)
VALUES (1, 'Numerical (1-5)', NULL, NULL, NULL, NULL, NULL, 'NUMERICAL_1_5');
INSERT INTO public.grading_scheme (id, name, deleted, created_by, created_date, last_modified_by, last_modified_date, code)
VALUES (2, 'Alphabetical (A-F)', NULL, NULL, NULL, NULL, NULL, 'ALPHABETICAL_A_F');
INSERT INTO public.grading_scheme (id, name, deleted, created_by, created_date, last_modified_by, last_modified_date, code)
VALUES (3, 'Pass/Fail', NULL, NULL, NULL, NULL, NULL, 'PASS_FAIL');
INSERT INTO public.grading_scheme (id, name, deleted, created_by, created_date, last_modified_by, last_modified_date, code)
VALUES (4, 'Narrative', NULL, NULL, NULL, NULL, NULL, 'NARRATIVE');
INSERT INTO public.grading_scheme (id, name, deleted, created_by, created_date, last_modified_by, last_modified_date, code)
VALUES (5, 'Percentage (0%-100%)', NULL, NULL, NULL, NULL, NULL, 'PERCENTAGE_0_100');

--
-- Data for Name: grading_scheme_value; Type: TABLE DATA; Schema: public; Owner: Schoolaby
--
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (1, '5', '90', NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (2, '4', '75', NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (3, '3', '50', NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (4, '2', '20', NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (5, '1', '0', NULL, 1, NULL, NULL, NULL, NULL);
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (6, 'A', '91', NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (7, 'B', '81', NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (8, 'C', '71', NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (9, 'D', '61', NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (10, 'E', '51', NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (11, 'F', '0', NULL, 2, NULL, NULL, NULL, NULL);
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (12, 'PASS', '51', NULL, 3, NULL, NULL, NULL, NULL);
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (13, 'FAIL', '0', NULL, 3, NULL, NULL, NULL, NULL);
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (14, 'PASS', '0', NULL, 4, NULL, NULL, NULL, NULL);
INSERT INTO public.grading_scheme_value (id, grade, percentage_range, deleted, grading_scheme_id, created_by,
                                         created_date, last_modified_by, last_modified_date)
VALUES (15, 'PASS', '0', NULL, 5, NULL, NULL, NULL, NULL);
