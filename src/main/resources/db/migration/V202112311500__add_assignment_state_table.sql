CREATE TABLE public.assignment_state
(
    id             BIGSERIAL NOT NULL PRIMARY KEY,
    assignment_id  BIGINT NOT NULL REFERENCES public.assignment,
    user_id        BIGINT NOT NULL REFERENCES public.jhi_user,
    state          TEXT   NOT NULL,
    authority_name TEXT REFERENCES public.jhi_authority
);

CREATE SEQUENCE public.assignment_state_seq OWNED BY public.assignment_state.id;
