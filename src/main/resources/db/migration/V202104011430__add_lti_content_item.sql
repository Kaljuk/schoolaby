CREATE TABLE lti_content_item
(
    id               bigserial PRIMARY KEY,
    type             TEXT  NOT NULL,
    resource_link_id TEXT  NOT NULL,
    properties       JSONB NOT NULL
);

ALTER TABLE lti_resource
    ADD COLUMN lti_content_item_id bigint NULL REFERENCES lti_content_item;

ALTER TABLE lti_config
    ADD COLUMN content_item_selection_enabled boolean DEFAULT false;
