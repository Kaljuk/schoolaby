CREATE TABLE submission_uploaded_files (
    submission_id BIGINT REFERENCES submission (id),
    uploaded_file_id BIGINT REFERENCES uploaded_file (id),
    PRIMARY KEY (submission_id, uploaded_file_id)
);

INSERT
INTO submission_uploaded_files
SELECT uploaded_file.submission_id, uploaded_file.id
FROM uploaded_file
WHERE uploaded_file.submission_id IS NOT NULL;

ALTER TABLE uploaded_file
    DROP COLUMN submission_id;

