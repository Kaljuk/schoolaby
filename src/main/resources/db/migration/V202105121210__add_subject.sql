CREATE TABLE public.subject
(
    id    BIGINT NOT NULL PRIMARY KEY,
    label TEXT   NOT NULL
);

INSERT INTO public.subject (id, label)
VALUES (1, 'schoolabyApp.subject.estonian');
INSERT INTO public.subject (id, label)
VALUES (2, 'schoolabyApp.subject.russian');
INSERT INTO public.subject (id, label)
VALUES (3, 'schoolabyApp.subject.englishForeign');
INSERT INTO public.subject (id, label)
VALUES (4, 'schoolabyApp.subject.germanForeign');
INSERT INTO public.subject (id, label)
VALUES (5, 'schoolabyApp.subject.russianForeign');
INSERT INTO public.subject (id, label)
VALUES (6, 'schoolabyApp.subject.frenchForeign');
INSERT INTO public.subject (id, label)
VALUES (7, 'schoolabyApp.subject.estonianForeign');
INSERT INTO public.subject (id, label)
VALUES (8, 'schoolabyApp.subject.mathematics');
INSERT INTO public.subject (id, label)
VALUES (9, 'schoolabyApp.subject.natureEducation');
INSERT INTO public.subject (id, label)
VALUES (10, 'schoolabyApp.subject.biology');
INSERT INTO public.subject (id, label)
VALUES (11, 'schoolabyApp.subject.geography');
INSERT INTO public.subject (id, label)
VALUES (12, 'schoolabyApp.subject.physics');
INSERT INTO public.subject (id, label)
VALUES (13, 'schoolabyApp.subject.chemistry');
INSERT INTO public.subject (id, label)
VALUES (14, 'schoolabyApp.subject.humanities');
INSERT INTO public.subject (id, label)
VALUES (15, 'schoolabyApp.subject.history');
INSERT INTO public.subject (id, label)
VALUES (16, 'schoolabyApp.subject.socialStudies');
INSERT INTO public.subject (id, label)
VALUES (17, 'schoolabyApp.subject.music');
INSERT INTO public.subject (id, label)
VALUES (18, 'schoolabyApp.subject.art');
INSERT INTO public.subject (id, label)
VALUES (19, 'schoolabyApp.subject.industrialArts');
INSERT INTO public.subject (id, label)
VALUES (20, 'schoolabyApp.subject.craftsAndHousehold');
INSERT INTO public.subject (id, label)
VALUES (21, 'schoolabyApp.subject.technology');
INSERT INTO public.subject (id, label)
VALUES (22, 'schoolabyApp.subject.physicalEducation');
INSERT INTO public.subject (id, label)
VALUES (23, 'schoolabyApp.subject.literature');
INSERT INTO public.subject (id, label)
VALUES (24, 'schoolabyApp.subject.nationalDefence');
INSERT INTO public.subject (id, label)
VALUES (25, 'schoolabyApp.subject.religiousEducation');
INSERT INTO public.subject (id, label)
VALUES (26, 'schoolabyApp.subject.economics');
INSERT INTO public.subject (id, label)
VALUES (27, 'schoolabyApp.subject.finnish');
INSERT INTO public.subject (id, label)
VALUES (28, 'schoolabyApp.subject.informatics');
INSERT INTO public.subject (id, label)
VALUES (29, 'schoolabyApp.subject.generalEducation');
INSERT INTO public.subject (id, label)
VALUES (30, 'schoolabyApp.subject.cyberDefence');
INSERT INTO public.subject (id, label)
VALUES (31, 'schoolabyApp.subject.swedish');
INSERT INTO public.subject (id, label)
VALUES (32, 'schoolabyApp.subject.other');

ALTER TABLE public.journey ADD COLUMN subject_id BIGINT REFERENCES public.subject;

ALTER TABLE public.educational_alignment ADD COLUMN subject_id BIGINT REFERENCES public.subject;

CREATE SEQUENCE IF NOT EXISTS educational_alignment_sequence AS BIGINT;
SELECT setval('educational_alignment_sequence', (SELECT max(id) FROM public.educational_alignment));

