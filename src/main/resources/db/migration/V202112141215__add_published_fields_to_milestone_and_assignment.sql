ALTER TABLE assignment
    ADD COLUMN published TIMESTAMP WITHOUT TIME ZONE;
ALTER TABLE milestone
    ADD COLUMN published TIMESTAMP WITHOUT TIME ZONE;

UPDATE public.assignment
SET published = NOW() - INTERVAL '1 DAY'
WHERE id IN (
    SELECT id
    FROM public.assignment
    WHERE assignment.hidden = FALSE
);

UPDATE public.milestone
SET published = NOW() - INTERVAL '1 DAY'
WHERE id IN (
    SELECT id
    FROM public.milestone
    WHERE milestone.hidden = FALSE
);
