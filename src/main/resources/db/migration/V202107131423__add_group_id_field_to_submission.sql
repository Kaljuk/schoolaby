ALTER TABLE submission
    ADD COLUMN group_id bigint,
    ADD CONSTRAINT fk_group_id
        FOREIGN KEY (group_id)
            REFERENCES "group" (id);
