CREATE TABLE public.lti_line_item
(
    id                 bigint                      NOT NULL PRIMARY KEY,
    score_maximum      bigint                      NOT NULL,
    label              text,
    lti_resource_id    bigint                      NOT NULL REFERENCES public.lti_resource,
    created_by         text                        NOT NULL,
    created_date       timestamp WITHOUT TIME ZONE NOT NULL,
    last_modified_by   text                        NOT NULL,
    last_modified_date timestamp WITHOUT TIME ZONE NOT NULL,
    deleted            timestamp WITHOUT TIME ZONE
);

CREATE TABLE public.lti_score
(
    id                 bigint                      NOT NULL PRIMARY KEY,
    line_item_id       bigint                      NOT NULL REFERENCES lti_line_item,
    timestamp          timestamp WITHOUT TIME ZONE NOT NULL,
    score_given        bigint,
    score_maximum      bigint,
    comment            text,
    activity_progress  text,
    grading_progress   text,
    user_id            bigint                      NOT NULL REFERENCES jhi_user,
    created_by         text                        NOT NULL,
    created_date       timestamp WITHOUT TIME ZONE NOT NULL,
    last_modified_by   text                        NOT NULL,
    last_modified_date timestamp WITHOUT TIME ZONE NOT NULL,
    deleted            timestamp WITHOUT TIME ZONE,
    CONSTRAINT if_score_given_not_null_score_maximum_not_null CHECK ( NOT (score_given IS NOT NULL AND score_maximum IS NULL) )
);

ALTER TABLE public.lti_resource
    ADD COLUMN tool_resource_id text;
