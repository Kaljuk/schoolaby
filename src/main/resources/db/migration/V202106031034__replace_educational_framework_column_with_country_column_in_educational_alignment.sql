ALTER TABLE educational_alignment
    ADD COLUMN country text;

UPDATE educational_alignment
SET country = 'Estonia';

ALTER TABLE educational_alignment
    DROP COLUMN educational_framework;
