ALTER TABLE submission
    RENAME submitted TO submitted_for_grading_date;

ALTER TABLE submission
    RENAME re_submittable TO resubmittable;

ALTER TABLE submission
    ADD submitted_for_grading boolean DEFAULT FALSE NOT NULL;

UPDATE submission
SET submitted_for_grading = TRUE
WHERE status IN ('SUBMITTED');

ALTER TABLE submission
    DROP COLUMN status;
