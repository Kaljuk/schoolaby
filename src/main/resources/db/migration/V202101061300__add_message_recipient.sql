CREATE TABLE message_recipient
(
    id         bigint NOT NULL,
    message_id BIGINT NOT NULL
        CONSTRAINT fk_message_recipient_message_id REFERENCES message (id),
    person_id  BIGINT NOT NULL
        CONSTRAINT fk_message_recipient_person_id REFERENCES jhi_user (id),
    read       timestamp without time zone DEFAULT NULL
);

ALTER TABLE ONLY public.message_recipient
    ADD CONSTRAINT message_recipient_pkey PRIMARY KEY (id);
