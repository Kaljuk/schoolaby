UPDATE chat
SET journey_id = j.id
FROM chat c
         JOIN submission s on c.submission_id = s.id
         JOIN assignment a on s.assignment_id = a.id
         JOIN milestone m on a.milestone_id = m.id
         JOIN journey j on m.journey_id = j.id;
