ALTER TABLE public.selected_criterion_level ADD COLUMN submission_feedback_id BIGINT REFERENCES public.submission_feedback, ALTER COLUMN submission_id DROP NOT NULL;

UPDATE public.selected_criterion_level scl SET submission_feedback_id = (SELECT sf.id FROM submission_feedback sf WHERE scl.submission_id = sf.submission_id LIMIT 1);
