ALTER TABLE public.journey_signup_code
    ADD COLUMN one_time BOOLEAN DEFAULT FALSE NOT NULL;

ALTER TABLE public.journey_signup_code
    ADD COLUMN used BOOLEAN DEFAULT FALSE NOT NULL;
