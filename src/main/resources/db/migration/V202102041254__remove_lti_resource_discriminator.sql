-- bug caused entity_id to be null after removing resource from milestone/assignment, nothing to do with these invalid resources
DELETE
FROM lti_launch
WHERE lti_resource_id IN (SELECT id
                          FROM lti_resource
                          WHERE entity_id IS NULL);
DELETE
FROM lti_resource
WHERE entity_id IS NULL;

ALTER TABLE lti_resource
    ADD COLUMN milestone_id  bigint REFERENCES milestone,
    ADD COLUMN assignment_id bigint REFERENCES assignment;

UPDATE lti_resource
SET milestone_id = entity_id
WHERE entity_type = 'MILESTONE';
UPDATE lti_resource
SET assignment_id = entity_id
WHERE entity_type = 'ASSIGNMENT';

ALTER TABLE lti_resource
    DROP COLUMN entity_type,
    DROP COLUMN entity_id,
    ADD CONSTRAINT entity_ref CHECK
        ((milestone_id IS NULL AND assignment_id IS NOT NULL)
            OR (milestone_id IS NOT NULL AND assignment_id IS NULL));
