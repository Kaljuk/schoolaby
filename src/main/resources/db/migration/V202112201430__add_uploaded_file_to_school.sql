ALTER TABLE public.school
    ADD COLUMN uploaded_file_id BIGINT REFERENCES public.uploaded_file;
