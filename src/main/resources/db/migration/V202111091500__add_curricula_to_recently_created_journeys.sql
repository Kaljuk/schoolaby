UPDATE public.journey
SET curriculum_id = 1
WHERE journey.id IN (SELECT j.id
                     FROM public.journey j
                              LEFT JOIN educational_level el ON j.educational_level_id = el.id
                     WHERE j.deleted IS NULL
                       AND j.curriculum_id IS NULL
                       AND j.national_curriculum IS TRUE
                       AND el.name = 'Gümnaasium');

UPDATE public.journey
SET curriculum_id = 2
WHERE id IN (SELECT j.id
             FROM public.journey j
                      LEFT JOIN educational_level el ON j.educational_level_id = el.id
             WHERE j.deleted IS NULL
               AND j.curriculum_id IS NULL
               AND j.national_curriculum IS TRUE
               AND NOT (el.name = 'Gümnaasium' OR el.name = 'Täiendkoolitus'));

UPDATE public.journey
SET curriculum_id = 4
WHERE id IN (SELECT j.id
             FROM public.journey j
                      LEFT JOIN educational_level el ON j.educational_level_id = el.id
             WHERE j.deleted IS NULL
               AND j.curriculum_id IS NULL
               AND el.name = 'Täiendkoolitus');

UPDATE public.journey
SET curriculum_id = 4
WHERE id IN (SELECT j.id
             FROM public.journey j
                      LEFT JOIN educational_level el ON j.educational_level_id = el.id
             WHERE j.deleted IS NULL
               AND j.curriculum_id IS NULL
               AND j.national_curriculum IS FALSE);

UPDATE public.journey
SET curriculum_id = 3
WHERE id IN (SELECT j.id
             FROM public.journey j
                      LEFT JOIN educational_level el ON j.educational_level_id = el.id
             WHERE j.deleted IS NULL
               AND j.curriculum_id IS NULL
               AND j.national_curriculum IS TRUE
               AND el.country = 'Tanzania');
