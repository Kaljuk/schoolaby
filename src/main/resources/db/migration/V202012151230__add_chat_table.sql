CREATE TABLE public.chat
(
    id                 bigint NOT NULL,
    journey_id         bigint,
    submission_id      bigint,
    title              text,
    created_by         text NOT NULL,
    created_date       timestamp without time zone NOT NULL,
    last_modified_by   text NOT NULL,
    last_modified_date timestamp without time zone NOT NULL,
    deleted            timestamp without time zone
);

ALTER TABLE ONLY public.chat
    ADD CONSTRAINT chat_pkey PRIMARY KEY (id);

CREATE TABLE public.chat_people
(
    chat_id   bigint not null
        constraint fk_chat_people_chat_id
            references chat,
    person_id bigint not null
        constraint fk_chat_people_person_id
            references jhi_user,
    constraint chat_people_pkey
        primary key (chat_id, person_id)
);

ALTER TABLE public.message
    DROP COLUMN journey_id;
ALTER TABLE public.message
    DROP COLUMN submission_id;
ALTER TABLE public.message
    DROP COLUMN step_id;
ALTER TABLE public.message
    DROP COLUMN recipient_id;
ALTER TABLE public.message
    ADD COLUMN chat_id bigint;

ALTER TABLE ONLY public.message
    ADD CONSTRAINT fk_message_chat_id FOREIGN KEY (chat_id) REFERENCES public.chat (id);
