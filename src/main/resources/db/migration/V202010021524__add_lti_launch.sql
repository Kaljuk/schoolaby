ALTER TABLE lti_resource
    ADD PRIMARY KEY (id);

CREATE TABLE lti_launch
(
    id                 bigserial PRIMARY KEY,
    user_id            bigint NOT NULL REFERENCES jhi_user,
    lti_resource_id    bigint NOT NULL REFERENCES lti_resource,
    result             TEXT,
    created_by         TEXT   NOT NULL,
    created_date       TIMESTAMP,
    last_modified_by   TEXT,
    last_modified_date TIMESTAMP,
    deleted            TIMESTAMP
);
