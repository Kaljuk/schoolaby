ALTER TABLE public.educational_level ADD COLUMN country text;

UPDATE public.educational_level SET country = 'Estonia';

INSERT INTO public.educational_level (id, name, country) VALUES
(6, 'Form I', 'Tanzania'),
(7, 'Form II', 'Tanzania'),
(8, 'Form III', 'Tanzania'),
(9, 'Form IV', 'Tanzania');
