ALTER TABLE public.journey_state DROP CONSTRAINT journey_state_pkey;
ALTER TABLE public.journey_state ALTER COLUMN authority_name DROP NOT NULL;
ALTER TABLE public.journey_state ADD COLUMN id BIGINT;

CREATE SEQUENCE public.journey_state_seq OWNED BY public.journey_state.id;
UPDATE public.journey_state SET id = nextval('public.journey_state_seq') WHERE id IS NULL;

ALTER TABLE public.journey_state ADD PRIMARY KEY (id);
ALTER TABLE public.journey_state ADD CONSTRAINT journey_state_unique UNIQUE (journey_id, user_id, authority_name);
