INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Perimeters And Areas', 'subject', 'Tanzania', 'Perimeters And Areas', NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Coordinate Geometry', 'subject', 'Tanzania', 'Coordinate Geometry', NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Ratio, Profit And Loss', 'subject', 'Tanzania', 'Ratio, Profit And Loss', NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Numbers (II)', 'subject', 'Tanzania', 'Numbers (II)', NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, 40);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Algebra', 'subject', 'Tanzania', 'Algebra', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, 40);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Geometry', 'subject', 'Tanzania', 'Geometry', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, 40);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Approximations', 'subject', 'Tanzania', 'Approximations', NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, 40);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Units', 'subject', 'Tanzania', 'Units', NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, 40);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Decimal And Percentage', 'subject', 'Tanzania', 'Decimal And Percentage', NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 40);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Fractions', 'subject', 'Tanzania', 'Fractions', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, 40);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Numbers', 'subject', 'Tanzania', 'Numbers', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, 40);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Ufahamu', 'subject', 'Tanzania', 'Ufahamu', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, 33);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Uandishi Wa Barua', 'subject', 'Tanzania', 'Uandishi Wa Barua', NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, 33);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Uandishi Wa Insha', 'subject', 'Tanzania', 'Uandishi Wa Insha', NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, 33);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Usimulizi', 'subject', 'Tanzania', 'Usimulizi', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, 33);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Fasihi Simulizi', 'subject', 'Tanzania', 'Fasihi Simulizi', NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, 33);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Fasihi Kwa Ujumla', 'subject', 'Tanzania', 'Fasihi Kwa Ujumla', NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, 33);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Aina Za Maneno', 'subject', 'Tanzania', 'Aina Za Maneno', NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, 33);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Mawasiliano', 'subject', 'Tanzania', 'Mawasiliano', NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, 33);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Computer Evolution', 'subject', 'Tanzania', 'Computer Evolution', NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, 41);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Computer handling', 'subject', 'Tanzania', 'Computer handling', NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, 41);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'The Computer Software', 'subject', 'Tanzania', 'The Computer Software', NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, 41);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'The Computer', 'subject', 'Tanzania', 'The Computer', NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, 41);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Information', 'subject', 'Tanzania', 'Information', NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, 41);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Development Of Social And Political Systems', 'subject', 'Tanzania',
        'Development Of Social And Political Systems', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 44);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Development Of Economic Activities And Their Impact', 'subject', 'Tanzania',
        'Development Of Economic Activities And Their Impact', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        44);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Evolution Of Man, Technology And Environment', 'subject', 'Tanzania',
        'Evolution Of Man, Technology And Environment', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 44);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Sources And Importance Of History', 'subject', 'Tanzania',
        'Sources And Importance Of History', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 44);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Map Work', 'subject', 'Tanzania', 'Map Work', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, 45);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Climate', 'subject', 'Tanzania', 'Climate', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, 45);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Weather', 'subject', 'Tanzania', 'Weather', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, 45);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Major Features Of The Earth''s Surface', 'subject', 'Tanzania',
        'Major Features Of The Earth''s Surface', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Concept Of Geography', 'subject', 'Tanzania', 'Concept Of Geography', NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Writing A Variety Of Texts', 'subject', 'Tanzania', 'Writing A Variety Of Texts',
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Taking Notes', 'subject', 'Tanzania', 'Taking Notes', NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, 34);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Writing Personal Letters', 'subject', 'Tanzania', 'Writing Personal Letters', NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Analysing Information From The Media', 'subject', 'Tanzania',
        'Analysing Information From The Media', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Interpreting Literary Works', 'subject', 'Tanzania', 'Interpreting Literary Works',
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Reading A Variety Of Texts', 'subject', 'Tanzania', 'Reading A Variety Of Texts',
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Expressing Future Plans/Activities', 'subject', 'Tanzania',
        'Expressing Future Plans/Activities', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Talking About Past Events/Activities', 'subject', 'Tanzania',
        'Talking About Past Events/Activities', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Expressing Opinions And Feelings', 'subject', 'Tanzania',
        'Expressing Opinions And Feelings', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Talking About One''s Family', 'subject', 'Tanzania', 'Talking About One''s Family',
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Expressing Likes And Dislikes', 'subject', 'Tanzania',
        'Expressing Likes And Dislikes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Expressing Ongoing Activities', 'subject', 'Tanzania',
        'Expressing Ongoing Activities', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Expressing Personal And Group Routines/Habits', 'subject', 'Tanzania',
        'Expressing Personal And Group Routines/Habits', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Using A Dictionary', 'subject', 'Tanzania', 'Using A Dictionary', NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Giving Directions', 'subject', 'Tanzania', 'Giving Directions', NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Listening To And Understanding Simple Texts About A Variety Of Events And Situations', 'subject',
        'Tanzania',
        'Listening To And Understanding Simple Texts About A Variety Of Events And Situations', NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, 34);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Production', 'subject', 'Tanzania', 'Production', NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, 47);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'The Scope Of Commerce', 'subject', 'Tanzania', 'The Scope Of Commerce', NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, 47);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Road Safety Education', 'subject', 'Tanzania', 'Road Safety Education', NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, 46);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Family', 'subject', 'Tanzania', 'Family', NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, 46);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Work', 'subject', 'Tanzania', 'Work', NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, 46);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Responsible Citizenship', 'subject', 'Tanzania', 'Responsible Citizenship', NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 46);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Human Rights', 'subject', 'Tanzania', 'Human Rights', NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, 46);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Promotion Of Life Skills', 'subject', 'Tanzania', 'Promotion Of Life Skills', NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 46);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Our Nation', 'subject', 'Tanzania', 'Our Nation', NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, 46);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Air Combustion, Rusting And Fire Fighting', 'subject', 'Tanzania',
        'Air Combustion, Rusting And Fire Fighting', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 38);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Matter', 'subject', 'Tanzania', 'Matter', NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, 38);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'The Scientific Procedure', 'subject', 'Tanzania', 'The Scientific Procedure', NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 38);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Heat Sources And Flames', 'subject', 'Tanzania', 'Heat Sources And Flames', NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 38);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Laboratory Techniques And Safety', 'subject', 'Tanzania',
        'Laboratory Techniques And Safety', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 38);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Introduction To Chemistry', 'subject', 'Tanzania', 'Introduction To Chemistry',
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 38);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Elementary Balance Sheet', 'subject', 'Tanzania', 'Elementary Balance Sheet', NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 48);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Elementary Trading Profit And Loss Accounts', 'subject', 'Tanzania',
        'Elementary Trading Profit And Loss Accounts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 48);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Stock', 'subject', 'Tanzania', 'Stock', NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, 48);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Trial Balance', 'subject', 'Tanzania', 'Trial Balance', NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, 48);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Classification Of Accounts', 'subject', 'Tanzania', 'Classification Of Accounts',
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 48);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Principles Of Double Entry System', 'subject', 'Tanzania',
        'Principles Of Double Entry System', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 48);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Subject Matter Of Book Keeping', 'subject', 'Tanzania',
        'Subject Matter Of Book Keeping', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 48);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Classification Of Living Things', 'subject', 'Tanzania',
        'Classification Of Living Things', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Cell Structure And Organization', 'subject', 'Tanzania',
        'Cell Structure And Organization', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Safety In Our Environment', 'subject', 'Tanzania', 'Safety In Our Environment',
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Introduction To Biology', 'subject', 'Tanzania', 'Introduction To Biology', NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Light', 'subject', 'Tanzania', 'Light', NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, 39);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Work, Energy And Power', 'subject', 'Tanzania', 'Work, Energy And Power', NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 39);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Pressure', 'subject', 'Tanzania', 'Pressure', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, 39);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Structure And Properties Of Matter', 'subject', 'Tanzania',
        'Structure And Properties Of Matter', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 39);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Archimedes'' Principle And Law Of Flotation', 'subject', 'Tanzania',
        'Archimedes'' Principle And Law Of Flotation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 39);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Force', 'subject', 'Tanzania', 'Force', NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, 39);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Measurement', 'subject', 'Tanzania', 'Measurement', NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, 39);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Introduction To Laboratory Practice', 'subject', 'Tanzania',
        'Introduction To Laboratory Practice', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 39);
INSERT INTO public.educational_alignment (id, title, alignment_type, country, target_name, target_url,
                                          start_date, end_date, deleted, created_by, created_date, last_modified_by,
                                          last_modified_date, taxon_id, subject_id)
VALUES (nextval('educational_alignment_sequence'), 'Introduction To Physics', 'subject', 'Tanzania', 'Introduction To Physics', NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 39);
