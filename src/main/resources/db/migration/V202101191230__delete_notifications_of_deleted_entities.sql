UPDATE notification
SET deleted = j.deleted
FROM notification n
         JOIN journey j on n.journey_id = j.id
WHERE n.assignment_id IS NULL
  AND j.deleted IS NOT NULL;

UPDATE notification
SET deleted = a.deleted
FROM notification n
         JOIN assignment a on n.assignment_id = a.id
WHERE a.deleted IS NOT NULL;
