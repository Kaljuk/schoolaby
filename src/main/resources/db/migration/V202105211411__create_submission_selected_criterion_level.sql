CREATE TABLE selected_criterion_level
(
    id BIGINT NOT NULL PRIMARY KEY,
    modified_description TEXT NOT NULL,
    submission_id BIGINT NOT NULL
        CONSTRAINT fk_submission_selected_criterion_level_submission REFERENCES submission (id),
    criterion_level_id BIGINT NOT NULL
        CONSTRAINT fk_submission_selected_criterion_level_criterion_level REFERENCES criterion_level (id),
    created_by         TEXT NOT NULL,
    created_date       TIMESTAMP,
    last_modified_by   TEXT,
    last_modified_date TIMESTAMP,
    deleted            TIMESTAMP
);
