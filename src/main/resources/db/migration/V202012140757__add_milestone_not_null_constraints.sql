UPDATE milestone
SET created_by = 'admin'
WHERE created_by IS NULL;

UPDATE milestone ms
SET creator_id = (SELECT u.id FROM jhi_user u WHERE u.login = ms.created_by)
WHERE ms.creator_id IS NULL;

-- last modified by user that is no longer in db
UPDATE milestone ms
SET creator_id = (SELECT u.id FROM jhi_user u WHERE u.login = 'admin'),
    created_by = 'admin'
WHERE ms.creator_id IS NULL;

UPDATE milestone
SET created_date = now()
WHERE milestone.created_date IS NULL;

UPDATE milestone
SET deleted = now()
WHERE id IN (SELECT m.id
             FROM milestone m
                      INNER JOIN journey j ON m.journey_id = j.id
             WHERE m.deleted IS NULL
               AND (j.deleted IS NOT NULL));

UPDATE assignment
SET deleted = now()
WHERE id IN (SELECT a.id
             FROM assignment a
                      INNER JOIN milestone m ON a.milestone_id = m.id
                      INNER JOIN journey j ON m.journey_id = j.id
             WHERE a.deleted IS NULL
               AND (j.deleted IS NOT NULL OR m.deleted IS NOT NULL));

UPDATE lti_resource
SET deleted = now()
WHERE id IN (SELECT lr.id
             FROM lti_resource lr
                      INNER JOIN milestone m ON m.id = lr.entity_id
                 AND entity_type = 'MILESTONE'
                 AND m.deleted IS NOT NULL
                 AND lr.deleted IS NULL);

ALTER TABLE milestone
    ALTER COLUMN end_date SET NOT NULL,
    ALTER COLUMN journey_id SET NOT NULL,
    ALTER COLUMN creator_id SET NOT NULL,
    ALTER COLUMN created_by SET NOT NULL,
    ALTER COLUMN created_date SET NOT NULL,
    ALTER COLUMN last_modified_by SET NOT NULL,
    ALTER COLUMN last_modified_date SET NOT NULL;
