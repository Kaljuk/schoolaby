CREATE TABLE public.notification
(
    id                 bigint NOT NULL,
    message            text   NOT NULL,
    link               text   NOT NULL,
    user_id            bigint NOT NULL,
    deleted            timestamp without time zone,

    created_by         text NOT NULL,
    created_date       timestamp without time zone NOT NULL,
    last_modified_by   text NOT NULL,
    last_modified_date timestamp without time zone NOT NULL
);

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fk_notification_user_id FOREIGN KEY (user_id) REFERENCES public.jhi_user (id);
