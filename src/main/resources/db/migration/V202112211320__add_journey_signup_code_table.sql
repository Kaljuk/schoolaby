CREATE TABLE public.journey_signup_code(
   sign_up_code TEXT NOT NULL ,
   authority_name TEXT REFERENCES public.jhi_authority NOT NULL,
   journey_id BIGINT REFERENCES public.journey NOT NULL,
   PRIMARY KEY (sign_up_code, authority_name, journey_id)
);

ALTER TABLE public.journey
ALTER COLUMN sign_up_code
DROP NOT NULL;

CREATE OR REPLACE FUNCTION random_string( int ) RETURNS TEXT as $$
SELECT string_agg(substring('0123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMOPQRSTVXYZ', round(random() * 40)::integer, 1), '') FROM generate_series(1, $1);
$$ language sql;

INSERT INTO public.journey_signup_code(journey_id, authority_name, sign_up_code)
SELECT id AS journey_id, 'ROLE_TEACHER' as authority_name, random_string(6) FROM journey;

INSERT INTO public.journey_signup_code(journey_id, authority_name, sign_up_code)
SELECT id AS journey_id, 'ROLE_STUDENT' as authority_name, sign_up_code FROM journey;
