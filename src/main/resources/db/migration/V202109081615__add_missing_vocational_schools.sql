INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '68', '10176579', 'Eesti Esimene Erakosmeetikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '825', '10654381', 'Eesti Massaaži- ja Teraapiakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5631', '70009646', 'Eesti Merekool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '3', '70003968', 'G. Otsa nim Tallinna Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '51', '70003796', 'Haapsalu Kutsehariduskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '23', '70005619', 'Heino Elleri Muusikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '22', '70002549', 'Hiiumaa Ametikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1041', '70007742', 'Ida-Virumaa Kutsehariduskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '134', '10732046', 'Juuksurite Erakool "Maridel"', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1400', '70008546', 'Järvamaa Kutsehariduskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '81', '70000846', 'Kehtna Kutsehariduskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '46', '70003744', 'Kuressaare Ametikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '18', '70002443', 'Luua Metsanduskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '13', '70002555', 'Olustvere Teenindus- ja Maamajanduskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '152', '10721315', 'Pärnu Saksa Tehnoloogiakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '107', '70006369', 'Pärnumaa Kutsehariduskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '48', '70003840', 'Rakvere Ametikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '20', '70002420', 'Räpina Aianduskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '752', '70006286', 'Tallinna Balletikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '31', '70003951', 'Tallinna Ehituskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '1017', '75029429', 'Tallinna Kopli Ametikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '29', '70003767', 'Tallinna Lasnamäe Mehaanikakool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '6', '70003460', 'Tallinna Majanduskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '5', '70003974', 'Tallinna Polütehnikum', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '32', '70004637', 'Tallinna Teeninduskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '91', '70005559', 'Tallinna Tööstushariduskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '12', '70004092', 'Tartu Kunstikool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '826', '75024308', 'Tartu Kutsehariduskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '88', '70005571', 'Valgamaa Kutseõppekeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '49', '70004643', 'Vana-Vigala Tehnika- ja Teeninduskool', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '43', '70005565', 'Viljandi Kutseõppekeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');
INSERT INTO public.school(
id, ehis_id, reg_nr, name, country, created_date, created_by, last_modified_date, last_modified_by)
VALUES (nextval('school_id_seq'), '92', '70005542', 'Võrumaa Kutsehariduskeskus', 'Estonia', NOW()::timestamp, 'system', NOW()::timestamp, 'system');

