UPDATE jhi_user
SET country = 'Estonia'
WHERE country = 'Eesti';

UPDATE jhi_user
SET country = 'Finland'
WHERE country = 'Suomi';

UPDATE jhi_user
SET country = 'Ukraine'
WHERE country = 'Україна';
