package com.schoolaby.domain.keys;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AssignmentMaterialKey implements Serializable {
    private Long assignment;
    private Long material;
}
