package com.schoolaby.domain.keys;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JourneyUserKey implements Serializable {
    private Long user;
    private Long journey;
}
