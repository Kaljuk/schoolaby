package com.schoolaby.domain;

import com.schoolaby.domain.keys.JourneyUserKey;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import java.time.Instant;
import java.util.Objects;

import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

@Entity
@Table(name = "journey_students")
@Cache(usage = READ_WRITE)
@Getter
@Setter
@IdClass(JourneyUserKey.class)
public class JourneyStudent {
    @Id
    @ManyToOne
    private User user;

    @Id
    @ManyToOne
    private Journey journey;

    private Instant pinnedDate;

    private Double averageGrade;

    public JourneyStudent user(User user) {
        setUser(user);
        return this;
    }

    public JourneyStudent journey(Journey journey) {
        setJourney(journey);
        return this;
    }

    public JourneyStudent pinnedDate(Instant pinnedDate) {
        setPinnedDate(pinnedDate);
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof JourneyStudent) {
            JourneyStudent journeyStudent = (JourneyStudent) obj;
            return journeyStudent.getUser().equals(getUser()) && journeyStudent.getJourney().equals(getJourney());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUser(), getJourney(), pinnedDate);
    }
}
