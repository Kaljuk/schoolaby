package com.schoolaby.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.schoolaby.security.SecurityUtils;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.Loader;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static java.util.stream.Collectors.toSet;
import static javax.persistence.CascadeType.REMOVE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

@Entity
@Getter
@Setter
@Table(name = "material")
@Cache(usage = READ_WRITE)
@SQLDelete(sql = "UPDATE material SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findMaterialById")
@NamedQuery(name = "findMaterialById", query = "SELECT m FROM Material m WHERE m.id = ?1 AND m.deleted IS NULL")
@Where(clause = "deleted IS NULL")
public class Material extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    private String type;
    private String description;
    private String externalId;

    @NotNull
    @Column(name = "url", nullable = false)
    private String url;

    @Column(name = "image_url", nullable = false)
    private String imageUrl;

    @OneToOne
    private UploadedFile uploadedFile;

    @ManyToMany
    @Cache(usage = READ_WRITE)
    @JoinTable(
        name = "material_educational_alignments",
        joinColumns = @JoinColumn(name = "material_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "educational_alignments_id", referencedColumnName = "id")
    )
    private Set<EducationalAlignment> educationalAlignments = new HashSet<>();

    @OneToMany(mappedBy = "material", cascade = REMOVE)
    @JsonIgnore
    private Set<AssignmentMaterial> assignmentMaterials = new HashSet<>();

    @ManyToMany(mappedBy = "materials")
    @Cache(usage = READ_WRITE)
    @JsonIgnore
    private Set<Milestone> milestones = new HashSet<>();

    private Boolean restricted;

    private Integer sequenceNumber;

    private String country;

    public Material id(Long id) {
        setId(id);
        return this;
    }

    public Material title(String title) {
        this.title = title;
        return this;
    }

    public Material type(String type) {
        this.type = type;
        return this;
    }

    public Material description(String description) {
        this.description = description;
        return this;
    }

    public Material externalId(String externalId) {
        this.externalId = externalId;
        return this;
    }

    public Material url(String url) {
        this.url = url;
        return this;
    }

    public Material educationalAlignments(Set<EducationalAlignment> educationalAlignments) {
        this.educationalAlignments = educationalAlignments;
        return this;
    }

    public Material addEducationalAlignments(EducationalAlignment educationalAlignment) {
        this.educationalAlignments.add(educationalAlignment);
        educationalAlignment.getMaterials().add(this);
        return this;
    }

    public Material removeEducationalAlignments(EducationalAlignment educationalAlignment) {
        this.educationalAlignments.remove(educationalAlignment);
        educationalAlignment.getMaterials().remove(this);
        return this;
    }

    public Set<Assignment> getAssignments() {
        return getAssignmentMaterials().stream().map(AssignmentMaterial::getAssignment).collect(toSet());
    }

    public Material assignments(Set<Assignment> assignments) {
        Set<AssignmentMaterial> assignmentMaterials = assignments.stream().map(assignment ->
            new AssignmentMaterial()
                .setAssignment(assignment)
                .setMaterial(this)
                .setSequenceNumber(getSequenceNumber())
            ).collect(toSet());
        setAssignmentMaterials(assignmentMaterials);
        return this;
    }

    public Material addAssignments(Assignment assignment) {
        getAssignmentMaterials().add(new AssignmentMaterial()
            .setAssignment(assignment)
            .setMaterial(this)
            .setSequenceNumber(getSequenceNumber())
        );
        return this;
    }

    public Material removeAssignments(Assignment assignment) {
        AssignmentMaterial toRemove = getAssignmentMaterials().stream().filter(am -> am.getAssignment()
            .getId().equals(assignment.getId())).findFirst().orElse(new AssignmentMaterial());
        getAssignmentMaterials().remove(toRemove);
        return this;
    }

    public Material milestones(Set<Milestone> milestones) {
        this.milestones = milestones;
        return this;
    }

    public Material addMilestone(Milestone milestone) {
        this.milestones.add(milestone);
        milestone.getMaterials().add(this);
        return this;
    }

    public Material removeMilestone(Milestone milestone) {
        this.milestones.remove(milestone);
        milestone.getMaterials().remove(this);
        return this;
    }

    public Material uploadedFile(UploadedFile uploadedFile) {
        setUploadedFile(uploadedFile);
        return this;
    }

    public Material restricted(Boolean published) {
        setRestricted(published);
        return this;
    }

    public boolean isCreatedByCurrentUser() {
        return SecurityUtils.getCurrentUserLogin().orElseThrow().equals(getCreatedBy());
    }

    public Material createdBy(String createdBy) {
        setCreatedBy(createdBy);
        return this;
    }

    public Material country(String country) {
        setCountry(country);
        return this;
    }

    @PreRemove
    protected void preRemove() {
        if (this.milestones != null) {
            this.milestones = this.milestones.stream().map(milestone -> milestone.removeMaterials(this)).collect(toSet());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Material)) {
            return false;
        }
        return id != null && id.equals(((Material) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Material{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", type='" + getType() + "'" +
            ", description='" + getDescription() + "'" +
            ", externalId='" + getExternalId() + "'" +
            ", url='" + getUrl() + "'" +
            "}";
    }
}
