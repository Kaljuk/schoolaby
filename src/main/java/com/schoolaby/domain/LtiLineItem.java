package com.schoolaby.domain;

import com.querydsl.core.annotations.QueryInit;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.Loader;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.REMOVE;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.SEQUENCE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.NONSTRICT_READ_WRITE;

@Entity
@Table(name = "lti_line_item")
@Cache(usage = NONSTRICT_READ_WRITE)
@SQLDelete(sql = "UPDATE lti_line_item SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findLtiLineItemById")
@NamedQuery(name = "findLtiLineItemById", query = "SELECT li FROM LtiLineItem li WHERE li.id = ?1 AND li.deleted IS NULL")
@Where(clause = "deleted IS NULL")
@Getter
@Setter
public class LtiLineItem extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "sequenceGenerator")
    private Long id;
    @NotNull
    private String label;
    private Long scoreMaximum;
    @ManyToOne(fetch = LAZY, optional = false)
    @QueryInit({"milestone.journey", "assignment.milestone.journey"})
    private LtiResource ltiResource;

    @OneToMany(mappedBy = "lineItem", cascade = REMOVE)
    private Set<LtiScore> scores = new HashSet<>();

    public boolean userHasScore(Long userId) {
        return scores.stream().anyMatch(score -> score.getUser().getId().equals(userId) && score.getScoreGiven() != null);
    }

    public void setScores(Collection<LtiScore> scores) {
        this.scores.clear();
        this.scores.addAll(scores);
        this.scores.forEach(score -> score.setLineItem(this));
    }

    public LtiLineItem addScore(LtiScore score) {
        score.setLineItem(this);
        this.scores.add(score);
        return this;
    }

    public LtiLineItem id(Long id) {
        setId(id);
        return this;
    }

    public LtiLineItem label(String label) {
        setLabel(label);
        return this;
    }

    public LtiLineItem scoreMaximum(Long scoreMaximum) {
        setScoreMaximum(scoreMaximum);
        return this;
    }

    public LtiLineItem ltiResource(LtiResource ltiResource) {
        setLtiResource(ltiResource);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LtiLineItem)) {
            return false;
        }
        return id != null && id.equals(((LtiLineItem) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
