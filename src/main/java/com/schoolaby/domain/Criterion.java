package com.schoolaby.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import static javax.persistence.CascadeType.*;
import static javax.persistence.FetchType.EAGER;

@Entity
@Table(name = "criterion")
@SQLDelete(sql = "UPDATE criterion SET deleted = now() WHERE id = ?")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Loader(namedQuery = "findCriterionById")
@NamedQuery(name = "findCriterionById", query = "SELECT c FROM Criterion c WHERE c.id = ?1 AND c.deleted IS NULL")
@Where(clause = "deleted IS NULL")
@Getter
@Setter
public class Criterion extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;
    private String title;
    private String description;
    @Column(name = "sequence_number")
    private Long sequenceNumber;

    @OneToMany(mappedBy = "criterion", cascade = {PERSIST, MERGE, REMOVE}, fetch = EAGER, orphanRemoval = true)
    private Set<CriterionLevel> levels = new LinkedHashSet<>();

    @ManyToOne
    private Rubric rubric;

    public Criterion setLevels(Set<CriterionLevel> levels) {
        levels.forEach(level -> level.setCriterion(this));
        this.levels = levels;
        return this;
    }
}
