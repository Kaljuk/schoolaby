package com.schoolaby.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@SQLDelete(sql = "UPDATE selected_criterion_level SET deleted = now() WHERE id = ?")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Loader(namedQuery = "findSelectedCriterionLevelById")
@NamedQuery(name = "findSelectedCriterionLevelById", query = "SELECT s FROM SelectedCriterionLevel s WHERE s.id = ?1 AND s.deleted IS NULL")
@Where(clause = "deleted IS NULL")
@Getter
@Setter
public class SelectedCriterionLevel extends AbstractAuditingEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    private String modifiedDescription;

    @ManyToOne
    private SubmissionFeedback submissionFeedback;

    @ManyToOne
    private CriterionLevel criterionLevel;
}
