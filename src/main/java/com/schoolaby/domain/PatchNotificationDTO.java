package com.schoolaby.domain;

import java.time.Instant;

public class PatchNotificationDTO {
    private Long id;
    private Instant read;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getRead() {
        return read;
    }

    public void setRead(Instant read) {
        this.read = read;
    }
}
