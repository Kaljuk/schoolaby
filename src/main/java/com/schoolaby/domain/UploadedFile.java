package com.schoolaby.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.schoolaby.security.SecurityUtils;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.Loader;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.GenerationType.SEQUENCE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

@Entity
@Getter
@Setter
@Table(name = "uploaded_file")
@Cache(usage = READ_WRITE)
@SQLDelete(sql = "UPDATE uploaded_file SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findUploadedFileById")
@NamedQuery(name = "findUploadedFileById", query = "SELECT pr FROM UploadedFile pr WHERE pr.id = ?1 AND pr.deleted IS NULL")
@Where(clause = "deleted IS NULL")
public class UploadedFile extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    private String originalName;
    private String uniqueName;
    private String type;
    private String extension;

    @ManyToOne(fetch = FetchType.LAZY)
    @Cache(usage = READ_WRITE)
    @JoinTable(
        name = "submission_feedback_uploaded_files",
        joinColumns = @JoinColumn(name = "uploaded_file_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "submission_feedback_id", referencedColumnName = "id")
    )
    @JsonIgnoreProperties("uploadedFiles")
    private SubmissionFeedback submissionFeedback;

    public boolean isCreatedByCurrentUser() {
        return SecurityUtils.getCurrentUserLogin().orElseThrow().equals(getCreatedBy());
    }

    public UploadedFile type(String type) {
        this.type = type;
        return this;
    }

    public UploadedFile extension(String extension) {
        this.extension = extension;
        return this;
    }

    public UploadedFile uniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
        return this;
    }

    public void setUniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
    }

    public UploadedFile originalName(String originalName) {
        setOriginalName(originalName);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UploadedFile)) {
            return false;
        }
        return id != null && id.equals(((UploadedFile) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UploadedFile{" +
            "id=" + getId() +
            ", uniqueName='" + getUniqueName() + "'" +
            ", type='" + getType() + "'" +
            ", extension='" + getExtension() + "'" +
            ", originalName='" + getOriginalName() + "'" +
            "}";
    }
}
