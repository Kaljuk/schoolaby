package com.schoolaby.domain;

import com.schoolaby.domain.keys.JourneyUserKey;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import java.time.Instant;
import java.util.Objects;

import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

@Entity
@Table(name = "journey_teachers")
@Cache(usage = READ_WRITE)
@Getter
@Setter
@IdClass(JourneyUserKey.class)
public class JourneyTeacher {
    @Id
    @ManyToOne
    private User user;

    @Id
    @ManyToOne
    private Journey journey;

    private Instant joinedDate;


    public JourneyTeacher user(User user) {
        setUser(user);
        return this;
    }

    public JourneyTeacher journey(Journey journey) {
        setJourney(journey);
        return this;
    }

    public JourneyTeacher joinedDate(Instant joinedDate) {
        setJoinedDate(joinedDate);
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof JourneyTeacher) {
            JourneyTeacher journeyTeacher = (JourneyTeacher) obj;
            return journeyTeacher.getUser().equals(getUser()) && journeyTeacher.getJourney().equals(getJourney());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUser(), getJourney(), getJoinedDate());
    }
}
