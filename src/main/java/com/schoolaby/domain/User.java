package com.schoolaby.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.schoolaby.config.Constants;
import com.schoolaby.domain.ExternalAuthentication.ExternalAuthenticationType;
import com.schoolaby.service.Language;
import lombok.*;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.util.*;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.FetchType.EAGER;
import static org.hibernate.annotations.CacheConcurrencyStrategy.NONSTRICT_READ_WRITE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "jhi_user")
@Cache(usage = NONSTRICT_READ_WRITE)
public class User extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 100)
    @Column(length = 50, unique = true, nullable = false)
    private String login;

    @JsonIgnore
    @Size(min = 60, max = 60)
    @Column(name = "password_hash", length = 60, nullable = false)
    private String password;

    @Size(max = 50)
    @Column(name = "first_name", length = 50)
    private String firstName;

    @Size(max = 50)
    @Column(name = "last_name", length = 50)
    private String lastName;

    @Email
    @Size(min = 5, max = 254)
    @Column(length = 254, unique = true)
    //    @Pattern(regexp = "^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$")
    private String email;

    @Builder.Default
    @NotNull
    @Column(nullable = false)
    private boolean activated = false;

    @Size(min = 2, max = 10)
    @Column(name = "lang_key", length = 10)
    private String langKey;

    private String country;

    @Size(max = 256)
    @Column(name = "image_url", length = 256)
    private String imageUrl;

    @Size(max = 20)
    @Column(name = "activation_key", length = 20)
    @JsonIgnore
    private String activationKey;

    @Size(max = 20)
    @Column(name = "reset_key", length = 20)
    @JsonIgnore
    private String resetKey;

    @Builder.Default
    @Column(name = "reset_date")
    private Instant resetDate = null;

    @Builder.Default
    @JsonIgnore
    @ManyToMany(cascade = MERGE, fetch = EAGER)
    @JoinTable(
        name = "jhi_user_authority",
        joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "authority_name", referencedColumnName = "name")}
    )
    @Cache(usage = NONSTRICT_READ_WRITE)
    @BatchSize(size = 20)
    private Set<Authority> authorities = new HashSet<>();

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "personal_code")
    private String personalCode;

    @Builder.Default
    @OneToMany(mappedBy = "creator")
    @Cache(usage = READ_WRITE)
    @JsonIgnore
    private Set<Journey> createdJourneys = new HashSet<>();

    @Builder.Default
    @OneToMany(mappedBy = "creator")
    @Cache(usage = READ_WRITE)
    @JsonIgnore
    private Set<Assignment> createdAssignments = new HashSet<>();

    @Builder.Default
    @OneToMany(mappedBy = "person", fetch = EAGER, cascade = ALL, orphanRemoval = true)
    @Cache(usage = READ_WRITE)
    private Set<PersonRole> personRoles = new HashSet<>();

    @Builder.Default
    @ManyToMany(mappedBy = "students")
    @Cache(usage = READ_WRITE)
    @JsonIgnore
    private Set<Assignment> assignments = new HashSet<>();

    @Builder.Default
    @ManyToMany(mappedBy = "authors")
    @Cache(usage = READ_WRITE)
    @JsonIgnore
    private Set<Submission> submissions = new HashSet<>();

    @Builder.Default
    @OneToMany(mappedBy = "student")
    private Set<SubmissionFeedback> submissionFeedbacks = new HashSet<>();

    @Builder.Default
    @ManyToMany(mappedBy = "people")
    @Cache(usage = READ_WRITE)
    @JsonIgnore
    private Set<Chat> chats = new HashSet<>();

    @Builder.Default
    @OneToMany(mappedBy = "user", cascade = ALL, orphanRemoval = true, fetch = EAGER)
    @Cache(usage = READ_WRITE)
    @JsonIgnore
    private Set<ExternalAuthentication> externalAuthentications = new HashSet<>();

    @Column(name = "terms_agreed")
    private boolean termsAgreed;

    @Column(name = "first_login")
    private boolean firstLogin;

    public void addPersonRole(PersonRole personRole) {
        personRole.setPerson(this);
        this.personRoles.add(personRole);
    }

    public void setPersonRoles(Set<PersonRole> personRoles) {
        this.personRoles.clear();
        personRoles.forEach(this::addPersonRole);
    }

    public boolean hasPersonRole(PersonRole personRole) {
        return getPersonRoles().stream()
            .filter(existingRole -> existingRole.getSchool() != null && existingRole.getSchool().getEhisId() != null)
            .anyMatch(existingRole ->
                existingRole.getSchool().getEhisId().equals(personRole.getSchool().getEhisId()) &&
                    existingRole.getRole().equals(personRole.getRole())
            );
    }

    public User personRoles(Set<PersonRole> personRoles) {
        setPersonRoles(personRoles);
        return this;
    }

    public void setLangKey(String langKey) {
        Optional<Language> supportedLanguage = Arrays.stream(Language.values()).filter(language -> language.name().equalsIgnoreCase(langKey)).findFirst();
        if (supportedLanguage.isPresent()) {
            this.langKey = langKey;
        } else {
            this.langKey = Constants.DEFAULT_LANGUAGE;
        }
    }

    public void addExternalAuthentication(ExternalAuthentication externalAuthentication) {
        this.externalAuthentications.add(externalAuthentication);
        externalAuthentication.setUser(this);
    }

    public User addExternalAuthentication(String sub, ExternalAuthenticationType type) {
        ExternalAuthentication authentication = new ExternalAuthentication();
        authentication.setId(new ExternalAuthentication.Id(sub, type));
        this.addExternalAuthentication(authentication);

        return this;
    }

    public User addAuthority(Authority authority) {
        this.authorities.add(authority);
        return this;
    }

    public User id(Long id) {
        setId(id);
        return this;
    }

    public HashMap<String, Object> getClaims() {
        HashMap<String, Object> profileClaims = new HashMap<>();
        profileClaims.put("given_name", getFirstName());
        profileClaims.put("family_name", getLastName());
        profileClaims.put("email", getEmail());
        profileClaims.put("email_verified", isActivated());
        profileClaims.put("schools", getSchoolClaims());
        return profileClaims;
    }

    private List<HashMap<String, Object>> getSchoolClaims() {
        List<HashMap<String, Object>> schoolClaims = new ArrayList<>();
        getPersonRoles().forEach(personRole -> {
            if(personRole.getSchool() != null) {
                schoolClaims.add(personRole.getClaims());
            }
        });
        return schoolClaims;
    }

    public boolean hasAuthorities() {
        return !getAuthorities().isEmpty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof User)) {
            return false;
        }
        return id != null && id.equals(((User) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return (
            "User{" +
                "login='" +
                login +
                '\'' +
                ", firstName='" +
                firstName +
                '\'' +
                ", lastName='" +
                lastName +
                '\'' +
                ", email='" +
                email +
                '\'' +
                '\'' +
                ", activated='" +
                activated +
                '\'' +
                "}"
        );
    }
}
