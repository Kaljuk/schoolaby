package com.schoolaby.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.schoolaby.service.dto.states.EntityState;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.schoolaby.common.DateUtils.EST_TZ;
import static com.schoolaby.config.filter.TenantServiceAspect.FILTER_HIDDEN;
import static com.schoolaby.config.filter.TenantServiceAspect.STUDENT_FILTER;
import static com.schoolaby.domain.Assignment.STUDENT_IN_ASSIGNMENT_STUDENTS_OR_ASSIGNMENT_STUDENTS_EMPTY;
import static com.schoolaby.domain.GradingScheme.SCHEMES_WITHOUT_GRADE;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.stream.Collectors.toSet;
import static javax.persistence.CascadeType.*;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.SEQUENCE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;


@Entity
@Getter
@Setter
@Table(name = "assignment")
@Cache(usage = READ_WRITE)
@SQLDelete(sql = "UPDATE assignment SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findAssignmentById")
@NamedQuery(name = "findAssignmentById", query = "SELECT a FROM Assignment a WHERE a.id = ?1 AND a.deleted IS NULL")
@Where(clause = "deleted IS NULL")
@FilterDef(name = FILTER_HIDDEN)
@FilterDef(name = STUDENT_FILTER,
    parameters = @ParamDef(name = "currentUserId", type = "long"))
@Filter(name = STUDENT_FILTER, condition = STUDENT_IN_ASSIGNMENT_STUDENTS_OR_ASSIGNMENT_STUDENTS_EMPTY)
@Filter(name = FILTER_HIDDEN, condition = "published IS NOT NULL AND published < NOW()")
public class Assignment extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String STUDENT_IN_ASSIGNMENT_STUDENTS_OR_ASSIGNMENT_STUDENTS_EMPTY = "(EXISTS(SELECT * FROM assignment_students ast WHERE id = ast.assignment_id AND ast.students_id = :currentUserId) OR NOT EXISTS(SELECT * FROM assignment_students ast WHERE id = ast.assignment_id))";

    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "deadline")
    private Instant deadline;

    @Column(name = "flexible_deadline")
    private boolean flexibleDeadline;

    @Column(name = "requires_submission")
    private boolean requiresSubmission;

    private Boolean gradeGroupMembersIndividually = false;

    @OneToMany(mappedBy = "assignment", cascade = REMOVE)
    @Cache(usage = READ_WRITE)
    private Set<Submission> submissions = new HashSet<>();

    @OneToMany(mappedBy = "assignment", cascade = {PERSIST, MERGE, REMOVE}, orphanRemoval = true)
    private Set<AssignmentMaterial> assignmentMaterials = new HashSet<>();

    @ManyToMany
    @Cache(usage = READ_WRITE)
    @JoinTable(
        name = "assignment_students",
        joinColumns = @JoinColumn(name = "assignment_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "students_id", referencedColumnName = "id")
    )
    private Set<User> students = new HashSet<>();

    @ManyToMany
    @Cache(usage = READ_WRITE)
    @JoinTable(
        name = "assignment_educational_alignments",
        joinColumns = @JoinColumn(name = "assignment_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "educational_alignments_id", referencedColumnName = "id")
    )
    private Set<EducationalAlignment> educationalAlignments = new HashSet<>();

    @OneToMany(cascade = {PERSIST, MERGE, REMOVE}, fetch = EAGER, mappedBy = "assignment", orphanRemoval = true)
    @Cache(usage = READ_WRITE)
    private Set<Group> groups = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "assignments", allowSetters = true)
    private GradingScheme gradingScheme;

    @ManyToOne(cascade = {REFRESH, PERSIST})
    @JsonIgnoreProperties(value = "assignments", allowSetters = true)
    private Milestone milestone;

    @ManyToOne
    @JsonIgnoreProperties(value = "createdAssignments", allowSetters = true)
    private User creator;

    @OneToMany(mappedBy = "assignment", cascade = ALL, orphanRemoval = true)
    private Set<LtiResource> ltiResources = new HashSet<>();

    @OneToMany(mappedBy = "assignment", cascade = REMOVE)
    private Set<Notification> notifications = new HashSet<>();

    @Column(name = "published")
    private Instant published;

    @OneToMany(mappedBy = "assignment", cascade = {PERSIST, MERGE, REMOVE})
    private Set<AssignmentState> states = new HashSet<>();

    public boolean isGroupAssignment() {
        return getGroups() != null && !getGroups().isEmpty();
    }

    public Assignment addGroup(Group group) {
        this.groups.add(group);
        group.setAssignment(this);
        return this;
    }

    public Assignment setGroups(Set<Group> groups) {
        this.groups = groups;
        groups.forEach(group -> group.setAssignment(this));
        return this;
    }

    public Assignment id(final Long id) {
        setId(id);
        return this;
    }

    public Assignment title(String title) {
        this.title = title;
        return this;
    }


    public Assignment description(String description) {
        this.description = description;
        return this;
    }


    public Assignment deadline(Instant deadline) {
        this.deadline = deadline;
        return this;
    }

    public Assignment flexibleDeadline(Boolean flexibleDeadline) {
        this.flexibleDeadline = flexibleDeadline;
        return this;
    }

    public Assignment requiresSubmission(Boolean requiresSubmission) {
        this.requiresSubmission = requiresSubmission;
        return this;
    }

    public Assignment submissions(Set<Submission> submissions) {
        this.submissions = submissions;
        return this;
    }

    public Assignment addSubmission(Submission submission) {
        if (submission != null) {
            submission.setAssignment(this);
            this.submissions.add(submission);
        }
        return this;
    }

    public Set<Material> getMaterials() {
        return getAssignmentMaterials().stream().map(AssignmentMaterial::getMaterial).collect(toSet());
    }

    public Assignment materials(Set<Material> materials) {
        Set<AssignmentMaterial> assignmentMaterials = materials.stream().map(material -> new AssignmentMaterial()
            .setAssignment(this)
            .setMaterial(material)
            .setSequenceNumber(material.getSequenceNumber())
        ).collect(toSet());
        setAssignmentMaterials(assignmentMaterials);
        return this;
    }

    public Assignment addMaterial(Material material) {
        getAssignmentMaterials().add(new AssignmentMaterial()
            .setAssignment(this)
            .setMaterial(material)
            .setSequenceNumber(material.getSequenceNumber()));
        return this;
    }

    public Set<AssignmentMaterial> removeMaterials(Material material) {
        AssignmentMaterial toRemove = getAssignmentMaterials().stream().filter(am -> am.getMaterial()
            .getId().equals(material.getId())).findFirst().orElse(new AssignmentMaterial());
        getAssignmentMaterials().remove(toRemove);
        return getAssignmentMaterials();
    }

    public Assignment addStudent(User person) {
        this.students.add(person);
        person.getAssignments().add(this);
        return this;
    }

    public Assignment addEducationalAlignment(EducationalAlignment educationalAlignment) {
        this.educationalAlignments.add(educationalAlignment);
        educationalAlignment.getAssignments().add(this);
        return this;
    }

    public Assignment gradingScheme(GradingScheme gradingScheme) {
        setGradingScheme(gradingScheme);
        return this;
    }

    public Assignment milestone(Milestone milestone) {
        setMilestone(milestone);
        milestone.getAssignments().add(this);
        return this;
    }

    public Assignment creator(User person) {
        this.creator = person;
        return this;
    }

    public Assignment removeGroup(Group group) {
        this.groups.remove(group);
        return this;
    }

    public boolean removeStudent(Long studentId) {
        return students.removeIf(p -> studentId.equals(p.getId()));
    }

    public void removeStudentFromGroups(Long studentId) {
        getGroups().forEach(group -> {
            group.removeStudent(studentId);
            if (group.getStudents().isEmpty()) {
                removeGroup(group);
            }
        });
    }

    public boolean removeSubmission(Long submissionId) {
        return submissions.removeIf(s -> submissionId.equals(s.getId()));
    }

    public void removeSubmissions(Set<Long> submissionIds) {
        Set<Submission> newSubmissions = getSubmissions()
            .stream()
            .filter(submission -> !submissionIds.contains(submission.getId()))
            .collect(toSet());
        setSubmissions(newSubmissions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Assignment)) {
            return false;
        }
        return id != null && id.equals(((Assignment) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Assignment{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", deadline='" + getDeadline() + "'" +
            ", flexibleDeadline='" + isFlexibleDeadline() + "'" +
            "}";
    }


    public void setLtiResources(Set<LtiResource> ltiResources) {
        ltiResources.forEach(ltiResource -> ltiResource.setAssignment(this));
        this.ltiResources = ltiResources;
    }

    public Integer getSubmissionsCount() {
        return (int) getSubmittedSubmissions().stream().filter(s -> s.isSubmitted() || s.isGraded()).count();
    }

    public boolean deadlinePassed() {
        return now().isAfter(getDeadline());
    }

    public boolean dueByTomorrow() {
        return !deadlinePassed() && DAYS.between(ZonedDateTime.now(EST_TZ).truncatedTo(DAYS), getDeadline().atZone(EST_TZ).truncatedTo(DAYS)) < 2;
    }

    public Optional<Submission> findSubmission(Long userId) {
        return submissions.stream().filter(submission -> submission.hasAuthor(userId)).findFirst();
    }

    private Optional<Submission> findSubmission() {
        return findSubmission(getCurrentUserId());
    }

    public Integer getAverageScore(Long studentId) {
        Optional<Submission> submission = findSubmission(studentId);
        GradingScheme gradingScheme = getGradingScheme();

        if (submission.isPresent() && !SCHEMES_WITHOUT_GRADE.contains(gradingScheme.getCode())) {
            String grade = submission.get().findFeedback(studentId).map(SubmissionFeedback::getGrade).orElse(null);
            if (grade != null) {
                return gradingScheme.getPercentage(grade);
            }
        }
        return null;
    }

    public Assignment addLtiResource(LtiResource ltiResource) {
        this.ltiResources.add(ltiResource);
        ltiResource.setAssignment(this);
        return this;
    }

    public boolean isHighestGrade() {
        return findSubmission()
            .map(Submission::isHighestGrade)
            .orElse(false);
    }

    private Set<Submission> getAssignedGroupsSubmissions() {
        return getSubmissions().stream()
            .filter(submission -> getGroups().stream().anyMatch(group -> group.equals(submission.getGroup()))).collect(toSet());
    }

    protected Set<Submission> getAssignedStudentsSubmissions() {
        return getSubmissions().stream()
            .filter(submission -> submission.getAuthors().stream()
                .anyMatch(author -> getAssignedStudents().contains(author))).collect(toSet());
    }

    public Set<User> getAssignedStudents() {
        if (!getStudents().isEmpty()) {
            return getStudents();
        }
        return getMilestone().getJourney().getJourneyStudentsUsers();
    }

    public Assignment published(Instant published) {
        setPublished(published);
        return this;
    }

    public boolean isHidden() {
        return published == null || published.isAfter(now());
    }

    public Assignment states(Set<AssignmentState> states) {
        setStates(states);
        return this;
    }

    public void addState(AssignmentState state) {
        getStates().add(state);
    }

    public Set<Submission> getSubmittedSubmissions() {
        return isGroupAssignment() ? getAssignedGroupsSubmissions() : getAssignedStudentsSubmissions();
    }

    private Optional<AssignmentState> getExistingState(AssignmentState state) {
        return getStates()
            .stream()
            .filter(assignmentState -> assignmentState.equals(state))
            .findFirst();
    }

    public void calculateStates() {
        calculateStateByAuthority(getMilestone().getJourney().getJourneyStudentsUsers(), STUDENT);
        calculateStateByAuthority(getMilestone().getJourney().getJourneyTeachersUsers(), TEACHER);
    }

    private void calculateStateByAuthority(Set<User> users, String authorityName) {
        users.forEach(user -> calculateAndSaveState(new AssignmentState()
            .assignment(this)
            .user(user)
            .authority(new Authority().name(authorityName))));
    }

    private void calculateAndSaveState(AssignmentState state) {
        getExistingState(state).ifPresentOrElse(AssignmentState::calculate,
            () -> addState(state.calculate()));
    }

    public EntityState getState() {
        Long userId = getCurrentUserId();
        boolean isStudent = getMilestone().getJourney().isStudentInJourney(userId);
        boolean isTeacher = getMilestone().getJourney().isTeacherInJourney(userId);

        if (!isStudent && !isTeacher) {
            return null;
        }
        String authorityName = isTeacher ? TEACHER : STUDENT;
        return getState(getCurrentUserId(), authorityName);
    }

    public EntityState getState(Long userId, String authorityName) {
        Optional<AssignmentState> state = getStates()
            .stream().filter(assignmentState -> assignmentState.getUser().getId().equals(userId) && assignmentState.getAuthority().getName().equals(authorityName)).findFirst();
        return state.map(AssignmentState::getState).orElse(null);
    }
}
