package com.schoolaby.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "journey_signup_code")
@Getter
@Setter
public class JourneySignupCode implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "sign_up_code", nullable = false)
    private String signUpCode;

    @NotNull
    @OneToOne
    private Authority authority;

    @ManyToOne
    private Journey journey;

    @NotNull
    private boolean oneTime;

    @NotNull
    private boolean used;

    public JourneySignupCode signUpCode(String signUpCode) {
        setSignUpCode(signUpCode);
        return this;
    }

    public JourneySignupCode authority(Authority authority) {
        setAuthority(authority);
        return this;
    }

    public JourneySignupCode journey(Journey journey) {
        setJourney(journey);
        return this;
    }

    public boolean isUsable() {
        return !oneTime || !used;
    }
}
