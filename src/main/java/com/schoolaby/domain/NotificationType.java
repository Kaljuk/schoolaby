package com.schoolaby.domain;

public enum NotificationType {
    MESSAGE,
    GENERAL
}
