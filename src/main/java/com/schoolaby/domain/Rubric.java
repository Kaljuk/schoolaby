package com.schoolaby.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.*;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import static javax.persistence.CascadeType.*;
import static javax.persistence.FetchType.EAGER;

@Entity
@SQLDelete(sql = "UPDATE rubric SET deleted = now() WHERE id = ?")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Loader(namedQuery = "findRubricById")
@NamedQuery(name = "findRubricById", query = "SELECT r FROM Rubric r WHERE r.id = ?1 AND r.deleted IS NULL")
@Where(clause = "deleted IS NULL")
@Getter
@Setter
public class Rubric extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;
    private String title;
    private Boolean isTemplate;

    @OneToMany(mappedBy = "rubric", cascade = {PERSIST, MERGE, REMOVE}, fetch = EAGER, orphanRemoval = true)
    private Set<Criterion> criterions = new LinkedHashSet<>();

    @OneToOne
    private Assignment assignment;

    public Rubric setCriterions(Set<Criterion> criterions) {
        criterions.forEach(criterionLevel -> criterionLevel.setRubric(this));
        this.criterions = criterions;
        return this;
    }
}
