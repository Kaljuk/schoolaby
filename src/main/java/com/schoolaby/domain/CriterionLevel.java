package com.schoolaby.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Set;

import static javax.persistence.CascadeType.REMOVE;

@Entity
@Table(name = "criterion_level")
@SQLDelete(sql = "UPDATE criterion_level SET deleted = now() WHERE id = ?")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Loader(namedQuery = "findLevelById")
@NamedQuery(name = "findLevelById", query = "SELECT l FROM CriterionLevel l WHERE l.id = ?1 AND l.deleted IS NULL")
@Where(clause = "deleted IS NULL")
@Getter
@Setter
public class CriterionLevel extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;
    private String title;
    private Long points;
    private String description;
    @Column(name = "sequence_number")
    private Long sequenceNumber;

    @ManyToOne
    private Criterion criterion;

    @OneToMany(mappedBy = "criterionLevel", cascade = {REMOVE})
    private Set<SelectedCriterionLevel> selectedCriterionLevels;
}

