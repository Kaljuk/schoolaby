package com.schoolaby.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * Idea taken from https://projektid.hitsa.ee/display/HAK/JSON-LD+skeem
 * Abovementioned schema is not final, therefor this is subject to change
 */
@Entity
@Table(name = "educational_alignment")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SQLDelete(sql = "UPDATE educational_alignment SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findEducationalAlignmentById")
@NamedQuery(name = "findEducationalAlignmentById", query = "SELECT ea FROM EducationalAlignment ea WHERE ea.id = ?1 AND ea.deleted IS NULL")
@Where(clause = "deleted IS NULL")
public class EducationalAlignment extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @NotNull
    @Column(name = "alignment_type", nullable = false)
    private String alignmentType;

    @NotNull
    @Column(name = "country", nullable = false)
    private String country;

    @NotNull
    @Column(name = "target_name", nullable = false)
    private String targetName;

    @Column(name = "target_url")
    private String targetUrl;

    @Column(name = "start_date")
    private Instant startDate;

    @Column(name = "end_date")
    private Instant endDate;

    @Column(name = "taxon_id")
    private Long taxonId;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(
        name = "educational_alignment_children",
        joinColumns = @JoinColumn(name = "educational_alignment_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "children_id", referencedColumnName = "id")
    )
    private Set<EducationalAlignment> children = new HashSet<>();

    @ManyToMany(mappedBy = "educationalAlignments")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    private Set<Milestone> milestones = new HashSet<>();

    @ManyToMany(mappedBy = "educationalAlignments")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    private Set<Material> materials = new HashSet<>();

    @ManyToMany(mappedBy = "educationalAlignments")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    private Set<Assignment> assignments = new HashSet<>();

    @ManyToMany(mappedBy = "children")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    private Set<EducationalAlignment> parents = new HashSet<>();

    @OneToOne
    private Subject subject;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public EducationalAlignment title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlignmentType() {
        return alignmentType;
    }

    public EducationalAlignment alignmentType(String alignmentType) {
        this.alignmentType = alignmentType;
        return this;
    }

    public void setAlignmentType(String alignmentType) {
        this.alignmentType = alignmentType;
    }

    public String getCountry() {
        return country;
    }

    public EducationalAlignment country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTargetName() {
        return targetName;
    }

    public EducationalAlignment targetName(String targetName) {
        this.targetName = targetName;
        return this;
    }

    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public EducationalAlignment targetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
        return this;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public EducationalAlignment startDate(Instant startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public EducationalAlignment endDate(Instant endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Set<EducationalAlignment> getChildren() {
        return children;
    }

    public EducationalAlignment children(Set<EducationalAlignment> educationalAlignments) {
        this.children = educationalAlignments;
        return this;
    }

    public EducationalAlignment addChildren(EducationalAlignment educationalAlignment) {
        this.children.add(educationalAlignment);
        educationalAlignment.getParents().add(this);
        return this;
    }

    public EducationalAlignment removeChildren(EducationalAlignment educationalAlignment) {
        this.children.remove(educationalAlignment);
        educationalAlignment.getParents().remove(this);
        return this;
    }

    public void setChildren(Set<EducationalAlignment> educationalAlignments) {
        this.children = educationalAlignments;
    }

    public Set<Milestone> getMilestones() {
        return milestones;
    }

    public EducationalAlignment milestones(Set<Milestone> milestones) {
        this.milestones = milestones;
        return this;
    }

    public EducationalAlignment addMilestones(Milestone milestone) {
        this.milestones.add(milestone);
        milestone.getEducationalAlignments().add(this);
        return this;
    }

    public EducationalAlignment removeMilestones(Milestone milestone) {
        this.milestones.remove(milestone);
        milestone.getEducationalAlignments().remove(this);
        return this;
    }

    public void setMilestones(Set<Milestone> milestones) {
        this.milestones = milestones;
    }

    public Set<Material> getMaterials() {
        return materials;
    }

    public EducationalAlignment materials(Set<Material> materials) {
        this.materials = materials;
        return this;
    }

    public EducationalAlignment addMaterials(Material material) {
        this.materials.add(material);
        material.getEducationalAlignments().add(this);
        return this;
    }

    public EducationalAlignment removeMaterials(Material material) {
        this.materials.remove(material);
        material.getEducationalAlignments().remove(this);
        return this;
    }

    public void setMaterials(Set<Material> materials) {
        this.materials = materials;
    }

    public Set<Assignment> getAssignments() {
        return assignments;
    }

    public EducationalAlignment assignments(Set<Assignment> assignments) {
        this.assignments = assignments;
        return this;
    }

    public EducationalAlignment addAssignments(Assignment assignment) {
        this.assignments.add(assignment);
        assignment.getEducationalAlignments().add(this);
        return this;
    }

    public EducationalAlignment removeAssignments(Assignment assignment) {
        this.assignments.remove(assignment);
        assignment.getEducationalAlignments().remove(this);
        return this;
    }

    public void setAssignments(Set<Assignment> assignments) {
        this.assignments = assignments;
    }

    public Set<EducationalAlignment> getParents() {
        return parents;
    }

    public EducationalAlignment parents(Set<EducationalAlignment> educationalAlignments) {
        this.parents = educationalAlignments;
        return this;
    }

    public EducationalAlignment addParents(EducationalAlignment educationalAlignment) {
        this.parents.add(educationalAlignment);
        educationalAlignment.getChildren().add(this);
        return this;
    }

    public EducationalAlignment removeParents(EducationalAlignment educationalAlignment) {
        this.parents.remove(educationalAlignment);
        educationalAlignment.getChildren().remove(this);
        return this;
    }

    public void setParents(Set<EducationalAlignment> educationalAlignments) {
        this.parents = educationalAlignments;
    }

    public Long getTaxonId() {
        return taxonId;
    }

    public void setTaxonId(Long taxonId) {
        this.taxonId = taxonId;
    }

    public EducationalAlignment taxonId(Long taxonId) {
        setTaxonId(taxonId);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EducationalAlignment)) {
            return false;
        }
        return id != null && id.equals(((EducationalAlignment) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EducationalAlignment{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", alignmentType='" + getAlignmentType() + "'" +
            ", country='" + getCountry() + "'" +
            ", targetName='" + getTargetName() + "'" +
            ", targetUrl='" + getTargetUrl() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            "}";
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }
}
