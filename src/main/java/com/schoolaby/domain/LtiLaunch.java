package com.schoolaby.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@SQLDelete(sql = "UPDATE lti_launch SET deleted = now() WHERE id = ?")
@Where(clause = "deleted IS NULL")
public class LtiLaunch extends AbstractAuditingEntity {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    @ManyToOne(fetch = LAZY)
    private User user;
    @ManyToOne(fetch = LAZY, optional = false)
    @JsonIgnoreProperties(value = "ltiApp", allowGetters = true)
    private LtiResource ltiResource;
    private String result;

    public Long getId() {
        return id;
    }

    public LtiLaunch setId(Long id) {
        this.id = id;
        return this;
    }

    public User getUser() {
        return user;
    }

    public LtiLaunch setUser(User user) {
        this.user = user;
        return this;
    }

    public LtiResource getLtiResource() {
        return ltiResource;
    }

    public LtiLaunch setLtiResource(LtiResource ltiResource) {
        this.ltiResource = ltiResource;
        return this;
    }

    public String getResult() {
        return result;
    }

    public LtiLaunch setResult(String result) {
        this.result = result;
        return this;
    }

    public LtiLaunch id(Long id) {
        setId(id);
        return this;
    }

    public LtiLaunch user(User user) {
        setUser(user);
        return this;
    }

    public LtiLaunch ltiResource(LtiResource ltiResource) {
        setLtiResource(ltiResource);
        return this;
    }

    public LtiLaunch result(String result) {
        setResult(result);
        return this;
    }

    @Override
    @JsonIgnore(value = false)
    public Instant getCreatedDate() {
        return super.getCreatedDate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LtiLaunch)) {
            return false;
        }
        return id != null && id.equals(((LtiLaunch) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
