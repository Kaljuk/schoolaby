package com.schoolaby.domain;

import com.querydsl.core.annotations.QueryInit;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.Loader;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

import static javax.persistence.GenerationType.SEQUENCE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.NONSTRICT_READ_WRITE;

@Entity
@Table(name = "lti_score")
@Cache(usage = NONSTRICT_READ_WRITE)
@SQLDelete(sql = "UPDATE lti_score SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findLtiScoreById")
@NamedQuery(name = "findLtiScoreById", query = "SELECT ls FROM LtiScore ls WHERE ls.id = ?1 AND ls.deleted IS NULL")
@Where(clause = "deleted IS NULL")
@Getter
@Setter
public class LtiScore extends AbstractAuditingEntity implements Serializable {
    public static final String GRADING_PROGRESS_FULLY_GRADED = "FullyGraded";
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;
    @NotNull
    private Instant timestamp;
    @Column(columnDefinition = "NUMERIC(10,2)")
    private Double scoreGiven;
    @Column(columnDefinition = "NUMERIC(10,2)")
    private Double scoreMaximum;
    private String comment;
    private String activityProgress;
    private String gradingProgress;
    @ManyToOne
    @NotNull
    private User user;
    @ManyToOne
    @NotNull
    @QueryInit("ltiResource.assignment")
    private LtiLineItem lineItem;

    public LtiResource getLtiResource() {
        return lineItem.getLtiResource();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LtiScore)) {
            return false;
        }
        return id != null && id.equals(((LtiScore) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
