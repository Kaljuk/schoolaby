package com.schoolaby.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.schoolaby.service.dto.LtiVersion.LTI_ADVANTAGE_VERSION;
import static javax.persistence.GenerationType.SEQUENCE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.NONSTRICT_READ_WRITE;

@Entity
@Table(name = "lti_app")
@Cache(usage = NONSTRICT_READ_WRITE)
@Getter
@Setter
public class LtiApp extends AbstractAuditingEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "sequenceGenerator")
    private Long id;
    @NotNull
    private String name;
    private String description;
    private String imageUrl;
    private String version;
    private String clientId;
    private Boolean contentItemSelectionEnabled = false;
    private Boolean resubmittable;

    //These can also be specified by the user in LtiConfig
    private String launchUrl;
    private String deepLinkingUrl;
    private String loginInitiationUrl;
    private String redirectHost;
    private String jwksUrl;

    @OneToMany(mappedBy = "ltiApp")
    private List<LtiConfig> configs = new ArrayList<>();

    public LtiApp addConfig(LtiConfig config) {
        this.configs.add(config);
        config.setLtiApp(this);
        return this;
    }

    public boolean isLtiAdvantage() {
        return this.getVersion().equals(LTI_ADVANTAGE_VERSION.getValue());
    }
}
