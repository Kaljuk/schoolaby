package com.schoolaby.domain;

import com.schoolaby.service.dto.states.EntityState;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import java.io.Serializable;

import static com.schoolaby.service.dto.states.EntityState.*;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

@Entity
@Cache(usage = READ_WRITE)
@Getter
@Setter
public class JourneyState implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "journeyStateSeqGenerator")
    @SequenceGenerator(name = "journeyStateSeqGenerator", sequenceName = "journey_state_seq", allocationSize = 1)
    private Long id;

    @ManyToOne
    private Journey journey;

    @ManyToOne
    private User user;

    @ManyToOne
    @JoinColumn(name = "authority_name", referencedColumnName = "name")
    private Authority authority;

    private String state;

    public EntityState getState() {
        return state != null ? EntityState.valueOf(state) : null;
    }

    public JourneyState setState(EntityState state) {
        if(state != null) {
            this.state = state.toString();
        }
        return this;
    }

    public JourneyState journey(Journey journey) {
        setJourney(journey);
        return this;
    }

    public JourneyState user(User user) {
        setUser(user);
        return this;
    }

    public JourneyState state(EntityState entityState) {
        setState(entityState);
        return this;
    }

    public JourneyState authority(Authority authority) {
        setAuthority(authority);
        return this;
    }

    public JourneyState calculate() {
        validateFieldsForCalculation();

        boolean completed = false;
        if (!getJourney().getMilestones().isEmpty()) {
            completed = getJourney()
                .getMilestones()
                .stream()
                .filter(milestone -> !FAILED.equals(milestone.getState(getUser().getId(), authority.getName())))
                .allMatch(milestone -> COMPLETED.equals(milestone.getState(getUser().getId(), authority.getName())));
        }
        boolean notStarted = true;
        if (!getJourney().getMilestones().isEmpty()) {
            notStarted = getJourney()
                .getMilestones()
                .stream()
                .allMatch(milestone -> NOT_STARTED.equals(milestone.getState(getUser().getId(), authority.getName())));
        }

        if (completed && getJourney().hasEndDatePassed()) {
            setState(COMPLETED);
        } else if (notStarted && getJourney().isStartDateInFuture()) {
            setState(NOT_STARTED);
        } else {
            setState(IN_PROGRESS);
        }
        return this;
    }

    private void validateFieldsForCalculation() {
        if (getUser() == null) {
            throw new IllegalStateException("User field is required to calculate state");
        } else if (getJourney() == null) {
            throw new IllegalStateException("Journey field is required to calculate state");
        } else if (getAuthority() == null) {
            throw new IllegalStateException("Authority field is required to calculate state");
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof JourneyState) {
            JourneyState journeyState = (JourneyState) obj;

            return journeyState.getJourney().getId().equals(getJourney().getId()) &&
                journeyState.getUser().getId().equals(getUser().getId()) &&
                journeyState.getAuthority().getName().equals(getAuthority().getName());
        }
        return false;
    }
}
