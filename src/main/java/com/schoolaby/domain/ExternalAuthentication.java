package com.schoolaby.domain;

import org.hibernate.annotations.Type;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

import static javax.persistence.EnumType.STRING;

@Entity
@Table(name = "external_authentication")
public class ExternalAuthentication extends AbstractAuditingEntity {
    @EmbeddedId
    private Id id;
    @Type(type = "jsonb")
    private String data;
    @ManyToOne
    private User user;

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ExternalAuthenticationType getType() {
        return this.getId().getType();
    }

    public String getExternalId() {
        return this.getId().getExternalId();
    }

    public void setExternalId(String externalId) {
        this.getId().setExternalId(externalId);
    }

    @Embeddable
    public static class Id implements Serializable {
        private String externalId;

        @Enumerated(STRING)
        @Type(type = "pgsql_enum")
        private ExternalAuthenticationType type;

        public Id() {
        }

        public Id(String externalId, ExternalAuthenticationType type) {
            this.externalId = externalId;
            this.type = type;
        }

        public String getExternalId() {
            return externalId;
        }

        public void setExternalId(String externalId) {
            this.externalId = externalId;
        }

        public ExternalAuthenticationType getType() {
            return type;
        }

        public void setType(ExternalAuthenticationType type) {
            this.type = type;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Id)) return false;
            Id that = (Id) o;
            return Objects.equals(getExternalId(), that.getExternalId()) && Objects.equals(getType(), that.getType());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getExternalId(), getType());
        }
    }

    public enum ExternalAuthenticationType {
        HARID, GOOGLE, AZURE, YAHOO, EKOOL
    }
}
