package com.schoolaby.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Loader;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import java.io.Serializable;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@Loader(namedQuery = "findCurriculumById")
@NamedQuery(name = "findCurriculumById", query = "SELECT c FROM Curriculum c WHERE c.id = ?1")
@Getter
@Setter
public class Curriculum implements Serializable {

    @Id
    private Long id;

    private String title;

    private String country;

    private Integer sequenceNumber;

    private boolean requiresSubject = true;

    public Curriculum id(Long id) {
        setId(id);
        return this;
    }

    public Curriculum title(String title) {
        setTitle(title);
        return this;
    }

    public Curriculum country(String country) {
        setCountry(country);
        return this;
    }

    public Curriculum sequenceNumber(Integer sequenceNumber) {
        setSequenceNumber(sequenceNumber);
        return this;
    }

    public Curriculum requiresSubject(boolean requiresSubject) {
        setRequiresSubject(requiresSubject);
        return this;
    }
}
