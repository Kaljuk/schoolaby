package com.schoolaby.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.Loader;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.REMOVE;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.SEQUENCE;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

@Entity
@Getter
@Setter
@Table(name = "\"group\"")
@Cache(usage = READ_WRITE)
@SQLDelete(sql = "UPDATE \"group\" SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findGroupById")
@NamedQuery(name = "findGroupById", query = "SELECT g FROM Group g WHERE g.id = ?1 AND g.deleted IS NULL")
@Where(clause = "deleted IS NULL")
public class Group extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(nullable = false)
    private String name;

    @ManyToMany(fetch = EAGER)
    @Cache(usage = READ_WRITE)
    @JoinTable(
        name = "group_students",
        joinColumns = @JoinColumn(name = "group_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "students_id", referencedColumnName = "id")
    )
    private Set<User> students = new HashSet<>();

    @OneToMany(cascade = REMOVE, orphanRemoval = true, mappedBy = "group")
    private Set<Submission> submissions = new HashSet<>();

    @OneToMany(mappedBy = "group")
    private Set<SubmissionFeedback> submissionFeedbacks = new HashSet<>();

    @ManyToOne
    private Assignment assignment;

    public boolean removeStudent(Long studentId) {
        return students.removeIf(p -> studentId.equals(p.getId()));
    }

    public Group addStudent(User student) {
        this.students.add(student);
        return this;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Group)) {
            return false;
        }
        return id != null && id.equals(((Group) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
