package com.schoolaby.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.Loader;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.*;
import static javax.persistence.FetchType.LAZY;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;

@Entity
@Getter
@Setter
@Cache(usage = READ_WRITE)
@Table(name = "submission_feedback")
@SQLDelete(sql = "UPDATE submission_feedback SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findSubmissionFeedbackById")
@NamedQuery(name = "findSubmissionFeedbackById", query = "SELECT pr FROM SubmissionFeedback pr WHERE pr.id = ?1 AND pr.deleted IS NULL")
@Where(clause = "deleted IS NULL")
public class SubmissionFeedback extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    private String grade;

    private String value;

    @NotNull
    private Instant feedbackDate;

    @ManyToOne(fetch = LAZY)
    private User student;

    @ManyToOne(fetch = LAZY)
    private Group group;

    @ManyToOne(fetch = LAZY)
    private User creator;

    @ManyToOne(cascade = {PERSIST, MERGE})
    private Submission submission;

    @OneToMany
    @Cache(usage = READ_WRITE)
    @JoinTable(
        name = "submission_feedback_uploaded_files",
        joinColumns = @JoinColumn(name = "submission_feedback_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "uploaded_file_id", referencedColumnName = "id")
    )
    @JsonIgnoreProperties("submissionFeedback")
    private Set<UploadedFile> uploadedFiles = new HashSet<>();

    @OneToMany(mappedBy = "submissionFeedback", cascade = {PERSIST, MERGE, REMOVE}, orphanRemoval = true)
    private Set<SelectedCriterionLevel> selectedCriterionLevels = new HashSet<>();

    public void setGrade(String grade) {
        if (grade == null) {
            this.grade = null;
            return;
        }
        this.grade = grade.toUpperCase();
    }

    public boolean isGraded() {
        return grade != null && !isEmpty(grade);
    }

    public boolean isRejected() {
        return isGraded() && !hasPassingGrade() && isResubmittable();
    }

    public boolean hasPassingGrade() {
        return submission.getAssignment().getGradingScheme().isPassingGrade(grade);
    }

    private boolean isResubmittable() {
        return submission.isResubmittable();
    }

    public void setSubmission(Submission submission) {
        this.submission = submission;
        submission.addSubmissionFeedback(this);
    }

    public SubmissionFeedback grade(String grade) {
        setGrade(grade);
        return this;
    }

    public SubmissionFeedback student(User student) {
        setStudent(student);
        return this;
    }

    public SubmissionFeedback group(Group group) {
        setGroup(group);
        return this;
    }

    public SubmissionFeedback submission(Submission submission) {
        setSubmission(submission);
        return this;
    }

    public SubmissionFeedback feedbackDate(Instant feedbackDate) {
        setFeedbackDate(feedbackDate);
        return this;
    }

    public Set<UploadedFile> getFeedbackFiles() {
        return uploadedFiles;
    }

    public void setFeedbackFiles(Set<UploadedFile> feedbackFiles) {
        this.uploadedFiles.clear();
        this.uploadedFiles.addAll(feedbackFiles);
    }

    public SubmissionFeedback feedbackFiles(Set<UploadedFile> feedbackFiles) {
        setFeedbackFiles(feedbackFiles);
        return this;
    }

    public Set<SelectedCriterionLevel> getSelectedCriterionLevels() {
        return selectedCriterionLevels;
    }

    public void setSelectedCriterionLevels(Set<SelectedCriterionLevel> selectedCriterionLevels) {
        if (selectedCriterionLevels != null) {
            selectedCriterionLevels.forEach(selectedCriterionLevel -> selectedCriterionLevel.setSubmissionFeedback(this));
        }
        this.selectedCriterionLevels = selectedCriterionLevels;
    }

    public SubmissionFeedback selectedCriterionLevels(Set<SelectedCriterionLevel> selectedCriterionLevels) {
        setSelectedCriterionLevels(selectedCriterionLevels);
        return this;
    }

    public SubmissionFeedback creator(User creator) {
        setCreator(creator);
        return this;
    }

    public boolean isTeacher(Long userId) {
        return getSubmission().getAssignment().getMilestone().getJourney().isTeacherInJourney(userId);
    }

    public boolean isStudent(Long userId) {
        return getSubmission().getAssignment().getMilestone().getJourney().isStudentInJourney(userId);
    }
}
