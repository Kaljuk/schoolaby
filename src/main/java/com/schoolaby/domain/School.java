package com.schoolaby.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@Table(name = "school")
public class School extends AbstractAuditingEntity {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private String ehisId;
    private String regNr;
    private String name;
    private String country;
    @Column(name = "external_id")
    private Long externalId;
    @OneToOne
    private UploadedFile uploadedFile;

    public School id(Long id) {
        setId(id);
        return this;
    }

    public School ehisId(String ehisId) {
        setEhisId(ehisId);
        return this;
    }

    public School regNr(String regNr) {
        setRegNr(regNr);
        return this;
    }

    public School name(String name) {
        setName(name);
        return this;
    }

    public School country(String country) {
        setCountry(country);
        return this;
    }

    public School externalId(Long externalId) {
        setExternalId(externalId);
        return this;
    }

    public School uploadedFile(UploadedFile uploadedFile) {
        setUploadedFile(uploadedFile);
        return this;
    }

    public Long getExternalId() {
        return externalId;
    }

    public void setExternalId(Long externalId) {
        this.externalId = externalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        School school = (School) o;
        return id != null && Objects.equals(id, school.id) ||
            ehisId != null && Objects.equals(ehisId, school.ehisId) ||
            regNr != null && Objects.equals(regNr, school.regNr) ||
            name != null && Objects.equals(name, school.name);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "School{" +
            "ehisId='" + ehisId + '\'' +
            ", regNr='" + regNr + '\'' +
            ", name='" + name + '\'' +
            "}";
    }
}
