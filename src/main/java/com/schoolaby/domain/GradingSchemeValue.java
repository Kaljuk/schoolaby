package com.schoolaby.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A GradingSchemeValue.
 */
@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(name = "grading_scheme_value")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SQLDelete(sql = "UPDATE grading_scheme_value SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findGradingSchemeValueById")
@NamedQuery(name = "findGradingSchemeValueById", query = "SELECT gsv FROM GradingSchemeValue gsv WHERE gsv.id = ?1 AND gsv.deleted IS NULL")
@Where(clause = "deleted IS NULL")
public class GradingSchemeValue extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "grade", nullable = false)
    private String grade;

    @NotNull
    @Range(max = 100)
    @Column(name = "percentage_range", nullable = false)
    private Integer percentageRange;

    @ManyToOne
    @JsonIgnoreProperties(value = "values", allowSetters = true)
    private GradingScheme gradingScheme;
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GradingSchemeValue)) {
            return false;
        }
        return id != null && id.equals(((GradingSchemeValue) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GradingSchemeValue{" +
            "id=" + getId() +
            ", grade='" + getGrade() + "'" +
            ", percentageRange='" + getPercentageRange() + "'" +
            "}";
    }
}
