package com.schoolaby.domain.enumeration;

import com.schoolaby.security.Role;

import java.util.List;

public enum LtiRole {
    STUDENT("Learner", Role.Constants.STUDENT),
    INSTRUCTOR("Instructor", Role.Constants.TEACHER),
    NONE("None", Role.Constants.ANONYMOUS),
    ADMIN("Administrator", Role.Constants.ADMIN);

    private final String roleName;
    private final String authorityConstant;

    LtiRole(String roleName, String authorityConstant) {
        this.roleName = roleName;
        this.authorityConstant = authorityConstant;
    }

    public String getRoleName() {
        return roleName;
    }

    public String getAuthorityConstant() {
        return authorityConstant;
    }

    public static LtiRole getRole(String authorityConstant) {
        return List
            .of(LtiRole.values())
            .stream()
            .filter(ltiRole -> ltiRole.getAuthorityConstant().equals(authorityConstant))
            .findFirst()
            .orElse(LtiRole.NONE);
    }
}
