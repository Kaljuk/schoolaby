package com.schoolaby.domain.enumeration;

import java.util.Arrays;

public enum Country {
    ESTONIA("Estonia"),
    TANZANIA("Tanzania"),
    UNITED_STATES("United States"),
    FINLAND("Finland"),
    UKRAINE("Ukraine");

    public static final Country DEFAULT_COUNTRY = ESTONIA;

    private final String value;

    Country(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Country getByValueOrDefault(String value) {
        return Arrays.stream(Country.values())
            .filter(country -> country.value.equals(value))
            .findFirst()
            .orElse(ESTONIA);
    }
}
