package com.schoolaby.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.REMOVE;

@Entity
@Table(name = "message")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SQLDelete(sql = "UPDATE message SET deleted = now() WHERE id = ?")
@Loader(namedQuery = "findMessageById")
@NamedQuery(name = "findMessageById", query = "SELECT m FROM Message m WHERE m.id = ?1 AND m.deleted IS NULL")
@Where(clause = "deleted IS NULL")
public class Message extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "value", nullable = false)
    private String value;

    @ManyToOne
    private User creator;

    @ManyToOne
    @JsonIgnoreProperties(value = "messages", allowSetters = true)
    private Chat chat;

    @OneToMany(mappedBy = "message", cascade = REMOVE)
    private Set<MessageRecipient> messageRecipients = new HashSet<>();

    private boolean feedback;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User person) {
        this.creator = person;
    }

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public Set<MessageRecipient> getMessageRecipients() {
        return messageRecipients;
    }

    public void setMessageRecipients(Set<MessageRecipient> messageRecipients) {
        this.messageRecipients = messageRecipients;
    }

    public Message id(Long id) {
        setId(id);
        return this;
    }

    public Message value(String value) {
        setValue(value);
        return this;
    }

    public Message creator(User creator) {
        setCreator(creator);
        return this;
    }

    public Message chat(Chat chat) {
        setChat(chat);
        return this;
    }

    public Message messageRecipients(Set<MessageRecipient> messageRecipients) {
        this.messageRecipients.clear();
        this.messageRecipients.addAll(messageRecipients);
        return this;
    }

    public Instant getReadTimestamp(Long userId) {
        return getMessageRecipients().stream()
            .filter(m -> m.getPerson().getId().equals(userId))
            .map(MessageRecipient::getRead)
            .findFirst()
            .orElse(null);
    }

    public boolean isFeedback() {
        return feedback;
    }

    public void setFeedback(boolean feedback) {
        this.feedback = feedback;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Message)) {
            return false;
        }
        return id != null && id.equals(((Message) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Message{" +
            "id=" + getId() +
            ", value='" + getValue() + "'" +
            "}";
    }
}
