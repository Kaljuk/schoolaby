package com.schoolaby.domain;

import com.schoolaby.domain.keys.AssignmentMaterialKey;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;

@Getter
@Setter
@Table(name="assignment_materials")
@Entity
@IdClass(AssignmentMaterialKey.class)
public class AssignmentMaterial implements Serializable {

    @Id
    @ManyToOne
    private Assignment assignment;

    @Id
    @JoinColumn(name = "materials_id")
    @ManyToOne(cascade = {PERSIST, MERGE})
    private Material material;

    private Integer sequenceNumber;
}
