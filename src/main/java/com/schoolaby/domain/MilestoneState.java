package com.schoolaby.domain;

import com.schoolaby.security.Role;
import com.schoolaby.service.dto.states.EntityState;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.slf4j.Logger;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static com.schoolaby.service.dto.states.EntityState.*;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE;
import static org.slf4j.LoggerFactory.getLogger;

@Entity
@Cache(usage = READ_WRITE)
@Getter
@Setter
public class MilestoneState implements Serializable {
    private static final Logger log = getLogger(MilestoneState.class);

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "milestoneStateGenerator")
    @SequenceGenerator(name = "milestoneStateGenerator", sequenceName = "milestone_state_seq", allocationSize = 1)
    private Long id;

    @ManyToOne
    private Milestone milestone;

    @ManyToOne
    private User user;

    @ManyToOne
    @JoinColumn(name = "authority_name", referencedColumnName = "name")
    private Authority authority;

    private String state;

    public EntityState getState() {
        return state != null ? EntityState.valueOf(state) : null;
    }

    public MilestoneState setState(EntityState state) {
        if (state != null) {
            this.state = state.toString();
        }
        return this;
    }

    public MilestoneState milestone(Milestone milestone) {
        setMilestone(milestone);
        return this;
    }

    public MilestoneState user(User user) {
        setUser(user);
        return this;
    }

    public MilestoneState authority(Authority authority) {
        setAuthority(authority);
        return this;
    }

    public MilestoneState calculate() {
        validateFieldsForCalculation();

        Supplier<Stream<Assignment>> assignmentStreamSupplier = () -> getMilestone()
            .getAssignments()
            .stream();

        Set<Assignment> filteredAssignments = assignmentStreamSupplier.get()
            .filter(assignment -> assignment.getDeleted() == null)
            .collect(toSet());
        getMilestone().setAssignments(filteredAssignments);

        EntityState stateType = null;

        if (getMilestone().getAssignments().isEmpty()) {
            stateType = Instant.now().isBefore(this.getMilestone().getEndDate()) ? NOT_STARTED : COMPLETED;
        }

        List<EntityState> statesByPriority = assignmentStreamSupplier.get()
            .map(assignment -> assignment.getState(getUser().getId(), getAuthority().getName()))
            .filter(Objects::nonNull)
            .distinct()
            .sorted(comparingInt(EntityState::getPriority))
            .collect(toList());

        if (stateType == null) {
            if(statesByPriority.isEmpty()) {
                setState(UNKNOWN);
                log.warn("Milestone state could not be calculated for user:{}, milestone:{}", getUser().getId(), getId());
                return this;
            }

            EntityState highestPriority = statesByPriority.get(0);

            if (
                isInProgress(highestPriority, statesByPriority)
            ) {
                stateType = IN_PROGRESS;
            } else {
                stateType = highestPriority;
            }
        }

        setState(stateType);
        return this;
    }

    private boolean isInProgress(EntityState highestPriority, List<EntityState> entityStatesByPriority) {
        return highestPriority == NOT_STARTED &&
            entityStatesByPriority.stream().anyMatch(entityState -> entityState.equals(COMPLETED)) &&
            getUser().getAuthorities().stream().anyMatch(authority -> authority.getName().equals(Role.Constants.STUDENT));
    }

    private void validateFieldsForCalculation() {
        if (getUser() == null) {
            throw new IllegalStateException("User field is required to calculate state");
        } else if (getMilestone() == null) {
            throw new IllegalStateException("Milestone field is required to calculate state");
        } else if (getAuthority() == null) {
            throw new IllegalStateException("Authority field is required to calculate state");
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MilestoneState) {
            MilestoneState milestoneState = (MilestoneState) obj;

            if(milestoneState.getMilestone().getId() == null) {
                return false;
            }

            return milestoneState.getMilestone().getId().equals(getMilestone().getId()) &&
                milestoneState.getUser().getId().equals(getUser().getId()) &&
                milestoneState.getAuthority().getName().equals(getAuthority().getName());
        }
        return false;
    }
}
