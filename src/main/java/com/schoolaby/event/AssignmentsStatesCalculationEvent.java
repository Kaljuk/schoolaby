package com.schoolaby.event;

import com.schoolaby.domain.Assignment;
import lombok.Getter;

import java.util.Set;

@Getter
public class AssignmentsStatesCalculationEvent {
    private final Set<Assignment> assignments;

    public AssignmentsStatesCalculationEvent(Set<Assignment> assignments) {
        this.assignments = assignments;
    }
}
