package com.schoolaby.event;

import com.schoolaby.domain.Assignment;
import com.schoolaby.service.AssignmentWriteService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.Set;

import static java.util.stream.Collectors.toSet;


@Component
@RequiredArgsConstructor
public class StateCalculationListener {
    private final AssignmentWriteService assignmentWriteService;

    @EventListener
    public void handleAssignmentStateCalculation(AssignmentStateCalculationEvent event) {
        event.getAssignment().calculateStates();
        event.getAssignment().getMilestone().calculateStates();
        event.getAssignment().getMilestone().getJourney().calculateStates();
    }

    @EventListener
    @TransactionalEventListener
    public void handleAssignmentsStatesCalculation(AssignmentsStatesCalculationEvent event) {
        Set<Long> assignmentIds = event.getAssignments().stream().map(Assignment::getId).collect(toSet());
        assignmentWriteService.calculateStatesOfAssignmentsAsync(assignmentIds);
    }

    @EventListener
    public void handleMilestoneStateCalculation(MilestoneStateCalculationEvent event) {
        event.getMilestone().calculateStates();
        event.getMilestone().getJourney().calculateStates();
    }

    @EventListener
    public void handleJourneyStateCalculation(JourneyStateCalculationEvent event) {
        event.getJourney().calculateStates();
    }
}
