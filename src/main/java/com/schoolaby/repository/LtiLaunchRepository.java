package com.schoolaby.repository;

import com.querydsl.core.BooleanBuilder;
import com.schoolaby.domain.LtiLaunch;
import com.schoolaby.repository.exception.EntityNotFoundException;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.schoolaby.domain.QLtiLaunch.ltiLaunch;
import static com.schoolaby.security.SecurityUtils.*;

@Repository
public class LtiLaunchRepository extends QuerydslRepositorySupport {
    private final LtiLaunchJpaRepository jpaRepository;

    public LtiLaunchRepository(LtiLaunchJpaRepository jpaRepository) {
        super(LtiLaunch.class);
        this.jpaRepository = jpaRepository;
    }

    private BooleanBuilder predicate() {
        BooleanBuilder predicate = new BooleanBuilder();

        if (!isCurrentUserTeacherOrAdmin() && getCurrentUserId() != null) {
            predicate.and(ltiLaunch.user.id.eq(getCurrentUserId()));
        }

        return predicate;
    }

    public List<LtiLaunch> findAll(Long assignmentId, Long userId, Boolean syncGrade) {
        BooleanBuilder predicate = predicate();
        if (assignmentId != null) {
            predicate.and(ltiLaunch.ltiResource.assignment.id.eq(assignmentId));
        }
        if (userId != null) {
            predicate.and(ltiLaunch.user.id.eq(userId));
        }
        if (syncGrade != null) {
            predicate.and(ltiLaunch.ltiResource.syncGrade.eq(syncGrade));
        }
        return jpaRepository.findAll(predicate);
    }

    public List<LtiLaunch> findByResourceAndUser(Long resourceId, Long userId) {
        BooleanBuilder predicate = predicate()
            .and(ltiLaunch.ltiResource.id.eq(resourceId))
            .and(ltiLaunch.user.id.eq(userId));

        return jpaRepository.findAll(predicate);
    }

    public LtiLaunch save(LtiLaunch ltiLaunch) {
        return jpaRepository.save(ltiLaunch);
    }

    public boolean existsById(Long id) {
        return jpaRepository.exists(predicate().and(ltiLaunch.id.eq(id)));
    }

    public void deleteById(Long id) {
        if (existsById(id)) {
            jpaRepository.deleteById(id);
        }
    }

    public Optional<LtiLaunch> findOne(Long id) {
        // results are currently posted as an unauthenticated user, fixed by SCHOOL-123
        if (!isAuthenticated()) {
            return jpaRepository.findOne(ltiLaunch.id.eq(id));
        } else {
            return jpaRepository.findOne(predicate().and(ltiLaunch.id.eq(id)));
        }
    }

    public LtiLaunch getOne(Long id) {
        return findOne(id).orElseThrow(() -> new EntityNotFoundException(LtiLaunch.class, id));
    }
}
