package com.schoolaby.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.schoolaby.domain.Notification;
import com.schoolaby.domain.NotificationType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Collection;
import java.util.List;

import static com.querydsl.core.types.Order.ASC;
import static com.querydsl.core.types.Order.DESC;
import static com.schoolaby.domain.QNotification.notification;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;

@Repository
public class NotificationRepository extends BaseQuerydslRepository {
    private final NotificationJpaRepository notificationJpaRepository;

    public NotificationRepository(NotificationJpaRepository notificationJpaRepository, JPAQueryFactory queryFactory) {
        super(Notification.class, queryFactory);
        this.notificationJpaRepository = notificationJpaRepository;
    }

    private BooleanBuilder predicate() {
        BooleanBuilder predicate = new BooleanBuilder();
        BooleanBuilder isRecipient = new BooleanBuilder().and(notification.recipient.id.eq(getCurrentUserId()));
        BooleanBuilder isStudentInJourney = new BooleanBuilder().and(notification.journey.students.any().user.id.eq(getCurrentUserId()));
        BooleanBuilder isTeacherInJourney = new BooleanBuilder().and(notification.journey.teachers.any().user.id.eq(getCurrentUserId()));

        return predicate.and(isRecipient).and(isStudentInJourney.or(isTeacherInJourney));
    }

    public Page<Notification> findAll(Pageable pageable, Collection<Long> ids) {
        BooleanBuilder predicate = predicate().and(notification.id.in(ids));
        return notificationJpaRepository.findAll(predicate, pageable);
    }

    public Page<Notification> findAll(Pageable pageable, NotificationType type) {
        BooleanBuilder predicate = predicate();
        predicate.and(notification.assignment.isNull().or(notification.assignment.published.before(Instant.now()).and(notification.assignment.milestone.published.before(Instant.now()))));
        if (type != null) {
            predicate.and(notification.type.in(type));
        }

        JPAQuery<Notification> query = queryFactory.selectFrom(notification)
            .leftJoin(notification.assignment.milestone)
            .leftJoin(notification.recipient)
            .where(predicate)
            .distinct()
            .limit(pageable.getPageSize())
            .offset((long) pageable.getPageNumber() * pageable.getPageSize());

        for (Sort.Order order : pageable.getSort()) {
            PathBuilder<Object> orderByExpression = new PathBuilder<>(Notification.class, "notification");
            query.orderBy(new OrderSpecifier(order.isAscending() ? ASC : DESC, orderByExpression.get(order.getProperty())));
        }

        List<Notification> notifications = query.fetch();

        return new PageImpl<>(notifications, pageable, notifications.size());
    }

    public Notification save(Notification notification) {
        return notificationJpaRepository.save(notification);
    }

    public List<Notification> saveAll(List<Notification> notifications) {
        return notificationJpaRepository.saveAll(notifications);
    }
}
