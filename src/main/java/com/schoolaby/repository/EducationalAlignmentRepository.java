package com.schoolaby.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.schoolaby.domain.EducationalAlignment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static com.schoolaby.domain.QEducationalAlignment.educationalAlignment;
import static com.schoolaby.domain.enumeration.Country.*;

@Repository
public class EducationalAlignmentRepository extends BaseQuerydslRepository {
    private final EducationalAlignmentJpaRepository jpaRepository;

    public EducationalAlignmentRepository(EducationalAlignmentJpaRepository jpaRepository, JPAQueryFactory queryFactory) {
        super(EducationalAlignment.class, queryFactory);
        this.jpaRepository = jpaRepository;
    }

    private BooleanBuilder predicate() {
        return new BooleanBuilder();
    }

    public Page<EducationalAlignment> findAll(Pageable pageable, String alignmentType, String title, String country) {
        BooleanBuilder predicate = predicate();
        if (alignmentType != null) {
            predicate.and(educationalAlignment.alignmentType.eq(alignmentType));
        }
        if (title != null) {
            predicate.and(educationalAlignment.title.containsIgnoreCase(title));
        }
        if (country.equals(TANZANIA.getValue()) || country.equals(UKRAINE.getValue())) {
            predicate.and(educationalAlignment.country.eq(country));
        } else {
            predicate.and(educationalAlignment.country.eq(DEFAULT_COUNTRY.getValue()));
        }
        return jpaRepository.findAll(predicate, pageable);
    }

    public Optional<EducationalAlignment> findOne(Long id) {
        return jpaRepository.findOne(predicate().and(educationalAlignment.id.eq(id)));
    }

    public EducationalAlignment save(EducationalAlignment educationalAlignment) {
        return jpaRepository.save(educationalAlignment);
    }

    public boolean existsById(Long id) {
        return jpaRepository.exists(predicate().and(educationalAlignment.id.eq(id)));
    }

    public void deleteById(Long id) {
        if (existsById(id)) {
            jpaRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException();
        }
    }

    public EducationalAlignment getOne(Long id) {
        return findOne(id).orElseThrow();
    }
}
