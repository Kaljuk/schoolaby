package com.schoolaby.repository;

import com.schoolaby.domain.GradingSchemeValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface GradingSchemeValueRepository extends JpaRepository<GradingSchemeValue, Long> {

    Set<GradingSchemeValue> findAllByGradingSchemeId(Long id);
}
