package com.schoolaby.repository;

import com.schoolaby.domain.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ChatJpaRepository extends JpaRepository<Chat, Long>, QuerydslPredicateExecutor<Chat> {
}
