package com.schoolaby.repository;

import com.schoolaby.domain.EducationalLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface EducationalLevelRepository extends JpaRepository<EducationalLevel, Long> {
    Set<EducationalLevel> findAllByCountry(String country);
}
