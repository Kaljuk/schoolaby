package com.schoolaby.repository;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static org.hibernate.graph.GraphSemantic.FETCH;

@NoRepositoryBean
class BaseQuerydslRepository extends QuerydslRepositorySupport {
    @PersistenceContext
    private EntityManager em;
    protected final JPAQueryFactory queryFactory;

    public BaseQuerydslRepository(Class<?> domainClass, JPAQueryFactory queryFactory) {
        super(domainClass);
        this.queryFactory = queryFactory;
    }

    // EntityGraph ignores second level cache, so it might be slower
    public <T, Q extends EntityPathBase<T>> Page<T> findAll(Q path, NumberPath<Long> idPath, Predicate predicate, Pageable pageable, String fetchGraphName) {
        Querydsl querydsl = new Querydsl(em, new PathBuilder<>(path.getType(), path.getMetadata()));
        JPAQuery<Long> pagingQuery = queryFactory.select(idPath).from(path).where(predicate);
        long total = pagingQuery.fetchCount();

        List<Long> ids = querydsl.applyPagination(pageable, pagingQuery).fetch();
        JPAQuery<T> query = queryFactory.selectFrom(path).where(idPath.in(ids));
        query.setHint(FETCH.getJpaHintName(), em.getEntityGraph(fetchGraphName));
        List<T> results = querydsl.applySorting(pageable.getSort(), query).fetch();
        return new PageImpl<T>(results, pageable, total);
    }
}
