package com.schoolaby.repository;

import com.schoolaby.config.external_material.EkoolikottProperties;
import com.schoolaby.config.external_material.OpiqProperties;
import com.schoolaby.logging.teams.TeamsNotifier;
import com.schoolaby.service.dto.EkoolikottResponseDTO;
import com.schoolaby.service.dto.MaterialDTO;
import com.schoolaby.service.dto.OpiqResponseDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;
import reactor.netty.http.client.HttpClient;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.springframework.http.HttpHeaders.ACCEPT;

@Repository
@RequiredArgsConstructor
@Slf4j
public class ExternalMaterialRepository {

    public static final String SEARCH_STRING = "search_string";
    public static final String LIMIT = "limit";
    public static final String SORT = "sort";
    public static final String TYPE = "type";
    public static final String SORT_DIRECTION = "sortDirection";
    public static final String QUERY = "q";
    public static final String OPIQ_MATERIAL = ".OpiqMaterial";
    public static final String EKOOLIKOTT_MATERIAL = ".EkoolikottMaterial";
    public static final String SUBJECTS = "subjects";
    public static final String TAXON = "taxon";
    public static final int MAX_EKOOLIKOTT_TAXON_REQUESTS = 3;
    private final WebClient webClient = WebClient.builder()
        .clientConnector(new ReactorClientHttpConnector(HttpClient.create().followRedirect(true))).build();

    private final EkoolikottProperties ekoolikottProperties;
    private final TeamsNotifier teamsNotifier;
    private final OpiqProperties opiqProperties;

    public List<MaterialDTO> getOpiqMaterials(String searchString, String subjects, Integer calculatedLimit) {
        try {
            OpiqResponseDTO opiqResponseDto = webClient.get()
                .uri(uriBuilder -> uriBuilder
                    .scheme("https")
                    .host(opiqProperties.getHost())
                    .port(-1)
                    .path("/api/kits/chapters/sections/search")
                    .queryParam(SUBJECTS, subjects)
                    .queryParam(SEARCH_STRING, searchString)
                    .build())
                .headers(headers -> headers.add(opiqProperties.getApiKeyName(), opiqProperties.getApiKeyValue()))
                .retrieve().bodyToMono(OpiqResponseDTO.class).block();

            return Objects.requireNonNull(opiqResponseDto).getPageResult().stream()
                .map(opiqMaterialDTO ->
                    new MaterialDTO()
                        .title(opiqMaterialDTO.getKit().getTitle())
                        .externalId(opiqMaterialDTO.getKit().getId().toString())
                        .url(opiqMaterialDTO.getUrl())
                        .imageUrl(opiqMaterialDTO.getThumbnailUrl())
                        .type(OPIQ_MATERIAL)
                        .description(opiqMaterialDTO.getChapter().getTitle()))
                .limit(calculatedLimit)
                .collect(toList());
        } catch (WebClientException exception) {
            log.error(String.format("Exception received from Opiq: %s", exception.getLocalizedMessage()));
            teamsNotifier.send(exception);
            return emptyList();
        }
    }

    private EkoolikottResponseDTO getEkoolikottMaterial(NativeWebRequest request, String searchString, Integer calculatedLimit, String taxon) {
        try {
            return webClient.get()
                .uri(uriBuilder -> uriBuilder
                    .scheme(ekoolikottProperties.getScheme())
                    .host(ekoolikottProperties.getHost())
                    .port(ekoolikottProperties.getPort())
                    .path("/rest/v2/search")
                    .queryParam(LIMIT, calculatedLimit.toString())
                    .queryParam(SORT, "type")
                    .queryParam(TYPE, "material")
                    .queryParam(SORT_DIRECTION, "asc")
                    .queryParam(QUERY, searchString)
                    .queryParam(TAXON, taxon)
                    .build())
                .headers(headers -> headers.set(ACCEPT, request.getHeader(ACCEPT)))
                .retrieve().bodyToMono(EkoolikottResponseDTO.class).block();
        } catch (WebClientException exception) {
            log.error(String.format("Exception received from E-koolikott: %s", exception.getLocalizedMessage()));
            teamsNotifier.send(exception);
            return null;
        }
    }

    private List<MaterialDTO> map(EkoolikottResponseDTO ekoolikottResponseDTO) {
        return ekoolikottResponseDTO.getItems().stream()
            .filter(ekoolikottMaterialDTO -> !ekoolikottMaterialDTO.getVisibility().equals("PRIVATE") && !ekoolikottMaterialDTO.getDeleted())
            .sorted((ekoolikottMaterialA, ekoolikottMaterialB) -> {
                if (ekoolikottMaterialA.getViews() > ekoolikottMaterialB.getViews()) {
                    return -1;
                } else if (ekoolikottMaterialA.getViews() < ekoolikottMaterialB.getViews()) {
                    return 1;
                }
                return 0;
            })
            .map(ekoolikottMaterialDto -> {
                MaterialDTO materialDto = new MaterialDTO();

                materialDto
                    .externalId(Long.toString(ekoolikottMaterialDto.getId()))
                    .type(EKOOLIKOTT_MATERIAL)
                    .url("https://e-koolikott.ee/oppematerjal/" + ekoolikottMaterialDto.getId())
                    .title(ekoolikottMaterialDto.getTitle())
                    .description(ekoolikottMaterialDto.getDescription())
                    .imageUrl(ekoolikottMaterialDto.getThumbnailName() != null ? "/api/suggestions/rest/picture/" + ekoolikottMaterialDto.getThumbnailName() : null);
                return materialDto;
            }).collect(Collectors.toList());
    }

    public List<MaterialDTO> getEkoolikottMaterials(NativeWebRequest request, String searchString, Integer calculatedLimit, String taxon) {
        Set<EkoolikottResponseDTO> responses = new HashSet<>();

        if (taxon != null) {
            Set<String> taxonIds = Arrays.stream(taxon.trim().split(",")).limit(MAX_EKOOLIKOTT_TAXON_REQUESTS).collect(toSet());
            taxonIds.forEach(taxonId -> responses.add(getEkoolikottMaterial(request, searchString, calculatedLimit / taxonIds.size(), taxonId)));
        } else {
            responses.add(getEkoolikottMaterial(request, searchString, calculatedLimit, null));
        }

        try {
            return responses.stream().filter(Objects::nonNull).flatMap(ekoolikottResponseDTO -> map(ekoolikottResponseDTO).stream()).collect(toList());
        } catch (WebClientException exception) {
            log.error(String.format("Exception received from E-koolikott: %s", exception.getLocalizedMessage()));
            teamsNotifier.send(exception);
            return emptyList();
        }
    }
}
