package com.schoolaby.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.schoolaby.domain.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static com.schoolaby.domain.QMessage.message;
import static com.schoolaby.security.Role.Constants.ADMIN;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static com.schoolaby.security.SecurityUtils.isCurrentUserInRole;

@Repository
public class MessageRepository extends BaseQuerydslRepository {
    private final MessageJpaRepository jpaRepository;

    public MessageRepository(MessageJpaRepository jpaRepository, JPAQueryFactory queryFactory) {
        super(Message.class, queryFactory);
        this.jpaRepository = jpaRepository;
    }

    private BooleanBuilder predicate() {
        BooleanBuilder predicate = new BooleanBuilder();
        BooleanExpression isInStudents = message.chat.journey.students.any().user.id.eq(getCurrentUserId());

        predicate.and(message.chat.people.any().id.eq(getCurrentUserId()));
        if (isCurrentUserInRole(TEACHER)) {
            predicate.and(
                (message.chat.journey.teachers.any().user.id.eq(getCurrentUserId()))
                    .or(isInStudents)
            );
        } else if (!isCurrentUserInRole(ADMIN)) {
            predicate.and(isInStudents);
        }
        return predicate;
    }

    public Page<Message> findAll(Pageable pageable) {
        return jpaRepository.findAll(predicate(), pageable);
    }

    public Page<Message> findLatestMessagesInChats(Pageable pageable) {
        return jpaRepository.findAll(predicate().and(message.id.in(queryFactory.select(message.id.max()).from(message).where(message.creator.id.notIn(getCurrentUserId())).groupBy(message.chat.id))), pageable);
    }

    public Optional<Message> findOne(Long id) {
        return jpaRepository.findOne(predicate().and(message.id.eq(id)));
    }

    public Page<Message> findAllByChatSubmissionId(Long submissionId, Pageable pageable, Boolean feedback) {
        BooleanBuilder predicate = predicate().and(message.chat.submission.id.eq(submissionId));
        if (feedback != null) {
            predicate.and(message.feedback.eq(feedback));
        }
        return jpaRepository.findAll(predicate, pageable);
    }

    public Page<Message> findAllByChatJourneyId(Long journeyId, Pageable pageable) {
        return jpaRepository.findAll(predicate().and(message.chat.journey.id.eq(journeyId)).and(message.chat.submission.id.isNull()), pageable);
    }

    public Optional<Message> findMessageFeedbackByChatSubmissionId(Long submissionId) {
        return Optional.ofNullable(queryFactory.selectFrom(message)
            .where(predicate().and(message.chat.submission.id.eq(submissionId))
                .and(message.feedback.eq(true)))
            .fetchOne());
    }

    public Message save(Message message) {
        return jpaRepository.save(message);
    }

    public boolean existsById(Long id) {
        return jpaRepository.exists(predicate().and(message.id.eq(id)));
    }

    public void deleteById(Long id) {
        if (existsById(id)) {
            jpaRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException();
        }
    }

    public Message getOne(Long id) {
        return findOne(id).orElseThrow();
    }
}
