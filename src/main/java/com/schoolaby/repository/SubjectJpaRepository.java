package com.schoolaby.repository;

import com.schoolaby.domain.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface SubjectJpaRepository extends JpaRepository<Subject, Long>, QuerydslPredicateExecutor<Subject> {
    Set<Subject> findAllByCountry(String country);
}
