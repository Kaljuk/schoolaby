package com.schoolaby.repository;

import com.schoolaby.domain.JourneyStudent;
import com.schoolaby.domain.keys.JourneyUserKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JourneyStudentJpaRepository extends JpaRepository<JourneyStudent, JourneyUserKey> {

    void deleteByJourneyIdAndUserId(Long journeyId, Long userId);

    JourneyStudent findByJourneyIdAndUserId(Long journeyId, Long userId);
}
