package com.schoolaby.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.schoolaby.domain.Milestone;
import com.schoolaby.security.Role;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.querydsl.QSort;
import org.springframework.data.repository.query.Param;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.schoolaby.domain.QMilestone.milestone;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static com.schoolaby.security.SecurityUtils.isCurrentUserInRole;
import static java.util.stream.Collectors.toList;

@Repository
public class MilestoneRepository extends QuerydslRepositorySupport {

    private final MilestoneJpaRepository jpaRepository;

    public MilestoneRepository(MilestoneJpaRepository jpaRepository) {
        super(Milestone.class);
        this.jpaRepository = jpaRepository;
    }

    private BooleanBuilder predicate() {
        return predicate(false);
    }

    private BooleanBuilder predicate(boolean template) {
        BooleanExpression isInStudents = milestone.journey.students.any().user.id.eq(getCurrentUserId());
        BooleanExpression isInTeachers = milestone.journey.teachers.any().user.id.eq(getCurrentUserId());

        BooleanBuilder predicate = new BooleanBuilder();
        if (isCurrentUserInRole(Role.Constants.TEACHER) && !template) {
            predicate.and(isInTeachers.or(isInStudents));
        } else if (isCurrentUserInRole(Role.Constants.TEACHER) && template) {
            predicate.and(isInTeachers.or(milestone.journey.template.isTrue()));
        } else if (!isCurrentUserInRole(Role.Constants.ADMIN)) {
            predicate.and(isInStudents);
        }
        return predicate;
    }

    public List<Milestone> findAll(Long journeyId, boolean template) {
        BooleanBuilder predicate = predicate(template);
        if (journeyId != null) {
            predicate.and(milestone.journey.id.eq(journeyId));
        }

        Sort.Order order = Sort.Order.by("endDate");
        Iterable<Milestone> resultAsIterable = jpaRepository.findAll(predicate, Sort.by(order));

        return Streamable.of(resultAsIterable).toList();
    }

    public Optional<Milestone> findOne(@Param("id") Long id, boolean template) {
        return jpaRepository.findOne(predicate(template).and(milestone.id.eq(id)));
    }

    public Milestone save(Milestone milestone) {
        return jpaRepository.save(milestone);
    }

    public boolean existsById(Long id) {
        return jpaRepository.exists(predicate().and(milestone.id.eq(id)));
    }

    public void deleteById(Long id) {
        if (existsById(id)) {
            jpaRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException();
        }
    }

    public List<Milestone> findAllOutOfSyncMilestones(Set<Long> excludedIds) {
        BooleanBuilder predicate = new BooleanBuilder()
            .and(
                milestone.states.isEmpty().or(
                    milestone.endDate.after(milestone.lastModifiedDate)
                        .and(milestone.id.notIn(excludedIds))
                )
            )
            .and(milestone.deleted.isNull())
            .and(milestone.journey.deleted.isNull());
        return findAllAndGetStream(predicate).collect(toList());
    }

    public Stream<Milestone> findAllAndGetStream(BooleanBuilder predicate) {
        return StreamSupport
            .stream(jpaRepository
                .findAll(predicate)
                .spliterator(), false);
    }
}
