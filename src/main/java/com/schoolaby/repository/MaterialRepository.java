package com.schoolaby.repository;

import com.schoolaby.domain.Material;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface MaterialRepository extends JpaRepository<Material, Long> {
    @Query(
        value = "select distinct material from Material material left join fetch material.educationalAlignments",
        countQuery = "select count(distinct material) from Material material"
    )
    Page<Material> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct material from Material material left join fetch material.educationalAlignments")
    List<Material> findAllWithEagerRelationships();

    @Query("select material from Material material left join fetch material.educationalAlignments where material.id =:id")
    Optional<Material> findOneWithEagerRelationships(@Param("id") Long id);

    Page<Material> findAllByRestrictedFalseAndEducationalAlignmentsIdIn(List<Long> educationalAlignmentIds, Pageable pageable);

    @Query("select material from Material material where (material.createdBy = :createdBy or material.restricted is false or material.restricted is null) " +
        "and material.country is null " +
        "and (lower(material.description) like %:search% or lower(material.title) like %:search%)")
    Page<Material> findAllBySearchString(@Param("search") String search, @Param("createdBy") String createdBy, Pageable pageable);

    @Query("select material from Material material where (material.createdBy = :createdBy or material.restricted is false or material.restricted is null) " +
        "and material.country = :country " +
        "and (lower(material.description) like %:search% or lower(material.title) like %:search%)")
    Page<Material> findAllBySearchAndCountry(@Param("search") String search, @Param("createdBy") String createdBy, @Param("country") String country, Pageable pageable);

    Set<Material> findAllByIdInAndRestrictedFalseAndCreatedByNot(List<Long> id, String createdBy);
}
