package com.schoolaby.repository;

import com.schoolaby.domain.JourneySignupCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface JourneySignupCodeJpaRepository extends JpaRepository<JourneySignupCode, Long>, QuerydslPredicateExecutor<JourneySignupCode> {
}
