package com.schoolaby.repository;

import com.schoolaby.domain.Submission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface SubmissionJpaRepository extends JpaRepository<Submission, Long>, QuerydslPredicateExecutor<Submission> {
}
