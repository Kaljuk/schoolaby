package com.schoolaby.repository;

import com.schoolaby.domain.LtiLaunch;
import com.schoolaby.repository.common.CollectionsQdslPredicateExecutor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LtiLaunchJpaRepository extends JpaRepository<LtiLaunch, Long>, CollectionsQdslPredicateExecutor<LtiLaunch> {
}
