package com.schoolaby.repository;

import com.schoolaby.domain.PersistentAuditEvent;
import com.schoolaby.repository.common.CollectionsQdslPredicateExecutor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;
import java.util.List;

public interface PersistenceAuditEventRepository extends JpaRepository<PersistentAuditEvent, Long>, CollectionsQdslPredicateExecutor<PersistentAuditEvent> {
    List<PersistentAuditEvent> findByPrincipal(String principal);

    List<PersistentAuditEvent> findByPrincipalAndAuditEventDateAfterAndAuditEventType(String principal, Instant after, String type);
}
