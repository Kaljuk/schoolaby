package com.schoolaby.repository;

import com.schoolaby.domain.Curriculum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CurriculumJpaRepository extends JpaRepository<Curriculum, Long> {
    Set<Curriculum> findAllByCountryOrCountryIsNull(String country);
}
