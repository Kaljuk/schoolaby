package com.schoolaby.repository;

import com.schoolaby.domain.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageJpaRepository extends JpaRepository<Message, Long>, QuerydslPredicateExecutor<Message> {

}
