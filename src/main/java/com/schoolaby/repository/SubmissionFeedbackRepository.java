package com.schoolaby.repository;

import com.querydsl.core.BooleanBuilder;
import com.schoolaby.domain.Submission;
import com.schoolaby.domain.SubmissionFeedback;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.schoolaby.domain.QSubmissionFeedback.submissionFeedback;

@Repository
public class SubmissionFeedbackRepository extends QuerydslRepositorySupport {
    private final SubmissionFeedbackJpaRepository jpaRepository;

    public SubmissionFeedbackRepository(SubmissionFeedbackJpaRepository jpaRepository) {
        super(Submission.class);
        this.jpaRepository = jpaRepository;
    }

    private BooleanBuilder predicate() {
        return new BooleanBuilder();
    }

    public Optional<SubmissionFeedback> findById(Long id) {
        return jpaRepository.findById(id);
    }

    public List<SubmissionFeedback> findAllIndividualBySubmissionId(Long submissionId) {
        return (List<SubmissionFeedback>) jpaRepository.findAll(predicate()
            .and(submissionFeedback.submission.id.eq(submissionId))
            .and(submissionFeedback.student.isNotNull())
            .and(submissionFeedback.group.isNull())
        );
    }

    public Optional<SubmissionFeedback> findOneByStudentIdAndSubmissionId(Long studentId, Long submissionId) {
        return jpaRepository.findOne(predicate()
            .and(submissionFeedback.student.id.eq(studentId))
            .and(submissionFeedback.submission.id.eq(submissionId))
        );
    }

    public List<SubmissionFeedback> findAllByStudentIdAndSubmissionId(Long studentId, Long submissionId) {
        return (List<SubmissionFeedback>) jpaRepository.findAll(predicate()
            .and(submissionFeedback.student.id.eq(studentId))
            .and(submissionFeedback.submission.id.eq(submissionId)), Sort.by(Sort.Direction.DESC, "feedbackDate")
        );
    }

    public Optional<SubmissionFeedback> findOneByGroupIdAndSubmissionId(Long groupId, Long submissionId) {
        return jpaRepository.findOne(predicate()
            .and(submissionFeedback.group.id.eq(groupId))
            .and(submissionFeedback.submission.id.eq(submissionId))
        );
    }

    public SubmissionFeedback save(SubmissionFeedback submissionFeedback) {
        return jpaRepository.save(submissionFeedback);
    }
}
