package com.schoolaby.repository;

import com.schoolaby.domain.SubmissionFeedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface SubmissionFeedbackJpaRepository extends JpaRepository<SubmissionFeedback, Long>, QuerydslPredicateExecutor<SubmissionFeedback> {
}
