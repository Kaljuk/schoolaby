package com.schoolaby.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.schoolaby.domain.Assignment;
import com.schoolaby.security.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.schoolaby.domain.QAssignment.assignment;
import static com.schoolaby.domain.QMilestone.milestone;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static com.schoolaby.security.SecurityUtils.isCurrentUserInRole;
import static java.util.stream.Collectors.toList;

@Repository
public class AssignmentRepository extends QuerydslRepositorySupport {
    private final AssignmentJpaRepository jpaRepository;

    public AssignmentRepository(AssignmentJpaRepository jpaRepository) {
        super(Assignment.class);
        this.jpaRepository = jpaRepository;
    }

    private BooleanBuilder predicate() {
        return predicate(false);
    }

    private BooleanBuilder predicate(boolean template) {
        BooleanBuilder predicate = new BooleanBuilder();
        if (!isCurrentUserInRole(Role.Constants.ADMIN)) {
            BooleanExpression isInStudents = assignment.milestone.journey.students.any().user.id.eq(getCurrentUserId());
            BooleanExpression isInTeachers = assignment.milestone.journey.teachers.any().user.id.eq(getCurrentUserId());

            if (isCurrentUserInRole(Role.Constants.TEACHER) && !template) {
                predicate.and(isInTeachers.or(isInStudents));
            } else if (isCurrentUserInRole(Role.Constants.TEACHER) && template) {
                predicate.and(assignment.milestone.journey.template.isTrue().or(isInTeachers));
            } else {
                predicate.and(isInStudents)
                    .and(assignment.milestone.published.before(Instant.now()));
            }
        }
        return predicate;
    }

    private BooleanExpression deadlineBetweenExpression(Instant from, Instant to) {
        return assignment.deadline.between(from, to);
    }

    public Page<Assignment> findAll(Pageable pageable) {
        return jpaRepository.findAll(predicate(), pageable);
    }

    public Optional<Assignment> findOne(Long id, boolean template) {
        return jpaRepository.findOne(predicate(template).and(assignment.id.eq(id)));
    }

    public List<Assignment> findAssignmentsByIds(Set<Long> assignmentIds) {
        return jpaRepository.findAllById(assignmentIds);
    }

    public Page<Assignment> findAllByDeadlineBetweenPaginated(Instant from, Instant to, Pageable pageable) {
        return jpaRepository.findAll(predicate().and(deadlineBetweenExpression(from, to)), pageable);
    }

    /*
     * Using unpaged until assignment states are persisted in the db
     * TODO: SCHOOL-845 filter also by persisted assignment state
     * */
    public Page<Assignment> findAllInJourneyByDeadlineBetweenUnpaged(Long journeyId, Instant from, Instant to) {
        return jpaRepository.findAll(predicate().and(deadlineBetweenExpression(from, to).and(assignment.milestone.journey.id.eq(journeyId))), Pageable.unpaged());
    }

    public List<Assignment> findAllByDeadlineBetween(Instant from, Instant to) {
        return (List<Assignment>) jpaRepository.findAll(predicate().and(deadlineBetweenExpression(from, to)));
    }

    public List<Assignment> findBySearch(Long journeyId, String search) {
        return (List<Assignment>) jpaRepository.findAll(predicate()
            .and(assignment.milestone.journey.id.eq(journeyId))
            .and(assignment.groups.isNotEmpty())
            .and(assignment.title.containsIgnoreCase(search)));
    }

    public Assignment save(Assignment assignment) {
        return jpaRepository.save(assignment);
    }

    public List<Assignment> saveAll(Set<Assignment> assignments) {
        return jpaRepository.saveAll(assignments);
    }

    public boolean existsById(Long id) {
        return jpaRepository.exists(predicate().and(assignment.id.eq(id)));
    }

    public void deleteById(Long id) {
        if (existsById(id)) {
            jpaRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException();
        }
    }

    public Stream<Assignment> findAllAndGetStream(BooleanBuilder predicate) {
        return StreamSupport
            .stream(jpaRepository
                .findAll(predicate)
                .spliterator(), false);
    }

    public List<Assignment> findAllOutOfSyncAssignments() {
        BooleanBuilder predicate = new BooleanBuilder().and(
            assignment.states.isEmpty().or(
                assignment.deadline.after(assignment.lastModifiedDate)
            )
        ).and(assignment.deleted.isNull())
            .and(assignment.milestone.deleted.isNull())
            .and(assignment.milestone.journey.deleted.isNull());
        return findAllAndGetStream(predicate).collect(toList());
    }
}
