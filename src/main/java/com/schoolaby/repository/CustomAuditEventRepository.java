package com.schoolaby.repository;

import com.schoolaby.config.Constants;
import com.schoolaby.config.audit.AuditEventConverter;
import com.schoolaby.domain.PersistentAuditEvent;
import com.schoolaby.domain.User;
import com.schoolaby.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.boot.actuate.audit.AuditEventRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class CustomAuditEventRepository implements AuditEventRepository {
    private static final String AUTHORIZATION_FAILURE = "AUTHORIZATION_FAILURE";
    protected static final int EVENT_DATA_COLUMN_MAX_LENGTH = 255;
    private final Logger log = LoggerFactory.getLogger(getClass());

    private final PersistenceAuditEventRepository persistenceAuditEventRepository;
    private final AuditEventConverter auditEventConverter;
    private final UserService userService;

    public CustomAuditEventRepository(
        PersistenceAuditEventRepository persistenceAuditEventRepository,
        AuditEventConverter auditEventConverter,
        UserService userService) {
        this.persistenceAuditEventRepository = persistenceAuditEventRepository;
        this.auditEventConverter = auditEventConverter;
        this.userService = userService;
    }

    @Override
    public List<AuditEvent> find(String principal, Instant after, String type) {
        Iterable<PersistentAuditEvent> persistentAuditEvents = persistenceAuditEventRepository.findByPrincipalAndAuditEventDateAfterAndAuditEventType(
            principal,
            after,
            type
        );
        return auditEventConverter.convertToAuditEvent(persistentAuditEvents);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void add(AuditEvent event) {
        if (!AUTHORIZATION_FAILURE.equals(event.getType()) && !Constants.ANONYMOUS_USER.equals(event.getPrincipal())) {
            PersistentAuditEvent persistentAuditEvent = new PersistentAuditEvent();
            User user = getUserByEventPrincipal(event.getPrincipal());
            String principal = user != null ? user.getLogin() : event.getPrincipal();
            Long userId = user != null ? user.getId() : null;
            persistentAuditEvent.setPrincipal(principal);
            persistentAuditEvent.setUserId(userId);
            persistentAuditEvent.setAuditEventType(event.getType());
            persistentAuditEvent.setAuditEventDate(event.getTimestamp());
            Map<String, String> eventData = auditEventConverter.convertDataToStrings(event.getData());
            persistentAuditEvent.setData(truncate(eventData));
            persistenceAuditEventRepository.save(persistentAuditEvent);
        }
    }

    private User getUserByEventPrincipal(String principal) {
        Optional<User> userByLogin = userService.findOneByLogin(principal);
        User userBySub = userService.findUserByExternalId(principal);

        return userByLogin.orElse(userBySub);
    }

    /**
     * Truncate event data that might exceed column length.
     */
    private Map<String, String> truncate(Map<String, String> data) {
        Map<String, String> results = new HashMap<>();

        if (data != null) {
            for (Map.Entry<String, String> entry : data.entrySet()) {
                String value = entry.getValue();
                if (value != null) {
                    int length = value.length();
                    if (length > EVENT_DATA_COLUMN_MAX_LENGTH) {
                        value = value.substring(0, EVENT_DATA_COLUMN_MAX_LENGTH);
                        log.warn(
                            "Event data for {} too long ({}) has been truncated to {}. Consider increasing column width.",
                            entry.getKey(),
                            length,
                            EVENT_DATA_COLUMN_MAX_LENGTH
                        );
                    }
                }
                results.put(entry.getKey(), value);
            }
        }
        return results;
    }
}
