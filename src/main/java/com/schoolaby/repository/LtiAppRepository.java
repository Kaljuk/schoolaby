package com.schoolaby.repository;

import com.schoolaby.domain.LtiApp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LtiAppRepository extends JpaRepository<LtiApp, Long> {
    List<LtiApp> findAllByConfigsJourneyId(Long id);

    Optional<LtiApp> findByDeepLinkingUrl(String deepLinkingUrl);

    Optional<LtiApp> findByClientId(String clientId);
}
