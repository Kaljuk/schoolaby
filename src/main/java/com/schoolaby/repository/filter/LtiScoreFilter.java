package com.schoolaby.repository.filter;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class LtiScoreFilter {
    private final Long id;
    private final Long lineItemId;
    private final Long userId;
    private final Long assignmentId;
    private final String gradingProgress;
    private final Boolean syncGrade;
}
