package com.schoolaby.repository;

import com.schoolaby.domain.GradingScheme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface GradingSchemeRepository extends JpaRepository<GradingScheme, Long> {
}
