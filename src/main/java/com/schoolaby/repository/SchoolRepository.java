package com.schoolaby.repository;

import com.schoolaby.domain.School;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface SchoolRepository extends JpaRepository<School, Long> {

    @Query("select s from School s where (s.regNr = :regNr) or (s.ehisId = :ehisId) or (s.name = :name)")
    Optional<School> findByRegNrOrEhisIdOrName(@Param("regNr") String regNr, @Param("ehisId") String ehisId, @Param("name") String name);

    Set<School> findAllByNameContainingIgnoreCaseAndCountry(String name, String country);

    Set<School> findAllByIdIn(Set<Long> ids);
}
