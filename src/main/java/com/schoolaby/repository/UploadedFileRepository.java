package com.schoolaby.repository;

import com.schoolaby.domain.UploadedFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UploadedFileRepository extends JpaRepository<UploadedFile, Long> {
    void deleteByUniqueName(String uniqueName);

    Optional<UploadedFile> findByUniqueName(String uniqueName);
}
