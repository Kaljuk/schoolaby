package com.schoolaby.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.schoolaby.domain.LtiScore;
import com.schoolaby.repository.exception.EntityNotFoundException;
import com.schoolaby.repository.filter.LtiScoreFilter;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.schoolaby.domain.QLtiScore.ltiScore;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static com.schoolaby.security.SecurityUtils.isCurrentUserTeacherOrAdmin;

@Repository
public class LtiScoreRepository extends BaseQuerydslRepository {
    private final LtiScoreJpaRepository jpaRepository;

    public LtiScoreRepository(LtiScoreJpaRepository jpaRepository, JPAQueryFactory queryFactory) {
        super(LtiScore.class, queryFactory);
        this.jpaRepository = jpaRepository;
    }

    private BooleanBuilder predicate() {
        BooleanBuilder predicate = new BooleanBuilder();
        if (!isCurrentUserTeacherOrAdmin()) {
            predicate.and(ltiScore.user.id.eq(getCurrentUserId()));
        }
        return predicate;
    }

    public LtiScore save(LtiScore score) {
        return jpaRepository.save(score);
    }

    public List<LtiScore> findAll(LtiScoreFilter filter) {
        BooleanBuilder predicate = predicate();
        if (filter.getId() != null) {
            predicate.and(ltiScore.id.eq(filter.getId()));
        }
        if (filter.getLineItemId() != null) {
            predicate.and(ltiScore.lineItem.id.eq(filter.getLineItemId()));
        }
        if (filter.getUserId() != null) {
            predicate.and(ltiScore.user.id.eq(filter.getUserId()));
        }
        if (filter.getAssignmentId() != null) {
            predicate.and(ltiScore.lineItem.ltiResource.assignment.id.eq(filter.getAssignmentId()));
        }
        if (filter.getSyncGrade() != null) {
            predicate.and(ltiScore.lineItem.ltiResource.syncGrade.eq(filter.getSyncGrade()));
        }
        if (filter.getGradingProgress() != null) {
            predicate.and(ltiScore.gradingProgress.eq(filter.getGradingProgress()));
        }
        return jpaRepository.findAll(predicate);
    }

    public Optional<LtiScore> findOne(LtiScoreFilter filter) {
        BooleanBuilder predicate = new BooleanBuilder();
        if (filter.getId() != null) {
            predicate.and(ltiScore.id.eq(filter.getId()));
        }
        if (filter.getLineItemId() != null) {
            predicate.and(ltiScore.lineItem.id.eq(filter.getLineItemId()));
        }
        if (filter.getUserId() != null) {
            predicate.and(ltiScore.user.id.eq(filter.getUserId()));
        }
        return jpaRepository.findOne(predicate);
    }

    public LtiScore getOne(Long id) {
        return findOne(LtiScoreFilter.builder().id(id).build()).orElseThrow(() -> new EntityNotFoundException(LtiScore.class, id));
    }
}
