package com.schoolaby.security;

import com.schoolaby.domain.enumeration.LtiRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;
import java.util.stream.Stream;

import static com.schoolaby.security.Role.Constants.*;
import static java.util.stream.Collectors.toList;

@Slf4j
public final class SecurityUtils {
    public static final String IS_TEACHER_OR_ADMIN = "hasAnyAuthority(\"" + TEACHER + "\", \"" + ADMIN + "\")";
    public static final String IS_STUDENT_OR_TEACHER_OR_ADMIN = "hasAnyAuthority(\"" + STUDENT + "\", \"" + TEACHER + "\", \"" + ADMIN + "\")";
    public static final String IS_ADMIN = "hasAuthority(\"" + ADMIN + "\")";
    public static final String IS_STUDENT = "hasAuthority(\"" + STUDENT + "\")";
    public static final String IS_AUTHENTICATED = "isAuthenticated()";

    private SecurityUtils() {
    }

    /**
     * Get the login of the current user.
     *
     * @return the login of the current user.
     */
    public static Optional<String> getCurrentUserLogin() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return Optional.ofNullable(extractPrincipal(securityContext.getAuthentication()));
    }

    private static String extractPrincipal(Authentication authentication) {
        if (authentication == null) {
            return null;
        } else if (authentication.getPrincipal() instanceof UserDetails) {
            UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();
            return springSecurityUser.getUsername();
        } else if (authentication.getPrincipal() instanceof String) {
            return (String) authentication.getPrincipal();
        }
        return null;
    }

    /**
     * Get the JWT of the current user.
     *
     * @return the JWT of the current user.
     */
    public static Optional<String> getCurrentUserJWT() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return Optional
            .ofNullable(securityContext.getAuthentication())
            .filter(authentication -> authentication.getCredentials() instanceof String)
            .map(authentication -> (String) authentication.getCredentials());
    }

    /**
     * Check if a user is authenticated.
     *
     * @return true if the user is authenticated, false otherwise.
     */
    public static boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null && getAuthorities(authentication).noneMatch(Role.Constants.ANONYMOUS::equals);
    }

    /**
     * If the current user has a specific authority (security role).
     * <p>
     * The name of this method comes from the {@code isUserInRole()} method in the Servlet API.
     *
     * @param authority the authority to check.
     * @return true if the current user has the authority, false otherwise.
     */
    public static boolean isCurrentUserInRole(String authority) {
        return getAuthorities().anyMatch(authority::equals);
    }

    public static boolean isCurrentUserTeacherOrAdmin() {
        return isCurrentUserInRole(TEACHER) || isCurrentUserInRole(ADMIN);
    }

    public static boolean currentUserHasAnyAuthority(String... expectedAuthorities) {
        return getAuthorities().anyMatch(authority -> Arrays.asList(expectedAuthorities).contains(authority));
    }

    public static LtiRole getCurrentUserLtiRole() {
        return getAuthorities().map(LtiRole::getRole).findFirst().orElseThrow();
    }

    public static Long getCurrentUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        try {
            return ((User) authentication.getPrincipal()).getId();
        } catch (Exception e) {
            log.warn("Unable to find user");
            return null;
        }
    }

    public static User createSpringSecurityUser(com.schoolaby.domain.User user) {
        if (!user.isActivated()) {
            throw new UserNotActivatedException("User " + user.getId() + " was not activated");
        }
        List<GrantedAuthority> grantedAuthorities = user
            .getAuthorities()
            .stream()
            .map(authority -> new SimpleGrantedAuthority(authority.getName()))
            .collect(toList());

        return new User(user.getLogin(), user.getPassword(), grantedAuthorities, user.getId());
    }

    public static void setInternalUserSecurityContext() {
        SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(ADMIN));
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken("INTERNAL_USER", "INTERNAL_USER", authorities);
        securityContext.setAuthentication(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    private static Stream<String> getAuthorities(Authentication authentication) {
        return authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority);
    }

    public static Stream<String> getAuthorities() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication == null ? Stream.empty() : authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority);
    }
}
