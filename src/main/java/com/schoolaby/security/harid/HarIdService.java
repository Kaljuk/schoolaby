package com.schoolaby.security.harid;

import com.schoolaby.domain.User;
import com.schoolaby.service.SchoolService;
import com.schoolaby.service.UserService;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Service
@Transactional
public class HarIdService {
    private final HarIdRepository harIdRepository;
    private final UserService userService;
    private final SchoolService schoolService;

    public HarIdService(HarIdRepository harIdRepository, UserService userService, SchoolService schoolService) {
        this.harIdRepository = harIdRepository;
        this.userService = userService;
        this.schoolService = schoolService;
    }

    public User saveOrUpdateHarIdData(String authorizationCode) {
        return saveOrUpdate(getHarIdUser(authorizationCode));
    }

    public User saveOrUpdate(HarIdUser harIdUser) {
        schoolService.saveHaridSchools(harIdUser.getRoles());
        return userService.save(harIdUser);
    }

    public HarIdUser getHarIdUser(String authorizationCode) {
        HarIdCode harIdCode = harIdRepository.getHarIdCode(authorizationCode);
        HarIdUser harIdUser = harIdRepository.getHarIdUser(harIdCode);
        if (harIdUser == null || isBlank(harIdUser.getIdCode())) {
            throw new BadCredentialsException("HarIdUser doesnt have idCode or retrieving user details failed");
        }
        return harIdUser;
    }
}
