package com.schoolaby.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "ekool")
public class EkoolProperties {
    private String userSchoolsUri;
    private String userRolesUri;
    private String userBasicUri;
}
