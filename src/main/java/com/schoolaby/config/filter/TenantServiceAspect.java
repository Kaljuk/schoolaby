package com.schoolaby.config.filter;

import com.schoolaby.security.SecurityUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static com.schoolaby.security.Role.Constants.ADMIN;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.security.SecurityUtils.currentUserHasAnyAuthority;
import static com.schoolaby.security.SecurityUtils.isAuthenticated;

@Aspect
@Component
public class TenantServiceAspect {
    public static final String FILTER_HIDDEN = "filterHidden";
    public static final String STUDENT_FILTER = "studentFilter";

    @PersistenceContext
    public EntityManager entityManager;

    @Pointcut("execution(public * org.springframework.data.repository.Repository+.*(..))")
    void isRepository() {
    }

    @Around("execution(public * *(..)) && isRepository()")
    public Object aroundExecution(final ProceedingJoinPoint pjp) throws Throwable {
        if (isAuthenticated() && !currentUserHasAnyAuthority(ADMIN, TEACHER)) {
            Session unwrap = this.entityManager
                .unwrap(Session.class);
            final Filter hiddenFilter = unwrap
                .enableFilter(FILTER_HIDDEN);
            hiddenFilter.validate();
            if (SecurityUtils.getCurrentUserId() != null) {
                final Filter studentFilter = unwrap
                    .enableFilter(STUDENT_FILTER);
                studentFilter.setParameter("currentUserId", SecurityUtils.getCurrentUserId());
                studentFilter.validate();
            }
        }
        return pjp.proceed();
    }
}
