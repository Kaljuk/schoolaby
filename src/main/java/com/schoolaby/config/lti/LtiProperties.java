package com.schoolaby.config.lti;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "lti")
public class LtiProperties {
    private String callbackUrl;
    private String contentItemReturnUrl;
    private String toolConsumerInstanceGuid;
    private String toolConsumerInfoProductFamilyCode;
    private String toolConsumerInfoVersion;
    private String toolConsumerInstanceName;
    private String toolConsumerInstanceContactEmail;
    private String launchPresentationDocumentTarget;
}
