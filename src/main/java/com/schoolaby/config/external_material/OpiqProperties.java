package com.schoolaby.config.external_material;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "opiq")
public class OpiqProperties {
    private String apiKeyName;
    private String apiKeyValue;
    private String host;
}
