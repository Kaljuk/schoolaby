package com.schoolaby.config;

import com.github.benmanes.caffeine.jcache.configuration.CaffeineConfiguration;
import io.github.jhipster.config.JHipsterProperties;
import io.github.jhipster.config.cache.PrefixedKeyGenerator;
import org.hibernate.cache.jcache.ConfigSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.OptionalLong;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
public class CacheConfiguration {
    private GitProperties gitProperties;
    private BuildProperties buildProperties;
    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Caffeine caffeine = jHipsterProperties.getCache().getCaffeine();

        CaffeineConfiguration<Object, Object> caffeineConfiguration = new CaffeineConfiguration<>();
        caffeineConfiguration.setMaximumSize(OptionalLong.of(caffeine.getMaxEntries()));
        caffeineConfiguration.setExpireAfterWrite(OptionalLong.of(TimeUnit.SECONDS.toNanos(caffeine.getTimeToLiveSeconds())));
        caffeineConfiguration.setStatisticsEnabled(true);
        jcacheConfiguration = caffeineConfiguration;
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, com.schoolaby.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, com.schoolaby.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, com.schoolaby.domain.User.class.getName());
            createCache(cm, com.schoolaby.domain.Authority.class.getName());
            createCache(cm, com.schoolaby.domain.User.class.getName() + ".authorities");
            createCache(cm, com.schoolaby.domain.User.class.getName() + ".chats");
            createCache(cm, com.schoolaby.domain.User.class.getName() + ".schools");
            createCache(cm, com.schoolaby.domain.Journey.class.getName());
            createCache(cm, com.schoolaby.domain.Journey.class.getName() + ".milestones");
            createCache(cm, com.schoolaby.domain.Journey.class.getName() + ".messages");
            createCache(cm, com.schoolaby.domain.Journey.class.getName() + ".teachers");
            createCache(cm, com.schoolaby.domain.Journey.class.getName() + ".students");
            createCache(cm, com.schoolaby.domain.Journey.class.getName() + ".schools");
            createCache(cm, com.schoolaby.domain.JourneyState.class.getName());
            createCache(cm, com.schoolaby.domain.JourneyStudent.class.getName());
            createCache(cm, com.schoolaby.domain.Curriculum.class.getName());
            createCache(cm, com.schoolaby.domain.Milestone.class.getName());
            createCache(cm, com.schoolaby.domain.Milestone.class.getName() + ".assignments");
            createCache(cm, com.schoolaby.domain.Milestone.class.getName() + ".messages");
            createCache(cm, com.schoolaby.domain.Milestone.class.getName() + ".materials");
            createCache(cm, com.schoolaby.domain.Milestone.class.getName() + ".educationalAlignments");
            createCache(cm, com.schoolaby.domain.GradingScheme.class.getName());
            createCache(cm, com.schoolaby.domain.GradingScheme.class.getName() + ".values");
            createCache(cm, com.schoolaby.domain.GradingScheme.class.getName() + ".assignments");
            createCache(cm, com.schoolaby.domain.GradingSchemeValue.class.getName());
            createCache(cm, com.schoolaby.domain.Material.class.getName());
            createCache(cm, com.schoolaby.domain.Material.class.getName() + ".educationalAlignments");
            createCache(cm, com.schoolaby.domain.Material.class.getName() + ".milestones");
            createCache(cm, com.schoolaby.domain.Assignment.class.getName());
            createCache(cm, com.schoolaby.domain.Assignment.class.getName() + ".submissions");
            createCache(cm, com.schoolaby.domain.Assignment.class.getName() + ".students");
            createCache(cm, com.schoolaby.domain.Assignment.class.getName() + ".educationalAlignments");
            createCache(cm, com.schoolaby.domain.Assignment.class.getName() + ".groups");
            createCache(cm, com.schoolaby.domain.Submission.class.getName());
            createCache(cm, com.schoolaby.domain.Submission.class.getName() + ".messages");
            createCache(cm, com.schoolaby.domain.Submission.class.getName() + ".uploadedFiles");
            createCache(cm, com.schoolaby.domain.Submission.class.getName() + ".authors");
            createCache(cm, com.schoolaby.domain.Submission.class.getName() + ".feedbackFiles");
            createCache(cm, com.schoolaby.domain.SubmissionFeedback.class.getName());
            createCache(cm, com.schoolaby.domain.UploadedFile.class.getName());
            createCache(cm, com.schoolaby.domain.EducationalAlignment.class.getName());
            createCache(cm, com.schoolaby.domain.EducationalAlignment.class.getName() + ".children");
            createCache(cm, com.schoolaby.domain.EducationalAlignment.class.getName() + ".milestones");
            createCache(cm, com.schoolaby.domain.EducationalAlignment.class.getName() + ".materials");
            createCache(cm, com.schoolaby.domain.EducationalAlignment.class.getName() + ".parents");
            createCache(cm, com.schoolaby.domain.User.class.getName());
            createCache(cm, com.schoolaby.domain.User.class.getName() + ".milestones");
            createCache(cm, com.schoolaby.domain.User.class.getName() + ".journeys");
            createCache(cm, com.schoolaby.domain.User.class.getName() + ".assignments");
            createCache(cm, com.schoolaby.domain.User.class.getName() + ".personRoles");
            createCache(cm, com.schoolaby.domain.User.class.getName() + ".submissions");
            createCache(cm, com.schoolaby.domain.User.class.getName() + ".teacherJourneys");
            createCache(cm, com.schoolaby.domain.User.class.getName() + ".studentJourneys");
            createCache(cm, com.schoolaby.domain.PersonRole.class.getName());
            createCache(cm, com.schoolaby.domain.Message.class.getName());
            createCache(cm, com.schoolaby.domain.Material.class.getName() + ".assignments");
            createCache(cm, com.schoolaby.domain.EducationalAlignment.class.getName() + ".assignments");
            createCache(cm, com.schoolaby.domain.User.class.getName() + ".createdAssignments");
            createCache(cm, com.schoolaby.domain.User.class.getName() + ".createdJourneys");
            createCache(cm, com.schoolaby.domain.User.class.getName() + ".externalAuthentications");
            createCache(cm, com.schoolaby.domain.LtiConfig.class.getName());
            createCache(cm, com.schoolaby.domain.LtiConfig.class.getName() + ".links");
            createCache(cm, com.schoolaby.domain.LtiResource.class.getName());
            createCache(cm, com.schoolaby.domain.LtiResource.class.getName() + ".lineItems");
            createCache(cm, com.schoolaby.domain.LtiLineItem.class.getName());
            createCache(cm, com.schoolaby.domain.LtiScore.class.getName());
            createCache(cm, com.schoolaby.domain.LtiApp.class.getName());
            createCache(cm, com.schoolaby.domain.MessageRecipient.class.getName());
            createCache(cm, com.schoolaby.domain.Notification.class.getName());
            createCache(cm, com.schoolaby.domain.Chat.class.getName());
            createCache(cm, com.schoolaby.domain.Chat.class.getName() + ".messages");
            createCache(cm, com.schoolaby.domain.Chat.class.getName() + ".people");
            createCache(cm, com.schoolaby.domain.Rubric.class.getName());
            createCache(cm, com.schoolaby.domain.Rubric.class.getName() + ".criterions");
            createCache(cm, com.schoolaby.domain.Criterion.class.getName());
            createCache(cm, com.schoolaby.domain.Criterion.class.getName() + ".levels");
            createCache(cm, com.schoolaby.domain.CriterionLevel.class.getName());
            createCache(cm, com.schoolaby.domain.SelectedCriterionLevel.class.getName());
            createCache(cm, com.schoolaby.domain.Group.class.getName());
            createCache(cm, com.schoolaby.domain.Group.class.getName() + ".students");
            createCache(cm, com.schoolaby.domain.Subject.class.getName());
            createCache(cm, com.schoolaby.domain.School.class.getName());
            createCache(cm, com.schoolaby.domain.School.class.getName() + ".users");
            createCache(cm, com.schoolaby.domain.MilestoneState.class.getName());
            createCache(cm, com.schoolaby.domain.AssignmentState.class.getName());
            createCache(cm, "default-query-results-region");
            createCache(cm, "default-update-timestamps-region");
            // jhipster-needle-caffeine-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache == null) {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

    @Autowired(required = false)
    public void setGitProperties(GitProperties gitProperties) {
        this.gitProperties = gitProperties;
    }

    @Autowired(required = false)
    public void setBuildProperties(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @Bean
    public KeyGenerator keyGenerator() {
        return new PrefixedKeyGenerator(this.gitProperties, this.buildProperties);
    }
}
