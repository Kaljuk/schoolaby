package com.schoolaby.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

import static java.lang.Long.parseLong;

@Configuration
public class RestRepositoryConfiguration {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public RestTemplate harIdRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public RestTemplate ekoolRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public RestTemplate externalHeaderCheckTemplate(@Value("${proxy.embeddable-headers-check.timeout}") String timeout) {
        Duration timeoutDuration = Duration.ofMillis(parseLong(timeout));
        return new RestTemplateBuilder()
            .setReadTimeout(timeoutDuration)
            .setConnectTimeout(timeoutDuration)
            .build();
    }
}
