package com.schoolaby.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile({ "test", "prod" })
public class StorageConfiguration {
    private final ApplicationProperties properties;

    public StorageConfiguration(ApplicationProperties properties) {
        this.properties = properties;
    }

    @Bean
    public AmazonS3 amazonS3() {
        ApplicationProperties.Storage storage = properties.getStorage();
        BasicAWSCredentials credentials = new BasicAWSCredentials(storage.getAccessKey(), storage.getAccessSecret());

        return AmazonS3ClientBuilder
            .standard()
            .withEndpointConfiguration(new EndpointConfiguration(storage.getEndpoint(), storage.getRegion()))
            .withCredentials(new AWSStaticCredentialsProvider(credentials))
            .build();
    }
}
