package com.schoolaby.config.oauth2;

import com.schoolaby.domain.User;
import com.schoolaby.security.jwt.TokenProvider;
import com.schoolaby.service.UserService;
import io.undertow.util.BadRequestException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.schoolaby.security.OauthSecurityProblemSupport.OAUTH_SERVER_REQUEST;

@Component
@RequiredArgsConstructor
public class OAuth2AuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    private final TokenProvider tokenProvider;
    private final UserService userService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        String[] state = request.getParameter("state").split(",");
        String existingUserId = null;
        if (state != null && state.length == 2) {
            existingUserId = state[1];
        }

        OAuth2AuthenticationToken token = (OAuth2AuthenticationToken) authentication;
        OAuth2User principal = token.getPrincipal();
        String providerName = token.getAuthorizedClientRegistrationId();
        User user;
        try {
            user = userService.save(principal, providerName, existingUserId);
        } catch (BadRequestException e) {
            logger.error("Unable to find or create oauth user", e);
            getRedirectStrategy().sendRedirect(request, response, "/login");
            return;
        }

        if (response.isCommitted()) {
            logger.debug("Response has already been committed. Unable to redirect.");
            return;
        }

        String url;
        if (existingUserId == null) {
            url = "/authenticate/oauth?token=" + tokenProvider.createToken(user);
        } else {
            url = "/account/settings?success=" + providerName;
        }

        // If user visited a backend endpoint while unauthenticated then that URI is saved in
        // this session variable. This will redirect the now authenticated user to the previous URI
        // that was originally forbidden
        if (request.getSession().getAttribute(OAUTH_SERVER_REQUEST) != null) {
            request.getSession().removeAttribute(OAUTH_SERVER_REQUEST);
            String jwt = tokenProvider.createToken(user);
            Cookie cookie = new Cookie("jhi-authenticationToken", jwt);
            cookie.setPath("/");
            response.addCookie(cookie);
            super.onAuthenticationSuccess(request, response, authentication);
        } else {
            getRedirectStrategy().sendRedirect(request, response, url);
        }
    }
}
