package com.schoolaby.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.core.NestedRuntimeException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Base64;

@Service
@RequiredArgsConstructor
public class LtiContextHelper {
    private final ObjectMapper objectMapper;

    public String toBase64Url(Long journeyId) {
        try {
            byte[] jsonBytes = objectMapper.writeValueAsBytes(new LtiContext(journeyId));
            return Base64.getUrlEncoder().encodeToString(jsonBytes);
        } catch (JsonProcessingException e) {
            throw new LtiContextException("Failed to convert LtiContext to json!", e);
        }
    }

    public LtiContext parse(String context) {
        byte[] decode = Base64.getUrlDecoder().decode(context);
        try {
            return objectMapper.readValue(decode, LtiContext.class);
        } catch (IOException e) {
            throw new LtiContextException("Failed to convert json to LtiContext!", e);
        }
    }

    private static class LtiContextException extends NestedRuntimeException {
        public LtiContextException(String msg, Throwable cause) {
            super(msg, cause);
        }
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    public static class LtiContext {
        private Long journeyId;
    }
}
