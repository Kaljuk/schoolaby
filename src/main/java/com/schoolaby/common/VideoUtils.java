package com.schoolaby.common;

import ws.schild.jave.encode.AudioAttributes;
import ws.schild.jave.encode.EncodingAttributes;
import ws.schild.jave.encode.VideoAttributes;
import ws.schild.jave.encode.enums.X264_PROFILE;

import static org.apache.commons.io.FilenameUtils.removeExtension;

public class VideoUtils {
    private static final String AUDIO_CODEC = "aac";
    private static final int AUDIO_BITRATE = 64000;
    private static final int AUDIO_CHANNELS = 2;
    private static final int AUDIO_SAMPLING_RATE = 44100;

    private static final String VIDEO_OUTPUT_FORMAT = "mp4";
    private static final String VIDEO_CODEC = "h264";
    private static final int VIDEO_BIT_RATE = 500000;
    private static final int VIDEO_FRAME_RATE = 30;



    public static String replaceFileNameExtension(String fileName) {
        return String.format("%s.%s", removeExtension(fileName), VIDEO_OUTPUT_FORMAT);
    }

    public static EncodingAttributes getEncodingAttributes() {
        EncodingAttributes encodingAttributes = new EncodingAttributes();
        encodingAttributes.setOutputFormat(VIDEO_OUTPUT_FORMAT);
        encodingAttributes.setAudioAttributes(getAudioAttributes());
        encodingAttributes.setVideoAttributes(getVideoAttributes());
        return encodingAttributes;
    }

    public static AudioAttributes getAudioAttributes() {
        AudioAttributes audioAttributes = new AudioAttributes();
        audioAttributes.setCodec(AUDIO_CODEC);
        audioAttributes.setBitRate(AUDIO_BITRATE);
        audioAttributes.setChannels(AUDIO_CHANNELS);
        audioAttributes.setSamplingRate(AUDIO_SAMPLING_RATE);
        return audioAttributes;
    }

    public static VideoAttributes getVideoAttributes() {
        VideoAttributes videoAttributes = new VideoAttributes();
        videoAttributes.setCodec(VIDEO_CODEC);
        videoAttributes.setX264Profile(X264_PROFILE.BASELINE);
        videoAttributes.setBitRate(VIDEO_BIT_RATE);
        videoAttributes.setFrameRate(VIDEO_FRAME_RATE);
        return videoAttributes;
    }
}
