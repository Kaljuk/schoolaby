package com.schoolaby.common;

import com.schoolaby.domain.UploadedFile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

import static org.springframework.http.HttpStatus.FORBIDDEN;

public class FileUtil {

    public static final String USER_NOT_THE_CREATOR_OF_FILE = "Current user is not the creator of the file!";

    public static String readFile(String absolutePath) {
        try {
            return new String(new ClassPathResource(absolutePath).getInputStream().readAllBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String readFile(String relativePath, Class<?> relativeFrom) {
        try {
            return new String(new ClassPathResource(relativePath, relativeFrom).getInputStream().readAllBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void validateIsUserTheCreatorOfTheFile(UploadedFile uploadedFile) {
        if (!uploadedFile.isCreatedByCurrentUser()) {
            throw new ResponseStatusException(FORBIDDEN, USER_NOT_THE_CREATOR_OF_FILE);
        }
    }
}
