package com.schoolaby.common;

import java.util.HashMap;
import java.util.Map;

import static com.schoolaby.common.FileUtil.readFile;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

public class AutoPostingForm {
    private static final String FORM_TEMPLATE = readFile("auto-posting-form.html");
    private final String url;
    private final Map<String, String> params = new HashMap<>();

    public AutoPostingForm(String url) {
        this.url = url;
    }

    public AutoPostingForm addParam(String key, String value) {
        params.put(key, value);
        return this;
    }

    @Override
    public String toString() {
        return FORM_TEMPLATE
            .replaceFirst("\\{url}", url)
            .replaceFirst("\\{params}", params.entrySet().stream()
                .map(entry -> format("<input type=\"hidden\" name=\"%s\" value=\"%s\" />", entry.getKey(), entry.getValue()))
                .collect(joining()));
    }
}
