package com.schoolaby.logging.teams;

import org.zalando.problem.Problem;

public interface TeamsNotifier {

    void send(Problem problem);

    void send(RuntimeException exception);
}
