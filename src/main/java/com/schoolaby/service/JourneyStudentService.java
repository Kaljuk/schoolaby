package com.schoolaby.service;

import com.schoolaby.domain.JourneyStudent;
import com.schoolaby.domain.keys.JourneyUserKey;
import com.schoolaby.repository.JourneyStudentJpaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class JourneyStudentService {
    private final JourneyStudentJpaRepository journeyStudentRepository;

    public void delete(Long journeyId, Long userId) {
        journeyStudentRepository.deleteByJourneyIdAndUserId(journeyId, userId);
    }

    public JourneyStudent togglePin(Long journeyId, Long studentId) {
        JourneyUserKey journeyUserKey = new JourneyUserKey().setJourney(journeyId).setUser(studentId);
        Optional<JourneyStudent> journeyStudentOptional = journeyStudentRepository.findById(journeyUserKey);

        if (journeyStudentOptional.isPresent()) {
            JourneyStudent journeyStudent = journeyStudentOptional.get();
            handlePinnedDate(journeyStudent);
            return journeyStudent;
        }
        return null;
    }

    public void calculateJourneyStudentAverageGrade(Long journeyId, Long userId, Double percentage) {
        JourneyStudent journeyStudent = journeyStudentRepository.findByJourneyIdAndUserId(journeyId, userId);
        journeyStudent.setAverageGrade(percentage);
    }

    private void handlePinnedDate(JourneyStudent journeyStudent) {
        if (journeyStudent.getPinnedDate() == null) {
            journeyStudent.setPinnedDate(Instant.now());
        } else {
            journeyStudent.setPinnedDate(null);
        }
    }
}
