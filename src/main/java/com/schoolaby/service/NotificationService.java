package com.schoolaby.service;

import com.schoolaby.domain.*;
import com.schoolaby.repository.NotificationRepository;
import com.schoolaby.service.dto.NotificationDTO;
import com.schoolaby.service.mapper.NotificationMapper;
import lombok.RequiredArgsConstructor;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static com.schoolaby.domain.NotificationType.GENERAL;
import static com.schoolaby.domain.NotificationType.MESSAGE;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.HttpStatus.FORBIDDEN;

@Service
@Transactional
@RequiredArgsConstructor
public class NotificationService {
    private final Logger log = getLogger(NotificationService.class);
    private final NotificationRepository notificationRepository;
    private final NotificationMapper notificationMapper;
    private final GroupService groupService;

    private void saveSubmissionNotificationFromTemplate(Notification notificationTemplate, List<User> recipients, String additionalParam) {
        saveAll(createSubmissionNotificationsFromTemplate(notificationTemplate, recipients, additionalParam));
    }

    public Notification save(Notification notification) {
        log.debug("Request to create Notification : {}", notification);
        return notificationRepository.save(notification);
    }

    public List<Notification> saveAll(List<Notification> notifications) {
        log.debug("Request to create Notifications: {}", notifications);
        return notificationRepository.saveAll(notifications);
    }

    private List<Notification> createSubmissionNotificationsFromTemplate(Notification notificationTemplate, List<User> recipients, String additionalParam) {
        return createSubmissionNotifications(
            notificationTemplate.getAssignment(),
            notificationTemplate.getMessage(),
            notificationTemplate.getCreator(),
            recipients,
            additionalParam
        );
    }

    private List<Notification> createSubmissionNotifications(Assignment assignment, String message, User creator, List<User> recipients, String additionalParam) {
        Journey journey = assignment.getMilestone().getJourney();
        String link = format("/assignment/%s?journeyId=%s", assignment.getId(), journey.getId());

        if (additionalParam != null && !additionalParam.isBlank()) {
            link = link + additionalParam;
        }
        List<Notification> notifications = new ArrayList<>();

        for (User recipient : recipients) {
            Notification notification = new Notification()
                .message(message)
                .link(link)
                .type(GENERAL)
                .journey(journey)
                .assignment(assignment)
                .recipient(recipient)
                .creator(creator);
            notifications.add(notification);
        }

        return notifications;
    }

    private void createSolutionSubmittedNotificationFromTemplate(Notification notificationTemplate, List<User> recipients, Optional<Group> studentGroup) {
        String additionalParam;

        if (studentGroup.isPresent() && !notificationTemplate.getAssignment().getGradeGroupMembersIndividually()) {
            additionalParam = format("&groupId=%s", studentGroup.get().getId());
        } else {
            additionalParam = format("&studentId=%s", notificationTemplate.getCreator().getId());
        }
        saveSubmissionNotificationFromTemplate(notificationTemplate, recipients, additionalParam);
    }

    public void createSolutionSubmittedNotifications(Submission submission) {
        Assignment assignment = submission.getAssignment();
        List<User> teachers = new ArrayList<>(assignment.getMilestone().getJourney().getJourneyTeachersUsers());
        User student = submission.getAuthors().stream().findFirst().orElseThrow();
        Optional<Group> studentGroup = submission.getGroup() == null ?
            Optional.empty() :
            groupService.findOne(submission.getGroup().getId());

        Notification notificationTemplate = new Notification()
            .message("{SUBMITTED_SOLUTION}")
            .assignment(assignment)
            .creator(student);

        if (studentGroup.isPresent() && assignment.getGradeGroupMembersIndividually()) {
            createNotificationsForGroupMembers(notificationTemplate, teachers, studentGroup);
        } else {
            createSolutionSubmittedNotificationFromTemplate(notificationTemplate, teachers, studentGroup);
        }
    }

    private void createNotificationsForGroupMembers(Notification notificationTemplate, List<User> recipients, Optional<Group> studentGroup) {
        String groupParam = format("&groupId=%s", studentGroup.orElseThrow().getId());
        List<Notification> notifications = new ArrayList<>();

        studentGroup
            .orElseThrow()
            .getStudents()
            .forEach(groupMember ->
                notifications.addAll(createSubmissionNotificationsFromTemplate(
                    notificationTemplate.creator(groupMember),
                    recipients,
                    format("%s&studentId=%s", groupParam, groupMember.getId())
                ))
            );

        saveAll(notifications);
    }

    public void createSubmissionFeedbackNotification(SubmissionFeedback submissionFeedback) {
        Assignment assignment = submissionFeedback.getSubmission().getAssignment();
        User teacher = submissionFeedback.getCreator();
        List<User> students = new ArrayList<>();

        if (submissionFeedback.getGroup() != null) {
            students.addAll(submissionFeedback.getGroup().getStudents());
        } else {
            students.add(submissionFeedback.getStudent());
        }

        String message = submissionFeedback.isRejected() ? "{HAS_REJECTED}" : "{GIVEN_FEEDBACK}";
        saveAll(createSubmissionNotifications(assignment, message, teacher, students, null));
    }

    public List<NotificationDTO> update(List<PatchNotificationDTO> patchNotificationDTOs) {
        log.debug("Request to mark Notifications read for user : {}", getCurrentUserId());

        Map<Long, Instant> mapByIdAndRead = patchNotificationDTOs.stream()
            .collect(Collectors.toMap(PatchNotificationDTO::getId, PatchNotificationDTO::getRead));
        List<Notification> notificationsInDB = notificationRepository.findAll(Pageable.unpaged(), mapByIdAndRead.keySet()).getContent();
        notificationsInDB.forEach(notification -> {
            Long currentUserId = getCurrentUserId();
            if (!notification.getRecipient().getId().equals(currentUserId)) {
                throw new ResponseStatusException(FORBIDDEN, format("User %d is not a recipient of the notification %d!", currentUserId, notification.getId()));
            }
            notification.read(mapByIdAndRead.get(notification.getId()));
        });

        return notificationsInDB.stream().map(notificationMapper::toDto).collect(toList());
    }

    @Transactional(readOnly = true)
    public Page<NotificationDTO> findAllByNotificationType(Pageable pageable, NotificationType type) {
        log.debug("Request to get notifications by user {}", getCurrentUserId());
        return notificationRepository.findAll(pageable, type).map(notificationMapper::toDto);
    }

    public void createNotificationsForMessage(Chat chat, Message message) {
        List<Notification> notifications = chat.getPeople().stream()
            .filter(user -> !message.getCreator().equals(user))
            .map(user -> createNotification(chat, message, user))
            .collect(toList());
        notificationRepository.saveAll(notifications);
    }

    private Notification createNotification(Chat chat, Message message, User recipient) {
        Notification notification = new Notification();
        Submission submission = chat.getSubmission();
        URIBuilder uriBuilder = new URIBuilder();
        uriBuilder.setPath("/chats");
        uriBuilder.setParameter("journeyId", chat.getJourney().getId().toString());
        if (submission != null) {
            uriBuilder.setParameter("submissionId", submission.getId().toString());
            notification.assignment(submission.getAssignment());
        }
        notification
            .journey(chat.getJourney())
            .message("{RECEIVED_MESSAGE}")
            .type(MESSAGE)
            .link(uriBuilder.toString())
            .recipient(recipient)
            .creator(message.getCreator());
        return notification;
    }
}
