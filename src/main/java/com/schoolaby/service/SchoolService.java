package com.schoolaby.service;

import com.schoolaby.domain.School;
import com.schoolaby.repository.SchoolRepository;
import com.schoolaby.security.harid.HarIdUser;
import com.schoolaby.service.dto.EkoolSchoolsDTO.EkoolSchoolDTO;
import com.schoolaby.service.dto.SchoolDTO;
import com.schoolaby.service.mapper.SchoolMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.schoolaby.domain.enumeration.Country.ESTONIA;
import static java.util.stream.Collectors.toSet;

@Service
@Transactional
public class SchoolService {
    private final SchoolRepository schoolRepository;
    private final SchoolMapper schoolMapper;

    public SchoolService(SchoolRepository schoolRepository, SchoolMapper schoolMapper) {
        this.schoolRepository = schoolRepository;
        this.schoolMapper = schoolMapper;
    }

    public void saveHaridSchools(Set<HarIdUser.Role> roles) {
        Set<School> schools = roles.stream()
            .map(role -> new School()
                .ehisId(role.getProviderEhisId())
                .regNr(role.getProviderRegNr())
                .name(role.getProviderName())
                .country(ESTONIA.getValue())
            ).collect(toSet());
        save(schools);
    }

    public List<School> saveEkoolSchools(Set<EkoolSchoolDTO> ekoolSchools) {
        Set<School> schools = ekoolSchools.stream()
            .map(school -> new School()
                .ehisId(school.getEhisId())
                .regNr(school.getRegNr())
                .name(school.getName())
                .country(ESTONIA.getValue())
                .externalId(school.getId())
            ).collect(toSet());
        return save(schools);
    }

    private List<School> save(Set<School> schools) {
        return schoolRepository.saveAll(
            schools
                .stream()
                .distinct()
                .peek(school -> schoolRepository.findByRegNrOrEhisIdOrName(school.getRegNr(), school.getEhisId(), school.getName())
                    .ifPresent(existingSchool -> school.setId(existingSchool.getId())))
                .collect(toSet())
        );
    }

    public Set<School> findAllByIdIn(Set<Long> ids) {
        return schoolRepository.findAllByIdIn(ids);
    }

    public Set<SchoolDTO> findSchoolsBySearch(String search, String country) {
        return new HashSet<>(schoolMapper.toDto(schoolRepository.findAllByNameContainingIgnoreCaseAndCountry(search, country)));
    }

    public Optional<School> findByRegNrOrEhisIdOrName(String providerRegNr, String providerEhisId, String providerName) {
        return schoolRepository.findByRegNrOrEhisIdOrName(providerRegNr, providerEhisId, providerName);
    }
}
