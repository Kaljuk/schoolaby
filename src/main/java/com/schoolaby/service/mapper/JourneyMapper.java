package com.schoolaby.service.mapper;

import com.schoolaby.domain.Journey;
import com.schoolaby.domain.JourneyStudent;
import com.schoolaby.domain.User;
import com.schoolaby.service.dto.JourneyDTO;
import org.mapstruct.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneOffset;
import java.util.Set;

import static com.schoolaby.security.Role.Constants.*;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static com.schoolaby.security.SecurityUtils.isCurrentUserInRole;

@Mapper(uses = {UserMapper.class, MilestoneMapper.class, AssignmentMapper.class, EducationalLevelMapper.class, LtiConfigMapper.class, SubjectMapper.class, SchoolMapper.class, CurriculumMapper.class, JourneyStudentMapper.class})
public interface JourneyMapper extends EntityMapper<JourneyDTO, Journey> {
    @Mapping(source = "creator.id", target = "creatorId")
    @Mapping(source = "creator", target = "teacherName", qualifiedByName = "mapJourneyTeacherName")
    @Mapping(source = "students", target = "studentCount", qualifiedByName = "mapStudentsCount")
    @Mapping(source = ".", target = "progress", qualifiedByName = "mapJourneyProgress")
    @Mapping(source = ".", target = "assignmentsCount", qualifiedByName = "mapAssignmentsCount")
    @Mapping(source = ".", target = "daysCount", qualifiedByName = "mapDaysCount")
    @Mapping(source = ".", target = "monthsCount", qualifiedByName = "mapMonthsCount")
    @Mapping(source = ".", target = "yearsCount", qualifiedByName = "mapYearsCount")
    @Mapping(source = ".", target = "teacherSignupCode", qualifiedByName = "mapTeacherSignupCode")
    @Mapping(source = ".", target = "studentSignupCode", qualifiedByName = "mapStudentSignupCode")
    @Mapping(target = "milestones", ignore = true)
    JourneyDTO toDto(Journey journey);

    @Mapping(source = "creatorId", target = "creator")
    Journey toEntity(JourneyDTO journeyDTO);

    @Named("mapJourneyTeacherName")
    default String mapJourneyTeacherName(User creator) {
        if (creator != null) {
            return String.format("%s %s", creator.getFirstName(), creator.getLastName());
        }
        return null;
    }

    @Named("mapJourneyProgress")
    default Long mapJourneyProgress(Journey journey) {
        return journey.getJourneyProgress();
    }

    @Named("mapStudentsCount")
    default int mapStudentsCount(Set<JourneyStudent> students) {
        if (students != null) {
            return students.size();
        }

        return 0;
    }

    @Named("mapAssignmentsCount")
    default int mapAssignmentsCount(Journey journey) {
        return journey.getAssignments().size();
    }

    @Named("mapDaysCount")
    default int mapDaysCount(Journey journey) {
        return getJourneyPeriod(journey).getDays();
    }

    @Named("mapMonthsCount")
    default int mapMonthsCount(Journey journey) {
        return getJourneyPeriod(journey).getMonths();
    }

    @Named("mapYearsCount")
    default int mapYearsCount(Journey journey) {
        return getJourneyPeriod(journey).getYears();
    }

    @Named("mapTeacherSignupCode")
    default String mapTeacherSignupCode(Journey journey) {
        if (isCurrentUserInRole(ADMIN) || (getCurrentUserId() != null && journey.getCreator().getId().equals(getCurrentUserId()))) {
            return journey.getSignupCode(TEACHER);
        }
        return null;
    }

    @Named("mapStudentSignupCode")
    default String mapStudentSignupCode(Journey journey) {
        if (isCurrentUserInRole(ADMIN) || (getCurrentUserId() != null && journey.isTeacherInJourney(getCurrentUserId()))) {
            return journey.getSignupCode(STUDENT);
        }
        return null;
    }

    default Period getJourneyPeriod(Journey journey) {
        LocalDate startDate = LocalDateTime.ofInstant(journey.getStartDate(), ZoneOffset.UTC).toLocalDate();
        LocalDate endDate = LocalDateTime.ofInstant(journey.getEndDate(), ZoneOffset.UTC).toLocalDate();
        return Period.between(startDate, endDate);
    }

    default Journey fromId(Long id) {
        if (id == null) {
            return null;
        }
        Journey journey = new Journey();
        journey.setId(id);
        return journey;
    }
}
