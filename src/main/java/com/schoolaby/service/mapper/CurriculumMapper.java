package com.schoolaby.service.mapper;

import com.schoolaby.domain.Curriculum;
import com.schoolaby.service.dto.CurriculumDTO;
import org.mapstruct.Mapper;

@Mapper
public interface CurriculumMapper extends EntityMapper<CurriculumDTO, Curriculum> {
}
