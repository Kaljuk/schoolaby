package com.schoolaby.service.mapper;

import com.schoolaby.domain.Group;
import com.schoolaby.domain.Submission;
import com.schoolaby.service.dto.GroupDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Set;

@Mapper(uses = {UserMapper.class, SubmissionMapper.class})
public interface GroupMapper extends EntityMapper<GroupDTO, Group> {
    @Mapping(source = "submissions", target = "hasSubmission", qualifiedByName = "mapHasSubmission")
    GroupDTO toDto(Group group);

    @Named("mapHasSubmission")
    default boolean mapHasSubmission(Set<Submission> submissions) {
        return submissions != null && !submissions.isEmpty();
    }

    default Group fromId(Long id) {
        if (id == null) {
            return null;
        }
        return new Group().setId(id);
    }
}
