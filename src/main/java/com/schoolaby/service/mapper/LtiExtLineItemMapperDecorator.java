package com.schoolaby.service.mapper;

import com.schoolaby.common.LtiContextHelper;
import com.schoolaby.config.lti.LtiAdvantageProperties;
import com.schoolaby.domain.LtiLineItem;
import com.schoolaby.service.dto.lti.external.LtiExtLineItemDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.ArrayList;
import java.util.Collection;

import static java.lang.String.format;

public abstract class LtiExtLineItemMapperDecorator implements LtiExtLineItemMapper {
    @Autowired
    @Qualifier("delegate")
    private LtiExtLineItemMapper delegate;
    @Autowired
    private LtiContextHelper contextHelper;
    @Autowired
    private LtiAdvantageProperties properties;

    @Override
    public LtiExtLineItemDTO toDto(LtiLineItem lineItem) {
        return delegate.toDto(lineItem)
            .setId(getUri(lineItem));
    }

    @Override
    public Collection<LtiExtLineItemDTO> toDto(Collection<LtiLineItem> entities) {
        if (entities == null) {
            return null;
        }

        Collection<LtiExtLineItemDTO> collection = new ArrayList<>(entities.size());
        for (LtiLineItem ltiLineItem : entities) {
            collection.add(toDto(ltiLineItem));
        }

        return collection;
    }


    private String getUri(LtiLineItem lineItem) {
        String host = properties.getIss();
        Long journeyId = lineItem.getLtiResource().getJourney().getId();
        String context = contextHelper.toBase64Url(journeyId);
        return format("%s/api/lti-advantage/%s/lineitems/%s", host, context, lineItem.getId());
    }
}
