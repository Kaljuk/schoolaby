package com.schoolaby.service.mapper;

import com.schoolaby.domain.JourneyTeacher;
import com.schoolaby.service.dto.JourneyTeacherDTO;
import org.mapstruct.Mapper;

@Mapper(uses = {UserMapper.class})
public interface JourneyTeacherMapper extends EntityMapper<JourneyTeacherDTO, JourneyTeacher> {
}
