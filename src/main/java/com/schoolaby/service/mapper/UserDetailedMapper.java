package com.schoolaby.service.mapper;

import com.schoolaby.domain.Authority;
import com.schoolaby.domain.User;
import com.schoolaby.service.dto.UserDetailedDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toSet;

/**
 * Mapper for the entity {@link User} and its DTO called {@link UserDetailedDTO}.
 * <p>
 * Normal mappers are generated using MapStruct, this one is hand-coded as MapStruct
 * support is still in beta, and requires a manual step with an IDE.
 */
@Service
@RequiredArgsConstructor
public class UserDetailedMapper {

    private final PersonRoleMapper personRoleMapper;

    public List<UserDetailedDTO> usersToUserDTOs(List<User> users) {
        return users.stream().filter(Objects::nonNull).map(this::userToUserDTO).collect(Collectors.toList());
    }

    public UserDetailedDTO userToUserDTO(User user) {
        return new UserDetailedDTO()
            .setId(user.getId())
            .setLogin(user.getLogin())
            .setFirstName(user.getFirstName())
            .setLastName(user.getLastName())
            .setEmail(user.getEmail())
            .setPhoneNumber(user.getPhoneNumber())
            .setPersonalCode(user.getPersonalCode())
            .setActivated(user.isActivated())
            .setImageUrl(user.getImageUrl())
            .setLangKey(user.getLangKey())
            .setCountry(user.getCountry())
            .setPersonRoles(new HashSet<>(personRoleMapper.toDto(user.getPersonRoles())))
            .setTermsAgreed(user.isTermsAgreed())
            .setFirstLogin(user.isFirstLogin())
            .setAuthorities(user
                .getAuthorities()
                .stream()
                .map(Authority::getName)
                .collect(toSet()))
            .setExternalAuthentications(user
                .getExternalAuthentications()
                .stream()
                .map(auth ->
                    auth.getType()
                        .toString()
                        .toLowerCase())
                .collect(toSet()));
    }

    public List<User> userDTOsToUsers(List<UserDetailedDTO> userDTOs) {
        return userDTOs.stream().filter(Objects::nonNull).map(this::userDTOToUser).collect(Collectors.toList());
    }

    public User userDTOToUser(UserDetailedDTO userDTO) {
        if (userDTO == null) {
            return null;
        } else {
            User user = new User();
            user.setId(userDTO.getId());
            user.setLogin(userDTO.getLogin());
            user.setFirstName(userDTO.getFirstName());
            user.setLastName(userDTO.getLastName());
            user.setEmail(userDTO.getEmail());
            user.setImageUrl(userDTO.getImageUrl());
            user.setActivated(userDTO.isActivated());
            user.setCountry(userDTO.getCountry());
            user.setLangKey(userDTO.getLangKey());
            Set<Authority> authorities = this.authoritiesFromStrings(userDTO.getAuthorities());
            user.setAuthorities(authorities);
            user.setPersonRoles(new HashSet<>(personRoleMapper.toEntity(userDTO.getPersonRoles())));
            user.setFirstLogin(userDTO.isFirstLogin());
            return user;
        }
    }

    private Set<Authority> authoritiesFromStrings(Set<String> authoritiesAsString) {
        Set<Authority> authorities = new HashSet<>();

        if (authoritiesAsString != null) {
            authorities =
                authoritiesAsString
                    .stream()
                    .map(
                        string -> {
                            Authority auth = new Authority();
                            auth.setName(string);
                            return auth;
                        }
                    )
                    .collect(Collectors.toSet());
        }

        return authorities;
    }

    public static User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }
}
