package com.schoolaby.service.mapper;

import com.schoolaby.domain.SelectedCriterionLevel;
import com.schoolaby.service.dto.SelectedCriterionLevelDTO;
import org.mapstruct.Mapper;

@Mapper(uses = {SubjectMapper.class, CriterionLevelMapper.class})
public interface SelectedCriterionLevelMapper extends EntityMapper<SelectedCriterionLevelDTO, SelectedCriterionLevel> {
}
