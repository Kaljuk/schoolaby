package com.schoolaby.service.mapper;

import com.schoolaby.domain.User;
import com.schoolaby.service.dto.UserDTO;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper extends EntityMapper<UserDTO, User> {
    default User fromId(Long id) {
        if (id == null) {
            return null;
        }
        return new User().id(id);
    }
}
