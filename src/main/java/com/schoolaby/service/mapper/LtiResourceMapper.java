package com.schoolaby.service.mapper;

import com.schoolaby.domain.LtiResource;
import com.schoolaby.service.dto.lti.LtiResourceDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {LtiAppMapper.class})
public interface LtiResourceMapper extends EntityMapper<LtiResourceDTO, LtiResource> {
    @Mapping(source = "assignment.id", target = "assignmentId")
    LtiResourceDTO toDto(LtiResource ltiResource);

    LtiResource toEntity(LtiResourceDTO ltiResourceDTO);
}
