package com.schoolaby.service.mapper;

import com.schoolaby.domain.CriterionLevel;
import com.schoolaby.service.dto.CriterionLevelDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface CriterionLevelMapper extends EntityMapper<CriterionLevelDTO, CriterionLevel> {

    @Mapping(source = "criterion.id", target = "criterionId")
    CriterionLevelDTO toDto(CriterionLevel entity);
}
