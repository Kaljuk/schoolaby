package com.schoolaby.service.mapper;

import com.schoolaby.domain.SubmissionFeedback;
import com.schoolaby.service.dto.submission.SubmissionFeedbackDTO;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.LinkedHashSet;
import java.util.Set;

@Mapper(uses = {UserMapper.class, SubmissionMapper.class, SelectedCriterionLevelMapper.class, GroupMapper.class})
public interface SubmissionFeedbackMapper extends EntityMapper<SubmissionFeedbackDTO, SubmissionFeedback> {
    @Mapping(source = "submission.id", target = "submissionId")
    @Mapping(source = "student.id", target = "studentId")
    @Mapping(source = "group.id", target = "groupId")
    @Mapping(source = "creator.id", target = "creatorId")
    SubmissionFeedbackDTO toDto(SubmissionFeedback submissionFeedback);

    @IterableMapping(qualifiedByName = "mapWithoutSubmission")
    LinkedHashSet<SubmissionFeedbackDTO> toDto(Set<SubmissionFeedback> submissionFeedbackList);

    @Named("mapWithoutSubmission")
    @Mapping(target = "submission", ignore = true)
    @Mapping(source = "submission.id", target = "submissionId")
    @Mapping(source = "student.id", target = "studentId")
    @Mapping(source = "group.id", target = "groupId")
    @Mapping(source = "creator.id", target = "creatorId")
    SubmissionFeedbackDTO mapWithoutSubmission(SubmissionFeedback submissionFeedback);

    @Mapping(source = "submissionId", target = "submission")
    @Mapping(source = "studentId", target = "student")
    @Mapping(source = "groupId", target = "group")
    @Mapping(source = "creatorId", target = "creator")
    SubmissionFeedback toEntity(SubmissionFeedbackDTO submissionFeedbackDTO);

    @Named("toNewEntity")
    @Mapping(source = "studentId", target = "student")
    @Mapping(source = "groupId", target = "group")
    @Mapping(source = "creatorId", target = "creator")
    SubmissionFeedback toNewEntity(SubmissionFeedbackDTO submissionFeedbackDTO);
}
