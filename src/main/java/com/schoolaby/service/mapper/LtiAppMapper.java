package com.schoolaby.service.mapper;

import com.schoolaby.domain.LtiApp;
import com.schoolaby.service.dto.LtiAppDTO;
import org.mapstruct.Mapper;

@Mapper(uses = {LtiConfigMapper.class})
public interface LtiAppMapper extends EntityMapper<LtiAppDTO, LtiApp> {
    LtiAppDTO toDto(LtiApp ltiApp);

    LtiApp toEntity(LtiAppDTO ltiAppDTO);
}
