package com.schoolaby.service.mapper;

import com.schoolaby.domain.Submission;
import com.schoolaby.service.dto.submission.SubmissionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {UserMapper.class, AssignmentMapper.class, UploadedFileMapper.class, GroupMapper.class, SubmissionFeedbackMapper.class})
public interface SubmissionMapper extends EntityMapper<SubmissionDTO, Submission> {
    @Mapping(source = "assignment.id", target = "assignmentId")
    @Mapping(source = "assignment.title", target = "assignmentTitle")
    @Mapping(source = "group.id", target = "groupId")
    @Mapping(source = "group.name", target = "groupName")
    SubmissionDTO toDto(Submission submission);

    @Mapping(target = "removeUploadedFile", ignore = true)
    @Mapping(source = "groupId", target = "group")
    @Mapping(source = "assignmentId", target = "assignment")
    Submission toEntity(SubmissionDTO submissionDTO);

    default Submission fromId(Long id) {
        if (id == null) {
            return null;
        }
        Submission submission = new Submission();
        submission.setId(id);
        return submission;
    }
}
