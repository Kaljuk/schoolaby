package com.schoolaby.service.mapper;

import com.schoolaby.domain.School;
import com.schoolaby.service.dto.SchoolDTO;
import org.mapstruct.Mapper;

@Mapper
public interface SchoolMapper extends EntityMapper<SchoolDTO, School> {
    default School fromId(Long id) {
        if (id == null) {
            return null;
        }
        School school = new School();
        school.setId(id);
        return school;
    }
}
