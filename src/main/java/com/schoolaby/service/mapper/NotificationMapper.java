package com.schoolaby.service.mapper;

import com.schoolaby.domain.Assignment;
import com.schoolaby.domain.Group;
import com.schoolaby.domain.Notification;
import com.schoolaby.domain.User;
import com.schoolaby.service.dto.NotificationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Optional;


@Mapper
public interface NotificationMapper extends EntityMapper<NotificationDTO, Notification> {
    @Mapping(source = "journey.title", target = "journeyName")
    @Mapping(source = "journey.subject", target = "subject")
    @Mapping(source = "journey.id", target = "journeyId")
    @Mapping(source = "assignment.title", target = "assignmentName")
    @Mapping(source = "creator", target = "creatorName", qualifiedByName = "mapCreatorName")
    @Mapping(source = ".", target = "groupName", qualifiedByName = "mapGroupName")
    NotificationDTO toDto(Notification notification);

    @Mapping(source = "subject", target = "journey.subject")
    Notification toEntity(NotificationDTO notificationDTO);

    @Named("mapCreatorName")
    default String mapCreatorName(User creator) {
        if (creator != null) {
            return String.format("%s %s", creator.getFirstName(), creator.getLastName());
        }
        return null;
    }

    @Named("mapGroupName")
    default String mapGroupName(Notification notification) {
        Assignment assignment = notification.getAssignment();
        if (notification.getMessage().contains("{SUBMITTED_SOLUTION}") && assignment != null && !assignment.getGradeGroupMembersIndividually() && !assignment.getGroups().isEmpty()) {
            Optional<Group> group = assignment.getGroups()
                .stream()
                .filter(assignmentGroup -> assignmentGroup.getStudents().contains(notification.getCreator()))
                .findFirst();
            if (group.isPresent()) {
                return group.get().getName();
            }
        }
        return null;
    }

    default Notification fromId(Long id) {
        if (id == null) {
            return null;
        }
        Notification notification = new Notification();
        notification.setId(id);
        return notification;
    }
}
