package com.schoolaby.service.mapper;

import com.schoolaby.domain.Criterion;
import com.schoolaby.domain.CriterionLevel;
import com.schoolaby.service.dto.CriterionDTO;
import com.schoolaby.service.dto.CriterionLevelDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper
public interface CriterionMapper extends EntityMapper<CriterionDTO, Criterion> {
    CriterionLevelMapper criterionLevelMapper = Mappers.getMapper(CriterionLevelMapper.class);

    default Set<CriterionLevelDTO> setLevels (Set<CriterionLevel> criterionLevels) {
        return criterionLevels.stream().map(criterionLevelMapper::toDto)
            .sorted(Comparator.comparingLong(CriterionLevelDTO::getSequenceNumber))
            .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    @Mapping(source = "levels", target = "minPoints", qualifiedByName = "calculateMinPoints")
    @Mapping(source = "levels", target = "maxPoints", qualifiedByName = "calculateMaxPoints")
    CriterionDTO toDto(Criterion entity);

    @Named("calculateMinPoints")
    static Long calculateMinPoints(Set<CriterionLevel> criterionLevels) {
        if (criterionLevels.size() > 0) {
            return criterionLevels.stream()
                .min(Comparator.comparing(CriterionLevel::getPoints))
                .orElseThrow(NoSuchElementException::new)
                .getPoints();
        }
        return null;
    }

    @Named("calculateMaxPoints")
    static Long calculateMaxPoints(Set<CriterionLevel> criterionLevels) {
        if (criterionLevels.size() > 0) {
            return criterionLevels.stream()
                .max(Comparator.comparing(CriterionLevel::getPoints))
                .orElseThrow(NoSuchElementException::new)
                .getPoints();
        }
        return null;
    }
}
