package com.schoolaby.service.mapper;

import com.schoolaby.domain.EducationalAlignment;
import com.schoolaby.service.dto.EducationalAlignmentDTO;
import org.mapstruct.Mapper;

@Mapper
public interface EducationalAlignmentMapper extends EntityMapper<EducationalAlignmentDTO, EducationalAlignment> {
    EducationalAlignment toEntity(EducationalAlignmentDTO educationalAlignmentDTO);

    default EducationalAlignment fromId(Long id) {
        if (id == null) {
            return null;
        }
        EducationalAlignment educationalAlignment = new EducationalAlignment();
        educationalAlignment.setId(id);
        return educationalAlignment;
    }
}
