package com.schoolaby.service.mapper;

import com.schoolaby.domain.LtiScore;
import com.schoolaby.service.dto.lti.external.LtiExtScoreDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {UserMapper.class})
public interface LtiExtScoreMapper extends EntityMapper<LtiExtScoreDTO, LtiScore> {

    LtiExtScoreDTO toDto(LtiScore score);

    @Mapping(source = "userId", target = "user")
    LtiScore toEntity(LtiExtScoreDTO scoreDTO);

    default LtiScore fromId(Long id) {
        if (id == null) {
            return null;
        }
        LtiScore score = new LtiScore();
        score.setId(id);
        return score;
    }
}
