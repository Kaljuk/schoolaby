package com.schoolaby.service.mapper;

import com.schoolaby.domain.JourneySignupCode;
import com.schoolaby.service.dto.JourneySignupCodeDTO;
import org.mapstruct.Mapper;

@Mapper
public interface JourneySignupCodeMapper extends EntityMapper<JourneySignupCodeDTO, JourneySignupCode> {
}
