package com.schoolaby.service.mapper;

import com.schoolaby.domain.LtiLineItem;
import com.schoolaby.service.dto.lti.external.LtiExtLineItemDTO;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper
@DecoratedWith(LtiExtLineItemMapperDecorator.class)
public interface LtiExtLineItemMapper extends EntityMapper<LtiExtLineItemDTO, LtiLineItem> {

    @Mapping(source = "ltiResource.resourceLinkId", target = "resourceLinkId")
    @Mapping(source = "ltiResource.toolResourceId", target = "resourceId")
    LtiExtLineItemDTO toDto(LtiLineItem lineItem);

    LtiLineItem toEntity(LtiExtLineItemDTO lineItem);
}
