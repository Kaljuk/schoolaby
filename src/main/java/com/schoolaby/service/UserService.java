package com.schoolaby.service;

import com.schoolaby.config.Constants;
import com.schoolaby.domain.Authority;
import com.schoolaby.domain.ExternalAuthentication.ExternalAuthenticationType;
import com.schoolaby.domain.PersonRole;
import com.schoolaby.domain.School;
import com.schoolaby.domain.User;
import com.schoolaby.repository.AuthorityRepository;
import com.schoolaby.repository.PersonRoleRepository;
import com.schoolaby.repository.UserRepository;
import com.schoolaby.security.Role;
import com.schoolaby.security.SecurityUtils;
import com.schoolaby.security.harid.HarIdUser;
import com.schoolaby.service.dto.EkoolRolesDTO;
import com.schoolaby.service.dto.PersonRoleDTO;
import com.schoolaby.service.dto.UserDetailedDTO;
import com.schoolaby.service.mapper.EkoolRoleMapper;
import com.schoolaby.service.mapper.HarIdRoleMapper;
import com.schoolaby.service.mapper.PersonRoleMapper;
import com.schoolaby.service.mapper.UserDetailedMapper;
import io.github.jhipster.security.RandomUtil;
import io.undertow.util.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static com.schoolaby.config.Constants.DEFAULT_LANGUAGE;
import static com.schoolaby.domain.ExternalAuthentication.ExternalAuthenticationType.*;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static io.github.jhipster.security.RandomUtil.generateActivationKey;
import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.RandomStringUtils.random;
import static org.springframework.http.HttpStatus.FORBIDDEN;

@Service
@Transactional
public class UserService {
    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final EkoolService ekoolService;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthorityRepository authorityRepository;
    private final HarIdRoleMapper harIdRoleMapper;
    private final EkoolRoleMapper ekoolRoleMapper;
    private final SchoolService schoolService;
    private final CacheManager cacheManager;
    private final UserDetailedMapper userDetailedMapper;
    private final PersonRoleMapper personRoleMapper;
    private final PersonRoleRepository personRoleRepository;

    public UserService(
        EkoolService ekoolService, UserRepository userRepository,
        PasswordEncoder passwordEncoder,
        AuthorityRepository authorityRepository,
        HarIdRoleMapper harIdRoleMapper,
        EkoolRoleMapper ekoolRoleMapper,
        SchoolService schoolService,
        CacheManager cacheManager,
        UserDetailedMapper userDetailedMapper,
        PersonRoleMapper personRoleMapper,
        PersonRoleRepository personRoleRepository) {
        this.ekoolService = ekoolService;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
        this.harIdRoleMapper = harIdRoleMapper;
        this.ekoolRoleMapper = ekoolRoleMapper;
        this.schoolService = schoolService;
        this.cacheManager = cacheManager;
        this.userDetailedMapper = userDetailedMapper;
        this.personRoleMapper = personRoleMapper;
        this.personRoleRepository = personRoleRepository;
    }

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository
            .findOneByActivationKey(key)
            .map(
                user -> {
                    // activate given user for the registration key.
                    user.setActivated(true);
                    user.setActivationKey(null);
                    this.clearUserCaches(user);
                    log.debug("Activated user: {}", user);
                    return user;
                }
            );
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
        log.debug("Reset user password for reset key {}", key);
        return userRepository
            .findOneByResetKey(key)
            .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400)))
            .map(
                user -> {
                    user.setPassword(passwordEncoder.encode(newPassword));
                    user.setResetKey(null);
                    user.setResetDate(null);
                    this.clearUserCaches(user);
                    return user;
                }
            );
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository
            .findOneByEmailIgnoreCase(mail)
            .filter(User::isActivated)
            .map(
                user -> {
                    user.setResetKey(RandomUtil.generateResetKey());
                    user.setResetDate(Instant.now());
                    this.clearUserCaches(user);
                    return user;
                }
            );
    }

    public User registerUser(UserDetailedDTO userDTO, String password) {
        userRepository
            .findOneByLogin(userDTO.getLogin().toLowerCase())
            .ifPresent(
                existingUser -> {
                    boolean removed = removeNonActivatedUser(existingUser);
                    if (!removed) {
                        throw new UsernameAlreadyUsedException();
                    }
                }
            );
        userRepository
            .findOneByEmailIgnoreCase(userDTO.getEmail())
            .ifPresent(
                existingUser -> {
                    boolean removed = removeNonActivatedUser(existingUser);
                    if (!removed) {
                        throw new EmailAlreadyUsedException();
                    }
                }
            );

        validateUserAuthorities(userDTO.getAuthorities());
        User newUser = userDetailedMapper.userDTOToUser(userDTO);
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(userDTO.getLogin().toLowerCase());
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        if (userDTO.getEmail() != null) {
            newUser.setEmail(userDTO.getEmail().toLowerCase());
        }
        // new user is active by default, as children might not know how to use email
        newUser.setActivated(true);
        newUser.setActivationKey(generateActivationKey());
        newUser.setPersonRoles(attachPersonRoles(
            new HashSet<>(personRoleMapper.toEntity(userDTO.getPersonRoles()))
        ));
        newUser.setTermsAgreed(userDTO.isTermsAgreed());
        save(newUser);
        this.clearUserCaches(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    private boolean removeNonActivatedUser(User existingUser) {
        if (existingUser.isActivated()) {
            return false;
        }
        userRepository.delete(existingUser);
        userRepository.flush();
        this.clearUserCaches(existingUser);
        return true;
    }

    public User createUser(UserDetailedDTO userDTO) {
        User user = userDetailedMapper.userDTOToUser(userDTO);
        user.setLogin(userDTO.getLogin().toLowerCase());
        if (userDTO.getEmail() != null) {
            user.setEmail(userDTO.getEmail().toLowerCase());
        }
        if (userDTO.getLangKey() == null) {
            user.setLangKey(DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(true);
        if (userDTO.getAuthorities() != null) {
            Set<Authority> authorities = userDTO
                .getAuthorities()
                .stream()
                .map(authorityRepository::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toSet());
            user.setAuthorities(authorities);
        }
        user.setPersonRoles(attachPersonRoles(
                new HashSet<>(personRoleMapper.toEntity(userDTO.getPersonRoles()))
            )
        );
        save(user);
        this.clearUserCaches(user);
        log.debug("Created Information for User: {}", user);
        return user;
    }

    public Optional<UserDetailedDTO> updateUser(UserDetailedDTO userDTO) {
        return Optional
            .of(userRepository.findById(userDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(
                user -> {
                    this.clearUserCaches(user);
                    user.setLogin(userDTO.getLogin().toLowerCase());
                    user.setFirstName(userDTO.getFirstName());
                    user.setLastName(userDTO.getLastName());
                    if (userDTO.getEmail() != null) {
                        user.setEmail(userDTO.getEmail().toLowerCase());
                    }
                    user.setImageUrl(userDTO.getImageUrl());
                    user.setActivated(userDTO.isActivated());
                    user.setLangKey(userDTO.getLangKey());
                    Set<Authority> managedAuthorities = user.getAuthorities();
                    managedAuthorities.clear();
                    userDTO
                        .getAuthorities()
                        .stream()
                        .map(authorityRepository::findById)
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .forEach(managedAuthorities::add);
                    user.setPersonRoles(attachPersonRoles(
                            new HashSet<>(personRoleMapper.toEntity(userDTO.getPersonRoles()))
                        )
                    );
                    this.clearUserCaches(user);
                    log.debug("Changed Information for User: {}", user);
                    return user;
                }
            )
            .map(userDetailedMapper::userToUserDTO);
    }

    public void deleteUser(String login) {
        userRepository
            .findOneByLogin(login)
            .ifPresent(
                user -> {
                    userRepository.delete(user);
                    this.clearUserCaches(user);
                    log.debug("Deleted User: {}", user);
                }
            );
    }

    public void deleteUser(User user) {
        userRepository.delete(user);
    }

    public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl, Set<String> authorities, String country, Set<PersonRoleDTO> personRoleDTOs, boolean termsAgreed) {
        SecurityUtils
            .getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(
                user -> {
                    user.setFirstName(firstName);
                    user.setLastName(lastName);
                    if (email != null) {
                        user.setEmail(email.toLowerCase());
                    }
                    user.setLangKey(langKey);
                    user.setImageUrl(imageUrl);
                    validateUserAuthorities(authorities);
                    user.setAuthorities(authorities.stream().map(authority -> new Authority().name(authority)).collect(toSet()));
                    user.setCountry(country);
                    user.setPersonRoles(attachPersonRoles(
                            new HashSet<>(personRoleMapper.toEntity(personRoleDTOs))
                        )
                    );
                    user.setTermsAgreed(termsAgreed);
                    this.clearUserCaches(user);
                    log.debug("Changed Information for User: {}", user);
                }
            );
    }

    private Set<PersonRole> attachPersonRoles(Set<PersonRole> personRoles) {
        Set<PersonRole> attachedPersonRoles = new HashSet<>();
        Set<PersonRole> existingPersonRoles = personRoleRepository.findAllByIdIn(getExistingPersonRoleIds(personRoles));
        Set<School> detachedSchools = schoolService.findAllByIdIn(getDetachedSchoolIds(personRoles));

        personRoles.forEach(personRole -> {
            if (personRole.getId() != null) {
                PersonRole attachedPersonRole = getPersonRoleFromSetById(existingPersonRoles, personRole.getId());
                attachedPersonRoles.add(attachedPersonRole
                    .role(personRole.getRole())
                    .school(personRole.getSchool()));
            } else {
                personRole.setSchool(getSchoolFromSetById(detachedSchools, personRole.getSchool().getId()));
                attachedPersonRoles.add(personRole);
            }
        });

        return attachedPersonRoles;
    }

    private Set<Long> getExistingPersonRoleIds(Set<PersonRole> personRoles) {
        return personRoles.stream()
            .map(PersonRole::getId)
            .filter(Objects::nonNull)
            .collect(toSet());
    }

    private Set<Long> getDetachedSchoolIds(Set<PersonRole> personRoles) {
        return personRoles.stream()
            .filter(personRole -> personRole.getId() == null)
            .map(personRole -> personRole.getSchool().getId())
            .collect(toSet());
    }

    private PersonRole getPersonRoleFromSetById(Set<PersonRole> existingPersonRoles, Long personRoleId) {
        return existingPersonRoles
            .stream()
            .filter(existingPersonRole -> existingPersonRole.getId().equals(personRoleId))
            .findFirst()
            .orElseThrow();
    }

    private School getSchoolFromSetById(Set<School> schools, Long schoolId) {
        return schools
            .stream()
            .filter(school -> school.getId().equals(schoolId))
            .findFirst()
            .orElseThrow();
    }

    @Transactional
    public void changePassword(String currentClearTextPassword, String newPassword) {
        SecurityUtils
            .getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(
                user -> {
                    String currentEncryptedPassword = user.getPassword();
                    if (!passwordEncoder.matches(currentClearTextPassword, currentEncryptedPassword)) {
                        throw new InvalidPasswordException();
                    }
                    String encryptedPassword = passwordEncoder.encode(newPassword);
                    user.setPassword(encryptedPassword);
                    this.clearUserCaches(user);
                    log.debug("Changed password for User: {}", user);
                }
            );
    }

    @Transactional(readOnly = true)
    public Page<UserDetailedDTO> findAllManagedUsers(Pageable pageable) {
        return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(userDetailedMapper::userToUserDTO);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithAuthoritiesByLogin(login);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        Long currentUserId = SecurityUtils.getCurrentUserId();
        return userRepository.findOneWithAuthoritiesById(currentUserId);
    }

    public User findUserByExternalId(String sub) {
        Optional<User> user = userRepository.findOneByExternalId(sub);
        return user.orElse(null);
    }

    public UserDetailedDTO getAccountDto() {
        return getUserWithAuthorities()
            .map(userDetailedMapper::userToUserDTO)
            .orElseThrow(() -> new AccountResourceException("User could not be found"));
    }

    public void agreeToTerms() {
        getUserWithAuthorities().orElseThrow().setTermsAgreed(true);
    }

    public void firstLogin() {
        getUserWithAuthorities().orElseThrow().setFirstLogin(false);
    }

    /**
     * Not activated users should be automatically deleted after 14 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        userRepository
            .findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(Instant.now().minus(14, ChronoUnit.DAYS))
            .forEach(
                user -> {
                    log.debug("Deleting not activated user {}", user.getLogin());
                    userRepository.delete(user);
                    this.clearUserCaches(user);
                }
            );
    }

    @Transactional(readOnly = true)
    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(toList());
    }

    @Transactional(readOnly = true)
    public Optional<User> findOneByEmailIgnoreCase(String email) {
        return userRepository.findOneByEmailIgnoreCase(email);
    }

    @Transactional(readOnly = true)
    public Optional<User> findOneByLogin(String login) {
        return userRepository.findOneByLogin(login);
    }

    public User save(OAuth2User principal, String providerName, String existingUserId) throws BadRequestException {
        //TODO: You can't randomly find a user by ID as two students might use their parents email.
        //TODO: SCHOOL-370 - Remove this after manual oauth2 connection flow is implemented (Users decide which accounts are joined)
        ExternalAuthenticationType providerType = valueOf(providerName.toUpperCase());
        String sub = principal.getName();
        Optional<User> existingOauthUser = userRepository.findOneByExternalAuthentication(sub, providerType);
        Optional<User> existingEmailUser = Optional.empty();
        Optional<User> loggedInUser = Optional.empty();
        String email = getEmail(principal, providerType);

        if (existingUserId != null) {
            loggedInUser = userRepository.findById(Long.valueOf(existingUserId));
        } else {
            if (email != null && !email.isEmpty()) {
                existingEmailUser = userRepository.findOneByEmailIgnoreCase(email);
            }
        }

        if (existingOauthUser.isPresent() && loggedInUser.isPresent() && existingOauthUser.get().equals(loggedInUser.get())) {
            // Accounts already connected, do nothing

            return loggedInUser.get();
        } else if (loggedInUser.isEmpty() && existingOauthUser.isPresent()) {
            // Accounts already connected, do nothing

            return existingOauthUser.get();
        } else if (loggedInUser.isPresent() && existingOauthUser.isPresent()) {
            // Both user accounts exist, but not connected. Can't connect, as we can't delete a user

            throw new BadRequestException("Unable to connect, as account is already connected with other user.");
        } else if (loggedInUser.isPresent() || existingEmailUser.isPresent()) {
            // Connect user with selected OAuth provider

            User user = loggedInUser.isPresent() ? loggedInUser.get() : existingEmailUser.get();
            user.addExternalAuthentication(sub, providerType);

            if (providerType.equals(EKOOL)) {
                mapEkoolRoles(user, principal);
            }

            return save(user);
        } else if (existingOauthUser.isPresent()) {
            // Not in connecting flow. Just log the user in

            return existingOauthUser.get();
        }

        return createOauthUser(principal, providerType, email);
    }

    private String getEmail(OAuth2User principal, ExternalAuthenticationType externalAuthenticationType) {
        return EKOOL.equals(externalAuthenticationType) ? ekoolService.getEmail(principal.getName()) : principal.getAttribute("email");
    }

    private User createOauthUser(OAuth2User principal, ExternalAuthenticationType provider, String email) {
        User user = provider.equals(EKOOL) ? createEkoolUser(principal, email) : createGeneralUser(principal, provider);
        return save(user);
    }

    private User createGeneralUser(OAuth2User principal, ExternalAuthenticationType provider) {
        String sub = principal.getName();
        String email = principal.getAttribute("email");
        String givenName = principal.getAttribute("given_name");
        String familyName = principal.getAttribute("family_name");
        String locale = principal.getAttribute("locale") != null ? principal.getAttribute("locale") : DEFAULT_LANGUAGE;
        Boolean email_verified = principal.getAttribute("email_verified") != null ? principal.getAttribute("email_verified") : TRUE;
        User user = User.builder()
            .email(email)
            .firstName(givenName)
            .lastName(familyName)
            .langKey(formatLocale(locale))
            .activated(email_verified)
            .imageUrl(principal.getAttribute("picture"))
            .login(format("%s_%s_%s_%s", provider, givenName, familyName, sub).replaceAll("[^A-Za-z0-9_]", ""))
            .authorities(Set.of(new Authority().name(STUDENT)))
            .password(passwordEncoder.encode(random(100)))
            .firstLogin(true)
            .build();
        user.addExternalAuthentication(sub, provider);
        return user;
    }

    private User createEkoolUser(OAuth2User principal, String email) {
        User user = userRepository.findOneByExternalAuthentication(principal.getName(), EKOOL)
            .orElse(userRepository.findOneByEmailIgnoreCase(email)
                .orElse(User.builder()
                    .login(format(
                        "ekool_%s_%s_%s",
                        principal.getAttribute("firstName"),
                        principal.getAttribute("lastName"),
                        principal.getName()
                    ).replaceAll("[^A-Za-z0-9]", ""))
                    .password(passwordEncoder.encode(random(100)))
                    .build()
                    .addExternalAuthentication(principal.getName(), EKOOL)
                )
            );

        mapEkoolRoles(user, principal);

        user.setFirstName(principal.getAttribute("firstName"));
        user.setLastName(principal.getAttribute("lastName"));
        user.setEmail(email);
        user.setLangKey("et");
        user.setActivated(true);
        user.setFirstLogin(true);

        return user;
    }

    private void mapEkoolRoles(User user, OAuth2User principal) {
        List<School> schools = schoolService.saveEkoolSchools(ekoolService.getSchools(principal.getName()));
        Set<EkoolRolesDTO.EkoolRoleDTO> roles = ekoolService.getRoles(principal.getName());

        // Remove wrapping if statement for Ekool authority override in all cases
        if (!user.hasAuthorities()) {
            user.setAuthorities(roles.stream()
                .map(ekoolRole -> Role.findEkoolRole(ekoolRole.getRoleName()))
                .flatMap(Optional::stream)
                .map(role -> {
                        Authority auth = new Authority();
                        auth.setName(role.getName());
                        return auth;
                    }
                )
                .collect(toSet()));
            if (user.getAuthorities().isEmpty()) {
                user.addAuthority(new Authority().name(STUDENT));
            }
        }
        roles.stream()
            .map(ekoolRoleDTO -> {
                PersonRole personRole = ekoolRoleMapper.toEntity(ekoolRoleDTO);
                Optional<School> school = schools.stream()
                    .filter(s -> ekoolRoleDTO.getSchoolId().equals(s.getExternalId()))
                    .findFirst();
                school.ifPresent(personRole::setSchool);
                return personRole;
            })
            .filter(personRole -> personRole.getRole() != null && !user.hasPersonRole(personRole))
            .forEach(user::addPersonRole);
    }

    public User save(HarIdUser harIdUser) {
        //TODO: You can't randomly find a user by ID as two students might use their parents email.
        //TODO: SCHOOL-370 - Remove this after manual oauth2 connection flow is implemented (Users decide which accounts are joined)
        User user = userRepository.findOneByExternalAuthentication(harIdUser.getSub(), HARID)
            .orElse(userRepository.findOneByEmailIgnoreCase(harIdUser.getEmail())
                .orElse(User.builder()
                    .login(format("harid_%s_%s_%s", harIdUser.getFirstName(), harIdUser.getLastName(), harIdUser.getSub()).replaceAll("[^A-Za-z0-9]", ""))
                    .password(passwordEncoder.encode(random(100)))
                    .build()
                    .addExternalAuthentication(harIdUser.getSub(), HARID)
                )
            );

        // Remove wrapping if statement for HarId authority override in all cases
        if (!user.hasAuthorities()) {
            user.setAuthorities(harIdUser.getRoles().stream()
                .map(harIdRole -> Role.findHaridRole(harIdRole.getMarker()))
                .flatMap(Optional::stream)
                .map(role -> {
                        Authority auth = new Authority();
                        auth.setName(role.getName());
                        return auth;
                    }
                )
                .collect(toSet()));
            if (user.getAuthorities().isEmpty()) {
                user.addAuthority(new Authority().name(STUDENT));
            }
        }
        user.setPersonRoles(harIdUser.getRoles().stream()
            .map(harIdRole -> {
                PersonRole personRole = harIdRoleMapper.toEntity(harIdRole);
                Optional<School> school = schoolService.findByRegNrOrEhisIdOrName(harIdRole.getProviderRegNr(), harIdRole.getProviderEhisId(), harIdRole.getProviderName());
                school.ifPresent(personRole::setSchool);
                return personRole;
            })
            .collect(toSet()));

        user.setEmail(harIdUser.getEmail());
        user.setFirstName(harIdUser.getFirstName());
        user.setLastName(harIdUser.getLastName());
        user.setLangKey(harIdUser.getLocale());
        user.setPhoneNumber(harIdUser.getPhoneNumber());
        user.setActivated(harIdUser.isEmailVerified());
        user.setFirstLogin(true);

        return save(user);
    }

    public User getOneById(Long id) {
        return userRepository.findById(id).orElseThrow();
    }

    private String formatLocale(String locale) {
        return locale.substring(0, 2);
    }

    private void clearUserCaches(User user) {
        Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE)).evict(user.getLogin());
        if (user.getEmail() != null) {
            Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE)).evict(user.getEmail());
        }
    }

    private void validateUserAuthorities(Set<String> authorities) {
        if (Set.of(STUDENT, TEACHER).stream().noneMatch(authorities::contains)) {
            throw new ResponseStatusException(FORBIDDEN, "Specified authority is invalid");
        }
    }

    public User save(User user) {
        return userRepository.save(user);
    }
}
