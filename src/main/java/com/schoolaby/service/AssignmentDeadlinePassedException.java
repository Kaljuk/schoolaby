package com.schoolaby.service;

public class AssignmentDeadlinePassedException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public AssignmentDeadlinePassedException(String exception) {
        super(exception);
    }
}
