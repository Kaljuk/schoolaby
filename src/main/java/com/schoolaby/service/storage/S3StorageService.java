package com.schoolaby.service.storage;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import static io.github.jhipster.config.JHipsterConstants.SPRING_PROFILE_PRODUCTION;
import static java.util.Arrays.asList;

@Service
@Profile({"test", "prod"})
public class S3StorageService implements StorageService {
    private static final String BUCKET_NAME = "schoolaby";
    private static final String FOLDER = "uploads";

    private final AmazonS3 s3Client;
    private final Environment env;

    public S3StorageService(AmazonS3 s3Client, Environment env) {
        this.s3Client = s3Client;
        this.env = env;
    }

    @Override
    public void save(MultipartFile file, String uniqueFileName) throws IOException {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(file.getContentType());
        try (InputStream inputStream = file.getInputStream()) {
            save(inputStream, uniqueFileName, metadata);
        }
    }

    @Override
    public void save(File file, String uniqueFileName, String contentType) throws IOException {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(contentType);
        try (InputStream inputStream = new FileInputStream(file)) {
            save(inputStream, uniqueFileName, metadata);
        } finally {
            file.delete();
        }

    }

    private void save(InputStream inputStream, String fileName, ObjectMetadata metadata) {
        String filePath = getFilePath(fileName);
        s3Client.putObject(new PutObjectRequest(BUCKET_NAME, filePath, inputStream, metadata));
    }

    @Override
    public FileDetails getDetails(String uniqueFileName) {
        S3Object object = s3Client.getObject(new GetObjectRequest(BUCKET_NAME, getFilePath(uniqueFileName)));
        MediaType mediaType = MediaType.valueOf(object.getObjectMetadata().getContentType());
        return new FileDetails(object.getObjectContent(), mediaType);
    }

    @Override
    public void delete(String uniqueFileName) {
        s3Client.deleteObject(new DeleteObjectRequest(BUCKET_NAME, getFilePath(uniqueFileName)));
    }

    private String getFilePath(String uniqueFileName) {
        return getRootFolder() + "/" + FOLDER + "/" + uniqueFileName;
    }

    private String getRootFolder() {
        Collection<String> activeProfiles = asList(env.getActiveProfiles());
        return activeProfiles.contains(SPRING_PROFILE_PRODUCTION) ? "production" : "test";
    }
}
