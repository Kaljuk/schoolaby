package com.schoolaby.service;

import com.schoolaby.domain.User;
import com.schoolaby.service.dto.SupportEmailDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static com.schoolaby.security.SecurityUtils.getCurrentUserId;

@Service
@RequiredArgsConstructor
public class SupportEmailService {
    private final UserService userService;
    private final MailService mailService;

    public void sendSupportEmail(SupportEmailDTO supportEmailDTO) {
        User user = userService.getOneById(getCurrentUserId());
        mailService.sendSupportEmail(supportEmailDTO, user);
    }
}
