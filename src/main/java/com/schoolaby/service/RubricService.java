package com.schoolaby.service;

import com.schoolaby.domain.Assignment;
import com.schoolaby.domain.Journey;
import com.schoolaby.domain.Rubric;
import com.schoolaby.repository.RubricJpaRepository;
import com.schoolaby.service.dto.RubricDTO;
import com.schoolaby.service.mapper.RubricMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import static com.schoolaby.common.JourneyUtils.validateIsUserATeacherInJourney;

@Service
@RequiredArgsConstructor
public class RubricService {
    private final RubricJpaRepository rubricJpaRepository;
    private final RubricMapper rubricMapper;
    private final AssignmentReadService assignmentReadService;

    public Set<RubricDTO> findAllRubricTemplatesBySearch(String search) {
        return new HashSet<>(rubricMapper.toDto(rubricJpaRepository.findAllByIsTemplateTrueAndTitleContainingIgnoreCase(search)));
    }

    public RubricDTO findDTOByAssignmentId(Long assignmentId) {
        Optional<Rubric> rubricOptional = findByAssignmentId(assignmentId);
        return rubricMapper.toDto(rubricOptional.orElse(null));
    }

    public Optional<Rubric> findByAssignmentId(Long assignmentId) {
        return rubricJpaRepository.findByAssignmentId(assignmentId);
    }

    public RubricDTO saveDTO(RubricDTO rubricDTO) {
        Assignment assignment = assignmentReadService.getOne(rubricDTO.getAssignmentId(), false);
        validateIsUserATeacherInJourney(assignment.getMilestone().getJourney());

        return rubricMapper.toDto(save(rubricMapper.toEntity(rubricDTO)));
    }

    public Rubric save(Rubric rubric) {
        return rubricJpaRepository.save(rubric);
    }

    public RubricDTO updateRubric(RubricDTO rubricDTO) {
        Rubric rubric = rubricMapper.toEntity(rubricDTO);
        Rubric rubricEntity = rubricJpaRepository.findById(rubricDTO.getId()).orElseThrow(NoSuchElementException::new);
        validateIsUserATeacherInJourney(rubricEntity.getAssignment().getMilestone().getJourney());

        rubric.setAssignment(rubricEntity.getAssignment());
        return rubricMapper.toDto(rubricJpaRepository.save(rubric));
    }

    public void deleteById(Long id) {
        Rubric rubric = rubricJpaRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        Journey journey = rubric.getAssignment().getMilestone().getJourney();
        validateIsUserATeacherInJourney(journey);

        rubricJpaRepository.delete(rubric);
    }

    public void delete(Rubric rubric) {
        rubricJpaRepository.delete(rubric);
    }
}
