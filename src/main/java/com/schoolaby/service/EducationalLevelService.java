package com.schoolaby.service;

import com.schoolaby.domain.EducationalLevel;
import com.schoolaby.repository.EducationalLevelRepository;
import com.schoolaby.service.dto.EducationalLevelDTO;
import com.schoolaby.service.mapper.EducationalLevelMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.schoolaby.domain.enumeration.Country.DEFAULT_COUNTRY;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class EducationalLevelService {
    private final EducationalLevelRepository educationalLevelRepository;
    private final EducationalLevelMapper educationalLevelMapper;

    @Transactional(readOnly = true)
    public List<EducationalLevelDTO> findByCountry(String country) {
        log.debug("Request to get all EducationLevels");
        Set<EducationalLevel> educationalLevels = educationalLevelRepository.findAllByCountry(country != null ? country : DEFAULT_COUNTRY.getValue());
        return new ArrayList<>(educationalLevelMapper.toDto(educationalLevels));
    }
}
