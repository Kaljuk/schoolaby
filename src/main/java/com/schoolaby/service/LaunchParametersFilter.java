package com.schoolaby.service;

import com.schoolaby.domain.Journey;
import com.schoolaby.domain.LtiConfig;
import com.schoolaby.domain.LtiLaunch;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Builder
@Getter
public class LaunchParametersFilter {
    @NotNull
    private final LtiConfig ltiConfig;
    @NotNull
    private final String resourceLinkId;
    @NotNull
    private final Journey journey;
    @NotNull
    private final String returnUrl;
    @NotNull
    private final String launchUrl;
    @NotNull
    private final String messageType;

    private final LtiLaunch ltiLaunch;
    private final String resourceTitle;
}
