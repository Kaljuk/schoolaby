package com.schoolaby.service;

import static java.lang.String.format;

import com.schoolaby.domain.GradingScheme;
import com.schoolaby.repository.GradingSchemeRepository;
import com.schoolaby.service.dto.GradingSchemeDTO;
import com.schoolaby.service.mapper.GradingSchemeMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link GradingScheme}.
 */
@Service
@Transactional
public class GradingSchemeService {
    private final Logger log = LoggerFactory.getLogger(GradingSchemeService.class);

    private final GradingSchemeRepository gradingSchemeRepository;

    private final GradingSchemeMapper gradingSchemeMapper;

    public GradingSchemeService(GradingSchemeRepository gradingSchemeRepository, GradingSchemeMapper gradingSchemeMapper) {
        this.gradingSchemeRepository = gradingSchemeRepository;
        this.gradingSchemeMapper = gradingSchemeMapper;
    }

    /**
     * Save a gradingScheme.
     *
     * @param gradingSchemeDTO the entity to save.
     * @return the persisted entity.
     */

    public GradingSchemeDTO create(GradingSchemeDTO gradingSchemeDTO) {
        log.debug("Request to create GradingScheme : {}", gradingSchemeDTO);

        return gradingSchemeMapper.toDto(gradingSchemeRepository.save(gradingSchemeMapper.toEntity(gradingSchemeDTO)));
    }

    public GradingSchemeDTO update(GradingSchemeDTO gradingSchemeDTO) {
        log.debug("Request to update GradingScheme : {}", gradingSchemeDTO);

        if (!gradingSchemeRepository.existsById(gradingSchemeDTO.getId())) {
            throw new RuntimeException(format("GradingScheme '%s' not found!", gradingSchemeDTO.getId()));
        }

        return gradingSchemeMapper.toDto(gradingSchemeRepository.save(gradingSchemeMapper.toEntity(gradingSchemeDTO)));
    }

    /**
     * Get all the gradingSchemes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<GradingSchemeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all GradingSchemes");
        return gradingSchemeRepository.findAll(pageable).map(gradingSchemeMapper::toDto);
    }

    /**
     * Get one gradingScheme by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<GradingSchemeDTO> findOne(Long id) {
        log.debug("Request to get GradingScheme : {}", id);
        return gradingSchemeRepository.findById(id).map(gradingSchemeMapper::toDto);
    }

    /**
     * Delete the gradingScheme by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete GradingScheme : {}", id);
        gradingSchemeRepository.deleteById(id);
    }
}
