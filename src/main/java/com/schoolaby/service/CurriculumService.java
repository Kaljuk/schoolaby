package com.schoolaby.service;

import com.schoolaby.domain.Curriculum;
import com.schoolaby.repository.CurriculumJpaRepository;
import com.schoolaby.service.dto.CurriculumDTO;
import com.schoolaby.service.mapper.CurriculumMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

import static com.schoolaby.domain.enumeration.Country.*;

@Service
@Transactional
@RequiredArgsConstructor
public class CurriculumService {
    private final CurriculumJpaRepository curriculumJpaRepository;
    private final CurriculumMapper curriculumMapper;

    public Set<CurriculumDTO> findAllByCountry(String country) {
        Set<Curriculum> curricula;
        if (TANZANIA.getValue().equals(country) || UKRAINE.getValue().equals(country)) {
            curricula = curriculumJpaRepository.findAllByCountryOrCountryIsNull(country);
        } else {
            curricula = curriculumJpaRepository.findAllByCountryOrCountryIsNull(ESTONIA.getValue());
        }
        return new HashSet<>(curriculumMapper.toDto(curricula));
    }
}
