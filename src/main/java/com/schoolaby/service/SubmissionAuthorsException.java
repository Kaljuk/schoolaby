package com.schoolaby.service;

public class SubmissionAuthorsException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public SubmissionAuthorsException(String exception) {
        super(exception);
    }
}
