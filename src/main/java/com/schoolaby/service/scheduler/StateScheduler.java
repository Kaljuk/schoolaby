package com.schoolaby.service.scheduler;

import com.schoolaby.domain.Assignment;
import com.schoolaby.domain.Journey;
import com.schoolaby.domain.Milestone;
import com.schoolaby.event.AssignmentStateCalculationEvent;
import com.schoolaby.event.JourneyStateCalculationEvent;
import com.schoolaby.event.MilestoneStateCalculationEvent;
import com.schoolaby.service.AssignmentReadService;
import com.schoolaby.service.JourneyService;
import com.schoolaby.service.MilestoneService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class StateScheduler {
    private final JourneyService journeyService;
    private final MilestoneService milestoneService;
    private final AssignmentReadService assignmentReadService;
    private final ApplicationEventPublisher applicationEventPublisher;

    /**
     * This will run with a delay of 1 hour after each run
     */
    @Scheduled(fixedDelay = 60 * 60 * 1000)
    @Transactional
    public void calculateDeadlineStates() {
        List<Assignment> assignments = assignmentReadService.findAllOutOfSyncAssignments();
        assignments.forEach(assignment -> applicationEventPublisher.publishEvent(new AssignmentStateCalculationEvent(assignment)));
        Set<Long> recalculatedMilestones = assignments.stream().mapToLong(assignment -> assignment.getMilestone().getId()).boxed().collect(Collectors.toSet());
        Set<Long> recalculatedJourneys = assignments.stream().mapToLong(assignment -> assignment.getMilestone().getJourney().getId()).boxed().collect(Collectors.toSet());

        List<Milestone> milestones = milestoneService.findAllOutOfSyncMilestones(recalculatedMilestones);
        milestones.forEach(milestone -> applicationEventPublisher.publishEvent(new MilestoneStateCalculationEvent(milestone)));
        recalculatedJourneys.addAll(milestones.stream().mapToLong(milestone -> milestone.getJourney().getId()).boxed().collect(Collectors.toSet()));

        List<Journey> journeys = journeyService.findAllOutOfSyncJourneys(recalculatedJourneys);
        journeys.forEach(journey -> applicationEventPublisher.publishEvent(new JourneyStateCalculationEvent(journey)));
    }
}
