package com.schoolaby.service;

public enum Language {
    EN, ET, FI, SW, UK
}
