package com.schoolaby.service;

import com.schoolaby.domain.Journey;
import com.schoolaby.domain.Milestone;
import com.schoolaby.event.MilestoneStateCalculationEvent;
import com.schoolaby.repository.MilestoneRepository;
import com.schoolaby.service.dto.MilestoneDTO;
import com.schoolaby.service.mapper.MilestoneMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.schoolaby.common.JourneyUtils.validateIsUserATeacherInJourney;

@Service
@Transactional
@RequiredArgsConstructor
public class MilestoneService {
    private final Logger log = LoggerFactory.getLogger(MilestoneService.class);

    private final MilestoneRepository milestoneRepository;
    private final MilestoneMapper milestoneMapper;
    private final JourneyService journeyService;
    private final UserService userService;
    private final MaterialService materialService;
    private final ApplicationEventPublisher applicationEventPublisher;

    public MilestoneDTO create(MilestoneDTO milestoneDTO) {
        log.debug("Request to create Milestone : {}", milestoneDTO);

        Journey journey = journeyService.findOne(milestoneDTO.getJourneyId(), false).orElseThrow();
        validateIsUserATeacherInJourney(journey);

        Milestone milestone = milestoneMapper.toEntity(milestoneDTO);
        milestone.setCreator(userService.getUserWithAuthorities().orElseThrow());

        materialService.replaceDetachedWithManagedMaterials(milestone.getMaterials());

        journey.addMilestone(milestone);
        Milestone savedMilestone = milestoneRepository.save(milestone);
        applicationEventPublisher.publishEvent(new MilestoneStateCalculationEvent(savedMilestone));
        return milestoneMapper.toDto(savedMilestone);
    }

    public MilestoneDTO update(MilestoneDTO milestoneDTO) {
        log.debug("Request to update Milestone : {}", milestoneDTO);
        Milestone milestone = getOne(milestoneDTO.getId());
        if (!milestoneDTO.getJourneyId().equals(milestone.getJourney().getId())) {
            throw new RuntimeException("Previous journey id and received journey id do not match");
        }
        validateIsUserATeacherInJourney(milestone.getJourney());
        Milestone receivedMilestone = milestoneMapper.toEntity(milestoneDTO);
        receivedMilestone.setStates(milestone.getStates());

        materialService.replaceNonRestrictedMaterialsCreatedByOtherUsers(receivedMilestone.getMaterials());
        Milestone savedMilestone = milestoneRepository.save(receivedMilestone);
        applicationEventPublisher.publishEvent(new MilestoneStateCalculationEvent(savedMilestone));

        return milestoneMapper.toDto(savedMilestone);
    }

    @Transactional(readOnly = true)
    public List<MilestoneDTO> findAll(Long journeyId, boolean template) {
        log.debug("Request to get all Milestones");
        return milestoneRepository.findAll(journeyId, template).stream()
            .map(milestoneMapper::toDto)
            .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public Optional<MilestoneDTO> findOne(Long id, boolean template) {
        log.debug("Request to get Milestone : {}", id);
        return milestoneRepository.findOne(id, template).map(milestoneMapper::toDto);
    }

    Milestone getOne(Long id) {
        return milestoneRepository.findOne(id, false).orElseThrow(EntityNotFoundException::new);
    }

    public void delete(Long id) {
        log.debug("Request to delete Milestone : {}", id);
        Milestone milestone = getOne(id);
        Journey journey = milestone.getJourney();
        validateIsUserATeacherInJourney(journey);

        milestoneRepository.deleteById(id);
        applicationEventPublisher.publishEvent(new MilestoneStateCalculationEvent(milestone));
        journey.removeMilestone(milestone);
    }

    @Transactional(readOnly = true)
    public List<Milestone> findAllOutOfSyncMilestones(Set<Long> excludedIds) {
        return milestoneRepository.findAllOutOfSyncMilestones(excludedIds);
    }
}
