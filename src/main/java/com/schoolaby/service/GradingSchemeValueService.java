package com.schoolaby.service;

import static java.lang.String.format;

import com.schoolaby.domain.GradingSchemeValue;
import com.schoolaby.repository.GradingSchemeValueRepository;
import com.schoolaby.service.dto.GradingSchemeValueDTO;
import com.schoolaby.service.mapper.GradingSchemeValueMapper;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link GradingSchemeValue}.
 */
@Service
@Transactional
public class GradingSchemeValueService {
    private final Logger log = LoggerFactory.getLogger(GradingSchemeValueService.class);

    private final GradingSchemeValueRepository gradingSchemeValueRepository;

    private final GradingSchemeValueMapper gradingSchemeValueMapper;

    public GradingSchemeValueService(
        GradingSchemeValueRepository gradingSchemeValueRepository,
        GradingSchemeValueMapper gradingSchemeValueMapper
    ) {
        this.gradingSchemeValueRepository = gradingSchemeValueRepository;
        this.gradingSchemeValueMapper = gradingSchemeValueMapper;
    }

    /**
     * Save a gradingSchemeValue.
     *
     * @param gradingSchemeValueDTO the entity to save.
     * @return the persisted entity.
     */

    public GradingSchemeValueDTO create(GradingSchemeValueDTO gradingSchemeValueDTO) {
        log.debug("Request to create GradingSchemeValue : {}", gradingSchemeValueDTO);

        GradingSchemeValue gradingSchemeValue = gradingSchemeValueMapper.toEntity(gradingSchemeValueDTO);
        gradingSchemeValue = gradingSchemeValueRepository.save(gradingSchemeValue);
        return gradingSchemeValueMapper.toDto(gradingSchemeValue);
    }

    public GradingSchemeValueDTO update(GradingSchemeValueDTO gradingSchemeValueDTO) {
        log.debug("Request to update GradingSchemeValue : {}", gradingSchemeValueDTO);

        if (!gradingSchemeValueRepository.existsById(gradingSchemeValueDTO.getId())) {
            throw new RuntimeException(format("GradingSchemeValue '%s' not found!", gradingSchemeValueDTO.getId()));
        }

        GradingSchemeValue gradingSchemeValue = gradingSchemeValueMapper.toEntity(gradingSchemeValueDTO);
        gradingSchemeValue = gradingSchemeValueRepository.save(gradingSchemeValue);
        return gradingSchemeValueMapper.toDto(gradingSchemeValue);
    }

    /**
     * Get all the gradingSchemeValues.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<GradingSchemeValueDTO> findAll(Pageable pageable) {
        log.debug("Request to get all GradingSchemeValues");
        return gradingSchemeValueRepository.findAll(pageable).map(gradingSchemeValueMapper::toDto);
    }

    /**
     * Get one gradingSchemeValue by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<GradingSchemeValueDTO> findOne(Long id) {
        log.debug("Request to get GradingSchemeValue : {}", id);
        return gradingSchemeValueRepository.findById(id).map(gradingSchemeValueMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Set<GradingSchemeValueDTO> findAllByGradingSchemeId(Long gradingSchemeId) {
        log.debug("Request to get GradingSchemeValues by GradingScheme id : {}", gradingSchemeId);
        return gradingSchemeValueRepository
            .findAllByGradingSchemeId(gradingSchemeId)
            .stream()
            .map(gradingSchemeValueMapper::toDto)
            .collect(Collectors.toUnmodifiableSet());
    }

    /**
     * Delete the gradingSchemeValue by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete GradingSchemeValue : {}", id);
        gradingSchemeValueRepository.deleteById(id);
    }
}
