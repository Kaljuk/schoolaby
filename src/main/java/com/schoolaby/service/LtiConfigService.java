package com.schoolaby.service;

import com.schoolaby.domain.LtiConfig;
import com.schoolaby.repository.LtiConfigRepository;
import com.schoolaby.repository.filter.LtiConfigFilter;
import com.schoolaby.security.SecurityUtils;
import com.schoolaby.service.dto.LtiConfigDTO;
import com.schoolaby.service.mapper.LtiConfigMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static com.schoolaby.domain.enumeration.LtiConfigType.JOURNEY;
import static com.schoolaby.domain.enumeration.LtiConfigType.PLATFORM;
import static com.schoolaby.security.Role.Constants.ADMIN;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static java.util.stream.Collectors.toList;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class LtiConfigService {
    private final LtiConfigRepository configRepository;
    private final LtiConfigMapper configMapper;

    public LtiConfigDTO create(LtiConfigDTO ltiConfigDTO) {
        log.debug("Request to create LtiConfig : {}", ltiConfigDTO);

        LtiConfig ltiConfig = configRepository.save(configMapper.toEntity(ltiConfigDTO).setType(JOURNEY));
        return configMapper.toDto(ltiConfig);
    }

    public LtiConfigDTO update(LtiConfigDTO ltiConfigDTO) {
        log.debug("Request to update LtiConfig : {}", ltiConfigDTO);

        LtiConfig configInDB = configRepository.findById(ltiConfigDTO.getId()).orElseThrow();
        configInDB.setConsumerKey(ltiConfigDTO.getConsumerKey());
        configInDB.setSharedSecret(ltiConfigDTO.getSharedSecret());
        configInDB.setDeepLinkingUrl(ltiConfigDTO.getDeepLinkingUrl());
        configInDB.setLoginInitiationUrl(ltiConfigDTO.getLoginInitiationUrl());
        configInDB.setRedirectHost(ltiConfigDTO.getRedirectHost());
        configInDB.setJwksUrl(ltiConfigDTO.getJwksUrl());
        configInDB.setLaunchUrl(ltiConfigDTO.getLaunchUrl());
        LtiConfig config = configRepository.save(configInDB);
        return configMapper.toDto(config);
    }

    public List<LtiConfigDTO> findAll(Long journeyId) {
        if (SecurityUtils.isCurrentUserInRole(ADMIN)) {
            return configRepository.findAllByJourneyId(journeyId).stream().map(configMapper::toDto).collect(toList());
        } else if (SecurityUtils.isCurrentUserInRole(TEACHER)) {
            return configRepository
                .findAllByJourneyIdAndJourneyTeacherId(journeyId, SecurityUtils.getCurrentUserId())
                .stream()
                .map(configMapper::toDto)
                .collect(toList());
        }

        return List.of();
    }

    public LtiConfig getOne(LtiConfigFilter ltiConfigFilter) {
        return configRepository.findOne(ltiConfigFilter).orElseThrow();
    }

    public void deleteLtiConfig(Long id) {
        LtiConfig ltiConfig = configRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        if (PLATFORM.equals(ltiConfig.getType())) {
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                "Lti configuration with type:PLATFORM cannot be deleted"
            );
        }
        ltiConfig.getJourney().removeLtiConfig(ltiConfig);
        configRepository.deleteById(id);
    }
}
