package com.schoolaby.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.*;

@Getter
@Setter
public class RubricDTO implements Serializable {
    private Long id;
    private String title;
    private Boolean isTemplate;
    private Long minPoints;
    private Long maxPoints;
    private Long assignmentId;
    private String createdBy;

    private Set<CriterionDTO> criterions = new LinkedHashSet<>();
}
