package com.schoolaby.service.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.Instant;

@Data
public class JourneyTeacherDTO implements Serializable {
    private UserDTO user;
    private Instant joinedDate;
}
