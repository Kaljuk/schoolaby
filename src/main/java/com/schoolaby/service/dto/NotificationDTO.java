package com.schoolaby.service.dto;

import com.schoolaby.domain.NotificationType;
import com.schoolaby.domain.Subject;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;


public class NotificationDTO implements Serializable {

    private Long id;

    @NotNull
    private String message;

    @NotNull
    private String link;

    private Instant read;

    private String journeyName;

    private Long journeyId;

    private String assignmentName;

    private String groupName;

    private String creatorName;

    private Subject subject;

    private Set<String> educationalAlignmentTargetNames = new HashSet<>();

    private Instant createdDate;

    private NotificationType type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getJourneyId() { return this.journeyId; }

    public void setJourneyId(Long id) { this.journeyId = id; }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getRead() {
        return read;
    }

    public void setRead(Instant read) {
        this.read = read;
    }

    public String getJourneyName() {
        return journeyName;
    }

    public void setJourneyName(String journeyName) {
        this.journeyName = journeyName;
    }

    public String getAssignmentName() {
        return assignmentName;
    }

    public void setAssignmentName(String assignmentName) {
        this.assignmentName = assignmentName;
    }

    public Set<String> getEducationalAlignmentTargetNames() {
        return educationalAlignmentTargetNames;
    }

    public void setEducationalAlignmentTargetNames(Set<String> educationalAlignmentTargetNames) {
        this.educationalAlignmentTargetNames = educationalAlignmentTargetNames;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NotificationDTO)) {
            return false;
        }

        return id != null && id.equals(((NotificationDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "NotificationDTO{" +
            "id=" + id +
            ", message='" + message + '\'' +
            ", link='" + link + '\'' +
            ", createdDate=" + createdDate +
            '}';
    }
}
