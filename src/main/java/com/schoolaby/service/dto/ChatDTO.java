package com.schoolaby.service.dto;

import java.io.Serializable;
import java.util.List;

public class ChatDTO implements Serializable {
    private Long id;

    private String title;

    private Long submissionId;

    private Long journeyId;

    private List<UserDTO> people;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<UserDTO> getPeople() {
        return people;
    }

    public void setPeople(List<UserDTO> people) {
        this.people = people;
    }

    public Long getSubmissionId() {
        return submissionId;
    }

    public void setSubmissionId(Long submissionId) {
        this.submissionId = submissionId;
    }

    public Long getJourneyId() {
        return journeyId;
    }

    public void setJourneyId(Long journeyId) {
        this.journeyId = journeyId;
    }
}
