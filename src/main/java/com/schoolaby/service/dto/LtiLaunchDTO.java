package com.schoolaby.service.dto;

import com.schoolaby.service.dto.lti.LtiResourceDTO;

import java.time.Instant;

public class LtiLaunchDTO {
    private Long id;
    private String result;
    private LtiResourceDTO ltiResource;
    private Instant createdDate;

    public Long getId() {
        return id;
    }

    public LtiLaunchDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getResult() {
        return result;
    }

    public LtiLaunchDTO setResult(String result) {
        this.result = result;
        return this;
    }

    public LtiResourceDTO getLtiResource() {
        return ltiResource;
    }

    public void setLtiResource(LtiResourceDTO ltiResource) {
        this.ltiResource = ltiResource;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LtiLaunchDTO)) {
            return false;
        }
        return id != null && id.equals(((LtiLaunchDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
