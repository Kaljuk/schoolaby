package com.schoolaby.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class SelectedCriterionLevelDTO implements Serializable {

    private Long id;

    @NotNull
    private String modifiedDescription;

    @NotNull
    private CriterionLevelDTO criterionLevel;
}
