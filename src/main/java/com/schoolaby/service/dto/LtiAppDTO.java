package com.schoolaby.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class LtiAppDTO implements Serializable {
    private Long id;
    private String name;
    private String description;
    private String imageUrl;
    private String version;
    private String launchUrl;
    private String clientId;
    private String deepLinkingUrl;
    private String loginInitiationUrl;
    private String redirectHost;
    private String jwksUrl;

}
