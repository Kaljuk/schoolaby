package com.schoolaby.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class GroupAssignmentDTO {
    private Long id;
    private String title;
    private Instant createdDate;
    @JsonIgnoreProperties(value = {"hasSubmission", "id"})
    private Set<GroupDTO> groups = new HashSet<>();
}
