package com.schoolaby.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class MaterialDTO implements Serializable {
    private Long id;

    @NotNull
    private String title;

    private String type;
    private String description;
    private String externalId;

    @NotNull
    private String url;
    private String imageUrl;
    private String imageName;

    private UploadedFileDTO uploadedFile;
    private Set<EducationalAlignmentDTO> educationalAlignments = new HashSet<>();

    private Boolean restricted;

    private String createdBy;

    private Integer sequenceNumber;

    private String country;

    public MaterialDTO restricted(Boolean restricted) {
        setRestricted(restricted);
        return this;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public MaterialDTO id(Long id) {
        setId(id);
        return this;
    }

    public MaterialDTO title(String title) {
        setTitle(title);
        return this;
    }

    public MaterialDTO type(String type) {
        setType(type);
        return this;
    }

    public MaterialDTO description(String description) {
        setDescription(description);
        return this;
    }

    public MaterialDTO externalId(String externalId) {
        setExternalId(externalId);
        return this;
    }

    public MaterialDTO url(String url) {
        setUrl(url);
        return this;
    }

    public MaterialDTO uploadedFile(UploadedFileDTO uploadedFile) {
        setUploadedFile(uploadedFile);
        return this;
    }

    public MaterialDTO educationalAlignments(Set<EducationalAlignmentDTO> educationalAlignments) {
        setEducationalAlignments(educationalAlignments);
        return this;
    }

    public MaterialDTO createdBy(String createdBy) {
        setCreatedBy(createdBy);
        return this;
    }

    public MaterialDTO imageUrl(String imageUrl) {
        setImageUrl(imageUrl);
        return this;
    }

    public MaterialDTO imageName(String imageName) {
        setImageName(imageName);
        return this;
    }

    public MaterialDTO country(String country) {
        setCountry(country);
        return this;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MaterialDTO)) {
            return false;
        }

        return id != null && id.equals(((MaterialDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MaterialDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", type='" + getType() + "'" +
            ", description='" + getDescription() + "'" +
            ", externalId='" + getExternalId() + "'" +
            ", url='" + getUrl() + "'" +
            ", uploadedFile='" + getUploadedFile() + "'" +
            ", educationalAlignments='" + getEducationalAlignments() + "'" +
            "}";
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }
}
