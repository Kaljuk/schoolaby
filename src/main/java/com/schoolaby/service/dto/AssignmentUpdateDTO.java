package com.schoolaby.service.dto;

import com.schoolaby.service.dto.lti.LtiResourceDTO;
import com.schoolaby.service.dto.states.EntityState;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.Instant;
import java.util.Set;

@Getter
@Setter
public class AssignmentUpdateDTO extends AssignmentDTO implements Serializable {
    private RubricDTO rubric;

    @Override
    public AssignmentUpdateDTO setId(Long id) {
        super.setId(id);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setGradeGroupMembersIndividually(Boolean gradeGroupMembersIndividually) {
        super.setGradeGroupMembersIndividually(gradeGroupMembersIndividually);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setTitle(String title) {
        super.setTitle(title);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setDescription(String description) {
        super.setDescription(description);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setDeadline(Instant deadline) {
        super.setDeadline(deadline);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setFlexibleDeadline(boolean flexibleDeadline) {
        super.setFlexibleDeadline(flexibleDeadline);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setState(EntityState state) {
        super.setState(state);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setHasGrades(boolean hasGrades) {
        super.setHasGrades(hasGrades);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setMaterials(Set<MaterialDTO> materials) {
        super.setMaterials(materials);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setStudents(Set<UserDTO> students) {
        super.setStudents(students);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setEducationalAlignments(Set<EducationalAlignmentDTO> educationalAlignments) {
        super.setEducationalAlignments(educationalAlignments);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setLtiResources(Set<LtiResourceDTO> ltiResources) {
        super.setLtiResources(ltiResources);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setGroups(Set<GroupDTO> groups) {
        super.setGroups(groups);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setGradingSchemeId(Long gradingSchemeId) {
        super.setGradingSchemeId(gradingSchemeId);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setMilestoneId(Long milestoneId) {
        super.setMilestoneId(milestoneId);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setJourneyId(Long journeyId) {
        super.setJourneyId(journeyId);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setCreatorId(Long creatorId) {
        super.setCreatorId(creatorId);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setTeacherName(String teacherName) {
        super.setTeacherName(teacherName);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setPublished(Instant published) {
        super.setPublished(published);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setHighestGrade(boolean isHighestGrade) {
        super.setHighestGrade(isHighestGrade);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setSubmissionsCount(Integer submissionsCount) {
        super.setSubmissionsCount(submissionsCount);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setSubject(SubjectDTO subject) {
        super.setSubject(subject);
        return this;
    }

    @Override
    public AssignmentUpdateDTO setRequiresSubmission(Boolean requiresSubmission) {
        super.setRequiresSubmission(requiresSubmission);
        return this;
    }
}
