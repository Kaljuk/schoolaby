package com.schoolaby.service.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
public class GradingSchemeValueDTO implements Serializable {
    private Long id;

    @NotNull
    private String grade;

    @NotNull
    @Range(max = 100)
    private Integer percentageRangeStart;

    @Range(max = 100)
    private Integer percentageRangeEnd;

    private Long gradingSchemeId;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GradingSchemeValueDTO)) {
            return false;
        }

        return id != null && id.equals(((GradingSchemeValueDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GradingSchemeValueDTO{" +
            "id=" + getId() +
            ", grade='" + getGrade() + "'" +
            ", percentageRangeStart='" + getPercentageRangeStart() + "'" +
            ", percentageRangeEnd='" + getPercentageRangeEnd() + "'" +
            ", gradingSchemeId=" + getGradingSchemeId() +
            "}";
    }
}
