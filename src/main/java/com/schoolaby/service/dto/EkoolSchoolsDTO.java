package com.schoolaby.service.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class EkoolSchoolsDTO implements Serializable {
    @JsonProperty("data")
    private Set<EkoolSchoolDTO> data = new HashSet<>();

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class EkoolSchoolDTO implements Serializable {
        @JsonProperty("id")
        private Long id;
        @JsonProperty("reg_nr")
        private String regNr;
        @JsonProperty("ehis_id")
        private String ehisId;
        @JsonProperty("name")
        private String name;
    }
}



