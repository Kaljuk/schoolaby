package com.schoolaby.service.dto;

import com.schoolaby.domain.enumeration.LtiConfigType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LtiConfigDTO {
    private Long id;
    private String consumerKey;
    private String sharedSecret;
    private String deploymentId;
    @NotNull
    private LtiAppDTO ltiApp;
    private Long journeyId;
    private LtiConfigType type;

    private String launchUrl;
    private String deepLinkingUrl;
    private String loginInitiationUrl;
    private String redirectHost;
    private String jwksUrl;
}
