package com.schoolaby.service.dto.lti.claims;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@JsonSerialize
public class DeepLinkingSettings {
    @JsonProperty("deep_link_return_url")
    private String returnUrl;
    @JsonProperty("accept_types")
    private final List<String> acceptTypes = new ArrayList<>();
    @JsonProperty("accept_presentation_document_targets")
    private final List<String> acceptPresentationDocumentTargets = new ArrayList<>();
    @JsonProperty("accept_multiple")
    private Boolean acceptMultiple;

    public DeepLinkingSettings returnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
        return this;
    }

    public DeepLinkingSettings acceptTypes(String... acceptTypes) {
        this.acceptTypes.clear();
        Collections.addAll(this.acceptTypes, acceptTypes);
        return this;
    }

    public DeepLinkingSettings acceptPresentationDocumentTargets(String... acceptPresentationDocumentTargets) {
        this.acceptPresentationDocumentTargets.clear();
        Collections.addAll(this.acceptPresentationDocumentTargets, acceptPresentationDocumentTargets);
        return this;
    }

    public DeepLinkingSettings acceptMultiple(Boolean acceptMultiple) {
        this.acceptMultiple = acceptMultiple;
        return this;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public List<String> getAcceptTypes() {
        return acceptTypes;
    }

    public List<String> getAcceptPresentationDocumentTargets() {
        return acceptPresentationDocumentTargets;
    }

    public Boolean getAcceptMultiple() {
        return acceptMultiple;
    }
}
