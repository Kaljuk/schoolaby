package com.schoolaby.service.dto.lti;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class LtiLineItemDTO implements Serializable {
    private String id;
    private Long scoreMaximum;
    private String label;
    private String tag;
}
