package com.schoolaby.service.dto.lti.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@JsonSerialize
@Getter
@Setter
@RequiredArgsConstructor
public class AccessTokenResponse {
    @JsonProperty("access_token")
    private final String accessToken;
    @JsonProperty("token_type")
    private final String type = "bearer";
    @JsonProperty("expires_in")
    private final Long expiresInSeconds = 3600L;
    @JsonProperty("scope")
    private final String scope;
}
