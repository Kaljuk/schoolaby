package com.schoolaby.service.dto.lti;

import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.Set;

@Getter
@Setter
public class LtiScoreDTO {
    private String title;
    private Long resourceId;
    private Long userId;
    private String score;
    private String scoreMax;
    private Instant createdDate;
    private Set<Instant> launches;
}
