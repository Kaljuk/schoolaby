package com.schoolaby.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
public class SchoolDTO implements Serializable {
    private Long id;
    private String regNr;
    private String ehisId;
    private String name;
    private UploadedFileDTO uploadedFile;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SchoolDTO schoolDTO = (SchoolDTO) o;
        return getId().equals(schoolDTO.getId()) &&
            Objects.equals(getRegNr(), schoolDTO.getRegNr()) &&
            Objects.equals(getEhisId(), schoolDTO.getEhisId()) &&
            getName().equals(schoolDTO.getName());
    }

    @Override
    public String toString() {
        return "SchoolDTO{" +
            "id=" + id +
            ", regNr='" + regNr + '\'' +
            ", ehisId='" + ehisId + '\'' +
            ", name='" + name + '\'' +
            '}';
    }
}
