package com.schoolaby.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Setter
public class CriterionDTO implements Serializable {
    @NotNull
    private Long id;
    @NotNull
    private String title;
    @NotNull
    private String description;
    @NotNull
    private Long sequenceNumber;
    private Long minPoints;
    private Long maxPoints;

    private Set<CriterionLevelDTO> levels  = new LinkedHashSet<>();
}


