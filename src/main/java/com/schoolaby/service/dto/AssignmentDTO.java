package com.schoolaby.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.schoolaby.service.dto.lti.LtiResourceDTO;
import com.schoolaby.service.dto.states.EntityState;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import static java.time.Instant.now;

@Getter
@Setter
public class AssignmentDTO implements Serializable {
    private Long id;
    @NotNull
    private String title;
    private String description;
    private Instant deadline;
    private boolean flexibleDeadline;
    private EntityState state;
    private boolean hasGrades;
    private Set<MaterialDTO> materials = new HashSet<>();
    private Set<UserDTO> students = new HashSet<>();
    private Set<EducationalAlignmentDTO> educationalAlignments = new HashSet<>();
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Set<LtiResourceDTO> ltiResources = new HashSet<>();
    private Set<GroupDTO> groups = new HashSet<>();
    private Long gradingSchemeId;
    private Long milestoneId;
    private Long journeyId;
    private Long creatorId;
    private String teacherName;
    @NotNull
    private boolean isHighestGrade;
    private Integer submissionsCount;
    private SubjectDTO subject;
    private Boolean requiresSubmission;
    private Boolean gradeGroupMembersIndividually = false;
    private Instant published;
    private boolean hidden;
    private Long previousAssignmentId;
    private Long nextAssignmentId;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssignmentDTO)) {
            return false;
        }

        return id != null && id.equals(((AssignmentDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AssignmentDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", deadline='" + getDeadline() + "'" +
            ", materials='" + getMaterials() + "'" +
            ", students='" + getStudents() + "'" +
            ", educationalAlignments='" + getEducationalAlignments() + "'" +
            ", gradingSchemeId=" + getGradingSchemeId() +
            ", milestoneId=" + getMilestoneId() +
            ", creatorId=" + getCreatorId() +
            ", submissionsCount=" + getSubmissionsCount() +
            "}";
    }
}
