package com.schoolaby.service.dto;

import com.schoolaby.service.dto.states.JoinJourneyResult;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class JoinJourneyResultDTO implements Serializable {
    private Long journeyId;
    private JoinJourneyResult status;
}
