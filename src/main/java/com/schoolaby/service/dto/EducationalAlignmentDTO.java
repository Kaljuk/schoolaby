package com.schoolaby.service.dto;

import com.schoolaby.domain.Subject;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

public class EducationalAlignmentDTO implements Serializable {
    private Long id;

    @NotNull
    private String title;

    @NotNull
    private String alignmentType;

    @NotNull
    private String country;

    @NotNull
    private String targetName;

    private String targetUrl;
    private Instant startDate;
    private Instant endDate;
    private Long taxonId;
    private Set<EducationalAlignmentDTO> children = new HashSet<>();
    private Subject subject;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlignmentType() {
        return alignmentType;
    }

    public void setAlignmentType(String alignmentType) {
        this.alignmentType = alignmentType;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTargetName() {
        return targetName;
    }

    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Set<EducationalAlignmentDTO> getChildren() {
        return children;
    }

    public void setChildren(Set<EducationalAlignmentDTO> educationalAlignments) {
        this.children = educationalAlignments;
    }

    public Long getTaxonId() {
        return taxonId;
    }

    public void setTaxonId(Long taxonId) {
        this.taxonId = taxonId;
    }

    public EducationalAlignmentDTO id(Long id) {
        setId(id);
        return this;
    }

    public EducationalAlignmentDTO title(String title) {
        setTitle(title);
        return this;
    }

    public EducationalAlignmentDTO alignmentType(String alignmentType) {
        setAlignmentType(alignmentType);
        return this;
    }

    public EducationalAlignmentDTO country(String country) {
        setCountry(country);
        return this;
    }

    public EducationalAlignmentDTO targetName(String targetName) {
        setTargetName(targetName);
        return this;
    }

    public EducationalAlignmentDTO targetUrl(String targetUrl) {
        setTargetUrl(targetUrl);
        return this;
    }

    public EducationalAlignmentDTO startDate(Instant startDate) {
        setStartDate(startDate);
        return this;
    }

    public EducationalAlignmentDTO endDate(Instant endDate) {
        setEndDate(endDate);
        return this;
    }

    public EducationalAlignmentDTO taxonId(Long taxonId) {
        setTaxonId(taxonId);
        return this;
    }

    public EducationalAlignmentDTO children(Set<EducationalAlignmentDTO> children) {
        setChildren(children);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EducationalAlignmentDTO)) {
            return false;
        }

        return id != null && id.equals(((EducationalAlignmentDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EducationalAlignmentDTO{" +
               "id=" + getId() +
               ", title='" + getTitle() + "'" +
               ", alignmentType='" + getAlignmentType() + "'" +
               ", educationalFramework='" + getCountry() + "'" +
               ", targetName='" + getTargetName() + "'" +
               ", targetUrl='" + getTargetUrl() + "'" +
               ", startDate='" + getStartDate() + "'" +
               ", endDate='" + getEndDate() + "'" +
               ", children='" + getChildren() + "'" +
               "}";
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }
}
