package com.schoolaby.service.dto.submission;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, property = "className")
public interface SubmissionPatchDTO {

}
