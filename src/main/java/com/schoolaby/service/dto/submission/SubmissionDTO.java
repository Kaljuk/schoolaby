package com.schoolaby.service.dto.submission;

import com.schoolaby.service.dto.UploadedFileDTO;
import com.schoolaby.service.dto.UserDTO;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Setter
public class SubmissionDTO implements Serializable {
    private Long id;

    @NotNull
    private String value = "";

    private boolean resubmittable;

    private Instant submittedForGradingDate;

    private Set<UploadedFileDTO> uploadedFiles = new HashSet<>();

    private Set<SubmissionFeedbackDTO> submissionFeedbacks = new LinkedHashSet<>();

    private Set<UserDTO> authors = new HashSet<>();

    @NotNull
    private Long assignmentId;

    private String assignmentTitle;

    private boolean isHighestGrade;

    private Long groupId;

    private String groupName;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SubmissionDTO)) {
            return false;
        }

        return id != null && id.equals(((SubmissionDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore

    @Override
    public String toString() {
        return "SubmissionDTO{" +
            "id=" + id +
            ", value='" + value + '\'' +
            ", resubmittable=" + resubmittable +
            ", submittedForGradingDate=" + submittedForGradingDate +
            ", authors=" + authors +
            ", assignmentId=" + assignmentId +
            '}';
    }
}
