package com.schoolaby.service;

import com.schoolaby.domain.PersonRole;
import com.schoolaby.repository.PersonRoleRepository;
import com.schoolaby.service.dto.PersonRoleDTO;
import com.schoolaby.service.mapper.PersonRoleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class PersonRoleService {
    private final Logger log = LoggerFactory.getLogger(PersonRoleService.class);

    private final PersonRoleRepository personRoleRepository;
    private final PersonRoleMapper personRoleMapper;

    public PersonRoleService(PersonRoleRepository personRoleRepository, PersonRoleMapper personRoleMapper) {
        this.personRoleRepository = personRoleRepository;
        this.personRoleMapper = personRoleMapper;
    }

    public PersonRoleDTO save(PersonRoleDTO personRoleDTO) {
        log.debug("Request to save PersonRole : {}", personRoleDTO);
        PersonRole personRole = personRoleMapper.toEntity(personRoleDTO);
        personRole = personRoleRepository.save(personRole);
        return personRoleMapper.toDto(personRole);
    }

    /**
     * Get all the personRoles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PersonRoleDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PersonRoles");
        return personRoleRepository.findAll(pageable).map(personRoleMapper::toDto);
    }

    /**
     * Get one personRole by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PersonRoleDTO> findOne(Long id) {
        log.debug("Request to get PersonRole : {}", id);
        return personRoleRepository.findById(id).map(personRoleMapper::toDto);
    }

    /**
     * Delete the personRole by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PersonRole : {}", id);
        personRoleRepository.deleteById(id);
    }
}
