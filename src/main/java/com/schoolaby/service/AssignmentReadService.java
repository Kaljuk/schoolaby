package com.schoolaby.service;

import com.schoolaby.domain.Assignment;
import com.schoolaby.domain.Journey;
import com.schoolaby.repository.AssignmentRepository;
import com.schoolaby.service.dto.AssignmentDTO;
import com.schoolaby.service.dto.GroupAssignmentDTO;
import com.schoolaby.service.dto.states.EntityState;
import com.schoolaby.service.mapper.AssignmentMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.time.ZoneId.systemDefault;
import static java.util.stream.Collectors.toList;
import static org.slf4j.LoggerFactory.getLogger;

@Service
@Transactional
@RequiredArgsConstructor
public class AssignmentReadService {
    private final Logger log = getLogger(AssignmentReadService.class);

    private final AssignmentRepository assignmentRepository;
    private final AssignmentMapper assignmentMapper;

    @Transactional(readOnly = true)
    public Page<AssignmentDTO> findAll(LocalDate fromDate, LocalDate toDate, List<EntityState> states, Long journeyId, Pageable pageable) {
        log.debug("Request to get all Assignments");
        Instant from = fromDate.atStartOfDay(systemDefault()).toInstant();
        Instant to = toDate.atStartOfDay(systemDefault()).plusDays(1).toInstant();
        Page<Assignment> assignments;

        if (journeyId != null) {
            assignments = assignmentRepository.findAllInJourneyByDeadlineBetweenUnpaged(journeyId, from, to);
        } else {
            assignments = assignmentRepository.findAllByDeadlineBetweenPaginated(from, to, pageable);
        }

        Page<AssignmentDTO> filteredByState = filterByState(states, pageable, assignments);

        // Limit page size to requested page size, needed bc PageImpl does not take into account given pageable
        // sort by date - as findAllInJourneyByDeadlineBetweenUnpaged is unpaged the result is not sorted
        // TODO: SCHOOL-845 remove this and filter assignment by state when requesting from repo
        if (filteredByState.getContent().size() != pageable.getPageSize() && journeyId != null) {
            List<AssignmentDTO> limitedAssignments = filteredByState.getContent().stream().sorted((a1, a2) -> {
                Instant deadline1 = a1.getDeadline();
                Instant deadline2 = a2.getDeadline();
                return deadline1.compareTo(deadline2);
            }).limit(pageable.getPageSize()).collect(toList());
            return new PageImpl<>(limitedAssignments, pageable, filteredByState.getTotalElements());
        }

        return filteredByState;
    }

    private Page<AssignmentDTO> filterByState(List<EntityState> states, Pageable pageable, Page<Assignment> assignmentPage) {
        List<AssignmentDTO> assignments = assignmentPage
            .stream()
            .filter(assignment -> states.contains(assignment.getState()))
            .map(assignmentMapper::toDtoWithoutMaterials)
            .collect(toList());

        return new PageImpl<>(assignments, pageable, assignments.size());
    }

    @Transactional(readOnly = true)
    public AssignmentDTO getOneDTO(Long id, boolean template) {
        log.debug("Request to get Assignment : {}", id);
        return assignmentMapper.toDto(getOne(id, template));
    }

    @Transactional(readOnly = true)
    public AssignmentDTO getOneWithPreviousIdAndNextIdDTO(Long id, boolean template) {
        log.debug("Request to get Assignment : {} with previous and next assignments id's", id);
        Assignment assignment = getOne(id, template);
        Journey journey = assignment.getMilestone().getJourney();
        Set<Assignment> journeyAssignments = journey.getAssignments();
        List<Assignment> sortedAssignments = journeyAssignments.stream().sorted((assignmentA, assignmentB) -> {
            if (assignmentA.getDeadline().isBefore(assignmentB.getDeadline())) {
                return -1;
            } else if (assignmentA.getDeadline().isAfter(assignmentB.getDeadline())) {
                return 1;
            }
            return assignmentA.getId().compareTo(assignmentB.getId());
        }).collect(Collectors.toList());

        int currentAssignmentIndex = sortedAssignments.indexOf(assignment);
        int previousAssignmentIndex = currentAssignmentIndex > 0 ? currentAssignmentIndex - 1 : -1;
        int nextAssignmentIndex = currentAssignmentIndex < journeyAssignments.size() - 1 ? currentAssignmentIndex + 1 : -1;

        AssignmentDTO assignmentDTO = assignmentMapper.toDto(assignment);

        if (previousAssignmentIndex != -1) {
            assignmentDTO.setPreviousAssignmentId(sortedAssignments.get(previousAssignmentIndex).getId());
        }
        if (nextAssignmentIndex != -1) {
            assignmentDTO.setNextAssignmentId(sortedAssignments.get(nextAssignmentIndex).getId());
        }
        return assignmentDTO;
    }

    @Transactional(readOnly = true)
    public Assignment getOne(Long id, boolean template) {
        return assignmentRepository.findOne(id, template).orElseThrow(EntityNotFoundException::new);
    }

    @Transactional(readOnly = true)
    public Set<Assignment> getAssignmentsByIds(Set<Long> assignmentIds) {
        return new HashSet<>(assignmentRepository.findAssignmentsByIds(assignmentIds));
    }

    public List<GroupAssignmentDTO> getGroupAssignmentsBySearch(Long journeyId, String search) {
        return new ArrayList<>(assignmentMapper.toGroupAssignmentDto(assignmentRepository.findBySearch(journeyId, search)));
    }

    public void removeStudentFromAssignment(Assignment assignment, Long studentId) {
        assignment.removeStudent(studentId);
        assignment.removeStudentFromGroups(studentId);
    }

    public List<Assignment> findAllOutOfSyncAssignments() {
        return assignmentRepository.findAllOutOfSyncAssignments();
    }
}
