package com.schoolaby.service;

import com.schoolaby.repository.JourneyTeacherJpaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class JourneyTeacherService {
    private final JourneyTeacherJpaRepository journeyTeacherJpaRepository;

    public void delete(Long journeyId, Long teacherId) {
        journeyTeacherJpaRepository.deleteByJourneyIdAndUserId(journeyId, teacherId);
    }
}
