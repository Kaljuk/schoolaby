package com.schoolaby.service;

import com.schoolaby.repository.JourneyStateJpaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class JourneyStateService {
    private final JourneyStateJpaRepository journeyStateRepository;

    public void deleteJourneyState(Long journeyId, Long userId, String authorityName) {
        journeyStateRepository.deleteByJourneyIdAndUserIdAndAuthorityName(journeyId, userId, authorityName);
    }
}
