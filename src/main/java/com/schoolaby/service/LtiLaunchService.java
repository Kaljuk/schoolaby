package com.schoolaby.service;

import com.google.api.client.auth.oauth.OAuthHmacSigner;
import com.google.api.client.auth.oauth.OAuthParameters;
import com.google.api.client.http.GenericUrl;
import com.schoolaby.config.lti.LtiProperties;
import com.schoolaby.domain.*;
import com.schoolaby.domain.enumeration.LtiConfigType;
import com.schoolaby.repository.LtiLaunchRepository;
import com.schoolaby.repository.filter.LtiConfigFilter;
import com.schoolaby.security.SecurityUtils;
import com.schoolaby.service.dto.LtiLaunchDTO;
import com.schoolaby.service.mapper.LtiLaunchMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.schoolaby.common.LtiUtils.createPendingScore;
import static com.schoolaby.security.SecurityUtils.getCurrentUserId;
import static com.schoolaby.service.mapper.UserDetailedMapper.userFromId;
import static java.lang.Long.parseLong;
import static java.lang.String.format;
import static java.util.Objects.requireNonNullElse;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpMethod.POST;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class LtiLaunchService {
    private static final String LTI_LAUNCH_MESSAGE_TYPE = "basic-lti-launch-request";
    private static final String CONTENT_ITEM_SELECTION_MESSAGE_TYPE = "ContentItemSelectionRequest";
    private static final String ACCEPT_MEDIA_TYPES = "application/vnd.ims.lti.v1.ltilink";

    private final LtiResourceService ltiResourceService;
    private final LtiConfigService configService;
    private final LtiLaunchRepository ltiLaunchRepository;
    private final LtiLaunchMapper ltiLaunchMapper;
    private final LtiProperties ltiProperties;
    private final JourneyService journeyService;
    private final UserService userService;
    private final SimpMessagingTemplate simpMessagingTemplate;

    public Map<String, Object> launchResource(Long ltiResourceId, String returnUrl) {
        log.info("Getting LTI launch parameters for LtiResource: {}", ltiResourceId);
        LtiResource ltiResource = ltiResourceService.getOne(ltiResourceId);
        LtiConfig ltiConfig = ltiResource.getLtiApp().getConfigs().stream()
            .filter(config -> config.getType().equals(LtiConfigType.PLATFORM) || ltiResource.getJourney().equals(config.getJourney()))
            .findFirst()
            .orElseThrow(() -> new DataIntegrityViolationException(format("No LtiConfig found for LtiResource: %s", ltiResource.getId())));

        LtiLaunch ltiLaunch = ltiLaunchRepository.save(
            new LtiLaunch()
                .user(userFromId(getCurrentUserId()))
                .ltiResource(ltiResource)
        );

        if (ltiResource.getAssignment() != null && ltiResource.isSyncGrade()) {
            List<LtiLaunch> launches = ltiLaunchRepository.findAll(ltiResource.getAssignment().getId(), getCurrentUserId(), null);
            simpMessagingTemplate.convertAndSendToUser(userService.getOneById(getCurrentUserId()).getLogin(), "/queue/lti-scores", createPendingScore(ltiLaunch, launches));
        }

        return composeLaunchParameters(
            LaunchParametersFilter.builder()
                .ltiConfig(ltiConfig)
                .resourceLinkId(ltiResource.getResourceLinkId())
                .ltiLaunch(ltiLaunch)
                .journey(ltiResource.getJourney())
                .resourceTitle(ltiResource.getTitle())
                .returnUrl(returnUrl)
                .launchUrl(requireNonNullElse(ltiResource.getLaunchUrl(), ltiConfig.getLaunchUrl()))
                .messageType(LTI_LAUNCH_MESSAGE_TYPE)
                .build()
        );
    }

    public Map<String, Object> launchApp(Long appId, Long journeyId, String resourceLinkId, String returnUrl) {
        log.info("Getting LTI launch parameters for LtiApp: {}", appId);
        LtiConfig ltiConfig = configService.getOne(LtiConfigFilter.builder()
            .appId(appId)
            .journeyId(journeyId)
            .build());
        Journey journey = journeyService.getOne(journeyId, false);

        String messageType = LTI_LAUNCH_MESSAGE_TYPE;
        if (ltiConfig.getLtiApp().getContentItemSelectionEnabled()) {
            messageType = CONTENT_ITEM_SELECTION_MESSAGE_TYPE;
        }

        return composeLaunchParameters(
            LaunchParametersFilter.builder()
                .ltiConfig(ltiConfig)
                .resourceLinkId(resourceLinkId)
                .journey(journey)
                .returnUrl(returnUrl)
                .launchUrl(ltiConfig.getLaunchUrl())
                .messageType(messageType)
                .build()
        );
    }

    private void addBasicLtiLaunchParams(Map<String, Object> ltiParameters, LaunchParametersFilter filter) {
        LtiLaunch ltiLaunch = filter.getLtiLaunch();
        boolean syncGrade = ltiLaunch != null && ltiLaunch.getLtiResource() != null && ltiLaunch.getLtiResource().isSyncGrade();
        if (syncGrade) {
            ltiParameters.put("lis_result_sourcedid", ltiLaunch.getId().toString());
        }
        if (filter.getResourceTitle() != null) {
            ltiParameters.put("resource_link_title", filter.getResourceTitle());
        }

        ltiParameters.put("resource_link_id", filter.getResourceLinkId());
        ltiParameters.put("launch_presentation_return_url", filter.getReturnUrl());
        ltiParameters.put("launch_presentation_document_target", ltiProperties.getLaunchPresentationDocumentTarget());
    }

    private void addContentItemSelectionLaunchParams(Map<String, Object> ltiParameters) {
        ltiParameters.put("content_item_return_url", ltiProperties.getContentItemReturnUrl());
        ltiParameters.put("accept_media_types", ACCEPT_MEDIA_TYPES);
        ltiParameters.put("accept_multiple", false);
        ltiParameters.put("accept_presentation_document_targets", ltiProperties.getLaunchPresentationDocumentTarget());
    }

    private Map<String, Object> composeLaunchParameters(@Valid LaunchParametersFilter filter) {
        String messageType = filter.getMessageType();
        LtiConfig ltiConfig = filter.getLtiConfig();
        Journey journey = filter.getJourney();
        LtiApp ltiApp = ltiConfig.getLtiApp();

        Map<String, Object> ltiParameters = new HashMap<>();

        if (messageType.equals(CONTENT_ITEM_SELECTION_MESSAGE_TYPE)) {
            addContentItemSelectionLaunchParams(ltiParameters);
        } else if (messageType.equals(LTI_LAUNCH_MESSAGE_TYPE)) {
            addBasicLtiLaunchParams(ltiParameters, filter);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_IMPLEMENTED, String.format("LTI launch for message_type '%s' is not supported", messageType));
        }

        ltiParameters.put("lti_message_type", filter.getMessageType());
        ltiParameters.put("launch_url", filter.getLaunchUrl());
        ltiParameters.put("oauth_version", "1.0");
        ltiParameters.put("lti_version", ltiApp.getVersion());
        ltiParameters.put("roles", SecurityUtils.getCurrentUserLtiRole().getRoleName());
        ltiParameters.put("user_id", getCurrentUserId());
        ltiParameters.put("context_id", journey.getId());
        ltiParameters.put("context_type", "CourseSection");
        ltiParameters.put("context_title", journey.getTitle());
        ltiParameters.put("context_label", format("J%s", journey.getId()));
        ltiParameters.put("tool_consumer_instance_guid", ltiProperties.getToolConsumerInstanceGuid());
        ltiParameters.put("tool_consumer_info_product_family_code", ltiProperties.getToolConsumerInfoProductFamilyCode());
        ltiParameters.put("tool_consumer_info_version", ltiProperties.getToolConsumerInfoVersion());
        ltiParameters.put("tool_consumer_instance_name", ltiProperties.getToolConsumerInstanceName());
        ltiParameters.put("tool_consumer_instance_contact_email", ltiProperties.getToolConsumerInstanceContactEmail());
        ltiParameters.put("lis_outcome_service_url", ltiProperties.getCallbackUrl());

        addOauth1Parameters(filter.getLaunchUrl(), ltiConfig.getConsumerKey(), ltiConfig.getSharedSecret(), ltiParameters);

        return ltiParameters;
    }

    private void addOauth1Parameters(String url, String consumerKey, String sharedSecret, Map<String, Object> ltiParameters) {
        OAuthHmacSigner signer = new OAuthHmacSigner();
        signer.clientSharedSecret = sharedSecret;

        OAuthParameters params = new OAuthParameters();
        params.consumerKey = consumerKey;
        params.signer = signer;
        params.version = (String) ltiParameters.get("oauth_version");
        params.computeTimestamp();
        params.computeNonce();

        GenericUrl requestUrl = new GenericUrl(url);
        ltiParameters.forEach(requestUrl::put);

        try {
            params.computeSignature(POST.name(), requestUrl);
        } catch (GeneralSecurityException e) {
            log.error("Failed to compute oauth_signature for url:{} consumerKey:{}, exception:{}", url, consumerKey, e.getMessage());
        }

        ltiParameters.put("oauth_consumer_key", params.consumerKey);
        ltiParameters.put("oauth_timestamp", params.timestamp);
        ltiParameters.put("oauth_nonce", params.nonce);
        ltiParameters.put("oauth_signature_method", params.signatureMethod);
        ltiParameters.put("oauth_version", params.version);
        ltiParameters.put("oauth_signature", params.signature);
    }

    public List<LtiLaunchDTO> findLtiLaunches(Long assignmentId, Long userId) {
        return ltiLaunchRepository.findAll(assignmentId, userId, null).stream()
            .map(ltiLaunchMapper::toDto)
            .collect(toList());
    }

    public LtiLaunchDTO replaceResult(String sourcedId, String resultScore) {
        return ltiLaunchMapper.toDto(ltiLaunchRepository.getOne(parseLong(sourcedId)).setResult(resultScore));
    }
}
