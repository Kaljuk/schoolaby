package com.schoolaby.service;

import com.schoolaby.domain.Group;
import com.schoolaby.repository.GroupJpaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class GroupService {

    private final GroupJpaRepository groupJpaRepository;

    public Optional<Group> findOne(Long id) {
        return groupJpaRepository.findFirstById(id);
    }
}
