/**
 * Data Access Objects used by WebSocket services.
 */
package com.schoolaby.web.websocket.dto;
