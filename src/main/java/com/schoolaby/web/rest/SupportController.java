package com.schoolaby.web.rest;

import com.schoolaby.service.SupportEmailService;
import com.schoolaby.service.dto.SupportEmailDTO;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;

import static com.schoolaby.security.SecurityUtils.IS_STUDENT_OR_TEACHER_OR_ADMIN;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.ResponseEntity.created;

@RestController
@RequestMapping("/api/support")
@RequiredArgsConstructor
public class SupportController {
    private final Logger log = getLogger(SupportController.class);
    private final SupportEmailService supportEmailService;

    @PostMapping
    @PreAuthorize(IS_STUDENT_OR_TEACHER_OR_ADMIN)
    public ResponseEntity<Void> sendEmail(@RequestBody SupportEmailDTO supportEmailDTO) throws URISyntaxException {
        log.debug("Rest request to send email with subject {}", supportEmailDTO.getSubject());
        supportEmailService.sendSupportEmail(supportEmailDTO);
        return created(new URI("")).build();
    }
}
