package com.schoolaby.web.rest;

import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WellController {

    @GetMapping("/.well-known/microsoft-identity-association.json")
    public String get() {
        return "{\n" +
            "  \"associatedApplications\": [\n" +
            "    {\n" +
            "      \"applicationId\": \"61f92aae-61a7-48ca-874f-5c334f92513f\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";
    }
}
