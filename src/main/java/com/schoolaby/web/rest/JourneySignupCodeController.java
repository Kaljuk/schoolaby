package com.schoolaby.web.rest;

import com.schoolaby.service.JourneySignupCodeService;
import com.schoolaby.service.dto.JourneySignupCodeDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;

import static org.springframework.http.ResponseEntity.created;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/journeys/signup-code")
public class JourneySignupCodeController {
    public static final String JOURNEY_JOIN_LINK = "/journey/join?signUpCode=";

    private final JourneySignupCodeService journeySignupCodeService;

    @PostMapping
    public ResponseEntity<JourneySignupCodeDTO> createOnetimeSignupCode(@RequestBody JourneySignupCodeDTO signupCodeDTO) throws URISyntaxException {
        log.debug("REST request to generate onetime signupCode: {}", signupCodeDTO.getSignUpCode());
        JourneySignupCodeDTO result = journeySignupCodeService.createOneTimeSignupCode(signupCodeDTO);
        return created(new URI(JOURNEY_JOIN_LINK + result.getSignUpCode())).body(result);
    }
}
