package com.schoolaby.web.rest;

import com.schoolaby.domain.enumeration.MaterialSource;
import com.schoolaby.service.ExternalMaterialService;
import com.schoolaby.service.dto.MaterialDTO;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.NativeWebRequest;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static com.schoolaby.domain.enumeration.MaterialSource.EKOOLIKOTT;
import static com.schoolaby.domain.enumeration.MaterialSource.OPIQ;
import static java.util.Collections.list;
import static org.springframework.http.HttpHeaders.ACCEPT;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class SuggestionController {

    public static final String SUGGESTIONS = "suggestions";

    private final Logger log = LoggerFactory.getLogger(SuggestionController.class);
    private final ExternalMaterialService externalMaterialService;

    RestTemplate restTemplate = new RestTemplate();

    @GetMapping(SUGGESTIONS + "/search")
    public ResponseEntity<List<MaterialDTO>> getMaterialsBySearch(
        NativeWebRequest request,
        @RequestParam String searchString,
        @RequestParam Integer limit,
        @RequestParam(required = false) MaterialSource source
    ) {
        List<MaterialDTO> responseMaterials;
        if (EKOOLIKOTT.equals(source)) {
            responseMaterials = externalMaterialService.getEkoolikottMaterialsBySearchString(request, searchString, 80);
            int toIndex = Math.min(responseMaterials.size(), 3);
            responseMaterials = responseMaterials.subList(0, toIndex);
        } else if (OPIQ.equals(source)) {
            responseMaterials = externalMaterialService.getOpiqMaterialsBySearchString(searchString, limit);
        } else {
            responseMaterials = externalMaterialService.getMaterialsBySearchString(request, searchString, limit);
        }

        return ResponseEntity.ok().body(responseMaterials);
    }

    @GetMapping(SUGGESTIONS + "/suggested")
    public ResponseEntity<List<MaterialDTO>> getSuggestedMaterials(
        NativeWebRequest request,
        @RequestParam(required = false) String searchString,
        @RequestParam Integer limit,
        @RequestParam(required = false) String subjects,
        @RequestParam(required = false) MaterialSource source,
        @RequestParam(required = false) String taxon
    ) {
        List<MaterialDTO> responseMaterials;
        if (EKOOLIKOTT.equals(source)) {
            responseMaterials = externalMaterialService.getSuggestedEkoolikottMaterials(request, 80, taxon);
            int toIndex = Math.min(responseMaterials.size(), 3);
            responseMaterials = responseMaterials.subList(0, toIndex);
        } else if (OPIQ.equals(source)) {
            responseMaterials = externalMaterialService.getSuggestedOpiqMaterials(searchString, subjects, limit);
        } else {
            responseMaterials = externalMaterialService.getSuggestedMaterials(request, searchString, subjects, limit, taxon);
        }

        return ResponseEntity.ok().body(responseMaterials);
    }

    @GetMapping(SUGGESTIONS + "/**")
    public ResponseEntity<Resource> getSuggestions(HttpServletRequest request) throws URISyntaxException {
        log.info("Getting " + SUGGESTIONS + " for: {}", request.getQueryString());

        HttpHeaders headers = new HttpHeaders();
        headers.set(ACCEPT, list(request.getHeaders(ACCEPT)).get(0));

        URI uri = new URI(
            "https",
            null,
            "www.e-koolikott.ee",
            -1,
            request.getRequestURI().split(SUGGESTIONS)[1],
            request.getQueryString(),
            null
        );
        return restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity(headers), Resource.class);
    }
}
