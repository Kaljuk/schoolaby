package com.schoolaby.web.rest;

import com.schoolaby.service.CurriculumService;
import com.schoolaby.service.dto.CurriculumDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/curricula")
public class CurriculumController {
    private final CurriculumService curriculumService;

    @GetMapping
    public ResponseEntity<Set<CurriculumDTO>> findAllByCountry(@RequestParam(required = false) String country) {
        return ok(curriculumService.findAllByCountry(country));
    }
}
