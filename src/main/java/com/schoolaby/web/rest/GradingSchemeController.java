package com.schoolaby.web.rest;

import com.schoolaby.service.GradingSchemeService;
import com.schoolaby.service.dto.GradingSchemeDTO;
import com.schoolaby.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static com.schoolaby.security.SecurityUtils.IS_ADMIN;
import static io.github.jhipster.web.util.HeaderUtil.*;

@RestController
@RequestMapping("/api")
public class GradingSchemeController {
    private final Logger log = LoggerFactory.getLogger(GradingSchemeController.class);

    private static final String ENTITY_NAME = "gradingScheme";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GradingSchemeService gradingSchemeService;

    public GradingSchemeController(GradingSchemeService gradingSchemeService) {
        this.gradingSchemeService = gradingSchemeService;
    }

    /**
     * {@code POST  /grading-schemes} : Create a new gradingScheme.
     *
     * @param gradingSchemeDTO the gradingSchemeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new gradingSchemeDTO, or with status {@code 400 (Bad Request)} if the gradingScheme has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PreAuthorize(IS_ADMIN)
    @PostMapping("/grading-schemes")
    public ResponseEntity<GradingSchemeDTO> createGradingScheme(@Valid @RequestBody GradingSchemeDTO gradingSchemeDTO)
        throws URISyntaxException {
        log.debug("REST request to save GradingScheme : {}", gradingSchemeDTO);
        if (gradingSchemeDTO.getId() != null) {
            throw new BadRequestAlertException("A new gradingScheme cannot already have an ID", ENTITY_NAME, "idExists");
        }
        GradingSchemeDTO result = gradingSchemeService.create(gradingSchemeDTO);
        return ResponseEntity
            .created(new URI("/api/grading-schemes/" + result.getId()))
            .headers(createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getName()))
            .body(result);
    }

    /**
     * {@code PUT  /grading-schemes} : Updates an existing gradingScheme.
     *
     * @param gradingSchemeDTO the gradingSchemeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated gradingSchemeDTO,
     * or with status {@code 400 (Bad Request)} if the gradingSchemeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the gradingSchemeDTO couldn't be updated.
     */
    @PreAuthorize(IS_ADMIN)
    @PutMapping("/grading-schemes")
    public ResponseEntity<GradingSchemeDTO> updateGradingScheme(@Valid @RequestBody GradingSchemeDTO gradingSchemeDTO) {
        log.debug("REST request to update GradingScheme : {}", gradingSchemeDTO);
        if (gradingSchemeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idNull");
        }
        GradingSchemeDTO result = gradingSchemeService.update(gradingSchemeDTO);
        return ResponseEntity
            .ok()
            .headers(createEntityUpdateAlert(applicationName, true, ENTITY_NAME, gradingSchemeDTO.getName()))
            .body(result);
    }

    /**
     * {@code GET  /grading-schemes} : get all the gradingSchemes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of gradingSchemes in body.
     */
    @GetMapping("/grading-schemes")
    public ResponseEntity<List<GradingSchemeDTO>> getAllGradingSchemes(Pageable pageable) {
        log.debug("REST request to get a page of GradingSchemes");
        Page<GradingSchemeDTO> page = gradingSchemeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /grading-schemes/:id} : get the "id" gradingScheme.
     *
     * @param id the id of the gradingSchemeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the gradingSchemeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/grading-schemes/{id}")
    public ResponseEntity<GradingSchemeDTO> getGradingScheme(@PathVariable Long id) {
        log.debug("REST request to get GradingScheme : {}", id);
        Optional<GradingSchemeDTO> gradingSchemeDTO = gradingSchemeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(gradingSchemeDTO);
    }

    /**
     * {@code DELETE  /grading-schemes/:id} : delete the "id" gradingScheme.
     *
     * @param id the id of the gradingSchemeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @PreAuthorize(IS_ADMIN)
    @DeleteMapping("/grading-schemes/{id}")
    public ResponseEntity<Void> deleteGradingScheme(@PathVariable Long id) {
        log.debug("REST request to delete GradingScheme : {}", id);
        gradingSchemeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
