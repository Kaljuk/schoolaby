package com.schoolaby.web.rest;

import com.schoolaby.service.AssignmentReadService;
import com.schoolaby.service.AssignmentWriteService;
import com.schoolaby.service.dto.AssignmentDTO;
import com.schoolaby.service.dto.AssignmentPatchDTO;
import com.schoolaby.service.dto.AssignmentUpdateDTO;
import com.schoolaby.service.dto.GroupAssignmentDTO;
import com.schoolaby.service.dto.states.EntityState;
import com.schoolaby.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;

import static com.schoolaby.security.SecurityUtils.IS_STUDENT_OR_TEACHER_OR_ADMIN;
import static com.schoolaby.security.SecurityUtils.IS_TEACHER_OR_ADMIN;
import static io.github.jhipster.web.util.HeaderUtil.*;
import static io.github.jhipster.web.util.PaginationUtil.generatePaginationHttpHeaders;
import static org.springframework.http.ResponseEntity.*;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

@RestController
@RequestMapping("/api")
public class AssignmentController {
    private final Logger log = LoggerFactory.getLogger(AssignmentController.class);

    private static final String ENTITY_NAME = "assignment";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AssignmentReadService assignmentReadService;
    private final AssignmentWriteService assignmentWriteService;

    public AssignmentController(AssignmentReadService assignmentReadService, AssignmentWriteService assignmentWriteService) {
        this.assignmentReadService = assignmentReadService;
        this.assignmentWriteService = assignmentWriteService;
    }

    @PostMapping("/assignments")
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<AssignmentDTO> createAssignment(@Valid @RequestBody AssignmentUpdateDTO assignmentUpdateDTO) throws URISyntaxException {
        log.debug("REST request to save Assignment : {}", assignmentUpdateDTO);
        if (assignmentUpdateDTO.getId() != null) {
            throw new BadRequestAlertException("A new assignment cannot already have an ID", ENTITY_NAME, "idExists");
        }
        AssignmentDTO result = assignmentWriteService.create(assignmentUpdateDTO);
        return created(new URI("/api/assignments/" + result.getId()))
            .headers(createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getTitle()))
            .body(result);
    }

    @PutMapping("/assignments")
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<AssignmentDTO> updateAssignment(@Valid @RequestBody AssignmentUpdateDTO assignmentUpdateDTO) {
        log.debug("REST request to update Assignment : {}", assignmentUpdateDTO);
        if (assignmentUpdateDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idNull");
        }
        AssignmentDTO result = assignmentWriteService.update(assignmentUpdateDTO);
        return ok().headers(createEntityUpdateAlert(applicationName, true, ENTITY_NAME, assignmentUpdateDTO.getTitle())).body(result);
    }

    @PatchMapping("/assignments/{id}")
    @PreAuthorize(IS_STUDENT_OR_TEACHER_OR_ADMIN)
    public ResponseEntity<AssignmentDTO> patch(@PathVariable("id") Long id,
                                               @Valid @RequestBody AssignmentPatchDTO assignmentPatchDTO) {
        log.debug("REST request to patch Assignment : {}", id);
        AssignmentDTO assignmentDTO = assignmentWriteService.patch(id, assignmentPatchDTO);
        return ok().headers(createEntityUpdateAlert(applicationName, true, ENTITY_NAME, assignmentDTO.getTitle())).body(assignmentDTO);
    }

    @GetMapping("/assignments")
    public ResponseEntity<List<AssignmentDTO>> getAllAssignments(
        Pageable pageable,
        @RequestParam(required = false) Long journeyId,
        @RequestParam(defaultValue = "-4000-01-01") LocalDate fromDate,
        @RequestParam(defaultValue = "+294000-12-31") LocalDate toDate,
        @RequestParam List<EntityState> states
    ) {
        log.debug("REST request to get a page of Assignments");

        Page<AssignmentDTO> page = assignmentReadService.findAll(fromDate, toDate, states, journeyId, pageable);

        return ok().headers(generatePaginationHttpHeaders(fromCurrentRequest(), page)).body(page.getContent());
    }

    @GetMapping("/assignments/{id}")
    public ResponseEntity<AssignmentDTO> getAssignment(
        @PathVariable Long id,
        @RequestParam(required = false, defaultValue = "false") Boolean template,
        @RequestParam(required = false) Boolean withPreviousAndNextIds
    ) {
        log.debug("REST request to get Assignment : {}", id);
        AssignmentDTO assignmentDTO = Boolean.TRUE.equals(withPreviousAndNextIds) ? assignmentReadService.getOneWithPreviousIdAndNextIdDTO(id, template) : assignmentReadService.getOneDTO(id, template);
        if (assignmentDTO == null) {
            return notFound().build();
        } else {
            return ok(assignmentDTO);
        }
    }

    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    @GetMapping("/group-assignments")
    public ResponseEntity<List<GroupAssignmentDTO>> getGroupAssignmentsBySearch(@RequestParam Long journeyId, @RequestParam String search) {
        log.debug("REST request to get assignments by journey id : {} and search : {}", journeyId, search);
        return ok(assignmentReadService.getGroupAssignmentsBySearch(journeyId, search));
    }

    @DeleteMapping("/assignments/{id}")
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<Void> deleteAssignment(@PathVariable Long id) {
        log.debug("REST request to delete Assignment : {}", id);
        assignmentWriteService.delete(id);
        return noContent().headers(createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
