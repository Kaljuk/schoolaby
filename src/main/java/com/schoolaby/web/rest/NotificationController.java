package com.schoolaby.web.rest;

import com.schoolaby.domain.NotificationType;
import com.schoolaby.domain.PatchNotificationDTO;
import com.schoolaby.service.NotificationService;
import com.schoolaby.service.dto.NotificationDTO;
import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.schoolaby.security.SecurityUtils.IS_AUTHENTICATED;
import static io.github.jhipster.web.util.PaginationUtil.generatePaginationHttpHeaders;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

@RestController
@RequestMapping("/api/notifications")
public class NotificationController {
    private final Logger log = getLogger(NotificationController.class);

    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping
    public ResponseEntity<List<NotificationDTO>> findAll(NotificationType type, Pageable pageable) {
        log.debug("REST request to get Notifications");
        Page<NotificationDTO> notifications = notificationService.findAllByNotificationType(pageable, type);
        return ok()
            .headers(generatePaginationHttpHeaders(fromCurrentRequest(), notifications))
            .body(notifications.getContent());
    }

    @PatchMapping
    public ResponseEntity<List<NotificationDTO>> patchNotifications(@RequestBody(required = false) List<PatchNotificationDTO> patchNotificationDTO) {
        log.debug("REST request to mark Notifications as read");
        List<NotificationDTO> updatedNotifications = notificationService.update(patchNotificationDTO);
        return ok().body(updatedNotifications);
    }
}
