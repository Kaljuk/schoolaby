package com.schoolaby.web.rest;

import com.schoolaby.service.EducationalLevelService;
import com.schoolaby.service.dto.EducationalLevelDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/educational-levels")
@Slf4j
@RequiredArgsConstructor
public class EducationalLevelController {
    private final EducationalLevelService educationalLevelService;

    @GetMapping
    public ResponseEntity<List<EducationalLevelDTO>> findAll(@RequestParam(required = false, defaultValue = "Estonia") String country) {
        log.debug("REST request to get a set of EducationalLevels");
        return ok(educationalLevelService.findByCountry(country));
    }
}
