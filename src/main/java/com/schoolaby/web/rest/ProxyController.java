package com.schoolaby.web.rest;

import org.slf4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.SocketTimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter.XFRAME_OPTIONS_HEADER;

@RestController
@RequestMapping("/api/proxy")
public class ProxyController {
    private final Logger log = getLogger(ProxyController.class);
    private final RestTemplate restTemplate;
    private final RestTemplate externalHeaderCheckTemplate;

    public ProxyController(RestTemplate restTemplate, RestTemplate externalHeaderCheckTemplate) {
        this.restTemplate = restTemplate;
        this.externalHeaderCheckTemplate = externalHeaderCheckTemplate;
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<Resource> get(@RequestParam String url) {
        log.info("Proxying '{}'", url);
        return restTemplate.getForEntity(url, Resource.class);
    }

    @GetMapping("/embeddable")
    @ResponseBody
    public ResponseEntity<Boolean> isEmbeddable(@RequestParam String url) {
        log.info("Checking if {} is embeddable", url);
        Pattern pattern = Pattern.compile("\\{([^}]*.?)\\}");
        Matcher matcher = pattern.matcher(url);

        if (matcher.find()) {
            log.info("{} contains curly brackets '{' '}' and is not embeddable", url);
            return ok(false);
        }

        try {
            HttpHeaders headers = externalHeaderCheckTemplate.headForHeaders(url);
            String xFrameOptions = headers.getFirst(XFRAME_OPTIONS_HEADER);
            return ok(xFrameOptions == null);
        } catch (HttpClientErrorException e) {
            log.info("{} when testing {} headers, not embeddable", e.getStatusCode(), url);
            return ok(false);
        } catch (RestClientException e) {
            if (e.getCause() instanceof SocketTimeoutException) {
                log.info("Testing headers for {} took too long, not embeddable.", url);
            } else {
                log.info(e.getMessage(), e);
            }
            return ok(false);
        }
    }
}
