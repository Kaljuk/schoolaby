package com.schoolaby.web.rest;

import com.schoolaby.service.SubmissionFeedbackService;
import com.schoolaby.service.dto.submission.SubmissionFeedbackDTO;
import com.schoolaby.service.dto.submission.SubmissionGradingPatchDTO;
import com.schoolaby.service.dto.submission.SubmissionPatchDTO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;

import static com.schoolaby.security.SecurityUtils.IS_TEACHER_OR_ADMIN;
import static io.github.jhipster.web.util.HeaderUtil.createEntityCreationAlert;
import static io.github.jhipster.web.util.HeaderUtil.createEntityUpdateAlert;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api")
public class SubmissionFeedbackController {
    private static final String ENTITY_NAME = "submissionFeedback";
    private final Logger log = getLogger(SubmissionFeedbackController.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private final SubmissionFeedbackService submissionFeedbackService;

    public SubmissionFeedbackController(SubmissionFeedbackService submissionFeedbackService) {
        this.submissionFeedbackService = submissionFeedbackService;
    }

    @GetMapping("/submission-feedback")
    public ResponseEntity<Set<SubmissionFeedbackDTO>> get(@RequestParam Long studentId, @RequestParam Long submissionId) throws URISyntaxException {
        log.debug("REST request to get SubmissionFeedback by studentId:{}, submissionId:{}", studentId, submissionId);

        Set<SubmissionFeedbackDTO> submissionFeedbackDTO = submissionFeedbackService.findAll(studentId, submissionId);

        return ok(submissionFeedbackDTO);
    }

    @PostMapping("/submission-feedback")
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<SubmissionFeedbackDTO> post(@Valid @RequestBody SubmissionPatchDTO submissionPatchDTO) throws URISyntaxException {
        log.debug("REST request to create Submission Feedback : {}", submissionPatchDTO);

        SubmissionFeedbackDTO submissionFeedback;
        if (submissionPatchDTO instanceof SubmissionGradingPatchDTO) {
            submissionFeedback = submissionFeedbackService.gradeNonExistingSubmission((SubmissionGradingPatchDTO) submissionPatchDTO);
        } else {
            throw new RuntimeException("Invalid submissionPatchDTO");
        }

        return created(new URI("api/submission-feedback/" + submissionFeedback.getId()))
            .headers(createEntityCreationAlert(applicationName, true, ENTITY_NAME, submissionFeedback.getId().toString()))
            .body(submissionFeedback);
    }

    @PatchMapping("/submission-feedback")
    @PreAuthorize(IS_TEACHER_OR_ADMIN)
    public ResponseEntity<SubmissionFeedbackDTO> patch(@RequestParam Long submissionId,
                                                       @Valid @RequestBody SubmissionPatchDTO submissionPatchDTO) {
        log.debug("REST request to partially update Submission : {}", submissionPatchDTO);

        SubmissionFeedbackDTO submissionFeedback;
        if (submissionPatchDTO instanceof SubmissionGradingPatchDTO) {
            submissionFeedback = submissionFeedbackService.gradeSubmission(submissionId, (SubmissionGradingPatchDTO) submissionPatchDTO);
        } else {
            throw new RuntimeException("Invalid submissionPatchDTO");
        }

        return ok()
            .headers(createEntityUpdateAlert(applicationName, true, ENTITY_NAME, submissionFeedback.getId().toString()))
            .body(submissionFeedback);
    }
}
