/**
 * View Models used by Spring MVC REST controllers.
 */
package com.schoolaby.web.rest.vm;
