package com.schoolaby.service;

import com.schoolaby.repository.EkoolRepository;
import com.schoolaby.service.dto.EkoolPersonalDatasDTO;
import com.schoolaby.service.dto.EkoolRolesDTO;
import com.schoolaby.service.dto.EkoolSchoolsDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.common.TestUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
class EkoolServiceTest {

    private final OAuth2AuthorizedClient oAuth2AuthorizedClient = getEkoolOauth2AuthorizedClient();

    @Mock
    private EkoolRepository ekoolRepository;
    @Mock
    private OAuth2AuthorizedClientService oAuth2AuthorizedClientService;

    @InjectMocks
    private EkoolService ekoolService;

    @Test
    void shouldGetSchools() {
        EkoolSchoolsDTO ekoolSchoolsDTO = new EkoolSchoolsDTO()
            .setData(Set.of(
                new EkoolSchoolsDTO.EkoolSchoolDTO()
                    .setName(SCHOOL_NAME)
            ));
        doReturn(oAuth2AuthorizedClient).when(oAuth2AuthorizedClientService).loadAuthorizedClient(EKOOL_REGISTRATION_ID, EXTERNAL_ID);
        doReturn(ekoolSchoolsDTO).when(ekoolRepository).getUserSchools(oAuth2AuthorizedClient.getAccessToken());

        List<EkoolSchoolsDTO.EkoolSchoolDTO> schools = new ArrayList<>(ekoolService.getSchools(EXTERNAL_ID));

        assertEquals(SCHOOL_NAME, schools.get(0).getName());
    }

    @Test
    void shouldGetRoles() {
        EkoolRolesDTO ekoolRolesDTO = new EkoolRolesDTO()
            .setData(Set.of(
                new EkoolRolesDTO.EkoolRoleDTO()
                    .setRoleName(ROLE_NAME)
            ));
        doReturn(oAuth2AuthorizedClient).when(oAuth2AuthorizedClientService).loadAuthorizedClient(EKOOL_REGISTRATION_ID, EXTERNAL_ID);
        doReturn(ekoolRolesDTO).when(ekoolRepository).getUserRoles(oAuth2AuthorizedClient.getAccessToken());

        List<EkoolRolesDTO.EkoolRoleDTO> roles = new ArrayList<>(ekoolService.getRoles(EXTERNAL_ID));

        assertEquals(ROLE_NAME, roles.get(0).getRoleName());
    }

    @Test
    void shouldGetEmail() {
        EkoolPersonalDatasDTO ekoolPersonalDatasDTO = new EkoolPersonalDatasDTO()
            .setData(new EkoolPersonalDatasDTO.EkoolPersonalDataDTO().setEmail(USER_EMAIL));
        doReturn(oAuth2AuthorizedClient).when(oAuth2AuthorizedClientService).loadAuthorizedClient(EKOOL_REGISTRATION_ID, EXTERNAL_ID);
        doReturn(ekoolPersonalDatasDTO).when(ekoolRepository).getUserBasic(oAuth2AuthorizedClient.getAccessToken());

        String email = ekoolService.getEmail(EXTERNAL_ID);

        assertEquals(USER_EMAIL, email);
    }
}
