package com.schoolaby.service;

import com.schoolaby.domain.*;
import com.schoolaby.repository.LineItemRepository;
import com.schoolaby.repository.LtiLaunchRepository;
import com.schoolaby.repository.LtiScoreRepository;
import com.schoolaby.repository.UserRepository;
import com.schoolaby.service.dto.lti.LtiScoreDTO;
import com.schoolaby.service.dto.lti.external.LtiExtScoreDTO;
import com.schoolaby.service.mapper.LtiExtScoreMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.List;
import java.util.Optional;

import static com.schoolaby.common.TestObjects.*;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LtiGradingServiceTest {

    @Mock
    private LtiScoreRepository scoreRepository;
    @Mock
    private SimpMessagingTemplate simpMessagingTemplate;
    @Mock
    private UserRepository userRepository;
    @Mock
    private LtiLaunchRepository ltiLaunchRepository;

    @InjectMocks
    private LtiGradingService ltiGradingService;

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void shouldSendScoreToWebsocket(boolean syncGrade) {
        User user = createUser().setId(1L);
        Assignment assignment = createAssignment().setId(1L).setMilestone(createMilestone().journey(createJourney()));
        LtiResource ltiResource = createLtiResource().setId(1L).setAssignment(assignment).setSyncGrade(syncGrade);
        LtiLineItem ltiLineItem = createLtiLineItem().setId(1L).setLtiResource(ltiResource);
        LtiScore ltiScore = createLtiScore().setUser(user).setLineItem(ltiLineItem);

        doReturn(ltiScore).when(scoreRepository).getOne(eq(ltiScore.getId()));

        if (syncGrade) {
            doReturn(Optional.of(user)).when(userRepository).findById(user.getId());
        }

        ltiGradingService.sendScoreToWebSocket(ltiScore.getId());

        if (syncGrade) {
            verify(simpMessagingTemplate).convertAndSendToUser(eq(user.getLogin()), eq("/queue/lti-scores"), argThat((payload) -> {
                LtiScoreDTO ltiScoreDTO = (LtiScoreDTO) payload;
                return ltiScoreDTO.getResourceId().equals(ltiResource.getId()) && ltiScoreDTO.getScore().equals(ltiScore.getScoreGiven().toString());
            }));
        } else {
            verifyNoMoreInteractions(simpMessagingTemplate);
        }
    }
}
