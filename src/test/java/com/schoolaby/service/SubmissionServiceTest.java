package com.schoolaby.service;

import com.schoolaby.common.TestUtils;
import com.schoolaby.domain.*;
import com.schoolaby.repository.SubmissionRepository;
import com.schoolaby.service.dto.UserDTO;
import com.schoolaby.service.dto.submission.SubmissionDTO;
import com.schoolaby.service.mapper.SubmissionMapper;
import com.schoolaby.service.mapper.UserDetailedMapper;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.schoolaby.common.TestCases.createStudent;
import static com.schoolaby.common.TestCases.createTeacher;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.common.TestUtils.mapUserToDetailedDTO;
import static com.schoolaby.security.Role.Constants.ADMIN;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;

@ExtendWith(SpringExtension.class)
class SubmissionServiceTest {
    @Mock
    private SubmissionRepository submissionRepository;
    @Mock
    private NotificationService notificationService;
    @Mock
    private AssignmentReadService assignmentReadService;
    @Mock
    private SubmissionMapper submissionMapper;
    @Mock
    private GroupService groupService;
    @Mock
    private UserDetailedMapper userDetailedMapper;
    @Mock
    private JourneyService journeyService;
    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @InjectMocks
    private SubmissionService submissionService;

    @Test
    @WithMockCustomUser(authorities = {ADMIN})
    void shouldCreate() {
        Journey journey = createJourney().creator(createTeacher());
        Milestone milestone = createMilestone().journey(journey);
        Assignment assignment = createAssignment()
            .id(1L)
            .milestone(milestone);
        Submission submission = createSubmission()
            .authors(Set.of(createStudent()))
            .assignment(assignment);
        SubmissionDTO submissionDTO = createSimpleSubmissionDto(submission);

        doReturn(assignment).when(assignmentReadService).getOne(eq(assignment.getId()), eq(false));
        doReturn(submission).when(submissionMapper).toEntity(eq(submissionDTO));
        doReturn(submissionDTO).when(submissionMapper).toDto(eq(submission));
        doReturn(submission).when(submissionRepository).save(eq(submission));

        SubmissionDTO createdSubmission = submissionService.create(submissionDTO);
        assertThat(createdSubmission.getValue()).isEqualTo(submission.getValue());
        assertThat(createdSubmission.isResubmittable()).isEqualTo(submission.isResubmittable());
        assertThat(createdSubmission.getSubmittedForGradingDate()).isEqualTo(submission.getSubmittedForGradingDate());
    }

    @Test
    @WithMockCustomUser(authorities = {STUDENT})
    void shouldCreateWithGroup() {
        Journey journey = createJourney().creator(createTeacher());
        Milestone milestone = createMilestone().journey(journey);
        User student = createStudent().setId(1L);
        User student2 = createStudent()
            .setId(2L)
            .setEmail("student2@netgroup.com")
            .setLogin("student");
        Group group = createGroup()
            .setId(1L)
            .setStudents(Set.of(student, student2));
        Assignment assignment = createAssignment()
            .id(1L)
            .milestone(milestone)
            .setGroups(Set.of(group));
        Submission submission = createSubmission()
            .authors(Set.of(student, student2))
            .group(group)
            .assignment(assignment);
        SubmissionDTO submissionDTO = createSimpleSubmissionDto(submission);

        doReturn(assignment).when(assignmentReadService).getOne(eq(assignment.getId()), eq(false));
        doReturn(submission).when(submissionMapper).toEntity(eq(submissionDTO));
        doReturn(submissionDTO).when(submissionMapper).toDto(eq(submission));
        doReturn(submission).when(submissionRepository).save(eq(submission));
        doReturn(Optional.of(group)).when(groupService).findOne(eq(submission.getGroup().getId()));
        doReturn(List.of(mapUserToDetailedDTO(student), mapUserToDetailedDTO(student2))).when(userDetailedMapper).usersToUserDTOs(or(eq(List.of(student, student2)), eq(List.of(student2, student))));

        SubmissionDTO createdSubmission = submissionService.create(submissionDTO);
        assertThat(createdSubmission.getGroupId()).isEqualTo(group.getId());
    }

    @Test
    @WithMockCustomUser(authorities = {ADMIN})
    void shouldUpdate() {
        SubmissionDTO submissionDTO = new SubmissionDTO();
        submissionDTO.setId(1L);
        submissionDTO.setResubmittable(true);

        User student = createStudent();
        Submission existingSubmission = createSubmission()
            .id(1L)
            .resubmittable(false);
        Journey journey = createJourney().creator(createTeacher());
        Milestone milestone = createMilestone().journey(journey);
        Assignment assignment = createAssignment()
            .milestone(milestone);
        Submission submission = createSubmission()
            .authors(Set.of(student))
            .resubmittable(true)
            .assignment(assignment);

        doReturn(submission).when(submissionMapper).toEntity(eq(submissionDTO));
        doReturn(submissionDTO).when(submissionMapper).toDto(eq(submission));
        doReturn(Optional.of(submission)).when(submissionRepository).findOne(eq(existingSubmission.getId()));
        doReturn(submission).when(submissionRepository).save(eq(submission));
        ArgumentCaptor<Notification> captor = forClass(Notification.class);
        doReturn(new Notification()).when(notificationService).save(captor.capture());

        SubmissionDTO updated = submissionService.update(submissionDTO);
        assertThat(updated.isResubmittable()).isTrue();
    }

    private SubmissionDTO createSimpleSubmissionDto(Submission submission) {
        SubmissionDTO submissionDTO = new SubmissionDTO();
        submissionDTO.setValue(submission.getValue());
        submissionDTO.setResubmittable(submission.isResubmittable());
        submissionDTO.setSubmittedForGradingDate(submission.getSubmittedForGradingDate());
        submissionDTO.setAssignmentId(1L);
        if (submission.getGroup() != null) {
            submissionDTO.setGroupId(submission.getGroup().getId());
        }

        Set<UserDTO> authors = submission.getAuthors().stream().map(TestUtils::mapUserToDTO).collect(toSet());
        submissionDTO.setAuthors(authors);

        return submissionDTO;
    }
}
