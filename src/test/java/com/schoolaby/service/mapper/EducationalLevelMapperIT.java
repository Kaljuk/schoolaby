package com.schoolaby.service.mapper;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.EducationalLevel;
import com.schoolaby.service.dto.EducationalLevelDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.schoolaby.common.TestObjects.createEducationalLevel;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class EducationalLevelMapperIT extends BaseIntegrationTest {

    @Autowired
    private EducationalLevelMapper educationalLevelMapper;

    @Test
    void shouldSortById() {
        Set<EducationalLevel> unMappedLevels = Set.of(
            createEducationalLevel().setId(3L),
            createEducationalLevel().setId(1L),
            createEducationalLevel().setId(2L)
        );

        List<EducationalLevelDTO> mappedLevels = new ArrayList<>(educationalLevelMapper.toDto(unMappedLevels));

        assertEquals(1L, mappedLevels.get(0).getId());
        assertEquals(2L, mappedLevels.get(1).getId());
        assertEquals(3L, mappedLevels.get(2).getId());
    }
}
