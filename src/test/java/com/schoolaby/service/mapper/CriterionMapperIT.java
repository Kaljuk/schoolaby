package com.schoolaby.service.mapper;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.Criterion;
import com.schoolaby.domain.CriterionLevel;
import com.schoolaby.domain.Rubric;
import com.schoolaby.service.dto.CriterionDTO;
import com.schoolaby.service.dto.CriterionLevelDTO;
import com.schoolaby.service.dto.RubricDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.schoolaby.common.TestObjects.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CriterionMapperIT extends BaseIntegrationTest {

    @Autowired
    private CriterionMapper criterionMapper;

    @Test
    void shouldSetCriterionLevels() {
        Set<CriterionLevelDTO> result = criterionMapper.setLevels(Set.of(
            createCriterionLevel().setSequenceNumber(7L),
            createCriterionLevel().setSequenceNumber(22L),
            createCriterionLevel().setSequenceNumber(5L)
        ));

        List<CriterionLevelDTO> resultList = new ArrayList<>(result);

        assertEquals(5L, resultList.get(0).getSequenceNumber());
        assertEquals(7L, resultList.get(1).getSequenceNumber());
        assertEquals(22L, resultList.get(2).getSequenceNumber());
    }

    @Test
    void shouldSetMinAndMaxPoints() {
        Set<CriterionLevel> criterionLevels = Set.of(
            createCriterionLevel().setPoints(5L),
            createCriterionLevel().setPoints(8L),
            createCriterionLevel().setPoints(2L)
        );
        Criterion criterion = createCriterion().setLevels(criterionLevels);

        CriterionDTO criterionDTO = criterionMapper.toDto(criterion);

        assertEquals(8L, criterionDTO.getMaxPoints());
        assertEquals(2L, criterionDTO.getMinPoints());
    }
}
