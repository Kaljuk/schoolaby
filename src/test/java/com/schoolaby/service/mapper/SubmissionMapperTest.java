package com.schoolaby.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SubmissionMapperTest {
    private SubmissionMapper submissionMapper;

    @BeforeEach
    public void setUp() {
        submissionMapper = new SubmissionMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(submissionMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(submissionMapper.fromId(null)).isNull();
    }
}
