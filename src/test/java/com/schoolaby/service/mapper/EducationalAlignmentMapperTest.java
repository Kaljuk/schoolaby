package com.schoolaby.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EducationalAlignmentMapperTest {
    private EducationalAlignmentMapper educationalAlignmentMapper;

    @BeforeEach
    public void setUp() {
        educationalAlignmentMapper = new EducationalAlignmentMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(educationalAlignmentMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(educationalAlignmentMapper.fromId(null)).isNull();
    }
}
