package com.schoolaby.service.mapper;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.*;
import com.schoolaby.service.dto.JourneyDTO;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Set;

import static com.schoolaby.common.TestCases.getStudent;
import static com.schoolaby.common.TestCases.getTeacher;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static java.time.Instant.now;
import static java.time.ZoneOffset.UTC;
import static java.time.temporal.ChronoUnit.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class JourneyMapperIT extends BaseIntegrationTest {

    private final String CREATOR_FIRST_NAME = "John";
    private final String CREATOR_LAST_NAME = "Doe";
    private final String STUDENT_FIRST_NAME = "Anna";
    private final String STUDENT_LAST_NAME = "Karenina";
    private final Instant DEADLINE = now().plusSeconds(3600).truncatedTo(MILLIS);

    @Autowired
    private JourneyMapper journeyMapperImpl;

    @Test
    void testEntityFromId() {
        Long id = 1L;
        assertThat(journeyMapperImpl.fromId(id).getId()).isEqualTo(id);
        assertThat(journeyMapperImpl.fromId(null)).isNull();
    }

    @Test
    @WithMockCustomUser(authorities = TEACHER)
    void testToDtoTeacherName() {
        JourneyDTO journeyDTO = journeyMapperImpl.toDto(createJourney());
        assertThat(journeyDTO.getTeacherName()).isEqualTo("John Doe");
    }

    @Test
    @WithMockCustomUser(authorities = TEACHER)
    void shouldMapProgress() {
        Journey journey = createJourney();
        JourneyDTO journeyDTO = journeyMapperImpl.toDto(journey);
        assertThat(journeyDTO.getProgress()).isZero();

        Assignment assignment = journey.getMilestones().stream().findFirst().orElseThrow()
            .getAssignments().stream().findFirst().orElseThrow();
        assignment
            .getSubmissions().stream().findFirst().orElseThrow()
            .addSubmissionFeedback(new SubmissionFeedback().grade("5"));
        assignment.calculateStates();

        journeyDTO = journeyMapperImpl.toDto(journey);
        assertThat(journeyDTO.getProgress()).isEqualTo(100);
    }

    @Test
    void shouldMapJourneyPeriod() {
        Journey journey = createJourney();
        journey.startDate(createJourneyDate().minus(6, DAYS).toInstant());
        journey.endDate(createJourneyDate().plus(3, MONTHS).toInstant());

        assertEquals(6, journeyMapperImpl.mapDaysCount(journey));
        assertEquals(3, journeyMapperImpl.mapMonthsCount(journey));
        assertEquals(0, journeyMapperImpl.mapYearsCount(journey));

        journey.startDate(createJourneyDate().minus(5, DAYS).toInstant());
        journey.endDate(createJourneyDate().plus(2, YEARS).toInstant());

        assertEquals(5, journeyMapperImpl.mapDaysCount(journey));
        assertEquals(0, journeyMapperImpl.mapMonthsCount(journey));
        assertEquals(2, journeyMapperImpl.mapYearsCount(journey));
    }

    @Test
    void shouldCountAssignments() {
        Journey journey = createJourney();
        Milestone milestone = createMilestone();
        Assignment assignment = createAssignment();
        milestone.addAssignment(assignment);
        journey.addMilestone(milestone);

        assertEquals(2, journeyMapperImpl.mapAssignmentsCount(journey));
    }

    @Test
    @WithMockCustomUser(authorities = TEACHER, userId = "-2")
    void shouldMapAllSignupCodes() {
        Journey journey = createJourney();
        Milestone milestone = createMilestone();
        Assignment assignment = createAssignment();
        User teacher = getTeacher(entityManager);
        JourneyTeacher journeyTeacher = createJourneyTeacher()
            .user(teacher)
            .journey(journey);
        milestone.addAssignment(assignment);
        journey.addMilestone(milestone);
        journey.setJourneySignupCodes(Set.of(
            new JourneySignupCode()
                .authority(new Authority().name(TEACHER))
                .signUpCode("teacherSignupCode"),
            new JourneySignupCode()
                .authority(new Authority().name(STUDENT))
                .signUpCode("studentSignupCode")
        ));
        journey.teachers(Set.of(journeyTeacher));
        journey.creator(teacher);

        assertEquals("studentSignupCode", journeyMapperImpl.mapStudentSignupCode(journey));
        assertEquals("teacherSignupCode", journeyMapperImpl.mapTeacherSignupCode(journey));
    }

    @Test
    @WithMockCustomUser(authorities = TEACHER, userId = "-2")
    void shouldOnlyMapStudentSignupCodeWhenJourneyTeacherIsNotCreator() {
        Journey journey = createJourney();
        Milestone milestone = createMilestone();
        Assignment assignment = createAssignment();
        User teacher = getTeacher(entityManager);
        JourneyTeacher journeyTeacher = createJourneyTeacher()
            .user(teacher)
            .journey(journey);
        milestone.addAssignment(assignment);
        journey.addMilestone(milestone);
        journey.setJourneySignupCodes(Set.of(
            new JourneySignupCode()
                .authority(new Authority().name(TEACHER))
                .signUpCode("teacherSignupCode"),
            new JourneySignupCode()
                .authority(new Authority().name(STUDENT))
                .signUpCode("studentSignupCode")
        ));
        journey.addStudent(getStudent(entityManager));
        journey.teachers(Set.of(journeyTeacher));
        journey.creator(getStudent(entityManager));

        assertEquals("studentSignupCode", journeyMapperImpl.mapStudentSignupCode(journey));
        assertNull(journeyMapperImpl.mapTeacherSignupCode(journey));
    }

    @Test
    @WithMockCustomUser(authorities = STUDENT, userId = "-3")
    void shouldNotMapAnySignupCodesInStudentRole() {
        Journey journey = createJourney();
        Milestone milestone = createMilestone();
        Assignment assignment = createAssignment();
        milestone.addAssignment(assignment);
        journey.addMilestone(milestone);
        journey.setJourneySignupCodes(Set.of(
            new JourneySignupCode()
                .authority(new Authority().name(TEACHER))
                .signUpCode("teacherSignupCode"),
            new JourneySignupCode()
                .authority(new Authority().name(STUDENT))
                .signUpCode("studentSignupCode")
        ));
        journey.addStudent(getStudent(entityManager));

        assertNull(journeyMapperImpl.mapStudentSignupCode(journey));
        assertNull(journeyMapperImpl.mapTeacherSignupCode(journey));
    }

    private Journey createJourney() {
        User user = new User();
        user.setId(2L);
        user.setFirstName(CREATOR_FIRST_NAME);
        user.setLastName(CREATOR_LAST_NAME);

        User student = new User();
        student.setId(-1L);
        student.setFirstName(STUDENT_FIRST_NAME);
        student.setLastName(STUDENT_LAST_NAME);

        Journey journey = new Journey();
        journey.setId(1L);
        journey.setStartDate(Instant.now().plusSeconds(200));
        journey.setEndDate(Instant.now().plus(10, DAYS));
        journey.setCreator(user);
        journey.addTeacher(user);
        journey.addStudent(student);

        Milestone milestone = new Milestone();
        milestone.setJourney(journey);
        journey.addMilestone(milestone);

        Assignment assignment = new Assignment();
        assignment.id(1L);
        assignment.requiresSubmission(true);
        assignment.setMilestone(milestone);
        assignment.setDeadline(DEADLINE);
        assignment.setGradingScheme(
            new GradingScheme()
                .addValue(
                    new GradingSchemeValue()
                        .setGrade("5")
                        .setPercentageRange(100)));
        milestone.addAssignment(assignment);

        Submission submission = new Submission();
        submission.setAssignment(assignment);
        submission.addAuthor(student);
        submission.submittedForGradingDate(DEADLINE);
        assignment.addSubmission(submission);

        return journey;
    }

    private ZonedDateTime createJourneyDate() {
        return ZonedDateTime.of(2022, 6, 15, 12, 0, 0, 0, UTC);
    }
}
