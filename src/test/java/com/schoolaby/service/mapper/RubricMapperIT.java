package com.schoolaby.service.mapper;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.Criterion;
import com.schoolaby.domain.CriterionLevel;
import com.schoolaby.domain.Rubric;
import com.schoolaby.service.dto.CriterionDTO;
import com.schoolaby.service.dto.RubricDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.schoolaby.common.TestObjects.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RubricMapperIT extends BaseIntegrationTest {

    @Autowired
    private RubricMapper rubricMapper;

    @Test
    void shouldSetCriterions() {
        Set<CriterionDTO> result = rubricMapper.setCriterions(Set.of(
            createCriterion().setSequenceNumber(1L),
            createCriterion().setSequenceNumber(15L),
            createCriterion().setSequenceNumber(8L)
        ));

        List<CriterionDTO> resultList = new ArrayList<>(result);

        assertEquals(1L, resultList.get(0).getSequenceNumber());
        assertEquals(8L, resultList.get(1).getSequenceNumber());
        assertEquals(15L, resultList.get(2).getSequenceNumber());
    }

    @Test
    void shouldSetMinAndMaxPoints() {
        Set<Criterion> criteria = Set.of(
            createCriterion().setLevels(getCriterionLevels()),
            createCriterion().setLevels(getCriterionLevels())
        );
        Rubric rubric = createRubric().setCriterions(criteria);

        RubricDTO rubricDTO = rubricMapper.toDto(rubric);

        assertEquals(10L, rubricDTO.getMaxPoints());
        assertEquals(4L, rubricDTO.getMinPoints());
    }

    private Set<CriterionLevel> getCriterionLevels() {
        return Set.of(
            createCriterionLevel().setPoints(5L),
            createCriterionLevel().setPoints(2L)
        );
    }
}
