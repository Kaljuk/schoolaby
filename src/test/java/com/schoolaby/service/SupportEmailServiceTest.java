package com.schoolaby.service;

import com.schoolaby.domain.User;
import com.schoolaby.service.dto.SupportEmailDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

import static com.schoolaby.common.TestCases.createTeacher;
import static com.schoolaby.security.SecurityUtils.createSpringSecurityUser;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
public class SupportEmailServiceTest {
    @Mock
    private UserService userService;
    @Mock
    private MailService mailService;
    @Mock
    private Authentication authentication;
    @Mock
    private SecurityContext securityContext;

    @InjectMocks
    private SupportEmailService supportEmailService;

    @Test
    void shouldSendSupportEmail() {
        User user = createTeacher().setId(1L);
        com.schoolaby.security.User securityUser = createSpringSecurityUser(user);
        SupportEmailDTO supportEmailDTO = new SupportEmailDTO()
            .setSubject("Support email subject")
            .setContent("Support email content");

        doReturn(authentication).when(securityContext).getAuthentication();
        SecurityContextHolder.setContext(securityContext);
        doReturn(securityUser).when(authentication).getPrincipal();
        doReturn(user).when(userService).getOneById(eq(user.getId()));

        supportEmailService.sendSupportEmail(supportEmailDTO);
        verify(mailService).sendSupportEmail(eq(supportEmailDTO), eq(user));
    }

}
