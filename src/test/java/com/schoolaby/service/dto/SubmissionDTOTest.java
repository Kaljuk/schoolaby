package com.schoolaby.service.dto;

import com.schoolaby.service.dto.submission.SubmissionDTO;
import com.schoolaby.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SubmissionDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubmissionDTO.class);
        SubmissionDTO submissionDTO1 = new SubmissionDTO();
        submissionDTO1.setId(1L);
        SubmissionDTO submissionDTO2 = new SubmissionDTO();
        assertThat(submissionDTO1).isNotEqualTo(submissionDTO2);
        submissionDTO2.setId(submissionDTO1.getId());
        assertThat(submissionDTO1).isEqualTo(submissionDTO2);
        submissionDTO2.setId(2L);
        assertThat(submissionDTO1).isNotEqualTo(submissionDTO2);
        submissionDTO1.setId(null);
        assertThat(submissionDTO1).isNotEqualTo(submissionDTO2);
    }
}
