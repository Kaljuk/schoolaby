package com.schoolaby.service.dto;

import com.schoolaby.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UploadedFileDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UploadedFileDTO.class);
        UploadedFileDTO uploadedFileDTO1 = new UploadedFileDTO();
        uploadedFileDTO1.setId(1L);
        UploadedFileDTO uploadedFileDTO2 = new UploadedFileDTO();
        assertThat(uploadedFileDTO1).isNotEqualTo(uploadedFileDTO2);
        uploadedFileDTO2.setId(uploadedFileDTO1.getId());
        assertThat(uploadedFileDTO1).isEqualTo(uploadedFileDTO2);
        uploadedFileDTO2.setId(2L);
        assertThat(uploadedFileDTO1).isNotEqualTo(uploadedFileDTO2);
        uploadedFileDTO1.setId(null);
        assertThat(uploadedFileDTO1).isNotEqualTo(uploadedFileDTO2);
    }
}
