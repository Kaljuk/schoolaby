package com.schoolaby.service.dto;

import com.schoolaby.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GradingSchemeDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(GradingSchemeDTO.class);
        GradingSchemeDTO gradingSchemeDTO1 = new GradingSchemeDTO();
        gradingSchemeDTO1.setId(1L);
        GradingSchemeDTO gradingSchemeDTO2 = new GradingSchemeDTO();
        assertThat(gradingSchemeDTO1).isNotEqualTo(gradingSchemeDTO2);
        gradingSchemeDTO2.setId(gradingSchemeDTO1.getId());
        assertThat(gradingSchemeDTO1).isEqualTo(gradingSchemeDTO2);
        gradingSchemeDTO2.setId(2L);
        assertThat(gradingSchemeDTO1).isNotEqualTo(gradingSchemeDTO2);
        gradingSchemeDTO1.setId(null);
        assertThat(gradingSchemeDTO1).isNotEqualTo(gradingSchemeDTO2);
    }
}
