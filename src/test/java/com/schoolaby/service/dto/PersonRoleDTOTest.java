package com.schoolaby.service.dto;

import com.schoolaby.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PersonRoleDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PersonRoleDTO.class);
        PersonRoleDTO personRoleDTO1 = new PersonRoleDTO();
        personRoleDTO1.setId(1L);
        PersonRoleDTO personRoleDTO2 = new PersonRoleDTO();
        assertThat(personRoleDTO1).isNotEqualTo(personRoleDTO2);
        personRoleDTO2.setId(personRoleDTO1.getId());
        assertThat(personRoleDTO1).isEqualTo(personRoleDTO2);
        personRoleDTO2.setId(2L);
        assertThat(personRoleDTO1).isNotEqualTo(personRoleDTO2);
        personRoleDTO1.setId(null);
        assertThat(personRoleDTO1).isNotEqualTo(personRoleDTO2);
    }
}
