package com.schoolaby.service.dto;

import com.schoolaby.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EducationalAlignmentDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EducationalAlignmentDTO.class);
        EducationalAlignmentDTO educationalAlignmentDTO1 = new EducationalAlignmentDTO();
        educationalAlignmentDTO1.setId(1L);
        EducationalAlignmentDTO educationalAlignmentDTO2 = new EducationalAlignmentDTO();
        assertThat(educationalAlignmentDTO1).isNotEqualTo(educationalAlignmentDTO2);
        educationalAlignmentDTO2.setId(educationalAlignmentDTO1.getId());
        assertThat(educationalAlignmentDTO1).isEqualTo(educationalAlignmentDTO2);
        educationalAlignmentDTO2.setId(2L);
        assertThat(educationalAlignmentDTO1).isNotEqualTo(educationalAlignmentDTO2);
        educationalAlignmentDTO1.setId(null);
        assertThat(educationalAlignmentDTO1).isNotEqualTo(educationalAlignmentDTO2);
    }
}
