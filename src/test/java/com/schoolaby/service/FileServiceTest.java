package com.schoolaby.service;

import com.schoolaby.repository.UploadedFileRepository;
import com.schoolaby.service.storage.StorageService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class FileServiceTest {
    @Mock
    private StorageService storageService;
    @Mock
    private UploadedFileRepository uploadedFileRepository;
    @Spy
    @InjectMocks
    private FileService fileService;

    @Test
    void shouldUploadTxtFile() throws IOException {
        MockMultipartFile multipartFile = new MockMultipartFile("file", "user-file.txt", "text/plain", "test data".getBytes());
        ArgumentCaptor<String> uniqueNameArgumentCaptor = ArgumentCaptor.forClass(String.class);

        fileService.upload(multipartFile);
        verify(storageService).save(eq(multipartFile), uniqueNameArgumentCaptor.capture());
        verify(uploadedFileRepository).save(argThat(uploadedFile ->
            uploadedFile.getOriginalName().equals("user-file.txt") && uploadedFile.getExtension().equals("txt")));
    }

    @Test
    void shouldRemoveDisallowedCharactersFromUniqueFilename() throws IOException {
        MockMultipartFile multipartFile = new MockMultipartFile("file", "&$@=;:+,?u#s\\e{r^-}f%i`l\"e]>[~<|.txt", "text/plain", "test data".getBytes());
        ArgumentCaptor<String> uniqueNameArgumentCaptor = ArgumentCaptor.forClass(String.class);

        fileService.upload(multipartFile);
        verify(storageService).save(eq(multipartFile), uniqueNameArgumentCaptor.capture());
        verify(uploadedFileRepository).save(argThat(uploadedFile ->
            uploadedFile.getUniqueName().contains(".user-file.txt") && uploadedFile.getExtension().equals("txt")));
    }
}
