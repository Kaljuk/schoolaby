package com.schoolaby.service;

import com.schoolaby.domain.School;
import com.schoolaby.repository.SchoolRepository;
import com.schoolaby.security.harid.HarIdUser;
import com.schoolaby.service.dto.EkoolSchoolsDTO;
import com.schoolaby.service.dto.SchoolDTO;
import com.schoolaby.service.mapper.SchoolMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.common.TestUtils.mapSchoolToDTO;
import static com.schoolaby.domain.enumeration.Country.ESTONIA;
import static com.schoolaby.domain.enumeration.Country.TANZANIA;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class SchoolServiceTest {
    @Mock
    private SchoolRepository schoolRepository;
    @Mock
    private SchoolMapper schoolMapper;

    @InjectMocks
    private SchoolService schoolService;

    @Test
    void shouldFindSchoolsBySearch() {
        String oneResultSearch = "test";
        String noResultSearch = "nope";
        String country = ESTONIA.getValue();
        School school = createSchool()
            .id(1L)
            .uploadedFile(createUploadedFile());
        SchoolDTO schoolDTO = new SchoolDTO()
            .setId(school.getId())
            .setEhisId(school.getEhisId())
            .setRegNr(school.getRegNr())
            .setName(school.getName());

        doReturn(Set.of(school)).when(schoolRepository).findAllByNameContainingIgnoreCaseAndCountry(eq(oneResultSearch), eq(country));
        doReturn(new HashSet<SchoolDTO>()).when(schoolRepository).findAllByNameContainingIgnoreCaseAndCountry(eq(noResultSearch), eq(country));
        doReturn(Set.of(schoolDTO)).when(schoolMapper).toDto(eq(Set.of(school)));
        doReturn(new HashSet<SchoolDTO>()).when(schoolMapper).toDto(eq(new HashSet<>()));

        Set<SchoolDTO> result = schoolService.findSchoolsBySearch(oneResultSearch, country);
        Set<SchoolDTO> emptyResult = schoolService.findSchoolsBySearch(noResultSearch, country);

        assertEquals(Set.of(schoolDTO), result);
        assertEquals(new HashSet<SchoolDTO>(), emptyResult);
    }

    @Test
    void shouldFindSelectedCountrySchools() {
        String search = "school";
        School estonianSchool = createSchool()
            .id(1L)
            .name("Estonian school");
        School tanzanianSchool = createSchool()
            .id(2L)
            .name("Tanzanian school")
            .country(TANZANIA.getValue());
        SchoolDTO estonianSchoolDTO = mapSchoolToDTO(estonianSchool);
        SchoolDTO tanzanianSchoolDTO = mapSchoolToDTO(tanzanianSchool);

        doReturn(Set.of(estonianSchool)).when(schoolRepository).findAllByNameContainingIgnoreCaseAndCountry(eq(search), eq(estonianSchool.getCountry()));
        doReturn(Set.of(estonianSchoolDTO)).when(schoolMapper).toDto(eq(Set.of(estonianSchool)));
        Set<SchoolDTO> estonianSchoolsResult = schoolService.findSchoolsBySearch(search, estonianSchool.getCountry());

        doReturn(Set.of(tanzanianSchool)).when(schoolRepository).findAllByNameContainingIgnoreCaseAndCountry(eq(search), eq(tanzanianSchool.getCountry()));
        doReturn(Set.of(tanzanianSchoolDTO)).when(schoolMapper).toDto(eq(Set.of(tanzanianSchool)));
        Set<SchoolDTO> tanzanianSchoolsResult = schoolService.findSchoolsBySearch(search, tanzanianSchool.getCountry());

        assertEquals(Set.of(estonianSchoolDTO), estonianSchoolsResult);
        assertEquals(Set.of(tanzanianSchoolDTO), tanzanianSchoolsResult);
    }

    @Test
    void shouldNotSaveDuplicateHaridSchools() {
        String schoolName = "test school";
        HarIdUser.Role role1 = new HarIdUser.Role();
        role1.setProviderName(schoolName);
        HarIdUser.Role role2 = new HarIdUser.Role();
        role2.setProviderName(schoolName);
        School school = new School().name(role1.getProviderName());

        schoolService.saveHaridSchools(Set.of(role1, role2));

        verify(schoolRepository).saveAll(Set.of(school));
    }

    @Test
    void shouldSaveEkoolSchools() {
        EkoolSchoolsDTO.EkoolSchoolDTO ekoolSchoolDTO = new EkoolSchoolsDTO.EkoolSchoolDTO()
            .setName(SCHOOL_NAME);
        School school = new School().name(ekoolSchoolDTO.getName());

        schoolService.saveEkoolSchools(Set.of(ekoolSchoolDTO));

        verify(schoolRepository).saveAll(Set.of(school));
    }

    @Test
    void shouldUpdateSchool() {
        String schoolName = "Test school";
        School existingSchool = new School()
            .id(1L)
            .name(schoolName);
        HarIdUser.Role modifiedSchoolRole = new HarIdUser.Role();
        modifiedSchoolRole.setProviderEhisId("ehis_id");
        modifiedSchoolRole.setProviderRegNr("reg_nr");
        modifiedSchoolRole.setProviderName(schoolName);
        School expectedSchool = new School()
            .id(existingSchool.getId())
            .ehisId(modifiedSchoolRole.getProviderEhisId())
            .regNr(modifiedSchoolRole.getProviderRegNr())
            .name(schoolName);

        doReturn(Optional.of(existingSchool)).when(schoolRepository).findByRegNrOrEhisIdOrName(
            eq(modifiedSchoolRole.getProviderRegNr()),
            eq(modifiedSchoolRole.getProviderEhisId()),
            eq(modifiedSchoolRole.getProviderName())
        );

        schoolService.saveHaridSchools(Set.of(modifiedSchoolRole));

        verify(schoolRepository).saveAll(eq(Set.of(expectedSchool)));
    }

}
