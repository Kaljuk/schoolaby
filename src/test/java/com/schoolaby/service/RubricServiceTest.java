package com.schoolaby.service;

import com.schoolaby.domain.*;
import com.schoolaby.repository.RubricJpaRepository;
import com.schoolaby.service.dto.RubricDTO;
import com.schoolaby.service.mapper.RubricMapper;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.schoolaby.common.TestCases.createTeacher;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.security.SecurityUtils.createSpringSecurityUser;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class RubricServiceTest {
    public static final String RUBRIC_TITLE = "Test rubric";
    com.schoolaby.security.User securityUser = createSpringSecurityUser(createTeacher().setId(2L));

    @Spy
    private RubricJpaRepository rubricJpaRepository;
    @Mock
    private RubricMapper rubricMapper;
    @Mock
    private AssignmentReadService assignmentReadService;
    @Mock
    private Authentication authentication;
    @Mock
    private SecurityContext securityContext;

    @InjectMocks
    private RubricService rubricService;

    @Test
    @WithMockCustomUser(authorities = {TEACHER})
    void shouldFindTemplatesBySearch() {
        Set<Rubric> rubricRepositoryResponse = Set.of(
            new Rubric().setTitle(RUBRIC_TITLE).setIsTemplate(true).setId(1L)
        );
        when(rubricJpaRepository.findAllByIsTemplateTrueAndTitleContainingIgnoreCase(eq("Test")))
        .thenReturn(rubricRepositoryResponse);
        when(rubricMapper.toDto(eq(rubricRepositoryResponse))).thenAnswer(invocation -> List.of(
            new RubricDTO().setTitle(RUBRIC_TITLE).setIsTemplate(true).setId(1L)
        ));

        List<RubricDTO> result = new ArrayList<>(rubricService.findAllRubricTemplatesBySearch("Test"));

        assertEquals(1, result.size());
        assertEquals(1L, result.get(0).getId());
        assertEquals(RUBRIC_TITLE, result.get(0).getTitle());
        assertTrue(result.get(0).getIsTemplate());
    }

    @Test
    @WithMockCustomUser(authorities = {TEACHER})
    void shouldCreate() {
        Assignment assignment = createAssignmentWithReferences().id(1L);
        RubricDTO rubricDTO = new RubricDTO()
            .setTitle(RUBRIC_TITLE)
            .setIsTemplate(true)
            .setId(1L)
            .setAssignmentId(assignment.getId());
        Rubric rubric = new Rubric()
            .setTitle(RUBRIC_TITLE)
            .setIsTemplate(true)
            .setId(1L)
            .setAssignment(assignment);
        doReturn(authentication).when(securityContext).getAuthentication();
        SecurityContextHolder.setContext(securityContext);
        doReturn(securityUser).when(authentication).getPrincipal();
        doReturn(rubric).when(rubricMapper).toEntity(eq(rubricDTO));
        doReturn(rubricDTO).when(rubricMapper).toDto(eq(rubric));
        doReturn(rubric).when(rubricJpaRepository).save(eq(rubric));
        doReturn(assignment).when(assignmentReadService).getOne(eq(assignment.getId()), eq(false));

        RubricDTO result = rubricService.saveDTO(rubricDTO);

        assertEquals(1L, result.getId());
        assertEquals(RUBRIC_TITLE, result.getTitle());
        assertTrue(result.getIsTemplate());
    }

    @Test
    @WithMockCustomUser(authorities = {TEACHER})
    void shouldUpdate() {
        Assignment assignment = createAssignmentWithReferences().id(1L);
        RubricDTO rubricDTO = new RubricDTO()
            .setTitle("Edited title")
            .setIsTemplate(true)
            .setId(1L)
            .setAssignmentId(assignment.getId());
        Rubric existingRubric = new Rubric()
            .setTitle(RUBRIC_TITLE)
            .setIsTemplate(true)
            .setId(1L)
            .setAssignment(assignment);
        Rubric rubric = new Rubric()
            .setTitle("Edited title")
            .setIsTemplate(true)
            .setId(1L)
            .setAssignment(assignment);
        doReturn(authentication).when(securityContext).getAuthentication();
        SecurityContextHolder.setContext(securityContext);
        doReturn(securityUser).when(authentication).getPrincipal();
        doReturn(existingRubric).when(rubricMapper).toEntity(eq(rubricDTO));
        doReturn(rubricDTO).when(rubricMapper).toDto(eq(rubric));
        doReturn(rubric).when(rubricJpaRepository).save(eq(existingRubric));
        doReturn(assignment).when(assignmentReadService).getOne(eq(assignment.getId()), eq(false));


        RubricDTO result = rubricService.saveDTO(rubricDTO);

        assertEquals(1L, result.getId());
        assertEquals("Edited title", result.getTitle());
        assertTrue(result.getIsTemplate());
    }

    @Test
    @WithMockCustomUser(authorities = {TEACHER})
    void shouldDelete() {
        Assignment assignment = createAssignmentWithReferences().id(1L);
        Rubric rubric = createRubric()
            .setId(1L)
            .setAssignment(assignment);
        doReturn(authentication).when(securityContext).getAuthentication();
        SecurityContextHolder.setContext(securityContext);
        doReturn(securityUser).when(authentication).getPrincipal();
        doReturn(Optional.of(rubric)).when(rubricJpaRepository).findById(rubric.getId());

        rubricService.deleteById(1L);

        verify(rubricJpaRepository, times(1)).delete(rubric);
    }

    private Assignment createAssignmentWithReferences() {
        User teacher = createTeacher().id(2L);
        Journey journey = createJourney().addTeacher(teacher);
        Milestone milestone = createMilestone().journey(journey);

        return createAssignment().milestone(milestone);
    }
}
