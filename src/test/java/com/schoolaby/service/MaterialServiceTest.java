package com.schoolaby.service;

import com.schoolaby.domain.Material;
import com.schoolaby.repository.MaterialRepository;
import com.schoolaby.service.dto.MaterialDTO;
import com.schoolaby.service.mapper.MaterialMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.schoolaby.common.TestCases.TEACHER_LOGIN;
import static com.schoolaby.common.TestCases.createTeacher;
import static com.schoolaby.common.TestObjects.createMaterial;
import static com.schoolaby.security.SecurityUtils.createSpringSecurityUser;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.data.domain.Pageable.unpaged;

@ExtendWith(MockitoExtension.class)
class MaterialServiceTest {

    private static final String MATERIAL_TITLE = "Non restricted material";

    com.schoolaby.security.User securityUser = createSpringSecurityUser(createTeacher().setId(1L));

    @Mock
    private MaterialRepository materialRepository;
    @Mock
    private MaterialMapper materialMapper;
    @Mock
    private Authentication authentication;
    @Mock
    private SecurityContext securityContext;

    @InjectMocks
    private MaterialService materialService;

    @Test
    void findAllNonRestrictedByEducationalAlignments() {
        List<Long> educationalAlignmentIds = List.of(1L);
        Pageable pageable = unpaged();
        Material publicMaterial = createMaterial()
            .title(MATERIAL_TITLE)
            .restricted(false);
        MaterialDTO publicMaterialDTO = new MaterialDTO()
            .title(MATERIAL_TITLE)
            .restricted(false);

        doReturn(new PageImpl<>(List.of(publicMaterial))).when(materialRepository).findAllByRestrictedFalseAndEducationalAlignmentsIdIn(eq(educationalAlignmentIds), eq(pageable));
        doAnswer(invocation -> publicMaterialDTO).when(materialMapper).toDto(eq(publicMaterial));

        Page<MaterialDTO> result = materialService.findAllNonRestrictedByEducationalAlignments(educationalAlignmentIds, pageable);
        MaterialDTO resultMaterial = result.getContent().get(0);

        assertEquals(result.getTotalElements(), 1);
        assertEquals(MATERIAL_TITLE, resultMaterial.getTitle());
        assertEquals(false, resultMaterial.getRestricted());
        assertNull(resultMaterial.getSequenceNumber());
    }

    @Test
    void replaceDetachedWithManagedMaterials() {
        mockAuthentication();

        Long existingMaterialId = 1L;
        Material existingMaterial = createMaterial()
            .id(existingMaterialId)
            .createdBy(TEACHER_LOGIN)
            .title(MATERIAL_TITLE)
            .restricted(false);
        Material newMaterial = createMaterial()
            .title(MATERIAL_TITLE)
            .restricted(false);
        doReturn(List.of(existingMaterial)).when(materialRepository).findAllById(eq(List.of(existingMaterialId)));

        Set<Material> allMaterials = new HashSet<>();
        allMaterials.add(newMaterial);
        allMaterials.add(existingMaterial);
        materialService.replaceDetachedWithManagedMaterials(allMaterials);

        assertEquals(2, allMaterials.size());
    }

    @Test
    void shouldFindMaterialsAccordingToCountry() {
        String searchString = "material";
        String ukraineCountry = "Ukraine";
        Pageable pageable = unpaged();
        Material material = createMaterial().id(1L);
        Material ukrainianMaterial = createMaterial()
            .id(2L)
            .country(ukraineCountry)
            .title("Ukrainian material title");
        MaterialDTO materialDTO = new MaterialDTO()
            .id(material.getId())
            .title(material.getTitle())
            .country(material.getCountry());
        MaterialDTO ukrainianMaterialDTO = new MaterialDTO()
            .id(ukrainianMaterial.getId())
            .title(ukrainianMaterial.getTitle())
            .country(ukrainianMaterial.getCountry());

        mockAuthentication();
        doReturn(new PageImpl<>(List.of(material)))
            .when(materialRepository)
            .findAllBySearchString(eq(searchString), eq(securityUser.getUsername()), eq(pageable));
        doReturn(new PageImpl<>(List.of(ukrainianMaterial)))
            .when(materialRepository)
            .findAllBySearchAndCountry(eq(searchString), eq(securityUser.getUsername()), eq(ukraineCountry), eq(pageable));
        doAnswer(invocation -> materialDTO).when(materialMapper).toDto(eq(material));
        doAnswer(invocation -> ukrainianMaterialDTO).when(materialMapper).toDto(eq(ukrainianMaterial));

        Page<MaterialDTO> resultWithoutCountry = materialService.findAllBySearchAndCountry(searchString, null, pageable);
        Page<MaterialDTO> resultWithCountry = materialService.findAllBySearchAndCountry(searchString, ukraineCountry, pageable);
        MaterialDTO resultMaterialWithoutCountry = resultWithoutCountry.getContent().get(0);
        MaterialDTO resultMaterialWithCountry = resultWithCountry.getContent().get(0);

        assertEquals(1, resultWithoutCountry.getTotalElements());
        assertEquals(material.getId(), resultMaterialWithoutCountry.getId());
        assertEquals(material.getTitle(), resultMaterialWithoutCountry.getTitle());
        assertEquals(material.getCountry(), resultMaterialWithoutCountry.getCountry());
        assertEquals(1, resultWithCountry.getTotalElements());
        assertEquals(ukrainianMaterialDTO.getId(), resultMaterialWithCountry.getId());
        assertEquals(ukrainianMaterialDTO.getTitle(), resultMaterialWithCountry.getTitle());
        assertEquals(ukrainianMaterialDTO.getCountry(), resultMaterialWithCountry.getCountry());
    }

    @Test
    void shouldNotAllowToUpdateMaterialWhenUserIsNotTheCreatorOfTheMaterial() {
        Material material = createMaterial().id(10L).createdBy("someTeacher");
        MaterialDTO materialDTO = new MaterialDTO().createdBy("someTeacher");

        mockAuthentication();
        doReturn(material).when(materialMapper).toEntity(eq(materialDTO));

        assertThrows(ResponseStatusException.class, () -> materialService.save(materialDTO));
    }

    @Test
    void shouldNotAllowToDeleteMaterialWhenUserIsNotTheCreatorOfTheMaterial() {
        Material material = createMaterial().id(10L).createdBy("someTeacher");

        mockAuthentication();
        doReturn(Optional.of(material)).when(materialRepository).findById(eq(material.getId()));

        assertThrows(ResponseStatusException.class, () -> materialService.delete(material.getId()));

    }

    private void mockAuthentication() {
        SecurityContextHolder.setContext(securityContext);
        doReturn(authentication).when(securityContext).getAuthentication();
        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(securityUser);
    }
}
