package com.schoolaby.service;

import com.schoolaby.domain.Journey;
import com.schoolaby.domain.JourneyStudent;
import com.schoolaby.domain.User;
import com.schoolaby.domain.keys.JourneyUserKey;
import com.schoolaby.repository.JourneyStudentJpaRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;

import java.time.Instant;
import java.util.Optional;

import static com.schoolaby.common.TestObjects.createJourney;
import static com.schoolaby.common.TestObjects.createUser;
import static org.mockito.Mockito.doReturn;
import static org.mockito.ArgumentMatchers.eq;

@ExtendWith(MockitoExtension.class)
public class JourneyStudentServiceTest {
    @Mock
    private JourneyStudentJpaRepository journeyStudentRepository;

    @InjectMocks
    private JourneyStudentService journeyStudentService;

    @Test
    void shouldPin() {
        User student = createUser().setId(2L);
        Journey journey = createJourney().id(1L);
        JourneyStudent journeyStudent = new JourneyStudent()
            .journey(journey)
            .user(student);
        JourneyUserKey journeyUserKey = new JourneyUserKey()
            .setJourney(journey.getId())
            .setUser(student.getId());

        doReturn(Optional.of(journeyStudent)).when(journeyStudentRepository).findById(eq(journeyUserKey));

        JourneyStudent receivedJourneyStudent = journeyStudentService.togglePin(journey.getId(), student.getId());

        assertNotNull(receivedJourneyStudent.getPinnedDate());
    }

    @Test
    void shouldUnPin() {
        User student = createUser().setId(2L);
        Journey journey = createJourney().id(1L);
        JourneyStudent journeyStudent = new JourneyStudent()
            .journey(journey)
            .user(student)
            .setPinnedDate(Instant.now());
        JourneyUserKey journeyUserKey = new JourneyUserKey()
            .setJourney(journey.getId())
            .setUser(student.getId());

        doReturn(Optional.of(journeyStudent)).when(journeyStudentRepository).findById(eq(journeyUserKey));

        JourneyStudent receivedJourneyStudent = journeyStudentService.togglePin(journey.getId(), student.getId());

        assertNull(receivedJourneyStudent.getPinnedDate());
    }
}
