package com.schoolaby.service;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.*;
import com.schoolaby.service.dto.JoinJourneyResultDTO;
import com.schoolaby.service.dto.JourneyDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;

import java.util.Map;
import java.util.Set;

import static com.schoolaby.common.TestCases.*;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static java.util.Collections.emptySet;
import static org.junit.jupiter.api.Assertions.*;

class JourneyServiceIT extends BaseIntegrationTest {
    @Autowired
    private JourneyService journeyService;

    private Journey journey;
    Submission submissionSchemeNumerical;
    Submission submissionSchemeNone;
    SubmissionFeedback submissionFeedbackSchemeNumerical;
    SubmissionFeedback submissionFeedbackSchemeNone;
    private User student;
    private User studentToDelete;
    private Journey journeyAfterStudentRemoval;

    @BeforeEach
    void initTest() {
        transactionHelper.withTransaction(() -> {
            student = getStudent(entityManager);
            User teacher = getTeacher(entityManager);
            journey = createJourney();
            Milestone milestone = createMilestone();
            Assignment assignment1 = createAssignment();
            Assignment assignment2 = createAssignment();
            submissionSchemeNumerical = createSubmission();
            submissionSchemeNone = createSubmission();

            journey
                .educationalLevel(createEducationalLevel())
                .addMilestone(milestone
                    .creator(teacher)
                    .addAssignment(assignment1
                        .creator(teacher)
                        .addStudent(student)
                        .gradingScheme(getGradingSchemeNumerical(entityManager))
                        .addSubmission(submissionSchemeNumerical.addAuthor(student)))
                    .addAssignment(assignment2
                        .creator(teacher)
                        .addStudent(student)
                        .gradingScheme(getGradingSchemeNone(entityManager))
                        .addSubmission(submissionSchemeNone.addAuthor(student)))
                )
                .creator(teacher);
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldReturnStudentsAverageGradesByJourneyId() {
        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            submissionFeedbackSchemeNumerical = createSubmissionFeedback()
                .submission(submissionSchemeNumerical)
                .student(student)
                .grade("5")
                .creator(teacher);
            submissionFeedbackSchemeNone = createSubmissionFeedback()
                .submission(submissionSchemeNone)
                .student(student)
                .grade("-")
                .creator(teacher);
            journey
                .addStudent(getStudent(entityManager))
                .addTeacher(teacher);
            persistCreatedEntities(entityManager);
        });

        Map<Long, Double> result = journeyService.getJourneyStudentsAverageGrade(journey.getId());

        assertEquals(1, result.size());
        assertTrue(result.containsKey(STUDENT_ID));
        assertEquals(90.0, result.get(STUDENT_ID));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldJoinAsTeacher() {
        transactionHelper.withTransaction(() -> {
            journey.addStudent(getStudent(entityManager));
            journey.setJourneySignupCodes(Set.of(
                new JourneySignupCode()
                    .authority(new Authority().name(TEACHER))
                    .signUpCode("teacherSignupCode")
                    .journey(journey)
            ));
            journey.teachers(emptySet());
            persistCreatedEntities(entityManager);
        });

        JoinJourneyResultDTO joinJourneyResultDTO = journeyService.join(new JoinJourneyDTO().signUpCode("teacherSignupCode"));
        JourneyDTO journeyDTO = journeyService.findOneDTO(joinJourneyResultDTO.getJourneyId(), false).orElseThrow();

        assertEquals(1, journeyDTO.getTeachers().size());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldJoinAsStudent() {
        transactionHelper.withTransaction(() -> {
            journey.setJourneySignupCodes(Set.of(
                new JourneySignupCode()
                    .authority(new Authority().name(STUDENT))
                    .signUpCode("studentSignupCode")
                    .journey(journey)
            ));
            journey
                .students(emptySet())
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        JoinJourneyResultDTO joinJourneyResultDTO = journeyService.join(new JoinJourneyDTO().signUpCode("studentSignupCode"));
        JourneyDTO journeyDTO = journeyService.findOneDTO(joinJourneyResultDTO.getJourneyId(), false).orElseThrow();

        assertEquals(1, journeyDTO.getStudents().size());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldJoinAsStudentWithOneTimeSignupCode() {
        transactionHelper.withTransaction(() -> {
            journey.setJourneySignupCodes(Set.of(
                new JourneySignupCode()
                    .authority(new Authority().name(STUDENT))
                    .signUpCode("studentSignupCode")
                    .journey(journey)
                    .setOneTime(true)
            ));
            journey
                .students(emptySet())
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        JoinJourneyResultDTO joinJourneyResultDTO = journeyService.join(new JoinJourneyDTO().signUpCode("studentSignupCode"));
        JourneyDTO journeyDTO = journeyService.findOneDTO(joinJourneyResultDTO.getJourneyId(), false).orElseThrow();

        assertEquals(1, journeyDTO.getStudents().size());

        JourneySignupCode oneTimeSignupCode = journeyService.getOne(journey.getId(), false).getJourneySignupCodes().stream()
            .filter(journeySignupCode -> journeySignupCode.getSignUpCode().equals("studentSignupCode"))
            .findFirst().orElseThrow();
        assertTrue(oneTimeSignupCode.isUsed());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldReturnStudentsAverageGradesByJourneyIdWhenZeroSubmissions() {
        transactionHelper.withTransaction(() -> {
            journey
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });
        Map<Long, Double> result = journeyService.getJourneyStudentsAverageGrade(journey.getId());

        assertEquals(1, result.size());
        assertTrue(result.containsKey(student.getId()));
        assertTrue(result.containsValue(0.0));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldRemoveStudentFromAssignmentWhenRemovingStudentFromJourney() {
        transactionHelper.withTransaction(() -> {
            studentToDelete = createStudent();
            journey
                .addStudent(getStudent(entityManager))
                .addStudent(studentToDelete)
                .addTeacher(getTeacher(entityManager))
                .getMilestones()
                .stream().findFirst().orElseThrow()
                .getAssignments().stream().findFirst().orElseThrow()
                .addStudent(studentToDelete);
            persistCreatedEntities(entityManager);
        });

        transactionHelper.withTransaction(() -> {
            journeyService.removeStudent(journey.getId(), studentToDelete.getId());
            journeyAfterStudentRemoval = journeyService.getOne(journey.getId(), false);
        });

        assertEquals(1, journeyAfterStudentRemoval.getStudents().size());
        assertNotEquals(studentToDelete.getId(), journey
            .getStudents()
            .stream()
            .findFirst()
            .orElseThrow()
            .getUser()
            .getId());

        Milestone milestoneAfterStudentRemoval = journeyAfterStudentRemoval
            .getMilestones()
            .stream()
            .findFirst()
            .orElseThrow();
        milestoneAfterStudentRemoval.getAssignments().forEach(assignment -> assertEquals(1, assignment.getStudents().size()));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotDeleteGroupFeedbackWhenRemovingStudentFromJourney() {
        transactionHelper.withTransaction(() -> {
            studentToDelete = createStudent();
            Group group = createGroup()
                .addStudent(studentToDelete)
                .addStudent(student);
            Submission submission = createSubmission()
                .addAuthor(studentToDelete)
                .addAuthor(student);
            journey
                .addStudent(studentToDelete)
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            journey.getMilestones()
                .stream().findFirst().orElseThrow()
                .getAssignments().stream().findFirst().orElseThrow()
                .addSubmission(submission
                    .group(group)
                    .addSubmissionFeedback(createSubmissionFeedback()
                        .group(group)
                        .submission(submission)
                        .creator(getTeacher(entityManager))
                    ))
                .addGroup(group);
            persistCreatedEntities(entityManager);
        });

        transactionHelper.withTransaction(() -> {
            journeyService.removeStudent(journey.getId(), studentToDelete.getId());
            journeyAfterStudentRemoval = journeyService.getOne(journey.getId(), false);
        });

        Assignment groupAssignment = journeyAfterStudentRemoval.getAssignments()
            .stream().filter(assignment -> assignment.getGroups().size() == 1).findFirst().orElseThrow();
        Submission groupSubmission = groupAssignment.getSubmissions()
            .stream().filter(submission -> submission.getGroup() != null).findFirst().orElseThrow();
        assertEquals(1, groupSubmission.getAuthors().size());
        assertEquals(1, groupSubmission.getSubmissionFeedbacks().size());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldIndividualFeedbackWhenRemovingStudentFromJourney() {
        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            studentToDelete = createStudent();
            Group group = createGroup()
                .addStudent(studentToDelete)
                .addStudent(student);
            Submission submission = createSubmission()
                .addAuthor(studentToDelete)
                .addAuthor(student);
            journey
                .addStudent(studentToDelete)
                .addStudent(getStudent(entityManager))
                .addTeacher(teacher);
            journey.getMilestones()
                .stream().findFirst().orElseThrow()
                .getAssignments().stream().findFirst().orElseThrow()
                .addSubmission(submission
                    .group(group)
                    .addSubmissionFeedback(createSubmissionFeedback()
                        .student(studentToDelete)
                        .submission(submission)
                        .creator(teacher)
                    ))
                .addGroup(group);
            persistCreatedEntities(entityManager);
        });

        transactionHelper.withTransaction(() -> {
            journeyService.removeStudent(journey.getId(), studentToDelete.getId());
            journeyAfterStudentRemoval = journeyService.getOne(journey.getId(), false);
        });

        Assignment groupAssignment = journeyAfterStudentRemoval.getAssignments()
            .stream().filter(assignment -> assignment.getGroups().size() == 1).findFirst().orElseThrow();
        Submission groupSubmission = groupAssignment.getSubmissions()
            .stream().filter(submission -> submission.getGroup() != null).findFirst().orElseThrow();
        assertEquals(1, groupSubmission.getAuthors().size());
        assertEquals(0, groupSubmission.getSubmissionFeedbacks().size());
    }
}
