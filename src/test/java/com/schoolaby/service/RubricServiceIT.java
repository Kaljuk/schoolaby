package com.schoolaby.service;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.*;
import com.schoolaby.service.dto.RubricDTO;
import com.schoolaby.service.mapper.RubricMapper;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;

import java.util.Set;

import static com.schoolaby.common.TestCases.getGradingSchemeNumerical;
import static com.schoolaby.common.TestCases.getTeacher;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static org.junit.jupiter.api.Assertions.*;

@WithMockCustomUser(authorities = {TEACHER})
public class RubricServiceIT extends BaseIntegrationTest {
    public static final String UPDATED_TITLE = "Updated title";
    public static final String SEARCH = "updated";

    @Autowired
    private RubricService rubricService;
    @Autowired
    private RubricMapper rubricMapper;

    @Test
    void shouldGetTemplatesBySearch() {
        createRubric()
            .setTitle(UPDATED_TITLE)
            .setIsTemplate(true);
        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));

        transactionHelper.withNewTransaction(() -> {
            Set<RubricDTO> rubricDTOs = rubricService.findAllRubricTemplatesBySearch(SEARCH);

            assertNotEquals(0, rubricDTOs.size());
            assertTrue(rubricDTOs.stream().anyMatch(rubricDTO -> rubricDTO.getTitle().equals(UPDATED_TITLE)));
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldCreate() {
        Assignment assignment = assignmentWithReferences();

        RubricDTO rubricDTO = rubricMapper
            .toDto(createRubric())
            .setAssignmentId(assignment.getId());

        transactionHelper.withNewTransaction(() -> {
            RubricDTO savedRubric = rubricService.saveDTO(rubricDTO);

            assertNotNull(savedRubric.getId());
            assertEquals(rubricDTO.getTitle(), savedRubric.getTitle());
            assertEquals(rubricDTO.getIsTemplate(), savedRubric.getIsTemplate());
            assertEquals(rubricDTO.getCriterions().size(), savedRubric.getCriterions().size());
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldUpdate() {
        Rubric rubric = createRubric();
        transactionHelper.withTransaction(() -> {
            rubric.setAssignment(assignmentWithReferences());
            persistCreatedEntities(entityManager);
        });
        RubricDTO rubricDTO = rubricMapper.toDto(rubric.setTitle(UPDATED_TITLE));

        transactionHelper.withNewTransaction(() -> {
            RubricDTO editedRubric = rubricService.updateRubric(rubricDTO);
            assertEquals(rubricDTO.getId(), editedRubric.getId());
            assertEquals(rubricDTO.getTitle(), editedRubric.getTitle());
            assertEquals(rubricDTO.getIsTemplate(), editedRubric.getIsTemplate());
            assertEquals(rubricDTO.getCriterions().size(), editedRubric.getCriterions().size());
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldDelete() {
        Rubric rubric = createRubric();
        transactionHelper.withTransaction(() -> {
            rubric.setAssignment(assignmentWithReferences());
            persistCreatedEntities(entityManager);
        });

        assertNull(rubric.getDeleted());

        transactionHelper.withNewTransaction(() -> rubricService.deleteById(rubric.getId()));

        assertNotNull(entityManager.createNativeQuery("SELECT deleted FROM rubric WHERE id = ?").setParameter(1, rubric.getId()).getSingleResult());
    }


    private Assignment assignmentWithReferences() {
        EducationalLevel educationalLevel = createEducationalLevel();
        EducationalAlignment educationalAlignment = createEducationalAlignment();
        Journey journey = createJourney()
            .educationalLevel(educationalLevel);
        Milestone milestone = createMilestone()
            .journey(journey)
            .addEducationalAlignment(educationalAlignment);

        Assignment assignment = createAssignment()
            .milestone(milestone)
            .addEducationalAlignment(educationalAlignment)
            .gradingScheme(getGradingSchemeNumerical(entityManager));

        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            journey
                .addTeacher(teacher)
                .creator(teacher);
            milestone.creator(teacher);
            assignment.creator(teacher);

            persistCreatedEntities(entityManager);
        });

        return assignment;
    }
}
