package com.schoolaby.security.harid;

import com.schoolaby.service.SchoolService;
import com.schoolaby.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class HarIdServiceTest {
    @Mock
    private SchoolService schoolService;
    @Mock
    private UserService userService;
    @InjectMocks
    private HarIdService harIdService;

    @Test
    public void shouldSave() {
        String schoolName = "test school";
        HarIdUser.Role role1 = new HarIdUser.Role();
        role1.setProviderName(schoolName);
        HarIdUser.Role role2 = new HarIdUser.Role();
        role2.setProviderName(schoolName);

        HarIdUser harIdUser = new HarIdUser();
        harIdUser.setEmail("test@netgroup.com");
        harIdUser.setSub("1234567890");
        harIdUser.setRoles(Set.of(role1, role2));

        harIdService.saveOrUpdate(harIdUser);

        verify(schoolService).saveHaridSchools(eq(Set.of(role1, role2)));
        verify(userService).save(eq(harIdUser));
    }
}
