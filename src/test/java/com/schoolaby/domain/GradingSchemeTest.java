package com.schoolaby.domain;

import com.schoolaby.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

import static com.schoolaby.common.TestObjects.createGradingScheme;
import static com.schoolaby.common.TestObjects.createGradingSchemeValue;
import static java.util.Set.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GradingSchemeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(GradingScheme.class);
        GradingScheme gradingScheme1 = new GradingScheme();
        gradingScheme1.setId(1L);
        GradingScheme gradingScheme2 = new GradingScheme();
        gradingScheme2.setId(gradingScheme1.getId());
        assertThat(gradingScheme1).isEqualTo(gradingScheme2);
        gradingScheme2.setId(2L);
        assertThat(gradingScheme1).isNotEqualTo(gradingScheme2);
        gradingScheme1.setId(null);
        assertThat(gradingScheme1).isNotEqualTo(gradingScheme2);
    }

    @Test
    void verifyHasHighestGrade() {
        GradingScheme gradingScheme = createGradingScheme();
        GradingSchemeValue value1 = createGradingSchemeValue().setPercentageRange(90).setGrade("5");
        GradingSchemeValue value2 = createGradingSchemeValue().setPercentageRange(75).setGrade("4");
        GradingSchemeValue value3 = createGradingSchemeValue().setPercentageRange(50).setGrade("3");
        GradingSchemeValue value4 = createGradingSchemeValue().setPercentageRange(20).setGrade("2");
        gradingScheme.values(of(value1, value2, value3, value4));

        assertTrue(gradingScheme.isHighestGrade("5"));
    }

    @Test
    void verifyDoesNotHaveHighestGrade() {
        GradingScheme gradingScheme = createGradingScheme();
        GradingSchemeValue value1 = createGradingSchemeValue().setPercentageRange(90).setGrade("5");
        GradingSchemeValue value2 = createGradingSchemeValue().setPercentageRange(75).setGrade("4");
        GradingSchemeValue value3 = createGradingSchemeValue().setPercentageRange(50).setGrade("3");
        GradingSchemeValue value4 = createGradingSchemeValue().setPercentageRange(20).setGrade("2");
        gradingScheme.values(of(value1, value2, value3, value4));

        assertFalse(gradingScheme.isHighestGrade("4"));
        assertFalse(gradingScheme.isHighestGrade("3"));
        assertFalse(gradingScheme.isHighestGrade("2"));
    }
}
