package com.schoolaby.domain;

import com.schoolaby.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static com.schoolaby.common.TestObjects.createJourney;
import static com.schoolaby.common.TestObjects.createMilestone;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SubmissionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Submission.class);
        Submission submission1 = new Submission();
        submission1.setId(1L);
        Submission submission2 = new Submission();
        submission2.setId(submission1.getId());
        assertThat(submission1).isEqualTo(submission2);
        submission2.setId(2L);
        assertThat(submission1).isNotEqualTo(submission2);
        submission1.setId(null);
        assertThat(submission1).isNotEqualTo(submission2);
    }

    @Test
    void isCompletedTrueWhenFeedbackGradeIsPositive() {
        Submission submission = new Submission()
            .addSubmissionFeedback(new SubmissionFeedback()
                .grade("50")
            )
            .assignment(new Assignment()
                .gradingScheme(new GradingScheme()
                    .addValue(new GradingSchemeValue()
                        .setGrade("50")
                        .setPercentageRange(50))));

        assertTrue(submission.isCompleted());
    }

    @Test
    void isCompletedTrueWhenAllFeedbackGradesArePositive() {
        Submission submission = new Submission()
            .addSubmissionFeedback(new SubmissionFeedback()
                .grade("50")
            )
            .addSubmissionFeedback(new SubmissionFeedback()
                .grade("50")
            )
            .assignment(new Assignment()
                .gradingScheme(new GradingScheme()
                    .addValue(new GradingSchemeValue()
                        .setGrade("50")
                        .setPercentageRange(50))));

        assertTrue(submission.isCompleted());
    }

    @Test
    void isCompletedFalseWhenOneOfFeedbackGradesIsNegative() {
        Submission submission = new Submission()
            .addSubmissionFeedback(new SubmissionFeedback()
                .grade("50")
            )
            .addSubmissionFeedback(new SubmissionFeedback()
                .grade("49")
            )
            .assignment(new Assignment()
                .gradingScheme(new GradingScheme()
                    .addValue(new GradingSchemeValue()
                        .setGrade("50")
                        .setPercentageRange(50))));

        assertFalse(submission.isCompleted());
    }

    @Test
    void hasStudentCompletedSubmissionTrueWhenIndividualFeedbackIsPositive() {
        User student = new User().setId(1L);
        SubmissionFeedback submissionFeedback = new SubmissionFeedback()
            .student(student)
            .grade("50");
        Submission submission = new Submission()
            .addSubmissionFeedback(submissionFeedback)
            .assignment(new Assignment()
                .gradingScheme(new GradingScheme()
                    .addValue(new GradingSchemeValue()
                        .setGrade("50")
                        .setPercentageRange(50))));
        submissionFeedback.submission(submission);

        assertTrue(submission.hasStudentCompleted(student.getId()));
    }

    @Test
    void hasStudentCompletedSubmissionTrueWhenGroupFeedbackIsPositive() {
        User student = new User().setId(1L);
        SubmissionFeedback submissionFeedback = new SubmissionFeedback()
            .group(new Group())
            .grade("50");
        Submission submission = new Submission()
            .addSubmissionFeedback(submissionFeedback)
            .assignment(new Assignment()
                .gradingScheme(new GradingScheme()
                    .addValue(new GradingSchemeValue()
                        .setGrade("50")
                        .setPercentageRange(50))));
        submissionFeedback.submission(submission);

        assertTrue(submission.hasStudentCompleted(student.getId()));
    }

    @Test
    void hasStudentCompletedSubmissionFalseWhenGroupFeedbackIsPositiveAndIndividualFeedbackIsNegative() {
        User student = new User().setId(1L);
        SubmissionFeedback groupFeedback = new SubmissionFeedback()
            .group(new Group())
            .grade("50");
        SubmissionFeedback individualFeedback = new SubmissionFeedback()
            .student(student)
            .grade("49");
        Submission submission = new Submission()
            .addSubmissionFeedback(groupFeedback)
            .addSubmissionFeedback(individualFeedback)
            .assignment(new Assignment()
                .gradingScheme(new GradingScheme()
                    .addValue(new GradingSchemeValue()
                        .setGrade("50")
                        .setPercentageRange(50))));
        individualFeedback.submission(submission);

        assertFalse(submission.hasStudentCompleted(student.getId()));
    }


    @Test
    void isFailedTrueWhenFeedbackGradeIsNegativeAndNotResubmittable() {
        Submission submission = new Submission()
            .addSubmissionFeedback(new SubmissionFeedback()
                .grade("49")
            )
            .assignment(new Assignment()
                .gradingScheme(new GradingScheme()
                    .addValue(new GradingSchemeValue()
                        .setGrade("49")
                        .setPercentageRange(49))))
            .resubmittable(false);


        assertTrue(submission.isFailed());
    }

    @Test
    void hasStudentFailedTrueWhenIndividualFeedbackGradeIsNegativeAndNotResubmittable() {
        User student = new User().setId(1L);
        Journey journey = createJourney();
        Milestone milestone = createMilestone().journey(journey);
        SubmissionFeedback submissionFeedback = new SubmissionFeedback()
            .grade("49")
            .student(student);
        Submission submission = new Submission()
            .addSubmissionFeedback(submissionFeedback)
            .assignment(new Assignment()
                .milestone(milestone)
                .gradingScheme(new GradingScheme()
                    .addValue(new GradingSchemeValue()
                        .setGrade("49")
                        .setPercentageRange(49))))
            .resubmittable(false);
        submissionFeedback.submission(submission);

        assertTrue(submission.hasStudentFailed(student.getId()));
    }

    @Test
    void hasStudentFailedTrueWhenGroupFeedbackGradeIsNegativeAndNotResubmittable() {
        User student = new User().setId(1L);
        Journey journey = createJourney();
        Milestone milestone = createMilestone().journey(journey);
        SubmissionFeedback submissionFeedback = new SubmissionFeedback()
            .grade("49")
            .group(new Group());
        Submission submission = new Submission()
            .addSubmissionFeedback(submissionFeedback)
            .assignment(new Assignment()
                .milestone(milestone)
                .gradingScheme(new GradingScheme()
                    .addValue(new GradingSchemeValue()
                        .setGrade("49")
                        .setPercentageRange(49))))
            .resubmittable(false);
        submissionFeedback.submission(submission);

        assertTrue(submission.hasStudentFailed(student.getId()));
    }

    @Test
    void hasStudentFailedTrueWhenGroupFeedbackGradeIsPositiveAndIndividualNegativeAndNotResubmittable() {
        User student = new User().setId(1L);
        Journey journey = createJourney();
        Milestone milestone = createMilestone().journey(journey);
        SubmissionFeedback groupFeedback = new SubmissionFeedback()
            .grade("50")
            .group(new Group());
        SubmissionFeedback individualFeedback = new SubmissionFeedback()
            .grade("49")
            .student(student);
        Submission submission = new Submission()
            .addSubmissionFeedback(groupFeedback)
            .addSubmissionFeedback(individualFeedback)
            .assignment(new Assignment()
                .milestone(milestone)
                .gradingScheme(new GradingScheme()
                    .addValue(new GradingSchemeValue()
                        .setGrade("49")
                        .setPercentageRange(49))))
            .resubmittable(false);
        individualFeedback.submission(submission);

        assertTrue(submission.hasStudentFailed(student.getId()));
    }

    @Test
    void isRejectedTrueWhenGradeIsNegativeAndResubmittable() {
        Submission submission = new Submission()
            .addSubmissionFeedback(new SubmissionFeedback()
                .grade("49")
            )
            .assignment(new Assignment()
                .gradingScheme(new GradingScheme()
                    .addValue(new GradingSchemeValue()
                        .setGrade("49")
                        .setPercentageRange(49))))
            .resubmittable(true);

        assertTrue(submission.isRejected());
    }

    @Test
    void isStudentSubmissionRejectedTrueWhenIndividualFeedbackGradeIsNegativeAndResubmittable() {
        User student = new User().setId(1L);
        SubmissionFeedback submissionFeedback = new SubmissionFeedback()
            .grade("49")
            .student(student);
        Submission submission = new Submission()
            .addSubmissionFeedback(submissionFeedback)
            .assignment(new Assignment()
                .gradingScheme(new GradingScheme()
                    .addValue(new GradingSchemeValue()
                        .setGrade("49")
                        .setPercentageRange(49))))
            .resubmittable(true);
        submissionFeedback.submission(submission);

        assertTrue(submission.isStudentSubmissionRejected(student.getId()));
    }

    @Test
    void isStudentSubmissionRejectedTrueWhenGroupFeedbackGradeIsNegativeAndResubmittable() {
        User student = new User().setId(1L);
        SubmissionFeedback submissionFeedback = new SubmissionFeedback()
            .grade("49")
            .group(new Group());
        Submission submission = new Submission()
            .addSubmissionFeedback(submissionFeedback)
            .assignment(new Assignment()
                .gradingScheme(new GradingScheme()
                    .addValue(new GradingSchemeValue()
                        .setGrade("49")
                        .setPercentageRange(49))))
            .resubmittable(true);
        submissionFeedback.submission(submission);

        assertTrue(submission.isStudentSubmissionRejected(student.getId()));
    }

    @Test
    void isStudentSubmissionRejectedTrueWhenGroupFeedbackGradeIsPositiveAndIndividualNegativeAndResubmittable() {
        User student = new User().setId(1L);
        SubmissionFeedback groupFeedback = new SubmissionFeedback()
            .grade("50")
            .group(new Group());
        SubmissionFeedback individualFeedback = new SubmissionFeedback()
            .grade("49")
            .student(student);
        Submission submission = new Submission()
            .addSubmissionFeedback(groupFeedback)
            .addSubmissionFeedback(individualFeedback)
            .assignment(new Assignment()
                .gradingScheme(new GradingScheme()
                    .addValue(new GradingSchemeValue()
                        .setGrade("49")
                        .setPercentageRange(49))))
            .resubmittable(true);
        individualFeedback.submission(submission);

        assertTrue(submission.isStudentSubmissionRejected(student.getId()));
    }

    @Test
    void isSubmittedTrueWhenGradeIsEmptyAndSubmittedForGradingDateSpecified() {
        Submission submission = new Submission()
            .assignment(new Assignment())
            .submittedForGradingDate(Instant.now());

        assertTrue(submission.isSubmitted());
    }

    @Test
    void hasStudentSubmittedTrueWhenGradeIsEmptyAndSubmittedForGradingDateSpecified() {
        User student = new User().setId(1L);
        Submission submission = new Submission()
            .assignment(new Assignment())
            .submittedForGradingDate(Instant.now());

        assertTrue(submission.hasStudentSubmitted(student.getId()));
    }

    @Test
    void isInProgressTrueWhenEverythingIsLeftToDefault() {
        User student = new User().setId(1L);
        Submission submission = new Submission()
            .assignment(new Assignment());

        assertTrue(submission.isStudentSubmissionInProgress(student.getId()));
    }
}
