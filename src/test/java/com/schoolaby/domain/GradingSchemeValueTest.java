package com.schoolaby.domain;

import com.schoolaby.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GradingSchemeValueTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(GradingSchemeValue.class);
        GradingSchemeValue gradingSchemeValue1 = new GradingSchemeValue();
        gradingSchemeValue1.setId(1L);
        GradingSchemeValue gradingSchemeValue2 = new GradingSchemeValue();
        gradingSchemeValue2.setId(gradingSchemeValue1.getId());
        assertThat(gradingSchemeValue1).isEqualTo(gradingSchemeValue2);
        gradingSchemeValue2.setId(2L);
        assertThat(gradingSchemeValue1).isNotEqualTo(gradingSchemeValue2);
        gradingSchemeValue1.setId(null);
        assertThat(gradingSchemeValue1).isNotEqualTo(gradingSchemeValue2);
    }
}
