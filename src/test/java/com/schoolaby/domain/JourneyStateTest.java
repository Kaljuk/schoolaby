package com.schoolaby.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class JourneyStateTest {

    @Test
    void shouldThrowExceptionWhenUserFieldNull() {
        JourneyState journeyState = new JourneyState().journey(new Journey()).authority(new Authority());
        assertThrows(IllegalStateException.class, journeyState::calculate);
    }

    @Test
    void shouldThrowExceptionWhenJourneyFieldNull() {
        JourneyState journeyState = new JourneyState().user(new User()).authority(new Authority());
        assertThrows(IllegalStateException.class, journeyState::calculate);
    }

    @Test
    void shouldThrowExceptionWhenAuthorityFieldUnset() {
        JourneyState journeyState = new JourneyState().journey(new Journey()).user(new User());
        assertThrows(IllegalStateException.class, journeyState::calculate);
    }
}
