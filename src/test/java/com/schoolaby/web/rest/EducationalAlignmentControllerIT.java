package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.common.TestObjects;
import com.schoolaby.domain.EducationalAlignment;
import com.schoolaby.repository.EducationalAlignmentJpaRepository;
import com.schoolaby.service.dto.EducationalAlignmentDTO;
import com.schoolaby.service.mapper.EducationalAlignmentMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.domain.enumeration.Country.ESTONIA;
import static com.schoolaby.domain.enumeration.Country.TANZANIA;
import static com.schoolaby.domain.enumeration.EducationalAlignmentType.SUBJECT;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.MILLIS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class EducationalAlignmentControllerIT extends BaseIntegrationTest {
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String UPDATED_ALIGNMENT_TYPE = "BBBBBBBBBB";

    private static final String UPDATED_EDUCATIONAL_COUNTRY = TANZANIA.getValue();

    private static final String UPDATED_TARGET_NAME = "BBBBBBBBBB";

    private static final String UPDATED_TARGET_URL = "BBBBBBBBBB";

    private static final Instant UPDATED_START_DATE = now().truncatedTo(MILLIS);

    private static final Instant UPDATED_END_DATE = now().truncatedTo(MILLIS);

    @Autowired
    private EducationalAlignmentJpaRepository educationalAlignmentRepository;
    @Autowired
    private EducationalAlignmentMapper educationalAlignmentMapper;

    private EducationalAlignment educationalAlignment;

    @BeforeEach
    public void initTest() {
        educationalAlignment = TestObjects.createEducationalAlignment();
    }

    @Test
    @Transactional
    @WithUserDetails(ADMIN_LOGIN)
    void createEducationalAlignment() throws Exception {
        int databaseSizeBeforeCreate = educationalAlignmentRepository.findAll().size();
        EducationalAlignmentDTO educationalAlignmentDTO = educationalAlignmentMapper.toDto(educationalAlignment);

        mockMvc.perform(post("/api/educational-alignments")
            .contentType(APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(educationalAlignmentDTO)))
            .andExpect(status().isCreated());

        List<EducationalAlignment> educationalAlignments = educationalAlignmentRepository.findAll();
        assertThat(educationalAlignments).hasSize(databaseSizeBeforeCreate + 1);
        EducationalAlignment educationalAlignment = educationalAlignments.get(educationalAlignments.size() - 1);
        assertThat(educationalAlignment.getTitle()).isEqualTo(EDUCATIONAL_ALIGNMENT_TITLE);
        assertThat(educationalAlignment.getAlignmentType()).isEqualTo(EDUCATIONAL_ALIGNMENT_ALIGNMENT_TYPE);
        assertThat(educationalAlignment.getCountry()).isEqualTo(EDUCATIONAL_ALIGNMENT_COUNTRY);
        assertThat(educationalAlignment.getTargetName()).isEqualTo(EDUCATIONAL_ALIGNMENT_TARGET_NAME);
        assertThat(educationalAlignment.getTargetUrl()).isEqualTo(EDUCATIONAL_ALIGNMENT_TARGET_URL);
        assertThat(educationalAlignment.getStartDate()).isEqualTo(EDUCATIONAL_ALIGNMENT_START_DATE);
        assertThat(educationalAlignment.getEndDate()).isEqualTo(EDUCATIONAL_ALIGNMENT_END_DATE);
    }

    @Test
    @Transactional
    @WithUserDetails(ADMIN_LOGIN)
    void createEducationalAlignmentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = educationalAlignmentRepository.findAll().size();

        educationalAlignment.setId(1L);
        EducationalAlignmentDTO educationalAlignmentDTO = educationalAlignmentMapper.toDto(educationalAlignment);

        mockMvc.perform(post("/api/educational-alignments")
            .contentType(APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(educationalAlignmentDTO)))
            .andExpect(status().isBadRequest());

        List<EducationalAlignment> educationalAlignments = educationalAlignmentRepository.findAll();
        assertThat(educationalAlignments).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = educationalAlignmentRepository.findAll().size();
        educationalAlignment.setTitle(null);

        EducationalAlignmentDTO educationalAlignmentDTO = educationalAlignmentMapper.toDto(educationalAlignment);

        mockMvc.perform(post("/api/educational-alignments")
            .contentType(APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(educationalAlignmentDTO)))
            .andExpect(status().isBadRequest());

        List<EducationalAlignment> educationalAlignments = educationalAlignmentRepository.findAll();
        assertThat(educationalAlignments).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAlignmentTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = educationalAlignmentRepository.findAll().size();
        educationalAlignment.setAlignmentType(null);

        EducationalAlignmentDTO educationalAlignmentDTO = educationalAlignmentMapper.toDto(educationalAlignment);

        mockMvc.perform(post("/api/educational-alignments")
            .contentType(APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(educationalAlignmentDTO)))
            .andExpect(status().isBadRequest());

        List<EducationalAlignment> educationalAlignments = educationalAlignmentRepository.findAll();
        assertThat(educationalAlignments).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCountryIsRequired() throws Exception {
        int databaseSizeBeforeTest = educationalAlignmentRepository.findAll().size();
        educationalAlignment.setCountry(null);

        EducationalAlignmentDTO educationalAlignmentDTO = educationalAlignmentMapper.toDto(educationalAlignment);

        mockMvc.perform(post("/api/educational-alignments")
            .contentType(APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(educationalAlignmentDTO)))
            .andExpect(status().isBadRequest());

        List<EducationalAlignment> educationalAlignments = educationalAlignmentRepository.findAll();
        assertThat(educationalAlignments).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTargetNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = educationalAlignmentRepository.findAll().size();
        educationalAlignment.setTargetName(null);

        EducationalAlignmentDTO educationalAlignmentDTO = educationalAlignmentMapper.toDto(educationalAlignment);

        mockMvc.perform(post("/api/educational-alignments")
            .contentType(APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(educationalAlignmentDTO)))
            .andExpect(status().isBadRequest());

        List<EducationalAlignment> educationalAlignments = educationalAlignmentRepository.findAll();
        assertThat(educationalAlignments).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllEducationalAlignments() throws Exception {
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/educational-alignments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(educationalAlignment.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(EDUCATIONAL_ALIGNMENT_TITLE)))
            .andExpect(jsonPath("$.[*].alignmentType").value(hasItem(EDUCATIONAL_ALIGNMENT_ALIGNMENT_TYPE)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(EDUCATIONAL_ALIGNMENT_COUNTRY)))
            .andExpect(jsonPath("$.[*].targetName").value(hasItem(EDUCATIONAL_ALIGNMENT_TARGET_NAME)))
            .andExpect(jsonPath("$.[*].targetUrl").value(hasItem(EDUCATIONAL_ALIGNMENT_TARGET_URL)))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(EDUCATIONAL_ALIGNMENT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(EDUCATIONAL_ALIGNMENT_END_DATE.toString())));
    }

    @Test
    @Transactional
    void getAllEducationalAlignmentsByAlignmentType() throws Exception {
        educationalAlignment.setAlignmentType(SUBJECT.getAlignmentType());
        educationalAlignmentRepository.saveAndFlush(educationalAlignment);

        mockMvc.perform(get("/api/educational-alignments?alignmentType=subject"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].id").value(educationalAlignment.getId().intValue()))
            .andExpect(jsonPath("$.[0].title").value(EDUCATIONAL_ALIGNMENT_TITLE))
            .andExpect(jsonPath("$.[0].alignmentType").value(SUBJECT.getAlignmentType()))
            .andExpect(jsonPath("$.[0].country").value(EDUCATIONAL_ALIGNMENT_COUNTRY))
            .andExpect(jsonPath("$.[0].targetName").value(EDUCATIONAL_ALIGNMENT_TARGET_NAME))
            .andExpect(jsonPath("$.[0].targetUrl").value(EDUCATIONAL_ALIGNMENT_TARGET_URL))
            .andExpect(jsonPath("$.[0].startDate").value(EDUCATIONAL_ALIGNMENT_START_DATE.toString()))
            .andExpect(jsonPath("$.[0].endDate").value(EDUCATIONAL_ALIGNMENT_END_DATE.toString()));
    }

    @Test
    @Transactional
    void shouldGetEmptyPageWhenRandomStringAsAlignmentTypeWhenRequestingAllEducationalAlignments() throws Exception {
        educationalAlignment.setAlignmentType(SUBJECT.getAlignmentType());
        educationalAlignmentRepository.saveAndFlush(educationalAlignment);

        mockMvc.perform(get("/api/educational-alignments?alignmentType=random"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*]").value(empty()));
    }

    @Test
    @Transactional
    void getAllSuggestedEducationalAlignments() throws Exception {
        persistCreatedEntities(entityManager);

        String partialSearchString = "EducationalAlignment ti";
        mockMvc.perform(get("/api/educational-alignments")
            .param("title", partialSearchString))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].id").value(educationalAlignment.getId().intValue()))
            .andExpect(jsonPath("$.[0].title").value(EDUCATIONAL_ALIGNMENT_TITLE))
            .andExpect(jsonPath("$.[0].alignmentType").value(EDUCATIONAL_ALIGNMENT_ALIGNMENT_TYPE))
            .andExpect(jsonPath("$.[0].country").value(EDUCATIONAL_ALIGNMENT_COUNTRY))
            .andExpect(jsonPath("$.[0].targetName").value(EDUCATIONAL_ALIGNMENT_TARGET_NAME))
            .andExpect(jsonPath("$.[0].targetUrl").value(EDUCATIONAL_ALIGNMENT_TARGET_URL))
            .andExpect(jsonPath("$.[0].startDate").value(EDUCATIONAL_ALIGNMENT_START_DATE.toString()))
            .andExpect(jsonPath("$.[0].endDate").value(EDUCATIONAL_ALIGNMENT_END_DATE.toString()));
    }

    @Test
    @Transactional
    void getAllSuggestedEducationalAlignmentsByAlignmentType() throws Exception {
        educationalAlignment.setAlignmentType(SUBJECT.getAlignmentType());
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/educational-alignments")
            .param("title", EDUCATIONAL_ALIGNMENT_TITLE)
            .param("alignmentType", SUBJECT.getAlignmentType()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].id").value(educationalAlignment.getId().intValue()))
            .andExpect(jsonPath("$.[0].title").value(EDUCATIONAL_ALIGNMENT_TITLE))
            .andExpect(jsonPath("$.[0].alignmentType").value(SUBJECT.getAlignmentType()))
            .andExpect(jsonPath("$.[0].country").value(EDUCATIONAL_ALIGNMENT_COUNTRY))
            .andExpect(jsonPath("$.[0].targetName").value(EDUCATIONAL_ALIGNMENT_TARGET_NAME))
            .andExpect(jsonPath("$.[0].targetUrl").value(EDUCATIONAL_ALIGNMENT_TARGET_URL))
            .andExpect(jsonPath("$.[0].startDate").value(EDUCATIONAL_ALIGNMENT_START_DATE.toString()))
            .andExpect(jsonPath("$.[0].endDate").value(EDUCATIONAL_ALIGNMENT_END_DATE.toString()));
    }

    @Test
    @Transactional
    void shouldGetEmptyPageWhenRandomStringAsAlignmentTypeWhenRequestingSuggestedEducationalAlignments() throws Exception {
        educationalAlignment.setAlignmentType(SUBJECT.getAlignmentType());
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/educational-alignments?alignmentType=random&title=aaa"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[*]").value(empty()));
    }

    @Test
    @Transactional
    void getEducationalAlignment() throws Exception {
        educationalAlignmentRepository.saveAndFlush(educationalAlignment);

        mockMvc.perform(get("/api/educational-alignments/{id}", educationalAlignment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(educationalAlignment.getId().intValue()))
            .andExpect(jsonPath("$.title").value(EDUCATIONAL_ALIGNMENT_TITLE))
            .andExpect(jsonPath("$.alignmentType").value(EDUCATIONAL_ALIGNMENT_ALIGNMENT_TYPE))
            .andExpect(jsonPath("$.country").value(EDUCATIONAL_ALIGNMENT_COUNTRY))
            .andExpect(jsonPath("$.targetName").value(EDUCATIONAL_ALIGNMENT_TARGET_NAME))
            .andExpect(jsonPath("$.targetUrl").value(EDUCATIONAL_ALIGNMENT_TARGET_URL))
            .andExpect(jsonPath("$.startDate").value(EDUCATIONAL_ALIGNMENT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(EDUCATIONAL_ALIGNMENT_END_DATE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingEducationalAlignment() throws Exception {
        mockMvc.perform(get("/api/educational-alignments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithUserDetails(ADMIN_LOGIN)
    void updateEducationalAlignment() throws Exception {
        educationalAlignmentRepository.saveAndFlush(educationalAlignment);

        int databaseSizeBeforeUpdate = educationalAlignmentRepository.findAll().size();

        EducationalAlignment updatedEducationalAlignment = educationalAlignmentRepository.findById(educationalAlignment.getId()).get();
        entityManager.detach(updatedEducationalAlignment);
        updatedEducationalAlignment
            .title(UPDATED_TITLE)
            .alignmentType(UPDATED_ALIGNMENT_TYPE)
            .country(UPDATED_EDUCATIONAL_COUNTRY)
            .targetName(UPDATED_TARGET_NAME)
            .targetUrl(UPDATED_TARGET_URL)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE);
        EducationalAlignmentDTO educationalAlignmentDTO = educationalAlignmentMapper.toDto(updatedEducationalAlignment);

        mockMvc.perform(put("/api/educational-alignments")
            .contentType(APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(educationalAlignmentDTO)))
            .andExpect(status().isOk());

        List<EducationalAlignment> educationalAlignments = educationalAlignmentRepository.findAll();
        assertThat(educationalAlignments).hasSize(databaseSizeBeforeUpdate);
        EducationalAlignment educationalAlignment = educationalAlignments.get(educationalAlignments.size() - 1);
        assertThat(educationalAlignment.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(educationalAlignment.getAlignmentType()).isEqualTo(UPDATED_ALIGNMENT_TYPE);
        assertThat(educationalAlignment.getCountry()).isEqualTo(UPDATED_EDUCATIONAL_COUNTRY);
        assertThat(educationalAlignment.getTargetName()).isEqualTo(UPDATED_TARGET_NAME);
        assertThat(educationalAlignment.getTargetUrl()).isEqualTo(UPDATED_TARGET_URL);
        assertThat(educationalAlignment.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(educationalAlignment.getEndDate()).isEqualTo(UPDATED_END_DATE);
    }

    @Test
    @Transactional
    @WithUserDetails(ADMIN_LOGIN)
    void updateNonExistingEducationalAlignment() throws Exception {
        int databaseSizeBeforeUpdate = educationalAlignmentRepository.findAll().size();

        EducationalAlignmentDTO educationalAlignmentDTO = educationalAlignmentMapper.toDto(educationalAlignment);

        mockMvc.perform(put("/api/educational-alignments")
            .contentType(APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(educationalAlignmentDTO)))
            .andExpect(status().isBadRequest());

        List<EducationalAlignment> educationalAlignments = educationalAlignmentRepository.findAll();
        assertThat(educationalAlignments).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    @WithUserDetails(ADMIN_LOGIN)
    void deleteEducationalAlignment() throws Exception {
        educationalAlignmentRepository.saveAndFlush(educationalAlignment);
        int databaseSizeBeforeDelete = educationalAlignmentRepository.findAll().size();

        mockMvc.perform(delete("/api/educational-alignments/{id}", educationalAlignment.getId())
            .accept(APPLICATION_JSON))
            .andExpect(status().isNoContent());

        List<EducationalAlignment> educationalAlignments = educationalAlignmentRepository.findAll();
        assertThat(educationalAlignments).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    void shouldGetOnlyTanzanianEducationalAlignmentWhenCountrySpecified() throws Exception {
        EducationalAlignment tanzanianEducationalAlignment = TestObjects.createEducationalAlignment().country(TANZANIA.getValue());

        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/educational-alignments")
            .param("sort", "id,desc")
            .param("country", TANZANIA.getValue()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].id").value(tanzanianEducationalAlignment.getId().intValue()))
            .andExpect(jsonPath("$.[0].title").value(EDUCATIONAL_ALIGNMENT_TITLE))
            .andExpect(jsonPath("$.[0].alignmentType").value(EDUCATIONAL_ALIGNMENT_ALIGNMENT_TYPE))
            .andExpect(jsonPath("$.[0].country").value(TANZANIA.getValue()))
            .andExpect(jsonPath("$.[0].targetName").value(EDUCATIONAL_ALIGNMENT_TARGET_NAME))
            .andExpect(jsonPath("$.[0].targetUrl").value(EDUCATIONAL_ALIGNMENT_TARGET_URL))
            .andExpect(jsonPath("$.[0].startDate").value(EDUCATIONAL_ALIGNMENT_START_DATE.toString()))
            .andExpect(jsonPath("$.[0].endDate").value(EDUCATIONAL_ALIGNMENT_END_DATE.toString()));
    }

    @Test
    @Transactional
    void shouldGetOnlyEstonianEducationalAlignmentWhenCountrySpecified() throws Exception {
        TestObjects.createEducationalAlignment().country(TANZANIA.getValue());

        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/educational-alignments")
            .param("sort", "id,desc")
            .param("country", ESTONIA.getValue()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].id").value(educationalAlignment.getId().intValue()))
            .andExpect(jsonPath("$.[0].title").value(EDUCATIONAL_ALIGNMENT_TITLE))
            .andExpect(jsonPath("$.[0].alignmentType").value(EDUCATIONAL_ALIGNMENT_ALIGNMENT_TYPE))
            .andExpect(jsonPath("$.[0].country").value(EDUCATIONAL_ALIGNMENT_COUNTRY))
            .andExpect(jsonPath("$.[0].targetName").value(EDUCATIONAL_ALIGNMENT_TARGET_NAME))
            .andExpect(jsonPath("$.[0].targetUrl").value(EDUCATIONAL_ALIGNMENT_TARGET_URL))
            .andExpect(jsonPath("$.[0].startDate").value(EDUCATIONAL_ALIGNMENT_START_DATE.toString()))
            .andExpect(jsonPath("$.[0].endDate").value(EDUCATIONAL_ALIGNMENT_END_DATE.toString()));
    }

    @Test
    @Transactional
    void shouldGetOnlyEstonianEducationalAlignmentWhenCountryNotSpecified() throws Exception {
        TestObjects.createEducationalAlignment().country(TANZANIA.getValue());

        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/educational-alignments")
            .param("sort", "id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(educationalAlignment.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(EDUCATIONAL_ALIGNMENT_TITLE)))
            .andExpect(jsonPath("$.[*].alignmentType").value(hasItem(EDUCATIONAL_ALIGNMENT_ALIGNMENT_TYPE)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(EDUCATIONAL_ALIGNMENT_COUNTRY)))
            .andExpect(jsonPath("$.[*].targetName").value(hasItem(EDUCATIONAL_ALIGNMENT_TARGET_NAME)))
            .andExpect(jsonPath("$.[*].targetUrl").value(hasItem(EDUCATIONAL_ALIGNMENT_TARGET_URL)))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(EDUCATIONAL_ALIGNMENT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(EDUCATIONAL_ALIGNMENT_END_DATE.toString())));
    }

    @Test
    @Transactional
    void shouldGetOnlyEstonianEducationalAlignmentWhenCountryNotEstonianOrTanzanian() throws Exception {
        TestObjects.createEducationalAlignment().country(TANZANIA.getValue());

        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/educational-alignments")
            .param("sort", "id,desc")
            .param("country", "Finland"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[*].id").value(hasItem(educationalAlignment.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(EDUCATIONAL_ALIGNMENT_TITLE)))
            .andExpect(jsonPath("$.[*].alignmentType").value(hasItem(EDUCATIONAL_ALIGNMENT_ALIGNMENT_TYPE)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(EDUCATIONAL_ALIGNMENT_COUNTRY)))
            .andExpect(jsonPath("$.[*].targetName").value(hasItem(EDUCATIONAL_ALIGNMENT_TARGET_NAME)))
            .andExpect(jsonPath("$.[*].targetUrl").value(hasItem(EDUCATIONAL_ALIGNMENT_TARGET_URL)))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(EDUCATIONAL_ALIGNMENT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(EDUCATIONAL_ALIGNMENT_END_DATE.toString())));
    }
}
