package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.Submission;
import com.schoolaby.domain.UploadedFile;
import com.schoolaby.repository.SubmissionJpaRepository;
import com.schoolaby.repository.UploadedFileRepository;
import com.schoolaby.service.dto.UploadedFileDTO;
import com.schoolaby.service.mapper.UploadedFileMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UploadedFileControllerIT extends BaseIntegrationTest {
    private static final String DEFAULT_UNIQUE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_UNIQUE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_EXTENSION = "AAAAAAAAAA";
    private static final String UPDATED_EXTENSION = "BBBBBBBBBB";

    private static final String DEFAULT_ORIGINAL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_NAME = "BBBBBBBBBB";

    @Autowired
    private UploadedFileRepository uploadedFileRepository;
    @Autowired
    private SubmissionJpaRepository submissionJpaRepository;
    @Autowired
    private UploadedFileMapper uploadedFileMapper;

    private UploadedFile uploadedFile;

    public static UploadedFile createEntity(EntityManager em) {
        return new UploadedFile()
            .uniqueName(DEFAULT_UNIQUE_NAME)
            .type(DEFAULT_TYPE)
            .extension(DEFAULT_EXTENSION)
            .originalName(DEFAULT_ORIGINAL_NAME);
    }

    @BeforeEach
    public void initTest() {
        uploadedFile = createEntity(entityManager);
    }

    @Test
    @Transactional
    public void createUploadedFile() throws Exception {
        int databaseSizeBeforeCreate = uploadedFileRepository.findAll().size();
        UploadedFileDTO uploadedFileDTO = uploadedFileMapper.toDto(uploadedFile);
        mockMvc.perform(post("/api/uploaded-files")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(uploadedFileDTO)))
            .andExpect(status().isCreated());

        List<UploadedFile> uploadedFileList = uploadedFileRepository.findAll();
        assertThat(uploadedFileList).hasSize(databaseSizeBeforeCreate + 1);
        UploadedFile testUploadedFile = uploadedFileList.get(uploadedFileList.size() - 1);
        assertThat(testUploadedFile.getOriginalName()).isEqualTo(DEFAULT_ORIGINAL_NAME);
        assertThat(testUploadedFile.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testUploadedFile.getExtension()).isEqualTo(DEFAULT_EXTENSION);
        assertThat(testUploadedFile.getUniqueName()).isEqualTo(DEFAULT_UNIQUE_NAME);
    }

    @Test
    @Transactional
    public void createUploadedFileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = uploadedFileRepository.findAll().size();

        uploadedFile.setId(1L);
        UploadedFileDTO uploadedFileDTO = uploadedFileMapper.toDto(uploadedFile);

        mockMvc.perform(post("/api/uploaded-files")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(uploadedFileDTO)))
            .andExpect(status().isBadRequest());

        List<UploadedFile> uploadedFileList = uploadedFileRepository.findAll();
        assertThat(uploadedFileList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllUploadedFiles() throws Exception {
        Submission submission = SubmissionControllerIT.createEntity();
        submissionJpaRepository.saveAndFlush(submission);
        uploadedFileRepository.saveAndFlush(uploadedFile);

        mockMvc.perform(get("/api/uploaded-files?sort=id,desc&submissionId=" + submission.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(uploadedFile.getId().intValue())))
            .andExpect(jsonPath("$.[*].uniqueName").value(hasItem(DEFAULT_UNIQUE_NAME)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].extension").value(hasItem(DEFAULT_EXTENSION)))
            .andExpect(jsonPath("$.[*].originalName").value(hasItem(DEFAULT_ORIGINAL_NAME)));
    }

    @Test
    @Transactional
    public void getUploadedFile() throws Exception {
        uploadedFileRepository.saveAndFlush(uploadedFile);

        mockMvc.perform(get("/api/uploaded-files/{id}", uploadedFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(uploadedFile.getId().intValue()))
            .andExpect(jsonPath("$.uniqueName").value(DEFAULT_UNIQUE_NAME))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.extension").value(DEFAULT_EXTENSION))
            .andExpect(jsonPath("$.originalName").value(DEFAULT_ORIGINAL_NAME));
    }

    @Test
    @Transactional
    public void getNonExistingUploadedFile() throws Exception {
        mockMvc.perform(get("/api/uploaded-files/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void deleteUploadedFile() throws Exception {
        uploadedFileRepository.saveAndFlush(uploadedFile);
        int databaseSizeBeforeDelete = uploadedFileRepository.findAll().size();

        mockMvc.perform(delete("/api/uploaded-files/{id}", uploadedFile.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        List<UploadedFile> uploadedFileList = uploadedFileRepository.findAll();
        assertThat(uploadedFileList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
