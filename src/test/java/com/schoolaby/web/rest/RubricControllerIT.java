package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.*;
import com.schoolaby.service.mapper.RubricMapper;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;

import static com.schoolaby.common.TestCases.*;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.web.rest.TestUtil.convertObjectToJsonBytes;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WithMockCustomUser(authorities = {TEACHER})
public class RubricControllerIT extends BaseIntegrationTest {
    private static final String API_RUBRICS = "/api/rubrics/";
    private static final String API_RUBRICS_TEMPLATES = "/api/rubrics/templates";
    public static final String RUBRIC_TITLE = "AAAAAAAA";
    public static final String EDITED_RUBRIC_TITLE = "BBBBBBB";

    @Autowired
    private RubricMapper rubricMapper;

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldCreate() throws Exception {
        Rubric rubric = createRubric()
            .setTitle(RUBRIC_TITLE)
            .setAssignment(assignmentWithReferences(true));

        mockMvc.perform(post(API_RUBRICS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(rubricMapper.toDto(rubric))))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.id").exists())
            .andExpect(jsonPath("$.title").value(RUBRIC_TITLE));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotAllowToCreateWhenUserNotATeacherInJourney() throws Exception {
        User journeyCreator = createTeacher().setLogin("journeyCreator");
        Rubric rubric = createRubric();

        transactionHelper.withTransaction(() -> {
            Assignment assignment = assignmentWithReferences(false);
            assignment
                .creator(journeyCreator)
                .getMilestone()
                .creator(journeyCreator)
                .getJourney()
                .creator(journeyCreator)
                .addTeacher(journeyCreator)
                .addStudent(getTeacher(entityManager));
            rubric.setAssignment(assignment);
        });

        mockMvc.perform(post(API_RUBRICS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(rubricMapper.toDto(rubric))))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"Current user is not a teacher in the journey!\""));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetBySearch() throws Exception {
        Rubric rubric = createRubric()
            .setTitle(RUBRIC_TITLE)
            .setIsTemplate(true);
        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get(API_RUBRICS_TEMPLATES)
                .param("search", "a"))
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*].title", rubric.getTitle()).value(RUBRIC_TITLE));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetByAssignmentId() throws Exception {
        Rubric rubric = createRubric()
            .setTitle(RUBRIC_TITLE);
        transactionHelper.withNewTransaction(() -> {
            rubric.setAssignment(assignmentWithReferences(true));
            persistCreatedEntities(entityManager);
        });

        System.out.println();

        mockMvc.perform(get(API_RUBRICS)
                .param("assignmentId", String.valueOf(rubric.getAssignment().getId())))
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.title").value(RUBRIC_TITLE));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldUpdate() throws Exception {
        Rubric rubric = createRubric();
        transactionHelper.withTransaction(() -> {
            rubric.setAssignment(assignmentWithReferences(true));
            persistCreatedEntities(entityManager);
        });

        rubric.setTitle(EDITED_RUBRIC_TITLE);

        mockMvc.perform(put(API_RUBRICS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(rubricMapper.toDto(rubric))))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").exists())
            .andExpect(jsonPath("$.title").value(EDITED_RUBRIC_TITLE));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotAllowToUpdateWhenUserNotATeacherInJourney() throws Exception {
        User journeyCreator = createTeacher().setLogin("journeyCreator");
        Rubric rubric = createRubric();

        transactionHelper.withTransaction(() -> {
            Assignment assignment = assignmentWithReferences(false);
            assignment
                .creator(journeyCreator)
                .getMilestone()
                .creator(journeyCreator)
                .getJourney()
                .creator(journeyCreator)
                .addTeacher(journeyCreator)
                .addStudent(getTeacher(entityManager));
            rubric.setAssignment(assignment);

        });

        mockMvc.perform(put(API_RUBRICS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(rubricMapper.toDto(rubric))))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"Current user is not a teacher in the journey!\""));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldDelete() throws Exception {
        Rubric rubric = createRubric();
        transactionHelper.withTransaction(() -> {
            rubric.setAssignment(assignmentWithReferences(true));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(delete(API_RUBRICS + rubric.getId().toString())
                .contentType(APPLICATION_JSON))
            .andExpect(status().isNoContent());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotAllowToDeleteWhenUserNotATeacherInJourney() throws Exception {
        User journeyCreator = createTeacher().setLogin("journeyCreator");
        Rubric rubric = createRubric();

        transactionHelper.withTransaction(() -> {
            Assignment assignment = assignmentWithReferences(false);
            assignment
                .creator(journeyCreator)
                .getMilestone()
                .creator(journeyCreator)
                .getJourney()
                .creator(journeyCreator)
                .addTeacher(journeyCreator)
                .addStudent(getTeacher(entityManager));
            rubric.setAssignment(assignment);

        });

        mockMvc.perform(delete(API_RUBRICS + rubric.getId())
                .contentType(APPLICATION_JSON))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"Current user is not a teacher in the journey!\""));
    }

    private Assignment assignmentWithReferences(boolean withMembers) {
        EducationalLevel educationalLevel = createEducationalLevel();
        Journey journey = createJourney();
        Milestone milestone = createMilestone();
        Assignment assignment = createAssignment();
        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            assignment
                .creator(teacher)
                .gradingScheme(getGradingSchemeNumerical(entityManager));
            milestone.creator(teacher)
                .addAssignment(assignment);
            journey.creator(teacher)
                .educationalLevel(educationalLevel)
                .addMilestone(milestone);
            if (withMembers) {
                journey
                    .addTeacher(teacher)
                    .addStudent(getStudent(entityManager));
            }
            persistCreatedEntities(entityManager);
        });
        return assignment;
    }
}
