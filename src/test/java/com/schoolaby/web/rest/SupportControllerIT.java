package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.service.dto.SupportEmailDTO;
import org.junit.jupiter.api.Test;
import org.springframework.security.test.context.support.WithUserDetails;

import static com.schoolaby.web.rest.TestUtil.convertObjectToJsonBytes;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class SupportControllerIT extends BaseIntegrationTest {

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldSendSupportEmail() throws Exception {
        SupportEmailDTO supportEmailDTO = new SupportEmailDTO()
            .setSubject("Test subject")
            .setContent("Test content");

        mockMvc.perform(post("/api/support")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(supportEmailDTO)))
            .andExpect(status().isCreated());
    }
}
