package com.schoolaby.web.rest;

import com.nimbusds.jwt.JWTParser;
import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.common.FileUtil;
import com.schoolaby.domain.Authority;
import com.schoolaby.domain.User;
import com.schoolaby.repository.UserRepository;
import com.schoolaby.web.rest.vm.LoginVM;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;
import java.util.Set;

import static com.schoolaby.common.TestObjects.createUser;
import static com.schoolaby.common.TestObjects.persistCreatedEntities;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.header;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class AuthenticationControllerIT extends BaseIntegrationTest {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private RestTemplate harIdRestTemplate;

    private User user;

    @Test
    void harIdLoginShouldCreateDbUser() throws Exception {
        MockRestServiceServer haridMock = MockRestServiceServer.bindTo(harIdRestTemplate).build();
        haridMock.expect(requestTo("https://test.ee/et/access_tokens"))
            .andExpect(method(POST))
            .andExpect(content().string("grant_type=authorization_code&redirect_uri=http%3A%2F%2Flocalhost%2Fauthenticate%2Fharid&code=123"))
            .andRespond(
                withSuccess(
                    "{\"access_token\":\"1213\",\"token_type\":\"bearer\",\"expires_in\":86399,\"id_token\":\"some.jwt.info\"}",
                    APPLICATION_JSON
                )
            );
        haridMock.expect(requestTo("https://test.ee/et/user_info"))
            .andExpect(method(GET))
            .andExpect(header(AUTHORIZATION, "Bearer 1213"))
            .andRespond(
                withSuccess(FileUtil.readFile("/harid-user-info-response.json", this.getClass()),
                    APPLICATION_JSON
                )
            );

        mockMvc.perform(get("/api/harid/success").param("code", "123"))
            .andExpect(status().isOk());

        Optional<User> user = userRepository.findOneByEmailIgnoreCase("test.user@netgroup.com");
        assertTrue(user.isPresent());
    }

    @Test
    void testAuthorize() throws Exception {
        transactionHelper.withTransaction(() ->{
            user = createUser();
            user.setLogin("user-jwt-controller");
            user.setEmail("user-jwt-controller@example.com");
            user.setActivated(true);
            user.setPassword(passwordEncoder.encode("test"));

            persistCreatedEntities(entityManager);
        });

        LoginVM login = new LoginVM();
        login.setUsername("user-jwt-controller");
        login.setPassword("test");
        mockMvc
            .perform(post("/api/authenticate").contentType(APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(login)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id_token").isString())
            .andExpect(jsonPath("$.id_token").isNotEmpty())
            .andExpect(header().string("Authorization", not(nullValue())))
            .andExpect(header().string("Authorization", not(is(emptyString()))));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldRefreshJwt() throws Exception {
        transactionHelper.withNewTransaction(() -> {
            user = createUser();
            user.setLogin("user-jwt-controller");
            user.setEmail("user-jwt-controller@example.com");
            user.addAuthority(new Authority().name(TEACHER));
            user.setActivated(true);
            user.setPassword(passwordEncoder.encode("test"));

            persistCreatedEntities(entityManager);
        });

        LoginVM login = new LoginVM();
        login.setUsername("user-jwt-controller");
        login.setPassword("test");
        MvcResult mvcLoginResult = mockMvc.perform(post("/api/authenticate")
            .contentType(APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(login)))
            .andExpect(jsonPath("$.id_token").isString())
            .andExpect(jsonPath("$.id_token").isNotEmpty()).andReturn();
        String auth = getJwtClaimFromMockMcv(mvcLoginResult, "auth");
        assertEquals(TEACHER, auth);

        transactionHelper.withNewTransaction(() -> {
            user.setAuthorities(Set.of(new Authority().name(STUDENT)));
            userRepository.saveAndFlush(user);
        });

        MvcResult mvcUpdateResult = mockMvc
            .perform(get("/api/authenticate/refresh").contentType(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id_token").isString())
            .andExpect(jsonPath("$.id_token").isNotEmpty())
            .andExpect(header().string("Authorization", not(nullValue())))
            .andExpect(header().string("Authorization", not(is(emptyString())))).andReturn();
        String updatedAuth = getJwtClaimFromMockMcv(mvcUpdateResult, "auth");
        assertEquals(STUDENT, updatedAuth);
    }

    @Test
    void testAuthorizeWithRememberMe() throws Exception {
        transactionHelper.withNewTransaction(() -> {
            user = createUser();
            user.setLogin("user-jwt-controller-remember-me");
            user.setEmail("user-jwt-controller-remember-me@example.com");
            user.setActivated(true);
            user.setPassword(passwordEncoder.encode("test"));

            persistCreatedEntities(entityManager);
        });

        LoginVM login = new LoginVM();
        login.setUsername("user-jwt-controller-remember-me");
        login.setPassword("test");
        login.setRememberMe(true);
        mockMvc
            .perform(post("/api/authenticate").contentType(APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(login)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id_token").isString())
            .andExpect(jsonPath("$.id_token").isNotEmpty())
            .andExpect(header().string("Authorization", not(nullValue())))
            .andExpect(header().string("Authorization", not(is(emptyString()))));
    }

    @Test
    void testAuthorizeFails() throws Exception {
        LoginVM login = new LoginVM();
        login.setUsername("wrong-user");
        login.setPassword("wrong password");
        mockMvc
            .perform(post("/api/authenticate").contentType(APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(login)))
            .andExpect(status().isUnauthorized())
            .andExpect(jsonPath("$.id_token").doesNotExist())
            .andExpect(header().doesNotExist("Authorization"));
    }

    private String getJwtClaimFromMockMcv(MvcResult mvcResult, String claim) throws Exception {
        String response = mvcResult.getResponse().getContentAsString();
        String token = response.replace("{\"id_token\":\"", "").replace("\"}", "");
        return (String) JWTParser.parse(token).getJWTClaimsSet().getClaim(claim);
    }
}
