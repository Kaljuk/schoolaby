package com.schoolaby.web.rest.upload;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.service.storage.LocalStorageService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;

import static org.hamcrest.Matchers.matchesPattern;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class FileControllerIT extends BaseIntegrationTest {
    @MockBean
    private LocalStorageService storageServiceMock;

    @Test
    void shouldSaveFile() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "user-file.txt", "text/plain", "test data".getBytes());
        ArgumentCaptor<String> fileNameCaptor = ArgumentCaptor.forClass(String.class);
        doNothing().when(storageServiceMock).save(eq(file), fileNameCaptor.capture());

        mockMvc.perform(multipart("/api/files").file(file))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(notNullValue()))
            .andExpect(jsonPath("$.originalName").value("user-file.txt"))
            .andExpect(jsonPath("$.type").value("txt"))
            .andExpect(jsonPath("$.extension").value("txt"))
            .andExpect(jsonPath("$.uniqueName").value(matchesPattern(fileNameCaptor.getValue())));
    }
}
