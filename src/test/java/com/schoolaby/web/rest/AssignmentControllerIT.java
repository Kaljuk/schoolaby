package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.*;
import com.schoolaby.repository.AssignmentJpaRepository;
import com.schoolaby.repository.LtiResourceJpaRepository;
import com.schoolaby.security.SpringSecurityAuditorAware;
import com.schoolaby.service.dto.AssignmentDTO;
import com.schoolaby.service.dto.AssignmentPatchDTO;
import com.schoolaby.service.dto.MaterialDTO;
import com.schoolaby.service.dto.lti.LtiResourceDTO;
import com.schoolaby.service.dto.states.EntityState;
import com.schoolaby.service.mapper.AssignmentMapper;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.security.test.context.support.WithUserDetails;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;

import static com.schoolaby.common.TestCases.*;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.service.dto.states.EntityState.OVERDUE;
import static com.schoolaby.service.dto.states.EntityState.URGENT;
import static com.schoolaby.web.rest.TestUtil.convertObjectToJsonBytes;
import static java.lang.Long.MAX_VALUE;
import static java.time.Instant.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.MILLIS;
import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.joining;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WithMockCustomUser(authorities = {TEACHER})
class AssignmentControllerIT extends BaseIntegrationTest {
    private static final String UPDATED_TITLE = "BBBBBBBBBB";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";
    private static final Instant UPDATED_DEADLINE = now().truncatedTo(MILLIS);
    private static final Boolean UPDATED_FLEXIBLE_DEADLINE = true;
    private static final String API_ASSIGNMENTS = "/api/assignments";
    private static final String API_GROUP_ASSIGNMENTS = "/api/group-assignments";

    @Autowired
    private LtiResourceJpaRepository ltiResourceJpaRepository;
    @Autowired
    private AssignmentJpaRepository assignmentJpaRepository;
    @Autowired
    private AssignmentMapper assignmentMapper;
    @SpyBean
    private SpringSecurityAuditorAware springSecurityAuditorAware;

    private Milestone milestoneWithReferences() {
        return milestoneWithReferences(false);
    }

    private Milestone milestoneWithReferences(boolean persist) {
        Journey journey = createJourney();
        Milestone milestone = createMilestone();
        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            Subject subject = getSubject(entityManager);
            journey.educationalLevel(createEducationalLevel())
                .addStudent(getStudent(entityManager))
                .creator(teacher)
                .setSubject(subject)
                .addMilestone(milestone
                    .creator(teacher));

            if (persist) {
                persistCreatedEntities(entityManager);
            }
        });
        return milestone;
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldCreate() throws Exception {
        EducationalLevel educationalLevel = createEducationalLevel();
        EducationalAlignment educationalAlignment = createEducationalAlignment();
        Journey journey = createJourney();
        Milestone milestone = createMilestone();
        User student1 = createStudent();
        String student2Login = "student2";
        String student2Email = "student2@netgroup.com";
        User student2 = createStudent().setLogin(student2Login).setEmail(student2Email);
        String student3Login = "student3";
        String student3Email = "student3@netgroup.com";
        String group2Name = "Grupp 2";
        User student3 = createStudent().setLogin(student3Login).setEmail(student3Email);
        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            journey.educationalLevel(educationalLevel)
                .creator(teacher)
                .addTeacher(teacher)
                .addMilestone(milestone
                    .creator(teacher)
                    .addEducationalAlignment(educationalAlignment));
            persistCreatedEntities(entityManager);
        });
        Assignment assignment = createAssignment();
        Material material = createMaterial();
        LtiResource ltiResource = createLtiResource();
        LtiLineItem ltiLineItem = createLtiLineItem();
        Group group1 = createGroup().setStudents(Set.of(student1, student2));
        Group group2 = createGroup()
            .setName(group2Name)
            .setStudents(Set.of(student3));

        assignment
            .creator(User.builder().id(TEACHER_ID).build())
            .milestone(milestone)
            .addLtiResource(ltiResource
                .addLtiLineItem(ltiLineItem))
            .addStudent(User.builder().id(STUDENT_ID).build())
            .addEducationalAlignment(educationalAlignment)
            .addMaterial(material)
            .setGroups(Set.of(group1, group2))
            .gradingScheme(getGradingSchemeNumerical(entityManager));

        mockMvc.perform(post(API_ASSIGNMENTS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentMapper.toDto(assignment), true)))
            .andExpect(status().isCreated());

        transactionHelper.withTransaction(() -> {
            List<Assignment> assignments = assignmentJpaRepository.findAll();
            assertEquals(1, assignments.size());
            Assignment savedAssignment = assignments.get(0);
            Set<LtiResource> ltiResources = new HashSet<>(ltiResourceJpaRepository.findAll());
            assertEquals(1, ltiResources.size());
            assertNotNull(savedAssignment.getId());
            assertEquals(ASSIGNMENT_TITLE, savedAssignment.getTitle());
            assertEquals(ASSIGNMENT_DESCRIPTION, savedAssignment.getDescription());
            assertEquals(ASSIGNMENT_DEADLINE, savedAssignment.getDeadline());
            assertEquals(ASSIGNMENT_FLEXIBLE_DEADLINE, savedAssignment.isFlexibleDeadline());
            assertEquals(TEACHER_ID, savedAssignment.getCreator().getId());
            assertEquals(2, savedAssignment.getGroups().size());
            Group receivedGroup1 = savedAssignment.getGroups().stream().filter(group -> group.getName().equals(GROUP_NAME)).findFirst().orElseThrow();
            Group receivedGroup2 = savedAssignment.getGroups().stream().filter(group -> group.getName().equals(group2Name)).findFirst().orElseThrow();
            assertEquals(2, receivedGroup1.getStudents().size());
            assertEquals(1, receivedGroup2.getStudents().size());
            assertTrue(receivedGroup1.getStudents().containsAll(Set.of(student1, student2)));
            assertTrue(receivedGroup2.getStudents().contains(student3));
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnCreateWhenUserNotATeacherInJourney() throws Exception {
        User teacher = createTeacher();
        EducationalLevel educationalLevel = createEducationalLevel();
        EducationalAlignment educationalAlignment = createEducationalAlignment();
        Journey journey = createJourney()
            .educationalLevel(educationalLevel)
            .creator(teacher)
            .addTeacher(teacher);
        Milestone milestone = createMilestone()
            .creator(teacher)
            .addEducationalAlignment(educationalAlignment);
        transactionHelper.withTransaction(() -> {
            journey
                .addStudent(getTeacher(entityManager))
                .addMilestone(milestone);
            persistCreatedEntities(entityManager);
        });

        Assignment assignment = createAssignment()
            .milestone(milestone)
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .creator(teacher);

        mockMvc.perform(post(API_ASSIGNMENTS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentMapper.toDto(assignment))))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"Current user is not a teacher in the journey!\""));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldCreateAsCoTeacherOfTheJourney() throws Exception {
        User creator = createTeacher();
        EducationalLevel educationalLevel = createEducationalLevel();
        EducationalAlignment educationalAlignment = createEducationalAlignment();
        Journey journey = createJourney()
            .educationalLevel(educationalLevel)
            .creator(creator)
            .addTeacher(creator);
        Milestone milestone = createMilestone()
            .creator(creator)
            .addEducationalAlignment(educationalAlignment);
        transactionHelper.withTransaction(() -> {
            journey
                .addTeacher(getTeacher(entityManager))
                .addMilestone(milestone);
            persistCreatedEntities(entityManager);
        });

        Assignment assignment = createAssignment()
            .milestone(milestone)
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .creator(creator);

        mockMvc.perform(post(API_ASSIGNMENTS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentMapper.toDto(assignment), true)))
            .andExpect(status().isCreated());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnCreateWhenUserNotATeacherOnJourney() throws Exception {
        User teacher = createTeacher();
        EducationalLevel educationalLevel = createEducationalLevel();
        EducationalAlignment educationalAlignment = createEducationalAlignment();
        Journey journey = createJourney()
            .educationalLevel(educationalLevel)
            .creator(teacher)
            .addTeacher(teacher);
        Milestone milestone = createMilestone()
            .creator(teacher)
            .addEducationalAlignment(educationalAlignment);
        transactionHelper.withTransaction(() -> {
            journey
                .addStudent(getTeacher(entityManager))
                .addMilestone(milestone);
            persistCreatedEntities(entityManager);
        });

        Assignment assignment = createAssignment()
            .milestone(milestone)
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .creator(teacher);

        mockMvc.perform(post(API_ASSIGNMENTS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentMapper.toDto(assignment))))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"Current user is not a teacher in the journey!\""));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnCreateWithId() throws Exception {
        Milestone milestone = milestoneWithReferences(true);
        Assignment assignment = createAssignment()
            .milestone(milestone)
            .id(-9999999L);

        mockMvc.perform(post(API_ASSIGNMENTS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentMapper.toDto(assignment))))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("errorKey").value("idExists"));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldRequireTitleOnCreate() throws Exception {
        Milestone milestone = milestoneWithReferences(true);
        Assignment assignment = createAssignment()
            .milestone(milestone)
            .title(null);

        mockMvc.perform(post(API_ASSIGNMENTS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentMapper.toDto(assignment))))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("fieldErrors[0].objectName").value("assignmentUpdate"))
            .andExpect(jsonPath("fieldErrors[0].field").value("title"))
            .andExpect(jsonPath("fieldErrors[0].message").value("NotNull"));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetAllAssignmentsAsTeacher() throws Exception {
        Milestone milestone = milestoneWithReferences();
        Assignment assignment = createAssignment()
            .setRequiresSubmission(true);
        User studentWhoIsNotInJourney = createUser().addAuthority(new Authority().name(STUDENT));

        transactionHelper.withNewTransaction(() -> {
            User student = getStudent(entityManager);
            milestone.getJourney().addTeacher(getTeacher(entityManager));
            Submission submission = createSubmission().addAuthor(student);

            milestone.addAssignment(assignment
                .creator(getTeacher(entityManager))
                .gradingScheme(getGradingSchemeNumerical(entityManager))
                .addSubmission(submission)
                .addSubmission(createSubmission().addAuthor(studentWhoIsNotInJourney)));

            createSubmissionFeedback()
                .submission(submission)
                .student(student)
                .grade(null)
                .creator(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });
        String today = ofPattern("yyyy-MM-dd").format(LocalDateTime.now());
        String todayPlus7 = ofPattern("yyyy-MM-dd").format(LocalDateTime.now().plusDays(7L));

        mockMvc.perform(get(API_ASSIGNMENTS + "?fromDate=" + today + "&toDate=" + todayPlus7 + "&states=IN_PROGRESS,URGENT,NOT_STARTED"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(1))
            .andExpect(jsonPath("$.[0].id").value(assignment.getId().intValue()))
            .andExpect(jsonPath("$.[0].title").value(ASSIGNMENT_TITLE))
            .andExpect(jsonPath("$.[0].description").value(ASSIGNMENT_DESCRIPTION))
            .andExpect(jsonPath("$.[0].state").value(URGENT.toString()))
            .andExpect(jsonPath("$.[0].deadline").value(ASSIGNMENT_DEADLINE.toString()))
            .andExpect(jsonPath("$.[0].submissionsCount").value(1));
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldGetActiveAssignmentsAsStudent() throws Exception {
        Milestone milestone = milestoneWithReferences();
        Assignment assignment = createAssignment()
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone);
        transactionHelper.withNewTransaction(() -> {
            milestone.getJourney()
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });
        String today = ofPattern("yyyy-MM-dd").format(LocalDateTime.now());
        String todayPlus7 = ofPattern("yyyy-MM-dd").format(LocalDateTime.now().plusDays(7L));

        mockMvc.perform(get(API_ASSIGNMENTS + "?fromDate=" + today + "&toDate=" + todayPlus7 + "&states=IN_PROGRESS,URGENT,NOT_STARTED"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(1))
            .andExpect(jsonPath("$.[0].id").value(assignment.getId().intValue()))
            .andExpect(jsonPath("$.[0].title").value(ASSIGNMENT_TITLE))
            .andExpect(jsonPath("$.[0].description").value(ASSIGNMENT_DESCRIPTION))
            .andExpect(jsonPath("$.[0].state").value(URGENT.toString()))
            .andExpect(jsonPath("$.[0].deadline").value(ASSIGNMENT_DEADLINE.toString()));
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldGetOverdueAssignmentsAsStudent() throws Exception {
        Milestone milestone = milestoneWithReferences();
        String yesterday = ofPattern("yyyy-MM-dd").format(LocalDateTime.now().minusDays(1L));
        String lastMonth = ofPattern("yyyy-MM-dd").format(LocalDateTime.now().minusDays(31L));

        Instant deadline = now().minusSeconds(86400L);
        Assignment assignment = createAssignment()
            .creator(getTeacher(entityManager))
            .deadline(deadline)
            .flexibleDeadline(true)
            .requiresSubmission(true)
            .setMilestone(milestone);
        transactionHelper.withNewTransaction(() -> {
            milestone.getJourney()
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get(API_ASSIGNMENTS + "?fromDate=" + lastMonth + "&toDate=" + yesterday + "&states=OVERDUE,REJECTED"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(1))
            .andExpect(jsonPath("$.[0].id").value(assignment.getId().intValue()))
            .andExpect(jsonPath("$.[0].title").value(ASSIGNMENT_TITLE))
            .andExpect(jsonPath("$.[0].description").value(ASSIGNMENT_DESCRIPTION))
            .andExpect(jsonPath("$.[0].state").value(OVERDUE.toString()))
            .andExpect(jsonPath("$.[0].deadline").value(deadline.toString()));
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldGetById() throws Exception {
        Milestone milestone = milestoneWithReferences();
        Assignment assignment = createAssignment();
        User studentWhoIsNotInJourney = createUser().addAuthority(new Authority().name(STUDENT));
        transactionHelper.withNewTransaction(() -> {
            assignment
                .creator(getTeacher(entityManager))
                .gradingScheme(getGradingSchemeNumerical(entityManager))
                .addSubmission(createSubmission().addAuthor(getStudent(entityManager)))
                .addSubmission(createSubmission().addAuthor(studentWhoIsNotInJourney))
                .milestone(milestone);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get(API_ASSIGNMENTS + "/{id}", assignment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(assignment.getId().intValue()))
            .andExpect(jsonPath("$.title").value(ASSIGNMENT_TITLE))
            .andExpect(jsonPath("$.description").value(ASSIGNMENT_DESCRIPTION))
            .andExpect(jsonPath("$.deadline").value(ASSIGNMENT_DEADLINE.toString()))
            .andExpect(jsonPath("$.submissionsCount").value(1))
            .andExpect(jsonPath("$.flexibleDeadline").value(ASSIGNMENT_FLEXIBLE_DEADLINE))
            .andExpect(jsonPath("$.subject.label").value(SUBJECT_LABEL));
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldThrowWhenNotFound() throws Exception {
        mockMvc.perform(get(API_ASSIGNMENTS + "/{id}", MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldUpdate() throws Exception {
        Milestone milestone = milestoneWithReferences();
        User student1 = createStudent();
        User student2 = createStudent().setLogin("student123").setEmail("student123@netgroup.com");
        Group group1 = createGroup()
            .setStudents(Set.of(student1, student2));
        String group2Name = "Grupp 2";
        Group group2 = createGroup()
            .setName(group2Name)
            .setStudents(Set.of(getStudent(entityManager)));

        Assignment assignment = createAssignment()
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone)
            .setGroups(Set.of(group1, group2));
        transactionHelper.withNewTransaction(() -> {
            milestone.getJourney()
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });
        entityManager.detach(assignment);

        String updatedGroupName = "Updated Grupp";
        group2.setStudents(Set.of(student1, student2)).setName(updatedGroupName);

        assignment
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .deadline(UPDATED_DEADLINE)
            .flexibleDeadline(UPDATED_FLEXIBLE_DEADLINE)
            .setGroups(Set.of(group1, group2));


        mockMvc.perform(put(API_ASSIGNMENTS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentMapper.toDto(assignment))))
            .andExpect(status().isOk());

        transactionHelper.withTransaction(() -> {
            List<Assignment> assignments = assignmentJpaRepository.findAll();
            assertEquals(1, assignments.size());
            Assignment testAssignment = assignments.get(0);
            assertEquals(UPDATED_TITLE, testAssignment.getTitle());
            assertEquals(UPDATED_DESCRIPTION, testAssignment.getDescription());
            assertEquals(UPDATED_DEADLINE, testAssignment.getDeadline());
            assertEquals(UPDATED_FLEXIBLE_DEADLINE, testAssignment.isFlexibleDeadline());
            assertTrue(testAssignment.getGroups().stream().anyMatch(g -> g.getName().equals(updatedGroupName)));

            Group receivedGroup = testAssignment.getGroups().stream().filter(group -> group.getName().equals(GROUP_NAME)).findFirst().orElseThrow();
            assertEquals(2, receivedGroup.getStudents().size());
            assertTrue(receivedGroup.getStudents().containsAll(Set.of(student1, student2)));
            assertEquals(testAssignment.getStates().size(), 2);
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnUpdateWhenUserNotATeacherInJourney() throws Exception {
        User teacher = createTeacher();
        Milestone milestone = milestoneWithReferences();
        Assignment assignment = createAssignment()
            .creator(teacher)
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone);

        transactionHelper.withTransaction(() -> {
            milestone
                .creator(teacher)
                .getJourney()
                .teachers(new HashSet<>())
                .students(new HashSet<>())
                .addStudent(getTeacher(entityManager))
                .addTeacher(teacher)
                .creator(teacher);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(put(API_ASSIGNMENTS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentMapper.toDto(assignment))))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"Current user is not a teacher in the journey!\""));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowWhenUpdatingWithoutId() throws Exception {
        Milestone milestone = milestoneWithReferences();
        Assignment assignment = createAssignment().milestone(milestone);

        mockMvc.perform(put(API_ASSIGNMENTS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentMapper.toDto(assignment))))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("entityName").value("assignment"))
            .andExpect(jsonPath("errorKey").value("idNull"));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldPatch() throws Exception {
        Milestone milestone = milestoneWithReferences();
        final User student1 = createStudent();
        final User student2 = createStudent().setLogin("student123").setEmail("student123@netgroup.com");
        final Group group1 = createGroup()
            .setStudents(Set.of(student1, student2));
        String group2Name = "Grupp 2";
        final Group group2 = createGroup()
            .setName(group2Name)
            .setStudents(Set.of(getStudent(entityManager)));

        Assignment assignment = createAssignment()
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone)
            .setGroups(Set.of(group1, group2));
        transactionHelper.withNewTransaction(() -> {
            milestone.getJourney()
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        AssignmentPatchDTO assignmentPatchDTO = new AssignmentPatchDTO()
            .setTitle(UPDATED_TITLE)
            .setDeadline(UPDATED_DEADLINE)
            .setDescription(UPDATED_DESCRIPTION)
            .setLtiResources(Set.of(new LtiResourceDTO()
                .setTitle(LTI_RESOURCE_TITLE)
            ))
            .setMaterials(Set.of(new MaterialDTO()
                .setTitle(MATERIAL_TITLE)
                .setUrl(MATERIAL_URL)
            ));

        mockMvc.perform(patch(API_ASSIGNMENTS + "/{id}", assignment.getId())
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentPatchDTO)))
            .andExpect(status().isOk());

        transactionHelper.withTransaction(() -> {
            List<Assignment> assignments = assignmentJpaRepository.findAll();
            assertEquals(1, assignments.size());
            Assignment testAssignment = assignments.get(0);
            assertEquals(UPDATED_TITLE, testAssignment.getTitle());
            assertEquals(UPDATED_DESCRIPTION, testAssignment.getDescription());
            assertEquals(UPDATED_DEADLINE, testAssignment.getDeadline());
            assertEquals(1, testAssignment.getLtiResources().size());
            assertEquals(1, testAssignment.getMaterials().size());
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldDelete() throws Exception {
        final EducationalLevel educationalLevel = createEducationalLevel();
        final EducationalAlignment educationalAlignment = createEducationalAlignment();
        final Journey journey = createJourney();
        final Milestone milestone = createMilestone();
        final Assignment assignment = createAssignment();
        final Submission submission = createSubmission();
        final Material material = createMaterial();
        final LtiResource ltiResource = createLtiResource();
        final LtiLaunch ltiLaunch = createLtiLaunch();
        final LtiLineItem ltiLineItem = createLtiLineItem();
        final LtiScore ltiScore = createLtiScore();
        final Group group = createGroup();
        transactionHelper.withNewTransaction(() -> {
            User teacher = getTeacher(entityManager);
            User student = getStudent(entityManager);
            journey.educationalLevel(educationalLevel)
                .creator(teacher)
                .addTeacher(teacher)
                .addMilestone(milestone
                    .creator(teacher)
                    .addEducationalAlignment(educationalAlignment)
                    .addAssignment(assignment
                        .creator(teacher)
                        .addLtiResource(ltiResource
                            .addLtiLaunch(ltiLaunch.setUser(teacher))
                            .addLtiLineItem(ltiLineItem
                                .addScore(ltiScore
                                    .setUser(student))))
                        .addStudent(student)
                        .addEducationalAlignment(educationalAlignment)
                        .addMaterial(material)
                        .gradingScheme(getGradingSchemeNumerical(entityManager))
                        .setGroups(Set.of(group.setStudents(Set.of(student))))
                        .addSubmission(submission
                            .addAuthor(student))));

            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(delete(API_ASSIGNMENTS + "/{id}", assignment.getId())
                .accept(APPLICATION_JSON))
            .andExpect(status().isNoContent());

        transactionHelper.withNewTransaction(() -> {
            // entities that should be soft deleted
            assertNotNull(entityManager.createNativeQuery("SELECT deleted FROM assignment WHERE id = ?").setParameter(1, assignment.getId()).getSingleResult());
            assertNotNull(entityManager.createNativeQuery("SELECT deleted FROM submission WHERE id = ?").setParameter(1, submission.getId()).getSingleResult());
            assertNotNull(entityManager.createNativeQuery("SELECT deleted FROM lti_resource WHERE id = ?").setParameter(1, ltiResource.getId()).getSingleResult());
            assertNotNull(entityManager.createNativeQuery("SELECT deleted FROM lti_launch WHERE id = ?").setParameter(1, ltiLaunch.getId()).getSingleResult());
            assertNotNull(entityManager.createNativeQuery("SELECT deleted FROM lti_line_item WHERE id = ?").setParameter(1, ltiLineItem.getId()).getSingleResult());
            assertNotNull(entityManager.createNativeQuery("SELECT deleted FROM lti_score WHERE id = ?").setParameter(1, ltiScore.getId()).getSingleResult());
            assertNotNull(entityManager.createNativeQuery("SELECT deleted FROM \"group\" WHERE id = ?").setParameter(1, group.getId()).getSingleResult());

            // entities that should only have relation deleted
            assertEquals(0, entityManager.find(Milestone.class, milestone.getId()).getAssignments().size());
            assertEquals(0, entityManager.find(EducationalAlignment.class, educationalAlignment.getId()).getAssignments().size());
            assertEquals(0, entityManager.find(Material.class, material.getId()).getAssignments().size());
            assertNotNull(getGradingSchemeNumerical(entityManager));
            assertEquals(0, getStudent(entityManager).getAssignments().size());
            assertEquals(0, getTeacher(entityManager).getCreatedAssignments().size());
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnDeleteWhenUserNotATeacherInJourney() throws Exception {
        User teacher = createTeacher();
        Milestone milestone = milestoneWithReferences();
        Assignment assignment = createAssignment()
            .creator(teacher)
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone);

        transactionHelper.withTransaction(() -> {
            milestone
                .creator(teacher)
                .getJourney()
                .teachers(new HashSet<>())
                .students(new HashSet<>())
                .addStudent(getTeacher(entityManager))
                .addTeacher(teacher)
                .creator(teacher);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(delete(API_ASSIGNMENTS + "/{id}", assignment.getId())
                .contentType(APPLICATION_JSON))
            .andExpect(status().isForbidden())
            .andExpect(jsonPath("$.detail").value("403 FORBIDDEN \"Current user is not a teacher in the journey!\""));
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldHideHiddenAssignmentsFromStudentWhenAssignmentStudentsEmpty() throws Exception {
        Milestone milestone = milestoneWithReferences();
        Assignment hiddenAssignment = createAssignment();
        transactionHelper.withNewTransaction(() -> {
            User student = getStudent(entityManager);
            milestone.getJourney().addStudent(student);
            Assignment shownAssignment = createAssignment()
                .creator(getTeacher(entityManager))
                .gradingScheme(getGradingSchemeNumerical(entityManager))
                .milestone(milestone);
            hiddenAssignment
                .creator(getTeacher(entityManager))
                .gradingScheme(getGradingSchemeNumerical(entityManager))
                .milestone(milestone)
                .published(null);
            persistCreatedEntities(entityManager);
            shownAssignment.calculateStates();
            hiddenAssignment.calculateStates();
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get(API_ASSIGNMENTS)
                .param("states", Arrays.stream(EntityState.values()).map(Object::toString).collect(joining(",")))
                .accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("length()").value(1))
            .andExpect(jsonPath("$.[0].hidden").value(false));
        mockMvc.perform(get(API_ASSIGNMENTS + "/{id}", hiddenAssignment.getId())
                .accept(APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldHideHiddenAssignmentsFromStudentWhenStudentInAssignmentStudents() throws Exception {
        Milestone milestone = milestoneWithReferences();
        Assignment hiddenAssignment = createAssignment();
        transactionHelper.withNewTransaction(() -> {
            User student = getStudent(entityManager);
            milestone.getJourney().addStudent(student);
            Assignment assignment1 = createAssignment()
                .creator(getTeacher(entityManager))
                .gradingScheme(getGradingSchemeNumerical(entityManager))
                .milestone(milestone)
                .addStudent(student);
            Assignment assignment2 = createAssignment()
                .creator(getTeacher(entityManager))
                .gradingScheme(getGradingSchemeNumerical(entityManager))
                .milestone(milestone)
                .addStudent(student)
                .published(now().plusSeconds(360000L));
            hiddenAssignment
                .creator(getTeacher(entityManager))
                .gradingScheme(getGradingSchemeNumerical(entityManager))
                .milestone(milestone)
                .published(null)
                .addStudent(student);
            persistCreatedEntities(entityManager);
            assignment1.calculateStates();
            assignment2.calculateStates();
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get(API_ASSIGNMENTS)
                .param("states", Arrays.stream(EntityState.values()).map(Object::toString).collect(joining(",")))
                .accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("length()").value(1))
            .andExpect(jsonPath("$.[0].hidden").value(false));
        mockMvc.perform(get(API_ASSIGNMENTS + "/{id}", hiddenAssignment.getId())
                .accept(APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotHideHiddenAssignmentsFromTeacher() throws Exception {
        Milestone milestone = milestoneWithReferences();
        Assignment assignment1 = createAssignment()
            .requiresSubmission(true)
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone);
        Assignment assignment2 = createAssignment()
            .requiresSubmission(true)
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone)
            .published(now().plusSeconds(360000L));
        Assignment hiddenAssignment = createAssignment()
            .requiresSubmission(true)
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone)
            .published(null);
        transactionHelper.withNewTransaction(() -> {
            milestone.getJourney()
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
            assignment1.calculateStates();
            assignment2.calculateStates();
            hiddenAssignment.calculateStates();
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get(API_ASSIGNMENTS)
                .param("states", Arrays.stream(EntityState.values()).map(Object::toString).collect(joining(",")))
                .accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("length()").value(3))
            .andExpect(jsonPath("$.[*].hidden").value(hasItems(true, false)));
        mockMvc.perform(get(API_ASSIGNMENTS + "/{id}", hiddenAssignment.getId())
                .accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("hidden").value(true));
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldHideNonHiddenAssignmentFromStudentWhenMilestoneIsHidden() throws Exception {
        Milestone milestone = milestoneWithReferences()
            .published(null);
        Assignment assignment = createAssignment()
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone);
        transactionHelper.withNewTransaction(() -> {
            User student = getStudent(entityManager);
            milestone.getJourney()
                .addStudent(student)
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get(API_ASSIGNMENTS)
                .param("states", Arrays.stream(EntityState.values()).map(Object::toString).collect(joining(",")))
                .accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("length()").value(0));
        mockMvc.perform(get(API_ASSIGNMENTS + "/{id}", assignment.getId())
                .accept(APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnGetAssignmentWhenTeacherNotInJourney() throws Exception {
        Milestone milestone = milestoneWithReferences();
        milestone.getJourney().setTeachers(null);
        Assignment assignment = createAssignment()
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone);
        transactionHelper.withNewTransaction(() -> {
            milestone.getJourney().addStudent(getStudent(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get(API_ASSIGNMENTS + "/{id}", assignment.getId()))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldThrowOnGetAssignmentWhenStudentNotInJourney() throws Exception {
        Milestone milestone = milestoneWithReferences();
        milestone.getJourney()
            .students(null)
            .teachers(null);
        Assignment assignment = createAssignment()
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone);
        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get(API_ASSIGNMENTS + "/{id}", assignment.getId()))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldGetEmptyResponseOnGetAllAssignmentsWhenStudentNotInJourney() throws Exception {
        Milestone milestone = milestoneWithReferences();
        Assignment assignment = createAssignment();
        Submission submission = createSubmission();
        milestone.getJourney().setStudents(null);
        milestone.addAssignment(assignment
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .addSubmission(submission));
        createSubmissionFeedback()
            .student(getStudent(entityManager))
            .submission(submission)
            .grade(null)
            .creator(getTeacher(entityManager));
        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));
        String today = ofPattern("yyyy-MM-dd").format(LocalDateTime.now());
        String todayPlus7 = ofPattern("yyyy-MM-dd").format(LocalDateTime.now().plusDays(7L));

        mockMvc.perform(get(API_ASSIGNMENTS + "?fromDate=" + today + "&toDate=" + todayPlus7 + "&states=IN_PROGRESS,URGENT,NOT_STARTED"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(0));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetEmptyResponseOnGetAllAssignmentsWhenTeacherNotInJourney() throws Exception {
        Milestone milestone = milestoneWithReferences();
        Assignment assignment = createAssignment();
        SubmissionFeedback submissionFeedback = createSubmissionFeedback();
        Submission submission = createSubmission();
        milestone.getJourney().setTeachers(null);
        milestone.addAssignment(assignment
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .addSubmission(submission));
        submissionFeedback
            .submission(submission)
            .student(getStudent(entityManager))
            .grade(null)
            .creator(getTeacher(entityManager));
        transactionHelper.withNewTransaction(() -> {
            milestone.getJourney().addStudent(getStudent(entityManager));
            persistCreatedEntities(entityManager);
        });
        String today = ofPattern("yyyy-MM-dd").format(LocalDateTime.now());
        String todayPlus7 = ofPattern("yyyy-MM-dd").format(LocalDateTime.now().plusDays(7L));

        mockMvc.perform(get(API_ASSIGNMENTS + "?fromDate=" + today + "&toDate=" + todayPlus7 + "&states=IN_PROGRESS,URGENT,NOT_STARTED"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(0));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnUpdateWhenAssignmentNotPresent() throws Exception {
        Milestone milestone = milestoneWithReferences(true);
        Assignment assignment = createAssignment()
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone);
        assignment
            .id(1L)
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .deadline(UPDATED_DEADLINE)
            .flexibleDeadline(UPDATED_FLEXIBLE_DEADLINE);

        mockMvc.perform(put(API_ASSIGNMENTS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentMapper.toDto(assignment))))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnDeleteWhenTeacherNotInJourney() throws Exception {
        Milestone milestone = milestoneWithReferences();
        milestone.getJourney().setTeachers(null);
        Assignment assignment = createAssignment()
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone);
        transactionHelper.withNewTransaction(() -> {
            milestone.getJourney().addStudent(getStudent(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(delete(API_ASSIGNMENTS + "/{id}", assignment.getId())
                .accept(APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldThrowOnDeleteWhenAssignmentNotPresent() throws Exception {
        mockMvc.perform(delete(API_ASSIGNMENTS + "/{id}", 1)
                .accept(APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldUpdateNonRestrictedMaterialOnUpdateWhenCreatedByUser() throws Exception {
        String updatedMaterialTitle = "Updated material title";
        String updatedMaterialDescription = "Updated material description";
        Boolean updatedMaterialRestricted = true;
        String updatedMaterialUrl = "Updated material url";

        Milestone milestone = milestoneWithReferences();
        Material material = createMaterial().restricted(false);
        Assignment assignment = createAssignment()
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone)
            .addMaterial(material);

        transactionHelper.withNewTransaction(() -> {
            milestone.getJourney()
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        material
            .title(updatedMaterialTitle)
            .description(updatedMaterialDescription)
            .restricted(updatedMaterialRestricted)
            .url(updatedMaterialUrl);
        assignment
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .deadline(UPDATED_DEADLINE)
            .flexibleDeadline(UPDATED_FLEXIBLE_DEADLINE)
            .materials(Set.of(material));

        mockMvc.perform(put(API_ASSIGNMENTS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentMapper.toDto(assignment))))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.materials.size()").value(1))
            .andExpect(jsonPath("$.materials[0].title").value(updatedMaterialTitle))
            .andExpect(jsonPath("$.materials[0].description").value(updatedMaterialDescription))
            .andExpect(jsonPath("$.materials[0].restricted").value(updatedMaterialRestricted))
            .andExpect(jsonPath("$.materials[0].url").value(updatedMaterialUrl));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotUpdateNonRestrictedMaterialOnUpdateWhenNotCreatedByUser() throws Exception {
        when(springSecurityAuditorAware.getCurrentAuditor()).thenReturn(Optional.of("teacher2"));

        String updatedMaterialTitle = "Updated material title";
        String updatedMaterialDescription = "Updated material description";
        Boolean updatedMaterialRestricted = true;
        String updatedMaterialUrl = "Updated material url";

        Milestone milestone = milestoneWithReferences();
        Material material = createMaterial().restricted(false);
        Assignment assignment = createAssignment()
            .creator(getTeacher(entityManager))
            .gradingScheme(getGradingSchemeNumerical(entityManager))
            .milestone(milestone)
            .addMaterial(material);

        transactionHelper.withNewTransaction(() -> {
            milestone.getJourney()
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        material
            .title(updatedMaterialTitle)
            .description(updatedMaterialDescription)
            .restricted(updatedMaterialRestricted)
            .url(updatedMaterialUrl);
        assignment
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .deadline(UPDATED_DEADLINE)
            .flexibleDeadline(UPDATED_FLEXIBLE_DEADLINE)
            .materials(Set.of(material));

        mockMvc.perform(put(API_ASSIGNMENTS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentMapper.toDto(assignment))))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.materials.size()").value(1))
            .andExpect(jsonPath("$.materials[0].title").value(MATERIAL_TITLE))
            .andExpect(jsonPath("$.materials[0].description").value(MATERIAL_DESCRIPTION))
            .andExpect(jsonPath("$.materials[0].restricted").value(false))
            .andExpect(jsonPath("$.materials[0].url").value(MATERIAL_URL));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotUpdateNonRestrictedMaterialOnCreateWhenNotCreatedByUser() throws Exception {
        when(springSecurityAuditorAware.getCurrentAuditor()).thenReturn(Optional.of("teacher2"));

        String updatedMaterialTitle = "Updated material title";
        String updatedMaterialDescription = "Updated material description";
        Boolean updatedMaterialRestricted = true;
        String updatedMaterialUrl = "Updated material url";

        EducationalLevel educationalLevel = createEducationalLevel();
        EducationalAlignment educationalAlignment = createEducationalAlignment();
        Journey journey = createJourney();
        Milestone milestone = createMilestone();
        Material material = createMaterial().restricted(false);
        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            journey.educationalLevel(educationalLevel)
                .creator(teacher)
                .addTeacher(teacher)
                .addMilestone(milestone
                    .creator(teacher)
                    .addEducationalAlignment(educationalAlignment));
            persistCreatedEntities(entityManager);
        });

        material
            .title(updatedMaterialTitle)
            .description(updatedMaterialDescription)
            .restricted(updatedMaterialRestricted)
            .url(updatedMaterialUrl);

        Assignment assignment = createAssignment();
        assignment
            .creator(User.builder().id(TEACHER_ID).build())
            .milestone(milestone)
            .addStudent(User.builder().id(STUDENT_ID).build())
            .addEducationalAlignment(educationalAlignment)
            .addMaterial(material)
            .gradingScheme(getGradingSchemeNumerical(entityManager));
        material.addAssignments(assignment);

        mockMvc.perform(post(API_ASSIGNMENTS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentMapper.toDto(assignment))))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.materials.size()").value(1))
            .andExpect(jsonPath("$.materials[0].title").value(MATERIAL_TITLE))
            .andExpect(jsonPath("$.materials[0].description").value(MATERIAL_DESCRIPTION))
            .andExpect(jsonPath("$.materials[0].restricted").value(false))
            .andExpect(jsonPath("$.materials[0].url").value(MATERIAL_URL));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldUpdateNonRestrictedMaterialOnCreateWhenCreatedByUser() throws Exception {
        String updatedMaterialTitle = "Updated material title";
        String updatedMaterialDescription = "Updated material description";
        Boolean updatedMaterialRestricted = true;
        String updatedMaterialUrl = "Updated material url";

        EducationalLevel educationalLevel = createEducationalLevel();
        EducationalAlignment educationalAlignment = createEducationalAlignment();
        Journey journey = createJourney();
        Milestone milestone = createMilestone();
        Material material = createMaterial().restricted(false);
        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            journey.educationalLevel(educationalLevel)
                .creator(teacher)
                .addTeacher(teacher)
                .addMilestone(milestone
                    .creator(teacher)
                    .addEducationalAlignment(educationalAlignment));
            persistCreatedEntities(entityManager);
        });
        entityManager.detach(material);
        material
            .title(updatedMaterialTitle)
            .description(updatedMaterialDescription)
            .restricted(updatedMaterialRestricted)
            .url(updatedMaterialUrl);
        Assignment assignment = createAssignment();
        assignment
            .creator(User.builder().id(TEACHER_ID).build())
            .milestone(milestone)
            .addStudent(User.builder().id(STUDENT_ID).build())
            .addEducationalAlignment(educationalAlignment)
            .addMaterial(material)
            .gradingScheme(getGradingSchemeNumerical(entityManager));
        material.addAssignments(assignment);

        mockMvc.perform(post(API_ASSIGNMENTS)
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(assignmentMapper.toDto(assignment))))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$.materials.size()").value(1))
            .andExpect(jsonPath("$.materials[0].title").value(updatedMaterialTitle))
            .andExpect(jsonPath("$.materials[0].description").value(updatedMaterialDescription))
            .andExpect(jsonPath("$.materials[0].restricted").value(updatedMaterialRestricted))
            .andExpect(jsonPath("$.materials[0].url").value(updatedMaterialUrl));
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldGetAllAssignmentsWhereStudentIsInAssignmentStudents() throws Exception {
        Milestone milestone = milestoneWithReferences();
        Assignment assignment = createAssignment();
        transactionHelper.withNewTransaction(() -> {
            milestone.addAssignment(assignment
                .creator(getTeacher(entityManager))
                .gradingScheme(getGradingSchemeNumerical(entityManager))
                .addStudent(getStudent(entityManager)));
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });
        String today = ofPattern("yyyy-MM-dd").format(LocalDateTime.now());
        String todayPlus7 = ofPattern("yyyy-MM-dd").format(LocalDateTime.now().plusDays(7L));

        mockMvc.perform(get(API_ASSIGNMENTS + "?fromDate=" + today + "&toDate=" + todayPlus7 + "&states=IN_PROGRESS,URGENT,NOT_STARTED"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(1));
    }

    @Test
    @WithMockCustomUser(value = STUDENT, userId = "-3")
    void shouldGetAllAssignmentsWhenStudentNotInAssignmentAndAssignmentStudentsEmpty() throws Exception {
        Milestone milestone = milestoneWithReferences();
        Assignment assignment = createAssignment();
        transactionHelper.withNewTransaction(() -> {
            milestone.addAssignment(assignment
                .creator(getTeacher(entityManager))
                .gradingScheme(getGradingSchemeNumerical(entityManager))
                .addStudent(getStudent(entityManager)));
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });
        String today = ofPattern("yyyy-MM-dd").format(LocalDateTime.now());
        String todayPlus7 = ofPattern("yyyy-MM-dd").format(LocalDateTime.now().plusDays(7L));

        mockMvc.perform(get(API_ASSIGNMENTS + "?fromDate=" + today + "&toDate=" + todayPlus7 + "&states=IN_PROGRESS,URGENT,NOT_STARTED"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(1));
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldNotGetAssignmentsWhenStudentNotInAssignmentAndAssignmentStudentsNotEmpty() throws Exception {
        Milestone milestone = milestoneWithReferences();
        Assignment assignment = createAssignment();
        User studentWhoIsNotInAssignment = createUser().addAuthority(new Authority().name(STUDENT));
        transactionHelper.withNewTransaction(() -> {
            milestone.getJourney().addStudent(getStudent(entityManager));
            milestone.addAssignment(assignment
                .creator(getTeacher(entityManager))
                .gradingScheme(getGradingSchemeNumerical(entityManager))
                .addStudent(studentWhoIsNotInAssignment));
            persistCreatedEntities(entityManager);
            assignment.calculateStates();
            persistCreatedEntities(entityManager);
        });
        String today = ofPattern("yyyy-MM-dd").format(LocalDateTime.now());
        String todayPlus7 = ofPattern("yyyy-MM-dd").format(LocalDateTime.now().plusDays(7L));

        mockMvc.perform(get(API_ASSIGNMENTS + "?fromDate=" + today + "&toDate=" + todayPlus7 + "&states=IN_PROGRESS,URGENT,NOT_STARTED"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("length()").value(0));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotUpdateResourceLineItemsWhenExistingResourceHasLineItemsAndLtiResourceDTOHasNot() throws Exception {
        Milestone milestone = milestoneWithReferences();
        Assignment assignment = createAssignment();
        assignment.addLtiResource(createLtiResource().addLtiLineItem(createLtiLineItem()));
        transactionHelper.withTransaction(() -> {
            milestone.getJourney()
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            assignment.creator(getTeacher(entityManager)).setMilestone(milestone);
            persistCreatedEntities(entityManager);
        });

        AssignmentDTO assignmentDTO = assignmentMapper.toDto(assignment);
        assignmentDTO.getLtiResources().stream().findFirst().orElseThrow().setLineItems(null);

        mockMvc.perform(put(API_ASSIGNMENTS)
                .content(convertObjectToJsonBytes(assignmentDTO, true))
                .contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk());

        transactionHelper.withTransaction(() -> {
            LtiResource savedLtiResource = ltiResourceJpaRepository.findAll().stream().findFirst().orElseThrow();
            assertEquals(1, savedLtiResource.getLineItems().size());
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldFindGroupAssignmentsBySearch() throws Exception {
        Milestone milestone = milestoneWithReferences();
        Milestone differentJourneyMilestone = milestoneWithReferences();
        differentJourneyMilestone.getJourney().setJourneySignupCodes(emptySet());
        Group group1 = createGroup().setName("Group 1");
        Group group2 = createGroup().setName("Group 2");
        Assignment assignment1 = createAssignment()
            .title("First assignment")
            .setGroups(Set.of(group1));
        Assignment assignment2 = createAssignment()
            .title("Second assignment");
        Assignment assignment3 = createAssignment()
            .title("Third assignment")
            .setGroups(Set.of(group2));

        transactionHelper.withTransaction(() -> {
            milestone.getJourney()
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            assignment1
                .creator(getTeacher(entityManager))
                .setMilestone(milestone);
            assignment2
                .creator(getTeacher(entityManager))
                .setMilestone(milestone);
            assignment3
                .creator(getTeacher(entityManager))
                .setMilestone(differentJourneyMilestone);
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get(API_GROUP_ASSIGNMENTS)
                .param("journeyId", String.valueOf(milestone.getJourney().getId()))
                .param("search", "assignment"))
            .andExpect(jsonPath("length()").value(1))
            .andExpect(jsonPath("$.[0].title").value("First assignment"));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldAddNewStudentInGroupToSubmissionAuthorsIfSubmissionExists() throws Exception {
        Milestone milestone = milestoneWithReferences();
        User student1 = createStudent();

        Submission submission = createSubmission().addAuthor(student1);

        Group group1 = createGroup()
            .setName("Group 1")
            .addStudent(student1)
            .setSubmissions(Set.of(submission));

        Assignment assignment1 = createAssignment()
            .title("First assignment")
            .addSubmission(submission)
            .setGroups(Set.of(group1));

        submission.setAssignment(assignment1);
        submission.setGroup(group1);

        transactionHelper.withTransaction(() -> {
            milestone.getJourney()
                .addStudent(student1)
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            assignment1
                .creator(getTeacher(entityManager))
                .setMilestone(milestone);
            persistCreatedEntities(entityManager);

        });

        group1.addStudent(getStudent(entityManager));
        assignment1.setGroups(Set.of(group1));

        AssignmentDTO assignmentDTO = assignmentMapper.toDto(assignment1);

        mockMvc.perform(put(API_ASSIGNMENTS)
                .content(convertObjectToJsonBytes(assignmentDTO))
                .contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk());

        transactionHelper.withTransaction(() -> {
            Assignment updatedAssignment = assignmentJpaRepository.getById(assignment1.getId());
            Group groupWithSubmission = updatedAssignment.getGroups().stream()
                .filter(group -> group.getId().equals(group1.getId())).findFirst().orElseThrow();
            assertEquals(2, groupWithSubmission.getSubmissions().stream().findFirst()
                .orElse(new Submission()).getAuthors().size());
        });
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetTemplateJourneyAssignment() throws Exception {
        User teacher = createTeacher();
        Milestone milestone = milestoneWithReferences().creator(teacher);
        JourneyTeacher journeyTeacher = createJourneyTeacher()
            .journey(milestone.getJourney())
            .user(teacher);
        milestone.getJourney()
            .creator(teacher)
            .template(true)
            .teachers(Set.of(journeyTeacher))
            .students(Set.of());
        Assignment assignment = createAssignment()
            .milestone(milestone)
            .creator(teacher);

        transactionHelper.withTransaction(() -> {
            assignment.gradingScheme(getGradingSchemeNumerical(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/assignments/" + assignment.getId())
                .param("template", "true"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.title").value(assignment.getTitle()));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetWithPreviousAndNextIds() throws Exception {
        User teacher = getTeacher(entityManager);
        Milestone milestone = milestoneWithReferences().creator(teacher);
        milestone.getJourney()
            .creator(teacher)
            .setStudents(Set.of());
        Assignment assignment = createAssignment()
            .milestone(milestone)
            .creator(teacher);
        Assignment previousAssignment = createAssignment()
            .milestone(milestone)
            .creator(teacher)
            .deadline(now());
        Assignment nextAssignment = createAssignment()
            .milestone(milestone)
            .creator(teacher)
            .deadline(now().plus(2, DAYS));

        transactionHelper.withTransaction(() -> {
            milestone.getJourney().addTeacher(getTeacher(entityManager));
            assignment.gradingScheme(getGradingSchemeNumerical(entityManager));
            previousAssignment.gradingScheme(getGradingSchemeNumerical(entityManager));
            nextAssignment.gradingScheme(getGradingSchemeNumerical(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/assignments/" + assignment.getId())
                .param("withPreviousAndNextIds", "true"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.title").value(assignment.getTitle()))
            .andExpect(jsonPath("$.previousAssignmentId").value(previousAssignment.getId()))
            .andExpect(jsonPath("$.nextAssignmentId").value(nextAssignment.getId()));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotGetNonTemplateJourneyAssignment() throws Exception {
        User teacher = createTeacher();
        Milestone milestone = milestoneWithReferences().creator(teacher);
        JourneyTeacher journeyTeacher = createJourneyTeacher()
            .journey(milestone.getJourney())
            .user(teacher);
        milestone.getJourney()
            .creator(teacher)
            .template(false)
            .teachers(Set.of(journeyTeacher))
            .students(Set.of());
        Assignment assignment = createAssignment()
            .milestone(milestone)
            .creator(teacher);

        transactionHelper.withTransaction(() -> {
            assignment.gradingScheme(getGradingSchemeNumerical(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/assignments/" + assignment.getId())
                .param("template", "true"))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldNotGetTemplateJourneyAssignmentWhenTemplateUndefinedInQuery() throws Exception {
        User teacher = createTeacher();
        Milestone milestone = milestoneWithReferences().creator(teacher);
        JourneyTeacher journeyTeacher = createJourneyTeacher()
            .journey(milestone.getJourney())
            .user(teacher);
        milestone.getJourney()
            .creator(teacher)
            .template(true)
            .teachers(Set.of(journeyTeacher))
            .students(Set.of());
        Assignment assignment = createAssignment()
            .milestone(milestone)
            .creator(teacher);

        transactionHelper.withTransaction(() -> {
            assignment.gradingScheme(getGradingSchemeNumerical(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(get("/api/assignments/" + assignment.getId()))
            .andExpect(status().isNotFound());
    }
}
