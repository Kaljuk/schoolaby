package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.Journey;
import com.schoolaby.domain.Notification;
import com.schoolaby.domain.PatchNotificationDTO;
import com.schoolaby.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static com.schoolaby.common.TestCases.*;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.domain.NotificationType.GENERAL;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class NotificationControllerIT extends BaseIntegrationTest {
    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldGetAllNotificationsForCurrentUser() throws Exception {
        User teacher = getTeacher(entityManager);
        User student = getStudent(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .setSubject(getSubject(entityManager))
            .creator(teacher)
            .addStudent(student);
        createNotification()
            .creator(student)
            .journey(journey)
            .recipient(teacher);
        createNotification()
            .creator(teacher)
            .journey(journey)
            .recipient(student);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/notifications")
            .contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].message").value(NOTIFICATION_MESSAGE))
            .andExpect(jsonPath("$.[0].link").value(NOTIFICATION_LINK))
            .andExpect(jsonPath("$.[0].subject.label").value(SUBJECT_LABEL))
            .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldGetAllNotificationsByTypeForCurrentUser() throws Exception {
        User teacher = getTeacher(entityManager);
        User student = getStudent(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addStudent(student);
        createNotification()
            .creator(teacher)
            .type(GENERAL)
            .message(NOTIFICATION_MESSAGE)
            .journey(journey)
            .recipient(student);
        createNotification()
            .creator(student)
            .message("{JOINED_JOURNEY}")
            .journey(journey)
            .recipient(teacher);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get(String.format("/api/notifications?type=%s", GENERAL))
            .contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$.[0].type").value(GENERAL.toString()))
            .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldPatchNotificationsForCurrentUser() throws Exception {
        User teacher = getTeacher(entityManager);
        User student = getStudent(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addStudent(student);
        Notification notification1 = createNotification()
            .creator(teacher)
            .message("{RECEIVED_MESSAGE}")
            .journey(journey)
            .recipient(student);
        Notification notification2 = createNotification()
            .creator(teacher)
            .message("{RECEIVED_MESSAGE}")
            .journey(journey)
            .recipient(student);
        persistCreatedEntities(entityManager);

        List<PatchNotificationDTO> patchNotificationDTOS = new ArrayList<>();

        PatchNotificationDTO patchNotificationDTO = new PatchNotificationDTO();
        Instant read1 = Instant.now();
        patchNotificationDTO.setId(notification1.getId());
        patchNotificationDTO.setRead(read1);
        patchNotificationDTOS.add(patchNotificationDTO);

        patchNotificationDTO = new PatchNotificationDTO();
        Instant read2 = Instant.now();
        patchNotificationDTO.setId(notification2.getId());
        patchNotificationDTO.setRead(read2);
        patchNotificationDTOS.add(patchNotificationDTO);

        mockMvc.perform(patch("/api/notifications")
            .contentType(APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(patchNotificationDTOS)))
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$.[0].id").value(notification1.getId()))
            .andExpect(jsonPath("$.[0].read").value(read1.toString()))
            .andExpect(jsonPath("$.[1].id").value(notification2.getId()))
            .andExpect(jsonPath("$.[1].read").value(read2.toString()))
            .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldThrowOnPatchNotificationsWhenCurrentUserNotInJourney() throws Exception {
        User teacher = getTeacher(entityManager);
        User student = getStudent(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher)
            .addStudent(student);
        Notification notification1 = createNotification()
            .creator(teacher)
            .message("{RECEIVED_MESSAGE}")
            .journey(journey)
            .recipient(student);
        Notification notification2 = createNotification()
            .creator(teacher)
            .message("{RECEIVED_MESSAGE}")
            .journey(journey)
            .recipient(student);
        persistCreatedEntities(entityManager);

        List<PatchNotificationDTO> patchNotificationDTOS = new ArrayList<>();

        PatchNotificationDTO patchNotificationDTO = new PatchNotificationDTO();
        Instant read1 = Instant.now();
        patchNotificationDTO.setId(notification1.getId());
        patchNotificationDTO.setRead(read1);
        patchNotificationDTOS.add(patchNotificationDTO);

        patchNotificationDTO = new PatchNotificationDTO();
        Instant read2 = Instant.now();
        patchNotificationDTO.setId(notification2.getId());
        patchNotificationDTO.setRead(read2);
        patchNotificationDTOS.add(patchNotificationDTO);

        mockMvc.perform(patch("/api/notifications")
            .contentType(APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(patchNotificationDTOS)))
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$.[0].id").value(notification1.getId()))
            .andExpect(jsonPath("$.[0].read").value(read1.toString()))
            .andExpect(jsonPath("$.[1].id").value(notification2.getId()))
            .andExpect(jsonPath("$.[1].read").value(read2.toString()))
            .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldGetEmptyResponseOnGetAllNotificationsWhenCurrentUserNotInJourney() throws Exception {
        User teacher = getTeacher(entityManager);
        User student = getStudent(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        createNotification()
            .creator(student)
            .journey(journey)
            .recipient(teacher);
        createNotification()
            .creator(teacher)
            .journey(journey)
            .recipient(student);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/notifications")
            .contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", hasSize(0)))
            .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    void shouldGetEmptyResponseOnGetAllNotificationsByTypeWhenCurrentUserNotInJourney() throws Exception {
        User teacher = getTeacher(entityManager);
        User student = getStudent(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(teacher);
        createNotification()
            .creator(teacher)
            .type(GENERAL)
            .message(NOTIFICATION_MESSAGE)
            .journey(journey)
            .recipient(student);
        createNotification()
            .creator(student)
            .message("{JOINED_JOURNEY}")
            .journey(journey)
            .recipient(teacher);
        persistCreatedEntities(entityManager);

        mockMvc.perform(get(String.format("/api/notifications?type=%s", GENERAL))
            .contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$", hasSize(0)))
            .andExpect(status().isOk());
    }
}
