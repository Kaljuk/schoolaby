package com.schoolaby.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.jwk.JWK;
import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.common.LtiContextHelper;
import com.schoolaby.common.socket.TestWebSocketClient;
import com.schoolaby.config.lti.LtiAdvantageProperties;
import com.schoolaby.domain.*;
import com.schoolaby.repository.LineItemRepository;
import com.schoolaby.service.dto.lti.LtiScoreDTO;
import com.schoolaby.service.dto.lti.claims.LtiContextToken;
import com.schoolaby.service.dto.lti.claims.ResourceLink;
import com.schoolaby.service.dto.lti.external.LtiExtScoreDTO;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.concurrent.BlockingQueue;

import static com.schoolaby.common.TestCases.*;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.config.socket.WebsocketSecurityConfiguration.SUBSCRIBE_LTI_SCORE_ENDPOINT;
import static com.schoolaby.domain.enumeration.LtiConfigType.JOURNEY;
import static com.schoolaby.domain.enumeration.LtiConfigType.PLATFORM;
import static com.schoolaby.service.dto.lti.claims.LtiContextToken.LtiMessageType.DEEP_LINKING;
import static com.schoolaby.web.rest.LtiAdvantageController.APPLICATION_VND_IMS_LIS_V_1_SCORE_JSON;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrlPattern;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class LtiAdvantageControllerIT extends BaseIntegrationTest {
    private Journey journey;
    private LtiApp app;
    private LtiLineItem ltiLineItem;
    private User student;
    private LtiResource ltiResource;
    private Assignment assignment;
    @Autowired
    private JWK ltiJwk;
    @Autowired
    private LtiContextHelper contextHelper;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private LineItemRepository lineItemRepository;
    @Autowired
    private LtiAdvantageProperties properties;
    @Autowired
    private TestWebSocketClient testWebSocketClient;
    @LocalServerPort
    private String portLocal;


    @BeforeEach
    void setup() {
        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            student = getStudent(entityManager);
            EducationalLevel educationalLevel = createEducationalLevel();
            EducationalAlignment educationalAlignment = createEducationalAlignment();
            journey = createJourney();
            Milestone milestone = createMilestone();
            assignment = createAssignment();
            ltiResource = createLtiResource();
            ltiLineItem = createLtiLineItem();

            journey.educationalLevel(educationalLevel)
                .creator(teacher)
                .addMilestone(milestone
                    .creator(teacher)
                    .addEducationalAlignment(educationalAlignment)
                    .addAssignment(assignment
                        .creator(teacher)
                        .addLtiResource(ltiResource
                            .addLtiLineItem(ltiLineItem))
                        .addStudent(student)
                        .addEducationalAlignment(educationalAlignment)
                        .gradingScheme(getGradingSchemeNumerical(entityManager))));

            app = createLtiApp();
        });
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldRedirectDeepLinkingRequestToToolLoginInitiationForJourneyConfig() throws Exception {
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setJourney(journey);
        ltiConfig.setType(JOURNEY);
        ltiConfig.setLtiApp(app);
        persistCreatedEntities(entityManager);

        ResultActions resultActions = mockMvc.perform(post("/api/lti-advantage/deep-linking")
            .param("appId", app.getId().toString())
            .param("journeyId", journey.getId().toString()));
        assertDeepLinkingResponse(resultActions);
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldRedirectDeepLinkingRequestToToolLoginInitiationForPlatformConfig() throws Exception {
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(PLATFORM);
        ltiConfig.setLtiApp(app);
        persistCreatedEntities(entityManager);

        ResultActions resultActions = mockMvc.perform(post("/api/lti-advantage/deep-linking")
            .param("appId", app.getId().toString())
            .param("journeyId", journey.getId().toString()));
        assertDeepLinkingResponse(resultActions);
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldInitiateDeepLinkingWhenAnotherAppPlatformConfigExists() throws Exception {
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(PLATFORM);
        ltiConfig.setLtiApp(createLtiApp());

        LtiConfig ltiConfig2 = createLtiConfig();
        ltiConfig2.setJourney(journey);
        ltiConfig2.setType(JOURNEY);
        ltiConfig2.setLtiApp(app);
        persistCreatedEntities(entityManager);

        ResultActions resultActions = mockMvc.perform(post("/api/lti-advantage/deep-linking")
            .param("appId", app.getId().toString())
            .param("journeyId", journey.getId().toString()));
        assertDeepLinkingResponse(resultActions);
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldAuthorize() throws Exception {
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setJourney(journey);
        ltiConfig.setType(JOURNEY);
        ltiConfig.setLtiApp(app);

        persistCreatedEntities(entityManager);
        String loginHint = new LtiContextToken()
            .journeyId(journey.getId().toString())
            .userId(TEACHER_ID.toString())
            .role("ROLE_TEACHER")
            .messageType(DEEP_LINKING)
            .resourceLink(new ResourceLink())
            .toToken(ltiJwk.toRSAKey().toPrivateKey(), properties.getTokenValidityInSeconds());

        mockMvc.perform(get("/api/lti-advantage/authorize")
            .param("login_hint", TEACHER_ID.toString())
            .param("lti_message_hint", loginHint)
            .param("redirect_uri", app.getDeepLinkingUrl()))
            .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    void shouldAuthorizeWhenAnotherAppPlatformConfigExists() throws Exception {
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(PLATFORM);
        ltiConfig.setLtiApp(createLtiApp()
            .setDeepLinkingUrl("http://localhost/deep-link")
            .setRedirectHost("localhost"));

        LtiConfig ltiConfig2 = createLtiConfig();
        ltiConfig2.setJourney(journey);
        ltiConfig2.setType(JOURNEY);
        ltiConfig2.setLtiApp(app);
        persistCreatedEntities(entityManager);
        String loginHint = new LtiContextToken()
            .journeyId(journey.getId().toString())
            .userId(TEACHER_ID.toString())
            .role("ROLE_TEACHER")
            .messageType(DEEP_LINKING)
            .resourceLink(new ResourceLink())
            .toToken(ltiJwk.toRSAKey().toPrivateKey(), properties.getTokenValidityInSeconds());

        mockMvc.perform(get("/api/lti-advantage/authorize")
            .param("login_hint", TEACHER_ID.toString())
            .param("lti_message_hint", loginHint)
            .param("redirect_uri", app.getDeepLinkingUrl()))
            .andExpect(status().isOk());
    }

    @Test
    @WithMockCustomUser(userId = "-3")
    void shouldCreateScore() throws Exception {
        String authToken = testWebSocketClient.createStudentAuthToken();
        testWebSocketClient.connect(authToken, portLocal);
        BlockingQueue<LtiScoreDTO> blockingQueue = testWebSocketClient.subscribe(SUBSCRIBE_LTI_SCORE_ENDPOINT, LtiScoreDTO.class);
        transactionHelper.withTransaction(() -> {
            LtiConfig ltiConfig = createLtiConfig();
            ltiConfig.setJourney(journey);
            ltiConfig.setType(JOURNEY);
            ltiConfig.setLtiApp(app);
            journey.addLtiConfig(ltiConfig);
            ltiLineItem = createLtiLineItem().setLtiResource(ltiResource);
            ltiResource.addLtiLineItem(ltiLineItem);
            ltiResource.addLtiLaunch(createLtiLaunch().user(student));
            ltiResource.setSyncGrade(true);

            persistCreatedEntities(entityManager);
        });

        String context = contextHelper.toBase64Url(journey.getId());
        Instant scoreTimestamp = Instant.now();

        mockMvc.perform(post("/api/lti-advantage/{context}/lineitems/{lineItemId}/scores", context, ltiLineItem.getId())
            .contentType(APPLICATION_VND_IMS_LIS_V_1_SCORE_JSON)
            .content(objectMapper.writeValueAsBytes(new LtiExtScoreDTO()
                .setTimestamp(scoreTimestamp)
                .setScoreGiven(5.0)
                .setScoreMaximum(10.0)
                .setUserId(student.getId().toString())
                .setActivityProgress(LTI_SCORE_ACTIVITY_PROGRESS)
                .setGradingProgress(LTI_SCORE_GRADING_PROGRESS))))
            .andExpect(status().isOk());

        transactionHelper.withNewTransaction(() -> {
            ltiLineItem = lineItemRepository.getOne(ltiLineItem.getId());
            assertEquals(1, ltiLineItem.getScores().size());
            LtiScore score = ltiLineItem.getScores().stream().iterator().next();
            assertEquals(5.0, score.getScoreGiven());
            assertEquals(10.0, score.getScoreMaximum());
            assertEquals(scoreTimestamp, score.getTimestamp());
            assertEquals(student.getId(), score.getUser().getId());
            assertEquals(LTI_SCORE_ACTIVITY_PROGRESS, score.getActivityProgress());
            assertEquals(LTI_SCORE_GRADING_PROGRESS, score.getGradingProgress());

            LtiScoreDTO ltiScoreDTO = null;
            try {
                ltiScoreDTO = blockingQueue.poll(1, SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            assertNotNull(ltiScoreDTO);
            assertEquals(ltiLineItem.getLtiResource().getId(), ltiScoreDTO.getResourceId());
            assertEquals(ltiLineItem.getLtiResource().getTitle(), ltiScoreDTO.getTitle());
            assertEquals(STUDENT_ID, ltiScoreDTO.getUserId());
            assertEquals(score.getScoreGiven().toString(), ltiScoreDTO.getScore());
            assertEquals(score.getScoreMaximum().toString(), ltiScoreDTO.getScoreMax());
        });
    }

    @Test
    @WithUserDetails(STUDENT_LOGIN)
    void shouldCreateLaunch() throws Exception {
        String authToken = testWebSocketClient.createStudentAuthToken();
        testWebSocketClient.connect(authToken, portLocal);
        BlockingQueue<LtiScoreDTO> blockingQueue = testWebSocketClient.subscribe(SUBSCRIBE_LTI_SCORE_ENDPOINT, LtiScoreDTO.class);

        LtiApp ltiApp = createLtiApp();
        LtiConfig ltiConfig = createLtiConfig();
        ltiConfig.setType(JOURNEY);
        journey.addLtiConfig(ltiConfig);
        ltiConfig.setJourney(journey);
        LtiResource ltiResource = createLtiResource()
            .setLtiApp(ltiApp.addConfig(ltiConfig))
            .setAssignment(assignment)
            .setSyncGrade(true);
        transactionHelper.withTransaction(() -> {
            journey
                .addStudent(getStudent(entityManager))
                .addTeacher(getTeacher(entityManager));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(post("/api/lti-advantage/launch")
            .param("resourceId", ltiResource.getId().toString())
        );

        LtiScoreDTO ltiScoreDTO = blockingQueue.poll(1, SECONDS);
        assertNotNull(ltiScoreDTO);
        assertEquals(ltiResource.getId(), ltiScoreDTO.getResourceId());
        assertEquals(ltiResource.getTitle(), ltiScoreDTO.getTitle());
        assertEquals(STUDENT_ID, ltiScoreDTO.getUserId());
        assertNull(ltiScoreDTO.getScore());
        assertNull(ltiScoreDTO.getScoreMax());
    }

    private void assertDeepLinkingResponse(ResultActions resultActions) throws Exception {
        resultActions
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrlPattern(app.getLoginInitiationUrl() + "?*"));
    }
}
