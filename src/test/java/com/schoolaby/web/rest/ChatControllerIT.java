package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.Chat;
import com.schoolaby.domain.Journey;
import com.schoolaby.domain.Submission;
import com.schoolaby.domain.User;
import com.schoolaby.repository.ChatJpaRepository;
import com.schoolaby.service.dto.ChatDTO;
import com.schoolaby.service.mapper.ChatMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.schoolaby.common.TestCases.*;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.domain.enumeration.EntityType.JOURNEY;
import static com.schoolaby.domain.enumeration.EntityType.SUBMISSION;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ChatControllerIT extends BaseIntegrationTest {

    @Autowired
    private EntityManager em;

    @Autowired
    private ChatJpaRepository chatJpaRepository;

    @Autowired
    private ChatMapper chatMapper;

    @Autowired
    private MockMvc restChatMockMvc;

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    public void getAllSubmissionChats() throws Exception {
        Chat chat = createChat();
        Submission submission = createSubmission();
        User student = new User();
        User teacher = createTeacher();
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager))
            .addStudent(student)
            .addMilestone(createMilestone()
                .creator(teacher)
                    .addAssignment(createAssignment()
                        .gradingScheme(getGradingSchemeNumerical(entityManager))
                        .creator(teacher)
                        .addSubmission(submission)));
        student.setId(STUDENT_ID);

        chat.submission(submission)
            .addPerson(student)
            .journey(journey);

        persistCreatedEntities(em);

        restChatMockMvc
            .perform(get("/api/chats?journeyId={journeyId}&entity={entityType}", journey.getId(), SUBMISSION))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chat.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(CHAT_TITLE)));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    public void getAllJourneyChats() throws Exception {
        User student = getStudent(entityManager);
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager))
            .addStudent(student);

        Chat chat = createChat()
            .addPerson(student)
            .journey(journey);

        persistCreatedEntities(em);

        restChatMockMvc
            .perform(get("/api/chats?journeyId={journeyId}&entity={entityType}", journey.getId(),JOURNEY))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chat.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(CHAT_TITLE)));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    public void getAllSubmissionChatsBySubmissionId() throws Exception {
        Chat chat = createChat();
        Submission submission = createSubmission();
        User student = new User();
        User teacher = createTeacher();
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .creator(getTeacher(entityManager))
            .addStudent(student)
            .addMilestone(createMilestone()
                .creator(teacher)
                .addAssignment(createAssignment()
                    .gradingScheme(getGradingSchemeNumerical(entityManager))
                    .creator(teacher)
                    .addSubmission(submission)));
        student.setId(STUDENT_ID);

        chat.submission(submission)
            .addPerson(student)
            .journey(journey);

        persistCreatedEntities(em);

        restChatMockMvc
            .perform(get("/api/chats?submissionId={submissionId}", submission.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].id").value(chat.getId().intValue()))
            .andExpect(jsonPath("$.[0].submissionId").value(submission.getId().intValue()))
            .andExpect(jsonPath("$.[0].title").value(CHAT_TITLE));
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    public void shouldThrowErrorWhenAllParamsAreFilled() throws Exception {
            restChatMockMvc
                .perform(get("/api/chats?submissionId=1&journeyId=1&entity=SUBMISSION"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithUserDetails(STUDENT_LOGIN)
    public void shouldThrowErrorWhenJourneyIdAndEntityAreNotUsedTogether() throws Exception {
        restChatMockMvc
            .perform(get("/api/chats?submissionId=1&entity=SUBMISSION"))
            .andExpect(status().isBadRequest());

        restChatMockMvc
            .perform(get("/api/chats?submissionId=1&journeyId=1"))
            .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithUserDetails(TEACHER_LOGIN)
    public void shouldCreateChat() throws Exception {
        int databaseSizeBeforeCreate = chatJpaRepository.findAll().size();
        User teacher = new User();
        teacher.setId(TEACHER_ID);

        Chat chat = createChat();
        chat.addPerson(teacher);
        ChatDTO chatDTO = chatMapper.toDto(chat);

        restChatMockMvc
            .perform(post("/api/chats").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(chatDTO)))
            .andExpect(status().isCreated());

        List<Chat> chats = chatJpaRepository.findAll();
        assertThat(chats).hasSize(databaseSizeBeforeCreate + 1);
        Chat testChat = chats.get(chats.size() - 1);
        assertThat(testChat.getTitle()).isEqualTo(CHAT_TITLE);
    }
}
