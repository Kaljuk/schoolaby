package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.School;
import com.schoolaby.domain.UploadedFile;
import org.junit.jupiter.api.Test;
import org.springframework.security.test.context.support.WithUserDetails;

import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.domain.enumeration.Country.ESTONIA;
import static com.schoolaby.domain.enumeration.Country.TANZANIA;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class SchoolControllerIT extends BaseIntegrationTest {
    private static final String API_SCHOOLS = "/api/schools";

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetBySearch() throws Exception {
        School school = createSchool();
        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get(API_SCHOOLS)
            .param("search", "Test")
            .param("country", "Estonia"))
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*].ehisId").value(school.getEhisId()))
            .andExpect(jsonPath("$[*].regNr").value(school.getRegNr()))
            .andExpect(jsonPath("$[*].name").value(school.getName()));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldGetUploadedFile() throws Exception {
        UploadedFile uploadedFile = createUploadedFile();
        createSchool().uploadedFile(uploadedFile);
        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get(API_SCHOOLS)
                .param("search", "Test")
                .param("country", "Estonia"))
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*].uploadedFile.originalName").value(uploadedFile.getOriginalName()))
            .andExpect(jsonPath("$[*].uploadedFile.uniqueName").value(uploadedFile.getUniqueName()))
            .andExpect(jsonPath("$[*].uploadedFile.extension").value(uploadedFile.getExtension()))
            .andExpect(jsonPath("$[*].uploadedFile.type").value(uploadedFile.getType()));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldOnlyReturnSchoolsMatchingSearch() throws Exception {
        createSchool();
        School school2 = createSchool().ehisId("ehis_id2").name("Second test school");
        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get(API_SCHOOLS)
            .param("search", "second")
            .param("country", "Estonia"))
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[*].ehisId").value(school2.getEhisId()))
            .andExpect(jsonPath("$[*].regNr").value(school2.getRegNr()))
            .andExpect(jsonPath("$[*].name").value(school2.getName()));
    }

    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void shouldFindSelectedCountrySchools() throws Exception {
        School estonianSchool = createSchool().name("Estonian school");
        School tanzanianSchool = createSchool().ehisId("ehis_id2").name("Tanzanian school").country(TANZANIA.getValue());
        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get(API_SCHOOLS)
            .param("search", "school")
            .param("country", ESTONIA.getValue()))
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[*].ehisId").value(estonianSchool.getEhisId()))
            .andExpect(jsonPath("$[*].regNr").value(estonianSchool.getRegNr()))
            .andExpect(jsonPath("$[*].name").value(estonianSchool.getName()));

        mockMvc.perform(get(API_SCHOOLS)
            .param("search", "school")
            .param("country", TANZANIA.getValue()))
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(1)))
            .andExpect(jsonPath("$[*].ehisId").value(tanzanianSchool.getEhisId()))
            .andExpect(jsonPath("$[*].regNr").value(tanzanianSchool.getRegNr()))
            .andExpect(jsonPath("$[*].name").value(tanzanianSchool.getName()));
    }
}
