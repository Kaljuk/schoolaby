package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.Authority;
import com.schoolaby.domain.Journey;
import com.schoolaby.domain.JourneySignupCode;
import com.schoolaby.domain.User;
import com.schoolaby.service.dto.JourneySignupCodeDTO;
import org.junit.jupiter.api.Test;
import org.springframework.security.test.context.support.WithUserDetails;

import java.util.Set;

import static com.schoolaby.common.TestCases.getTeacher;
import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.web.rest.TestUtil.convertObjectToJsonBytes;
import static java.util.Collections.emptySet;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class JourneySignupCodeControllerIT extends BaseIntegrationTest {
    @Test
    @WithUserDetails(TEACHER_LOGIN)
    void createOnetimeSignupCode() throws Exception {
        String signUpCode = randomAlphanumeric(4, 8).toLowerCase();
        Journey journey = createJourney()
            .educationalLevel(createEducationalLevel())
            .setJourneySignupCodes(emptySet());

        transactionHelper.withTransaction(() -> {
            User teacher = getTeacher(entityManager);
            journey.creator(teacher);
            journey.addTeacher(teacher);
            journey.setJourneySignupCodes(Set.of(new JourneySignupCode()
                .journey(journey)
                .signUpCode(signUpCode)
                .authority(new Authority().name(TEACHER))));
            persistCreatedEntities(entityManager);
        });

        mockMvc.perform(
                post("/api/journeys/signup-code")
                    .contentType(APPLICATION_JSON)
                    .content(convertObjectToJsonBytes(new JourneySignupCodeDTO().setSignUpCode(signUpCode)))
            )
            .andExpect(status().isCreated())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.signUpCode").isString());
    }
}
