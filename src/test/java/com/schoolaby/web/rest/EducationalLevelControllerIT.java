package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.EducationalLevel;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import static com.schoolaby.common.TestObjects.createEducationalLevel;
import static com.schoolaby.common.TestObjects.persistCreatedEntities;
import static com.schoolaby.domain.enumeration.Country.ESTONIA;
import static com.schoolaby.domain.enumeration.Country.TANZANIA;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockCustomUser(authorities = {TEACHER})
class EducationalLevelControllerIT extends BaseIntegrationTest {

    @Test
    @Transactional
    void shouldGetEstonianEducationalLevels() throws Exception {
        EducationalLevel estonianLevel = createEducationalLevel().name("Estonian level");
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/educational-levels")
                .param("country", ESTONIA.getValue())
                .contentType(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*].name", estonianLevel.getName()).exists());
        mockMvc.perform(get("/api/educational-levels")
                .contentType(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*].name", estonianLevel.getName()).exists());
    }

    @Test
    @Transactional
    void shouldGetTanzanianEducationalLevels() throws Exception {
        EducationalLevel tanzanianLevel = createEducationalLevel().name("Tanzanian level")
            .country(TANZANIA.getValue());
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/educational-levels")
                .param("country", TANZANIA.getValue())
                .contentType(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*].name", tanzanianLevel.getName()).exists());
    }

    @Test
    void shouldReturnEmptyListIfNothingFound() throws Exception {
        mockMvc.perform(get("/api/educational-levels")
                .param("country", "unknown")
                .contentType(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("length()").value(0));
    }
}
