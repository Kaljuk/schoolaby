package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.EducationalAlignment;
import com.schoolaby.domain.Material;
import com.schoolaby.domain.UploadedFile;
import com.schoolaby.repository.MaterialRepository;
import com.schoolaby.service.dto.MaterialDTO;
import com.schoolaby.service.mapper.MaterialMapper;
import com.schoolaby.web.WithMockCustomUser;
import org.hamcrest.core.IsNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

import static com.schoolaby.common.TestObjects.*;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.web.rest.TestUtil.convertObjectToJsonBytes;
import static java.util.stream.Collectors.joining;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WithMockCustomUser(authorities = {TEACHER})
class MaterialControllerIT extends BaseIntegrationTest {
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String UPDATED_EXTERNAL_ID = "BBBBBBBBBB";

    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final Boolean UPDATED_RESTRICTED = false;

    @Autowired
    private MaterialRepository materialRepository;
    @Autowired
    private MaterialMapper materialMapper;

    private Material material;

    @Test
    void shouldCreateMaterial() throws Exception {
        UploadedFile uploadedFile = createUploadedFile();
        transactionHelper.withTransaction(() -> persistCreatedEntities(entityManager));

        material = createMaterial().uploadedFile(uploadedFile);
        MaterialDTO materialDTO = materialMapper.toDto(material);

        mockMvc.perform(post("/api/materials")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(materialDTO)))
            .andExpect(status().isCreated());

        List<Material> materials = materialRepository.findAll();
        assertThat(materials).hasSize(1);
        material = materials.get(0);
        assertEquals(MATERIAL_TITLE, material.getTitle());
        assertEquals(MATERIAL_TYPE, material.getType());
        assertEquals(MATERIAL_DESCRIPTION, material.getDescription());
        assertEquals(MATERIAL_EXTERNAL_ID, material.getExternalId());
        assertEquals(MATERIAL_URL, material.getUrl());
        assertNotNull(material.getUploadedFile());
    }

    @Test
    @Transactional
    void shouldCreateMaterialWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = materialRepository.findAll().size();
        material = createMaterial();
        material.setId(1L);
        MaterialDTO materialDTO = materialMapper.toDto(material);

        mockMvc.perform(post("/api/materials")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(materialDTO)))
            .andExpect(status().isBadRequest());

        List<Material> materials = materialRepository.findAll();
        assertThat(materials).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void shouldCheckTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = materialRepository.findAll().size();
        material = createMaterial().title(null);
        MaterialDTO materialDTO = materialMapper.toDto(material);

        mockMvc.perform(post("/api/materials")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(materialDTO)))
            .andExpect(status().isBadRequest());

        List<Material> materials = materialRepository.findAll();
        assertThat(materials).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void shouldCheckUrlIsRequired() throws Exception {
        int databaseSizeBeforeTest = materialRepository.findAll().size();
        material = createMaterial().url(null);
        MaterialDTO materialDTO = materialMapper.toDto(material);

        mockMvc.perform(post("/api/materials")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(materialDTO)))
            .andExpect(status().isBadRequest());

        List<Material> materials = materialRepository.findAll();
        assertThat(materials).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void shouldGetAllMaterials() throws Exception {
        material = createMaterial();
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/materials?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(material.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(MATERIAL_TITLE)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(MATERIAL_TYPE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(MATERIAL_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].externalId").value(hasItem(MATERIAL_EXTERNAL_ID)))
            .andExpect(jsonPath("$.[*].url").value(hasItem(MATERIAL_URL)))
            .andExpect(jsonPath("$.[*].restricted").value(hasItem(MATERIAL_RESTRICTED)));
    }

    @Test
    @Transactional
    void shouldGetMaterial() throws Exception {
        material = createMaterial();
        persistCreatedEntities(entityManager);

        mockMvc.perform(get("/api/materials/{id}", material.getId()))
            .andExpect(status().isOk())
            .andExpect(content()
                .contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(material.getId().intValue()))
            .andExpect(jsonPath("$.title").value(MATERIAL_TITLE))
            .andExpect(jsonPath("$.type").value(MATERIAL_TYPE))
            .andExpect(jsonPath("$.description").value(MATERIAL_DESCRIPTION))
            .andExpect(jsonPath("$.externalId").value(MATERIAL_EXTERNAL_ID))
            .andExpect(jsonPath("$.url").value(MATERIAL_URL))
            .andExpect(jsonPath("$.restricted").value(MATERIAL_RESTRICTED));
    }

    @Test
    @Transactional
    void shouldGetNonExistingMaterial() throws Exception {
        mockMvc.perform(get("/api/materials/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void shouldUpdateMaterial() throws Exception {
        material = createMaterial();
        persistCreatedEntities(entityManager);
        int databaseSizeBeforeUpdate = materialRepository.findAll().size();
        Material updatedMaterial = materialRepository.findById(material.getId()).get();
        entityManager.detach(updatedMaterial);
        updatedMaterial
            .title(UPDATED_TITLE)
            .type(UPDATED_TYPE)
            .description(UPDATED_DESCRIPTION)
            .externalId(UPDATED_EXTERNAL_ID)
            .url(UPDATED_URL)
            .restricted(UPDATED_RESTRICTED);
        MaterialDTO materialDTO = materialMapper.toDto(updatedMaterial);

        mockMvc.perform(put("/api/materials")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(materialDTO)))
            .andExpect(status().isOk());

        List<Material> materials = materialRepository.findAll();
        assertThat(materials).hasSize(databaseSizeBeforeUpdate);
        Material material = materials.get(materials.size() - 1);
        assertThat(material.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(material.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(material.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(material.getExternalId()).isEqualTo(UPDATED_EXTERNAL_ID);
        assertThat(material.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(material.getRestricted()).isEqualTo(UPDATED_RESTRICTED);
    }

    @Test
    @Transactional
    void shouldUpdateNonExistingMaterial() throws Exception {
        int databaseSizeBeforeUpdate = materialRepository.findAll().size();
        MaterialDTO materialDTO = materialMapper.toDto(material);

        mockMvc.perform(put("/api/materials")
                .contentType(APPLICATION_JSON)
                .content(convertObjectToJsonBytes(materialDTO)))
            .andExpect(status().isBadRequest());

        List<Material> materials = materialRepository.findAll();
        assertThat(materials).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void shouldDeleteMaterial() throws Exception {
        material = createMaterial();
        persistCreatedEntities(entityManager);
        int databaseSizeBeforeDelete = materialRepository.findAll().size();

        mockMvc.perform(delete("/api/materials/{id}", material.getId())
                .accept(APPLICATION_JSON))
            .andExpect(status().isNoContent());

        List<Material> materials = materialRepository.findAll();
        assertThat(materials).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    void shouldGetNonRestrictedMaterialsByEducationalAlignmentId() throws Exception {
        EducationalAlignment educationalAlignment1 = createEducationalAlignment();
        EducationalAlignment educationalAlignment2 = createEducationalAlignment();

        material = createMaterial().restricted(false).addEducationalAlignments(educationalAlignment1);
        createMaterial().restricted(false).addEducationalAlignments(educationalAlignment2);

        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get("/api/materials/suggested")
                .param("educationalAlignmentIds", Arrays.stream(material.getEducationalAlignments().stream()
                        .map(EducationalAlignment::getId).toArray()).map(Object::toString)
                    .collect(joining(",")))
                .accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.size()").value(1))
            .andExpect(jsonPath("$.[*].id").value(hasItem(material.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(MATERIAL_TITLE)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(MATERIAL_TYPE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(MATERIAL_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].externalId").value(hasItem(MATERIAL_EXTERNAL_ID)))
            .andExpect(jsonPath("$.[*].url").value(hasItem(MATERIAL_URL)))
            .andExpect(jsonPath("$.[*].restricted").value(hasItem(false)));
    }

    @Test
    void shouldNotGetRestrictedMaterialsByEducationalAlignmentId() throws Exception {
        EducationalAlignment educationalAlignment1 = createEducationalAlignment();
        material = createMaterial().addEducationalAlignments(educationalAlignment1);

        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get("/api/materials/suggested")
                .param("educationalAlignmentIds", Arrays.stream(material.getEducationalAlignments().stream()
                        .map(EducationalAlignment::getId).toArray()).map(Object::toString)
                    .collect(joining(",")))
                .accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.size()").value(0));
    }

    @Test
    void shouldGetOwnMaterialsBySearchString() throws Exception {
        material = createMaterial().restricted(false);
        createMaterial().restricted(true);

        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get("/api/materials/search")
                .param("searchString", MATERIAL_TITLE)
                .accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.size()").value(2))
            .andExpect(jsonPath("$.[*].id").value(hasItem(material.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(MATERIAL_TITLE)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(MATERIAL_TYPE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(MATERIAL_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].externalId").value(hasItem(MATERIAL_EXTERNAL_ID)))
            .andExpect(jsonPath("$.[*].url").value(hasItem(MATERIAL_URL)))
            .andExpect(jsonPath("$.[*].restricted").value(hasItem(false)));
    }

    @Test
    void shouldGetOnlyUkrainianMaterialsWhenGivenCountryIsUkraine() throws Exception {
        String ukraineCountry = "Ukriane";
        createMaterial()
            .restricted(false)
            .country(ukraineCountry);

        createMaterial()
            .restricted(false);

        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get("/api/materials/search")
                .param("searchString", MATERIAL_TITLE)
                .param("country", ukraineCountry)
                .accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.size()").value(1))
            .andExpect(jsonPath("$.[0].title").value(MATERIAL_TITLE))
            .andExpect(jsonPath("$.[0].country").value(ukraineCountry));
    }

    @Test
    void shouldNotGetUkrainianMaterialsWhenNoCountryGiven() throws Exception {
        String ukraineCountry = "Ukriane";
        createMaterial()
            .restricted(false)
            .country(ukraineCountry);

        createMaterial()
            .restricted(false);

        transactionHelper.withNewTransaction(() -> persistCreatedEntities(entityManager));

        mockMvc.perform(get("/api/materials/search")
                .param("searchString", MATERIAL_TITLE)
                .accept(APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.size()").value(1))
            .andExpect(jsonPath("$.[0].title").value(MATERIAL_TITLE))
            .andExpect(jsonPath("$.[0].country").value(IsNull.nullValue()));
    }
}
