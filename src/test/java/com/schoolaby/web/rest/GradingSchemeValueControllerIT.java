package com.schoolaby.web.rest;

import com.schoolaby.common.BaseIntegrationTest;
import com.schoolaby.domain.GradingSchemeValue;
import com.schoolaby.repository.GradingSchemeValueRepository;
import com.schoolaby.service.dto.GradingSchemeValueDTO;
import com.schoolaby.service.mapper.GradingSchemeValueMapper;
import com.schoolaby.web.WithMockCustomUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.schoolaby.security.Role.Constants.ADMIN;
import static com.schoolaby.web.rest.TestUtil.convertObjectToJsonBytes;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WithMockCustomUser(authorities = {ADMIN})
class GradingSchemeValueControllerIT extends BaseIntegrationTest {
    private static final String DEFAULT_GRADE = "5";
    private static final String UPDATED_GRADE = "3";

    private static final Integer DEFAULT_PERCENTAGE_RANGE = 91;
    private static final Integer UPDATED_PERCENTAGE_RANGE = 61;

    @Autowired
    private GradingSchemeValueRepository gradingSchemeValueRepository;
    @Autowired
    private GradingSchemeValueMapper gradingSchemeValueMapper;

    private GradingSchemeValue gradingSchemeValue;

    public static GradingSchemeValue createEntity(EntityManager em) {
        return new GradingSchemeValue()
            .setGrade(DEFAULT_GRADE)
            .setPercentageRange(DEFAULT_PERCENTAGE_RANGE);
    }

    @BeforeEach
    void initTest() {
        gradingSchemeValue = createEntity(entityManager);
    }

    @Test
    @Transactional
    void createGradingSchemeValue() throws Exception {
        int databaseSizeBeforeCreate = gradingSchemeValueRepository.findAll().size();
        GradingSchemeValueDTO gradingSchemeValueDTO = gradingSchemeValueMapper.toDto(gradingSchemeValue);

        mockMvc.perform(post("/api/grading-scheme-values")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(gradingSchemeValueDTO)))
            .andExpect(status().isCreated());

        List<GradingSchemeValue> gradingSchemeValues = gradingSchemeValueRepository.findAll();
        assertThat(gradingSchemeValues).hasSize(databaseSizeBeforeCreate + 1);
        GradingSchemeValue gradingSchemeValue = gradingSchemeValues.get(gradingSchemeValues.size() - 1);
        assertThat(gradingSchemeValue.getGrade()).isEqualTo(DEFAULT_GRADE);
        assertThat(gradingSchemeValue.getPercentageRange()).isEqualTo(DEFAULT_PERCENTAGE_RANGE);
    }

    @Test
    @Transactional
    void createGradingSchemeValueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = gradingSchemeValueRepository.findAll().size();
        gradingSchemeValue.setId(1L);
        GradingSchemeValueDTO gradingSchemeValueDTO = gradingSchemeValueMapper.toDto(gradingSchemeValue);

        mockMvc.perform(post("/api/grading-scheme-values")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(gradingSchemeValueDTO)))
            .andExpect(status().isBadRequest());

        List<GradingSchemeValue> gradingSchemeValues = gradingSchemeValueRepository.findAll();
        assertThat(gradingSchemeValues).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkGradeIsRequired() throws Exception {
        int databaseSizeBeforeTest = gradingSchemeValueRepository.findAll().size();
        gradingSchemeValue.setGrade(null);
        GradingSchemeValueDTO gradingSchemeValueDTO = gradingSchemeValueMapper.toDto(gradingSchemeValue);

        mockMvc.perform(post("/api/grading-scheme-values")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(gradingSchemeValueDTO)))
            .andExpect(status().isBadRequest());

        List<GradingSchemeValue> gradingSchemeValues = gradingSchemeValueRepository.findAll();
        assertThat(gradingSchemeValues).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPercentageRangeIsRequired() throws Exception {
        int databaseSizeBeforeTest = gradingSchemeValueRepository.findAll().size();
        gradingSchemeValue.setPercentageRange(null);

        GradingSchemeValueDTO gradingSchemeValueDTO = gradingSchemeValueMapper.toDto(gradingSchemeValue);

        mockMvc.perform(post("/api/grading-scheme-values")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(gradingSchemeValueDTO)))
            .andExpect(status().isBadRequest());

        List<GradingSchemeValue> gradingSchemeValues = gradingSchemeValueRepository.findAll();
        assertThat(gradingSchemeValues).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllGradingSchemeValues() throws Exception {
        gradingSchemeValueRepository.saveAndFlush(gradingSchemeValue);

        mockMvc.perform(get("/api/grading-scheme-values?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(gradingSchemeValue.getId().intValue())))
            .andExpect(jsonPath("$.[*].grade").value(hasItem(DEFAULT_GRADE)))
            .andExpect(jsonPath("$.[*].percentageRangeStart").value(hasItem(DEFAULT_PERCENTAGE_RANGE)));
    }

    @Test
    @Transactional
    void getGradingSchemeValue() throws Exception {
        gradingSchemeValueRepository.saveAndFlush(gradingSchemeValue);

        mockMvc.perform(get("/api/grading-scheme-values/{id}", gradingSchemeValue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(gradingSchemeValue.getId().intValue()))
            .andExpect(jsonPath("$.grade").value(DEFAULT_GRADE))
            .andExpect(jsonPath("$.percentageRangeStart").value(DEFAULT_PERCENTAGE_RANGE));
    }

    @Test
    @Transactional
    void getNonExistingGradingSchemeValue() throws Exception {
        mockMvc.perform(get("/api/grading-scheme-values/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void updateGradingSchemeValue() throws Exception {
        gradingSchemeValueRepository.saveAndFlush(gradingSchemeValue);

        int databaseSizeBeforeUpdate = gradingSchemeValueRepository.findAll().size();

        GradingSchemeValue updatedGradingSchemeValue = gradingSchemeValueRepository.findById(gradingSchemeValue.getId()).get();
        entityManager.detach(updatedGradingSchemeValue);
        updatedGradingSchemeValue.setGrade(UPDATED_GRADE).setPercentageRange(UPDATED_PERCENTAGE_RANGE);
        GradingSchemeValueDTO gradingSchemeValueDTO = gradingSchemeValueMapper.toDto(updatedGradingSchemeValue);

        mockMvc.perform(put("/api/grading-scheme-values")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(gradingSchemeValueDTO)))
            .andExpect(status().isOk());

        List<GradingSchemeValue> gradingSchemeValues = gradingSchemeValueRepository.findAll();
        assertThat(gradingSchemeValues).hasSize(databaseSizeBeforeUpdate);
        GradingSchemeValue gradingSchemeValue = gradingSchemeValues.get(gradingSchemeValues.size() - 1);
        assertThat(gradingSchemeValue.getGrade()).isEqualTo(UPDATED_GRADE);
        assertThat(gradingSchemeValue.getPercentageRange()).isEqualTo(UPDATED_PERCENTAGE_RANGE);
    }

    @Test
    @Transactional
    void updateNonExistingGradingSchemeValue() throws Exception {
        int databaseSizeBeforeUpdate = gradingSchemeValueRepository.findAll().size();
        GradingSchemeValueDTO gradingSchemeValueDTO = gradingSchemeValueMapper.toDto(gradingSchemeValue);

        mockMvc.perform(put("/api/grading-scheme-values")
            .contentType(APPLICATION_JSON)
            .content(convertObjectToJsonBytes(gradingSchemeValueDTO)))
            .andExpect(status().isBadRequest());

        List<GradingSchemeValue> gradingSchemeValues = gradingSchemeValueRepository.findAll();
        assertThat(gradingSchemeValues).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteGradingSchemeValue() throws Exception {
        gradingSchemeValueRepository.saveAndFlush(gradingSchemeValue);
        int databaseSizeBeforeDelete = gradingSchemeValueRepository.findAll().size();

        mockMvc.perform(delete("/api/grading-scheme-values/{id}", gradingSchemeValue.getId())
            .accept(APPLICATION_JSON))
            .andExpect(status().isNoContent());

        List<GradingSchemeValue> gradingSchemeValues = gradingSchemeValueRepository.findAll();
        assertThat(gradingSchemeValues).hasSize(databaseSizeBeforeDelete - 1);
    }
}
