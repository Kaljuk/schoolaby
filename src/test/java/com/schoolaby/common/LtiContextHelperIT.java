package com.schoolaby.common;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LtiContextHelperIT extends BaseIntegrationTest {
    // {"journeyId":10}
    private static final String CONTEXT = "eyJqb3VybmV5SWQiOjEwfQ==";

    @Autowired
    private LtiContextHelper contextHelper;

    @Test
    void shouldEncodeJourneyIdAsBase64String() {
        assertEquals(CONTEXT, contextHelper.toBase64Url(10L));
    }

    @Test
    void shouldDecodeBase64StringToJourneyId() {
        LtiContextHelper.LtiContext context = contextHelper.parse(CONTEXT);
        assertEquals(10L, context.getJourneyId());
    }
}
