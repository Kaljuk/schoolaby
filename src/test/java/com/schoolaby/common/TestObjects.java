package com.schoolaby.common;

import com.schoolaby.domain.*;
import com.schoolaby.domain.enumeration.LtiConfigType;
import com.schoolaby.security.Role;
import com.schoolaby.service.dto.states.EntityState;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

import static com.schoolaby.domain.LtiScore.GRADING_PROGRESS_FULLY_GRADED;
import static com.schoolaby.domain.enumeration.Country.ESTONIA;
import static com.schoolaby.security.Role.Constants.STUDENT;
import static com.schoolaby.security.Role.Constants.TEACHER;
import static com.schoolaby.service.dto.LtiVersion.LTI_1_1;
import static com.schoolaby.service.dto.states.EntityState.COMPLETED;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.*;
import static java.util.Collections.emptySet;

public class TestObjects {
    public static final String JOURNEY_TITLE = "Journey title";
    public static final String JOURNEY_DESCRIPTION = "Journey description";
    public static final String STUDENT_JOURNEY_SIGN_UP_CODE = "Journey sign up code";
    public static final String TEACHER_JOURNEY_SIGN_UP_CODE = "Journey sign up code teacher";
    public static final String JOURNEY_VIDEO_CONFERENCE_URL = "http://localhost:8080/conference/6";
    public static final Boolean JOURNEY_TEMPLATE = true;
    public static final Instant JOURNEY_START_DATE = now();
    public static final Instant JOURNEY_END_DATE = JOURNEY_START_DATE.plus(30, DAYS);

    public static final String MILESTONE_TITLE = "Milestone title";
    public static final String MILESTONE_DESCRIPTION = "Milestone description";
    public static final Instant MILESTONE_END_DATE = now().plus(7, DAYS);
    public static final Instant MILESTONE_PUBLISHED = Instant.now().minus(1, DAYS);
    public static final EntityState MILESTONE_STATE = COMPLETED;

    public static final String USER_LOGIN = "testlogin";
    public static final String USER_PASSWORD = "$2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e";
    public static final String USER_FIRST_NAME = "First name";
    public static final String USER_LAST_NAME = "Last name";
    public static final String USER_EMAIL = "email@netgroup.com";
    public static final boolean USER_ACTIVATED = true;
    public static final String USER_LANG_KEY = "en";
    public static final String USER_IMAGE_URL = "http://localhost:8080/image.jpg";
    public static final String USER_ACTIVATION_KEY = "123123sasdsad213";
    public static final String USER_RESET_KEY = "User reset key";
    public static final Instant USER_RESET_DATE = now();
    public static final String USER_PHONE_NUMBER = "+372 123123123";
    public static final String USER_PERSONAL_CODE = "300000000000";

    private static final String GRADING_SCHEME_NAME = "AAAAAAAAAA";
    private static final String GRADING_SCHEME_CODE = "ALPHABETICAL";
    private static final Integer GRADING_SCHEME_SEQUENCE_NUMBER = 1;

    private static final String GRADING_SCHEME_VALUE_GRADE = "5";
    private static final Integer GRADING_SCHEME_VALUE_RANGE = 91;

    public static final String ASSIGNMENT_TITLE = "Assignment title";
    public static final String ASSIGNMENT_DESCRIPTION = "Assignment description";
    public static final Instant ASSIGNMENT_DEADLINE = now().plus(1, DAYS);
    public static final Boolean ASSIGNMENT_FLEXIBLE_DEADLINE = false;
    public static final Instant ASSIGNMENT_PUBLISHED = Instant.now().minus(1, DAYS);

    public static final EntityState ASSIGNMENT_STATE = COMPLETED;

    public static final String SUBMISSION_VALUE = "Submission value";
    public static final String SUBMISSION_GRADE = "5";
    public static final boolean SUBMISSION_RESUBMITTABLE = true;
    public static final Instant SUBMISSION_FEEDBACK_DATE = now();
    public static final Instant SUBMISSION_SUBMITTED_FOR_GRADING_DATE = now().minus(1, DAYS);
    public static final String SELECTED_CRITERION_LEVEL_DESCRIPTION = "Modified description";

    public static final String MATERIAL_TITLE = "Material title";
    public static final String MATERIAL_TYPE = "LINK";
    public static final String MATERIAL_DESCRIPTION = "Material description";
    public static final String MATERIAL_EXTERNAL_ID = "Material external id";
    public static final String MATERIAL_URL = "Material url";
    public static final Boolean MATERIAL_RESTRICTED = true;

    public static final String UPLOADED_FILE_ORIGINAL_NAME = "UploadedFile originalName.jpg";
    public static final String UPLOADED_FILE_UNIQUE_NAME = "UploadedFile uniqueName.jpg";
    public static final String UPLOADED_FILE_TYPE = "jpg";
    public static final String UPLOADED_FILE_EXTENSION = "jpg";

    public static final String MESSAGE_VALUE = "Message value";
    public static final String CHAT_TITLE = "Chat title";

    public static final String EDUCATIONAL_ALIGNMENT_TITLE = "EducationalAlignment title";
    public static final String EDUCATIONAL_ALIGNMENT_ALIGNMENT_TYPE = "EducationalAlignment alignment type";
    public static final String EDUCATIONAL_ALIGNMENT_COUNTRY = ESTONIA.getValue();
    public static final String EDUCATIONAL_ALIGNMENT_TARGET_NAME = "EducationalAlignment target name";
    public static final String EDUCATIONAL_ALIGNMENT_TARGET_URL = "EducationalAlignment target url";
    public static final Long EDUCATIONAL_ALIGNMENT_TAXON_ID = -91423L;
    public static final Instant EDUCATIONAL_ALIGNMENT_START_DATE = now().minus(1, DAYS);
    public static final Instant EDUCATIONAL_ALIGNMENT_END_DATE = now().plus(1, DAYS);

    public static final String LTI_CONFIG_CONSUMER_KEY = "LtiConfig consumer key";
    public static final String LTI_CONFIG_SHARED_SECRET = "LtiConfig shared secret";
    public static final String LTI_CONFIG_DEPLOYMENT_ID = "LtiApp deploymentId";
    public static final LtiConfigType LTI_CONFIG_TYPE = LtiConfigType.PLATFORM;

    public static final String LTI_APP_NAME = "LtiApp name";
    public static final String LTI_APP_DESCRIPTION = "LtiApp description";
    public static final String LTI_APP_VERSION = LTI_1_1.getValue();
    public static final String LTI_APP_LAUNCH_URL = "http://localhost:9000/lti-launch-url-mock";
    public static final String LTI_APP_IMAGE_URL = "LtiApp image url";
    public static final String LTI_APP_DEEP_LINKING_URL = "https://tool.localhost/deep-linking";
    public static final String LTI_APP_LOGIN_INITIATION_URL = "https://tool.localhost/login-initiation";
    public static final String LTI_APP_REDIRECT_HOST = "tool.localhost";
    public static final String LTI_APP_CLIENT_ID = "LtiApp clientId";
    public static final String LTI_APP_JWKS_URL = "https://tool.localhost/jwks";

    public static final String LTI_LAUNCH_RESULT = "0.6";

    public static final String LTI_LINE_ITEM_LABEL = "LtiLineItem label";
    public static final Long LTI_LINE_ITEM_SCORE_MAXIMUM = 10L;

    public static final String EDUCATIONAL_LEVEL_NAME = "EducationalLevel name";
    public static final String EDUCATIONAL_LEVEL_COUNTRY = ESTONIA.getValue();

    public static final String LTI_RESOURCE_TITLE = "LtiResource title";
    public static final String LTI_RESOURCE_DESCRIPTION = "LtiResource description";
    public static final boolean LTI_RESOURCE_SYNC_GRADE = false;
    public static final String LTI_RESOURCE_RESOURCE_LINK_ID = "LtiResource resource link id";

    public static final Instant LTI_SCORE_TIMESTAMP = now().minus(1, HOURS);
    public static final String LTI_SCORE_COMMENT = "LtiScore comment";
    public static final String LTI_SCORE_ACTIVITY_PROGRESS = "Completed";
    public static final String LTI_SCORE_GRADING_PROGRESS = GRADING_PROGRESS_FULLY_GRADED;
    public static final Double LTI_SCORE_SCORE_GIVEN = 9.0;
    public static final Double LTI_SCORE_SCORE_MAXIMUM = 10.0;

    public static final String NOTIFICATION_MESSAGE = "Notification message";
    public static final String NOTIFICATION_LINK = "Notification link";

    private static final List<Object> createdEntities = new ArrayList<>();
    public static final String RUBRIC_TITLE = "Test title";
    public static final boolean RUBRIC_IS_TEMPLATE = false;
    public static final String CRITERION_TITLE = "Criterion title";
    public static final String CRITERION_DESCRIPTION = "Criterion description";
    public static final String CRITERION_LEVEL_DESCRIPTION = "Criterion level description";
    public static final String CRITERION_LEVEL_TITLE = "Criterion level title";
    public static final long CRITERION_LEVEL_SEQUENCE_NUMBER = 0L;
    public static final long CRITERION_SEQUENCE_NUMBER = 0L;
    public static final long CRITERION_LEVEL_POINTS = 1L;
    public static final String SUBJECT_LABEL = "schoolabyApp.subject.estonian";
    public static final String GROUP_NAME = "Grupp 1";

    public static final Long SCHOOL_ID = 1L;
    public static final String SCHOOL_EHIS_ID = "ehis_id";
    public static final String SCHOOL_REG_NR = "reg_nr";
    public static final String SCHOOL_NAME = "Test school";
    public static final String SCHOOL_COUNTRY = ESTONIA.getValue();

    public static final Role PERSON_ROLE = Role.STUDENT;
    public static final boolean PERSON_ROLE_ACTIVE = true;
    public static final String PERSON_ROLE_GRADE = "grade";

    public static final String ROLE_NAME = "Teacher";

    public static final String CURRICULUM_TITLE = "TEST_CURRICULUM";
    public static final String CURRICULUM_COUNTRY = "Estonia";
    public static final Integer CURRICULUM_SEQUENCE_NUMBER = -1;

    public static void clear() {
        createdEntities.clear();
    }

    public static GradingSchemeValue createGradingSchemeValue() {
        GradingSchemeValue gradingSchemeValue = new GradingSchemeValue()
            .setGrade(GRADING_SCHEME_VALUE_GRADE)
            .setPercentageRange(GRADING_SCHEME_VALUE_RANGE);
        createdEntities.add(gradingSchemeValue);
        return gradingSchemeValue;
    }

    public static Submission createSubmission() {
        Submission submission = new Submission()
            .value(SUBMISSION_VALUE)
            .resubmittable(SUBMISSION_RESUBMITTABLE)
            .submittedForGradingDate(SUBMISSION_SUBMITTED_FOR_GRADING_DATE);
        createdEntities.add(submission);
        return submission;
    }

    public static SubmissionFeedback createSubmissionFeedback() {
        SubmissionFeedback submissionFeedback = new SubmissionFeedback()
            .feedbackDate(SUBMISSION_FEEDBACK_DATE);
        createdEntities.add(submissionFeedback);
        return submissionFeedback;
    }

    public static GradingScheme createGradingScheme() {
        GradingScheme gradingScheme = new GradingScheme()
            .setName(GRADING_SCHEME_NAME)
            .setCode(GRADING_SCHEME_CODE)
            .setSequenceNumber(GRADING_SCHEME_SEQUENCE_NUMBER);
        createdEntities.add(gradingScheme);
        return gradingScheme;
    }

    public static Assignment createAssignment() {
        Assignment assignment = new Assignment()
            .title(ASSIGNMENT_TITLE)
            .description(ASSIGNMENT_DESCRIPTION)
            .deadline(ASSIGNMENT_DEADLINE)
            .flexibleDeadline(ASSIGNMENT_FLEXIBLE_DEADLINE)
            .published(ASSIGNMENT_PUBLISHED)
            .gradingScheme(createGradingScheme());
        createdEntities.add(assignment);
        return assignment;
    }

    public static AssignmentState createAssignmentState() {
        AssignmentState assignmentState = new AssignmentState()
            .setState(ASSIGNMENT_STATE);
        createdEntities.add(assignmentState);
        return assignmentState;
    }

    public static MilestoneState createMilestoneState() {
        MilestoneState milestoneState = new MilestoneState()
            .setState(MILESTONE_STATE);
        createdEntities.add(milestoneState);
        return milestoneState;
    }

    public static Group createGroup() {
        Group group = new Group()
            .setName(GROUP_NAME);
        createdEntities.add(group);
        return group;
    }

    public static void createMilestones(int total, Journey journey, User teacher) {
        Set<Milestone> milestones = new HashSet<>();

        IntStream.range(0, total)
            .forEach(index -> {
                Milestone milestone = createMilestone(Integer.toString(index))
                    .journey(journey)
                    .creator(teacher);
                milestones.add(milestone);
            });

        createdEntities.addAll(milestones);
    }

    public static Milestone createMilestone() {
        return createMilestone(MILESTONE_TITLE);
    }

    public static Milestone createMilestone(String title) {
        Milestone milestone = new Milestone()
            .title(title)
            .description(MILESTONE_DESCRIPTION)
            .endDate(MILESTONE_END_DATE)
            .published(MILESTONE_PUBLISHED);
        createdEntities.add(milestone);
        return milestone;
    }

    public static Journey createJourney() {
        Journey journey = new Journey()
            .title(JOURNEY_TITLE)
            .description(JOURNEY_DESCRIPTION)
            .videoConferenceUrl(JOURNEY_VIDEO_CONFERENCE_URL)
            .startDate(JOURNEY_START_DATE)
            .endDate(JOURNEY_END_DATE)
            .template(JOURNEY_TEMPLATE);
        journey.setJourneySignupCodes(Set.of(
            new JourneySignupCode()
                .signUpCode(STUDENT_JOURNEY_SIGN_UP_CODE)
                .authority(new Authority().name(STUDENT))
                .journey(journey),
            new JourneySignupCode()
                .signUpCode(TEACHER_JOURNEY_SIGN_UP_CODE)
                .authority(new Authority().name(TEACHER))
                .journey(journey)
        ));
        createdEntities.add(journey);
        return journey;
    }

    public static Chat createChat() {
        Chat chat = new Chat()
            .title(CHAT_TITLE);
        createdEntities.add(chat);
        return chat;
    }

    public static User createUser() {
        User user = User.builder()
            .login(USER_LOGIN)
            .password(USER_PASSWORD)
            .firstName(USER_FIRST_NAME)
            .lastName(USER_LAST_NAME)
            .email(USER_EMAIL)
            .activated(USER_ACTIVATED)
            .langKey(USER_LANG_KEY)
            .imageUrl(USER_IMAGE_URL)
            .activationKey(USER_ACTIVATION_KEY)
            .resetKey(USER_RESET_KEY)
            .resetDate(USER_RESET_DATE)
            .phoneNumber(USER_PHONE_NUMBER)
            .personalCode(USER_PERSONAL_CODE)
            .build();
        createdEntities.add(user);
        return user;
    }

    public static Material createMaterial() {
        Material material = new Material()
            .title(MATERIAL_TITLE)
            .type(MATERIAL_TYPE)
            .description(MATERIAL_DESCRIPTION)
            .externalId(MATERIAL_EXTERNAL_ID)
            .url(MATERIAL_URL)
            .restricted(MATERIAL_RESTRICTED);
        createdEntities.add(material);
        return material;
    }

    public static UploadedFile createUploadedFile() {
        UploadedFile uploadedFile = new UploadedFile()
            .originalName(UPLOADED_FILE_ORIGINAL_NAME)
            .uniqueName(UPLOADED_FILE_UNIQUE_NAME)
            .type(UPLOADED_FILE_TYPE)
            .extension(UPLOADED_FILE_EXTENSION);
        createdEntities.add(uploadedFile);
        return uploadedFile;
    }

    public static Message createMessage() {
        Message message = new Message()
            .value(MESSAGE_VALUE)
            .messageRecipients(emptySet());
        createdEntities.add(message);
        return message;
    }

    public static EducationalAlignment createEducationalAlignment() {
        EducationalAlignment educationalAlignment = new EducationalAlignment()
            .title(EDUCATIONAL_ALIGNMENT_TITLE)
            .alignmentType(EDUCATIONAL_ALIGNMENT_ALIGNMENT_TYPE)
            .country(EDUCATIONAL_ALIGNMENT_COUNTRY)
            .targetName(EDUCATIONAL_ALIGNMENT_TARGET_NAME)
            .targetUrl(EDUCATIONAL_ALIGNMENT_TARGET_URL)
            .taxonId(EDUCATIONAL_ALIGNMENT_TAXON_ID)
            .startDate(EDUCATIONAL_ALIGNMENT_START_DATE)
            .endDate(EDUCATIONAL_ALIGNMENT_END_DATE);
        createdEntities.add(educationalAlignment);
        return educationalAlignment;
    }

    public static LtiConfig createLtiConfig() {
        LtiConfig ltiConfig = LtiConfig.builder()
            .consumerKey(LTI_CONFIG_CONSUMER_KEY)
            .sharedSecret(LTI_CONFIG_SHARED_SECRET)
            .deploymentId(LTI_CONFIG_DEPLOYMENT_ID)
            .ltiApp(createLtiApp())
            .type(LTI_CONFIG_TYPE).build();
        createdEntities.add(ltiConfig);
        return ltiConfig;
    }

    public static LtiApp createLtiApp() {
        LtiApp ltiApp = new LtiApp()
            .setName(LTI_APP_NAME)
            .setDescription(LTI_APP_DESCRIPTION)
            .setResubmittable(false)
            .setLaunchUrl(LTI_APP_LAUNCH_URL)
            .setImageUrl(LTI_APP_IMAGE_URL)
            .setVersion(LTI_APP_VERSION)
            .setDeepLinkingUrl(LTI_APP_DEEP_LINKING_URL)
            .setLoginInitiationUrl(LTI_APP_LOGIN_INITIATION_URL)
            .setRedirectHost(LTI_APP_REDIRECT_HOST)
            .setClientId(LTI_APP_CLIENT_ID)
            .setJwksUrl(LTI_APP_JWKS_URL);
        createdEntities.add(ltiApp);
        return ltiApp;
    }

    public static LtiLaunch createLtiLaunch() {
        LtiLaunch ltiLaunch = new LtiLaunch()
            .result(LTI_LAUNCH_RESULT);
        createdEntities.add(ltiLaunch);
        return ltiLaunch;
    }

    public static LtiLineItem createLtiLineItem() {
        LtiLineItem ltiLineItem = new LtiLineItem()
            .label(LTI_LINE_ITEM_LABEL)
            .scoreMaximum(LTI_LINE_ITEM_SCORE_MAXIMUM);
        createdEntities.add(ltiLineItem);
        return ltiLineItem;
    }

    public static LtiScore createLtiScore() {
        LtiScore ltiScore = new LtiScore()
            .setTimestamp(LTI_SCORE_TIMESTAMP)
            .setComment(LTI_SCORE_COMMENT)
            .setActivityProgress(LTI_SCORE_ACTIVITY_PROGRESS)
            .setGradingProgress(LTI_SCORE_GRADING_PROGRESS)
            .setScoreGiven(LTI_SCORE_SCORE_GIVEN)
            .setScoreMaximum(LTI_SCORE_SCORE_MAXIMUM);

        createdEntities.add(ltiScore);
        return ltiScore;
    }

    public static LtiResource createLtiResource() {
        LtiResource ltiResource = new LtiResource()
            .setTitle(LTI_RESOURCE_TITLE)
            .setDescription(LTI_RESOURCE_DESCRIPTION)
            .setResourceLinkId(LTI_RESOURCE_RESOURCE_LINK_ID)
            .setSyncGrade(LTI_RESOURCE_SYNC_GRADE);
        createdEntities.add(ltiResource);
        return ltiResource;
    }

    public static EducationalLevel createEducationalLevel() {
        EducationalLevel educationalLevel = new EducationalLevel()
            .name(EDUCATIONAL_LEVEL_NAME)
            .country(EDUCATIONAL_LEVEL_COUNTRY);
        createdEntities.add(0, educationalLevel);
        return educationalLevel;
    }

    public static Notification createNotification() {
        Notification notification = new Notification()
            .message(NOTIFICATION_MESSAGE)
            .link(NOTIFICATION_LINK);
        createdEntities.add(notification);
        return notification;
    }

    public static CriterionLevel createCriterionLevel() {
        CriterionLevel criterionLevel = new CriterionLevel()
            .setTitle(CRITERION_LEVEL_TITLE)
            .setDescription(CRITERION_LEVEL_DESCRIPTION)
            .setPoints(CRITERION_LEVEL_POINTS)
            .setSequenceNumber(CRITERION_LEVEL_SEQUENCE_NUMBER);
        createdEntities.add(criterionLevel);
        return criterionLevel;
    }

    public static Criterion createCriterion() {
        Criterion criterion = new Criterion()
            .setTitle(CRITERION_TITLE)
            .setDescription(CRITERION_DESCRIPTION)
            .setSequenceNumber(CRITERION_SEQUENCE_NUMBER);
        createdEntities.add(criterion);
        return criterion;
    }

    public static Rubric createRubric() {
        Rubric rubric = new Rubric()
            .setTitle(RUBRIC_TITLE)
            .setIsTemplate(RUBRIC_IS_TEMPLATE);
        createdEntities.add(rubric);
        return rubric;
    }

    public static Set<SelectedCriterionLevel> createSelectedCriterionLevelSet() {
        SelectedCriterionLevel level1 = new SelectedCriterionLevel()
            .setModifiedDescription(SELECTED_CRITERION_LEVEL_DESCRIPTION)
            .setCriterionLevel(createCriterionLevel())
            .setSubmissionFeedback(createSubmissionFeedback());

        SelectedCriterionLevel level2 = new SelectedCriterionLevel()
            .setModifiedDescription(SELECTED_CRITERION_LEVEL_DESCRIPTION)
            .setCriterionLevel(createCriterionLevel())
            .setSubmissionFeedback(createSubmissionFeedback());

        Set<SelectedCriterionLevel> levels = new HashSet<>();
        levels.add(level1);
        levels.add(level2);
        createdEntities.add(levels);
        return levels;
    }

    public static School createSchool() {
        School school = new School()
            .ehisId(SCHOOL_EHIS_ID)
            .regNr(SCHOOL_REG_NR)
            .name(SCHOOL_NAME)
            .country(SCHOOL_COUNTRY);
        createdEntities.add(school);
        return school;
    }

    public static PersonRole createPersonRole() {
        PersonRole personRole = new PersonRole()
            .role(PERSON_ROLE)
            .active(PERSON_ROLE_ACTIVE)
            .grade(PERSON_ROLE_GRADE);
        createdEntities.add(personRole);
        return personRole;
    }

    public static Curriculum createCurriculum() {
        Curriculum curriculum = new Curriculum()
            .title(CURRICULUM_TITLE)
            .country(CURRICULUM_COUNTRY)
            .sequenceNumber(CURRICULUM_SEQUENCE_NUMBER);
        createdEntities.add(curriculum);
        return curriculum;
    }

    public static JourneyTeacher createJourneyTeacher() {
        JourneyTeacher journeyTeacher = new JourneyTeacher()
            .joinedDate(Instant.now());
        createdEntities.add(journeyTeacher);
        return journeyTeacher;
    }

    public static void persistCreatedEntities(EntityManager em) {
        createdEntities.forEach(em::persist);
        em.flush();
    }
}
