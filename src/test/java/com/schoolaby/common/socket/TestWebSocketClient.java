package com.schoolaby.common.socket;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.schoolaby.security.Role;
import com.schoolaby.security.User;
import com.schoolaby.security.jwt.TokenProvider;
import io.github.jhipster.config.JHipsterProperties;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.security.Key;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static com.schoolaby.common.BaseIntegrationTest.STUDENT_ID;
import static com.schoolaby.common.BaseIntegrationTest.STUDENT_LOGIN;
import static java.util.concurrent.TimeUnit.SECONDS;
import static javax.management.timer.Timer.ONE_MINUTE;

@Component
@Profile("testcontainers")
public class TestWebSocketClient {
    @Value("${jhipster.security.authentication.jwt.base64-secret}")
    private String jwtSecretKey;
    private final ObjectMapper objectMapper;

    private final WebSocketStompClient webSocketStompClient;
    private StompSession stompSession;

    public TestWebSocketClient(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
        this.webSocketStompClient = createStompClient();
    }

    private WebSocketStompClient createStompClient() {
        WebSocketClient webSocketClient = new SockJsClient(List.of(new WebSocketTransport(new StandardWebSocketClient())));
        WebSocketStompClient stompClient = new WebSocketStompClient(webSocketClient);
        MappingJackson2MessageConverter messageConverter = new MappingJackson2MessageConverter();
        messageConverter.setObjectMapper(objectMapper);
        stompClient.setMessageConverter(messageConverter);
        return stompClient;
    }

    public void connect(String authToken, String port) throws ExecutionException, InterruptedException, TimeoutException {
        stompSession = webSocketStompClient.connect(getWebsocketPath(authToken, port), new StompSessionHandlerAdapter() {}).get(1, SECONDS);
    }

    private String getWebsocketPath(String authToken, String port) {
        return String.format("ws://localhost:%s/websocket/tracker?access_token=%s", port, authToken);
    }

    public <T> BlockingQueue<T> subscribe(String endpoint, Class<T> clazz) {
        TestStompFrameHandler<T> ltiScoreStompFrameHandler = new TestStompFrameHandler<>(clazz);
        stompSession.subscribe(endpoint, ltiScoreStompFrameHandler);
        return ltiScoreStompFrameHandler.blockingQueue;
    }

    public String createStudentAuthToken() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(Role.Constants.STUDENT));
        com.schoolaby.security.User user = new com.schoolaby.security.User(STUDENT_LOGIN, STUDENT_LOGIN, authorities, STUDENT_ID);

        return createUserAuthToken(user);
    }

    private String createUserAuthToken(User user) {
        TokenProvider tokenProvider = new TokenProvider(new JHipsterProperties());
        Key key =
            Keys.hmacShaKeyFor(
                Decoders.BASE64.decode(jwtSecretKey)
            );

        ReflectionTestUtils.setField(tokenProvider, "key", key);
        ReflectionTestUtils.setField(tokenProvider, "tokenValidityInMilliseconds", ONE_MINUTE);
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, user.getUsername(), user.getAuthorities());
        return tokenProvider.createToken(authentication, user.getId());
    }

    private static class TestStompFrameHandler<T> implements StompFrameHandler {
        public final BlockingQueue<T> blockingQueue = new ArrayBlockingQueue<>(1);
        private final Class<T> clazz;

        public TestStompFrameHandler(Class<T> clazz) {
            this.clazz = clazz;
        }

        @Override
        public @NotNull Type getPayloadType(@NotNull StompHeaders stompHeaders) {
            return clazz;
        }

        @Override
        public void handleFrame(@NotNull StompHeaders stompHeaders, Object payload) {
            blockingQueue.add((T) payload);
        }
    }
}
