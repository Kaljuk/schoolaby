TRUNCATE TABLE public.educational_alignment CASCADE;
TRUNCATE TABLE public.journey CASCADE;
TRUNCATE TABLE public.jhi_user_authority CASCADE;
TRUNCATE TABLE public.jhi_authority CASCADE;
TRUNCATE TABLE public.external_authentication CASCADE;
TRUNCATE TABLE public.jhi_user CASCADE;
TRUNCATE TABLE public.material CASCADE;
TRUNCATE TABLE public.uploaded_file CASCADE;
TRUNCATE TABLE public.school CASCADE;
TRUNCATE TABLE public.curriculum CASCADE;
TRUNCATE TABLE public.chat CASCADE;
DELETE
FROM public.grading_scheme
WHERE name NOT IN (
                   'Numerical (1-5)',
                   'Numerical (1-12)',
                   'Alphabetical (A-F)',
                   'Pass/Fail',
                   'Narrative',
                   'Percentage (0%-100%)',
                   'None'
    );

INSERT INTO public.jhi_authority (name)
VALUES ('ROLE_ADMIN');
INSERT INTO public.jhi_authority (name)
VALUES ('ROLE_STUDENT');
INSERT INTO public.jhi_authority (name)
VALUES ('ROLE_TEACHER');

INSERT INTO public.jhi_user (id, login, password_hash, first_name, last_name, email, activated, created_by)
VALUES (-1, 'admin', '$2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e', 'Administrator', 'Administrator',
        'admin@localhost', TRUE,
        'integration-test');
INSERT INTO public.jhi_user (id, login, password_hash, first_name, last_name, email, activated, created_by, country)
VALUES (-2, 'teacher', '$2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e', 'Teacher', 'Teacher',
        'teacher@localhost', TRUE, 'integration-test', 'Estonia');
INSERT INTO public.jhi_user (id, login, password_hash, first_name, last_name, email, activated, created_by)
VALUES (-3, 'student', '$2a$10$g1kZemwBmlUioHVa6uimhest5o80wTQ5iBk/H55Y701W3n5N/280e', 'Student', 'Student',
        'student@localhost', TRUE, 'integration-test');

INSERT INTO public.jhi_user_authority (user_id, authority_name)
VALUES (-1, 'ROLE_ADMIN');
INSERT INTO public.jhi_user_authority (user_id, authority_name)
VALUES (-2, 'ROLE_TEACHER');
INSERT INTO public.jhi_user_authority (user_id, authority_name)
VALUES (-3, 'ROLE_STUDENT');

INSERT INTO public.curriculum (id, title, country, requires_subject, sequence_number)
VALUES (1, 'TANZANIAN_CURRICULUM', 'Tanzania', TRUE, 1),
       (2, 'ESTONIAN_CURRICULUM', 'Estonia', TRUE, 2),
       (3, 'NATIONAL_CURRICULUM', NULL, TRUE, 3);
