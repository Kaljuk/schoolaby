import React from 'react';
import 'jest-canvas-mock';
import { Field } from 'react-final-form';
import * as MockedMarkdownEditorInput from 'app/shared/form';
import Input from 'app/shared/form/components/input';
import * as MockedFontAwesomeIcon from '@fortawesome/react-fontawesome';
import * as MockedMarkdownEditor from 'app/assignment/assignment-detail/submission/shared/markdown-editor';

Element.prototype.scrollTo = jest.fn();

window.matchMedia = jest.fn().mockImplementation(query => {
  return {
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(),
    removeListener: jest.fn(),
  };
});

window.document.createRange = jest.fn().mockImplementation(() => ({
  setEnd: jest.fn(),
  setStart: jest.fn(),
  getBoundingClientRect: jest.fn(),
  getClientRects: jest.fn(),
}));

window.HTMLElement.prototype.scrollIntoView = jest.fn();

const mockMediaRecorder = {
  start: jest.fn(),
  ondataavailable: jest.fn(),
  onerror: jest.fn(),
  state: '',
  addEventListener: jest.fn(),
  stop: jest.fn(),
};

// @ts-ignore
window.MediaRecorder = (jest.fn() as any).mockImplementation(() => mockMediaRecorder);

jest
  .spyOn(MockedMarkdownEditorInput, 'MarkdownEditorInput')
  .mockImplementation(s => <Field name={s.input.name} {...s} component={Input} />);

jest.spyOn(MockedMarkdownEditor, 'MarkdownEditor').mockImplementation(s => <input value={s.value} />);

jest.mock('react-jhipster', () => ({
  ...jest.requireActual('react-jhipster'),
  translate: jest.fn((value, interpolate) => {
    const values = interpolate && Object.values(interpolate).filter(param => !!param);
    return `${value}${values ? ' ' + values.join(', ') : ''}`;
  }),
}));

jest.mock('react-dom', () => ({
  ...jest.requireActual('react-dom'),
  createPortal: jest.fn(value => value),
}));

jest.spyOn(MockedFontAwesomeIcon, 'FontAwesomeIcon').mockImplementation(props => <svg aria-label={props.icon.toString()} />);

const mockGetUserMedia = jest.fn(async () => {
  return new Promise<MediaStream>(resolve => {
    resolve({
      getVideoTracks: jest.fn().mockImplementation(() => [
        {
          stop: jest.fn(),
        },
      ]),
      getAudioTracks: jest.fn().mockImplementation(() => [
        {
          stop: jest.fn(),
        },
      ]),
      active: true,
      id: 'asd',
      onaddtrack: jest.fn(),
      onremovetrack: jest.fn(),
      addTrack: jest.fn(),
      clone: jest.fn(),
      getTrackById: jest.fn(),
      getTracks: jest.fn(),
      removeTrack: jest.fn(),
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      dispatchEvent: jest.fn(),
    });
  });
});

// @ts-ignore
Object.defineProperty(global.navigator, 'mediaDevices', {
  value: {
    getUserMedia: mockGetUserMedia,
  },
});

HTMLFormElement.prototype.submit = jest.fn();

jest.setTimeout(15000);
