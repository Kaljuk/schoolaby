import React, { FC } from 'react';
import { IChat } from 'app/shared/model/chat.model';
import { ChatProvider } from 'app/shared/contexts/chat-context';
import MockChatContext from './mock-chat-context';

export interface IMockChatProvider {
  chat: IChat;
}

const MockChatProvider: FC<IMockChatProvider> = ({ children, chat }) => {
  return (
    <>
      <ChatProvider>
        <MockChatContext chat={chat}>{children}</MockChatContext>
      </ChatProvider>
    </>
  );
};

export default MockChatProvider;
