import React, { FC, useEffect } from 'react';
import { IMockJourneyTemplateProvider } from './mock-journey-template-provider';
import {
  useShowJourneyTemplatesViewState,
  useJourneyTemplateSubjectState,
  useSelectedJourneyTemplateState,
} from 'app/shared/contexts/journey-template-context';

const MockJourneyTemplateContext: FC<IMockJourneyTemplateProvider> = ({
  children,
  templateSubject,
  showTemplatesView,
  selectedTemplateId,
}) => {
  const { setTemplateSubject } = useJourneyTemplateSubjectState();
  const { setShowTemplatesView } = useShowJourneyTemplatesViewState();
  const { setSelectedTemplateId } = useSelectedJourneyTemplateState();

  useEffect(() => {
    setTemplateSubject(templateSubject);
    setShowTemplatesView(showTemplatesView);
    setSelectedTemplateId(selectedTemplateId);
  }, []);

  return <>{children}</>;
};

export default MockJourneyTemplateContext;
