import React, { FC, useEffect } from 'react';
import { useMaterialState } from 'app/shared/contexts/material-context';
import { IMockMaterialProvider } from './mock-material-provider';

const MockMaterialContext: FC<IMockMaterialProvider> = ({ children, materials }) => {
  const { setMaterials } = useMaterialState();

  useEffect(() => {
    setMaterials(materials);
  }, []);

  return <>{children}</>;
};

export default MockMaterialContext;
