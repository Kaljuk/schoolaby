import React, { FC } from 'react';
import { MaterialProvider } from 'app/shared/contexts/material-context';
import { Material } from 'app/shared/model/material.model';
import MockMaterialContext from './mock-material-context';

export interface IMockMaterialProvider {
  materials: Material[];
}

const MockMaterialProvider: FC<IMockMaterialProvider> = ({ children, materials }) => {
  return (
    <>
      <MaterialProvider>
        <MockMaterialContext materials={materials}>{children}</MockMaterialContext>
      </MaterialProvider>
    </>
  );
};

export default MockMaterialProvider;
