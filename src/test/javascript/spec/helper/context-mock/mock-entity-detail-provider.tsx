import React, { FC } from 'react';
import { EntityDetailProvider } from 'app/shared/contexts/entity-detail-context';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { GradingScheme } from 'app/shared/model/grading-scheme.model';
import { IUser } from 'app/shared/model/user.model';
import MockEntityDetailContext from './mock-entity-detail-context';
import { IAssignment } from 'app/shared/model/assignment.model';

export interface IMockEntityDetailProvider {
  entity?: any;
  submissions?: ISubmission[];
  gradingSchemeEntity?: GradingScheme;
  selectedUser?: IUser;
  deadlineTarget?: IAssignment;
}

const MockEntityDetailProvider: FC<IMockEntityDetailProvider> = ({
  children,
  entity,
  selectedUser,
  gradingSchemeEntity,
  submissions,
  deadlineTarget,
}) => {
  return (
    <>
      <EntityDetailProvider>
        <MockEntityDetailContext
          entity={entity}
          selectedUser={selectedUser}
          gradingSchemeEntity={gradingSchemeEntity}
          submissions={submissions}
          deadlineTarget={deadlineTarget}
        >
          {children}
        </MockEntityDetailContext>
      </EntityDetailProvider>
    </>
  );
};

export default MockEntityDetailProvider;
