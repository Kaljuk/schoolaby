import React, { FC, useEffect } from 'react';
import { useGeneratedChatState } from 'app/shared/contexts/chat-context';
import { IMockChatProvider } from './mock-chat-provider';

const MockChatContext: FC<IMockChatProvider> = ({ children, chat }) => {
  const { setGeneratedChat } = useGeneratedChatState();

  useEffect(() => {
    setGeneratedChat(chat);
  }, []);

  return <>{children}</>;
};

export default MockChatContext;
