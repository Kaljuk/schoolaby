import React from 'react';
import { fireEvent, render, screen, waitFor, within } from '@testing-library/react';
import { ThroughProvider } from 'react-through';
import { createBrowserHistory } from 'history';
import Marketplace from 'app/marketplace/marketplace';
import { RouteComponentProps, Router } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { MaterialProvider } from 'app/shared/contexts/material-context';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { COUNTRY } from 'app/config/constants';

const mockStore = configureMockStore();

describe('Marketplace', () => {
  const queryClient = new QueryClient();

  const createDefaultHistory = () => {
    const defaultHistory = createBrowserHistory();
    defaultHistory.push = jest.fn();

    defaultHistory.location.state = {
      entity: {
        title: 'Test Milestone',
        educationalAlignments: [
          {
            alignmentType: 'subjectArea',
            children: Array(0),
            country: 'Estonia',
            endDate: null,
            id: 3,
            startDate: '2020-07-01T05:00:12Z',
            targetName: 'Matemaatika',
            targetUrl: null,
            taxonId: 109,
            taxonIds: [109],
            title: 'Matemaatika',
          },
        ],
      },
    };

    return defaultHistory;
  };

  const createHistoryWithMaterials = () => {
    const defaultHistory = createDefaultHistory();
    defaultHistory.location.state = {
      entity: {
        title: 'Test Milestone',
        educationalAlignments: [
          {
            alignmentType: 'subjectArea',
            children: Array(0),
            country: 'Estonia',
            endDate: null,
            id: 3,
            startDate: '2020-07-01T05:00:12Z',
            targetName: 'Matemaatika',
            targetUrl: null,
            taxonId: 109,
            taxonIds: [109],
            title: 'Matemaatika',
          },
        ],
        materials: [
          {
            id: null,
            title: 'Kümnendmurru koostis. Kümnendmurru kujutamine arvkiirel 5. klass',
            type: '.EkoolikottMaterial',
            description: null,
            externalId: '20827',
            url: null,
            imageUrl: null,
            uploadedFile: null,
            educationalAlignments: [],
            restricted: null,
            createdBy: null,
            sequenceNumber: null,
          },
        ],
      },
    };

    return defaultHistory;
  };

  const getStore = () => ({
    authentication: {
      account: {
        country: 'Estonia',
      },
    },
    locale: {
      currentLocale: '',
    },
  });

  const marketplaceDefaultProps = (): RouteComponentProps<{ entityId: string }> => {
    return {
      history: undefined,
      location: undefined,
      match: {
        params: {
          entityId: '1',
        },
        isExact: true,
        path: 'path',
        url: '/marketplace/milestone/',
      },
    };
  };

  const renderMarketplace = (marketplaceProps = marketplaceDefaultProps(), history = createDefaultHistory(), store = getStore()) =>
    render(
      <Provider store={mockStore(store)}>
        <EntityUpdateProvider>
          <MaterialProvider>
            <QueryClientProvider client={queryClient}>
              <ThroughProvider>
                <Router history={history}>
                  <Marketplace {...marketplaceProps} />
                </Router>
              </ThroughProvider>
            </QueryClientProvider>
          </MaterialProvider>
        </EntityUpdateProvider>
      </Provider>
    );

  it('should redirect to not found page if entity is not passed in history state', () => {
    const history = createDefaultHistory();
    history.location.state = undefined;
    renderMarketplace(marketplaceDefaultProps(), history);

    expect(history.push).toBeCalledWith('/not_found');
  });

  it('should render marketplace components if entity is passed in history state', async () => {
    const props = marketplaceDefaultProps();
    props.match.url = '/marketplace/assignment/';
    renderMarketplace(props);

    const edAlignmentTag = screen.getByTestId('tag');
    expect(edAlignmentTag).toHaveTextContent('Matemaatika');

    screen.getByText('schoolabyApp.marketplace.addOwnMaterial');

    const searchField = screen.getByRole('textbox');
    expect(searchField).toHaveAttribute('placeholder', 'global.search');
    screen.getByText('entity.action.suggested');
    screen.getByText('schoolabyApp.marketplace.apps.title');

    screen.getByText('schoolabyApp.marketplace.popularOpiq');
    screen.getByText('schoolabyApp.marketplace.popularEKK');
    screen.getByText('schoolabyApp.marketplace.suggestedMaterials');
    await screen.findByText('schoolabyApp.material.home.notFound');

    screen.getByText('schoolabyApp.marketplace.noChosenMaterials');
    screen.getByRole('button', { name: 'entity.action.cancel' });
  });

  it('should open modal on add new link button clicked', async () => {
    renderMarketplace();

    const addNewMaterialButton = screen.getByText('schoolabyApp.marketplace.addOwnMaterial');
    const addLinkButton = screen.getByText('schoolabyApp.marketplace.add.link');

    await waitFor(() => {
      fireEvent.click(addNewMaterialButton);
      fireEvent.click(addLinkButton);
    });

    screen.getByText('schoolabyApp.marketplace.modal.add.link');
  });

  it('should render milestone translations if url contains /milestone', () => {
    renderMarketplace();

    screen.getByText('schoolabyApp.marketplace.milestone.title Test Milestone');
    screen.getByText('schoolabyApp.marketplace.milestone.chosen Test Milestone');
    screen.getByRole('button', { name: 'schoolabyApp.marketplace.milestone.add' });
  });

  it('should render assignment translations if url contains /assignment', () => {
    const marketPlaceProps = { ...marketplaceDefaultProps() };
    marketPlaceProps.match.url = '/marketplace/assignment/';
    renderMarketplace(marketPlaceProps);

    screen.getByText('schoolabyApp.marketplace.assignment.title Test Milestone');
    screen.getByText('schoolabyApp.marketplace.assignment.chosen Test Milestone');
    screen.getByRole('button', { name: 'schoolabyApp.marketplace.assignment.add' });
  });

  it('should disable add to milestone/assignment button if there are no chosen materials', () => {
    renderMarketplace();

    const addButton = screen.getByRole('button', { name: 'schoolabyApp.marketplace.milestone.add' });
    expect(addButton).toBeDisabled();
  });

  it('should show material cards', async () => {
    renderMarketplace();

    await new Promise(r => setTimeout(r, 1000));

    const materialCards = await screen.findAllByTestId('material-card');
    expect(materialCards).toHaveLength(6);
  });

  it('should show search results', async () => {
    renderMarketplace();

    const searchField = screen.getByRole('textbox');
    fireEvent.change(searchField, { target: { value: 'value' } });

    await new Promise(r => setTimeout(r, 1000));

    const materialCards = await screen.findAllByTestId('material-card');
    await waitFor(() => expect(materialCards).toHaveLength(8));
  });

  it('should add material to chosen materials section when add button is clicked', async () => {
    renderMarketplace();

    const materialCards = await screen.findAllByTestId('material-card');
    const addButton = within(materialCards[0]).getByRole('button');
    fireEvent.click(addButton);

    const updatedMaterialCards = await screen.findAllByTestId('material-card');
    expect(updatedMaterialCards).toHaveLength(7);

    const noMaterialsChosenText = screen.queryByText('schoolabyApp.marketplace.noChosenMaterials');
    expect(noMaterialsChosenText).not.toBeInTheDocument();

    const addToEntityButton = screen.getByRole('button', { name: 'schoolabyApp.marketplace.milestone.add' });
    expect(addToEntityButton).toBeEnabled();
  });

  it('should show chosen materials if entity has any and remove material from chosen on remove button clicked', async () => {
    renderMarketplace(marketplaceDefaultProps(), createHistoryWithMaterials());

    const chosenMaterialsSection = screen.getByText('schoolabyApp.marketplace.milestone.chosen Test Milestone').closest('div');
    const chosenMaterial = within(chosenMaterialsSection).getByTestId('material-card');
    within(chosenMaterial).getByText('Kümnendmurru koostis. Kümnendmurru kujutamine arvkiirel 5. klass');

    const ekoolikottSection = screen.getByText('schoolabyApp.marketplace.popularEKK').closest('div');
    const materialCard = await within(ekoolikottSection).findByTestId('material-card');
    within(materialCard).getByText('Kümnendmurru koostis. Kümnendmurru kujutamine arvkiirel 5. klass');
    expect(within(materialCard).getByRole('button')).toBeDisabled();

    const allMaterialCards = screen.getAllByTestId('material-card');
    expect(allMaterialCards).toHaveLength(7);

    const deleteButton = within(chosenMaterial).getByRole('button');
    fireEvent.click(deleteButton);

    screen.getByText('schoolabyApp.marketplace.noChosenMaterials');
    const materialCardAfterDelete = await within(ekoolikottSection).findByTestId('material-card');
    const allMaterialCardsAfterDelete = screen.getAllByTestId('material-card');
    expect(within(materialCardAfterDelete).getByRole('button')).toBeEnabled();
    expect(allMaterialCardsAfterDelete).toHaveLength(6);
  });

  it('should open material card modal when material is clicked', async () => {
    renderMarketplace();

    const ekoolikottSection = screen.getByText('schoolabyApp.marketplace.popularEKK').closest('div');
    const materialCard = await within(ekoolikottSection).findByTestId('material-card');

    fireEvent.click(materialCard);

    const dialog = screen.getByRole('dialog');
    within(dialog).getByRole('heading', { name: 'Kümnendmurru koostis. Kümnendmurru kujutamine arvkiirel 5. klass' });

    const closeButton = within(dialog).getByRole('close-button');
    fireEvent.click(closeButton);
    expect(
      within(dialog).queryByRole('heading', { name: 'Kümnendmurru koostis. Kümnendmurru kujutamine arvkiirel 5. klass' })
    ).not.toBeInTheDocument();
  });

  it('should not display apps option when milestone in url', async () => {
    renderMarketplace();

    await waitFor(() => {
      expect(screen.queryByText('schoolabyApp.marketplace.apps.title')).not.toBeInTheDocument();
    });
  });

  it('should display apps section', async () => {
    const props = marketplaceDefaultProps();
    props.match.url = '/marketplace/assignment/';
    renderMarketplace(props);

    const appsButton = await screen.findByText('schoolabyApp.marketplace.apps.title');
    fireEvent.click(appsButton);

    await screen.findByText('schoolabyApp.marketplace.apps.associatedApps');
  });

  it('should hide OPIQ and E-Koolikott sections when user is Ukrainian', async () => {
    const store = getStore();
    store.authentication.account.country = COUNTRY.UKRAINE;
    renderMarketplace(marketplaceDefaultProps(), createDefaultHistory(), store);

    await screen.findByText('schoolabyApp.marketplace.suggestedMaterials');
    expect(screen.queryByText('schoolabyApp.marketplace.popularOpiq')).not.toBeInTheDocument();
    expect(screen.queryByText('schoolabyApp.marketplace.popularEKK')).not.toBeInTheDocument();
  });
});
