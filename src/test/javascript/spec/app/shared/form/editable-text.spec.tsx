import EditableText from 'app/shared/form/editable-text';
import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

describe('EditableText', () => {
  const Example = () => <h1>Test content</h1>;

  const defaultProps = {
    name: 'test-name',
    initialValue: 'test value',
    validate: jest.fn(),
    editor: 'text',
    isAllowedToModify: true,
  };

  const component = (props?) => <EditableText {...defaultProps} {...props} />;

  it('should call patch request when clicking save', () => {
    const mockedPatchRequest = {
      mutateAsync: jest.fn(),
    };

    render(
      component({
        children: <Example />,
        patchRequest: mockedPatchRequest,
      })
    );

    const content = screen.getByText('Test content');

    fireEvent.click(content);

    screen.getByText('entity.action.cancel');
    screen.getByDisplayValue('test value');
    const saveButton = screen.getByText('entity.action.save');

    fireEvent.click(saveButton);

    expect(mockedPatchRequest.mutateAsync).toHaveBeenCalled();
  });

  it('should show original content when clicking cancel', () => {
    const mockedPatchRequest = jest.fn();

    render(
      component({
        children: <Example />,
        patchRequest: mockedPatchRequest,
      })
    );

    const content = screen.getByText('Test content');

    fireEvent.click(content);

    expect(screen.queryByText('Test content')).toBeNull();

    const cancelButton = screen.getByText('entity.action.cancel');
    screen.getByText('entity.action.save');

    fireEvent.click(cancelButton);

    screen.getByText('Test content');
    expect(mockedPatchRequest).not.toHaveBeenCalled();
  });

  it('should not be editable if not allowed to modify', () => {
    const mockedPatchRequest = jest.fn();

    render(
      component({
        children: <Example />,
        patchRequest: mockedPatchRequest,
        isAllowedToModify: false,
      })
    );

    const content = screen.getByText('Test content');

    fireEvent.click(content);
    const inputField = screen.queryByRole('textbox');
    expect(inputField).not.toBeInTheDocument();
  });
});
