import React from 'react';
import { render, screen } from '@testing-library/react';
import { DatePicker } from 'app/shared/form';
import { Field, Form } from 'react-final-form';
import { DateTime } from 'luxon';
import { DATEPICKER_DATE_FORMAT, DATEPICKER_DATE_TIME_FORMAT } from 'app/shared/form/components/date-picker';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';

describe('DatePicker', () => {
  const mockStore = configureMockStore();

  const renderComponent = (dateTime: typeof DATEPICKER_DATE_FORMAT | typeof DATEPICKER_DATE_TIME_FORMAT = DATEPICKER_DATE_FORMAT) => {
    render(
      <Provider store={mockStore({ locale: { currentLocale: 'en' } })}>
        <Form
          onSubmit={() => {}}
          initialValues={{ deadline: DateTime.fromISO('2021-06-17T15:30:00.000', { zone: 'local' }) }}
          render={() => (
            <form onSubmit={() => {}}>
              <Field
                id="assignment-deadline"
                type="datetime-local"
                className="form-control"
                name="deadline"
                placeholder={'DD/MM/YYYY HH:mm'}
                render={dateProps => {
                  return <DatePicker meta={dateProps.meta} input={dateProps.input} dateTimeFormat={dateTime} />;
                }}
              />
            </form>
          )}
        />
      </Provider>
    );
  };

  it('Should show date and time when specified', () => {
    renderComponent(DATEPICKER_DATE_TIME_FORMAT);
    const textbox = screen.getByRole('textbox');
    expect(textbox).toHaveValue('17/06/2021 15:30');
  });

  it('Should show date when specified', () => {
    renderComponent();
    const textbox = screen.getByRole('textbox');
    expect(textbox).toHaveValue('17/06/2021');
  });
});
