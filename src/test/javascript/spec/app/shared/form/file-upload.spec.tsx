import React from 'react';
import configureMockStore from 'redux-mock-store';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import MockEntityDetailProvider from '../../../helper/context-mock/mock-entity-detail-provider';
import ContextProvider from '../../../helper/context-mock/context-provider';
import { IAssignment } from 'app/shared/model/assignment.model';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import FileUpload from 'app/shared/form/file-upload';
import { FileUploadProvider } from 'app/shared/form/file-upload-context';

describe('File upload and download', () => {
  let store: any;
  const mockStore = configureMockStore();

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'en',
      },
      applicationProfile: {
        isInProduction: false,
      },
    };
    store = mockStore(initialState);
  });

  const assignment: IAssignment = {
    id: 11,
    state: EntityState.IN_PROGRESS,
  };

  const renderComponent = () =>
    render(
      <ContextProvider contextProvider={MockEntityDetailProvider} store={store} props={{ entity: assignment }}>
        <FileUploadProvider>
          <FileUpload existingFiles={[]} onUpload={() => undefined} onDelete={() => undefined} uploadDisabled={false} />
        </FileUploadProvider>
      </ContextProvider>
    );

  const openCameraModal = async () => {
    const cameraButton = await screen.findByText('global.upload.camera');
    fireEvent.click(cameraButton);
  };

  const closeCameraModal = async () => {
    const modalCloseButton = await screen.findByRole('close-button');
    fireEvent.click(modalCloseButton);
  };

  const minimizeCameraModal = async () => {
    const minimizeCameraModalButton = await screen.findByLabelText('schoolabyApp.submission.camera.minimize');
    fireEvent.click(minimizeCameraModalButton);
  };

  it('should render uppy upload component', async () => {
    renderComponent();
    await screen.findByText('global.upload.dropPasteImport');
    await screen.findByText('global.upload.browse');
    await screen.findByText('global.upload.camera');
  });

  it('should open camera modal', async () => {
    renderComponent();
    await openCameraModal();

    await screen.findByText('schoolabyApp.uploadedFile.cameraModal.title');
    await screen.findByLabelText('schoolabyApp.submission.camera.takePicture');
    await screen.findByLabelText('schoolabyApp.submission.camera.startRecording');
    await screen.findByLabelText('schoolabyApp.submission.camera.minimize');

    expect(screen.queryByLabelText('schoolabyApp.submission.camera.stopRecording')).not.toBeInTheDocument();
    expect(screen.queryByLabelText('schoolabyApp.submission.camera.maximize')).not.toBeInTheDocument();
    expect(screen.queryByLabelText('schoolabyApp.submission.camera.close')).not.toBeInTheDocument();
    expect(screen.queryByLabelText('schoolabyApp.submission.camera.toggleFacingMode')).not.toBeInTheDocument();
  });

  it('should minimize and maximize', async () => {
    renderComponent();
    await openCameraModal();
    await minimizeCameraModal();

    await screen.findByLabelText('schoolabyApp.submission.camera.takePicture');
    await screen.findByLabelText('schoolabyApp.submission.camera.startRecording');
    await screen.findByLabelText('schoolabyApp.submission.camera.close');
    const maximizeCameraModal = await screen.findByLabelText('schoolabyApp.submission.camera.maximize');

    expect(screen.queryByText('schoolabyApp.uploadedFile.cameraModal.title')).not.toBeInTheDocument();
    expect(screen.queryByLabelText('schoolabyApp.submission.camera.stopRecording')).not.toBeInTheDocument();
    expect(screen.queryByLabelText('schoolabyApp.submission.camera.toggleFacingMode')).not.toBeInTheDocument();
    expect(screen.queryByLabelText('schoolabyApp.submission.camera.minimize')).not.toBeInTheDocument();

    fireEvent.click(maximizeCameraModal);
    await screen.findByText('schoolabyApp.uploadedFile.cameraModal.title');
  });

  it('should close minimized', async () => {
    renderComponent();
    await openCameraModal();
    await minimizeCameraModal();

    expect(screen.queryByText('global.upload.browse')).not.toBeInTheDocument();
    expect(screen.queryByText('global.upload.camera')).not.toBeInTheDocument();

    const closeButton = await screen.findByLabelText('schoolabyApp.submission.camera.close');
    fireEvent.click(closeButton);

    await screen.findByText('global.upload.browse');
    await screen.findByText('global.upload.camera');
  });

  it('should minimize modal on close if capturing', async () => {
    renderComponent();
    await openCameraModal();

    const startRecording = await screen.findByLabelText('schoolabyApp.submission.camera.startRecording');
    fireEvent.click(startRecording);

    await closeCameraModal();

    await screen.findByLabelText('schoolabyApp.submission.camera.close');
    await waitFor(() => {
      expect(screen.queryByText('schoolabyApp.uploadedFile.cameraModal.title')).not.toBeInTheDocument();
    });
    expect(screen.queryByText('global.upload.browse')).not.toBeInTheDocument();
    expect(screen.queryByText('global.upload.camera')).not.toBeInTheDocument();
  });

  it('should close modal if not capturing', async () => {
    renderComponent();
    await openCameraModal();
    await closeCameraModal();

    await screen.findByText('global.upload.browse');
    await screen.findByText('global.upload.camera');

    await waitFor(() => {
      expect(screen.queryByText('schoolabyApp.uploadedFile.cameraModal.title')).not.toBeInTheDocument();
    });
  });
});
