import { orderDateTimes } from 'app/shared/util/date-utils';

describe('Date utils', () => {
  describe('orderDateTimes', () => {
    it('should order in given direction', () => {
      const date1 = '2021-01-01T00:00:01Z';
      const date2 = '2021-01-01T00:00:02Z';
      const date3 = '2021-01-01T00:00:03Z';
      const unorderedDateTimeStrings = [date2, date1, date3];

      expect(orderDateTimes(unorderedDateTimeStrings, 'asc')).toEqual([date1, date2, date3]);
      expect(orderDateTimes(unorderedDateTimeStrings, 'desc')).toEqual([date3, date2, date1]);
    });
  });
});
