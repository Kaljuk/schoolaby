import {
  DANGER,
  getAssignmentCheckmarkColor,
  getClassNameByAssignmentState,
  getClassNameByMilestoneState,
  getColor,
  PRIMARY,
  SECONDARY,
} from 'app/shared/util/color-utils';
import { EntityState } from 'app/shared/model/enumerations/entity-state';

describe('Color utils', () => {
  it('should get correct assignment checkmark color', () => {
    expect(getAssignmentCheckmarkColor({ state: EntityState.COMPLETED })).toBe(PRIMARY);
    expect(getAssignmentCheckmarkColor({ state: EntityState.IN_PROGRESS })).toBe(SECONDARY);
    expect(getAssignmentCheckmarkColor({ state: EntityState.NOT_STARTED })).toBe(SECONDARY);
    expect(getAssignmentCheckmarkColor({ state: EntityState.URGENT })).toBe(SECONDARY);
    expect(getAssignmentCheckmarkColor({ state: EntityState.REJECTED })).toBe(DANGER);
    expect(getAssignmentCheckmarkColor({ state: EntityState.FAILED })).toBe(DANGER);
    expect(getAssignmentCheckmarkColor({ state: EntityState.OVERDUE })).toBe('');
  });

  it('should get correct color', () => {
    expect(getColor('success', 'border')).toBe(undefined);
    expect(getColor('success', 'bg')).toBe('bg-success');
    expect(getColor('danger', 'border')).toBe('border-danger');
  });

  it('should get correct className by assignment state', () => {
    expect(getClassNameByAssignmentState(EntityState.COMPLETED, 'border')).toBe('border-green');
    expect(getClassNameByAssignmentState(EntityState.NOT_STARTED, 'bg', 'transparent')).toBe('transparent-bg-blue');
    expect(getClassNameByAssignmentState(EntityState.IN_PROGRESS, 'bg', 'transparent')).toBe('transparent-bg-orange');
    expect(getClassNameByAssignmentState(EntityState.URGENT, 'text')).toBe('text-orange');
    expect(getClassNameByAssignmentState(EntityState.REJECTED, 'bg', 'light')).toBe('light-bg-red');
    expect(getClassNameByAssignmentState(EntityState.OVERDUE, 'bg', 'medium')).toBe('medium-bg-red');
    expect(getClassNameByAssignmentState(undefined, 'bg')).toBe('');
  });

  it('should get correct className by milestone state', () => {
    expect(getClassNameByMilestoneState(EntityState.COMPLETED, 'border')).toBe('border-green');
    expect(getClassNameByMilestoneState(EntityState.NOT_STARTED, 'bg', 'transparent')).toBe('transparent-bg-blue');
    expect(getClassNameByMilestoneState(EntityState.IN_PROGRESS, 'bg', 'transparent')).toBe('transparent-bg-orange');
    expect(getClassNameByMilestoneState(EntityState.REJECTED, 'bg', 'light')).toBe('light-bg-red');
    expect(getClassNameByMilestoneState(EntityState.OVERDUE, 'bg', 'medium')).toBe('medium-bg-red');
    expect(getClassNameByMilestoneState(EntityState.COMPLETED, 'text')).toBe('');
    expect(getClassNameByMilestoneState(EntityState.REJECTED, 'text')).toBe('text-red');
    expect(getClassNameByMilestoneState(EntityState.OVERDUE, 'text')).toBe('text-red');
    expect(getClassNameByMilestoneState(undefined, 'bg')).toBe('');
  });
});
