import { isPastDeadline } from 'app/assignment/assignment-detail/assignment-util';
import { IAssignment } from 'app/shared/model/assignment.model';
import { DateTime } from 'luxon';

describe('Assignment utils', () => {
  describe('isPastDeadline', () => {
    it('Should return true if assignment deadline has passed', () => {
      const deadline = new Date(DateTime.local());
      deadline.setMinutes(deadline.getMinutes() - 1);
      const assignment: IAssignment = {
        deadline: deadline.toISOString(),
      };

      expect(isPastDeadline(assignment)).toEqual(true);
    });

    it('Should return false if assignment deadline has not passed', () => {
      const deadline = new Date(DateTime.local());
      deadline.setMinutes(deadline.getMinutes() + 1);
      const assignment: IAssignment = {
        deadline: deadline.toISOString(),
      };

      expect(isPastDeadline(assignment)).toEqual(false);
    });
  });
});
