import React from 'react';
import configureMockStore from 'redux-mock-store';
import { fireEvent, render, screen } from '@testing-library/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { AppContext } from 'app/app';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import AssignmentMobileMenu from 'app/shared/layout/mobile-menus/assignment-mobile-menu/assignment-mobile-menu';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { Provider } from 'react-redux';
import { EntityDetailProvider } from 'app/shared/contexts/entity-detail-context';

jest.mock('react-router-dom', () => ({
  __esModule: true,
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: jest.fn(),
    location: {
      pathname: '/assignment/1/',
    },
  }),
}));

describe('Assignment mobile menu', () => {
  const mockStore = configureMockStore();
  const queryClient = new QueryClient();
  const defaultProps = () => ({
    journey: {
      id: 1,
    },
    assignment: {
      id: 1,
    },
    isAllowedToModify: true,
  });

  const defaultStore = () => ({ applicationProfile: { inProduction: false } });

  const renderComponent = (props = defaultProps(), isNewAssignmentViewDesign = false) =>
    render(
      <Provider store={mockStore(defaultStore())}>
        <AppContext.Provider value={{ setModal() {} }}>
          <QueryClientProvider client={queryClient}>
            <EntityUpdateProvider>
              <EntityDetailProvider>
                <AssignmentContext.Provider value={props}>
                  <AssignmentMobileMenu newAssignmentViewDesign={isNewAssignmentViewDesign} />
                </AssignmentContext.Provider>
              </EntityDetailProvider>
            </EntityUpdateProvider>
          </QueryClientProvider>
        </AppContext.Provider>
      </Provider>
    );

  it('should display tabs and more options button', async () => {
    renderComponent();

    await screen.findByText('schoolabyApp.assignment.detail.tabs.assignment');
    await screen.findByText('schoolabyApp.assignment.detail.tabs.backpack');
    await screen.findByText('schoolabyApp.assignment.detail.tabs.analytics');
    await screen.findByText('global.menu.more');
  });

  it('should display options', async () => {
    renderComponent();

    const moreOptionsButton = await screen.findByText('global.menu.more');
    fireEvent.click(moreOptionsButton);

    await screen.findByText('entity.action.edit');
    await screen.findByText('entity.action.delete');
  });

  it('should not show analytics tab if not allowed to modify', async () => {
    const props = { ...defaultProps(), isAllowedToModify: false };
    renderComponent(props);

    await screen.findByText('schoolabyApp.assignment.detail.tabs.assignment');
    await screen.findByText('schoolabyApp.assignment.detail.tabs.backpack');
    expect(screen.queryByText('schoolabyApp.assignment.detail.tabs.analytics')).not.toBeInTheDocument();
  });

  it('should show students tab if new assignment view design and is allowed to modify', async () => {
    renderComponent(defaultProps(), true);

    await screen.findByText('schoolabyApp.assignment.submissions');
  });

  it('should not show students tab if new assignment view design and not allowed to modify', async () => {
    const props = { ...defaultProps(), isAllowedToModify: false };
    renderComponent(props, true);

    await screen.findByText('schoolabyApp.assignment.detail.tabs.assignment');
    await screen.findByText('schoolabyApp.assignment.detail.tabs.backpack');
    expect(screen.queryByText('global.heading.tabs.students')).not.toBeInTheDocument();
  });

  it('should not show students tab if not new assignment view design and allowed to modify', async () => {
    renderComponent();

    await screen.findByText('schoolabyApp.assignment.detail.tabs.assignment');
    await screen.findByText('schoolabyApp.assignment.detail.tabs.backpack');
    expect(screen.queryByText('global.heading.tabs.students')).not.toBeInTheDocument();
  });
});
