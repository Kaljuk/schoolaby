import React from 'react';
import { render, screen } from '@testing-library/react';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { QueryClient, QueryClientProvider } from 'react-query';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { RootProvider } from 'app/shared/contexts/root-context';
import SubjectSearchDropdownNew from 'app/shared/layout/subject-search-dropdown-new/subject-search-dropdown-new';

describe('SubjectSearchDropdownNew', function () {
  let store;
  const queryClient = new QueryClient();
  const mockStore = configureMockStore();

  const renderComponent = props => {
    return render(
      <RootProvider>
        <Provider store={store}>
          <EntityUpdateProvider>
            <QueryClientProvider client={queryClient}>
              <SubjectSearchDropdownNew {...props} />
            </QueryClientProvider>
          </EntityUpdateProvider>
        </Provider>
      </RootProvider>
    );
  };

  beforeEach(() => {
    const initialState = {
      authentication: {
        account: {
          country: 'Estonia',
        },
      },
    };
    store = mockStore(initialState);
  });

  it('should render placeholder', () => {
    renderComponent({ disabled: false });

    screen.getByText('entity.action.searchByKeyword');
  });
});
