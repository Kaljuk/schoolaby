import React from 'react';
import { AUTHORITIES } from 'app/config/constants';
import configureMockStore from 'redux-mock-store';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import NotificationDropdown from 'app/shared/layout/header/notification/notification-dropdown';
import { QueryClient, QueryClientProvider } from 'react-query';
import { UserNotificationProvider } from 'app/shared/contexts/user-notification-context';

describe('Notification dropdown', () => {
  let store: any;
  const mockStore = configureMockStore();
  const queryClient = new QueryClient();

  beforeAll(() => {
    store = mockStore({
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
      applicationProfile: {
        inProduction: false,
      },
    });
  });

  const renderComponent = () => {
    return render(
      <QueryClientProvider client={queryClient}>
        <Provider store={store}>
          <UserNotificationProvider>
            <NotificationDropdown
              icon={<></>}
              title={<></>}
              notifications={[]}
              unreadCount={0}
              isChats
              dropdownOpen={false}
              setDropdownOpen={() => {}}
            />
          </UserNotificationProvider>
        </Provider>
      </QueryClientProvider>,
      { wrapper: MemoryRouter }
    );
  };

  test('user can see button that redirects to chats view', () => {
    renderComponent();

    const button = screen.getByText('global.header.viewAll');
    // @ts-ignore
    expect(button.href).toContain('/chats');
  });
});
