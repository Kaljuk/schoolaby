import React from 'react';
import Header from 'app/shared/layout/header/header';
import { render } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { QueryClient, QueryClientProvider } from 'react-query';
import { UserNotificationProvider } from 'app/shared/contexts/user-notification-context';

describe('Header', () => {
  let store: any;
  const mockStore = configureMockStore();
  const history = createMemoryHistory();
  const queryClient = new QueryClient();

  const headerProps = {
    hasSelectedRole: true,
    isAdmin: true,
    ribbonEnv: 'dev',
    isInProduction: false,
    isSwaggerEnabled: false,
  };

  const adminProps = {
    ...headerProps,
    isAdmin: true,
  };
  const userProps = {
    ...headerProps,
    isAdmin: false,
  };
  const guestProps = {
    ...headerProps,
    isAdmin: false,
    hasSelectedRole: false,
  };

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'et',
      },
      applicationProfile: {
        inProduction: false,
      },
    };
    store = mockStore(initialState);
  });

  const wrapper = props => {
    return render(
      <QueryClientProvider client={queryClient}>
        <Provider store={store}>
          <UserNotificationProvider>
            <Router history={history}>
              <Header {...props} />
            </Router>
          </UserNotificationProvider>
        </Provider>
      </QueryClientProvider>
    );
  };

  it('should display Header LeftMenu component Admin items', () => {
    const component = wrapper(adminProps);
    component.getByText('global.menu.dashboard');
    component.getByText('global.menu.journeys');
    component.getByText('global.menu.settings');
    component.getByText('global.menu.entities.main');

    component.getByText('global.header.messages');
    component.getByText('global.header.notifications');
    component.getByText('UU');
    component.getByText('EN');
  });

  it('should display Header LeftMenu component User items', () => {
    const component = wrapper(userProps);
    component.getByText('global.menu.dashboard');
    component.getByText('global.menu.journeys');

    component.getByText('global.header.messages');
    component.getByText('global.header.notifications');
    component.getByText('UU');
    component.getByText('EN');
  });

  it('should display Header LeftMenu component default items', () => {
    const component = wrapper(guestProps);
    component.getByLabelText('global.locale');
  });
});
