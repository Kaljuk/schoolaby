import { fireEvent, render, screen, within } from '@testing-library/react';
import React from 'react';
import configureMockStore from 'redux-mock-store';
import JourneyOptionsModal from 'app/shared/layout/journey-options/journey-options-modal/journey-options-modal';
import { Provider } from 'react-redux';
import { AUTHORITIES } from 'app/config/constants';
import { QueryClient, QueryClientProvider } from 'react-query';
import { JourneyTemplateProvider } from 'app/shared/contexts/journey-template-context';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

describe('Journey options modal', () => {
  let store: any;
  const mockStore = configureMockStore();

  beforeAll(() => {
    store = mockStore({
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
      locale: {
        currentLocale: 'et',
      },
    });
  });

  const renderComponent = () =>
    render(
      <Provider store={store}>
        <JourneyTemplateProvider>
          <QueryClientProvider client={new QueryClient()}>
            <JourneyOptionsModal isModalOpen closeModal={() => {}} />
          </QueryClientProvider>
        </JourneyTemplateProvider>
      </Provider>
    );

  it('should display new journey option', () => {
    renderComponent();
    screen.getByText('schoolabyApp.journey.createNewJourney');
  });

  it('should display new journey from template option', () => {
    renderComponent();
    screen.getByText('schoolabyApp.journey.useJourneyTemplate');
  });

  it('should display new journey modal on new journey option click', async () => {
    renderComponent();
    const newJourneyOption = await screen.findByText('schoolabyApp.journey.createNewJourney');
    fireEvent.click(newJourneyOption);

    await screen.findByText('schoolabyApp.journey.addJourneyTitle');
    await screen.findByPlaceholderText('schoolabyApp.journey.journeyTitlePlaceholder');
    await screen.findByText('entity.action.cancel');
    await screen.findByText('entity.action.add');
  });

  it('should display journey template modal on journey template option click', async () => {
    renderComponent();
    const newJourneyOption = await screen.findByText('schoolabyApp.journey.useJourneyTemplate');
    fireEvent.click(newJourneyOption);

    await screen.findByText('schoolabyApp.journey.selectSubjectToNewJourney');
    await screen.findByText('schoolabyApp.journey.selectSubject');
    await screen.findByText('entity.action.cancel');
    await screen.findByText('entity.action.add');
  });

  it('should display error message when journey title field empty on submit', async () => {
    renderComponent();
    fireEvent.click(screen.getByText('schoolabyApp.journey.createNewJourney'));
    fireEvent.click(await screen.findByText('entity.action.add'));
    await screen.findByText('entity.validation.required');
  });

  it('should display error message when template subject empty on submit', async () => {
    renderComponent();
    fireEvent.click(screen.getByText('schoolabyApp.journey.useJourneyTemplate'));
    fireEvent.click(await screen.findByText('entity.action.add'));
    await screen.findByText('schoolabyApp.journey.subject.required');
  });

  it('should not show warning text on options', () => {
    renderComponent();

    const templateOption = screen.queryByText('schoolabyApp.journey.useJourneyTemplate').closest('div');
    const templateWarningText = within(templateOption).queryByText('schoolabyApp.journey.useJourneyTemplateBlocked');
    expect(templateWarningText).not.toBeInTheDocument();

    const newJourneyOption = screen.getByText('schoolabyApp.journey.createNewJourney').closest('div');
    const warningText = within(newJourneyOption).queryByText('schoolabyApp.journey.useJourneyTemplateBlocked');
    expect(warningText).not.toBeInTheDocument();
  });
});
