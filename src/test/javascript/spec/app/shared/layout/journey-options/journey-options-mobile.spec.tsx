import React from 'react';
import { AUTHORITIES } from 'app/config/constants';
import { JourneyTemplateProvider } from 'app/shared/contexts/journey-template-context';
import JourneyOptionsModal from 'app/shared/layout/journey-options/journey-options-modal/journey-options-modal';
import { render, screen, fireEvent, within } from '@testing-library/react';
import { Provider } from 'react-redux';
import { QueryClientProvider, QueryClient } from 'react-query';
import configureMockStore from 'redux-mock-store';

jest.mock('react-device-detect', () => ({
  isMobile: true,
}));

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

describe('Journey options modal mobile', () => {
  let store: any;
  const mockStore = configureMockStore();

  beforeAll(() => {
    store = mockStore({
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
      locale: {
        currentLocale: 'et',
      },
    });
  });

  const renderComponent = () =>
    render(
      <Provider store={store}>
        <JourneyTemplateProvider>
          <QueryClientProvider client={new QueryClient()}>
            <JourneyOptionsModal isModalOpen closeModal={() => {}} />
          </QueryClientProvider>
        </JourneyTemplateProvider>
      </Provider>
    );

  it('should not open journey template modal on template option click', async () => {
    renderComponent();

    const templateOption = await screen.findByText('schoolabyApp.journey.useJourneyTemplate');
    fireEvent.click(templateOption);
    expect(screen.queryByText('schoolabyApp.journey.selectSubjectToNewJourney')).not.toBeInTheDocument();
  });

  it('should show warning text only on template option', () => {
    renderComponent();

    const templateOption = screen.getByText('schoolabyApp.journey.useJourneyTemplate').closest('div');
    within(templateOption).getByText('schoolabyApp.journey.useJourneyTemplateBlocked');

    const newJourneyOption = screen.getByText('schoolabyApp.journey.createNewJourney').closest('div');
    const warningText = within(newJourneyOption).queryByText('schoolabyApp.journey.useJourneyTemplateBlocked');
    expect(warningText).not.toBeInTheDocument();
  });
});
