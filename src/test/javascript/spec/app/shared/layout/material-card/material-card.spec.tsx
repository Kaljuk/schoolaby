import MaterialNarrowCard from 'app/shared/layout/material-card/material-narrow-card';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import React from 'react';
import { E_KOOLIKOTT_MATERIAL, Material, OPIQ_MATERIAL } from 'app/shared/model/material.model';
import { MemoryRouter } from 'react-router-dom';
import { rest } from 'msw';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { AUTHORITIES } from 'app/config/constants';
import { server } from '../../../../rtl-setup';
import { MaterialProvider } from 'app/shared/contexts/material-context';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';

global['document'].createRange = () => ({
  setStart() {},
  setEnd() {},
  // @ts-ignore
  commonAncestorContainer: {
    nodeName: 'BODY',
    ownerDocument: document,
  },
});

describe('MaterialCard', () => {
  let store: any;
  const mockStore = configureMockStore();

  beforeAll(() => {
    store = mockStore({
      authentication: {
        account: {
          authorities: [AUTHORITIES.STUDENT],
          login: 'teacher',
        },
      },
      locale: {
        currentLocale: 'en',
      },
    });
  });

  const renderComponent = (material, props) => {
    return render(
      <Provider store={store}>
        <EntityUpdateProvider>
          <MaterialProvider>
            <MaterialNarrowCard material={material} {...props} />
          </MaterialProvider>
        </EntityUpdateProvider>
      </Provider>,
      { wrapper: MemoryRouter }
    );
  };

  const getMaterial = (): Material => {
    return Material.fromJson({
      title: 'title text',
      description: 'description text',
      url: 'http://google.com',
    });
  };

  test('user can see link material details in modal', async () => {
    renderComponent(getMaterial(), { add: false });

    fireEvent.click(screen.getByRole('img'));

    await waitFor(() => screen.getByRole('dialog'));
    expect(screen.queryByRole('dialog')).toBeVisible();
    expect(screen.getByRole('close-button'));
    expect(screen.getAllByText(/title text/i));
    expect(screen.getAllByText(/description text/i)).toHaveLength(2);
    expect(screen.getByRole('url-textbox')).toHaveDisplayValue(/http:\/\/google.com/i);
  });

  test('user can see eKoolikott material details', async () => {
    const material = getMaterial();
    material.externalId = '1102';
    material.type = E_KOOLIKOTT_MATERIAL;
    renderComponent(material, false);

    fireEvent.click(screen.getByTestId('material-card'));

    await waitFor(() => screen.getByRole('iframe'));
    expect(screen.getByRole('iframe')).toHaveProperty('src', 'https://e-koolikott.ee/oppematerjal/1102');
  });

  test('user can see material details with no-embed', async () => {
    server.use(
      rest.get('https://noembed.com/embed', (req, res, ctx) => {
        return res(ctx.json({ html: '<span>no-embed content</span>' }));
      })
    );
    const material = getMaterial();
    material.type = 'LINK';
    renderComponent(material, { add: false });

    fireEvent.click(screen.getByRole('img'));

    await waitFor(() => screen.getByText('no-embed content'));
  });

  it('Should show created by you icon and tooltip when material is created by current user', async () => {
    const material = getMaterial();
    material.type = 'LINK';
    material.createdBy = 'teacher';

    renderComponent(material, { add: true });

    fireEvent.mouseOver(screen.getByRole('ownedByUserIconContainer'));

    await waitFor(() => {
      screen.getByText('schoolabyApp.material.createdByYou');
    });
  });

  it('Should show sequence number of material when sequenced true', () => {
    const material = getMaterial();
    material.type = 'LINK';
    material.createdBy = 'teacher';
    material.sequenceNumber = 0;

    renderComponent(material, { add: true, sequenced: true });

    screen.getByText('1');
    screen.getByLabelText('schoolabyApp.material.sequenceNumber');
  });

  it('Should not show sequence number of material when sequenced false', () => {
    const material = getMaterial();
    material.type = 'LINK';
    material.createdBy = 'teacher';
    material.sequenceNumber = 0;

    renderComponent(material, { add: true });

    expect(screen.queryByText('1')).toBeNull();
    expect(screen.queryByLabelText('schoolabyApp.material.sequenceNumber')).toBeNull();
  });

  it('Should not show sequence number container', () => {
    const material = getMaterial();
    material.type = 'LINK';
    material.createdBy = 'teacher';
    material.sequenceNumber = null;

    renderComponent(material, { add: true });

    const sequenceNumberLabel = screen.queryByLabelText('schoolabyApp.material.sequenceNumber');
    expect(sequenceNumberLabel).toBeNull();
  });

  it('Should not show created by you icon when material is not created by current user', () => {
    const material = getMaterial();
    material.type = 'LINK';
    material.createdBy = 'teacher2';

    renderComponent(material, { add: false });

    expect(screen.queryByRole('ownedByUserIconContainer')).toBeNull();
  });

  it('Should not show iframe when material from Opiq', async () => {
    const material = getMaterial();
    material.type = OPIQ_MATERIAL;

    renderComponent(material, { add: false });

    await waitFor(() => expect(screen.queryByRole('iframe')).not.toBeInTheDocument());
  });

  it('Should show Opiq icon when material from Opiq', () => {
    const material = getMaterial();
    material.type = OPIQ_MATERIAL;

    renderComponent(material, { add: false });

    screen.getByAltText('opiq-icon');
  });

  it('Should show E-koolikott icon when material from E-koolikott', () => {
    const material = getMaterial();
    material.type = E_KOOLIKOTT_MATERIAL;

    renderComponent(material, { add: false });

    screen.getByAltText('ekoolikott-icon');
  });
});
