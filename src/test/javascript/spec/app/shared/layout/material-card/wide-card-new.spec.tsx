import React from 'react';

import WideCardNew, { WideCardNewProps } from 'app/shared/layout/material-card-new/material-wide-card-new/wide-card-new';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { MANUAL_MATERIAL } from 'app/shared/model/material.model';

describe('WideCardNew', () => {
  const defaultProps = {
    title: 'Test title',
    description: 'Test description',
    chosen: false,
    image: <h1>Test image</h1>,
    type: MANUAL_MATERIAL,
    deletable: false,
  };

  const renderComponent = (props: WideCardNewProps) => {
    return render(<WideCardNew {...defaultProps} {...props} />);
  };

  it('should render', async () => {
    const onClick = jest.fn();
    renderComponent({ onClick });

    const title = screen.getByText('Test title');
    screen.getByText('Test description');
    screen.getByText('Test image');

    await waitFor(() => {
      fireEvent.click(title);
    });

    expect(onClick).toHaveBeenCalled();
  });
});
