import React from 'react';
import { DropdownMenu } from 'reactstrap';
import Heading, { IHeadingProps, ITabParams } from 'app/shared/layout/heading/heading';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { AUTHORITIES } from 'app/config/constants';
import { render, screen, waitFor } from '@testing-library/react';
import { RootProvider } from 'app/shared/contexts/root-context';
import { BrowserRouter } from 'react-router-dom';
import { ThroughProvider } from 'react-through';
import { ASSIGNMENT, JOURNEY } from 'app/shared/util/entity-utils';
import { QueryClient, QueryClientProvider } from 'react-query';

jest.mock('react-router-dom', () => ({
  __esModule: true,
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: jest.fn(),
    location: {
      pathname: '/journey/2/',
      search: '',
    },
  }),
}));

describe('Heading', () => {
  let mountedWrapper;
  let store: any;
  const mockStore = configureMockStore();
  const queryClient = new QueryClient();

  const headingProps: IHeadingProps = {
    dropdown: <DropdownMenu>Test dropdown</DropdownMenu>,
    title: 'Test title',
    journey: { teachers: [{ user: { id: 1 } }] },
  };

  const wrapper = props => {
    return {
      ...render(
        <RootProvider>
          <QueryClientProvider client={queryClient}>
            <Provider store={props.store ? props.store : store}>
              <BrowserRouter>
                <ThroughProvider>
                  <Heading {...headingProps} {...props} />
                </ThroughProvider>
              </BrowserRouter>
            </Provider>
          </QueryClientProvider>
        </RootProvider>
      ),
      store,
    };
  };

  beforeEach(() => {
    mountedWrapper = undefined;
    store = mockStore({
      locale: {
        currentLocale: 'et',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
          id: 1,
        },
      },
    });
  });

  it('should render a Heading component', () => {
    const tabParams: ITabParams = {
      tabEntityId: undefined,
      entityType: JOURNEY,
    };
    const { container } = wrapper(tabParams);

    const header = container.querySelector('div[class="header"]');
    expect(header).toBeInTheDocument();
  });

  it('should render a title', () => {
    wrapper({ tabParams: undefined });
    expect(screen.getByText('Test title')).toBeInTheDocument();
  });

  it('should render a Dropdown', async () => {
    wrapper({ tabParams: undefined });

    await screen.findByLabelText('global.heading.dropdown');
  });

  it('should render tabs with APPs, grades and Journey tab', () => {
    const tabParams: ITabParams = {
      tabEntityId: undefined,
      entityType: JOURNEY,
    };
    wrapper({ tabParams });

    expect(screen.getByText('schoolabyApp.journey.detail.tabs.journey')).toBeInTheDocument();
    expect(screen.getByText('schoolabyApp.journey.detail.tabs.apps')).toBeInTheDocument();
    expect(screen.getByText('schoolabyApp.journey.detail.tabs.grades')).toBeInTheDocument();
    expect(screen.getAllByText(content => content.startsWith('schoolabyApp.journey.detail.tabs.')).length).toBe(3);
  });

  it('should not render tabs when authority is not teacher nor admin', () => {
    const state = {
      locale: {
        currentLocale: 'et',
      },
      authentication: {
        account: {
          authorities: [],
        },
      },
    };
    store = mockStore(state);

    const tabParams = {
      tabEntityId: undefined,
      entityType: JOURNEY,
    };

    wrapper({ tabParams, store });

    expect(screen.queryByText('schoolabyApp.journey.detail.tabs.journey')).not.toBeInTheDocument();
    expect(screen.queryAllByText(content => content.startsWith('schoolabyApp.journey.detail.tabs.')).length).toBe(0);
  });

  it('should render tabs without students tab', () => {
    const tabParams: ITabParams = {
      tabEntityId: undefined,
      entityType: JOURNEY,
    };
    wrapper({ tabParams });

    expect(screen.getByText('schoolabyApp.journey.detail.tabs.journey')).toBeInTheDocument();
    expect(screen.getByText('schoolabyApp.journey.detail.tabs.apps')).toBeInTheDocument();
    expect(screen.getByText('schoolabyApp.journey.detail.tabs.grades')).toBeInTheDocument();
    expect(screen.getAllByText(content => content.startsWith('schoolabyApp.journey.detail.tabs.')).length).toBe(3);
  });

  it('should render no tabs', () => {
    wrapper({ tabParams: undefined });
    expect(screen.queryAllByText(content => content.startsWith('schoolabyApp.journey.detail.tabs.')).length).toBe(0);
  });

  it('should render LTI apps not configured warning when at least one LTI app card has consumer key or shared secret as null', async () => {
    headingProps.tabParams = {
      entityType: JOURNEY,
      tabEntityId: 3,
    };
    wrapper({ ...headingProps });

    await waitFor(() => {
      const exclamationMark = screen.queryByRole('exclamation-mark-icon');
      expect(exclamationMark).not.toBeNull();
    });
  });

  it('should render tabs with students tab', () => {
    const tabParams: ITabParams = {
      tabEntityId: 3,
      entityType: JOURNEY,
    };
    wrapper({ tabParams });

    expect(screen.getByText('schoolabyApp.journey.detail.tabs.journey')).toBeInTheDocument();
    expect(screen.getByText('global.heading.tabs.students')).toBeInTheDocument();
    expect(screen.getByText('schoolabyApp.journey.detail.tabs.apps')).toBeInTheDocument();
    expect(screen.getByText('schoolabyApp.journey.detail.tabs.grades')).toBeInTheDocument();
  });

  it('should render assignment tabs', () => {
    const tabParams: ITabParams = {
      tabEntityId: 1,
      entityType: ASSIGNMENT,
    };
    wrapper({ tabParams });

    expect(screen.getByText('schoolabyApp.assignment.detail.tabs.assignment')).toBeInTheDocument();
    expect(screen.getAllByText(content => content.startsWith('schoolabyApp.assignment.detail.tabs.')).length).toBe(3);
  });
});
