import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import SupportModal from 'app/shared/layout/support-modal/support-modal';
import configureMockStore from 'redux-mock-store';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Provider } from 'react-redux';
import userEvent from '@testing-library/user-event';

describe('Support form', () => {
  const mockStore = configureMockStore();
  const queryClient = new QueryClient();
  let store: any;

  const renderComponent = () =>
    render(
      <Provider store={store}>
        <QueryClientProvider client={queryClient}>
          <SupportModal isOpen={true} toggle={() => {}} />
        </QueryClientProvider>
      </Provider>
    );

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'en',
      },
    };
    store = mockStore(initialState);
  });

  it('should display form contents', async () => {
    renderComponent();

    await screen.findByText('schoolabyApp.support.sendSupportMessage');
    await screen.findByLabelText('schoolabyApp.support.subject');
    await screen.findByLabelText('schoolabyApp.support.content');
    await screen.findByText('entity.action.cancel');
    await screen.findByText('schoolabyApp.chat.send');
  });

  it('should display validation errors', async () => {
    renderComponent();

    await screen.findByText('schoolabyApp.support.sendSupportMessage');
    fireEvent.click(await screen.findByText('schoolabyApp.chat.send'));

    expect(await screen.findAllByText('entity.validation.required')).toHaveLength(2);
  });

  it('should display success modal', async () => {
    renderComponent();

    const subjectInput = await screen.findByLabelText('schoolabyApp.support.subject');
    const contentInput = await screen.findByLabelText('schoolabyApp.support.content');
    userEvent.type(subjectInput, 'Subject');
    userEvent.type(contentInput, 'Content');
    fireEvent.click(await screen.findByText('schoolabyApp.chat.send'));

    await screen.findByText('schoolabyApp.support.emailSent');
    await screen.findByText('schoolabyApp.support.emailSentDescription');
  });
});
