import React from 'react';
import { ILtiCreateModal, LTI_MODAL_ACTION_CREATE, LtiCreateModal } from 'app/shared/layout/lti/lti-create-modal';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import configureMockStore from 'redux-mock-store';
import { render, screen, waitFor } from '@testing-library/react';
import { ILtiResource } from 'app/shared/model/lti-resource.model';
import { ILtiApp } from 'app/shared/model/lti-app.model';
import { MaterialProvider } from 'app/shared/contexts/material-context';

jest.mock('uuid', () => {
  return {
    v4: jest.fn(() => 'constant-uuid'),
  };
});

describe('LtiCreateModal', () => {
  let mountedWrapper;
  let store: any;
  const history = createBrowserHistory();
  const mockStore = configureMockStore();

  const ltiApp: ILtiApp = {
    imageUrl: 'http://0.0.0.0/img',
    id: 1,
    launchUrl: 'http://0.0.0.0/launch',
    version: 'v1p0',
    name: 'Test App',
    description: 'Test Description',
    clientId: null,
  };

  const ltiModalProps: ILtiCreateModal = {
    title: 'AAA',
    ltiResource: {
      description: 'Test description',
      resourceLinkId: '1',
      title: 'Test title',
      ltiApp,
    },
    showModal: true,
    onClose: jest.fn(),
    journeyId: 1,
  };

  const emptyLtiResource: ILtiResource = {
    ltiApp,
    title: '',
    description: '',
    resourceLinkId: '',
  };

  const wrapper = props => {
    const p = {
      ...ltiModalProps,
      ...props,
    };

    if (!mountedWrapper) {
      mountedWrapper = render(
        <Provider store={store}>
          <Router history={history}>
            <MaterialProvider>
              <EntityUpdateProvider>
                <LtiCreateModal {...p} />
              </EntityUpdateProvider>
            </MaterialProvider>
          </Router>
        </Provider>
      );
    }
    return mountedWrapper;
  };

  const renderLtiCreateModal = () =>
    render(
      <Provider store={store}>
        <Router history={history}>
          <MaterialProvider>
            <EntityUpdateProvider>
              <LtiCreateModal showModal={true} ltiResource={emptyLtiResource} onClose={jest.fn()} journeyId={1} />
            </EntityUpdateProvider>
          </MaterialProvider>
        </Router>
      </Provider>
    );

  const sendDeepLinkResponse = (includeScoreMaximum = true) => {
    window.parent.document.dispatchEvent(
      new CustomEvent('deepLinkingResponse', {
        detail: [
          {
            type: 'ltiResourceLink',
            title: 'quiz1',
            text: 'description',
            url: 'https://localhost:8080/api/launch/',
            lineItem: {
              scoreMaximum: includeScoreMaximum ? 0.0 : undefined,
              label: 'Unique label',
              resourceId: '134',
              tag: 'tag',
            },
          },
        ],
      })
    );
  };

  beforeEach(() => {
    mountedWrapper = undefined;
    const initialState = {
      locale: {
        currentLocale: 'et',
      },
    };
    store = mockStore(initialState);
  });

  it('Should fill form fields when deeplinking', async () => {
    renderLtiCreateModal();

    sendDeepLinkResponse();

    await waitFor(() => {
      const textBoxes = screen.getAllByRole('textbox');
      const titleField = textBoxes[0];
      const linkIdField = textBoxes[1];
      const maxScoreField = textBoxes[2];
      const descriptionField = textBoxes[3];

      expect(textBoxes).toHaveLength(4);
      expect(titleField).toHaveDisplayValue('quiz1');
      expect(linkIdField).toHaveDisplayValue('constant-uuid');
      expect(maxScoreField).toHaveDisplayValue('0');
      expect(descriptionField).toHaveDisplayValue('description');
    });
  });

  it('Should not fill max score form field when not received from deeplinking', async () => {
    renderLtiCreateModal();

    sendDeepLinkResponse(false);

    await waitFor(() => {
      const textBoxes = screen.getAllByRole('textbox');
      const titleField = textBoxes[0];
      const linkIdField = textBoxes[1];
      const descriptionField = textBoxes[2];

      expect(textBoxes).toHaveLength(3);
      expect(titleField).toHaveDisplayValue('quiz1');
      expect(linkIdField).toHaveDisplayValue('constant-uuid');
      expect(descriptionField).toHaveDisplayValue('description');
    });
  });

  it('Should render component with MILESTONE entityType', async () => {
    wrapper({});

    await screen.findByText(ltiModalProps.title);
  });

  it('Should show only add button when action is UPDATE', async () => {
    wrapper({
      action: LTI_MODAL_ACTION_CREATE,
    });

    await screen.findByText('global.form.chooseMaterial');
    const button = screen.queryByText('global.form.removeChosen');
    expect(button).not.toBeInTheDocument();
  });
});
