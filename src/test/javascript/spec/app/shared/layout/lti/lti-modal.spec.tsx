import React from 'react';
import { ILtiModal, LtiModal } from 'app/shared/layout/lti/lti-modal';
import ContextProvider from '../../../../helper/context-mock/context-provider';
import MockEntityDetailProvider from '../../../../helper/context-mock/mock-entity-detail-provider';
import { render, screen, waitFor } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';

describe('LtiModal', () => {
  let store: any;
  const mockStore = configureMockStore();

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'en',
      },
    };
    store = mockStore(initialState);
  });

  const ltiModalProps: ILtiModal = {
    isStudent: true,
    ltiResource: {
      id: 1,
      assignmentId: 1,
      title: 'A',
      resourceLinkId: 'a-b-c-d',
      description: 'B',
      ltiApp: {
        id: 1,
        name: 'app',
        description: 'description',
        launchUrl: 'launchurl',
        imageUrl: 'imageurl',
        version: 'LTI-1p0',
        clientId: '1',
      },
      syncGrade: false,
    },
    onClose: () => null,
  };

  function component() {
    return (
      <ContextProvider contextProvider={MockEntityDetailProvider} store={store} props={{}}>
        <LtiModal {...ltiModalProps} />
      </ContextProvider>
    );
  }

  it('Renders component with assignment resource', async () => {
    render(component());
    await waitFor(() => {
      expect(screen.getByText(ltiModalProps.ltiResource.title));
    });
  });
});
