import { render, screen, within, fireEvent } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import React from 'react';
import { ThroughProvider } from 'react-through';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { AUTHORITIES } from 'app/config/constants';
import { RootProvider } from 'app/shared/contexts/root-context';
import { IJourneyProps, Journey } from 'app/journey/journey';
import { QueryClient, QueryClientProvider } from 'react-query';
import { JourneyTemplateProvider } from 'app/shared/contexts/journey-template-context';

describe('Journey', () => {
  let store: any;
  const history = createBrowserHistory();
  const mockStore = configureMockStore();
  const queryClient = new QueryClient();

  const initializeJourneyProps = (): IJourneyProps => ({
    history: undefined,
    location: undefined,
    match: undefined,
    isTeacherOrAdmin: true,
    userId: 1,
    locale: 'en',
  });

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
    };
    store = mockStore(initialState);
  });

  const renderComponent = journeyProps =>
    render(
      <RootProvider>
        <Provider store={store}>
          <QueryClientProvider client={queryClient}>
            <ThroughProvider>
              <Router history={history}>
                <JourneyTemplateProvider>
                  <Journey {...journeyProps} />
                </JourneyTemplateProvider>
              </Router>
            </ThroughProvider>
          </QueryClientProvider>
        </Provider>
      </RootProvider>
    );

  it('Should show MyJourneysBreadcrumbItem', () => {
    renderComponent(initializeJourneyProps());
    const breadcrumbs = screen.getByLabelText('breadcrumb');
    const myJourneysBreadcrumbItem = within(breadcrumbs).getByText('global.menu.journeys');

    expect(myJourneysBreadcrumbItem).toHaveAttribute('href', '/journey');
  });

  it('should open journey options modal', async () => {
    const journeyProps = initializeJourneyProps();
    renderComponent(journeyProps);

    const newJourneyButton = await screen.findByText('entity.add.journey');
    fireEvent.click(newJourneyButton);
    await screen.findByText('schoolabyApp.journey.createNewJourney');
    await screen.findByText('schoolabyApp.journey.useJourneyTemplate');
  });
});
