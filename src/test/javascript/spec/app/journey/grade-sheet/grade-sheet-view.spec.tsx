import React from 'react';
import { render, screen } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ThroughProvider } from 'react-through';
import { RootProvider } from 'app/shared/contexts/root-context';
import { Provider } from 'react-redux';
import { AUTHORITIES } from 'app/config/constants';
import configureMockStore from 'redux-mock-store';
import GradeSheetView from 'app/journey/journey-detail/grade-sheet/grade-sheet-view';

describe('Grade Sheet View', () => {
  let store: any;
  const queryClient = new QueryClient();
  const mockStore = configureMockStore();

  const props = {
    match: {
      params: {
        id: '1',
      },
      isExact: true,
      path: 'path',
      url: 'url',
    },
    history: undefined,
    location: undefined,
  };

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'et',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
      applicationProfile: {
        inProduction: false,
      },
    };
    store = mockStore(initialState);
  });

  const renderGradeSheet = () => {
    return render(
      <RootProvider>
        <Router history={createBrowserHistory()}>
          <Provider store={store}>
            <QueryClientProvider client={queryClient}>
              <ThroughProvider>
                <GradeSheetView {...props} />
              </ThroughProvider>
            </QueryClientProvider>
          </Provider>
        </Router>
      </RootProvider>
    );
  };

  it('should render rows with student names', async () => {
    const { container } = renderGradeSheet();

    screen.getAllByText('schoolabyApp.journey.detail.tabs.grades');
    const table = container.querySelector(`table[class="grade-sheet-table table table-striped"]`);
    expect(table).toBeInTheDocument();
    await screen.findByText('Student1 Student1');
    await screen.findByText('Student2 Student2');
    const avatars = await screen.findAllByText('SS');
    expect(avatars.length).toBe(2);

    await screen.findByText('81.57');
  });
});
