import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import StudentGradeRows from 'app/journey/journey-detail/grade-sheet/student-grade-row/student-grade-rows';
import { NUMERICAL_1_5 } from 'app/shared/model/grading-scheme.model';
import { FAILED_GRADE_STYLE } from 'app/shared/util/grade-util';
import { YELLOW } from 'app/shared/util/color-utils';

describe('StudentGradeRows', () => {
  const rowProps = {
    studentSubmissions: [{}],
    maxRowCount: 3,
    student: {
      id: 1,
    },
    assignments: [{ id: 1, title: 'Title', gradingSchemeId: 1 }],
    gradingSchemes: [
      {
        id: 1,
        code: 'NUMERICAL_1_5',
        values: [
          {
            id: 5,
            grade: '1',
            percentageRangeStart: 0,
            percentageRangeEnd: 20,
            gradingSchemeId: 1,
          },
          {
            id: 4,
            grade: '2',
            percentageRangeStart: 20,
            percentageRangeEnd: 50,
            gradingSchemeId: 1,
          },
          {
            id: 3,
            grade: '3',
            percentageRangeStart: 50,
            percentageRangeEnd: 75,
            gradingSchemeId: 1,
          },
          {
            id: 2,
            grade: '4',
            percentageRangeStart: 75,
            percentageRangeEnd: 90,
            gradingSchemeId: 1,
          },
          {
            id: 1,
            grade: '5',
            percentageRangeStart: 90,
            percentageRangeEnd: 100,
            gradingSchemeId: 1,
          },
        ],
      },
    ],
    journeyId: 1,
  };

  const renderRows = props => {
    return render(
      <Router history={createBrowserHistory()}>
        <StudentGradeRows {...props} />
      </Router>
    );
  };

  it('should render multiple trophies with multiplier number', () => {
    rowProps.studentSubmissions = [
      { highestGrade: true, assignmentId: 1 },
      { highestGrade: true, assignmentId: 2 },
      { highestGrade: true, assignmentId: 2 },
      { highestGrade: true, assignmentId: 2 },
    ];
    rowProps.assignments = [
      { id: 1, title: 'Title', gradingSchemeId: 1 },
      { id: 2, title: 'Title', gradingSchemeId: 1 },
      { id: 3, title: 'Title', gradingSchemeId: 1 },
      { id: 4, title: 'Title', gradingSchemeId: 1 },
    ];
    renderRows(rowProps);

    screen.getByText('1x');
    const trophies = screen.getAllByLabelText('trophy');
    expect(trophies.length).toBe(3);
    const color = trophies[0].children.item(0).attributes.getNamedItem('fill').value;
    expect(trophies[0]).toBeVisible();
    expect(color).toEqual(YELLOW);
  });

  it('should render grade inside red container to indicate a failed grade', () => {
    rowProps.studentSubmissions = [{ highestGrade: true, assignmentId: 1, submissionFeedbacks: [{ studentId: 1, grade: '2' }] }];
    const { container } = renderRows(rowProps);

    const trophies = screen.getAllByLabelText('trophy');
    expect(trophies.length).toBe(1);
    screen.getByText('2');
    const failedGrade = container.querySelector(`div[class="grade-row ${FAILED_GRADE_STYLE}"]`);
    expect(failedGrade).toBeInTheDocument();
  });

  it('should render tooltip when grade is clicked', () => {
    rowProps.studentSubmissions = [{ highestGrade: true, assignmentId: 1, submissionFeedbacks: [{ studentId: 1, grade: '2' }] }];
    const { container } = renderRows(rowProps);

    const grade = screen.getByText('2');
    fireEvent.click(grade);

    waitFor(() => {
      screen.getByText('Title');
      screen.getByText('schoolabyApp.gradeSheet.graded');
      const linkToAssignment = container.querySelector(
        `a[href="/assignment/${rowProps.assignments[0].id}?journeyId=${rowProps.journeyId}"]`
      );
      expect(linkToAssignment).toBeInTheDocument();
    });
  });
});
