import React from 'react';
import { AppListModal, IAppListModal } from 'app/journey/journey-detail/app-list-modal';
import { ILtiApp } from 'app/shared/model/lti-app.model';
import { render, screen } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';

describe('AppListModal', () => {
  let mountedWrapper;
  let store;
  const mockStore = configureMockStore();

  const ltiApp: ILtiApp = {
    id: 1,
    name: 'Test app',
    description: 'Test description',
    launchUrl: 'http://test.url',
    imageUrl: 'http://test.url',
    version: 'version',
    clientId: null,
  };

  const appListModalProps: IAppListModal = {
    toggle() {},
    showModal: true,
    apps: [ltiApp],
    onCardClick() {},
  };

  const wrapper = () => {
    if (!mountedWrapper) {
      mountedWrapper = render(
        <Provider store={store}>
          <AppListModal {...appListModalProps} />
        </Provider>
      );
    }
    return mountedWrapper;
  };

  beforeEach(() => {
    const initialState = {
      applicationProfile: {
        inProduction: false,
      },
    };
    store = mockStore(initialState);
    mountedWrapper = undefined;
  });

  it('Renders component', async () => {
    wrapper();

    await screen.findByText('schoolabyApp.journey.app.modal.title');
    await screen.findByText('Test description');
  });
});
