import { render, screen, waitFor, within, fireEvent } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import React from 'react';
import { ThroughProvider } from 'react-through';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { AUTHORITIES } from 'app/config/constants';
import { RootProvider } from 'app/shared/contexts/root-context';
import { IJourneyDetailProps, JourneyDetailOld } from 'app/journey/journey-detail/journey-detail-old';
import { QueryClient, QueryClientProvider } from 'react-query';
import { EntityDetailProvider } from 'app/shared/contexts/entity-detail-context';

const mockStore = configureMockStore();

describe('Journey Detail Old', () => {
  const journeyDetailProps = (): IJourneyDetailProps => {
    return {
      history: undefined,
      location: undefined,
      match: {
        params: {
          id: '1',
        },
        isExact: true,
        path: 'path',
        url: 'url',
      },
      isTeacherOrAdmin: true,
      account: {},
      currentLocale: undefined,
    };
  };

  let store: any;
  const history = createBrowserHistory();

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
    };
    store = mockStore(initialState);
  });

  const queryClient = new QueryClient();

  const renderComponent = (detailProps: IJourneyDetailProps) => {
    render(
      <RootProvider>
        <Provider store={store}>
          <ThroughProvider>
            <QueryClientProvider client={queryClient}>
              <Router history={history}>
                <EntityDetailProvider>
                  <JourneyDetailOld {...detailProps} />
                </EntityDetailProvider>
              </Router>
            </QueryClientProvider>
          </ThroughProvider>
        </Provider>
      </RootProvider>
    );
  };

  it('should show MyJourneysBreadcrumbItem and JourneyTitleBreadcrumbItem', async () => {
    renderComponent(journeyDetailProps());

    await waitFor(() => {
      const breadcrumbs = screen.getByLabelText('breadcrumb');
      const myJourneysBreadcrumbItem = within(breadcrumbs).getByText('global.menu.journeys');
      const journeyTitleBreadcrumbItem = within(breadcrumbs).getByText('Journey title');

      expect(myJourneysBreadcrumbItem).toHaveAttribute('href', '/journey');
      expect(journeyTitleBreadcrumbItem).toHaveAttribute('href', '/journey/1');
    });
  });

  it('should not display breadcrumb when template preview is enabled', async () => {
    const detailProps = journeyDetailProps();
    detailProps.previewJourneyId = 1;
    detailProps.previewEnabled = true;
    renderComponent(detailProps);

    await waitFor(() => {
      expect(screen.queryByLabelText('breadcrumb')).not.toBeInTheDocument();
      expect(screen.queryByText('global.menu.journeys')).not.toBeInTheDocument();
    });
    expect(await screen.findAllByText('Journey title')).toHaveLength(1);
  });

  it('should display suitable tabs for template previewers', async () => {
    const detailProps = journeyDetailProps();
    detailProps.previewJourneyId = 1;
    detailProps.previewEnabled = true;
    renderComponent(detailProps);

    await waitFor(() => {
      expect(screen.queryByText('global.heading.tabs.students')).not.toBeInTheDocument();
      expect(screen.queryByText('schoolabyApp.journey.detail.tabs.grades')).not.toBeInTheDocument();
    });
    await screen.findByText('schoolabyApp.journey.detail.tabs.journey');
    await screen.findByText('schoolabyApp.journey.detail.tabs.apps');
  });

  it('should open apps tab in the same view when template preview is enabled', async () => {
    const detailProps = journeyDetailProps();
    detailProps.previewJourneyId = 1;
    detailProps.previewEnabled = true;
    detailProps.match = null;
    renderComponent(detailProps);

    fireEvent.click(await screen.findByText('schoolabyApp.journey.detail.tabs.apps'));
    await screen.findByText('schoolabyApp.journey.app.myapps');
  });

  it('should not be able to modify journey and join live when template preview enabled', async () => {
    const detailProps = journeyDetailProps();
    detailProps.account = {
      authorities: ['ROLE_TEACHER'],
      email: 'student@localhost.com',
      firstName: 'teacher',
      id: 7,
      lastName: 'teacher',
    };
    detailProps.previewJourneyId = 1;
    detailProps.previewEnabled = true;
    detailProps.match = null;

    renderComponent(detailProps);

    await waitFor(() => {
      expect(screen.queryByLabelText('global.heading.dropdown')).not.toBeInTheDocument();
      expect(screen.queryByText('schoolabyApp.journey.detail.inviteStudents')).not.toBeInTheDocument();
      expect(screen.queryByText('schoolabyApp.journey.detail.viewLive')).not.toBeInTheDocument();
      expect(screen.queryByText('entity.add.milestone')).not.toBeInTheDocument();
      expect(screen.queryByText('entity.add.task')).not.toBeInTheDocument();
    });
  });
});
