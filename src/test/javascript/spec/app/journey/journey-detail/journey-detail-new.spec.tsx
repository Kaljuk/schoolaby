import { render, screen, waitFor, within, fireEvent } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import React from 'react';
import { ThroughProvider } from 'react-through';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { AUTHORITIES } from 'app/config/constants';
import { RootProvider } from 'app/shared/contexts/root-context';
import { QueryClient, QueryClientProvider } from 'react-query';
import JourneyDetailNew from 'app/journey/journey-detail/journey-detail-new';
import { JOIN_LIVE_HREF } from '../../../rtl-setup';
import { EntityDetailProvider } from 'app/shared/contexts/entity-detail-context';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { MaterialProvider } from 'app/shared/contexts/material-context';
import { AppContext } from 'app/app';

const mockStore = configureMockStore();

describe('Journey Detail New', () => {
  let teacherStore: any;
  let studentStore: any;

  const defaultJourneyProps = {
    history: undefined,
    location: undefined,
    match: {
      params: {
        id: '1',
      },
      isExact: true,
      path: 'path',
      url: 'url',
    },
  };

  const history = createBrowserHistory();

  const teacherState = {
    locale: {
      currentLocale: 'en',
    },
    authentication: {
      account: {
        id: 7,
        authorities: [AUTHORITIES.TEACHER],
      },
    },
    applicationProfile: {
      inProduction: true,
    },
  };

  const studentState = {
    ...teacherState,
    authentication: {
      account: {
        authorities: [AUTHORITIES.STUDENT],
      },
    },
  };

  beforeEach(() => {
    teacherStore = mockStore(teacherState);
    studentStore = mockStore(studentState);
  });

  const queryClient = new QueryClient();

  const component = (isTeacher = true, journeyProps = defaultJourneyProps) =>
    render(
      <AppContext.Provider value={{ setModal() {} }}>
        <RootProvider>
          <Provider store={isTeacher ? teacherStore : studentStore}>
            <ThroughProvider>
              <QueryClientProvider client={queryClient}>
                <EntityDetailProvider>
                  <EntityUpdateProvider>
                    <MaterialProvider>
                      <Router history={history}>
                        <JourneyDetailNew {...journeyProps} />
                      </Router>
                    </MaterialProvider>
                  </EntityUpdateProvider>
                </EntityDetailProvider>
              </QueryClientProvider>
            </ThroughProvider>
          </Provider>
        </RootProvider>
      </AppContext.Provider>
    );

  it('should show HomeBreacrumbItem, MyJourneysBreadcrumbItem and JourneyTitleBreadcrumbItem', async () => {
    component();

    await waitFor(() => {
      const breadcrumbs = screen.getByLabelText('breadcrumb');
      within(breadcrumbs).getByLabelText('global.home');
      const myJourneysBreadcrumbItem = within(breadcrumbs).getByText('global.menu.journeys');
      const journeyTitleBreadcrumbItem = within(breadcrumbs).getByText('Journey title');

      expect(myJourneysBreadcrumbItem).toHaveAttribute('href', '/journey');
      expect(journeyTitleBreadcrumbItem).toHaveAttribute('href', '/journey/1');
    });
  });

  it('should show journey title and icon', async () => {
    component();

    await screen.findByLabelText('lightbulb');
    expect(await screen.findAllByText('Journey title')).toHaveLength(2);
  });

  it('should show invite students button if user is teacher in journey', async () => {
    component();

    await screen.findByRole('button', { name: 'schoolabyApp.journey.detail.invite' });
  });

  it('should not show invite students button if user is student in journey', async () => {
    component(false);

    // Otherwise next assertions may give false positive result (may pass even if elements are present)
    expect(await screen.findAllByText('Journey title')).toHaveLength(2);

    const inviteButton = screen.queryByRole('button', { name: 'schoolabyApp.journey.detail.invite' });
    expect(inviteButton).not.toBeInTheDocument();
  });

  it('should show join live button if journey has videoConferenceUrl', async () => {
    component();

    const joinLiveButton = await screen.findByRole('button', { name: /schoolabyApp.journey.detail.viewLive/ });
    const liveLink = within(joinLiveButton).getByRole('link');
    expect(liveLink).toHaveAttribute('href', 'http://' + JOIN_LIVE_HREF);
  });

  it('should not show join live button if journey does not have videoConferenceUrl', async () => {
    const journeyProps = {
      ...defaultJourneyProps,
      match: {
        ...defaultJourneyProps.match,
        params: {
          id: '2',
        },
      },
    };
    component(true, journeyProps);

    // Otherwise next assertions may give false positive result (may pass even if elements are present)
    expect(await screen.findAllByText('Journey title')).toHaveLength(2);

    const joinLiveButton = screen.queryByRole('button', { name: /schoolabyApp.journey.detail.viewLive/ });
    expect(joinLiveButton).not.toBeInTheDocument();
  });

  it('should show journey edit button if user is teacher in the journey', async () => {
    component();

    await screen.findByRole('button', { name: 'schoolabyApp.journey.detail.invite' });
    expect(await screen.findAllByRole('button', { name: /entity.action.edit/ })).toHaveLength(3);
  });

  it('should open dropdown menu on edit button click', async () => {
    history.push = jest.fn();
    component();

    await screen.findByRole('button', { name: 'schoolabyApp.journey.detail.invite' });
    const editButton = screen.getAllByRole('button', { name: /entity.action.edit/ })[0];

    await waitFor(() => {
      const menu = screen.queryByRole('menu');
      expect(menu).not.toBeInTheDocument();
    });

    fireEvent.click(editButton);

    const menu = screen.getByRole('menu');
    const editMenuItem = within(menu).getByRole('menuitem', { name: 'entity.action.edit' });
    within(menu).getByRole('menuitem', { name: /entity.action.delete/ });
    within(menu).getByRole('menuitem', { name: /schoolabyApp.journey.templates.publish/ });

    fireEvent.click(editMenuItem);
    expect(history.push).toBeCalledWith('/journey/1/edit?');
  });

  it('should not show journey edit button if user is student in the journey', async () => {
    component(false);

    // Otherwise next assertions may give false positive result (may pass even if elements are present)
    expect(await screen.findAllByText('Journey title')).toHaveLength(2);

    const editButton = screen.queryByRole('button', { name: 'entity.action.edit' });
    expect(editButton).not.toBeInTheDocument();
  });

  it('should open create new milestone form', async () => {
    component();

    fireEvent.click(await screen.findByText(/entity.add.milestone/));
    await waitFor(
      () => {
        expect(screen.getAllByText(/entity.add.milestone/)).toHaveLength(2);
        const milestoneTitleInput = screen.getByRole('textbox', { name: 'schoolabyApp.milestone.title' });
        expect(milestoneTitleInput).toHaveValue('');
      },
      { timeout: 15000 }
    );
  });

  it('should open milestone edit form', async () => {
    component();

    const editButtons = await screen.findAllByRole('button', { name: 'entity.action.edit' });
    expect(editButtons).toHaveLength(2);
    const editButton = editButtons[0];

    fireEvent.click(editButton);

    const menu = screen.getByRole('menu');
    const editMenuItem = within(menu).getByRole('menuitem', { name: 'entity.action.edit' });

    fireEvent.click(editMenuItem);
    await screen.findByText('entity.edit.milestone');
    expect(await screen.findByLabelText('schoolabyApp.milestone.title')).toHaveValue('Milestone title');
  });

  it('should open create new assignment form', async () => {
    component();

    fireEvent.click(await screen.findByText(/entity.add.task/));
    await waitFor(
      () => {
        expect(screen.getAllByText(/entity.add.task/)).toHaveLength(2);
        expect(screen.getByLabelText('schoolabyApp.assignment.title')).toHaveValue('');
      },
      { timeout: 15000 }
    );
  });

  it('should open assignment edit form', async () => {
    component();

    const editButtons = await screen.findAllByRole('button', { name: 'entity.action.edit' });
    expect(editButtons).toHaveLength(2);
    const editButton = editButtons[1];

    fireEvent.click(editButton);

    await waitFor(() => {
      const menu = screen.getByRole('menu');
      const editMenuItem = within(menu).getByRole('menuitem', { name: 'entity.action.edit' });
      fireEvent.click(editMenuItem);
    });

    await screen.findByText('entity.edit.task');
    expect(await screen.findByLabelText('schoolabyApp.assignment.title')).toHaveValue('Assignment title');
  });
});
