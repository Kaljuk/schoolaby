import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import InviteModal, { InviteModalProps } from 'app/journey/journey-detail/invite-modal/invite-modal';

const dataShare = jest.fn();

jest.mock('react-device-detect', () => ({
  isMobile: true,
}));

// @ts-ignore
window.navigator.share = dataShare;

describe('Invite modal on mobile', () => {
  const defaultProps: InviteModalProps = {
    journey: {
      teacherSignupCode: 'teacherSignupCode',
      studentSignupCode: 'studentSignupCode',
    },
    modal: true,
    toggle() {},
    type: 'STUDENT',
  };

  const renderComponent = (props?) => render(<InviteModal {...props} />);

  it('should display invite modal contents', async () => {
    // @ts-ignore
    window.navigator.canShare = () => true;
    renderComponent({
      ...defaultProps,
      type: 'STUDENT',
    });

    await screen.findByLabelText('schoolabyApp.journey.signUpCode');
    await screen.findByDisplayValue('studentSignupCode');
    await screen.findByText('global.or');
    await screen.findByLabelText('schoolabyApp.journey.sharableLink');
    await screen.findByDisplayValue(`http://localhost/journey/join?signUpCode=studentSignupCode`);
    expect(await screen.findAllByText('entity.action.share')).toHaveLength(2);
  });

  it('should show copy buttons when device is a mobile and data is not shareable', async () => {
    // @ts-ignore
    window.navigator.canShare = () => false;

    renderComponent({
      ...defaultProps,
      type: 'STUDENT',
    });

    expect(await screen.findAllByText('entity.action.copy')).toHaveLength(2);
  });

  it('should share correct data', async () => {
    // @ts-ignore
    window.navigator.canShare = () => true;

    // @ts-ignore
    window.getSelection = () => {
      return {
        empty: () => jest.fn(),
      };
    };

    renderComponent({
      ...defaultProps,
      type: 'STUDENT',
    });

    const shareButtons = await screen.findAllByText('entity.action.share');
    fireEvent.click(shareButtons[0]);
    expect(dataShare).toBeCalledWith({ text: 'studentSignupCode' });
    fireEvent.click(shareButtons[1]);
    expect(dataShare).toBeCalledWith({ url: `http://localhost/journey/join?signUpCode=studentSignupCode` });
  });
});
