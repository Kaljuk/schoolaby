import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import JoinJourney from 'app/journey/journey-detail/join-journey';
import { MemoryRouter } from 'react-router-dom';
import { QueryClientProvider, QueryClient } from 'react-query';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';

const mockHistoryPush = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe('JoinJourney', () => {
  const queryClient = new QueryClient();
  const mockStore = configureMockStore();

  const store = mockStore({
    applicationProfile: {
      inProduction: false,
    },
    locale: {
      currentLocale: 'en',
    },
  });

  const renderComponent = () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <QueryClientProvider client={queryClient}>
            <JoinJourney />
          </QueryClientProvider>
        </MemoryRouter>
      </Provider>
    );
  };

  it('Should redirect to journey when cancel button is clicked', () => {
    renderComponent();

    fireEvent.click(screen.getByRole('button', { name: /cancel/i }));
    expect(mockHistoryPush).toHaveBeenCalledWith('/journey');
  });
});
