import React from 'react';
import { render, screen } from '@testing-library/react';
import ContextProvider from '../../../../helper/context-mock/context-provider';
import MockEntityDetailProvider from '../../../../helper/context-mock/mock-entity-detail-provider';
import { RootProvider } from 'app/shared/contexts/root-context';
import configureMockStore from 'redux-mock-store';
import DeadlineModalNew from 'app/journey/journey-detail/journey-board/assignment/deadline-modal-new';

describe('DeadlineModalNew', () => {
  const mockStore = configureMockStore();

  const component = (props = {}) =>
    render(
      <RootProvider>
        <ContextProvider contextProvider={MockEntityDetailProvider} props={props} store={mockStore({ locale: { currentLocale: 'en' } })}>
          <DeadlineModalNew />
        </ContextProvider>
      </RootProvider>
    );

  it('should render when deadlineTarget set', () => {
    component({
      deadlineTarget: {
        deadline: '2021-05-09T13:17:49.929545Z',
      },
    });

    screen.getByText('schoolabyApp.assignment.deadlineModal.choose');
    screen.getByText('entity.action.cancel');
    screen.getByText('entity.action.save');
  });

  it('should not render when deadlineTarget not set', () => {
    component();

    expect(screen.queryByText('schoolabyApp.assignment.deadlineModal.choose')).toBeNull();
    expect(screen.queryByText('entity.action.cancel')).toBeNull();
    expect(screen.queryByText('entity.action.save')).toBeNull();
  });
});
