import React from 'react';
import { render, screen } from '@testing-library/react';
import { DeadlineModal } from 'app/journey/journey-detail/journey-board/assignment/deadline-modal';
import ContextProvider from '../../../../helper/context-mock/context-provider';
import MockEntityDetailProvider from '../../../../helper/context-mock/mock-entity-detail-provider';
import { RootProvider } from 'app/shared/contexts/root-context';
import configureMockStore from 'redux-mock-store';

describe('DeadlineModal', () => {
  const mockStore = configureMockStore()();

  it('should render when deadlineTarget set', () => {
    render(
      <RootProvider>
        <ContextProvider
          contextProvider={MockEntityDetailProvider}
          props={{
            deadlineTarget: {
              deadline: '2021-05-09T13:17:49.929545Z',
            },
          }}
          store={mockStore}
        >
          <DeadlineModal />
        </ContextProvider>
      </RootProvider>
    );

    screen.getByText('schoolabyApp.assignment.deadlineModal.choose');
    screen.getByText('entity.action.cancel');
    screen.getByText('entity.action.save');
  });

  it('should not render when deadlineTarget not set', () => {
    render(
      <RootProvider>
        <ContextProvider contextProvider={MockEntityDetailProvider} store={mockStore}>
          <DeadlineModal />
        </ContextProvider>
      </RootProvider>
    );

    expect(screen.queryByText('schoolabyApp.assignment.deadlineModal.choose')).toBeNull();
    expect(screen.queryByText('entity.action.cancel')).toBeNull();
    expect(screen.queryByText('entity.action.save')).toBeNull();
  });
});
