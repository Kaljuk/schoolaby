import React from 'react';
import { createBrowserHistory } from 'history';
import { fireEvent, render, screen, waitFor, within } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import { AUTHORITIES } from 'app/config/constants';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { IAssignment } from 'app/shared/model/assignment.model';
import { IJourney } from 'app/shared/model/journey.model';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import { Router } from 'react-router-dom';
import AssignmentContainerNew from 'app/journey/journey-detail/journey-board/assignment-new/assignment-container-new';
import MockEntityDetailProvider from '../../../../helper/context-mock/mock-entity-detail-provider';
import ContextProvider from '../../../../helper/context-mock/context-provider';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { QueryClientProvider, QueryClient } from 'react-query';

global['document'].createRange = () => ({
  setStart() {},
  setEnd() {},
  // @ts-ignore
  commonAncestorContainer: {
    nodeName: 'BODY',
    ownerDocument: document,
  },
});

describe('Assignment Container', () => {
  let store: any;
  const mockStore = configureMockStore();
  const history = createBrowserHistory();
  const queryClient = new QueryClient();

  const defaultAssignment = {
    id: 1,
    title: 'Title',
    flexibleDeadline: true,
    highestGrade: true,
    state: EntityState.COMPLETED,
    students: [
      {
        id: 1,
        firstName: 'firstName1',
        lastName: 'lastName1',
      },
      {
        id: 2,
        firstName: 'firstName2',
        lastName: 'lastName2',
      },
    ],
  };

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'et',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
    };
    store = mockStore(initialState);
  });

  const renderComponent = (isAllowedToModify = true, assignment: IAssignment = defaultAssignment, journey: IJourney = { id: 1 }) => {
    return {
      ...render(
        <QueryClientProvider client={queryClient}>
          <ContextProvider contextProvider={MockEntityDetailProvider} store={store}>
            <AssignmentContext.Provider value={{ assignment, isAllowedToModify, journey }}>
              <Router history={history}>
                <EntityUpdateProvider>
                  <AssignmentContainerNew />
                </EntityUpdateProvider>
              </Router>
            </AssignmentContext.Provider>
          </ContextProvider>
        </QueryClientProvider>
      ),
      store,
    };
  };

  it('should render container with icons and without dropdown', async () => {
    renderComponent(false);

    await waitFor(() => {
      screen.getAllByText('Title');
      screen.getByText('schoolabyApp.assignment.done');
    });
  });

  it('should render personalized assignment icon with tooltip if assignment is personalized', async () => {
    renderComponent();

    const icon = screen.getByTestId('personalized-icon');
    fireEvent.mouseOver(icon);
    await screen.findByText('FirstName1 LastName1');
    await screen.findByText('FirstName2 LastName2');
  });

  it('should render group assignment icon with tooltip if assignment is group assignment', async () => {
    const assignment: IAssignment = {
      ...defaultAssignment,
      students: null,
      groups: [
        {
          id: 1,
          name: 'group 1',
          students: [],
          hasSubmission: false,
        },
        {
          id: 2,
          name: 'group 2',
          students: [],
          hasSubmission: false,
        },
      ],
    };

    renderComponent(true, assignment);

    const icon = screen.getByTestId('personalized-icon');
    fireEvent.mouseOver(icon);
    await screen.findByText('Group 1');
    await screen.findByText('Group 2');
  });

  it('should render submission count out of all students in a journey', async () => {
    const submissionsCountString = '1/2';
    const assignment = { ...defaultAssignment, submissionsCount: 1 };

    renderComponent(true, assignment, { id: 1, studentCount: 2 });
    const submissionsCountElement = await screen.findByTestId('submissions-count');
    expect(submissionsCountElement).toHaveTextContent(submissionsCountString);
  });

  it('should show edit button if user is allowed to modify', () => {
    renderComponent();
    screen.getByRole('button', { name: 'entity.action.edit' });
  });

  it('should not show edit button if user is not allowed to modify', () => {
    renderComponent(false);
    const editButton = screen.queryByRole('button', { name: 'entity.action.edit' });
    expect(editButton).not.toBeInTheDocument();
  });

  it('should open dropdown menu on edit button click', async () => {
    history.push = jest.fn();

    renderComponent();

    const editButton = screen.getByRole('button', { name: 'entity.action.edit' });

    await waitFor(() => {
      const menu = screen.queryByRole('menu');
      expect(menu).not.toBeInTheDocument();
    });

    fireEvent.click(editButton);

    const menu = screen.getByRole('menu');
    within(menu).getByRole('menuitem', { name: 'entity.action.edit' });
    within(menu).getByRole('menuitem', { name: /entity.action.delete/ });
  });
});
