import React from 'react';
import { createBrowserHistory } from 'history';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { AUTHORITIES } from 'app/config/constants';
import { AssignmentContainer } from 'app/journey/journey-detail/journey-board/assignment/assignment-container';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-old';
import { IAssignment } from 'app/shared/model/assignment.model';
import { IJourney } from 'app/shared/model/journey.model';
import { EntityState } from 'app/shared/model/enumerations/entity-state';
import { Router } from 'react-router-dom';
import { DANGER, PRIMARY, SECONDARY } from 'app/shared/util/color-utils';
import MockEntityDetailProvider from '../../../../helper/context-mock/mock-entity-detail-provider';
import ContextProvider from '../../../../helper/context-mock/context-provider';
import { QueryClientProvider, QueryClient } from 'react-query';

global['document'].createRange = () => ({
  setStart() {},
  setEnd() {},
  // @ts-ignore
  commonAncestorContainer: {
    nodeName: 'BODY',
    ownerDocument: document,
  },
});

describe('Rendering journey boards assignment', () => {
  let store: any;
  const mockStore = configureMockStore();

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'et',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
    };
    store = mockStore(initialState);
  });

  const renderComponent = (assignment: IAssignment, journey: IJourney = {}, isAllowedToModify = true) => {
    return {
      ...render(
        <QueryClientProvider client={new QueryClient()}>
          <Router history={createBrowserHistory()}>
            <Provider store={store}>
              <ContextProvider contextProvider={MockEntityDetailProvider} store={store}>
                <AssignmentContext.Provider value={{ assignment, isAllowedToModify, journey }}>
                  <AssignmentContainer />
                </AssignmentContext.Provider>
              </ContextProvider>
            </Provider>
          </Router>
        </QueryClientProvider>
      ),
      store,
    };
  };

  it('should render container with icons and without dropdown', async () => {
    const assignment: IAssignment = {
      id: 1,
      title: 'Title',
      flexibleDeadline: true,
      highestGrade: true,
      state: EntityState.COMPLETED,
    };

    const { container } = renderComponent(assignment, { id: 1 }, false);

    await waitFor(() => {
      screen.getAllByText('Title');
      screen.getByText('schoolabyApp.assignment.completed');
      const checkmark = container.querySelector('svg[aria-label="checkmark"]');
      expect(checkmark).toBeInTheDocument();
    });
  });

  it('should render container with dropdown and without icons', async () => {
    const assignment: IAssignment = {
      id: 1,
      title: 'Title',
      flexibleDeadline: true,
      state: EntityState.COMPLETED,
    };

    renderComponent(assignment, { id: 1 });

    await waitFor(() => {
      screen.getAllByText('Title');
      screen.getByText('schoolabyApp.assignment.completed');
      screen.getByText('entity.action.edit');
      screen.getByText('entity.action.delete');
    });
  });

  it('Renders grey checkmark', () => {
    const assignment = { state: EntityState.IN_PROGRESS };
    renderComponent(assignment);

    const byLabelText = screen.getByLabelText('checkmark');
    const color = byLabelText.children.item(0).attributes.getNamedItem('fill').value;
    expect(byLabelText).toBeVisible();
    expect(color).toEqual(SECONDARY);
  });

  it('Renders green checkmark', () => {
    const assignment = { state: EntityState.COMPLETED };
    renderComponent(assignment);

    const byLabelText = screen.getByLabelText('checkmark');
    const color = byLabelText.children.item(0).attributes.getNamedItem('fill').value;
    expect(byLabelText).toBeVisible();
    expect(color).toEqual(PRIMARY);
  });

  it('Renders red cross aka X', () => {
    const assignment = { state: EntityState.FAILED };
    renderComponent(assignment);

    const byLabelText = screen.getByLabelText('cross');
    const color = byLabelText.children.item(0).attributes.getNamedItem('fill').value;
    expect(byLabelText).toBeVisible();
    expect(color).toEqual(DANGER);
  });

  it('Renders nothing when not started', () => {
    const assignment = { state: EntityState.NOT_STARTED };
    renderComponent(assignment);

    const cross = screen.queryByLabelText('cross');
    const checkmark = screen.queryByLabelText('checkmark');

    expect(cross).toBeNull();
    expect(checkmark).toBeNull();
  });

  it('Should render personalized assignment icon if assignment is personalized', async () => {
    const assignment: IAssignment = {
      id: 1,
      students: [
        {
          id: 1,
          firstName: 'firstName1',
          lastName: 'lastName1',
        },
        {
          id: 2,
          firstName: 'firstName2',
          lastName: 'lastName2',
        },
      ],
    };

    renderComponent(assignment);

    const icon = screen.getByTestId('personalized-icon');
    fireEvent.mouseOver(icon);
    await waitFor(() => {
      screen.getByText('FirstName1 LastName1');
      screen.getByText('FirstName2 LastName2');
    });
  });

  it('Should render submission count out of all students in a journey', async () => {
    const tooltipString = '1 / 2';
    const assignment = { state: EntityState.NOT_STARTED, submissionsCount: 1 };

    renderComponent(assignment, { studentCount: 2 });
    expect(screen.queryByText(tooltipString)).toBeNull();
    fireEvent.mouseOver(screen.getByTestId('custom-element'));
    await waitFor(() => {
      screen.getByText(tooltipString);
    });
  });
});
