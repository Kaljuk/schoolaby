import React from 'react';
import { render, screen } from '@testing-library/react';
import { createBrowserHistory } from 'history';
import { QueryClient, QueryClientProvider, useQuery } from 'react-query';
import { Router } from 'react-router-dom';
import QuestionsCard from 'app/dashboard/journey-list-card/inner-card/questions-card';
import * as NotificationsApiMock from 'app/shared/services/notification-api';
import { INotification } from 'app/shared/model/notification.model';
import { NOTIFICATIONS_KEY } from 'app/config/reactQueryKeyConstants';
import { getNotifications } from 'app/shared/services/notification-api';

describe('Questions card', () => {
  const queryClient = new QueryClient();
  const history = createBrowserHistory();

  const component = () =>
    render(
      <QueryClientProvider client={queryClient}>
        <Router history={history}>
          <QuestionsCard journeyId={1} />
        </Router>
      </QueryClientProvider>
    );

  it('should show questions', async () => {
    jest.spyOn(NotificationsApiMock, 'useGetNotifications').mockImplementation(params =>
      useQuery<INotification[]>([NOTIFICATIONS_KEY, params.type], async () => getNotifications(params), {
        enabled: true,
      })
    );
    component();

    await screen.findByText('test1');
    await screen.findByText('01/02/2021');
    await screen.findByText('Test Kasutaja');
    const recentQuestionsCount = await screen.findByLabelText('schoolabyApp.dashboard.recentQuestionsCount');
    expect(recentQuestionsCount).toHaveTextContent('1');
  });
});
