import React from 'react';
import { render, screen } from '@testing-library/react';
import { createBrowserHistory } from 'history';
import JourneyListCard from 'app/dashboard/journey-list-card/journey-list-card';
import { QueryClient, QueryClientProvider, useQuery } from 'react-query';
import { Router } from 'react-router-dom';
import { JOURNEY_ASSIGNMENTS_KEY, NOTIFICATIONS_KEY } from 'app/config/reactQueryKeyConstants';
import * as NotificationsApiMock from 'app/shared/services/notification-api';
import * as AssignmentApiMock from 'app/shared/services/assignment-api';
import { getNotifications } from 'app/shared/services/notification-api';
import { INotification } from 'app/shared/model/notification.model';
import { IAssignment } from 'app/shared/model/assignment.model';

describe('Journey list card', () => {
  jest
    .spyOn(AssignmentApiMock, 'useGetAssignments')
    .mockImplementation(params =>
      useQuery<IAssignment[]>([JOURNEY_ASSIGNMENTS_KEY, params.journeyId, params.states], () => [], { enabled: true })
    );

  const queryClient = new QueryClient();
  const history = createBrowserHistory();

  const defaultJourney = {
    id: 1,
    title: 'Journey title',
    teacherName: 'teacher teacher',
    progress: 50,
    schools: [
      {
        name: 'Test school',
        uploadedFile: {
          originalName: 'Test school image',
        },
      },
    ],
  };

  const component = (journey = defaultJourney) =>
    render(
      <QueryClientProvider client={queryClient}>
        <Router history={history}>
          <JourneyListCard journey={journey} />
        </Router>
      </QueryClientProvider>
    );

  it('should show journey data', async () => {
    jest.spyOn(NotificationsApiMock, 'useGetNotifications').mockImplementation(params =>
      useQuery<INotification[]>([NOTIFICATIONS_KEY, params.type], async () => getNotifications(params), {
        enabled: true,
      })
    );
    component();

    await screen.findByText('Test school');
    await screen.findByText('Journey title');
    await screen.findByText('2');
    await screen.findByText('50%');
    await screen.findByText('TT');
    await screen.findByAltText('Test school');
  });

  it('should show active tasks section', () => {
    component();

    screen.getByText('schoolabyApp.dashboard.tasks');
    screen.getByText('schoolabyApp.dashboard.noUpcomingTasks');
    screen.getByAltText('schoolabyApp.dashboard.noUpcomingTasks');

    const upcomingTasksCount = screen.getByLabelText('schoolabyApp.dashboard.upcomingTasksCount');
    expect(upcomingTasksCount).toHaveTextContent('0');
  });

  it('should show questions section', () => {
    const journey = {
      ...defaultJourney,
      id: 2,
    };
    component(journey);

    screen.getByText('schoolabyApp.dashboard.questions');
    screen.getByText('schoolabyApp.dashboard.noQuestions');
    screen.getByAltText('schoolabyApp.dashboard.noQuestions');

    const recentQuestionsCount = screen.getByLabelText('schoolabyApp.dashboard.recentQuestionsCount');
    expect(recentQuestionsCount).toHaveTextContent('0');
  });

  it('should show overdue tasks section', () => {
    component();

    screen.getByText('schoolabyApp.dashboard.teacher.overdueTasks');
    screen.getByText('schoolabyApp.dashboard.teacher.noOverdueTasks');
    screen.getByAltText('schoolabyApp.dashboard.teacher.noOverdueTasks');

    const upcomingTasksCount = screen.getByLabelText('schoolabyApp.dashboard.overdueTasksCount');
    expect(upcomingTasksCount).toHaveTextContent('0');
  });

  it('should show students section', () => {
    const journey = {
      ...defaultJourney,
      id: 2,
    };
    component(journey);

    screen.getByText('schoolabyApp.assignment.member.students');
    screen.getByText('schoolabyApp.dashboard.noStudents');
    screen.getByAltText('schoolabyApp.dashboard.noStudents');
  });
});
