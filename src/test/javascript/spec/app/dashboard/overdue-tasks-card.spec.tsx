import React from 'react';
import { render, screen } from '@testing-library/react';
import { createBrowserHistory } from 'history';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Router } from 'react-router-dom';
import OverdueTasksCard from 'app/dashboard/journey-list-card/inner-card/overdue-tasks-card';

describe('Overdue tasks card', () => {
  const queryClient = new QueryClient();
  const history = createBrowserHistory();

  const component = () =>
    render(
      <QueryClientProvider client={queryClient}>
        <Router history={history}>
          <OverdueTasksCard journeyId={1} journeyStudentCount={2} />
        </Router>
      </QueryClientProvider>
    );

  it('should show overdue tasks data', async () => {
    component();

    screen.getByText('schoolabyApp.dashboard.teacher.overdueTasks');
    await screen.findByText('Overdue assignment');
    await screen.findByText('09/05/2021');
    const submissions = screen.getByTestId('submissions-count');
    expect(submissions).toHaveTextContent('1/2');
  });
});
