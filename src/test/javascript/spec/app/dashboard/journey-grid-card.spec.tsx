import React from 'react';
import { render, screen } from '@testing-library/react';
import { createBrowserHistory } from 'history';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Router } from 'react-router-dom';
import JourneyGridCard from 'app/dashboard/journey-grid-card/journey-grid-card';
import { UserNotificationProvider } from 'app/shared/contexts/user-notification-context';

describe('Journey grid card', () => {
  const queryClient = new QueryClient();
  const history = createBrowserHistory();

  const defaultJourney = {
    id: 1,
    title: 'Journey title',
    teacherName: 'teacher teacher',
    progress: 50,
    schools: [
      {
        name: 'Test school',
        uploadedFile: {
          originalName: 'Test school image',
        },
      },
    ],
  };

  const component = (journey = defaultJourney) =>
    render(
      <QueryClientProvider client={queryClient}>
        <Router history={history}>
          <UserNotificationProvider>
            <JourneyGridCard journey={journey} />
          </UserNotificationProvider>
        </Router>
      </QueryClientProvider>
    );

  it('should show journey data', async () => {
    component();

    await screen.findByText('Test school');
    await screen.findByText('Journey title');
    await screen.findByAltText('Test school');
  });

  it('should show messages section', () => {
    component();

    screen.getByText('schoolabyApp.dashboard.grid.newMessages');
  });

  it('should show overdue section', () => {
    component();

    screen.getByText('schoolabyApp.dashboard.grid.overdue');
  });

  it('should show notifications section', () => {
    component();

    screen.getByText('schoolabyApp.dashboard.grid.notifications');
  });
});
