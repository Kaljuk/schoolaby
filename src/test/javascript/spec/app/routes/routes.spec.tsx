import React from 'react';
import { render, waitFor } from '@testing-library/react';
import AppRoutes from 'app/routes';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { RootProvider } from 'app/shared/contexts/root-context';
import { createBrowserHistory } from 'history';
import { Router } from 'react-router-dom';
import { AUTHORITIES, ROLE } from 'app/config/constants';
import { IUser } from 'app/shared/model/user.model';

interface RoutesStoreProps {
  authentication: { isAuthenticated: boolean; account: IUser };
  locale: { currentLocale: string };
}

describe('Routes', () => {
  const mockStore = configureMockStore();
  const accountSettingsPath = '/account/settings';
  let history;

  const createStore = (): RoutesStoreProps => ({
    authentication: {
      isAuthenticated: true,
      account: {
        login: 'student',
      },
    },
    locale: {
      currentLocale: 'en',
    },
  });

  const renderComponent = (store = createStore()) => {
    return render(
      <Provider store={mockStore(store)}>
        <Router history={history}>
          <RootProvider>
            <AppRoutes />
          </RootProvider>
        </Router>
      </Provider>
    );
  };

  beforeEach(() => {
    history = {
      ...createBrowserHistory(),
    };
  });

  it('should push history to settings page when account profile required fields are unfilled', async () => {
    const historyPush = jest.fn();
    history.push = historyPush;
    const historyState = { accountFieldsUnFilled: true, returnUrl: '/' };
    renderComponent();

    await waitFor(() => {
      expect(historyPush).toBeCalledWith(accountSettingsPath, historyState);
    });
  });

  it('should push history to settings page with correct returnUrl when account profile required fields are unfilled', async () => {
    const pathname = '/assignments/10';
    const search = '?journeyId=1';
    const historyPush = jest.fn();
    history.location.pathname = pathname;
    history.location.search = search;
    history.push = historyPush;
    const historyState = { accountFieldsUnFilled: true, returnUrl: pathname + search };

    renderComponent();

    await waitFor(() => {
      expect(historyPush).toBeCalledWith(accountSettingsPath, historyState);
    });
  });

  it('should not push history to settings page when user is already on settings page', async () => {
    const historyPush = jest.fn();
    history.location.pathname = accountSettingsPath;
    history.push = historyPush;

    renderComponent();

    await waitFor(() => {
      expect(historyPush).not.toBeCalled();
    });
  });

  it('should not push history to settings page when user profile has been filled', async () => {
    const historyPush = jest.fn();
    history.push = historyPush;
    const store = createStore();
    store.authentication.account = {
      ...store.authentication.account,
      firstName: 'student',
      lastName: 'student',
      email: 'student@localhost',
      country: 'Estonia',
      authorities: [AUTHORITIES.STUDENT],
      personRoles: [{ id: 1, role: ROLE.STUDENT, school: { id: 1, name: 'Test school' } }],
    };

    renderComponent(store);

    await waitFor(() => {
      expect(historyPush).not.toBeCalled();
    });
  });

  it('should not push history to settings page when user is not authenticated', async () => {
    const historyPush = jest.fn();
    history.push = historyPush;
    const store = createStore();
    store.authentication.isAuthenticated = false;
    store.authentication.account = {};

    renderComponent(store);

    await waitFor(() => {
      expect(historyPush).not.toBeCalled();
    });
  });
});
