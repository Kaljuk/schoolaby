import * as toastify from 'react-toastify';
import axios from 'axios';
import sinon from 'sinon';

import setupAxiosInterceptors from 'app/config/axios-interceptor';

describe('Axios Interceptor', () => {
  const client = axios;
  const onUnauthenticated = sinon.spy();
  setupAxiosInterceptors(onUnauthenticated);

  describe('setupAxiosInterceptors', () => {
    it('onRequestSuccess is called on fulfilled request', () => {
      expect((client.interceptors.request as any).handlers[0].fulfilled({ data: 'foo', url: '/test' })).toMatchObject({
        data: 'foo',
      });
    });
    it('onResponseSuccess is called on fulfilled response', () => {
      expect((client.interceptors.response as any).handlers[0].fulfilled({ data: 'foo' })).toEqual({ data: 'foo' });
    });
    it('onResponseError is called on rejected response', () => {
      (client.interceptors.response as any).handlers[0].rejected({
        response: {
          statusText: 'NotFound',
          status: 403,
          data: { message: 'Page not found' },
        },
      });
      expect(onUnauthenticated.calledOnce).toBe(true);
    });
  });

  describe('toast', () => {
    beforeEach(() => {
      sinon.spy(toastify.toast, 'error');
      sinon.spy(toastify.toast, 'success');
    });

    describe('when showToast is true', () => {
      it('should display success toast onResponseSuccess', () => {
        (client.interceptors.response as any).handlers[0].fulfilled({
          config: {
            showToast: true,
          },
          headers: {
            'x-schoolabyapp-alert': 'foo.created',
          },
        });
        const toastMsg = (toastify.toast as any).success.getCall(0).args[0];
        expect(toastMsg).toContain('foo.created');
      });

      it('should display error toast onResponseError with data.message value', () => {
        (client.interceptors.response as any).handlers[0].rejected({
          response: {
            config: {
              showToast: true,
            },
            data: {
              message: 'foo.error',
            },
          },
        });
        const toastMsg = (toastify.toast as any).error.getCall(0).args[0];
        expect(toastMsg).toContain('foo.error');
      });

      it('should display error toast onResponseError with error.unknown when data.message is missing', () => {
        (client.interceptors.response as any).handlers[0].rejected({
          response: {
            config: {
              showToast: true,
            },
          },
        });
        const toastMsg = (toastify.toast as any).error.getCall(0).args[0];
        expect(toastMsg).toContain('error.unknown');
      });
    });

    describe('when showToast is false', () => {
      it('should not display success toast onResponseSuccess', () => {
        (client.interceptors.response as any).handlers[0].fulfilled({
          config: {
            showToast: false,
          },
          headers: {
            'x-schoolabyapp-alert': 'foo.created',
          },
        });
        expect((toastify.toast as any).error.called).toEqual(false);
        expect((toastify.toast as any).success.called).toEqual(false);
      });

      it('should not display error toast onResponseError', () => {
        (client.interceptors.response as any).handlers[0].rejected({
          response: {
            config: {
              showToast: false,
            },
            data: {
              message: 'foo.error',
            },
          },
        });
        expect((toastify.toast as any).error.called).toEqual(false);
        expect((toastify.toast as any).success.called).toEqual(false);
      });
    });

    afterEach(() => {
      (toastify.toast as any).error.restore();
      (toastify.toast as any).success.restore();
    });
  });
});
