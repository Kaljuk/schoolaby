import React from 'react';
import TermsModal from 'app/account/common/terms-modal';
import { render, screen } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { AUTHORITIES } from 'app/config/constants';
import { QueryClient, QueryClientProvider } from 'react-query';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

describe('TermsModal', () => {
  const queryClient = new QueryClient();

  const initialState = {
    authentication: {
      isAuthenticated: true,
      account: {
        termsAgreed: false,
        authorities: [AUTHORITIES.TEACHER],
      },
    },
    applicationProfile: {
      inProduction: false,
    },
  };

  const renderComponent = store => {
    const mockStore = configureMockStore();

    render(
      <QueryClientProvider client={queryClient}>
        <Provider store={mockStore(store)}>
          <TermsModal />
        </Provider>
      </QueryClientProvider>
    );
  };

  it('should render when terms not agreed and has role', () => {
    const termsDisagreedState = initialState;
    termsDisagreedState.authentication.account.termsAgreed = false;
    renderComponent(termsDisagreedState);

    screen.getByText('global.hello');
    screen.getByText('global.renewedTerms');
    screen.getByText('global.agreeToTerms');
    screen.getByText('global.schoolabyTeam');

    screen.getByText('global.decline');
    screen.getByText('global.accept');
  });

  it('should not render when terms agreed and has role', () => {
    const termsDisagreedState = initialState;
    termsDisagreedState.authentication.account.termsAgreed = true;
    renderComponent(termsDisagreedState);

    expect(screen.queryByText('global.decline')).toBeNull();
    expect(screen.queryByText('global.accept')).toBeNull();
  });

  it('should not render when account has no roles', () => {
    const termsDisagreedState = initialState;
    termsDisagreedState.authentication.account.termsAgreed = true;
    termsDisagreedState.authentication.account.authorities = [];
    renderComponent(termsDisagreedState);

    expect(screen.queryByText('global.decline')).toBeNull();
    expect(screen.queryByText('global.accept')).toBeNull();
  });
});
