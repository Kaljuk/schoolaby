import { act, fireEvent, render, screen, waitFor } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import React from 'react';
import { ThroughProvider } from 'react-through';
import configureMockStore from 'redux-mock-store';
import { RootProvider } from 'app/shared/contexts/root-context';
import { QueryClient, QueryClientProvider } from 'react-query';
import { RegisterPage } from 'app/account/register/register';
import userEvent from '@testing-library/user-event';

describe('Register page', () => {
  const history = createBrowserHistory();
  const queryClient = new QueryClient();
  const store = configureMockStore()();

  const component = () => {
    return {
      ...render(
        <RootProvider>
          <QueryClientProvider client={queryClient}>
            <ThroughProvider>
              <Router history={history}>
                <RegisterPage currentLocale={'en'} handleRegister={() => {}} reset={() => ({ type: '' })} />
              </Router>
            </ThroughProvider>
          </QueryClientProvider>
        </RootProvider>
      ),
      store,
    };
  };

  it('Should display school search dropdown', async () => {
    component();
    await waitFor(() => {
      screen.getByText('settings.form.searchSchool');
      screen.getByText('global.form.role.selectYourMainRole');
      screen.getByText('global.form.personRoles');
      screen.getByLabelText('entity.add.school');
      expect(screen.getAllByLabelText('global.teacher')).toHaveLength(2);
      expect(screen.getAllByLabelText('global.student')).toHaveLength(2);
    });
  });

  it('Should display default roles selected', () => {
    component();
    const studentRoleRadioButtons = screen.getAllByLabelText('global.student');
    const teacherRoleRadioButtons = screen.getAllByLabelText('global.teacher');

    expect(studentRoleRadioButtons[0]).toBeChecked();
    expect(studentRoleRadioButtons[1]).toBeChecked();

    expect(teacherRoleRadioButtons[0]).not.toBeChecked();
    expect(teacherRoleRadioButtons[1]).not.toBeChecked();

    fireEvent.click(teacherRoleRadioButtons[0]);

    fireEvent.click(screen.getByLabelText('entity.add.school'));

    expect(screen.getAllByLabelText('global.teacher')[2]).toBeChecked();
    expect(screen.getAllByLabelText('global.student')[2]).not.toBeChecked();
  });

  it('Should display validation error when no schools are selected', async () => {
    component();
    const input = screen.getAllByRole('textbox')[4];
    const submitButton = screen.getByText('register.form.button');
    fireEvent.click(submitButton);
    await waitFor(() => {
      screen.getByText('settings.messages.validate.school.required');
    });

    act(() => userEvent.type(input, 'Tallinna Saksa'));

    await waitFor(() => {
      const dropdownItem = screen.getByText('Tallinna Saksa Gümnaasium');
      fireEvent.click(dropdownItem);
    });

    expect(screen.queryByText('settings.messages.validate.school.required')).not.toBeInTheDocument();
  });
});
