import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import React from 'react';
import { ThroughProvider } from 'react-through';
import configureMockStore from 'redux-mock-store';
import { AUTHORITIES, ROLE } from 'app/config/constants';
import { RootProvider } from 'app/shared/contexts/root-context';
import { IUserSettingsProps, SettingsPage } from 'app/account/settings/settings';
import { IUser } from 'app/shared/model/user.model';
import { QueryClient, QueryClientProvider } from 'react-query';

describe('Settings page', () => {
  const mockStore = configureMockStore();
  let history;
  const queryClient = new QueryClient();
  let store: any;

  const mockUser = (authority): IUser => ({
    login: 'login',
    firstName: 'first',
    lastName: 'last',
    authorities: [authority],
  });

  const component = (account: IUser) => {
    const initialState = {
      account,
    };
    store = mockStore(initialState);

    const settingPageProps: IUserSettingsProps = {
      account,
      getSession: jest.fn(),
      reset: jest.fn(),
      saveAccountSettings: jest.fn(),
      hasRole: true,
      locale: 'en',
    };

    return {
      ...render(
        <RootProvider>
          <QueryClientProvider client={queryClient}>
            <ThroughProvider>
              <Router history={history}>
                <SettingsPage {...settingPageProps} />
              </Router>
            </ThroughProvider>
          </QueryClientProvider>
        </RootProvider>
      ),
      store,
    };
  };

  beforeEach(() => {
    history = createBrowserHistory();
  });

  it('Should display main role selection when account has authority student', () => {
    component(mockUser(AUTHORITIES.STUDENT));
    const studentRadioBtn = screen.getAllByLabelText('global.student')[0];
    const teacherRadioBtn = screen.getAllByLabelText('global.teacher')[0];
    expect(studentRadioBtn).toBeDefined();
    expect(studentRadioBtn).toHaveProperty('checked', true);
    expect(teacherRadioBtn).toHaveProperty('checked', false);
  });

  it('Should display main role selection when account has authority teacher', () => {
    component(mockUser(AUTHORITIES.TEACHER));
    const studentRadioBtn = screen.getAllByLabelText('global.student')[0];
    const teacherRadioBtn = screen.getAllByLabelText('global.teacher')[0];
    expect(studentRadioBtn).toBeDefined();
    expect(studentRadioBtn).toHaveProperty('checked', false);
    expect(teacherRadioBtn).toHaveProperty('checked', true);
  });

  it('Should display school search dropdown', async () => {
    component(mockUser(AUTHORITIES.TEACHER));
    await waitFor(() => {
      screen.getByText('settings.form.searchSchool');
      screen.getByText('global.form.personRoles');
      screen.getByText('entity.add.school');
      expect(screen.getAllByLabelText('global.teacher')).toHaveLength(2);
      expect(screen.getAllByLabelText('global.student')).toHaveLength(2);
    });
  });

  it('should display alert when accountFieldsUnFilled is true in history state', async () => {
    history.location.state = {
      accountFieldsUnFilled: true,
    };
    component(mockUser(AUTHORITIES.TEACHER));

    await screen.findByText('settings.messages.requiredFieldsUnFilled');
  });

  it('should push history to correct url when returnUrl in history state', async () => {
    const historyPush = jest.fn();
    const testReturnUrl = '/test/url';
    history.location.state = {
      returnUrl: testReturnUrl,
    };
    history.push = historyPush;
    const account = {
      ...mockUser(AUTHORITIES.STUDENT),
      email: 'stuent@localhost',
      personRoles: [
        {
          id: 1,
          role: ROLE.STUDENT,
          school: { id: 1, name: 'Test school' },
        },
      ],
    };

    component(account);

    const submitButton = await screen.findByText('settings.form.button');
    fireEvent.click(submitButton);

    await waitFor(() => {
      expect(historyPush).toBeCalledWith(testReturnUrl);
    });
  });
});
