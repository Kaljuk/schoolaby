export const multipleElementsTextHelper = (node, text) => {
  const hasText = textNode => textNode.textContent === text;
  const nodeHasText = hasText(node);
  const childrenDontHaveText = Array.from(node.children).every(child => !hasText(child));
  return nodeHasText && childrenDontHaveText;
};
