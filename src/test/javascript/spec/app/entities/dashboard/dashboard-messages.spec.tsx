import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { RootProvider } from 'app/shared/contexts/root-context';
import DashboardMessages from 'app/dashboard/dashboard-messages';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: jest.fn(),
    location: {
      pathname: '/journey/1/students',
    },
  }),
}));

const mockStore = configureMockStore();
const history = createBrowserHistory();

describe('Dashboard messages', () => {
  let store: any;

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'et',
      },
    };
    store = mockStore(initialState);
  });

  const renderWithRedux = () => {
    return {
      ...render(
        <Provider store={store}>
          <Router history={history}>
            <RootProvider>
              <DashboardMessages />
            </RootProvider>
          </Router>
        </Provider>
      ),
      store,
    };
  };

  it('Renders dashboard messages', async () => {
    renderWithRedux();
    await waitFor(() => {
      screen.getByText('author');
      screen.getByText('message value');
      screen.getByText('14.06.2020');
      screen.getByText('journey title');
    });
  });
});
