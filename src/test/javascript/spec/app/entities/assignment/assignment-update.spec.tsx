import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { RootProvider } from 'app/shared/contexts/root-context';
import { MaterialProvider } from 'app/shared/contexts/material-context';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { QueryClient, QueryClientProvider } from 'react-query';
import MockEntityUpdateProvider, { IMockEntityUpdateProvider } from '../../../helper/context-mock/mock-entity-update-provider';
import AssignmentUpdate from 'app/assignment/assignment-update';
import { createBrowserHistory } from 'history';
import { Router } from 'react-router-dom';
import userEvent from '@testing-library/user-event';
import { COUNTRY } from 'app/config/constants';

jest.mock('react-router-dom', () => ({
  __esModule: true,
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

describe('Assignment update', () => {
  const mockStore = configureMockStore();
  const history = createBrowserHistory();

  const getUpdateProviderProps = () => ({
    assignmentUpdateId: 1234,
    milestoneUpdateId: 1,
  });

  const getStore = () => ({
    locale: {
      currentLocale: 'en',
    },
    applicationProfile: {
      inProduction: false,
    },
    authentication: {
      account: {
        country: 'Estonia',
      },
    },
  });

  const renderComponent = (updateProviderProps: IMockEntityUpdateProvider, store = getStore()) =>
    render(
      <Router history={history}>
        <RootProvider>
          <Provider store={mockStore(store)}>
            <QueryClientProvider client={new QueryClient()}>
              <MockEntityUpdateProvider selectedEducationalAlignments={[]} {...updateProviderProps}>
                <MaterialProvider>
                  <AssignmentUpdate />
                </MaterialProvider>
              </MockEntityUpdateProvider>
            </QueryClientProvider>
          </Provider>
        </RootProvider>
      </Router>
    );

  it('should display assignment form', async () => {
    const updateProviderProps = getUpdateProviderProps();
    updateProviderProps.assignmentUpdateId = undefined;
    renderComponent(updateProviderProps);

    await screen.findByText('entity.add.task');
    await screen.findByLabelText('schoolabyApp.assignment.title');
    await screen.findByPlaceholderText('schoolabyApp.assignment.titlePlaceholder');
    await screen.findByLabelText('schoolabyApp.assignment.description');
    await screen.findByText('schoolabyApp.educationalAlignment.detail.title');
    await screen.findByLabelText('schoolabyApp.assignment.gradingScheme');
    await screen.findByText('+ schoolabyApp.assignment.addRubric');
    expect(screen.queryByLabelText('schoolabyApp.assignment.requiresSubmission')).toBeChecked();
    await screen.findByLabelText('schoolabyApp.assignment.deadline');
    expect(screen.queryByLabelText('schoolabyApp.assignment.flexibleDeadline')).toBeChecked();
    expect(screen.queryByLabelText('entity.publishedState.published')).toBeChecked();
    expect(screen.queryByLabelText('entity.publishedState.unpublished')).not.toBeChecked();
    expect(screen.queryByLabelText('entity.publishedState.publishAt')).not.toBeChecked();
    await screen.findByText('schoolabyApp.assignment.detail.students');
    await screen.findByText('entity.action.cancel');
    await screen.findByText('entity.action.save');
  });

  it('should display assignment edit title when assignmentUpdateId exists', async () => {
    renderComponent(getUpdateProviderProps());

    await screen.findByText('entity.edit.task');
  });

  it('should display selected students as checked', async () => {
    renderComponent(getUpdateProviderProps());

    await screen.findByText('schoolabyApp.assignment.detail.students');

    const student1Checkbox = await screen.findByRole('checkbox', { name: 'Student1 Student1' });
    expect(student1Checkbox).toBeChecked();

    const student2Checkbox = await screen.findByRole('checkbox', { name: 'Student2 Student2' });
    expect(student2Checkbox).not.toBeChecked();

    const selectAllCheckbox = await screen.findByRole('checkbox', { name: 'schoolabyApp.assignment.update.selectAll' });
    expect(selectAllCheckbox).not.toBeChecked();
  });

  it('should check all students when selectAll checkbox is checked', async () => {
    renderComponent(getUpdateProviderProps());

    await screen.findByText('schoolabyApp.assignment.detail.students');

    const selectAllCheckbox = await screen.findByRole('checkbox', { name: 'schoolabyApp.assignment.update.selectAll' });
    expect(selectAllCheckbox).not.toBeChecked();

    fireEvent.click(selectAllCheckbox);

    expect(selectAllCheckbox).toBeChecked();

    const student1Checkbox = await screen.findByRole('checkbox', { name: 'Student1 Student1' });
    expect(student1Checkbox).toBeChecked();

    const student2Checkbox = await screen.findByRole('checkbox', { name: 'Student2 Student2' });
    expect(student2Checkbox).toBeChecked();
  });

  it('should uncheck all students when selectAll checkbox is unchecked', async () => {
    renderComponent(getUpdateProviderProps());

    await screen.findByText('schoolabyApp.assignment.detail.students');

    const selectAllCheckbox = await screen.findByRole('checkbox', { name: 'schoolabyApp.assignment.update.selectAll' });
    expect(selectAllCheckbox).not.toBeChecked();

    fireEvent.click(selectAllCheckbox);
    fireEvent.click(selectAllCheckbox);

    expect(selectAllCheckbox).not.toBeChecked();

    const student1Checkbox = await screen.findByRole('checkbox', { name: 'Student1 Student1' });
    expect(student1Checkbox).not.toBeChecked();

    const student2Checkbox = await screen.findByRole('checkbox', { name: 'Student2 Student2' });
    expect(student2Checkbox).not.toBeChecked();
  });

  it('should display validation errors', async () => {
    const updateProviderProps = getUpdateProviderProps();
    updateProviderProps.assignmentUpdateId = undefined;
    renderComponent(updateProviderProps);

    await screen.findByText('schoolabyApp.assignment.detail.students');

    const selectAllCheckbox = await screen.findByRole('checkbox', { name: 'schoolabyApp.assignment.update.selectAll' });
    fireEvent.click(selectAllCheckbox);

    fireEvent.click(await screen.findByText('entity.action.save'));

    expect(await screen.findAllByText('entity.validation.required')).toHaveLength(2);
    await screen.findByText('schoolabyApp.educationalAlignment.errors.inputSelector');

    const atLeastOneStudentRequired = await screen.findAllByText('schoolabyApp.assignment.update.atLeastOneStudentRequired');
    expect(atLeastOneStudentRequired).toHaveLength(2);
  });

  it('should select grading scheme "None" when assignment is new assignment and Mandatory to submit checkbox is unchecked', async () => {
    const updateProviderProps = getUpdateProviderProps();
    updateProviderProps.assignmentUpdateId = undefined;
    renderComponent(updateProviderProps);

    const mandatoryToSubmit = await screen.findByRole('checkbox', { name: 'schoolabyApp.assignment.requiresSubmission' });
    const selectGradingScheme = await screen.findByRole('combobox', { name: 'schoolabyApp.assignment.gradingScheme' });
    expect(selectGradingScheme).toHaveValue('0');

    fireEvent.click(mandatoryToSubmit);

    expect(selectGradingScheme).toHaveValue('2');
  });

  it('should not select grading scheme "None" when assignment is new assignment and user has already chosen grading scheme and Mandatory to submit checkbox is unchecked', async () => {
    const updateProviderProps = getUpdateProviderProps();
    updateProviderProps.assignmentUpdateId = undefined;
    renderComponent(updateProviderProps);

    const mandatoryToSubmit = await screen.findByRole('checkbox', { name: 'schoolabyApp.assignment.requiresSubmission' });
    const selectGradingScheme = await screen.findByRole('combobox', { name: 'schoolabyApp.assignment.gradingScheme' });
    expect(selectGradingScheme).toHaveValue('0');

    userEvent.selectOptions(selectGradingScheme, screen.getByRole('option', { name: 'schoolabyApp.gradingScheme.code.NUMERICAL_1_5' }));
    expect(selectGradingScheme).toHaveValue('1');

    fireEvent.click(mandatoryToSubmit);

    expect(selectGradingScheme).toHaveValue('1');
  });

  it('should not select grading scheme "None" when assignment is not new assignment and Mandatory to submit checkbox is unchecked', async () => {
    renderComponent(getUpdateProviderProps());

    const mandatoryToSubmit = await screen.findByRole('checkbox', { name: 'schoolabyApp.assignment.requiresSubmission' });
    const selectGradingScheme = await screen.findByRole('combobox', { name: 'schoolabyApp.assignment.gradingScheme' });
    expect(selectGradingScheme).toHaveValue('1');

    fireEvent.click(mandatoryToSubmit);

    expect(selectGradingScheme).toHaveValue('1');
  });

  it('should display rubric link with rubric title and points', async () => {
    renderComponent(getUpdateProviderProps());

    await screen.findByText('entity.edit.task');
    await screen.findByLabelText('schoolabyApp.assignment.title');

    await screen.findByText('schoolabyApp.assignment.rubric.titleWithName Test rubric (8 p)');
  });

  it('should make assignment educational alignments optional if user is Ukrainian', async () => {
    const updateProviderProps = { ...getUpdateProviderProps(), assignmentUpdateId: undefined };
    const store = getStore();
    store.authentication.account.country = COUNTRY.UKRAINE;
    renderComponent(updateProviderProps, store);

    const submitButton = await screen.findByText('entity.action.save');
    fireEvent.click(submitButton);

    expect(await screen.findAllByText('entity.validation.required')).toHaveLength(2);
    expect(screen.queryByText('schoolabyApp.educationalAlignment.errors.inputSelector')).not.toBeInTheDocument();
  });
});
