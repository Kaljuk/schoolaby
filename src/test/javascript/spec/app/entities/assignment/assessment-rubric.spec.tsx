import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { MemoryRouter, Router } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import MockEntityUpdateProvider from '../../../helper/context-mock/mock-entity-update-provider';
import { createRubric } from 'app/shared/util/rubric';
import { createBrowserHistory } from 'history';
import AssessmentRubric from 'app/assignment/assessment-rubric-new/assessment-rubric';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { IRubric } from 'app/shared/model/rubric/rubric.model';

describe('Assessment rubric', () => {
  const historyPush = jest.fn();
  const historyReplace = jest.fn();

  const mockStore = configureMockStore();

  const getContextProps = () => ({
    selectedRubric: undefined,
  });

  const createHistory = (rubric?: IRubric) => {
    const history = createBrowserHistory();
    history.push = historyPush;
    history.replace = historyReplace;

    history.location.state = {
      entity: {
        title: 'Test Assignment',
        rubric,
        journeyId: 1,
      },
    };

    return history;
  };

  const initialState = {
    authentication: {
      account: {
        country: 'Estonia',
        login: 'teacher',
      },
    },
    locale: {
      currentLocale: '',
    },
  };

  const store = mockStore(initialState);

  const renderComponent = (history = createHistory(), contextProps = getContextProps()) => {
    return render(
      <Provider store={store}>
        <Router history={history}>
          <MockEntityUpdateProvider {...contextProps}>
            <QueryClientProvider client={new QueryClient()}>
              <AssessmentRubric />
            </QueryClientProvider>
          </MockEntityUpdateProvider>
        </Router>
      </Provider>,
      { wrapper: MemoryRouter }
    );
  };

  it('should display heading and searchbar', async () => {
    renderComponent();
    await screen.findByText('schoolabyApp.assignment.rubric.headingTitle Test Assignment');
    await screen.findByText('schoolabyApp.assignment.rubric.search');
  });

  it('should display rubric title and save as template buttons', async () => {
    renderComponent();
    await screen.findByLabelText('schoolabyApp.assignment.rubric.title');
    await screen.findByPlaceholderText('schoolabyApp.assignment.rubric.titlePlaceholder');
    await screen.findByText('schoolabyApp.assignment.rubric.template.saveAsTemplate');
  });

  it('should display rubric criteria and criterion levels', async () => {
    renderComponent();
    expect(await screen.findAllByLabelText('schoolabyApp.assignment.rubric.criterion.title')).toHaveLength(3);
    expect(await screen.findAllByPlaceholderText('schoolabyApp.assignment.rubric.criterion.titlePlaceholder')).toHaveLength(3);
    expect(await screen.findAllByLabelText('schoolabyApp.assignment.rubric.criterion.description')).toHaveLength(3);
    expect(await screen.findAllByPlaceholderText('schoolabyApp.assignment.rubric.criterion.descriptionPlaceholder')).toHaveLength(3);

    expect(await screen.findAllByLabelText('schoolabyApp.assignment.rubric.criterion.level.title')).toHaveLength(9);
    expect(await screen.findAllByPlaceholderText('schoolabyApp.assignment.rubric.addTitle')).toHaveLength(9);
    expect(await screen.findAllByLabelText('schoolabyApp.assignment.rubric.criterion.level.points')).toHaveLength(9);
    expect(await screen.findAllByPlaceholderText('schoolabyApp.assignment.rubric.criterion.level.pointsPlaceholder')).toHaveLength(9);
    expect(await screen.findAllByLabelText('schoolabyApp.assignment.rubric.criterion.level.description')).toHaveLength(9);
    expect(await screen.findAllByPlaceholderText('schoolabyApp.assignment.rubric.criterion.level.descriptionPlaceholder')).toHaveLength(9);
  });

  it('should display buttons for adding and removing criteria', async () => {
    renderComponent();

    expect(await screen.findAllByText('+ schoolabyApp.assignment.rubric.criterion.addNew')).toHaveLength(4);
    expect(await screen.findAllByText('schoolabyApp.assignment.rubric.criterion.delete')).toHaveLength(3);
  });

  it('should display buttons for adding and removing levels', async () => {
    renderComponent();

    expect(await screen.findAllByLabelText('schoolabyApp.assignment.rubric.criterion.level.addNew')).toHaveLength(12);
    expect(await screen.findAllByLabelText('schoolabyApp.assignment.rubric.criterion.level.delete')).toHaveLength(9);
  });

  it('should display footer', async () => {
    renderComponent();
    await screen.findByText('entity.action.cancel');
    await screen.findByText('entity.action.save');
  });

  it('should display validation errors', async () => {
    renderComponent();
    fireEvent.click(await screen.findByText('entity.action.save'));

    await screen.findByText('schoolabyApp.assignment.rubric.errors.title.required');

    expect(await screen.findAllByText('schoolabyApp.assignment.rubric.errors.criterion.title.required')).toHaveLength(3);
    expect(await screen.findAllByText('schoolabyApp.assignment.rubric.errors.criterion.description.required')).toHaveLength(3);

    expect(await screen.findAllByText('schoolabyApp.assignment.rubric.errors.criterion.level.title.required')).toHaveLength(9);
    expect(await screen.findAllByText('schoolabyApp.assignment.rubric.errors.criterion.level.points.required')).toHaveLength(9);
    expect(await screen.findAllByText('schoolabyApp.assignment.rubric.errors.criterion.level.description.required')).toHaveLength(9);
  });

  it('should display save template confirmation modal', async () => {
    renderComponent(createHistory(createRubric()));

    fireEvent.click(await screen.findByText('schoolabyApp.assignment.rubric.template.saveAsTemplate'));

    await screen.findByText('schoolabyApp.assignment.rubric.template.saveModalTitle');
    await screen.findByText('schoolabyApp.assignment.rubric.template.saveModalContent');
    await screen.findByText('global.yes');
    await screen.findByText('global.no');
  });

  it('should display error message on save template confirmation modal', async () => {
    renderComponent();

    fireEvent.click(await screen.findByText('schoolabyApp.assignment.rubric.template.saveAsTemplate'));

    await screen.findByText('schoolabyApp.assignment.rubric.template.requiredFieldsError');
  });

  it('should display un-save confirmation modal', async () => {
    const rubric = { ...createRubric(), isTemplate: true };
    renderComponent(createHistory(rubric));

    fireEvent.click(await screen.findByText('schoolabyApp.assignment.rubric.template.removeTemplate'));

    await screen.findByText('schoolabyApp.assignment.rubric.template.unSaveModalTitle');
    await screen.findByText('schoolabyApp.assignment.rubric.template.unSaveModalContent');
    await screen.findByText('global.yes');
    await screen.findByText('global.no');
  });

  it('should display rubric template preview', async () => {
    const contextProps = getContextProps();
    contextProps.selectedRubric = {
      ...createRubric(),
      isTemplate: true,
      createdBy: initialState.authentication.account.login,
    };
    renderComponent(createHistory(), contextProps);

    expect(await screen.findAllByText('Test rubric')).toHaveLength(2);
    await screen.findByText('Criterion 1');
    await screen.findByText('Criterion 2');
    expect(await screen.findAllByText('Level 1')).toHaveLength(2);
    expect(await screen.findAllByText('Level 2')).toHaveLength(2);
    expect(await screen.findAllByText('Level description')).toHaveLength(4);
    expect(await screen.findAllByText('entity.action.cancel')).toHaveLength(2);
    await screen.findByText('schoolabyApp.assignment.rubric.template.removeTemplate');
    await screen.findByText('entity.action.select');
  });

  it('should not display delete template on rubric template preview when user not the creator', async () => {
    const contextProps = getContextProps();
    contextProps.selectedRubric = {
      ...createRubric(),
      isTemplate: true,
      createdBy: 'not.teacher',
    };
    renderComponent(createHistory(), contextProps);

    await waitFor(() => {
      screen.getAllByText('Test rubric');
      expect(screen.queryByText('schoolabyApp.assignment.rubric.template.removeTemplate')).not.toBeInTheDocument();
    });
  });

  it('should display a confirmation dialog on clicking select template button', async () => {
    const contextProps = getContextProps();
    contextProps.selectedRubric = {
      ...createRubric(),
      isTemplate: true,
      createdBy: 'not.teacher',
    };
    renderComponent(createHistory(), contextProps);

    fireEvent.click(await screen.findByText('entity.action.select'));

    await screen.findByText('schoolabyApp.assignment.rubric.template.selectModalTitle');
    await screen.findByText('schoolabyApp.assignment.rubric.template.selectModalContent');
    await screen.findByText('global.no');
    await screen.findByText('global.yes');
  });

  it('should fill rubric form with template values', async () => {
    const contextProps = getContextProps();
    contextProps.selectedRubric = {
      ...createRubric(),
    };
    renderComponent(createHistory(), contextProps);

    fireEvent.click(await screen.findByText('entity.action.select'));

    await screen.findByText('schoolabyApp.assignment.rubric.template.selectModalTitle');
    await screen.findByText('schoolabyApp.assignment.rubric.template.selectModalContent');

    fireEvent.click(await screen.findByText('global.yes'));

    expect(await screen.findByLabelText('schoolabyApp.assignment.rubric.title')).toHaveValue('Test rubric');

    const criterionTitleFields = await screen.findAllByLabelText('schoolabyApp.assignment.rubric.criterion.title');
    expect(criterionTitleFields).toHaveLength(2);
    expect(criterionTitleFields[0]).toHaveValue('Criterion 1');
    expect(criterionTitleFields[1]).toHaveValue('Criterion 2');

    const levelFields = await screen.findAllByLabelText('schoolabyApp.assignment.rubric.criterion.level.title');

    expect(levelFields).toHaveLength(4);
    expect(levelFields[0]).toHaveValue('Level 1');
    expect(levelFields[1]).toHaveValue('Level 2');
    expect(levelFields[2]).toHaveValue('Level 1');
    expect(levelFields[3]).toHaveValue('Level 2');
  });

  it('should cancel', async () => {
    renderComponent();

    await waitFor(() => {
      const cancelButton = screen.getByText('entity.action.cancel');
      fireEvent.click(cancelButton);
    });

    expect(historyReplace).toHaveBeenCalled();
  });

  it('should submit rubric', async () => {
    const contextProps = getContextProps();
    contextProps.selectedRubric = {
      ...createRubric(),
    };
    renderComponent(createHistory(), contextProps);

    fireEvent.click(await screen.findByText('entity.action.select'));

    await screen.findByText('schoolabyApp.assignment.rubric.template.selectModalTitle');
    await screen.findByText('schoolabyApp.assignment.rubric.template.selectModalContent');

    fireEvent.click(await screen.findByText('global.yes'));

    expect(await screen.findByLabelText('schoolabyApp.assignment.rubric.title')).toHaveValue('Test rubric');

    fireEvent.click(await screen.findByText('entity.action.save'));

    expect(historyReplace).toHaveBeenCalled();
  });

  it('should save and unsave rubric as template', async () => {
    const contextProps = getContextProps();
    contextProps.selectedRubric = {
      ...createRubric(),
    };
    renderComponent(createHistory(), contextProps);

    fireEvent.click(await screen.findByText('entity.action.select'));

    await screen.findByText('schoolabyApp.assignment.rubric.template.selectModalTitle');
    await screen.findByText('schoolabyApp.assignment.rubric.template.selectModalContent');

    fireEvent.click(await screen.findByText('global.yes'));

    expect(await screen.findByLabelText('schoolabyApp.assignment.rubric.title')).toHaveValue('Test rubric');

    fireEvent.click(await screen.findByText('schoolabyApp.assignment.rubric.template.saveAsTemplate'));
    fireEvent.click(await screen.findByText('global.yes'));

    fireEvent.click(await screen.findByText('schoolabyApp.assignment.rubric.template.removeTemplate'));
    fireEvent.click(await screen.findByText('global.yes'));
    await screen.findByText('schoolabyApp.assignment.rubric.template.saveAsTemplate');
  });
});
