import React from 'react';
import configureMockStore from 'redux-mock-store';
import { fireEvent, render, screen, waitFor, within } from '@testing-library/react';
import { AUTHORITIES } from 'app/config/constants';
import { RootProvider } from 'app/shared/contexts/root-context';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ThroughProvider } from 'react-through';
import MockEntityDetailProvider, { IMockEntityDetailProvider } from '../../../../../helper/context-mock/mock-entity-detail-provider';
import ContextProvider from '../../../../../helper/context-mock/context-provider';
import MockSubmissionGradingProvider, {
  IMockSubmissionGradingProvider,
} from '../../../../../helper/context-mock/mock-submission-grading-provider';
import SubmissionFeedbackBody from 'app/assignment/assignment-detail/submission/submission-grading-body/submission-feedback-body';
import { IMessage } from 'app/shared/model/message.model';
import { IUploadedFile } from 'app/shared/model/uploaded-file.model';
import { multipleElementsTextHelper } from '../../../../utils';
import { createSelectedCriterionLevels } from 'app/shared/util/rubric';
import { ISubmissionFeedback } from 'app/shared/model/submission/submission-feedback.model';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';

const mockStore = configureMockStore();
const queryClient = new QueryClient();

// For SimpleMDE editor, since it is using JSDOM
const mockCreateRange = () => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
  // @ts-ignore
  document.createRange = function () {
    return {
      setEnd() {},
      setStart() {},
      getBoundingClientRect() {
        return { right: 0 };
      },
      getClientRects() {
        return {
          length: 0,
          left: 0,
          right: 0,
        };
      },
    };
  };
};

describe('Submission feedback body', () => {
  let store: any;

  beforeEach(() => {
    mockCreateRange();
    const initialState = {
      isAllowedToModify: true,
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
          id: 7,
        },
      },
      applicationProfile: {
        isInProduction: false,
      },
    };
    store = mockStore(initialState);
  });

  const contextProps: IMockEntityDetailProvider = {
    selectedUser: {
      id: 5,
      login: 'student',
      firstName: 'student',
      lastName: 'student',
      email: 'student@localhost.com',
      personRoles: [],
      authorities: ['ROLE_STUDENT'],
    },
    entity: {
      id: 1234,
      title: 'Assignment title',
      description: 'Description text',
      gradingSchemeId: 10,
      deadline: '2020-09-07T09:24:00Z',
      flexibleDeadline: true,
      journeyId: 1,
      materials: [],
    },
    gradingSchemeEntity: {
      id: 10,
      name: 'Alphabetical (A-F)',
      code: 'ALPHABETICAL_A_F',

      isNonGradable(): boolean {
        return true;
      },
    },
    submissions: [
      {
        id: 6,
        value: 'Students comment',
        authors: [
          {
            id: 5,
            firstName: 'student',
            lastName: 'student',
            email: 'student@localhost.com',
            personRoles: [],
          },
        ],
        assignmentId: 1234,
      },
    ],
  };

  const feedback: ISubmissionFeedback = {
    grade: 'A',
    feedbackDate: '2020-09-07T09:24:00Z',
    value: 'teachers feedback',
    studentId: '5',
  };

  const submissionFeedback: IMessage = {
    value: 'teachers feedback',
  };

  const feedbackFile: IUploadedFile = {
    id: 2,
    uniqueName: 'unique name',
    originalName: 'original name',
    type: 'type',
    extension: 'pdf',
  };

  const providerMockData: IMockSubmissionGradingProvider = {
    submissionFeedback,
    feedbackFiles: [feedbackFile],
    selectedCriterionLevels: undefined,
    selectedFeedback: feedback,
  };

  const assignmentContext = {
    assignment: undefined,
    milestone: undefined,
    isAllowedToModify: true,
    journey: {
      id: 1,
      teachers: [
        {
          user: {
            authorities: ['ROLE_TEACHER'],
            email: 'student@localhost.com',
            firstName: 'teacher',
            id: 7,
            lastName: 'teacher',
          },
        },
      ],
    },
  };

  function component() {
    return {
      ...render(
        <ContextProvider contextProvider={MockEntityDetailProvider} store={store} props={contextProps}>
          <RootProvider>
            <EntityUpdateProvider>
              <AssignmentContext.Provider value={assignmentContext}>
                <QueryClientProvider client={queryClient}>
                  <ThroughProvider>
                    <MockSubmissionGradingProvider {...providerMockData}>
                      <SubmissionFeedbackBody assignment={contextProps.entity} />
                    </MockSubmissionGradingProvider>
                  </ThroughProvider>
                </QueryClientProvider>
              </AssignmentContext.Provider>
            </EntityUpdateProvider>
          </RootProvider>
        </ContextProvider>
      ),
      store,
    };
  }

  it('Should display markdown editor with previous feedback', async () => {
    const { container } = component();
    await waitFor(() => {
      const editor = container.querySelector('div[id="simplemde-editor-1-wrapper"]');
      expect(editor).toBeDefined();
      screen.getByDisplayValue('teachers feedback');
    });
  });

  it('Should display file upload component with existing file', async () => {
    const { container } = component();
    await waitFor(() => {
      const existingFileContainer = container.querySelector('ul[class="block-list mt-1 material-listd-flex"], [role="list"]');
      expect(existingFileContainer).toBeDefined();
      screen.getByText('original name');

      const uppyComponent = container.querySelector('div[class="uppy-Root uppy-Dashboard uppy-Dashboard--animateOpenClose"]');
      expect(uppyComponent).toBeDefined();
    });
  });

  it('Should display resubmittable checkbox with title', async () => {
    const { container } = component();
    await waitFor(() => {
      const checkbox = container.querySelector('input[class="form-check-input"], [type="checkbox"]');
      expect(checkbox).toBeDefined();
      screen.getByText('schoolabyApp.submission.submissionInfo.resubmittable');
    });
  });

  it('Should show assessment rubric button and form items when clicked', async () => {
    component();
    const rubricButton = await screen.findByText('schoolabyApp.assignment.rubric.viewTitle');
    fireEvent.click(rubricButton);

    const dialog = within(await screen.findByTestId('rubric-overview'));
    await dialog.findByText('Test rubric');

    expect(await dialog.findAllByRole('radio')).toHaveLength(4);
    expect(await dialog.findAllByRole('textbox')).toHaveLength(4);

    await dialog.findByText('entity.action.cancel');
    await dialog.findByText('entity.action.save');
  });

  it('Should show validation errors when hitting submit in rubric overview dialog', async () => {
    component();
    const rubricButton = await screen.findByText('schoolabyApp.assignment.rubric.viewTitle');
    fireEvent.click(rubricButton);

    await waitFor(() => {
      const dialog = within(screen.getByRole('dialog'));
      fireEvent.click(dialog.getByText('entity.action.save'));

      expect(dialog.getAllByText('schoolabyApp.submission.rubricSelectionRequired')).toHaveLength(2);
    });
  });

  it('Should show criterion titles and default points per criterion', async () => {
    component();

    await screen.findByText('Criterion 1');
    await screen.findByText('Criterion 2');
    await screen.findByText((content, node) => multipleElementsTextHelper(node, '0 / 5'));
    await screen.findByText((content, node) => multipleElementsTextHelper(node, '0 / 3'));
  });

  it('Should show total points title and default total points', async () => {
    component();

    await screen.findByText('schoolabyApp.assignment.rubric.assessmentRubricTotalPoints');
    await screen.findByText((content, node) => multipleElementsTextHelper(node, '0 / 8'));
  });

  it('Should show correct points title and description per criterion', async () => {
    providerMockData.selectedCriterionLevels = createSelectedCriterionLevels();
    component();

    await screen.findByText((content, node) => multipleElementsTextHelper(node, '5 / 5'));
    await screen.findByText((content, node) => multipleElementsTextHelper(node, '1 / 3'));
    await screen.findByText((content, node) => multipleElementsTextHelper(node, '6 / 8'));
    expect(await screen.findAllByText('Modified description')).toHaveLength(2);
  });
});
