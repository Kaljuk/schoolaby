import React from 'react';
import { screen, render } from '@testing-library/react';
import FeedbackHistory from 'app/assignment/assignment-detail/students/feedback-history/feedback-history';
import { IAssignment } from 'app/shared/model/assignment.model';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { QueryClientProvider, QueryClient } from 'react-query';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { RootProvider } from 'app/shared/contexts/root-context';

interface FeedbackHistoryComponentProps {
  assignment: IAssignment;
  submission: ISubmission;
  studentId: number;
  isAllowedToModify: boolean;
}

describe('Feedback history', () => {
  const mockStore = configureMockStore();

  const createDefaultSubmission = () => ({
    id: 6,
  });

  const createDefaultAssignment = () => ({
    id: 1234,
  });

  const createDefaultProps = (): FeedbackHistoryComponentProps => ({
    assignment: createDefaultAssignment(),
    submission: createDefaultSubmission(),
    studentId: 5,
    isAllowedToModify: true,
  });

  const component = ({ assignment, submission, studentId, isAllowedToModify }: FeedbackHistoryComponentProps = createDefaultProps()) => {
    render(
      <Provider store={mockStore()}>
        <RootProvider>
          <QueryClientProvider client={new QueryClient()}>
            <AssignmentContext.Provider value={{ assignment, isAllowedToModify, journey: {} }}>
              <FeedbackHistory submission={submission} studentId={studentId} />
            </AssignmentContext.Provider>
          </QueryClientProvider>
        </RootProvider>
      </Provider>
    );
  };

  it('should show feedback history', async () => {
    component();

    expect(await screen.findAllByText(/schoolabyApp.submission.feedbackGiven/)).toHaveLength(2);
    await screen.findByText(/04.02.2022/);
    await screen.findByText(/05.02.2022/);

    await screen.findByText(/schoolabyApp\.assignment\.detail\.grade\W+B/);
    await screen.findByText('Feedback value');
    await screen.findByText('Feedback value 2');
    await screen.findByText('Feedback file 1');
    await screen.findByText('Feedback file 2');
  });

  it('should show feedback history title when isAllowedToModify is false', async () => {
    component({ ...createDefaultProps(), isAllowedToModify: false });
    await screen.findByRole('heading', { name: 'schoolabyApp.submission.teachersFeedback' });
  });
});
