import { AUTHORITIES } from 'app/config/constants';
import GradeDistributionBarChart from 'app/assignment/assignment-detail/analytics/grade-distribution-bar-chart';
import React from 'react';
import { RootProvider } from 'app/shared/contexts/root-context';
import { Provider } from 'react-redux';
import { render, screen } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';

describe('GradeDistributionBarChart', () => {
  let store: any;
  const mockStore = configureMockStore();

  const gradeDistributionBarChartProps = {
    students: [{}, {}, {}],
    journey: {},
    gradingScheme: {
      values: [
        {
          grade: '1',
          percentageRangeStart: 0,
          percentageRangeEnd: 50,
        },
        {
          grade: '3',
          percentageRangeStart: 50,
          percentageRangeEnd: 75,
        },
        {
          grade: '5',
          percentageRangeStart: 75,
          percentageRangeEnd: 100,
        },
      ],
    },
    submissions: [
      {
        submissionFeedbacks: [
          {
            grade: '1',
            feedbackDate: '',
          },
        ],
      },
      {
        submissionFeedbacks: [
          {
            grade: '3',
            feedbackDate: '',
          },
        ],
      },
      {
        submissionFeedbacks: [
          {
            grade: '5',
            feedbackDate: '',
          },
        ],
      },
    ],
  };

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
    };
    store = mockStore(initialState);
  });

  it('Should render BarChartFooter, BarChartHeader and BarChart', () => {
    render(
      <RootProvider>
        <Provider store={store}>
          <GradeDistributionBarChart {...gradeDistributionBarChartProps} />
        </Provider>
      </RootProvider>
    );

    screen.getByText('schoolabyApp.assignment.analytics.barChart.title');
    screen.getByText('schoolabyApp.assignment.analytics.barChart.footer:');
    screen.getByText('75 / 100%');
    const barNumbers = screen.getAllByLabelText('schoolabyApp.assignment.analytics.barChart.barNumberLabel');
    expect(barNumbers.length).toBe(3);
    const studentCount = screen.getByLabelText('schoolabyApp.assignment.analytics.barChart.studentCount');
    expect(studentCount.textContent).toBe('3');
  });
});
