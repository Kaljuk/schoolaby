import React from 'react';
import configureMockStore from 'redux-mock-store';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { AUTHORITIES } from 'app/config/constants';
import { QueryClient, QueryClientProvider } from 'react-query';
import MockEntityDetailProvider, { IMockEntityDetailProvider } from '../../../../../helper/context-mock/mock-entity-detail-provider';
import ContextProvider from '../../../../../helper/context-mock/context-provider';
import SubmissionStudentList from 'app/assignment/assignment-detail/submission/submission-grading-body/submission-student-list';
import MockSubmissionGradingProvider, {
  IMockSubmissionGradingProvider,
} from '../../../../../helper/context-mock/mock-submission-grading-provider';

const mockStore = configureMockStore();
const queryClient = new QueryClient();

describe('Submission student list', () => {
  let store: any;

  beforeEach(() => {
    const initialState = {
      isAllowedToModify: true,
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
      applicationProfile: {
        inProduction: true,
      },
    };
    store = mockStore(initialState);
  });

  const students = [
    {
      id: 5,
      firstName: 'student',
      lastName: 'student',
      email: 'student@localhost.com',
      personRoles: [],
    },
    {
      id: 6,
      firstName: 'student2',
      lastName: 'student2',
      email: 'student2@localhost.com',
      personRoles: [],
    },
    {
      id: 7,
      firstName: 'student3',
      lastName: 'student3',
      email: 'student3@localhost.com',
      personRoles: [],
    },
    {
      id: 8,
      firstName: 'student4',
      lastName: 'student4',
      email: 'student4@localhost.com',
      personRoles: [],
    },
  ];

  const studentsForSorting = [
    {
      firstName: 'Aaria',
      lastName: 'B',
    },
    {
      firstName: 'Benny',
      lastName: 'A',
    },
  ];

  const contextProps: IMockEntityDetailProvider = {
    selectedUser: {
      id: 5,
      login: 'student',
      firstName: 'student',
      lastName: 'student',
      email: 'student@localhost.com',
      personRoles: [],
      authorities: ['ROLE_STUDENT'],
    },
    entity: {
      id: 1234,
      title: 'Assignment title',
      description: 'Description text',
      gradingSchemeId: 10,
      deadline: '2020-09-07T09:24:00Z',
      flexibleDeadline: true,
      journeyId: 1,
      materials: [],
    },
    gradingSchemeEntity: {
      id: 10,
      name: 'Alphabetical (A-F)',
      code: 'ALPHABETICAL_A_F',

      isNonGradable(): boolean {
        return true;
      },
    },
    submissions: [
      {
        id: 6,
        value: 'Students comment',
        submittedForGradingDate: '2020-09-06T09:24:00Z',
        authors: [
          {
            id: 5,
            firstName: 'student',
            lastName: 'student',
            email: 'student@localhost.com',
            personRoles: [],
          },
        ],
        assignmentId: 1234,
      },
    ],
  };

  const submissionGradingProps: IMockSubmissionGradingProvider = {
    submittedSubmissions: [],
  };

  function component() {
    return {
      ...render(
        <ContextProvider contextProvider={MockEntityDetailProvider} store={store} props={contextProps}>
          <QueryClientProvider client={queryClient}>
            <ContextProvider contextProvider={MockSubmissionGradingProvider} store={store} props={submissionGradingProps}>
              <SubmissionStudentList />
            </ContextProvider>
          </QueryClientProvider>
        </ContextProvider>
      ),
      store,
    };
  }

  it('Should display all students in a list with avatar name initials', async () => {
    component();
    await waitFor(() => {
      screen.getByText('Student1 Student1');
      screen.getByText('Student2 Student2');
      expect(screen.getAllByText('SS')).toHaveLength(2);
    });
  });

  it('Should display grade container next to student name', async () => {
    const { container } = component();
    await waitFor(() => {
      screen.getByText('Student1 Student1');
      const checkmark = container.querySelector('svg[class="icon-wrapper"], [aria-label="checkmark"]');
      expect(checkmark).toBeDefined();
    });
  });

  it('Should display exclamation mark next to all submission authors', async () => {
    contextProps.submissions[0].authors.push({
      id: 6,
      firstName: 'student2',
      lastName: 'student2',
      email: 'student2@localhost.com',
      personRoles: [],
    });
    contextProps.entity.students = students;
    contextProps.gradingSchemeEntity.isNonGradable = () => false;
    submissionGradingProps.submittedSubmissions = contextProps.submissions;
    component();

    await waitFor(() => {
      screen.getByText('Student Student');
      screen.getByText('Student2 Student2');
      screen.getByText('Student3 Student3');
      screen.getByText('Student4 Student4');
      expect(screen.getAllByText('SS')).toHaveLength(4);
      const checkmarks = screen.getAllByLabelText('exclamation');
      expect(checkmarks).toHaveLength(2);
    });
  });

  it('should display sort dropdown menu', () => {
    component();

    screen.getByLabelText('schoolabyApp.assignment.detail.sortDropdown');

    // DropdownItems are visible for tests without toggling dropdown
    screen.getByText('schoolabyApp.assignment.detail.sortByFirstName');
    screen.getByText('schoolabyApp.assignment.detail.sortByLastName');
  });

  it('should sort by first name', () => {
    contextProps.entity.students = studentsForSorting;
    component();

    const sortByFirstName = screen.getByText('schoolabyApp.assignment.detail.sortByFirstName');
    fireEvent.click(sortByFirstName);

    const studentNames = screen.getAllByTestId('user-fullname');

    expect(studentNames.length).toBe(2);
    expect(studentNames[0]).toHaveTextContent('Aaria B');
    expect(studentNames[1]).toHaveTextContent('Benny A');
  });

  it('should sort by last name', () => {
    contextProps.entity.students = studentsForSorting;
    component();

    const sortByLastName = screen.getByText('schoolabyApp.assignment.detail.sortByLastName');
    fireEvent.click(sortByLastName);

    const studentNames = screen.getAllByTestId('user-fullname');

    expect(studentNames.length).toBe(2);
    expect(studentNames[0]).toHaveTextContent('Benny A');
    expect(studentNames[1]).toHaveTextContent('Aaria B');
  });
});
