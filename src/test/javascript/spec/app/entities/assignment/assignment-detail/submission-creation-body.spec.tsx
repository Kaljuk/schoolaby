import React from 'react';
import configureMockStore from 'redux-mock-store';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import ContextProvider from '../../../../helper/context-mock/context-provider';
import MockEntityDetailProvider from '../../../../helper/context-mock/mock-entity-detail-provider';
import { AUTHORITIES } from 'app/config/constants';
import { scoreGradable } from '../../../../rtl-setup';
import { TranslatorContext } from 'react-jhipster';
import { ChatProvider } from 'app/shared/contexts/chat-context';
import { createMemoryHistory } from 'history';
import { DateTime } from 'luxon';
import { QueryClient, QueryClientProvider } from 'react-query';
import SubmissionCreationBody from 'app/assignment/assignment-detail/submission/submission-creation-body/submission-creation-body';
import { SubmissionGradingProvider } from 'app/shared/contexts/submission-grading-context';
import { RootProvider } from 'app/shared/contexts/root-context';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';

const queryClient = new QueryClient();

// For SimpleMDE editor, since it is using JSDOM
const mockCreateRange = () => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
  // @ts-ignore
  document.createRange = function () {
    return {
      setEnd() {},
      setStart() {},
      getBoundingClientRect() {
        return { right: 0 };
      },
      getClientRects() {
        return {
          length: 0,
          left: 0,
          right: 0,
        };
      },
    };
  };
};

const overdueDeadlineByMinute = () => {
  const deadline = new Date(DateTime.local());
  deadline.setMinutes(deadline.getMinutes() - 1);
  return deadline.toISOString();
};

describe('Submission creation body', () => {
  let store: any;
  const mockStore = configureMockStore();
  const history = createMemoryHistory();

  const journeyStudents = [
    {
      authorities: ['ROLE_STUDENT'],
      email: 'student@localhost.com',
      firstName: 'student1',
      id: 5,
      lastName: 'student1',
    },
    {
      authorities: ['ROLE_STUDENT'],
      email: 'student@localhost.com',
      firstName: 'student2',
      id: 6,
      lastName: 'student2',
    },
  ];

  const group = {
    id: 1,
    name: 'Group 1',
    students: journeyStudents,
    submissions: [],
  };

  beforeEach(() => {
    mockCreateRange();
    TranslatorContext.registerTranslations('en', {});

    const initialState = {
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          id: journeyStudents[0].id,
          authorities: [AUTHORITIES.STUDENT],
        },
      },
      submission: {
        id: 123,
        assignmentId: 1234,
        groupId: group.id,
      },
      applicationProfile: {
        isInProduction: false,
      },
    };
    store = mockStore(initialState);
  });

  const createSubmissionBodyProps = {
    hasAuthoritySubmit: true,
    account: {},
    createSubmission: jest.fn(),
    updateSubmission: jest.fn(),
    journey: {
      id: 3,
      title: 'Journey title',
      students: journeyStudents,
      teachers: [
        {
          user: {
            authorities: ['ROLE_TEACHER'],
            email: 'student@localhost.com',
            firstName: 'teacher',
            id: 7,
            lastName: 'teacher',
          },
        },
      ],
    },
  };

  const assignment = {
    id: 1234,
    title: 'Assignment title',
    description: 'Description text',
    gradingSchemeId: 10,
    deadline: DateTime.fromISO('2020-09-07T09:24:00', { zone: 'local' }),
    flexibleDeadline: true,
    materials: [],
    requiresSubmission: true,
    groups: [],
  };

  const getEntityDetailProviderProps = () => {
    return {
      entity: assignment,
      submissions: [
        {
          id: 6,
          value: '<p>Students comment</p>',
          authors: [
            {
              id: 5,
              firstName: 'student',
              lastName: 'student',
              email: 'student@localhost.com',
              personRoles: [],
            },
          ],
          submissionFeedbacks: [
            {
              studentId: 5,
              grade: 'A',
              feedbackDate: '2020-06-31T10:52:03',
              selectedCriterionLevels: [],
            },
          ],
          resubmittable: false,
          assignmentId: 1234,
          submittedForGradingDate: '2020-06-30T10:52:03',
        },
      ],
    };
  };

  const assignmentContext = {
    assignment,
    milestone: undefined,
    isAllowedToModify: true,
    journey: {
      id: 1,
      teachers: [
        {
          user: {
            authorities: ['ROLE_TEACHER'],
            email: 'student@localhost.com',
            firstName: 'teacher',
            id: 7,
            lastName: 'teacher',
          },
        },
      ],
    },
  };

  const component = (entityDetailProviderProps = getEntityDetailProviderProps()) => (
    <RootProvider>
      <ContextProvider contextProvider={MockEntityDetailProvider} routeProps={{ history }} store={store} props={entityDetailProviderProps}>
        <EntityUpdateProvider>
          <QueryClientProvider client={queryClient}>
            <ChatProvider>
              <SubmissionGradingProvider>
                <AssignmentContext.Provider value={assignmentContext}>
                  <SubmissionCreationBody {...createSubmissionBodyProps} />
                </AssignmentContext.Provider>
              </SubmissionGradingProvider>
            </ChatProvider>
          </QueryClientProvider>
        </EntityUpdateProvider>
      </ContextProvider>
    </RootProvider>
  );

  it('Should display submission creation body with assignment information', async () => {
    render(component());
    await screen.findByText('Description text');
    await screen.findByText('07.09.2020 09:24');
    await screen.findAllByText('Assignment title');
  });

  it('Should display submission submit button as enabled if submission has been made and teacher has not given feedback', async () => {
    const entityDetailProviderProps = getEntityDetailProviderProps();
    entityDetailProviderProps.submissions[0].submissionFeedbacks = [];
    render(component(entityDetailProviderProps));

    expect(await screen.findByRole('submit')).toBeDefined();
    expect(await screen.findByRole('submit')).not.toHaveAttribute('disabled');
  });

  it('Should display submission data if teacher has given feedback', async () => {
    const entityDetailProviderProps = getEntityDetailProviderProps();
    entityDetailProviderProps.submissions[0].submissionFeedbacks = [
      {
        studentId: 5,
        grade: 'A',
        feedbackDate: '2020-06-31T10:52:03',
        selectedCriterionLevels: [],
      },
    ];
    render(component(entityDetailProviderProps));

    await screen.findByText('30.06.2020');
  });

  it('Should show only gradable lti results', async () => {
    const entityDetailProviderProps = getEntityDetailProviderProps();
    entityDetailProviderProps.submissions[0].submittedForGradingDate = null;
    render(component(entityDetailProviderProps));

    const gradableElement = await screen.findByText(scoreGradable.title);
    expect(gradableElement);
  });

  it('Should show submission creation with previous feedback', async () => {
    const entityDetailProviderProps = getEntityDetailProviderProps();
    entityDetailProviderProps.submissions[0].submissionFeedbacks = [
      {
        studentId: 5,
        grade: 'A',
        feedbackDate: '2020-06-31T10:52:03',
        selectedCriterionLevels: [],
      },
    ];
    entityDetailProviderProps.submissions[0].submittedForGradingDate = '2020-06-30T10:52:03';
    render(component(entityDetailProviderProps));

    expect(await screen.findByText('30.06.2020'));
    expect(await screen.findAllByText(string => string.includes('schoolabyApp.submission.feedback'))).toBeDefined();
    expect(await screen.findAllByText(string => string.includes('schoolabyApp.submission.feedbackDate'))).toBeDefined();
  });

  it('Should show submit button as enabled when assignment not flexible and not overdue', async () => {
    const entityDetailProviderProps = getEntityDetailProviderProps();
    const deadline = new Date(DateTime.local());
    deadline.setMinutes(deadline.getMinutes() + 1);
    entityDetailProviderProps.submissions[0].resubmittable = false;
    entityDetailProviderProps.submissions[0].submissionFeedbacks = [];
    assignment.deadline = deadline.toISOString();

    render(component(entityDetailProviderProps));

    const submitButton = screen.getByRole('submit');

    await waitFor(() => {
      expect(submitButton).toBeEnabled();
    });
  });

  it('Should show submit button as disabled when assignment not flexible and submission not resubmittable and overdue', async () => {
    assignment.flexibleDeadline = false;
    assignment.deadline = overdueDeadlineByMinute();

    render(component());

    const submitButton = screen.getByRole('submit');

    await waitFor(() => {
      expect(submitButton).toBeDisabled();
    });
  });

  it('Should show submit button as disabled when assignment not flexible and submission not resubmittable and not overdue', async () => {
    assignment.flexibleDeadline = false;

    render(component());

    const submitButton = screen.getByRole('submit');

    await waitFor(() => {
      expect(submitButton).toBeDisabled();
    });
  });

  it('Should show submit button as disabled when assignment flexible and submission not resubmittable and not overdue', async () => {
    render(component());

    const submitButton = screen.getByRole('submit');

    await waitFor(() => {
      expect(submitButton).toBeDisabled();
    });
  });

  it('Should show submit button as disabled when assignment not flexible and feedbackDate missing and overdue', async () => {
    assignment.deadline = overdueDeadlineByMinute();
    assignment.flexibleDeadline = false;

    render(component());

    const submitButton = screen.getByRole('submit');

    await waitFor(() => {
      expect(submitButton).toBeDisabled();
    });
  });

  it('Should show submit button as enabled when assignment flexible and submission resubmittable and overdue', async () => {
    const entityDetailProviderProps = getEntityDetailProviderProps();
    entityDetailProviderProps.submissions[0].resubmittable = true;
    assignment.deadline = overdueDeadlineByMinute();

    render(component(entityDetailProviderProps));

    const submitButton = screen.getByRole('submit');

    await waitFor(() => {
      expect(submitButton).toBeEnabled();
    });
  });

  it('Should show submit button as enabled when assignment has flexible deadline and is overdue and student has not submitted a solution yet', async () => {
    const entityDetailProviderProps = getEntityDetailProviderProps();
    entityDetailProviderProps.submissions = [];
    assignment.flexibleDeadline = true;
    assignment.deadline = overdueDeadlineByMinute();

    render(component(entityDetailProviderProps));

    const submitButton = screen.getByRole('submit');

    await waitFor(() => {
      expect(submitButton).toBeEnabled();
    });
  });

  it('Should show submit button as enabled when assignment not flexible and feedbackDate missing and overdue', async () => {
    const entityDetailProviderProps = getEntityDetailProviderProps();
    entityDetailProviderProps.submissions[0].resubmittable = true;
    assignment.deadline = overdueDeadlineByMinute();

    render(component(entityDetailProviderProps));

    const submitButton = screen.getByRole('submit');

    await waitFor(() => {
      expect(submitButton).toBeEnabled();
    });
  });

  it('Should display navigate to chat button and navigate to chats with correct params', async () => {
    const entityDetailProviderProps = getEntityDetailProviderProps();
    history.push(`/assignment/1234?journeyId=${createSubmissionBodyProps.journey.id}`);
    entityDetailProviderProps.submissions[0].resubmittable = true;
    entityDetailProviderProps.submissions[0].submittedForGradingDate = '2020-06-30T10:52:03';
    history.push = jest.fn();
    render(component(entityDetailProviderProps));

    const button = await screen.findByText('schoolabyApp.submission.askMore');
    expect(button);

    fireEvent.click(button);

    await waitFor(() => {
      expect(history.push).toBeCalledWith(
        `/chats?journeyId=${createSubmissionBodyProps.journey.id}&submissionId=${entityDetailProviderProps.submissions[0].id}`
      );
    });
  });

  it('Should display assessment rubric button and rubric overview', async () => {
    render(component());

    const rubricButton = await screen.findByText('schoolabyApp.assignment.rubric.viewTitle');
    fireEvent.click(rubricButton);
    await screen.findByText('Test rubric');
    expect(await screen.findAllByText('Level 1')).toHaveLength(2);
    expect(await screen.findAllByText('Level 2')).toHaveLength(2);
    await screen.findByText('Criterion 1');
    await screen.findByText('Criterion 2');
    expect(await screen.findAllByText('Criterion description')).toHaveLength(2);
    expect(await screen.findAllByText('Level description')).toHaveLength(4);
    await screen.findByText('global.close');
  });

  it('Should not show group members when is not group assignment', async () => {
    render(component());

    await waitFor(() => {
      expect(screen.queryByText('schoolabyApp.assignment.groupAssignment')).not.toBeInTheDocument();
      expect(screen.queryByText(/schoolabyApp.assignment.yourGroup/)).not.toBeInTheDocument();
    });
  });

  it('Should show group members when is group assignment', async () => {
    assignment.id = 123456;
    assignment.groups = [group];
    render(component());

    await screen.findByText('schoolabyApp.assignment.groupAssignment');
    await screen.findByText('schoolabyApp.assignment.yourGroup:');
    await screen.findByText('Group 1 - Student2 Student2');
  });

  it('Should show submit button with text "send group answer" when assignment is group assignment', async () => {
    assignment.groups = [group];
    render(component());

    await screen.findByText('schoolabyApp.assignment.detail.sendGroupAnswer');
  });

  it('Should show submit button with text "send answer" when assignment is not group assignment', async () => {
    assignment.groups = [];
    render(component());

    await screen.findByText('schoolabyApp.assignment.detail.sendAnswer');
  });

  it('Should show success modal on submit', async () => {
    const entityDetailProviderProps = getEntityDetailProviderProps();
    const deadline = new Date(DateTime.local());
    deadline.setMinutes(deadline.getMinutes() + 1);
    assignment.deadline = deadline.toISOString();
    entityDetailProviderProps.submissions = [];

    render(component(entityDetailProviderProps));

    const submit = screen.getByText('schoolabyApp.assignment.detail.sendAnswer');
    expect(submit).toBeEnabled();
    fireEvent.click(submit);

    await screen.findByText('schoolabyApp.submission.goodWork');
    await screen.findByText('schoolabyApp.submission.submitSuccess');
  });

  it('should show submission details when a submission is created and deadline passed and deadline not flexible', async () => {
    const deadline = new Date(DateTime.local());
    deadline.setDate(deadline.getDate() - 1);
    assignment.deadline = deadline.toISOString();
    assignment.flexibleDeadline = false;

    render(component());

    await screen.findByText('schoolabyApp.submission.submissionInfo.yourSubmission');
    await screen.findByText(/Students comment/);
  });

  it('should show deadline passed message when no submission created and deadline passed and deadline not flexible', async () => {
    const entityDetailProviderProps = { ...getEntityDetailProviderProps(), submissions: [] };
    const deadline = new Date(DateTime.local());
    deadline.setDate(deadline.getDate() - 1);
    assignment.deadline = deadline.toISOString();
    assignment.flexibleDeadline = false;

    render(component(entityDetailProviderProps));

    await screen.findByText('schoolabyApp.assignment.detail.deadlinePassed');
  });
});
