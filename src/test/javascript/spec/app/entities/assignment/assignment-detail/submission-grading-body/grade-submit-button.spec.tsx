import React from 'react';
import { render, screen } from '@testing-library/react';
import MockEntityDetailProvider from '../../../../../helper/context-mock/mock-entity-detail-provider';
import { GradeSubmitButton } from 'app/assignment/assignment-detail/submission/submission-grading-body/grade-submit-button';
import { GradingScheme, NUMERICAL_1_12 } from 'app/shared/model/grading-scheme.model';
import { SubmissionGradingProvider } from 'app/shared/contexts/submission-grading-context';

describe('Grade submit button', () => {
  const renderComponent = gradingScheme =>
    render(
      <MockEntityDetailProvider gradingSchemeEntity={gradingScheme}>
        <SubmissionGradingProvider>
          <GradeSubmitButton />
        </SubmissionGradingProvider>
      </MockEntityDetailProvider>
    );

  it('should show max possible score', async () => {
    const gradingScheme = new GradingScheme();
    gradingScheme.code = NUMERICAL_1_12;

    renderComponent(gradingScheme);

    await screen.findByText('/ 12');
  });
});
