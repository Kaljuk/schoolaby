import React from 'react';
import configureMockStore from 'redux-mock-store';
import { render, screen, waitFor } from '@testing-library/react';
import { AUTHORITIES } from 'app/config/constants';
import { RootProvider } from 'app/shared/contexts/root-context';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ThroughProvider } from 'react-through';
import MockEntityDetailProvider, { IMockEntityDetailProvider } from '../../../../../helper/context-mock/mock-entity-detail-provider';
import ContextProvider from '../../../../../helper/context-mock/context-provider';
import MockSubmissionGradingProvider from '../../../../../helper/context-mock/mock-submission-grading-provider';
import SubmissionGradingDetailsView from 'app/assignment/assignment-detail/submission/submission-grading-body/submission-grading-details-view';
import { ISubmission } from 'app/shared/model/submission/submission.model';
import { ILtiResource } from 'app/shared/model/lti-resource.model';
import MockMaterialProvider from '../../../../../helper/context-mock/mock-material-provider';
import { Material } from 'app/shared/model/material.model';

const mockStore = configureMockStore();
const queryClient = new QueryClient();

describe('Student submission details', () => {
  let store: any;

  beforeEach(() => {
    const initialState = {
      isAllowedToModify: true,
      locale: {
        currentLocale: 'en',
      },
      authentication: {
        account: {
          authorities: [AUTHORITIES.TEACHER],
        },
      },
      applicationProfile: {
        inProduction: false,
      },
    };
    store = mockStore(initialState);
  });

  const ltiResource: ILtiResource = {
    resourceLinkId: 'resource link Id',
    title: 'LTI title',
    description: 'LTI description',
    ltiApp: {
      clientId: undefined,
      name: 'app name',
      id: 14,
      description: 'app description',
      launchUrl: 'default url',
      version: '1.3.1',
      imageUrl: 'image url',
    },
  };

  const contextProps: IMockEntityDetailProvider = {
    selectedUser: {
      id: 5,
      login: 'student',
      firstName: 'student',
      lastName: 'student',
      email: 'student@localhost.com',
      personRoles: [],
      authorities: ['ROLE_STUDENT'],
    },
    entity: {
      id: 1234,
      title: 'Assignment title',
      description: 'Description text',
      gradingSchemeId: 10,
      deadline: '2020-09-07T09:24:00Z',
      flexibleDeadline: true,
      journeyId: 1,
      materials: [Material.fromLtiResource(ltiResource)],
    },
    gradingSchemeEntity: {
      id: 10,
      name: 'Alphabetical (A-F)',
      code: 'ALPHABETICAL_A_F',

      isNonGradable(): boolean {
        return true;
      },
    },
    submissions: [
      {
        id: 6,
        value: 'Students comment',
        submissionFeedbacks: [
          {
            grade: 'A',
            feedbackDate: '2020-09-07T09:24:00Z',
            studentId: '5',
          },
        ],
        authors: [
          {
            id: 5,
            firstName: 'student',
            lastName: 'student',
            email: 'student@localhost.com',
            personRoles: [],
          },
        ],
        assignmentId: 1234,
      },
    ],
  };

  const studentSubmission: ISubmission = {
    id: 6,
    value: 'Students comment',
    submissionFeedbacks: [
      {
        grade: 'A',
        feedbackDate: '2020-09-07T09:24:00Z',
        studentId: '5',
      },
    ],
    submittedForGradingDate: '2020-09-07T09:24:00Z',
    authors: [
      {
        id: 5,
        firstName: 'student',
        lastName: 'student',
        email: 'student@localhost.com',
        personRoles: [],
      },
    ],
    assignmentId: 1234,
  };

  function component() {
    return {
      ...render(
        <ContextProvider contextProvider={MockEntityDetailProvider} store={store} props={contextProps}>
          <RootProvider>
            <QueryClientProvider client={queryClient}>
              <ThroughProvider>
                <MockMaterialProvider materials={[Material.fromLtiResource(ltiResource)]}>
                  <MockSubmissionGradingProvider ltiResources={[ltiResource]}>
                    <SubmissionGradingDetailsView submission={studentSubmission} isAllowedToModify />
                  </MockSubmissionGradingProvider>
                </MockMaterialProvider>
              </ThroughProvider>
            </QueryClientProvider>
          </RootProvider>
        </ContextProvider>
      ),
      store,
    };
  }

  it('Should display students submission information', async () => {
    component();
    await waitFor(() => {
      screen.getByText('Student Student schoolabyApp.submission.submissionInfo.studentSubmission');
      screen.getByText('Students comment');
      screen.getByText('schoolabyApp.submission.submissionInfo.studentAssets');
    });
  });

  it('Should display submission LTI data', async () => {
    component();
    await screen.findByText('LTI title');
    await screen.findByText('LTI description');
  });
});
