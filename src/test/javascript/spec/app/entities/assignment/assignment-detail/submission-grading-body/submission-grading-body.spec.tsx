import React from 'react';
import configureMockStore from 'redux-mock-store';
import { queryByAttribute, render, screen, waitFor } from '@testing-library/react';
import { AUTHORITIES } from 'app/config/constants';
import { QueryClient, QueryClientProvider } from 'react-query';
import { createBrowserHistory } from 'history';
import { MemoryRouter } from 'react-router-dom';
import { SubmissionGradingProvider } from 'app/shared/contexts/submission-grading-context';
import {
  ISubmissionGradingBody,
  SubmissionGradingBody,
} from 'app/assignment/assignment-detail/submission/submission-grading-body/submission-grading-body';
import MockEntityDetailProvider, { IMockEntityDetailProvider } from '../../../../../helper/context-mock/mock-entity-detail-provider';
import ContextProvider from '../../../../../helper/context-mock/context-provider';
import { MaterialProvider } from 'app/shared/contexts/material-context';
import { DateTime } from 'luxon';
import * as MockedMarkdownEditor from 'app/assignment/assignment-detail/submission/shared/markdown-editor';
import { RootProvider } from 'app/shared/contexts/root-context';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';

const mockStore = configureMockStore();
const history = createBrowserHistory();
const queryClient = new QueryClient();

describe('Submission grading body', () => {
  jest.spyOn(MockedMarkdownEditor, 'MarkdownEditor').mockImplementation(() => <input />);

  const props: ISubmissionGradingBody = {
    isAllowedToModify: true,
    account: {
      authorities: [AUTHORITIES.TEACHER],
    },
  };

  const contextProps: IMockEntityDetailProvider = {
    entity: {
      id: 1234,
      title: 'Assignment title',
      description: 'Description text',
      gradingSchemeId: 10,
      deadline: DateTime.fromISO('2020-09-07T09:24:00', { zone: 'local' }),
      flexibleDeadline: true,
      journeyId: 1,
      materials: [],
    },
    submissions: [
      {
        id: 6,
        value: 'Students comment',
        submittedForGradingDate: '2020-09-07T09:24:00Z',
        authors: [
          {
            id: 5,
            firstName: 'student',
            lastName: 'student',
            email: 'student@localhost.com',
            personRoles: [],
          },
        ],
        submissionFeedbacks: [],
        assignmentId: 1234,
      },
    ],
  };

  const initialState = {
    authentication: {
      account: {
        authorities: [AUTHORITIES.TEACHER],
      },
    },
  };

  const component = (isStudentSelected?: boolean) => ({
    ...render(
      <RootProvider>
        <ContextProvider
          contextProvider={MockEntityDetailProvider}
          routeProps={{ history }}
          store={mockStore(initialState)}
          props={contextProps}
        >
          <EntityUpdateProvider>
            <MaterialProvider>
              <QueryClientProvider client={queryClient}>
                {isStudentSelected ? (
                  <MemoryRouter initialEntries={['/assignment/1234?journeyId=1&studentId=5']}>
                    <SubmissionGradingProvider>
                      <SubmissionGradingBody {...props} />
                    </SubmissionGradingProvider>
                  </MemoryRouter>
                ) : (
                  <SubmissionGradingProvider>
                    <SubmissionGradingBody {...props} />
                  </SubmissionGradingProvider>
                )}
              </QueryClientProvider>
            </MaterialProvider>
          </EntityUpdateProvider>
        </ContextProvider>
      </RootProvider>
    ),
  });

  it('Should show icon when submission resubmitted by a student', async () => {
    component();

    await waitFor(() => {
      screen.getByText('Student1 Student1');
    });
    screen.getByRole('student-submitted-icon-span');
  });

  it('Should not show icon when submission is graded', async () => {
    contextProps.submissions[0].submittedForGradingDate = '2020-06-30T10:52:03';
    contextProps.submissions[0].submissionFeedbacks.push({
      studentId: '5',
      grade: 'A',
      feedbackDate: '2020-09-07T09:24:00Z',
      value: '',
    });

    component();

    await waitFor(() => {
      screen.getByText('Student1 Student1');
    });
    expect(screen.queryByRole('student-submitted-icon-span')).toBeNull();
  });

  it('Should not show icon when no submission has been made', async () => {
    contextProps.submissions[0].submittedForGradingDate = undefined;

    component();
    await waitFor(() => {
      screen.getByText('Student1 Student1');
    });
    expect(screen.queryByRole('student-submitted-icon-span')).toBeNull();
  });

  it('Should show icon when submission is not graded', async () => {
    contextProps.submissions[0].submittedForGradingDate = '2020-06-30T10:52:03';
    contextProps.submissions[0].submissionFeedbacks = [];

    component();
    await waitFor(() => {
      screen.getByText('Student1 Student1');
      screen.getByRole('student-submitted-icon-span');
    });
  });

  it('Should display submission grading body with assignment information', async () => {
    component();
    await waitFor(() => {
      screen.getByText('Description text');
      screen.getByText('07.09.2020 09:24');
      screen.getAllByText('Assignment title');
    });
  });

  it('Should display submission grading body button', async () => {
    const { container } = component();
    const getById = queryByAttribute.bind(null, 'id');
    await waitFor(() => {
      screen.getByRole('contentinfo');
      expect(getById(container, 'submission-submit')).toHaveAttribute('disabled');
    });
  });

  it('Should display submission data when student selected', async () => {
    contextProps.selectedUser = {
      id: 5,
      firstName: 'student',
      lastName: 'student',
      email: 'student@localhost.com',
      personRoles: [],
    };
    contextProps.submissions[0].submittedForGradingDate = '2020-06-30T10:52:03';

    component(true);
    await waitFor(() => {
      screen.getByText('30.06.2020');
    });
  });

  it('Should display no submission data when no student selected', async () => {
    contextProps.selectedUser = undefined;
    component();
    await waitFor(() => {
      const date = screen.queryByText('30.06.2020');
      expect(date).not.toBeInTheDocument();
    });
  });

  it('Should open students submission when student id in url', async () => {
    history.push = jest.fn();
    contextProps.submissions[0].submittedForGradingDate = '2020-06-30T10:52:03';

    const { container } = component(true);
    await waitFor(() => {
      screen.getByText('Student1 Student1');
      const selectedStudent = container.querySelector('div[class="selected list-box-wrapper d-flex"]');
      expect(selectedStudent).toBeDefined();
      screen.getByText('30.06.2020');
    });
  });
});
