import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { IJourneyUpdateProps } from 'app/journey/journey-update';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { AUTHORITIES, ROLE } from 'app/config/constants';
import { RootProvider } from 'app/shared/contexts/root-context';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ThroughProvider } from 'react-through';
import { createBrowserHistory } from 'history';
import JourneyUpdate from 'app/journey/journey-update';

let search = '';

jest.mock('react-router-dom', () => ({
  __esModule: true,
  ...jest.requireActual('react-router-dom'),
  useLocation: () => ({
    search,
  }),
}));

describe('Creating or updating a journey', () => {
  let store: any;
  const mockStore = configureMockStore();
  const mockHistoryPush = jest.fn();
  const queryClient = new QueryClient();
  const history = {
    ...createBrowserHistory(),
    push: mockHistoryPush,
  };

  const journeyUpdateProps = (): IJourneyUpdateProps => ({
    locale: undefined,
    history,
    location: undefined,
    userPersonRoles: [
      {
        id: 1,
        role: ROLE.TEACHER,
        school: {
          id: 1,
          name: 'Tallinna Saksa Gümnaasium',
        },
      },
      {
        id: 2,
        role: ROLE.TEACHER,
        school: {
          id: 2,
          name: 'Tallinna 32. Keskkool',
        },
      },
    ],
    country: undefined,
    match: {
      params: {
        id: '1',
      },
      isExact: true,
      path: 'path',
      url: 'url',
    },
  });

  const getInitialState = () => ({
    locale: {
      currentLocale: 'et',
    },
    authentication: {
      account: {
        authorities: [AUTHORITIES.TEACHER],
        personRoles: [
          {
            id: 1,
            role: ROLE.TEACHER,
            school: {
              id: 1,
              name: 'Tallinna Saksa Gümnaasium',
            },
          },
          {
            id: 2,
            role: ROLE.TEACHER,
            school: {
              id: 2,
              name: 'Tallinna 32. Keskkool',
            },
          },
        ],
      },
    },
  });

  beforeEach(() => {
    store = mockStore(getInitialState());
  });

  const renderWithRedux = updateProps => {
    return render(
      <RootProvider>
        <Provider store={store}>
          <EntityUpdateProvider>
            <QueryClientProvider client={queryClient}>
              <ThroughProvider>
                <MemoryRouter>
                  <JourneyUpdate {...updateProps} />
                </MemoryRouter>
              </ThroughProvider>
            </QueryClientProvider>
          </EntityUpdateProvider>
        </Provider>
      </RootProvider>
    );
  };

  it('should show existing journey data', async () => {
    const updateProps = journeyUpdateProps();
    await waitFor(() => {
      renderWithRedux(updateProps);
    });

    await screen.findByDisplayValue('Journey title');
    await screen.findByDisplayValue('Matemaatika 8. klassile');
    expect(await screen.findByLabelText('Tallinna Saksa Gümnaasium')).not.toBeChecked();
    expect(await screen.findByLabelText('Tallinna 32. Keskkool')).not.toBeChecked();
    expect(await screen.findByLabelText('Tallinna Kadrioru Saksa Gümnaasium')).toBeChecked();
    await screen.findByDisplayValue('26/03/2021');
    await screen.findByDisplayValue('15/10/2020');
    expect(await screen.findByLabelText('I kooliaste')).toBeChecked();
    expect(await screen.findByLabelText('II kooliaste')).not.toBeChecked();
    expect(await screen.findByLabelText('III kooliaste')).not.toBeChecked();
    expect(await screen.findByLabelText('Gümnaasium')).not.toBeChecked();
    expect(await screen.findByLabelText('Täiendkoolitus')).not.toBeChecked();
    expect(await screen.findByLabelText('schoolabyApp.journey.curriculum.EXTRACURRICULAR')).toBeChecked();
    expect(await screen.findByLabelText('schoolabyApp.journey.curriculum.NATIONAL_CURRICULUM')).not.toBeChecked();

    expect(screen.queryByText('schoolabyApp.journey.noTeacherRoleYetMessage')).not.toBeInTheDocument();
  });

  it('should display form labels, titles and buttons', async () => {
    const updateProps = journeyUpdateProps();
    await waitFor(() => {
      renderWithRedux(updateProps);
    });

    await screen.findByLabelText('schoolabyApp.journey.changeCoverImage');
    await screen.findByText('schoolabyApp.journey.editJourney');
    await screen.findByLabelText('schoolabyApp.journey.journeyTitle');
    await screen.findByText('schoolabyApp.journey.journeyBeginning');
    await screen.findByText('schoolabyApp.journey.journeyEnding');
    await screen.findByLabelText('schoolabyApp.journey.videoConferenceLink');
    await screen.findByLabelText('schoolabyApp.journey.description');
    await screen.findByText('schoolabyApp.journey.selectEducationalInstitution');
    await screen.findByText('schoolabyApp.journey.educationalLevel');
    await screen.findByText('schoolabyApp.journey.subject');
    await screen.findByText('entity.action.cancel');
    await screen.findByText('entity.action.save');
  });

  it('should display a warning modal on cancel button click', async () => {
    const updateProps = journeyUpdateProps();
    await waitFor(() => {
      renderWithRedux(updateProps);
    });

    fireEvent.click(await screen.findByText('entity.action.cancel'));

    await screen.findByText('schoolabyApp.journey.warningModal.title');
    await screen.findByText('schoolabyApp.journey.warningModal.description');
    await screen.findByText('global.no');
    await screen.findByText('global.yes');
  });

  it('should add journey school to options in schools selection if not in person roles', async () => {
    const updateProps = journeyUpdateProps();
    renderWithRedux(updateProps);

    expect(await screen.findByLabelText('Tallinna Kadrioru Saksa Gümnaasium')).toBeChecked();

    expect(await screen.findByLabelText('Tallinna Saksa Gümnaasium')).not.toBeChecked();
    expect(await screen.findByLabelText('Tallinna 32. Keskkool')).not.toBeChecked();
  });

  it('should disable school selection when there is only one option', async () => {
    const updateProps = { ...journeyUpdateProps() };
    const state = getInitialState();
    state.authentication.account.personRoles = [];

    store = mockStore(state);
    renderWithRedux(updateProps);

    const selectSchool = await screen.findByLabelText('Tallinna Kadrioru Saksa Gümnaasium');
    expect(selectSchool).toBeDisabled();
  });

  it('should show error message when user has no teacher roles', () => {
    const updateProps = journeyUpdateProps();
    const state = getInitialState();

    updateProps.match.params.id = undefined;
    state.authentication.account.personRoles = [
      {
        id: 1,
        role: ROLE.STUDENT,
        school: {
          id: 1,
          name: 'Tallinna Saksa Gümnaasium',
        },
      },
    ];
    store = mockStore(state);
    renderWithRedux(updateProps);
    screen.getByText('schoolabyApp.journey.noTeacherRoleYetMessage');
  });

  it('should fill journey title input field when journeyTitle param in url', async () => {
    search = 'journeyTitle=Test';
    const updateProps = journeyUpdateProps();
    updateProps.match.params.id = undefined;
    renderWithRedux(updateProps);

    await screen.findByDisplayValue('Test');
  });

  it('should fill journey form when template in url', async () => {
    search = 'template=1';
    const updateProps = journeyUpdateProps();
    updateProps.match.params.id = undefined;
    renderWithRedux(updateProps);

    await screen.findByDisplayValue('Journey title');
    await screen.findByDisplayValue('Matemaatika 8. klassile');
    expect(screen.queryByLabelText('I kooliaste')).toBeChecked();
    expect(screen.queryByLabelText('schoolabyApp.journey.curriculum.EXTRACURRICULAR')).toBeChecked();
  });
});
