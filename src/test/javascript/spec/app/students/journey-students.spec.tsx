import React from 'react';
import { IJourneyStudentProps, JourneyStudentsView } from 'app/journey/journey-detail/journey-students/journey-students-view';
import { render, screen, waitFor, within } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { AUTHORITIES } from 'app/config/constants';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { RootProvider } from 'app/shared/contexts/root-context';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ThroughProvider } from 'react-through';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: jest.fn(),
    location: {
      pathname: '/journey/1/students',
    },
  }),
}));

describe('Journey students', () => {
  let store: any;
  const mockStore = configureMockStore();
  const history = createBrowserHistory();
  const queryClient = new QueryClient();

  function journeyStudentProps(): IJourneyStudentProps {
    return {
      isTeacherOrAdmin: true,
      account: {},
      history: undefined,
      match: {
        params: {
          id: '1',
        },
        isExact: true,
        path: 'path',
        url: 'custom/url',
      },
      location: undefined,
      currentLocale: 'et',
    };
  }

  beforeEach(() => {
    const initialState = {
      locale: {
        currentLocale: 'et',
      },
      authentication: {
        account: {
          id: 6,
          authorities: [AUTHORITIES.TEACHER],
        },
      },
      applicationProfile: false,
    };
    store = mockStore(initialState);
  });

  const renderWithRedux = (journeyProps: IJourneyStudentProps = journeyStudentProps()) => {
    return {
      ...render(
        <RootProvider>
          <Provider store={store}>
            <EntityUpdateProvider>
              <Router history={history}>
                <QueryClientProvider client={queryClient}>
                  <ThroughProvider>
                    <JourneyStudentsView {...journeyProps} />
                  </ThroughProvider>
                </QueryClientProvider>
              </Router>
            </EntityUpdateProvider>
          </Provider>
        </RootProvider>
      ),
      store,
    };
  };

  it('Renders component', async () => {
    renderWithRedux();
    await waitFor(() => {
      screen.getByText('schoolabyApp.journey.members.students');
      screen.getByText('Student1 Student1');
      screen.getByText('Student2 Student2');
      expect(screen.queryByText('teacher teacher')).toBeNull();
    });
  });

  it('Should show MyJourneysBreadcrumbItem and JourneyTitleBreadcrumbItem and StudentsBreadcrumbItem', async () => {
    renderWithRedux();
    await waitFor(() => {
      const breadcrumbs = screen.getByLabelText('breadcrumb');
      const myJourneysBreadcrumbItem = within(breadcrumbs).getByText('global.menu.journeys');
      const journeyTitleBreadcrumbItem = within(breadcrumbs).getByText('Journey title');
      const studentsBreadcrumbItem = within(breadcrumbs).getByText('global.heading.tabs.students');

      expect(myJourneysBreadcrumbItem).toHaveAttribute('href', '/journey');
      expect(journeyTitleBreadcrumbItem).toHaveAttribute('href', '/journey/1');
      expect(studentsBreadcrumbItem).toHaveAttribute('href', '/journey/1/students');
    });
  });

  it('Renders last login date values to users', async () => {
    renderWithRedux();
    await waitFor(() => {
      screen.getByText('Student1 Student1');
      screen.queryByText('19.2.2021');
      expect(screen.queryByText('Student2 Student2'));
      expect(screen.queryByText('-'));
    });
  });

  it('should display journey co-teachers table', async () => {
    renderWithRedux();

    await screen.findByText('schoolabyApp.journey.members.coTeachers');
    expect(await screen.findAllByText('global.form.name')).toHaveLength(2);
    await screen.findByText('schoolabyApp.journey.joinDate');
    await screen.findByText('entity.action.delete');
    await screen.findByText('Co Teacher');
    await screen.findByText(/29.04.2022/);
  });

  it('should not display journey co-teachers table if teacher not journey owner', async () => {
    const props = journeyStudentProps();
    props.match.params.id = '2';

    renderWithRedux(props);

    await screen.findByText('schoolabyApp.journey.members.students');

    await waitFor(() => {
      expect(screen.queryByText('schoolabyApp.journey.members.coTeachers')).not.toBeInTheDocument();
    });
  });

  it('should not display journey co-teachers table if no co-teachers in journey', async () => {
    const props = journeyStudentProps();
    props.match.params.id = '3';

    renderWithRedux(props);

    await screen.findByText('schoolabyApp.journey.members.students');

    await waitFor(() => {
      expect(screen.queryByText('schoolabyApp.journey.members.coTeachers')).not.toBeInTheDocument();
    });
  });
});
