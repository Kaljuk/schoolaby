import Feedback from 'app/assignment/assignment-detail/students/feedback/feedback';
import React from 'react';
import { render, screen, fireEvent, within } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';
import ContextProvider from '../../helper/context-mock/context-provider';
import MockEntityDetailProvider from '../../helper/context-mock/mock-entity-detail-provider';
import { SubmissionGradingProvider } from 'app/shared/contexts/submission-grading-context';
import { QueryClientProvider, QueryClient } from 'react-query';
import { AssignmentContext } from 'app/journey/journey-detail/journey-detail-new';
import { IAssignment } from 'app/shared/model/assignment.model';
import { IJourney } from 'app/shared/model/journey.model';
import { EntityUpdateProvider } from 'app/shared/contexts/entity-update-context';
import { createRubric } from 'app/shared/util/rubric';
import { AUTHORITIES } from 'app/config/constants';

describe('Feedback', () => {
  const getDefaultStore = () => ({
    authentication: {
      account: {
        authorities: [AUTHORITIES.TEACHER],
      },
    },
    locale: {
      currentLocale: 'en',
    },
  });

  const getDefaultAssignment = (): IAssignment => ({
    id: 1234,
    title: 'Default assignment',
  });

  const getDefaultJourney = (): IJourney => ({
    id: 1,
    title: 'Default journey',
  });

  const renderComponent = (store, props, entityDetailContext, assignmentContext) => {
    const mockStore = configureMockStore();
    const queryClient = new QueryClient();

    render(
      <QueryClientProvider client={queryClient}>
        <ContextProvider props={entityDetailContext} contextProvider={MockEntityDetailProvider} store={mockStore(store)}>
          <EntityUpdateProvider>
            <SubmissionGradingProvider>
              <AssignmentContext.Provider value={assignmentContext}>
                <Feedback {...props} />
              </AssignmentContext.Provider>
            </SubmissionGradingProvider>
          </EntityUpdateProvider>
        </ContextProvider>
      </QueryClientProvider>
    );
  };

  it('should render', () => {
    const store = getDefaultStore();
    const assignment = getDefaultAssignment();
    const props = { assignment };
    const entityDetailContext = {
      selectedUser: {
        firstName: 'Firstname',
        lastName: 'Lastname',
      },
    };
    const assignmentContext = {
      assignment,
      isAllowedToModify: true,
      journey: getDefaultJourney(),
      rubric: createRubric(),
    };

    renderComponent(store, props, entityDetailContext, assignmentContext);

    screen.getByText('schoolabyApp.assignment.detail.feedbackTitle Firstname Lastname');
    screen.getByText('schoolabyApp.assignment.rubric.viewTitle');
    screen.getByLabelText('schoolabyApp.assignment.detail.grade');
    screen.getByLabelText('schoolabyApp.assignment.detail.feedback');
    screen.getByLabelText('schoolabyApp.submission.submissionInfo.resubmittable');
    screen.getByText('global.upload.camera');
    screen.getByText('global.upload.browse');
    screen.getByText('schoolabyApp.assignment.detail.submitGrade');
  });

  it('should display rubric overview', async () => {
    const store = getDefaultStore();
    const assignment = getDefaultAssignment();
    const props = { assignment };
    const rubric = createRubric();
    const entityDetailContext = {
      selectedUser: {
        firstName: 'Firstname',
        lastName: 'Lastname',
      },
    };
    const assignmentContext = {
      assignment,
      isAllowedToModify: true,
      journey: getDefaultJourney(),
      rubric,
    };

    renderComponent(store, props, entityDetailContext, assignmentContext);

    screen.getByText('schoolabyApp.assignment.detail.feedbackTitle Firstname Lastname');
    fireEvent.click(screen.getByText('schoolabyApp.assignment.rubric.viewTitle'));
    const rubricOverview = await screen.findByTestId('rubric-overview');
    await within(rubricOverview).findByText('schoolabyApp.assignment.rubric.gradingTitle Firstname Lastname');
    await within(rubricOverview).findByText('Criterion 1');
    await within(rubricOverview).findByText('Criterion 2');
    expect(await within(rubricOverview).findAllByText('Criterion description')).toHaveLength(2);
    expect(await within(rubricOverview).findAllByText('Level 1')).toHaveLength(2);
    expect(await within(rubricOverview).findAllByText('Level 2')).toHaveLength(2);
    expect(await within(rubricOverview).findAllByText('Level description')).toHaveLength(4);
    await within(rubricOverview).findByText('entity.action.cancel');
    await within(rubricOverview).findByText('entity.action.save');
  });

  it('should show max possible score', async () => {
    const store = getDefaultStore();

    const assignment = { ...getDefaultAssignment(), gradingSchemeId: 7 };

    const props = { assignment };

    const entityDetailContext = {
      selectedUser: {
        firstName: 'Firstname',
        lastName: 'Lastname',
      },
    };

    const assignmentContext = {
      assignment,
      isAllowedToModify: true,
      journey: getDefaultJourney(),
      rubric: createRubric(),
    };

    renderComponent(store, props, entityDetailContext, assignmentContext);
    await screen.findByPlaceholderText(/\/ 12/i);
  });
});
